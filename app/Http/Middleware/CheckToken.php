<?php

namespace App\Http\Middleware;

use App\ApiService\Master\ProgramApi;
use Closure;
use Session;

class CheckToken
{
    private $programApi;

    public function __construct()
    {
        $this->programApi = new ProgramApi();
    }

    public function handle($request, Closure $next)
    {
        // dd(session('role'));
        $slug = $request->segment(2);

        if (session('role') == 'learning-admin') {
            $slug = 'e_learning';
        }
        $slug = $this->programApi->get_by_slug($slug);
        // dd($slug);
        if ($slug['code'] == 200) {
            $request->session()->put('config', $slug['body']['data']['kode']);
            $request->session()->put('nama_program', $slug['body']['data']['nama']);
        }
        // dd(session()->all());

        if (!$request->session()->exists('token')) {

            $request->session()->put("URL_PWD", $request->getUri());
            // $pesan = array(
            //     'message' => 'Sessi anda untuk login telah berkhir',
            //     'icon' => 'error',
            //     'status' => 'gagal',
            // );
            return redirect()->route('auth.login');
        }
        //return $response;
        return $next($request);
    }
}
