<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Cache;

use Closure;

class OptimizeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * @var $response Response
         */

        $response = $next($request);

        $contentType = $response->headers->get('Content-Type');

        if (strpos($contentType, 'text/html') !== false) {
            $response->setContent($this->minify($response->getContent()));
        }

        return $response;
    }

    public function minify($input)
    {
        $search = [
            '/\>\s+/s',  // strip whitespaces after tags, except space
            '/\s+</s',  // strip whitespaces before tags, except space
            '/\n(\s+)?\/\/[^\n]*/',
            '!/\*[^*]*\*+([^/][^*]*\*+)*/!',
            '/\/\*[^\/]*\*\//',
            '/\/\*\*((\r\n|\n) \*[^\n]*)+(\r\n|\n) \*\//',
        ];

        $replace = [
            '> ',
            ' <',
            '',
            '',
            '',
            '',
            '',
        ];

        return preg_replace($search, $replace, $input);
    }

}
