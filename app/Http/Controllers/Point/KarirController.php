<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Kesiswaan\KarirApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KarirController extends Controller
{
    private $karirApi;
    private $rombelApi;
    private $jurusanApi;
    private $kelasSiswaApi;
    private $tahunApi;

    public function __construct()
    {
        $this->karirApi = new KarirApi();
        $this->jurusanApi = new JurusanApi();
        $this->rombelApi = new RombelApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        // dd(session()->all());
        $karir = $this->karirApi->get_by_sekolah();
        // dd($karir);
        $karir = $karir['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        session()->put('title', 'karir');
        return view('content.point.bimbingan_konseling.v_karir')->with(['template' => 'default', 'karir' => $karir, 'jurusan' => $jurusan, 'rombel' => $rombel, 'tahun' => $tahun]);
    }

    public function detail(Request $request)
    {
        $karir = $this->karirApi->get_by_id($request['id']);
        $karir = $karir['body']['data'];
        return response()->json($karir);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kelas_siswa' => implode(',', $request['id_kelas_siswa']),
            'tahun_ajaran' => $request['id_tahun_ajar'],
            'universitas' => $request['universitas'],
            'diterima' => $request['diterima'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->karirApi->create(json_encode($data));
        // dd($result);
        $karir = $this->data_karir();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $karir
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $karir
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_rombel' => $request['rombel'],
            'id_siswa' => $request['id_siswa'],
            'tahun_ajaran' => $request['tahun_ajaran'],
            'universitas' => $request['universitas'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->karirApi->update_info(json_encode($data));
        $karir = $this->data_karir();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $karir
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $karir
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->karirApi->soft_delete($request['id']);
        $karir = $this->data_karir();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $karir
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $karir
            ]);
        }
    }

    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'diterima' => $request['diterima']
        );
        $update = $this->karirApi->update_status(json_encode($data));
        $karir = $this->data_karir();
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil',
                'html' => $karir
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal',
                'html' => $karir
            ]);
        }
    }

    public function data_trash()
    {
        $trash = $this->load_trash();
        return response()->json($trash);
    }

    public function restore(Request $request)
    {
        $restore = $this->karirApi->restore($request['id']);
        $trash = $this->load_trash();
        $karir = $this->data_karir_beasiswa();
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
                'karir' => $karir,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
                'karir' => $karir,
            ]);
        }
    }

    public function hard_delete(Request $request)
    {
        $delete = $this->karirApi->delete($request['id']);
        $trash = $this->load_trash();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
            ]);
        }
    }

    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => Help::decode($request['id_jurusan']),
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        $siswa = $siswa['body']['data'];
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                    <td>
                    ' . $nomer++ . '</td>
                    <td>' . ucfirst($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['nisn'] . '</td>
                    <td>' . $sw['rombel'] . '</td>
                    <td>' . $sw['jurusan'] . '</td>
                    <td class="text-center"> <input class="form-check-input ml-0" name="id_kelas_siswa[]" value="'.$sw['id'].'" type="checkbox" value="" id="flexCheckDefault"></td>
                </tr>
               ';
            }
            $html .= '<tr>
                    <td colspan="6"></td><td class="text-center"> <button data-toggle="collapse" data-target="#detail' . $sw['id'] . '"
                    class="btn btn-sm btn-success accordion-toggle"><i class="fas fa-info-circle"></i> Selengkapnya</button>
                    </td>
                </tr>

                <tr>
                    <td class="hiddenRow" colspan="7">
                        <div class="accordian-body collapse" id="detail' . $sw['id'] . '">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th class="vertical-middle">Tahun Ajaran</th>
                                    <th>
                                    <select name="id_tahun_ajar" class="form-control" id="id_tahun_ajar">';
            $tahun = $this->tahunApi->get_by_sekolah();
            $tahun = $tahun['body']['data'];
            foreach ($tahun as $th) {
                $select = '';
                if ($th['id'] == session('id_tahun_ajar')) {
                    $select = "selected";
                }
                $html .= '<option value="'.$th['tahun_ajaran'].'" '.$select.'>'.$th['tahun_ajaran'].' '.$th['semester'].'</option>';
            }
            $html .= '</select>
                                    </th>
                                    <th></th>
                                    <th class="vertical-middle">Universitas</th>
                                    <th><input type="text" class="form-control" id="universitas" name="universitas"></th>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Diterima</th>
                                    <th>
                                    <select class="form-control" id="diterima" name="diterima">
                                    <option value="proses">Proses</option>
                                    <option value="tidak">Tidak</option>
                                    <option value="ya">Iya</option>
                                    </select>
                                    </th>
                                    <th></th>
                                    <th class="vertical-middle">Keterangan</th>
                                    <th><input type="text" class="form-control" id="keterangan" name="keterangan"></th>
                                </tr>
                                <tr>
                                <td colspan="5"><button class="btn btn-info btn-block" id="btnSimpanBeasiswa">Simpan</button</td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>';
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    private function load_trash()
    {
        $trash = $this->karirApi->all_trash();
        $trash = $trash['body']['data'];
        $html = '';

        if (!empty($trash)) {
            $no = 1;
            foreach ($trash as $tr) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$tr['nama'].'</td>
                    <td>'.$tr['kategori'].'</td>
                    <td>'.$tr['keterangan'].'</td>
                    <td>
                        <a href="javascript:void(0)" class="restore btn btn-info btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-undo"></i> Pulihkan
                        </a>
                        <a href="javascript:void(0)" class="hardDelete btn btn-danger btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-trash-alt"></i> Hapus Permanent
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '
           <tr><td colspan="5" class="text-center">Data saat ini belum tersedia</td></tr>
           ';
        }
        return $html;
    }

    private function data_karir()
    {
        $karir = $this->karirApi->get_by_sekolah();
        $karir = $karir['body']['data'];
        $html = '';
        if (!empty($karir)) {
            $no = 1;
            foreach ($karir as $kr) {
                $badge_color = '';
                if ($kr['diterima'] == 'ya') {
                    $badge_color = 'badge-success';
                } elseif ($kr['diterima'] == 'tidak') {
                    $badge_color = 'badge-danger';
                } else {
                    $badge_color = 'badge-info';
                }
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$kr['nama'].'</td>
                    <td>'.$kr['nisn'].'</td>
                    <td>'.$kr['rombel'].'</td>
                    <td>'.$kr['tahun_ajaran'].'</td>
                    <td>'.$kr['universitas'].'</td>
                    <td><a href="javascript:void(0)" data-id="'.$kr['id'].'" data-value="'.$kr['diterima'].'" class="edit_diterima"><span class="badge '.$badge_color.' text-inverse">'.$kr['diterima'].'</span></a></td>
                    <td>
                        <a href="javascript:void(0)" class="edit" data-id="'.$kr['id'].'">
                            <i class="material-icons list-icon md-18 text-info">edit</i>
                        </a>
                        <a href="javascript:void(0)" class="delete" data-id="'.$kr['id'].'">
                            <i class="material-icons list-icon md-18 text-danger">delete</i>
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="8" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
