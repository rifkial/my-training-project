<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Master\PesanApi;
use App\ApiService\User\BKApi;
use App\ApiService\User\OrangTuaApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Support\Facades\Session;

class PesanController extends Controller
{
    private $ortuApi;
    private $pesanApi;
    private $bkApi;

    public function __construct()
    {
        $this->ortuApi = new OrangTuaApi();
        $this->pesanApi = new PesanApi();
        $this->bkApi = new BKApi();
    }

    public function index()
    {
        // dd(session()->all());
        if (session("role") == "bk") {
            $list_chat = $this->ortuApi->get_by_sekolah();
        } else {
            $list_chat = $this->bkApi->get_by_sekolah();
        }
        // dd($list_chat);
        
        // $ortu = $this->ortuApi->get_by_sekolah();
        $list_chat = $list_chat['body']['data'];
        Session::put('title', 'Sanksi Pelanggaran');
        return view('content.point.bimbingan_konseling.v_pesan')->with(['template' => session('template'), 'list_chat' => $list_chat]);
    }

    public function get_chat(Request $request)
    {
        // dd($request);
        $pesan = $this->pesanApi->data_chat($request['email_penerima']);
        // dd($pesan);
        $pesan = $pesan['body']['data'];
        return response()->json($pesan);
    }

    public function save_chat(Request $request)
    {
        // dd($request);
        $encr = encrypt($request['email_penerima']);
        $data = array(
            'email_penerima' => $request['email_penerima'],
            'pesan' => $request['message'],
            'role' => session('role'),
        );
        // dd($data);
        $tampil_chat = $this->pesanApi->tampil_email($request['email_penerima']);
        // dd($tampil_chat);
        $nama = $tampil_chat['body']['data']['nama'];
        $broadcoast =  $this->broadcastMessage($nama, $request['pesan']);
        // dd($broadcoast);
        $result = $this->pesanApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    private function broadcastMessage($senderName, $message)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('New message from : ' . $senderName);
        $notificationBuilder->setBody($message)
            ->setSound('default')
            ->setClickAction(url()->current());
        $dataBuilder = new PayLoadDataBuilder();
        $dataBuilder->addData([
            'sender_name' => $senderName,
            'message' => $message
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        // $tokens = User::all()->pluck('fcm_token')->toArray();
        $tokens = $this->pesanApi->tes();
        // dd($tokens);
        $tokens = $tokens['body'];
        // dd($tokens);
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
        // dd($downstreamResponse);

        return $downstreamResponse->numberSuccess();
    }
}
