<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Master\PesanApi;
use App\ApiService\User\BKApi;
use App\ApiService\User\OrangTuaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class EmailController extends Controller
{
    private $ortuApi;
    private $pesanApi;
    private $bkApi;
    private $hash;

    public function __construct()
    {
        $this->ortuApi = new OrangTuaApi();
        $this->pesanApi = new PesanApi();
        $this->bkApi = new BKApi();
        $this->hash = new Hashids();
    }

    public function index()
    {
        if (session("role") == 'bk') {
            $email = $this->ortuApi->get_by_sekolah();
        } else {
            $email = $this->bkApi->get_by_sekolah();
        }
        $email = $email['body']['data'];
        Session::put('title', 'Sanksi Pelanggaran');
        return view('content.point.bimbingan_konseling.email.v_email')->with(['template' => 'default', 'email' => $email]);
    }

    public function pesan($slug)
    {
        Session::put('title', 'Pesan Konseling');
        if ($slug == "inbox") {
            return view('content.point.bimbingan_konseling.email.v_inbox')->with(['template' => 'default']);
        } elseif ($slug == 'send') {
            return view('content.point.bimbingan_konseling.email.v_send')->with(['template' => 'default']);
        } else {
            return view('content.point.bimbingan_konseling.email.v_trash')->with(['template' => 'default']);
        }
    }

    public function save_chat(Request $request)
    {
        $kelas_siswa = implode(",",$request['id_kelas_siswa']);
        $data = array(
            'id_kelas_siswa' => $kelas_siswa,
            'pesan' => $request['pesan'],
            'subject' => $request['subject'],
            'tujuan' => $request['asal'],
            'role' => session('role'),
        );


        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pesanApi->create_file_bk(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pesanApi->create_bk(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function get_datatable(Request $request)
    {
        $output = $request['output'];
        if ($output == 'inbox') {
            $pesan = $this->pesanApi->terima();
        } elseif ($output == 'chat_send') {
            $pesan = $this->pesanApi->terkirim();
        } else {
            $pesan = $this->pesanApi->all_trash();
        }
        // dd($pesan);
        $result = $pesan['body']['data'];
        $table = datatables()->of($result);
        if ($output != "trash") {
            $table->addColumn('subject', function ($data) {
                return '<a href="/program/point/pesan_email/detail_pesan/' . $this->hash->encode($data['id']) . '">' . $data['subject'] . '</a>';
            });
        } else {
            $table->addColumn('subject', function ($data) {
                return $data['subject'];
            });
        }
        if ($output == 'chat_send') {
            $table->editColumn('penerima', function ($pn) {
                return  $pn['penerima'];
            });
        } else {
            $table->editColumn('penerima', function ($pnr) {
                return $pnr['pengirim'];
            });
        }

        $table->editColumn('select', function ($rows) {
            $check =  '<input type="checkbox" class="delete_check" id="delcheck_' . $rows['id'] . '" value="' . $rows['id'] . '">';
            return $check;
        });
        if ($output != "trash") {
            $table->editColumn('pesan', function ($row) {
                return  str_word_count($row['pesan']) > 60 ? '<a href="/program/point/pesan_email/detail_pesan/' . $this->hash->encode($row['id']) . '">' . substr($row['pesan'], 0, 200) . "[..]" . '</a>' :  '<a href="/program/point/pesan_email/detail_pesan/' . $this->hash->encode($row['id']) . '"> ' . $row['pesan'] . '</a>';
            });
        } else {
            $table->editColumn('pesan', function ($row) {
                return (str_word_count($row['pesan']) > 60 ? substr($row['pesan'], 0, 200) . "[..]" : $row['pesan']);
            });
        }

        if ($output == "inbox") {
            $table->addColumn('status', function ($st) {
                if ($st['dibaca'] == "Belum") {
                    return '<span class="badge badge-danger text-inverse">' . $st['dibaca'] . '</span>';
                } else {
                    return '<span class="badge badge-success text-inverse">' . $st['dibaca'] . '</span>';
                }
            });
        } else {
            $table->addColumn('status', function ($st) {
                return '';
            });
        }
        $table->rawColumns(['subject', 'select', 'pesan', 'penerima', 'status']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function delete_array(Request $request)
    {
        $deleteids_arr = array();
        if (isset($request->deleteids_arr)) {
            $deleteids_arr = $request->deleteids_arr;
        }
        // dd($deleteids_arr);
        foreach ($deleteids_arr as $deleteid) {
            if ($request->aksi == 'soft_del') {
                $delete = $this->pesanApi->soft_delete($deleteid);
            } else {
                $delete = $this->pesanApi->delete($deleteid);
            }
        }
        dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $pesan = $this->pesanApi->get_by_id($request['id_pesan']);
        $result = $pesan['body']['data'];
        return response()->json($result);
    }

    public function detail_pesan($id)
    {
        $url = url()->previous();
        $last = explode("/", $url);
        if (session("role") == 'bk') {
            $email = $this->ortuApi->get_id_sekolah();
        } else {
            $email = $this->bkApi->get_by_sekolah();
        }
        $email = $email['body']['data'];
        $pesan = $this->pesanApi->get_by_id($this->hash->decode($id)[0]);
        // dd($pesan);
        $pesan = $pesan['body']['data'];
        $pesan['data_dari'] = last($last);
        // dd($pesan);
        if (last($last) == "inbox") {
            $status = $this->pesanApi->get_status_baca($this->hash->decode($id)[0]);
        }
        // dd($pesan);
        return view('content.point.bimbingan_konseling.email.v_detail_pesan')->with(['template' => 'default', 'email' => $email, 'pesan' => $pesan]);
    }

    public function delete(Request $request)
    {
        $delete = $this->pesanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function reply(Request $request)
    {
        // dd($request);
        $data = array(
            'sosial_penerima' => $request['sosial_penerima'],
            'pesan' => $request['pesan'],
            'subject' => $request['subject'],
            'role_pengirim' => $request['role_pengirim'],
            'role_penerima' => $request['role_penerima'],
        );


        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pesanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pesanApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
