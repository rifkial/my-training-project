<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Kesiswaan\BeasiswaApi;
use App\ApiService\Kesiswaan\JenisBeasiswaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use XcS\XcTools;

class BeasiswaController extends Controller
{
    private $beasiswaApi;
    private $kelasSiswaApi;
    private $jurusanApi;
    private $jenisApi;
    private $tahunApi;

    public function __construct()
    {
        $this->beasiswaApi = new BeasiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->jenisApi = new JenisBeasiswaApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        $jenis = $this->jenisApi->get_by_sekolah();
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        session()->put('title', 'Beasiswa');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        return view('content.point.bimbingan_konseling.v_beasiswa')->with(['template' => 'default', 'jenis' => $jenis, 'jurusan' => $jurusan, 'tahun' => $tahun, 'url' => $this->beasiswaApi->import(session('id_sekolah'))]);
    }

    public function detail(Request $request)
    {
        $beasiswa = $this->beasiswaApi->get_by_id($request['id']);
        $beasiswa = $beasiswa['body']['data'];
        return response()->json($beasiswa);
    }

    public function store(Request $request)
    {
        $data = array(
            'id_kelas_siswa' => implode(',', $request['id_kelas_siswa']),
            'id_jenis_beasiswa' => $request['id_jenis_beasiswa'],
            'tahun_ajaran' => $request['id_tahun_ajar'],
            'sumber' => $request['sumber'],
            'nominal' => $request['nominal'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        $result = $this->beasiswaApi->create(json_encode($data));
        // dd($result);
        $beasiswa = $this->data_beasiswa($request['id_jenis_beasiswa']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $beasiswa
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $beasiswa
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_siswa' => $request['id_siswa'],
            'id_rombel' => $request['id_rombel'],
            'id_jenis_beasiswa' => $request['id_jenis_beasiswa'],
            'tahun_ajaran' => $request['tahun_ajaran'],
            'sumber' => $request['sumber'],
            'nominal' => $request['nominal'],
            'keterangan' => $request['keterangan'],
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        $result = $this->beasiswaApi->update_info(json_encode($data));
        $beasiswa = $this->data_beasiswa($request['id_jenis_beasiswa']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $beasiswa
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $beasiswa
            ]);
        }
    }

    public function delete(Request $request)
    {
        $detail = $this->beasiswaApi->get_by_id($request['id']);
        $detail = $detail['body']['data'];
        // dd($detail);
        $delete = $this->beasiswaApi->soft_delete($request['id']);
        $beasiswa = $this->data_beasiswa($detail['id_jenis_beasiswa']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $beasiswa
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $beasiswa
            ]);
        }
    }

    private function data_beasiswa($id)
    {
        $beasiswa = $this->beasiswaApi->by_jenis($id);
        // dd($beasiswa);
        $beasiswa = $beasiswa['body']['data'];
        $html = '';
        if (!empty($beasiswa)) {
            $no = 1;
            foreach ($beasiswa as $bs) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$bs['nama_siswa'].'</td>
                    <td>'.$bs['nisn'].'</td>
                    <td>'.$bs['rombel'].'</td>
                    <td>'.$bs['tahun_ajaran'].'</td>
                    <td>'.$bs['sumber'].'</td>
                    <td>'.$bs['nominal'].'</td>
                    <td>
                        <a href="javascript:void(0)" class="info" data-id="'.$bs['id'].'">
                            <i class="material-icons list-icon md-18 text-success">info</i>
                        </a>
                        <a href="javascript:void(0)" class="edit" data-id="'.$bs['id'].'">
                            <i class="material-icons list-icon md-18 text-info">edit</i>
                        </a>
                        <a href="javascript:void(0)" class="delete" data-id="'.$bs['id'].'" data-jenis="'.$id.'">
                            <i class="material-icons list-icon md-18 text-danger">delete</i>
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => Help::decode($request['id_jurusan']),
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        $siswa = $siswa['body']['data'];
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                    <td>
                    ' . $nomer++ . '</td>
                    <td>' . ucfirst($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['nisn'] . '</td>
                    <td>' . $sw['rombel'] . '</td>
                    <td>' . $sw['jurusan'] . '</td>
                    <td class="text-center"> <input class="form-check-input ml-0" name="id_kelas_siswa[]" value="'.$sw['id'].'" type="checkbox" value="" id="flexCheckDefault"></td>
                </tr>
               ';
            }
            $html .= '<tr>
                    <td colspan="6"></td><td class="text-center"> <button data-toggle="collapse" data-target="#detail' . $sw['id'] . '"
                    class="btn btn-sm btn-success accordion-toggle"><i class="fas fa-info-circle"></i> Selengkapnya</button>
                    </td>
                </tr>

                <tr>
                    <td class="hiddenRow" colspan="7">
                        <div class="accordian-body collapse" id="detail' . $sw['id'] . '">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th class="vertical-middle">Tahun Ajaran</th>
                                    <th>
                                    <select name="id_tahun_ajar" class="form-control" id="id_tahun_ajar">';
            $tahun = $this->tahunApi->get_by_sekolah();
            $tahun = $tahun['body']['data'];
            foreach ($tahun as $th) {
                $select = '';
                if ($th['id'] == session('id_tahun_ajar')) {
                    $select = "selected";
                }
                $html .= '<option value="'.$th['tahun_ajaran'].'" '.$select.'>'.$th['tahun_ajaran'].' '.$th['semester'].'</option>';
            }
            $html .= '</select>
                                    </th>
                                    <th></th>
                                    <th class="vertical-middle">Sumber</th>
                                    <th><input type="text" class="form-control" id="sumber" name="sumber"></th>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Nominal</th>
                                    <th><input type="number" class="form-control" id="nominal" name="nominal"></th>
                                    <th></th>
                                    <th class="vertical-middle">Keterangan</th>
                                    <th><input type="text" class="form-control" id="keterangan" name="keterangan"></th>
                                </tr>
                                <tr>
                                <td colspan="5"><button class="btn btn-info btn-block" id="btnSimpanBeasiswa">Simpan</button</td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>';
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->beasiswaApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            $message = array(
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            );
        }
        return redirect()->back()->with(['message' => $message]);
    }

    public function data_trash()
    {
        $trash = $this->load_trash();
        return response()->json($trash);
    }

    private function load_trash()
    {
        $trash = $this->beasiswaApi->all_trash();
        $trash = $trash['body']['data'];
        $html = '';

        if (!empty($trash)) {
            $no = 1;
            foreach ($trash as $tr) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$tr['nama_siswa'].'</td>
                    <td>'.$tr['nisn'].'</td>
                    <td>'.$tr['nama_beasiswa'].'</td>
                    <td>'.$tr['sumber'].'</td>
                    <td>'.$tr['nominal'].'</td>
                    <td>
                        <a href="javascript:void(0)" class="restore btn btn-info btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-undo"></i>
                        </a>
                        <a href="javascript:void(0)" class="hardDelete btn btn-danger btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '
           <tr><td colspan="7" class="text-center">Data saat ini belum tersedia</td></tr>
           ';
        }
        return $html;
    }

    public function restore(Request $request)
    {
        // dd($request);
        $restore = $this->beasiswaApi->restore($request['id']);
        $trash = $this->load_trash();
        $jenis = $this->load_data_beasiswa();
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
                'jenis' => $jenis,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
                'jenis' => $jenis,
            ]);
        }
    }

    public function hard_delete(Request $request)
    {
        $delete = $this->beasiswaApi->delete($request['id']);
        $trash = $this->load_trash();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
            ]);
        }
    }

    private function load_data_beasiswa()
    {
        $jenis = $this->jenisApi->get_by_sekolah();
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $html = '';
        if (!empty($jenis)) {
            $no = 1;
            foreach ($jenis as $jn) {
                $html .= '
                <tr>
                <td>'.$no++.'</td>
                <td>'.$jn['nama'].'</td>
                <td>'.$jn['kategori'].'</td>
                <td>'.$jn['keterangan'].'</td>
                <td class="text-center">
                    <button data-toggle="collapse" data-target="#demo'.$jn['id'].'"
                        class="btn btn-sm btn-success accordion-toggle"><i
                            class="fas fa-info-circle"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo'.$jn['id'].'">
                        <button class="btn btn-info btn-sm my-3 pull-right addBeasiswa"
                            data-id="'.$jn['id'].'"><i class="fas fa-plus-circle"></i>
                            Tambah Beasiswa</button>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama Siswa</th>
                                    <th class="text-center">NISN</th>
                                    <th class="text-center">Rombel</th>
                                    <th class="text-center">Tahun Ajaran</th>
                                    <th class="text-center">Sumber</th>
                                    <th class="text-center">Nominal</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataBeasiswa'.$jn['id'].'">';
                if (!empty($jn['beasiswa'])) {
                    $nomer =1;
                    foreach ($jn['beasiswa'] as $bs) {
                        $html .= '
                                    <tr>
                                            <td class="text-center">'.$nomer++.'</td>
                                            <td class="text-center">'.$bs['nama_siswa'].'</td>
                                            <td class="text-center">'.$bs['nisn'].'</td>
                                            <td class="text-center">'.$bs['rombel'].'</td>
                                            <td class="text-center">'.$bs['tahun_ajaran'].'</td>
                                            <td class="text-center">'.$bs['sumber'].'</td>
                                            <td class="text-center">'.$bs['nominal'].'</td>
                                            <td class="text-center">
                                                <a href="javascript:void(0)" class="info"
                                                    data-id="'.$bs['id'].'">
                                                    <i
                                                        class="material-icons list-icon md-18 text-success">info</i>
                                                </a>
                                                <a href="javascript:void(0)" class="edit"
                                                    data-id="'.$bs['id'].'">
                                                    <i
                                                        class="material-icons list-icon md-18 text-info">edit</i>
                                                </a>
                                                <a href="javascript:void(0)" class="delete"
                                                    data-id="'.$bs['id'].'"
                                                    data-jenis="'.$jn['id'].'">
                                                    <i
                                                        class="material-icons list-icon md-18 text-danger">delete</i>
                                                </a>
                                            </td>
                                        </tr>
                                    ';
                    }
                } else {
                    $html .= '<tr><td colspan="8" class="text-center">Beasiswa saat ini belum  tersedia</td> </tr>';
                }
                $html .= '</tbody>
                            </table>
                        </div>
                    </td>
                </tr>';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Jenis Beasiswa tidak ditemukan</td></tr>';
        }
        return $html;
    }
}
