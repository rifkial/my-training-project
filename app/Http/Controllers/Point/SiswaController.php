<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Kesiswaan\PelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranSiswaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\User\SiswaApi;
use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SiswaController extends Controller
{
    private $jurusanApi;
    private $kelasSiswaApi;
    private $waliKelasApi;
    private $rombelApi;
    private $siswaApi;
    private $pelanggaranApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->waliKelasApi = new WaliKelasApi();
        $this->siswaApi = new SiswaApi();
        $this->rombelApi = new RombelApi();
        $this->pelanggaranApi = new PelanggaranSiswaApi();
    }

    public function index()
    {
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Data Siswa');
        return view('content.point.bimbingan_konseling.v_siswa')->with(['jurusan' => $jurusan, 'template' => 'default', 'rombel' => $rombel]);
    }

    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => Help::decode($request['id_jurusan']),
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    public function get_siswa_pesan(Request $request)
    {
        // dd($request);
        session()->put('request_point_temporary', [
            'id_jurusan' => Help::decode($request['id_jurusan']),
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa_search();
        if ($request['asal'] == 'siswa') {
            $text = "Pesan anda akan ditujukan ke Siswa";
        } else {
            $text = "Pesan anda akan ditujukan ke Orang Tua dari siswa yang terdaftar";
        }
        return response()->json([
            'siswa' => $siswa,
            'text' => $text,
            'asal' => $request['asal'],
        ]);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $walikelas = $this->waliKelasApi->get_by_rombel(session('request_point_temporary')['id_rombel']);
        // dd($walikelas);
        $walikelas = $walikelas['body']['data'];
        if (!empty($walikelas)) {
            $data_walikelas = '
            <div class="alert alert-info border-info" role="alert">
            <p class="mb-1"><strong>Biodata Wali Kelas:</strong>
            </p>
            <ul class="mr-t-10">
                <li>Nama : '.$walikelas['nama'].'</li>
                <li>NIP : '.$walikelas['nip'].'</li>
                <li>Telepon : '.$walikelas['telepon'].'</li>
                <li>Email : '.$walikelas['email'].'</li>
            </ul>
        </div>
            ';
        } else {
            $data_walikelas = '<div class="alert alert-warning border-warning" role="alert">
           <p class="mb-1"><strong>Biodata Wali Kelas:</strong>
           </p>
           <p class="mb-1">Wali kelas untuk saat ini belum tersedia</p>
       </div>';
        }

        $data_siswa = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $status = 'Aktif';
                $kelas_status = 'success';
                if ($sw['status'] != 1) {
                    $status = 'Tidak Aktif';
                    $kelas_status = 'danger';
                }
                $data_siswa .= '<tr>
                <td>'.$nomer++.'</td>
                <td>'.ucwords($sw['nama']).'</td>
                <td>'.$sw['nis'].'</td>
                <td>'.$sw['nisn'].'</td>
                <td>'.$sw['rombel'].'</td>
                <td>'.$sw['jurusan'].'</td>
                <td><span class="badge badge-'.$kelas_status.' text-inverse">'.$status.'</span></td>';
                if(session('role') != 'supervisor'){
                    $data_siswa .= ' <td class="text-center"><a href="javascript:void(0)" class="edit text-purple" data-id="'.$sw['id_siswa'].'"><i class="fas fa-user-edit"><i></a></td>';
                }
                $data_siswa .= '</tr>';
            }
        } else {
            $data_siswa .= '
            <tr>
                <td colspan="7" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        $data = array(
            'data_siswa' => $data_siswa,
            'data_walikelas' => $data_walikelas,
        );
        return $data;
    }

    private function data_siswa_search()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];

        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                <td>'.$nomer++.'</td>
                <td>'.$sw['nis'].'</td>
                <td>'.ucwords($sw['nama']).'</td>
                <td>'.$sw['rombel'].'</td>
                <td>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="id_kelas_siswa[]" value="'.$sw['id'].'"
                            id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            Pilih
                        </label>
                    </div>
                </td>
            </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="5" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function update_siswa(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'tahun_angkatan' => $request->tahun_angkatan,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'lintang' => $request->lintang,
            'bujur' => $request->bujur,
            'anak_ke' => $request->anak_ke,
            'status_keluarga' => $request->status_keluarga,
            'kls_diterima' => $request->kls_diterima,
            'tgl_diterima' => date('Y-m-d', strtotime($request->tgl_diterima)),
            'no_ijazah' => $request->no_ijazah,
            'th_ijazah' => $request->tahun_ijazah,
            'status_ortu' => $request->status_ortu,
            'no_skhun' => $request->no_skhun,
            'th_skhun' => $request->th_skhun,
            'nama_ayah' => $request->nama_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'nama_ibu' => $request->nama_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'nama_wali' => $request->nama_wali,
            'alamat_wali' => $request->alamat_wali,
            'telp_wali' => $request->telepon_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->siswaApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->siswaApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->siswaApi->update(json_encode($data));
            }
        }
        $siswa = $this->data_siswa();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'siswa' => $siswa
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'siswa' => $siswa
            ]);
        }
    }

    public function sering_dilakukan()
    {
        // dd("sering dilakukan");
        session()->put('title', 'Pelanggaran sering dilakukan');
        $pelanggaran = $this->pelanggaranApi->get_by_sering_dilakukan(session('id_kelas_siswa'));
        $pelanggaran = $pelanggaran['body']['data'];
        return view('content.point.bimbingan_konseling.v_sering_dilakukan')->with(['pelanggaran' => $pelanggaran, 'template' => 'default']);

    }
}
