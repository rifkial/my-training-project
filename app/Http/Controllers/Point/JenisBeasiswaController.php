<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Kesiswaan\JenisBeasiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use XcS\XcTools;

class JenisBeasiswaController extends Controller
{
    private $jenisApi;

    public function __construct()
    {
        $this->jenisApi = new JenisBeasiswaApi();
    }

    public function index()
    {
        $jenis = $this->jenisApi->get_by_sekolah();
        $jenis = $jenis['body']['data'];
        session()->put('title', 'Jenis Beasiswa');
        return view('content.point.bimbingan_konseling.v_jenis_beasiswa')->with(['template' => 'default', 'jenis' => $jenis, 'url' => $this->jenisApi->import()]);
    }

    public function detail(Request $request)
    {
        $jenis = $this->jenisApi->get_by_id($request['id']);
        $jenis = $jenis['body']['data'];
        return response()->json($jenis);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'kategori' => $request['kategori'],
            'keterangan' => $request['keterangan'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jenisApi->create(json_encode($data));
        $jenis = $this->data_jenis_beasiswa();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $jenis
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $jenis
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'kategori' => $request['kategori'],
            'keterangan' => $request['keterangan'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->jenisApi->update_info(json_encode($data));
        $jenis = $this->data_jenis_beasiswa();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $jenis
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $jenis
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->jenisApi->soft_delete($request['id']);
        $jenis = $this->data_jenis_beasiswa();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $jenis
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $jenis
            ]);
        }
    }

    public function data_trash()
    {
        $trash = $this->load_trash();
        return response()->json($trash);
    }

    public function restore(Request $request)
    {
        $restore = $this->jenisApi->restore($request['id']);
        $trash = $this->load_trash();
        $jenis = $this->data_jenis_beasiswa();
        if ($restore['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
                'jenis' => $jenis,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
                'jenis' => $jenis,
            ]);
        }
    }

    public function hard_delete(Request $request)
    {
        $delete = $this->jenisApi->delete($request['id']);
        $trash = $this->load_trash();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'trash' => $trash,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'trash' => $trash,
            ]);
        }
    }

    public function import(Request $request)
    {
        // dd($request);
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jenisApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        $jenis = $this->data_jenis_beasiswa();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'jenis' => $jenis
            ]);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal',
                'jenis' => $jenis
            ]);
        }
    }

    private function load_trash()
    {
        $trash = $this->jenisApi->all_trash();
        $trash = $trash['body']['data'];
        $html = '';

        if (!empty($trash)) {
            $no = 1;
            foreach ($trash as $tr) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$tr['nama'].'</td>
                    <td>'.$tr['kategori'].'</td>
                    <td>'.$tr['keterangan'].'</td>
                    <td>
                        <a href="javascript:void(0)" class="restore btn btn-info btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-undo"></i> Pulihkan
                        </a>
                        <a href="javascript:void(0)" class="hardDelete btn btn-danger btn-sm" data-id="'.$tr['id'].'">
                            <i class="fas fa-trash-alt"></i> Hapus Permanent
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '
           <tr><td colspan="5" class="text-center">Data saat ini belum tersedia</td></tr>
           ';
        }
        return $html;
    }

    private function data_jenis_beasiswa()
    {
        $jenis = $this->jenisApi->get_by_sekolah();
        $jenis = $jenis['body']['data'];
        $html = '';
        if (!empty($jenis)) {
            $no = 1;
            foreach ($jenis as $jn) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$jn['nama'].'</td>
                    <td>'.$jn['kategori'].'</td>
                    <td>'.$jn['keterangan'].'</td>
                    <td>
                        <a href="javascript:void(0)" class="edit" data-id="'.$jn['id'].'">
                            <i class="material-icons list-icon md-18 text-info">edit</i>
                        </a>
                        <a href="javascript:void(0)" class="delete" data-id="'.$jn['id'].'">
                            <i class="material-icons list-icon md-18 text-danger">delete</i>
                        </a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
