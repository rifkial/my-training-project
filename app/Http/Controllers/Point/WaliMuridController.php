<?php

namespace App\Http\Controllers\Point;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\OrangTuaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WaliMuridController extends Controller
{
    private $ortuApi;
    private $jurusanApi;
    private $tahunApi;
    private $rombelApi;

    public function __construct()
    {
        $this->ortuApi = new OrangTuaApi();
        $this->jurusanApi = new JurusanApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->rombelApi = new RombelApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Fakultas');
        $url = $this->ortuApi->import();
        $routes = "point_wali_murid-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        if (isset($_GET["jurusan"]) && $_GET["jurusan"] != '') {
            $jurusans = Help::decode($_GET["jurusan"]);
            $data_jr = $_GET['jurusan'];
        }else{
            $jurusans = "";
            $data_jr = "";
        }
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $asal = (isset($_GET["asal"])) ? $_GET["asal"] : " ";
        $tahuns = (isset($_GET["tahun"])) ? $_GET["tahun"] : "";
        $result = $this->ortuApi->get_by_sekolah($jurusans, $tahuns, $page, $search);
        // dd($result);
        $ortu = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::dataSiswa($data_jr, $tahuns, $meta, $routes, $search);
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        Session::put('title', 'Daftar Wali Murid');
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        return view('content.point.bimbingan_konseling.v_wali_murid')->with(['template' => 'default', 'jurusan' => $jurusan, 'search' => $search, 'select_jurusan' => $data_jr,
        'routes' => $routes, 'tahun' => $tahun, 'tahuns' => $tahuns, 'pagination' => $pagination, 'asal' => $asal, 'ortu' => $ortu, 'rombel' => $rombel, 'url' => $url]);
    }

    public function delete($id)
    {
        $delete = $this->pelanggaranApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
