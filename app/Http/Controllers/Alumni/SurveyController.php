<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\JawabanSurveyApi;
use App\ApiService\Alumni\KategoriSurveyApi;
use App\ApiService\Alumni\SurveyApi;
use App\Charts\JawabanChart;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    private $surveyApi;
    private $jawabanApi;
    private $katSurveyApi;
    private $hash;

    public function __construct()
    {
        $this->surveyApi = new SurveyApi();
        $this->jawabanApi = new JawabanSurveyApi();
        $this->katSurveyApi = new KategoriSurveyApi();
        $this->hash = new Hashids();
    }

    public function index()
    {
        $kategori = $this->katSurveyApi->get_id_sekolah(session('id'));
        $kategori = $kategori['body']['data'];
        if (session('role') == 'admin-alumni') {
            return view('content.alumni.survey.admin.v_survey')->with(['template' => session('template'), 'kategori' => $kategori]);
        } else {
            $kategori = $this->jawabanApi->cek_kategori();
            // dd($kategori);
            $kategori = $kategori['body']['data'];
            return view('content.dashboard.alumni.alumni-alumni')->with(['template' => session('template'), 'kategori' => $kategori]);
        }
    }

    public function select_kategori_survey(Request $request)
    {
        return response()->json([
            'success' => true,
            'url' => route('alumni-survey_by_kategori', $this->hash->encode($request['id']))
        ]);
    }

    public function store_soal(Request $request)
    {
        if ($request['jenis'] == 'pilihan') {
            $pilihan1 = $request['pilihan1'];
            $pilihan2 = $request['pilihan2'];
            $pilihan3 = $request['pilihan3'];
            $pilihan4 = $request['pilihan4'];
            $pilihan5 = $request['pilihan5'];
        } else {
            $pilihan1 = null;
            $pilihan2 = null;
            $pilihan3 = null;
            $pilihan4 = null;
            $pilihan5 = null;
        }
        $data = array(
            'id_kategori' => $this->hash->decode($request['id_kategori'])[0],
            'pertanyaan' => $request['pertanyaan'],
            'jenis' => $request['jenis'],
            'pilihan1' => $pilihan1,
            'pilihan2' => $pilihan2,
            'pilihan3' => $pilihan3,
            'pilihan4' => $pilihan4,
            'pilihan5' => $pilihan5,
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        $result = $this->surveyApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update_soal(Request $request)
    {
        // dd($request);
        if ($request['jenis'] == 'pilihan') {
            $pilihan1 = $request['pilihan1'];
            $pilihan2 = $request['pilihan2'];
            $pilihan3 = $request['pilihan3'];
            $pilihan4 = $request['pilihan4'];
            $pilihan5 = $request['pilihan5'];
        } else {
            $pilihan1 = null;
            $pilihan2 = null;
            $pilihan3 = null;
            $pilihan4 = null;
            $pilihan5 = null;
        }
        $data = array(
            'id_kategori' => $this->hash->decode($request['id_kategori'])[0],
            'id' => $request['id'],
            'pertanyaan' => $request['pertanyaan'],
            'jenis' => $request['jenis'],
            'pilihan1' => $pilihan1,
            'pilihan2' => $pilihan2,
            'pilihan3' => $pilihan3,
            'pilihan4' => $pilihan4,
            'pilihan5' => $pilihan5,
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        $result = $this->surveyApi->update(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function start_kategori($id)
    {
        $survey = $this->surveyApi->get_id_kategori($this->hash->decode($id)[0]);
        $data = $survey['body']['data'];
        // dd($data);
        $kategori = $this->katSurveyApi->get_by_id($this->hash->decode($id)[0]);
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        if (session('role') == 'admin-alumni') {
            return view('content.alumni.survey.admin.v_survey_by_kategori')->with(['template' => session('template'), 'data' => $data, 'id_kategori' => $id, 'kategori' => $kategori]);
        } else {
            return view('content.alumni.survey.v_survey')->with(['template' => session('template'), 'data' => $data, 'id_kategori' => $id]);
        }
    }

    public function statistik($id)
    {
        // dd($id);
        $survey = $this->jawabanApi->statistik_jawaban($this->hash->decode($id)[0]);
        // dd($survey);
        $data = $survey['body']['data'];
        $jawaban1 = 0;
        $jawaban2 = 0;
        $jawaban3 = 0;
        $jawaban4 = 0;
        $jawaban5 = 0;
        $ch= [];
        foreach ($data['pertanyaan'] as $pertanyaan) {
            $jawaban1 += $pertanyaan['jawaban1'];
            $jawaban2 += $pertanyaan['jawaban2'];
            $jawaban3 += $pertanyaan['jawaban3'];
            $jawaban4 += $pertanyaan['jawaban4'];
            $jawaban5 += $pertanyaan['jawaban5'];
        }
        $jawabanChart = new JawabanChart;
        $jawabanChart->labels(['Pilihan 1', 'Pilihan 2', 'Pilihan 3', 'Pilihan 4', 'Pilihan 5']);
        $jawabanChart->dataset('Jawaban Polling', 'bar', [$jawaban1, $jawaban2, $jawaban3, $jawaban4, $jawaban5])
        ->backgroundcolor('#03a9f3');
        return view('content.alumni.survey.admin.v_statistik_kategori')->with(['template' => session('template'), 'data' => $data, 'id_kategori' => $id, 'jawabanChart' => $jawabanChart]);
    }

    public function store(Request $request)
    {
        // dd($request);
        foreach ($request['data'] as $dta) {
            $id_kategori = '';
            if ($dta['name'] == 'id_kategori') {
                $id_kategori = $this->hash->decode($dta['value'])[0];
            }
        }
        foreach ($request['data'] as $dt) {
            if ($dt['name'] != "current" && $dt['name'] != "total" && $dt['name'] != "marked" && $dt['name'] != "id_kategori") {
                $data = array(
                    'id_survei' =>  (int)$dt['name'],
                    'jawaban' => $dt['value'],
                    'id_sekolah' => session('id_sekolah'),
                    'id_kategori' => $id_kategori,
                    'id_alumni' => session('id'),
                );
                $result = $this->jawabanApi->create(json_encode($data));
            }
        }

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete($id)
    {
        $delete = $this->surveyApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $message = array(
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            );
        } else {
            $message = array(
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            );
        }

        return redirect()->back()->with(['message' => $message]);
    }

    public function detail(Request $request)
    {
        $survey = $this->surveyApi->get_by_id($request['id']);
        // dd($survey);
        $survey = $survey['body']['data'];
        return response()->json($survey);
    }
}
