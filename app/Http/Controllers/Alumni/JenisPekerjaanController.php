<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\JenisPekerjaanApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class JenisPekerjaanController extends Controller
{
    private $jenisPekerjaanApi;

    public function __construct()
    {
        $this->jenisPekerjaanApi = new JenisPekerjaanApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Jenis pekerjaan');
        $routes = "alumni-jenis_pekerjaan";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $sort = (isset($_GET["sort"])) ? $_GET["sort"] : "ASC";
        $perPage = (isset($_GET["per_page"])) ? $_GET["per_page"] : "10";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->jenisPekerjaanApi->Pagination_sekolah($search, $status, $sort, $perPage, $page);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        $data = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pages = Utils::getPerPage();
        $sorts = Utils::getSort();
        $statuses = Utils::getStatus();
        $pagination = Utils::createLinksMetronic($meta, $routes, $search, $status, $sort);
        $template = session('template');
        return view('content.alumni.jenis_pekerjaan.v_jenis_pekerjaan', compact('template', 'data', 'pagination', 'pages', 'sorts', 'statuses', 'routes', 'search', 'status', 'sort', 'perPage', 'page'));




        session()->put('title', 'Jenis Pekerjaan');
        $jenis_pekerjaan = $this->jenisPekerjaanApi->sekolah(session('id_sekolah'));
        // dd($jenis_pekerjaan);
        if ($jenis_pekerjaan['code'] != 200) {
            $pesan = array(
                'message' => $jenis_pekerjaan['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jenis_pekerjaan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('tombol', function ($row) {
                return '<a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-sm btn-success data" data-id="' . $row['id'] . '"><i class="fa fa-bolt"></i> Pintasan Data</a>';
            });
            $table->rawColumns(['action', 'tombol']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.alumni.v_jenis_pekerjaan')->with([]);
    }

    public function store(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        foreach ($request['nama'] as $nama) {
            $data =array(
                    'nama' => $nama,
                    'id_fakultas' => $request['id_fakultas'],
                    'id_sekolah' => $id_sekolah,
                    'status' => 1

                );
            $jenis_pekerjaan = $this->jenisPekerjaanApi->create(json_encode($data));
        }
        // dd($jenis_pekerjaan);
        if ($jenis_pekerjaan['code'] == 200) {
            return response()->json([
                'success' => $jenis_pekerjaan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $jenis_pekerjaan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->jenisPekerjaanApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'id_fakultas' => $request->id_fakultas,
                'id_sekolah' => $id_sekolah,
                'status' => 1
            ];

        $jenis_pekerjaan = $this->jenisPekerjaanApi->update(json_encode($data));
        // dd($jenis_pekerjaan);
        if ($jenis_pekerjaan['code'] == 200) {
            return response()->json([
                'success' => $jenis_pekerjaan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $jenis_pekerjaan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->jenisPekerjaanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->jenisPekerjaanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jenis_pekerjaan = $this->jenisPekerjaanApi->all_trash();
        // dd($jenis_pekerjaan);
        $result = $jenis_pekerjaan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->jenisPekerjaanApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jenisPekerjaanApi->upload_excel(json_encode($data));
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update_status = $this->jenisPekerjaanApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
