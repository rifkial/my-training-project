<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AlumniApi;
use App\ApiService\Alumni\SosialApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SosialController extends Controller
{
    private $sosialApi;
    private $alumniApi;

    public function __construct()
    {
        $this->sosialApi = new SosialApi();
        $this->alumniApi = new AlumniApi();
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
                'media' => $request['media_sosmed'],
                'nama' => $request['nama'],
                'id_sekolah' => session('id_sekolah'),
            );
        $insert = $this->sosialApi->create(json_encode($data));
        if ($insert['code'] == 200) {
            $sosial = $this->data_sosmed();
            return response()->json([
                'message' => $insert['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'sosial' => $sosial
            ]);
        } else {
            return response()->json([
                'message' => $insert['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        if ($request->params == "auth") {
            $params = "auth";
            $id = null;
        } else {
            $params = "alumni";
            $id = $request['id'];
        }
        return response()->json($this->by_alumni($params, $id));
    }

    public function edit(Request $request)
    {
        $post  = $this->sosialApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'media' => $request['media_sosmed'],
            'nama' => $request['nama'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sosialApi->update(json_encode($data));
        if ($result['code'] == 200) {
            $sosial = $this->data_sosmed();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'sosial'  => $sosial,
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->sosialApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $sosial = $this->data_sosmed();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'sosial' => $sosial,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function by_alumni($params, $id)
    {
        if ($params == 'auth') {
            $alumni = $this->alumniApi->get_by_id(session('id'));
        } else {
            $alumni = $this->alumniApi->get_by_id($id);
        }
        
        $alumni = $alumni['body']['data'];
        $sosial = $alumni['sosial'];
        $html = '';
        if (!empty($sosial)) {
            $html .= '
            <table class="table table-bordered">
            <tr>
            <th>Media</th>
            <th>Akun</th>';
            if ($params == 'auth') {
                $html .= '<th>Aksi</th>';
            }
            
            $html .= '</tr>
            <tbody>
            ';
        
            foreach ($sosial as $ss) {
                $html.= '<tr>
                        <td>'.$ss['media'].'</td>
                        <td>'.$ss['nama'].'</td>';
                if ($params == 'auth') {
                    $html .= '<td>
                            <a href="javascript:void(0)" data-id="'.$ss['id'].'" class="btn btn-sm btn-info editSosial"><i class="fas fa-pencil-alt"></i></a>
                        </td>';
                }
                        
                $html .= '</tr>';
            }
            $html .= '</tbody>
            </table>';
        } else {
            if ($params == 'auth') {
                $html .= '
                <table class="table table-bordered">
                        <tr>
                            <td colspan="3">
                                <form id="formSossial" action="javascript:void(0)"
                                    onsubmit="addSosial(this)" name="formSosial" class="form-horizontal">
                                    <div class="form-group row">
                                        <input type="hidden" name="id_alumni" value="'.session('id').'">
                                        <input type="hidden" name="byDetail" value="true">
                                        <label class="col-md-2 col-form-label" for="l0"><i class="fab fa-facebook"></i> Facebook</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="facebook" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="l0"><i class="fab fa-twitter"></i> Twiter</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="twiter" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="l0"><i class="fab fa-instagram"></i> Instagram</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="instagram" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="l0"><i class="fab fa-linkedin"></i> linkedin</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="inkdln" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="l0"><i class="fas fa-globe"></i> Website</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="website" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l15"></label>
                                        <div class="col-md-9">
                                            <div class="button pull-right">
                                                <input type="hidden" name="action" id="action" value="Add" />
                                                <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                                    value="create">Simpan</button>
                                                <button class="btn btn-outline-danger"
                                                    id="btnCancel">Batalkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </table>
                ';
            } else {
                $html .= '<div style="text-align: center">Mohon maaf saat ini data belum tersedia</div>';
            }
        }
        return $html;
    }

    private function data_sosmed()
    {
        $sosial = $this->sosialApi->by_auth_alumni();
        $sosial = $sosial['body']['data'];
        $html = '';
        if (!empty($sosial)) {
            foreach ($sosial as $ss) {
                $check = '';
                if ($ss['status'] == 1) {
                    $check = 'checked';
                }
                $html .= '
                <tr>
                    <td>
                        <label class="switch">
                            <input type="checkbox" '.$check.'
                                class="sosmed_check" data-id="'.$ss['id'].'">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>'.$ss['media'].'</td>
                    <td><a href="'.$ss['nama'].'" class="text-info">'.Str::limit($ss['nama'], 50, '...').'</a></td>
                    
                    <td>
                        <a href="javascript:void(0)"
                            data-id="'.$ss['id'].'"
                            class="btn btn-info btn-sm editSosmed"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)" data-id="'.$ss['id'].'"
                            class="btn btn-danger btn-sm deleteSosmed"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>';
            }
        } else {
            $html .= ' <tr><td colspan="4" class="text-center">Data saat ini belum tersedia</td></tr>';
        }

        return $html;
    }
}
