<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\KategoriSurveyApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KategoriSurveyController extends Controller
{
    private $kategoriApi;

    public function __construct()
    {
        $this->kategoriApi = new KategoriSurveyApi();
    }


    public function store(Request $request)
    {
        $data = [];
        $keterangan = $request['keterangan'];
        $i = 0;
        foreach ($request['nama'] as $nama) {
            $data[] = array(
                    "nama" => $nama,
                    "keterangan" => $keterangan[$i],
                );
            $i++;
        }
        foreach ($data as $dt) {
            $data_insert = array(
                'nama' => $dt['nama'],
                'keterangan' => $dt['keterangan'],
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->kategoriApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $post  = $this->kategoriApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'keterangan' => $request['keterangan'][0],
                'id_sekolah' => $id_sekolah,
                'status' => 1
            ];

        $kategori = $this->kategoriApi->update(json_encode($data));
        if ($kategori['code'] == 200) {
            return response()->json([
                'message' => $kategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $kategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->kategoriApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
