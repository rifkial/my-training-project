<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\PendidikanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PendidikanController extends Controller
{
    private $pendidikanApi;

    public function __construct()
    {
        $this->pendidikanApi = new PendidikanApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data jurusan');
        $jurusan = $this->pendidikanApi->sekolah();
        if ($jurusan['code'] != 200) {
            $pesan = array(
                'message' => $jurusan['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jurusan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('tombol', function ($row) {
                return '<a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-sm btn-success data" data-id="' . $row['id'] . '"><i class="fa fa-bolt"></i> Pintasan Data</a>';
            });
            $table->rawColumns(['action', 'tombol']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.alumni.v_jurusan')->with(['fakultas' => $fakultas]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
                'nama' => $request['nama'],
                'jurusan' => $request['jurusan'],
                'jenjang' => $request['jenjang'],
                'th_masuk' => $request['tahun_masuk'],
                'th_lulus' => $request['tahun_keluar'],
                'alamat' => $request['alamat'],
                'id_alumni' => session('id'),
                'id_sekolah' => session('id_sekolah'),
                'status' => $request['status'],
            );
        $result = $this->pendidikanApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $pendidikan = $this->data_pendidikan();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'pendidikan' => $pendidikan
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->pendidikanApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'jurusan' => $request['jurusan'],
            'jenjang' => $request['jenjang'],
            'th_masuk' => $request['tahun_masuk'],
            'th_lulus' => $request['tahun_keluar'],
            'alamat' => $request['alamat'],
            'id_alumni' => session('id'),
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status'],
        );
        $pendidikan = $this->pendidikanApi->update(json_encode($data));
        if ($pendidikan['code'] == 200) {
            $pendidikan = $this->data_pendidikan();
            return response()->json([
                'message' => $pendidikan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'pendidikan'  => $pendidikan,
            ]);
        } else {
            return response()->json([
                'message' => $pendidikan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->pendidikanApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $pendidikan = $this->data_pendidikan();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'pendidikan' => $pendidikan,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->pendidikanApi->delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->pendidikanApi->all_trash();
        // dd($jurusan);
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore(Request $request)
    {
        $restore = $this->pendidikanApi->restore($request['id']);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_data(Request $request){
        if ($request->params == "auth") {
            $params = "auth";
            $id = null;
        } else {
            $params = "alumni";
            $id = $request['id'];
        }
        return response()->json($this->by_alumni($params, $id));
    }

    private function data_pendidikan()
    {
        $pendidikan = $this->pendidikanApi->by_auth_alumni();
        $pendidikan = $pendidikan['body']['data'];
        $html = '';
        if (!empty($pendidikan)) {
            foreach($pendidikan as $pd){
                $checked = '';
                if ($pd['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td>
                    <label class="switch">
                        <input type="checkbox" '.$checked.'
                            class="pendidikan_check" data-id="'.$pd['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td>'.Str::upper($pd['jenjang']).'</td>
                <td>'.$pd['nama'].'</td>
                <td>'.$pd['jurusan'].'</td>
                <td>Angkatan '.$pd['th_masuk']." - ".$pd['th_lulus'].'</td>
                <td>'.$pd['alamat'].'</td>
                <td>
                    <a href="javascript:void(0)" data-id="'.$pd['id'].'" class="btn btn-info btn-sm editPendidikan"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="'.$pd['id'].'" class="btn btn-danger btn-sm deletePendidikan"><i class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="7" class="text-center">Data saat ini belum tersedia</td></tr>';
        }

        return $html;
        
    }

    public function by_alumni($params, $id)
    {
        if($params == 'auth'){
            $pendidikan = $this->pendidikanApi->by_auth_alumni();
        }else{
            $pendidikan = $this->pendidikanApi->by_id_alumni($id);
        }
        $pendidikan = $pendidikan['body']['data'];
        $html = '';
        if (!empty($pendidikan)) {
            $html .= '
            <table style="width: 100%">';
            if ($params == 'auth') {
                $html .= '<tr>
                    <td style="width: 20%">
                        <a href="javascript:void(0)" onclick="btnAddP()" class="btn btn-outline-info mb-3">
                            <i class="fa fa-plus-circle"></i> Tambah Pendidikan
                        </a>
                    </td>
                    <td></td>
                    <td></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>';
            }
            
            $html .= '<tbody>';
            foreach ($pendidikan as $pd) {
                $html .= '<tr class="mt-2" style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                <td style="vertical-align: middle" class="pr-2 pl-2">
                    '.Help::getMonthYear($pd['th_masuk']).'
                </td>
                <td style="vertical-align: middle">
                    <h5 class="mt-0">
                        <b>'.strtoupper($pd['nama']).'</b>
                    </h5>
                    <h5 class="mt-0">'.strtoupper($pd['jenjang']).'
                    </h5>
                    <table style="width: 100%">
                        <tr>
                            <td>jurusan : '.ucwords($pd['jurusan']).'</td>
                        </tr>
                    </table>
                <td>';
                if ($params == 'auth') {
                    $html .= '<td style="text-align: right; vertical-align: middle" class="pr-2 pl-2">
                    <ul class="social-links list-inline mb-0 mt-2">
                        <li class="list-inline-item">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                class="btn btn-info btn-sm editPendidikan" data-original-title="Edit Pendidikan"
                                data-id="'.$pd['id'].'">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                class="btn btn-danger btn-sm deletePendidikan"
                                data-original-title="Hapus Pendidikan" data-id="'.$pd['id'].'">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </li>
                    </ul>
                </td>';
                }
                
            $html .= '</tr>
            <tr>
            <td>&nbsp;</td>
            </tr>';
            }
            $html .= '</tbody>
            </table>';
        } else {
            $firstYear = (int) date('Y');
            $lastYear = $firstYear - 20;
            $tahun = '';
            for ($year = $firstYear; $year >= $lastYear; $year--) {
                $tahun .= '<option value=' . $year . '>' . $year . '</option>';
            }
            if ($params == 'auth') {
                $html .= '
                <table style="width: 100%">
                    <tr>
                        <td colspan="3">
                            <form id="formPendidikan" action="javascript:void(0)" onsubmit="addPendidikan(this)" name="formPendidikan" class="form-horizontal">
                                <div class="form-group row">
                                    <input type="hidden" name="id_alumni" value="'.session('id').'">
                                    <input type="hidden" name="byDetail" value="true">
                                    <label class="col-md-3 col-form-label" for="l0">Institusi/
                                        Sekolah</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="nama" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Jurusan</label>
                                    <div class="col-md-9">
                                        <input type="text" name="jurusan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Jenjang</label>
                                    <div class="col-md-9">
                                        <select name="jenjang" class="form-control" id="jenjang">
                                            <option value="s1">S1</option>
                                            <option value="d4">D4</option>
                                            <option value="d3">D3</option>
                                            <option value="sma">SMA Sederajat</option>
                                            <option value="smp">SMP Sederajat</option>
                                            <option value="sd">SD Sederajat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Angkatan</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="tahun_masuk" id="tahun_masuk" class="form-control select3">
                                                    <option value="">Tahun Masuk</option>
                                                   '.$tahun.'
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="tahun_keluar" class="form-control select3" id="tahun_keluar">
                                                    <option value="">Tahun Lulus</option>
                                                    '.$tahun.'
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15"></label>
                                    <div class="col-md-9">
                                        <div class="button pull-right">
                                            <input type="hidden" name="action" id="action" value="Add" />
                                            <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                                value="create">Simpan</button>
                                            <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                </table>
                ';
            } else {
                $html .= '<div style="text-align: center">Saat ini data belum tersedia</div>';
            }
            
           
        }
        return $html;
    }
}
