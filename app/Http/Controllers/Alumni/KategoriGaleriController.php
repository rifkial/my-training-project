<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\KatGaleriApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KategoriGaleriController extends Controller
{
    private $katGaleriApi;

    public function __construct()
    {
        $this->katGaleriApi = new KatGaleriApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Kategori Galeri');
        $routes = "alumni-kategori_galeri";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $sort = (isset($_GET["sort"])) ? $_GET["sort"] : "ASC";
        $perPage = (isset($_GET["per_page"])) ? $_GET["per_page"] : "10";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->katGaleriApi->sekolah($search, $status, $sort, $perPage, $page);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error',
                'status' => 'gagal',
            );
            return redirect()->back()->with('message', $pesan);
        }
        $data = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pages = Utils::getPerPage();
        $sorts = Utils::getSort();
        $statuses = Utils::getStatus();
        $pagination = Utils::createLinksMetronic($meta, $routes, $search, $status, $sort);
        $template = session('template');
        return view('content.alumni.kategori_galeri.v_kategori_galeri', compact('template', 'data', 'pagination', 'pages', 'sorts', 'statuses', 'routes', 'search', 'status', 'sort', 'perPage', 'page'));
    }

    public function store(Request $request)
    {
        foreach ($request['nama'] as $nama) {
            $data =array(
                    'nama' => $nama,
                    'id_sekolah' => session('id_sekolah'),
                    'status' => $request['status']
                );
            $result = $this->katGaleriApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->katGaleriApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status']
        );
        $katGaleri = $this->katGaleriApi->update(json_encode($data));
        if ($katGaleri['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $katGaleri['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $katGaleri['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->katGaleriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    
    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update_status = $this->katGaleriApi->update_status(json_encode($data));
        if ($update_status['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update_status['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update_status['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_kategori()
    {
        $kategori = $this->katGaleriApi->id_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        $html = '';
        if (!empty($kategori)) {
            $no = 1;
            foreach ($kategori as $kt) {
                $checked = '';
                if ($kt['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$kt['nama'].'</td>
                    <td>
                        <label class="switch">
                            <input type="checkbox" '.$checked.' class="kategori_check" data-id="'.$kt['id'].'">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                        <a href="javascript:void(0)" data-toggle="collapse"
                            data-target="#galeri'.$kt['id'].'"
                            class="btn btn-sm btn-purple"><i class="fas fa-image"></i></a>
                        <a href="javascript:void(0)" data-id="'.$kt['id'].'"
                            class="btn btn-sm btn-info edit"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)" data-id="'.$kt['id'].'"
                            class="btn btn-sm btn-danger delete"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="hiddenRow">
                        <div class="accordian-body collapse" id="galeri'.$kt['id'].'">
                            <button class="btn btn-purple btn-sm my-3 pull-right"
                                onclick="addGaleri('.$kt['id'].')"><i
                                    class="fas fa-plus-circle"></i>
                                Tambah Galeri</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-purple text-white">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Gambar</th>
                                        <th class="text-center">Deskripsi</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Published</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="dataGaleri'.$kt['id'].'">';
                if (!empty($kt['galeri'])) {
                    $nomer = 1;
                    foreach ($kt['galeri'] as $glr) {
                        $glr_check = '';
                        if ($glr['status'] == 1) {
                            $glr_check = 'checked';
                        }
                        $html .= '<tr>
                            <td>'.$nomer++.'</td>
                            <td><img src="'.$glr['file'].'" alt="" height="50">
                            </td>
                            <td>
                                <b
                                    class="text-dark">'.$glr['nama'].'</b>
                                <span class="text-dark">by '.$glr['pembuat'].'</span>
                                <br>
                                <small class="m-0 text-dark"> '.Str::limit($glr['keterangan'], 100, '...').'</small>
                            </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" '.$glr_check.' class="galeri_check" data-id="'.$glr['id'].'">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                '.Help::getTanggalLengkap($glr['created_at']).'
                                <br>
                                <small>'.$glr['dibuat'].'</small>
                            </td>
                            <td>
                                <a href="javascript:void(0)"
                                    data-id="'.$glr['id'].'"
                                    class="editGaleri btn btn-info btn-sm"><i
                                        class="fas fa-pencil-alt"></i></a>
                                <a href="javascript:void(0)"
                                    data-id="'.$glr['id'].'"
                                    data-kategori="'.$kt['id'].'"
                                    class="btn btn-danger btn-sm deleteGaleri"><i
                                        class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>';
                    }
                } else {
                    $html .= '<tr><td colspan="6" class="text-center">Data saat ini tidak tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }
}
