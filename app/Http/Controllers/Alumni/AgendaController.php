<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AgendaApi;
use App\ApiService\Alumni\AlumniApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AgendaController extends Controller
{
    private $agendaApi;
    private $alumniApi;

    public function __construct()
    {
        $this->agendaApi = new AgendaApi();
        $this->alumniApi = new AlumniApi();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $agenda = $this->agendaApi->id_sekolah(session('id_sekolah'));
            // dd($agenda);
            $agenda = $agenda['body']['data'];
            $agendas = [];
            foreach ($agenda as $ag) {
               $agendas[] = array(
                   'id' => $ag['id'],
                   'tgl_mulai' => $ag['tgl_mulai'],
                   'day_only' => Help::getDayOnly($ag['tgl_mulai']),
                   'month_only' => Help::getMonthOnly($ag['tgl_mulai']),
                   'tgl_mulai_indonesia' => Help::getDayMonth($ag['tgl_mulai']),
                   'tgl_selesai_indonesia' => Help::getDayMonth($ag['tgl_selesai']),
                   'tgl_selesai' => $ag['tgl_selesai'],
                   'judul' => $ag['judul'],
                   'keterangan' => $ag['keterangan'],
                   'waktu' => $ag['waktu'],
                   'dibuat' => $ag['dibuat'],
                   'tempat' => $ag['tempat'],
                   'file' => $ag['file'],
                   'avatar' => $ag['avatar'],
                   'pembuat' => $ag['pembuat'],
               );
            }
            return response()->json($agendas);
        }
        $count_agenda = count($this->agendaApi->by_status(json_encode(["status" => 2]))['body']['data']);
        if (session('role') == 'admin-alumni') {
            return view('content.alumni.agenda.v_admin_agenda')->with(['template' => session('template'), 'approve' => $count_agenda]);
        } else {
            return view('content.alumni.agenda.v_agenda')->with(['template' => session('template')]);
        }
    }

    public function pending_agenda()
    {
        $agenda = $this->agendaApi->by_status(json_encode(["status" => 2]));
        // dd($agenda);
        $agenda = $agenda['body']['data'];
        return view('content.alumni.agenda.v_pending_agenda')->with(['template' => session('template'), 'agenda' => $agenda]);
    }

    public function update_status($id){
        $params = (isset($_GET["params"])) ? $_GET["params"] : "";
        if($params == 'aktif'){
            $status = 1;
        }else{
            $status = 0;
        }
        $data = array(
            'id' => $id,
            'status' => $status
        );
        $update =  $this->agendaApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            $message = array(
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            );
        } else {
            $message = array(
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            );
        }
        return redirect()->back()->with(['message' => $message]);
    }


    public function get_detail()
    {
        $alumni = $this->alumniApi->get_by_id(session('id'));
        $alumni = $alumni['body']['data'];
        $html = '
        <div id="calendar"></div>
        ';
        return response()->json($html);
    }

    public function get_agenda(Request $request)
    {
        if ($request['params'] == 'auth') {
            $alumni = $this->alumniApi->get_by_id(session('id'));
        } else {
            $alumni = $this->alumniApi->get_by_id($request['id']);
        }
        
        $alumni = $alumni['body']['data'];
        $agenda = $alumni['agenda'];
        return response()->json($agenda);
    }

    public function store(Request $request)
    {
        $data = array(
            'judul' => $request->judul_agenda,
            'keterangan' => $request->keterangan_agenda,
            'tempat' => $request->tempat_agenda,
            'tgl_mulai' => $request->start,
            'tgl_selesai' => $request->end,
            'waktu' => $request->waktu_agenda,
            'id_sekolah' => session('id_sekolah'),
            'status' => 0
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->agendaApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->agendaApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        // dd(date('Y-m-d', strtotime($request->start)) ." ". $request->waktu_agenda);
        $data = array(
            'id' => $request->id,
            'judul' => $request->judul_agenda,
            'keterangan' => $request->keterangan_agenda,
            'tempat' => $request->tempat_agenda,
            'tgl_mulai' => date('Y-m-d', strtotime($request->start)) ." ". $request->waktu_agenda,
            'tgl_selesai' => $request->end,
            'waktu' => $request->waktu_agenda,
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->agendaApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->agendaApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->agendaApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->agendaApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_id(Request $request)
    {
        // dd($request);
        $agenda = $this->agendaApi->get_by_id($request['id']);
        $agenda = $agenda['body']['data'];
        return response()->json($agenda);
    }

    public function soft_del($id)
    {
        $delete = $this->agendaApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $message = array(
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            );
        } else {
            $message = array(
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            );
        }
        return redirect()->back()->with(['message' => $message]);
    }
}
