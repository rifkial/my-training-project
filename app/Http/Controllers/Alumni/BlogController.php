<?php

namespace App\Http\Controllers\Alumni;

use App\ApiService\Alumni\AlumniApi;
use App\ApiService\Alumni\BlogApi;
use App\ApiService\Alumni\KategoriBlogApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;
use Illuminate\Support\Str;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class BlogController extends Controller
{
    private $blogApi;
    private $alumniApi;
    private $kategoriApi;
    private $profileApi;
    private $hash;
    public $perPage = 2;

    public function __construct()
    {
        $this->alumniApi = new AlumniApi();
        $this->blogApi = new BlogApi();
        $this->kategoriApi = new KategoriBlogApi();
        $this->profileApi = new ProfileApi();
        $this->hash = new Hashids();
    }

    public function index()
    {
        $routes = "alumni-blog";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->blogApi->id_sekolah(session('id_sekolah'), $page, $search);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        // dd($result);
        $blog = $result['body']['data'];
        // dd($blog);
        $meta = $result['body']['meta'];
        session()->put('title', 'Article');
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $before = $this->blogApi->get_status(json_encode(["status" => 0]))['body']['data'];
        $kategori = $this->kategoriApi->id_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        return view('content.alumni.blog.v_blog')->with(['template' => session('template'), 'blog' => $blog, 'approve' => count($before), 'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'kategori' => $kategori]);
    }

    public function blog_aprrove()
    {
        $routes = "alumni-blog_approve";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->blogApi->get_status_param(2,$page, $search);
        // dd($result);
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $blog = $result['body']['data'];
        $meta = $result['body']['meta'];
        session()->put('title', 'Article');
        $pagination = Utils::filterSimple($meta, $routes, $search);
        return view('content.alumni.blog.v_blog_approve')->with(['template' => session('template'), 'blog' => $blog, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search]);
    }
   
    public function create()
    {
        $kategori = $this->kategoriApi->id_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        return view('content.alumni.blog.v_create_blog')->with(['template' => session('template'), 'kategori' => $kategori]);
    }

    public function paginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $this->perPage), $items->count(), $this->perPage, $page, $options);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kategori' => Help::decode($request['id_kategori']),
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'status' => 2,
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->blogApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->blogApi->create(json_encode($data));
        }
        // dd($result);
        if (session('role') == 'admin-alumni') {
            if ($result['code'] == 200) {
                $message = array(
                    'message' => $result['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil',
                );
                return redirect()->route('alumni-blog')->with(['message' => $message]);
            } else {
                $message = array(
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal',
                );
    
                return redirect()->back()->with(['message' => $message]);
            }
        } else {
            if ($result['code'] == 200) {
                $blog = $this->data_blog();
                return response()->json([
                        'message' => $result['body']['message'],
                        'icon'  => 'success',
                        'status' => 'berhasil',
                        'blog' => $blog
                    ]);
            } else {
                return response()->json([
                        'message' => $result['body']['message'],
                        'icon'  => 'error',
                        'status' => 'gagal',
                    ]);
            }
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_kategori' => Help::decode($request['id_kategori']),
            'judul' => $request['judul'],
            'isi' => $request['isi'],
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->blogApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->blogApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->blogApi->update(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            $blog = $this->data_blog();
            return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'success',
                    'status' => 'berhasil',
                    'blog' => $blog
                ]);
        } else {
            return response()->json([
                    'message' => $result['body']['message'],
                    'icon'  => 'error',
                    'status' => 'gagal',
                ]);
        }
        // $html = $this->detail_blog('auth', null);
        // if ($result['code'] == 200) {
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'success',
        //         'status' => 'berhasil',
        //         'html' => $html,
        //     ]);
        // } else {
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'error',
        //         'status' => 'gagal',
        //         'html' => $html,
        //     ]);
        // }
    }

    public function detail()
    {
        $blog = $this->blogApi->detail(Help::decode($_GET['id']));
        // dd($blog);
        $blog = $blog['body']['data'];
        $last_file = explode('/', $blog['file']);
        $blog['file_edit'] = last($last_file);
        // dd($blog);
        $my_blog = $this->blogApi->get_sosial($blog['sosial']);
        // dd($my_blog);
        $my_blog = $my_blog['body']['data'];
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $profile = $profile['body']['data'];

        return view('content.alumni.blog.v_detail_blog')->with(['template' => session('template'), 'blog' => $blog, 'myBlog' => $my_blog, 'profile' => $profile]);
    }

    public function edit(Request $request)
    {
        $blog = $this->blogApi->get_by_id($request['id']);
        $blog = $blog['body']['data'];
        $ex = explode('/', $blog['file']);
        $blog['file_edit'] = end($ex);
        $blog['id_kategori'] = Help::encode($blog['id_kategori']);
        // dd($blog);
        return response()->json($blog);
    }

    public function blog(Request $request)
    {
        if ($request->params == "auth") {
            $params = "auth";
            $id = null;
        } else {
            $params = "alumni";
            $id = $request['id'];
        }

        return response()->json($this->detail_blog($params, $id));
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->blogApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $blog = $this->data_blog();
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'blog' => $blog
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_del($id)
    {
        $delete = $this->blogApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $message = array(
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
               
            );
        } else {
            $message = array(
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }
        return redirect()->back()->with(['message' => $message]);
    }

    public function detail_blog($params, $id)
    {
        if ($params == "auth") {
            $alumni = $this->alumniApi->get_by_id(session('id'));
        } else {
            $alumni = $this->alumniApi->get_by_id($id);
        }
        $alumni = $alumni['body']['data'];
        // dd($alumni);
        $kategori = $this->kategoriApi->id_sekolah(session('id_sekolah'));
        $kategori = $kategori['body']['data'];
        $html = '
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 mb-3">';
        if ($params == 'auth' || session('role') == 'admin-alumni') {
            $html .= '<button class="btn btn-success" onclick="btnAddBlog()">Tambah Data</button>';
        }

        $html .= '</div>
        <div class="col-md-5">
            <form action="javascript:void(0)" id="search_blog" name="search_blog" onsubmit="pencarianBlog(this)">
                <div class="input-group">
                    <input type="hidden" name="search_param" value="all" id="search_param">
                    <input type="text" class="form-control" name="nama_pencarian" placeholder="Search term...">
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="submit" id="btn-search"><span class="fas fa-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>
        ';
        if (!empty($alumni['blog'])) {
            $first_blog = $alumni['blog'][0];
            $first_blog_array = $alumni['blog'];
            unset($first_blog_array[0]);
            if (!empty($first_blog_array)) {
                $html .= '
                <div id="hasil_pencarian_blog" class="row" style="width: 100%">
                <div class="col-lg-7 mb-4">
                        <div id="avatar" style="height: 243px; width: 100%; position: relative">
                            <img class="img-fluid rounded mb-3" src="'.$first_blog['file'].'"
                                alt="A guide to building your online presence"
                                style="position: absolute;  max-height: -webkit-fill-available; margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center;">
                                <button type="button" class="btn btn-danger btn-circle btn-lg pull-right deleteBlog" data-id="'.$first_blog['id'].'"><i class="fas fa-trash"></i></button>
                                <button type="button" class="btn btn-info btn-circle btn-lg pull-right mr-1 editBlog" data-id="'.$first_blog['id'].'"><i class="fas fa-edit"></i></button>
                        </div>
                        <a href="'.route('alumni-blog_detail', $this->hash->encode($first_blog['id'])).'" class="mt-4 h2 text-dark">'.$first_blog['judul'].'</a>
                        <p class="mt-4">'.$first_blog['isi'] .'.</p>

                        <div class="d-flex text-small">
                            <a href="#">'.$first_blog['kategori'] .'</a>
                            <span class="text-muted ml-1">'.Help::getDayMonth($first_blog['created_at']).'</span>
                        </div>
                    </div>
                    <div class="col-lg-5 ">
                        <ul class="list-unstyled">';
                foreach ($first_blog_array as $bl) {
                    $html .= '
                            <li class="row mb-4" style="display: flex; align-items: center; justify-content: center;">
                                    <a href="#" class="col-3">
                                        <img src="'.$bl['file'].'" alt="Image" class="rounded img-fluid"
                                            style="width: 100%">
                                    </a>
                                    <div class="col-9" style="position: relative">
                                        <div style="position: absolute; right: 0">
                                            <button type="button" class="btn btn-danger btn-circle btn-md pull-right deleteBlog" data-id="'.$bl['id'].'"><i class="fas fa-trash"></i></button>
                                            <button type="button" class="btn btn-info btn-circle btn-md pull-right mr-1 editBlog" data-id="'.$bl['id'].'"><i class="fas fa-edit"></i></button>
                                        </div>
                                        <a href="'.route('alumni-blog_detail', $this->hash->encode($bl['id'])).'">
                                            <h6 class="mb-3 h5 text-dark" style="margin-right: 50px">'.$bl['judul'] .'</h6>
                                        </a>
                                        <div class="d-flex text-small">
                                            <a href="'.route('alumni-blog_detail', $this->hash->encode($bl['id'])).'">'.$bl['kategori'].'</a>
                                            <span
                                                class="text-muted ml-1">'.Help::getDayMonth($first_blog['created_at']).'</span>
                                        </div>
                                    </div>
                                </li>
                            ';
                }
                $html .= '</ul>
                        </div></div>';
            } else {
                $html .= '<div id="hasil_pencarian_blog" class="row" style="width: 100%">
                <div class="col-lg-12 mb-4">
                    <div id="avatar" style="height: 243px; width: 100%; position: relative">
                        <img class="img-fluid rounded mb-3" src="'.$first_blog['file'].'"
                            alt="A guide to building your online presence"
                            style="position: absolute;  max-height: -webkit-fill-available; margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center;">
                            <button type="button" class="btn btn-danger btn-circle btn-lg pull-right deleteBlog" data-id="'.$first_blog['id'].'"><i class="fas fa-trash"></i></button>
                                    <button type="button" class="btn btn-info btn-circle btn-lg pull-right mr-1 editBlog" data-id="'.$first_blog['id'].'"><i class="fas fa-edit"></i></button>
                    </div>
                    <a href="'.route('alumni-blog_detail', $this->hash->encode($first_blog['id'])).'" class="mt-4 h2 text-dark">'.$first_blog['judul'].'</a>
                    <p class="mt-4">'.$first_blog['isi'] .'.</p>

                    <div class="d-flex text-small">
                        <a href="'.route('alumni-blog_detail', $this->hash->encode($first_blog['id'])).'">'.$first_blog['kategori'].'</a>
                        <span class="text-muted ml-1">'.Help::getDayMonth($first_blog['created_at']).'</span>
                    </div>
                </div>
            </div>';
            }
        } else {
            if (session('id') == $alumni['id'] || session('role') == 'admin-alumni') {
                $html .= '
               <div class="col-md-12">
                        <form id="formAddBlog" name="formAddBlog" onsubmit="formTambahBlog(this)" action="javascript:void(0)" class="form-horizontal">
                            <div class="form-group row">
                                <input type="hidden" name="byDetail" value="true">
                                <label class="col-md-3 col-form-label" for="l0">Kategori Blog</label>
                                <div class="col-md-9">
                                    <select name="id_kategori" class="form-control">
                                        <option value="">-- Pilih Kategori --</option>';
                foreach ($kategori as $kt) {
                    $html .= '<option value="'.$kt['id'].'">'.$kt['nama'].'</option>';
                }
                $html .= '
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l10">Judul</label>
                                        <div class="col-md-9">
                                            <input type="text" name="judul" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l10">Isi</label>
                                        <div class="col-md-9">
                                            <textarea name="isi" class="form-control isi_blog" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <label class="col-md-3 col-form-label" for="l1"></label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                        class="form-group mb-1" width="100%" style="margin-top: 10px">
                                                </div>
                                                <div class="col-md-6" style="position: relative">
                                                    <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                                <input type="hidden" name="hidden_image" id="hidden_image">
                                            </div>
                                            <input type="hidden" name="old_image" id="old_image">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l15"></label>
                                        <div class="col-md-9">
                                            <div class="button pull-right">
                                                <input type="hidden" name="action" id="actionBlog" value="Add" />
                                                <button type="submit" class="btn btn-outline-info" id="addBlog"
                                                    value="create">Simpan</button>
                                                <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>';
            } else {
                $html .= '<div class="col-md-12">
               <p style="text-align: center">
                   Maaf, user belum punya blog yang dipost
               </p>
           </div>';
            }
        }
        $html .= '</div></div></section>';
        return $html;
    }

    public function search_blog(Request $request)
    {
        // dd($request);
        $data_search = array(
            'judul' => $request['nama_pencarian']
        );
        $pencarian = $this->blogApi->search_name(json_encode($data_search));
        $pencarian = $pencarian['body']['data'];
        $html = '';
        foreach ($pencarian as $pn) {
            $html .='
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="well blog">
                    <a href="'.route('alumni-blog_detail', $this->hash->encode($pn['id'])).'">
                        <div class="date primary">
                            <span class="blog-date">'.Help::getDayMonth($pn['created_at']).'</span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <div class="image">
                                    <img src="'.$pn['file'].'" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="blog-details">
                                    <h2>'.$pn['judul'].'</h2>
                                    <p>
                                       '.$pn['isi'].'
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            ';
        }

        return response()->json($html);
    }

    public function search(Request $request)
    {
        $data = array(
            'judul' => $request->keyword,
            'publik' => true
        );
        $blog = $this->blogApi->search_name(json_encode($data));
        // dd($blog);
        $meta = $blog['body']['meta'];
        $blog = $blog['body']['data'];
        $before = $this->blogApi->get_status(json_encode(["status" => 0]))['body']['data'];
        return view('content.alumni.blog.v_blog')->with(['template' => session('template'), 'blog' => $blog, 'approve' => count($before)]);
    }

    public function update_status($id)
    {
        // dd($id);
        $params = (isset($_GET["params"])) ? $_GET["params"] : "";
        if ($params == 'aktif') {
            $status = 1;
        } else {
            $status = 0;
        }
        $data = array(
            'id' => $id,
            'status' => $status
        );
        $update =  $this->blogApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            $message = array(
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            );
        } else {
            $message = array(
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            );
        }
        return redirect()->back()->with(['message' => $message]);
    }

    public function change_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['status']
        );
        $update =  $this->blogApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    private function data_blog()
    {
        $blog = $this->blogApi->my_blog();
        $blog = $blog['body']['data'];
        $html = '';
        if (!empty($blog)) {
            foreach ($blog as $bl) {
                $checked = '';
                if ($bl['status'] == 1) {
                    $checked = 'checked';
                }
                $see = 0;
                if ($bl['dilihat'] != null) {
                    $see = $bl['dilihat'];
                }
                $html .= '
                <tr>
                    <td class="vertical-middle">
                        <label class="switch">
                            <input type="checkbox" '.$checked.'
                                class="artikel_check"
                                data-id="'.$bl['id'].'">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td class="vertical-middle">
                        <a
                            href="'.route("alumni-blog_detail", ["id" => Help::encode($bl['id']), 'title' => Str::slug($bl['judul'], '-')]).'>
                            <b class="text-dark">'.strip_tags(Str::limit($bl['judul'], 50, '...')).'</b>
                            <span class="text-dark">by '.$bl['pembuat'].'</span>
                            <p class="m-0"><small
                                    class="text-dark">'.$see.'x
                                    Dilihat</small></p>
                            <p class="m-0 text-info">
                                '.request()->getSchemeAndHttpHost().'/program/alumni/blog/detail/'.Help::encode($bl['id']).'/'.Str::slug($bl['judul'], '-').'
                            </p>
                        </a>
                    </td>
                    <td class="vertical-middle">'.$bl['kategori'].'</td>
                    <td class="vertical-middle">
                        '.Help::getTanggalLengkap($bl['created_at']).'
                    </td>
                    <td class="vertical-middle">
                        <a href="javascript:void(0)"
                            data-id="'.$bl['id'].'"
                            class="btn btn-info btn-sm editArtikel"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)"
                            data-id="'.$bl['id'].'"
                            class="btn btn-danger btn-sm deleteArtikel"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= ' <tr><td colspan="5" class="text-center">Data saat ini belum tersedia</td></tr>';
        }
        return $html;
    }
}
