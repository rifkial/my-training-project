<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\PembimbingSertifikatApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class PembimbingIndustriController extends Controller
{
    private $pembimbingApi;
    private $industriApi;
    private $sertifikatApi;
    private $profileApi;

    public function __construct()
    {
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->industriApi = new IndustriApi();
        $this->profileApi = new ProfileApi();
        $this->sertifikatApi = new PembimbingSertifikatApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Pembimbing Industri');
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        $industri = $industri['body']['data'];
        $pembimbing = $this->pembimbingApi->sekolah(session('id_tahun_ajar'));
        // dd($pembimbing);
        $pembimbing = $pembimbing['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_pembimbing_industri')->with(['template' => $template, 'industri' => $industri, 'pembimbing' => $pembimbing]);
    }

    public function edit(Request $request)
    {
        $pembimbing = $this->pembimbingApi->get_by_id($request['id']);
        $pembimbing = $pembimbing['body']['data'];
        return response()->json($pembimbing);
    }
    
    public function by_industri(Request $request)
    {
        $pembimbing = $this->pembimbingApi->by_industri($request['id']);
        $pembimbing = $pembimbing['body']['data'];
        
        return response()->json($pembimbing);
    }

    public function detail()
    {
        // dd('tes');
        $id = Help::decode($_GET['k']);
        // dd($id);
        $pembimbing = $this->pembimbingApi->get_by_id($id);
        // dd($pembimbing);
        $pembimbing = $pembimbing['body']['data'];
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        $industri = $industri['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_edit_pembimbing_industri')->with(['template' => $template, 'industri' => $industri, 'pembimbing' => $pembimbing]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id_industri' => $request->id_industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pembimbingApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $pembimbing = $this->data_pembimbing();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'id_industri' => $request->industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pembimbingApi->update_info(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->pembimbingApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $pembimbing = $this->data_pembimbing();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'pembimbing' => $pembimbing,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function set_default(Request $request)
    {
        $data = array(
            'id_pemb_industri' => $request['id'],
            'id_template' => 1,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->sertifikatApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $template = $this->data_sertifikat($request['id']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $template
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    private function data_sertifikat($id_pembimbing)
    {
        $sertifikat = $this->sertifikatApi->get_by_pemb_industri($id_pembimbing);
        $sertifikat = $sertifikat['body']['data'];
        $html = '';
        $head = '';
        if (empty($sertifikat)) {
            $html .= '
            <tr>
                <td colspan="2" class="text-center">
                    Template saat ini tidak tersedia. <br>
                    <button data-id="'.$id_pembimbing.'" class="btn btn-success btn-sm set_default"><i
                            class="fas fa-wrench"></i> Set
                        Default</button>
                    <small class="text-danger">*Untuk
                        mengeset sertifikat sekarang</small>
                </td>
            </tr>
            ';
            $head = '<tr class="bg-danger">
                        <th class="text-center">Template</th>
                        <th class="text-center">Opsi</th>
                    </tr>';
        } else {
            $html .= '
                <tr>
                    <td class="text-center">'.$sertifikat['template'].'</td>
                    <td class="text-center">
                        <a href="'.route('prakerin_template-detail', $sertifikat['id_template']).'"
                        target="_blank"
                            class="btn btn-purple btn-sm"><i class="fas fa-info-circle"></i></a>
                    </td>
                </tr>
                ';
            $head = '<tr class="bg-info">
                        <th class="text-center">Template</th>
                        <th class="text-center">Opsi</th>
                    </tr>';
        }
        $data = array(
            'template' => $html,
            'head' => $head
        );
        return $data;
    }

    private function data_pembimbing()
    {
        $pembimbing = $this->pembimbingApi->sekolah(session('id_tahun_ajar'));
        $pembimbing = $pembimbing['body']['data'];
        $html = '';
        if (empty($pembimbing)) {
            $html .= ' <tr><td colspan="7" class="text-center">Data saat ini belum tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($pembimbing as $pb) {
                $html .= '
                <tr>
                                                        <td>'.$no++.'</td>
                                                        <td>'.$pb['nama'].'</td>
                                                        <td>'.$pb['industri'].'</td>
                                                        <td>'.$pb['telepon'].'</td>
                                                        <td>
                                                            <a href="javascript:void(0)" data-toggle="collapse"
                                                                data-target="#demo'.$pb['id'].'"
                                                                class="btn btn-info btn-sm accordion-toggle mx-1"><i
                                                                    class="fas fa-award"></i></a>';
                if (session('role') == 'admin-prakerin') {
                    $html .= '<a href="'.route('pkl_pembimbing_industri-detail_edit', ['k' => Help::encode($pb['id']), 'name' => str_slug($pb['nama'])]).'"
                                                                        class="btn btn-purple btn-sm"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)" data-id="'.$pb['id'].'"
                                                                        class=" delete btn btn-danger btn-sm"><i
                                                                            class="fas fa-trash"></i></a>';
                }
                $html .= '
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="hiddenRow">
                                                            <div class="accordian-body collapse"
                                                                id="demo'.$pb['id'].'">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <h5 class="box-title my-3">Template Sertifikat</h5>
                                                                    </div>
                                                                </div>
                                                                <table class="table table-striped">
                                                                    <thead id="head_tabel'.$pb['id'].'">';
                $class_head = 'bg-info';
                if (empty($pb['template_sertifikat'])) {
                    $class_head = 'bg-danger';
                }
                $html .= '
                                                                        <tr
                                                                            class="'.$class_head.'">
                                                                            <th class="text-center">Template</th>
                                                                            <th class="text-center">Opsi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="data_sertifikat'.$pb['id'].'">';
                if (empty($pb['template_sertifikat'])) {
                    $html .= '<tr>
                                                                        <td colspan="2" class="text-center">
                                                                            Template saat ini tidak tersedia. <br>
                                                                            <button data-id="'.$pb['id'].'"
                                                                                class="btn btn-success btn-sm set_default"><i
                                                                                    class="fas fa-wrench"></i> Set
                                                                                Default</button>
                                                                            <small class="text-danger">*Untuk
                                                                                mengeset sertifikat sekarang</small>
                                                                        </td>
                                                                    </tr>';
                } else {
                    foreach ($pb['template_sertifikat'] as $template) {
                        $html .= '<tr>
                                                                            <td class="text-center">
                                                                                '.$template['template'].'</td>
                                                                            <td class="text-center">
                                                                                <a href="'.route('prakerin_template-detail', $template['id_template']) .'"
                                                                                    target="_blank"
                                                                                    class="btn btn-purple btn-sm editPembimbing"
                                                                                    data-id="'.$pb['id'].'"
                                                                                    data-industri="'.$pb['id'].'"><i
                                                                                        class="fas fa-info-circle"></i></a>
                                                                            </td>
                                                                        </tr>';
                    }
                }
                $html .= '</tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>';
            }
        }
        return $html;
    }

    public function reset_password(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->pembimbingApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
            } else {
                return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->pembimbingApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->pembimbingApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }


}
