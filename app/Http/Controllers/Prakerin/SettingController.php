<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\PembimbingSertifikatApi;
use App\ApiService\Prakerin\TemplateSertifikatApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    private $templateApi;
    private $pSertifikatApi;

    public function __construct()
    {
        $this->templateApi = new TemplateSertifikatApi();
        $this->pSertifikatApi = new PembimbingSertifikatApi();
    }

    public function index(Request $request)
    {
        // dd("tes");
        Session::put('title', 'Setting');
        $template = $this->templateApi->get_all();
        $template = $template['body']['data'];
        return view('content.prakerin.' . session('role') . '.setting.v_setting')->with(['template' => session('template'), 'tempser' => $template]);
    }

    public function update(Request $request)
    {
        $data = array(
            'id_template' => $request['id'],
            'id_pemb_industri' => session('id'),
            'id_sekolah' => session('id_sekolah')
        );
        $sertifikat = $this->pSertifikatApi->get_by_pemb_industri(session('id'));
        $sertifikat = $sertifikat['body']['data'];
        if ($sertifikat != null) {
            $data['id'] = $sertifikat['id'];
        }
        $update = $this->pSertifikatApi->create(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }
}
