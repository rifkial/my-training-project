<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\InformasiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    private $informasiApi;

    public function __construct()
    {
        $this->informasiApi = new InformasiApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Menu Informasi');
        $informasi = $this->informasiApi->sekolah_tanpa_login(session('id_sekolah'));
        // dd($informasi);
        $informasi = $informasi['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_informasi')->with(['template' => $template, 'informasi' => $informasi]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'judul' => $request['nama'],
            'isi' => $request['isi'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->informasiApi->create(json_encode($data));
        // dd($result);
        $informasi = $this->data_informasi();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $informasi
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $informasi
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'judul' => $request['nama'],
            'isi' => $request['isi'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        $result = $this->informasiApi->update_info(json_encode($data));
        // dd($result);
        $informasi = $this->data_informasi();
        if ($result['code'] == 200) {
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $informasi
            ]);
        } else {
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $informasi
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->informasiApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }


    public function delete(Request $request)
    {
        $delete = $this->informasiApi->soft_delete($request['id']);
        // dd($delete);
        $informasi = $this->data_informasi();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $informasi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $informasi
            ]);
        }
    }

    private function data_informasi()
    {
        $informasi = $this->informasiApi->sekolah_tanpa_login(session('id_sekolah'));
        $informasi = $informasi['body']['data'];
        $html = '';
        if ($informasi == null) {
            $html .= '<tr><td colspan="4">Informasi anda kosong</td></tr>';
        } else {
            $nomer = 1;
            foreach ($informasi as $ifr) {
                $kelas = $ifr['status'] == 1 ? "success" : "danger";
                $status = $ifr['status'] == 1 ? "Tampil" : "Disembunyikan";
                $html .= '<tr>
                <td>'.$nomer++.'</td>
                <td>'.$ifr['judul'].'</td>
                <td><span
                        class="badge badge-'.$kelas.' text-inverse">'.$status.'</span>
                </td>
                <td>
                <a href="#"><i class="material-icons list-icon md-18 text-success">info</i></a>
                <a href="javascript:void(0)" data-id="'.$ifr['id'].'" class="edit"><i class="material-icons list-icon md-18 text-info">edit</i></a>
                <a href="javascript:void(0)" onclick="deleteInformasi('.$ifr['id'].')"><i class="material-icons list-icon md-18 text-danger">delete</i></a>
                </td>
                </tr>';
            }
        }
        return $html;
        
    }
}
