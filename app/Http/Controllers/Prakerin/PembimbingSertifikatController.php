<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\PembimbingSertifikatApi;
use App\ApiService\Prakerin\TemplateSertifikatApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class PembimbingSertifikatController extends Controller
{
    private $pembimbingApi;
    private $industriApi;
    private $sertifikatApi;
    private $templateApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->sertifikatApi = new PembimbingSertifikatApi();
        $this->templateApi = new TemplateSertifikatApi();
    }

    public function index()
    {
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        $template = $this->templateApi->get_all();
        $template = $template['body']['data'];
        session()->put('title', "Pengaturan Template Sertifikat");
        return view('content.prakerin.template_sertifikat.industri.v_industri')->with(['industri' => $industri, 'template' => session('template'), 'theme' => $template]);
    }

    public function set_default(Request $request)
    {
        $data = array(
            'id_pemb_industri' => $request['id'],
            'id_template' => 1,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->sertifikatApi->create(json_encode($data));
        $industri = $this->data_pembimbing_industri($request['id']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $industri
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $industri
            ]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_pemb_industri' => $request['id_pembimbing'],
            'id_template' => $request['id_template'],
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->sertifikatApi->create(json_encode($data));
        $industri = $this->data_pembimbing_industri($request['id_industri']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $industri
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $industri
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'id_pemb_industri' => $request['id_pembimbing'],
            'id_template' => $request['id_template'],
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->sertifikatApi->update_info(json_encode($data));
        $industri = $this->data_pembimbing_industri($request['id_industri']);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $industri
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $industri
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $sertifikat = $this->sertifikatApi->get_by_id($request['id']);
        // dd($sertifikat);
        $sertifikat = $sertifikat['body']['data'];
        return response()->json($sertifikat);
    }

    private function data_pembimbing_industri($id)
    {
        $p = $this->pembimbingApi->get_by_id($id);
        $p = $p['body']['data'];
        $pembimbing =$this->pembimbingApi->by_industri($p['id_industri']);
        $pembimbing = $pembimbing['body']['data'];
        $html = '';
        if (empty($pembimbing)) {
            $html .= '<tr>
            <td colspan="6" class="text-center">Pembimbing Industri belum tersedia</td>
            </tr>';
        } else {
            $nomer = 1;
            foreach ($pembimbing as $pmb) {
                $kelas = '';
                if (empty($pmb['template_sertifikat'])) {
                    $kelas = 'bg-danger';
                }
                $html .= '
                <tr class="'.$kelas.'">
                        <td class="align-middle">'.$nomer++.'</td>
                        <td class="align-middle">'.$pmb['nama'].'</td>
                        <td class="align-middle">'.$pmb['jenkel'].'</td>
                        <td class="align-middle">'.$pmb['telepon'].'</td>
                        <td class="align-middle">'.$pmb['email'].'</td>
                        <td class="text-center align-middle">';
                if (empty($pmb['template_sertifikat'])) {
                    $html .= '<small class="text-light">*Sertifikat belum di
                            set</small>
                        <br>
                        <a href="javascript:void(0)"
                            data-id="'.$pmb['id'].'"  data-industri="'.$p['id_industri'] .'"
                            class="btn btn-success btn-sm set_default"><i
                                class="fas fa-bolt"></i> Set Default</a>
                        <a href="javascript:void(0)"
                            class="btn btn-info btn-sm" onclick="pilihTemplate('.$pmb['id'].')"><i
                                class="fas fa-broom"></i> Set Custom</a>';
                } else {
                    $html .= ' <button class="btn btn-info btn-sm editTemplate"  data-id="'.$pmb['template_sertifikat'][0]['id'].'"><i class="fas fa-pencil-alt"></i> Edit </button>
                            <a href="'.route('prakerin_template-detail', $pmb['template_sertifikat'][0]['id_template']).'" target="_blank" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> Lihat</a>';
                }
                $html .= '</td></tr>';
            }
        }
        return $html;
    }
}
