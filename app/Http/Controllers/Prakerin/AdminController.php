<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\AdminApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class AdminController extends Controller
{
    private $adminApi;
    private $profileApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Admin Prakerin');
        $admin = $this->adminApi->sekolah();
        $result = $admin['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" class="key-'.$data['id'].' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'admin_prakerin\')"><i class="fa fa-key"></i> Reset Password</button>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'gambar']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.prakerin.v_admin')->with([]);
    }

    public function edit(Request $request)
    {
        // dd($request);
        $admin = $this->adminApi->get_by_id($request['id']);
        // dd($admin);
        $admin = $admin['body']['data'];
        $ex_file = explode('/', $admin['file']);
        $admin['file_edit'] = end($ex_file);
        return response()->json($admin);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->adminApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->adminApi->create(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }


    public function delete($id)
    {
        $delete = $this->adminApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function reset_password(Request $request)
    {
        $reset = $this->profileApi->reset_password_admin_prakerin($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
