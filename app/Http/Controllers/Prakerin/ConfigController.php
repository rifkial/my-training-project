<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\ConfigApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class ConfigController extends Controller
{
    private $configApi;

    public function __construct()
    {
        $this->configApi = new ConfigApi();
    }

    
    public function index()
    {
        session()->put('title', 'Settingan Config');
        $config = $this->configApi->get_by_sekolah(session('id_sekolah'));
        // dd($config);
        $config = $config['body']['data'];
        if ($config != null) {
            $aksi = "edit";
        } else {
            $aksi = "add";
        }
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }    
        return view('content.prakerin.admin-prakerin.v_config')->with(['template' => $template, 'aksi' => $aksi, 'config' => $config]);
    }

    
    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'judul' => $request['judul'],
            'footer' => $request['footer'],
            'id_sekolah' => session('id_sekolah'),
        );
        if ($request['remove_photo']) {
            if ($request['remove_photo'][0] == "file1" && empty($request['remove_photo'][1])) {
                $data['hapus'] = 'file1';
            } elseif ($request['remove_photo'][0] == "file" && empty($request['remove_photo'][1])) {
                $data['hapus'] = 'file';
            } elseif ($request['remove_photo'][0] && $request['remove_photo'][1]) {
                $data['hapus'] = 'hapus';
            } else {
                $data['hapus'] = 'aman';
            }
        }
        // dd($data);
       
        
        $file = $request->file('image');
        $file1 = $request->file('image1');
        // dd($file1);
        $data_file = [];
        $data_file[] = array(
            $file, $file1
        );
        // dd($data);

        if ($file && empty($file1)) {
            $namaFile = $file->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/prakerin/config/";
            Help::check_and_make_dir($basePath);
            $file->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->configApi->create_file(json_encode($data));
            File::delete($path);
        } elseif ($file1 && empty($file)) {
            $namaFile = $file1->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/config/";
            Help::check_and_make_dir($basePath);
            $file1->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->configApi->create_file1(json_encode($data));
            File::delete($path);
        } elseif ($file && $file1) {
            foreach ($data_file[0] as $fl) {
                $namaFile = $fl->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bkk/config/";
                Help::check_and_make_dir($basePath);
                $fl->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data['path'][] = $path;
            }
            $result = $this->configApi->create_files(json_encode($data));
            foreach ($data['path'] as $del_path) {
                File::delete($del_path);
            }
        } else {
            $result = $this->configApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'success', 
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'error', 
                'status' => 'gagal'
            ]);
        }
    }
}
