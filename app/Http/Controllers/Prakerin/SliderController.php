<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\SliderApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SliderController extends Controller
{
    private $sliderApi;

    public function __construct()
    {
        $this->sliderApi = new SliderApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Slider');
        $slider = $this->sliderApi->sekolah(session('id_sekolah'));
        // dd($slider);
        $slider = $slider['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        
        return view('content.prakerin.admin-prakerin.v_slider')->with(['template' => $template, 'slider' => $slider]);
    }

    public function edit(Request $request)
    {
        $slider = $this->sliderApi->get_by_id($request['id']);
        // dd($slider);
        $slider = $slider['body']['data'];
        return response()->json($slider);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'judul' => $request->nama,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->sliderApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->sliderApi->create(json_encode($data));
        }
        // dd($result);
        $slide = $this->data_slide();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'success', 
                'status' => 'berhasil',
                'data_slide' => $slide
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'error', 
                'status' => 'gagal',
                'data_slide' => $slide
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'judul' => $request->nama,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->sliderApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->sliderApi->update_info(json_encode($data));
        }
        $slide = $this->data_slide();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'success', 
                'status' => 'berhasil',
                'data_slide' => $slide
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'error', 
                'status' => 'gagal',
                'data_slide' => $slide
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->sliderApi->soft_delete($request['id']);
        $slide = $this->data_slide();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'data_slide' => $slide
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'data_slide' => $slide
            ]);
        }
    }

    private function data_slide()
    {
        $slider = $this->sliderApi->sekolah(session('id_sekolah'));
        $slider = $slider['body']['data'];
        $html = '';
        if (!empty($slider)) {
            $no = 1;
            foreach ($slider as $sd) {
                $html .= '
                <tr>
                <td class="vertical-middle text-center">'.$no++.'</td>
                <td class="vertical-middle text-center"><img src="'.$sd['file'].'" alt=""
                        height="70"></td>
                <td class="vertical-middle text-center">'.$sd['judul'].'</td>
                <td class="vertical-middle text-center">'.$sd['keterangan'].'</td>
                <td class="vertical-middle text-center">
                    <a href="javascript:void(0)" class="edit text-info"
                        data-id="'.$sd['id'].'"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="delete text-danger"
                        data-id="'.$sd['id'].'"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
