<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Prakerin\PedomanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class PedomanController extends Controller
{
    private $pedomanApi;

    public function __construct()
    {
        $this->pedomanApi = new PedomanApi();
    }

    public function index()
    {
        session()->put('title', 'Menu pedoman');
        $pedoman = $this->pedomanApi->sekolah_tanpa_login(session('id_sekolah'));
        // dd($pedoman);
        $pedoman = $pedoman['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_pedoman')->with(['template' => $template, 'pedoman' => $pedoman]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
            'status' => $request->status
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pedomanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pedomanApi->create(json_encode($data));
        }
        // dd($result);
        $pedoman = $this->data_pedoman();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'html' => $pedoman
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'html' => $pedoman
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
            'status' => $request->status
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pedomanApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pedomanApi->update_info(json_encode($data));
        }
        $pedoman = $this->data_pedoman();
        if ($result['code'] == 200) {
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $pedoman
            ]);
        } else {
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $pedoman
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->pedomanApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }


    public function delete(Request $request)
    {
        $delete = $this->pedomanApi->soft_delete($request['id']);
        // dd($delete);
        $pedoman = $this->data_pedoman();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $pedoman
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $pedoman
            ]);
        }
    }

    private function data_pedoman()
    {
        $pedoman = $this->pedomanApi->sekolah_tanpa_login(session('id_sekolah'));
        $pedoman = $pedoman['body']['data'];
        $html = '';
        if ($pedoman == null) {
            $html .= '<tr><td colspan="5">pedoman anda kosong</td></tr>';
        } else {
            $nomer = 1;
            foreach ($pedoman as $pd) {
                $kelas = $pd['status'] == 1 ? "success" : "danger";
                $status = $pd['status'] == 1 ? "Tampil" : "Disembunyikan";
                $html .= '<tr>
                <td>'.$nomer++.'</td>
                <td>'.$pd['nama'].'</td>
                <td>
                <button class="btn btn-info btn-sm" data-id=""="'.$pd['file'].'"
                    onclick="viewPDF('. $pd['id'].')">View PDF</button>
            </td>
                <td><span
                        class="badge badge-'.$kelas.' text-inverse">'.$status.'</span>
                </td>
                <td>
                <a href="javascript:void(0)" data-id="'.$pd['id'].'" class="edit"><i class="material-icons list-icon md-18 text-info">edit</i></a>
                <a href="javascript:void(0)" onclick="deletepedoman('.$pd['id'].')"><i class="material-icons list-icon md-18 text-danger">delete</i></a>
                </td>
                </tr>';
            }
        }
        return $html;
    }
}
