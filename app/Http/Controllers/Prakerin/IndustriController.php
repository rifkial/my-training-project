<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\IndonesiaApi;
use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\KelompokApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class IndustriController extends Controller
{
    private $industriApi;
    private $kelompokApi;
    private $indonesiaApi;
    private $pembimbingApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->kelompokApi = new KelompokApi();
    }

    public function index()
    {
        session()->put('title', 'Perusahaan');
        $url = $this->industriApi->download_excel();
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $result = $industri['body']['data'];
        $provinsi = $this->indonesiaApi->get_provinsi();
        $provinsi = $provinsi['body']['data'];
        // dd($provinsi);
        $kabupaten = $this->indonesiaApi->get_kabupaten();
        $kabupaten = $kabupaten['body']['data'];
        // if ($request->ajax()) {
        //     $table = datatables()->of($result)
        //         ->addColumn('action', function ($data) {
        //             $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
        //             $button .= '<a  class="btn btn-info btn-sm edit"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>';
        //             $button .= ' <button type="button" class="btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash-o"></i></button>';
        //             $button .= '</div>';
        //             return $button;
        //         });
        //     $table->editColumn('gambar', function ($row) {
        //         return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
        //     });
        //     $table->rawColumns(['action', 'gambar']);
        //     $table->addIndexColumn();

        //     return $table->make(true);
        // }
        $template = session('template');
        if (session('role') == 'admin-prakerin') {
            $template = 'default';
        }
        if (session('role') == 'admin-prakerin' || session('role') == 'supervisor') {
            return view('content.prakerin.admin-prakerin.v_industri')->with(['template' => $template, 'provinsi' => $provinsi, 'kabupaten' => $kabupaten, 'industri' => $result, 'url' => $url]);
        } else {
            return view('content.prakerin.' . session('role') . '.v_industri')->with(['template' => $template, 'provinsi' => $provinsi, 'kabupaten' => $kabupaten]);
        }
    }

    public function edit(Request $request)
    {
        $industri = $this->industriApi->get_by_id($request['id']);
        $industri = $industri['body']['data'];
        $ex_file = explode('/', $industri['file']);
        $industri['file_edit'] = end($ex_file);
        return response()->json($industri);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pimpinan' => $request->pimpinan,
            'id_provinsi' => $request->provinsi,
            'id_kabupaten' => $request->kabupaten,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->industriApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->industriApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $industri = $this->data_industri_full();
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'industri' => $industri
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'pimpinan' => $request->pimpinan,
            'id_provinsi' => $request->provinsi,
            'id_kabupaten' => $request->kabupaten,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->industriApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->industriApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->industriApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            $industri = $this->data_industri_full();
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'industri' => $industri
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->industriApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $industri = $this->data_industri_full();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'industri' => $industri,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function search(Request $request)
    {
        $nama = array(
            'nama' => $request->nama
        );
        $industri = $this->industriApi->search_by_industri(json_encode($nama), session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        $html = '';
        foreach ($industri as $ind) {
            $alamat = '-';
            $pimpinan = '-';
            if ($ind['alamat'] != null) {
                $alamat = $ind['alamat'];
            }
            if ($ind['pimpinan'] != null) {
                $pimpinan = ucwords($ind['pimpinan']);
            }
            $html .= '
                <div class="bg-white p-3 rounded mt-2">
                    <div class="row">
                        <div class="col-md-3" style="display: flex; align-items: center; justify-content: center;">
                            <div>
                                <img src="'.$ind['file'].'" alt="" style="width: 50%">
                            </div>
                        </div>
                        <div class="col-md-6 border-right">
                            <div class="listing-title">
                                <h5>'.$ind['nama'].'</h5>
                            </div>
                            <div class="d-flex flex-row align-items-center">
                                <div class="level mr-2"><span>Lokasi:</span><span class="font-weight-bold">&nbsp;
                                        Temanggung</span>
                                </div>
                                <div class="level mr-1"><span>Telepon:</span><span class="font-weight-bold">&nbsp;
                                        '.$ind['telepon'].'</span>
                                </div>
                            </div>
                            <div class="description">
                                <p class="mb-0"><strong>Alamat perusahaan :</strong></p>
                                <p>'. $alamat .'</p>
                            </div>
                        </div>
                        <div class="d-flex col-md-3"
                            style="display: flex; align-items: center; justify-content: center;">
                            <div class="d-flex flex-column justify-content-start user-profile w-100">
                                <div class="d-flex user"><img class="rounded-circle"
                                        src="http://localhost/smart_school/public/images/default.png" width="50">
                                    <div class="about ml-1">
                                        <span class="d-block text-black font-weight-bold">'. $pimpinan .'</span><span>Pimpinan</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                ';
        }
        return response()->json($html);
    }

    public function load_pembimbing_kelompok(Request $request)
    {
        $id_kelompok = $request['id_kelompok'];
        $id_pembimbing =$request['id_pemb_industri'];
        $pembimbing = $this->pembimbingApi->by_industri($request['id_industri']);
        $pembimbing = $pembimbing['body']['data'];
        $kelompok = $this->kelompokApi->get_by_industri($request['id_industri']);
        $kelompok = $kelompok['body']['data'];
        $data_kelompok = '';
        $data_pembimbing = '';
        foreach ($kelompok as $kl) {
            $data_kelompok .= '<option value="' . $kl['id'] . '" ' . (($kl['id'] == $id_kelompok) ? 'selected="selected"' : "") . '>' . $kl['nama'] . '</option>';
        }
        foreach ($pembimbing as $pb) {
            $data_pembimbing .= '<option value="' . $pb['id'] . '" ' . (($pb['id'] == $id_pembimbing) ? 'selected="selected"' : "") . '>' . $pb['nama'] . '</option>';
        }

        $output = array(
            'kelompok' => $data_kelompok,
            'pembimbing' => $data_pembimbing
        );
        return $output;
    }
    
    private function data_industri_full()
    {
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        $html = '';
        if (!empty($industri)) {
            $no =1;
            foreach ($industri as $id) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$id['nama'].'</td>
                    <td>'.$id['pimpinan'].'</td>
                    <td>'.$id['telepon'].'</td>
                    <td>'.$id['email'].'</td>
                    <td>'.$id['alamat'].'</td>
                    <td>
                        <a href="javascript:void(0)" data-toggle="collapse"
                            data-target="#demo'.$id['id'].'"
                            class="btn btn-info btn-sm accordion-toggle"><i
                                class="fas fa-users-cog"></i></a>
                        <a href="javascript:void(0)" class="btn btn-purple btn-sm edit"
                            data-id="'.$id['id'].'"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)" data-id="'.$id['id'].'"
                            class=" delete btn btn-danger btn-sm"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse"
                            id="demo'.$id['id'].'">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title my-3">Pembimbing Industri</h5>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-info btn-sm my-3 pull-right"
                                        onclick="tambahPembimbing({{ '.$id['id'].' }})"><i
                                            class="fas fa-plus-circle"></i>
                                        Tambah Pembimbing</button>
                                </div>
                            </div>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Telepon</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>';

                if (empty($id['pembimbing'])) {
                    $html .= ' <tr><td colspan="6" class="text-center">Data pembimbing saat ini tidak tersedia</td></tr>';
                } else {
                    $nomer = 1;
                    foreach ($id['pembimbing'] as $pb) {
                        $html .= '
                                <tr>
                                        <td>'.$nomer++.'</td>
                                        <td>'.strtoupper($pb['nama']).'</td>
                                        <td>'.$pb['telepon'].'</td>
                                        <td>'.$pb['email'].'</td>
                                        <td>'.$pb['alamat'].'</td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-purple btn-sm editPembimbing"
                                            data-id="'.$pb['id'].'" data-industri="'.$id['id'].'"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <a href="javascript:void(0)" data-id="'.$pb['id'].'"
                                            class="deletePembimbing btn btn-danger btn-sm" data-industri="'.$id['id'].'"><i
                                                class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                ';
                    }
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '<tr><td colspan="7" class="text-center">Data saat ini belum tersedia</td></tr>';
        }
        return $html;
    }

    public function load_kabupaten(Request $request)
    {
        $id_kabupaten = $request['id_kabupaten'];
        $data  = $this->indonesiaApi->get_kabupaten_by_provinsi($request['id_provinsi']);
        // dd($data);
        $result = $data['body']['data'];
        // dd($result);
        $output = '';
        foreach ($result['cities'] as $brand) {
            $output .= '<option value="'.$brand['id'].'" '.(($brand['id'] == $id_kabupaten) ? 'selected="selected"':"").'>'.$brand['name'].'</option>';
        }
        return $output;
    }

    public function deletePembimbing(Request $request)
    {
        $delete = $this->pembimbingApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function simpanPembimbing(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id_industri' => $request->id_industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->pembimbingApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function updatePembimbing(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'id_industri' => $request->id_industri,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->pembimbingApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->pembimbingApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->pembimbingApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            $pembimbing = $this->data_pembimbing($request['id_industri']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'pembimbing' => $pembimbing
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_pembimbing($id_industri)
    {
        $pembimbing = $this->pembimbingApi->by_industri($id_industri);
        $pembimbing = $pembimbing['body']['data'];
        $html = '';
        if (empty($pembimbing)) {
            $html .= '<tr><td colspan="6" class="text-center">Data belum tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($pembimbing as $pb) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.strtoupper($pb['nama']).'</td>
                    <td>'.$pb['telepon'].'</td>
                    <td>'.$pb['email'].'</td>
                    <td>'.$pb['alamat'].'</td>
                    <td>
                        <a href="javascript:void(0)"
                            class="btn btn-purple btn-sm editPembimbing"
                            data-id="'.$pb['id'].'" data-industri="'.$pb['id_industri'].'"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)"
                            data-id="'.$pb['id'].'" data-industri="'.$pb['id_industri'].'"
                            class="deletePembimbing btn btn-danger btn-sm"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                ';
            }
        }
        return $html;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->industriApi->upload_excel(json_encode($data), $request['role']);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            $industri = $this->data_industri_full();
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'industri' => $industri
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
