<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Prakerin\JenisNilaiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Foreach_;

class JenisNilaiController extends Controller
{
    private $jurusanApi;
    private $jenisNilaiApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->jenisNilaiApi = new JenisNilaiApi();
    }

    public function index()
    {
        $jenis = $this->jenisNilaiApi->get_by_group();
        $jenis = $jenis['body']['data'];
        Session::put('title', 'Jenis Nilai');
        $jurusan = $this->jurusanApi->get_by_sekolah(session('id_sekolah'));
        $jurusan = $jurusan['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_jenis_nilai')->with(['template' => $template, 'jurusan' => $jurusan, 'jenis' => $jenis]);
    }

    public function store(Request $request)
    {
        if (empty($request['id_jurusan']) || empty($request['nama'])) {
            return response()->json([
                'message' => "Harap masukan jurusan dan nama jenis terlebih dulu",
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
        $id_jurusan = implode(',', $request['id_jurusan']);
        $nama = implode(',', $request['nama']);
        $data = array(
            'nama' => $nama,
            'id_sekolah' => session('id_sekolah'),
            'id_jurusan' => $id_jurusan,
            'status' => 1
        );
        $result =  $this->jenisNilaiApi->create(json_encode($data));
        $jenis = $this->data_jenis();
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jenis' => $jenis
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'jenis' => $jenis
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request['nama'],
            'id_sekolah' => session('id_sekolah'),
            'id_jurusan' => $request['jurusan'],
            'status' => 1
        );
        $result = $this->jenisNilaiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $jenis = $this->data_jenis();
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'jenis' => $jenis
            ]);
        } else {
            return response()->json([
                'message' =>$result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->jenisNilaiApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }


    public function delete(Request $request)
    {
        $delete = $this->jenisNilaiApi->soft_delete($request['id']);
        $jenis = $this->child_jenis($request['jenis']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'jenis' => $jenis
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'jenis' => $jenis
            ]);
        }
    }

    private function child_jenis($jn)
    {
        $jenis =  str_replace('_', ' ', $jn);
        $data_jenis = $this->jenisNilaiApi->search_by_name(json_encode(['jenis' => $jenis]));
        // dd($data_jenis);
        $data_jenis = $data_jenis['body']['data'];
        $html = '';
        if (!empty($data_jenis)) {
            $no = 1;
            foreach ($data_jenis as $jn) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$jn['jurusan'].'</td>
                    <td>
                        <a href="javascript:void(0)"
                            data-id="'.$jn['id'].'"
                            data-jenis="'. str_replace(' ', '_', $jn['nama']).'"
                            class="edit text-info">
                            <i class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)"
                            data-id="'.$jn['id'].'"
                            data-jenis="'. str_replace(' ', '_', $jn['nama']).'"
                            class="delete text-danger">
                            <i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="3" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    private function data_jenis()
    {
        $jenis = $this->jenisNilaiApi->get_by_group();
        $jenis = $jenis['body']['data'];
        $html = '';
        if (!empty($jenis)) {
            $no = 1;
            foreach ($jenis as $jn) {
                $kelas_text = 'danger';
                if ($jn['jumlah'] > 0) {
                    $kelas_text = 'info';
                }
                $html .= '<tr>
                <td class="text-center">'.$no.'</td>
                <td class="text-center">'.$jn['nama_jenis'].'</td>
                <td class="text-center"><small
                        class="text-'.$kelas_text.'">'.$jn['jumlah'].'
                        Jurusan yang terhubung</small></td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="btn-sm accordion-toggle text-info"
                        data-toggle="collapse" data-target="#demo'.$no.'"><i
                            class="fas fa-info-circle"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo'.$no.'">
                        <table class="table">
                            <thead>
                                <tr class="bg-info">
                                    <th>Nomer</th>
                                    <th>Nama Jurusan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="tabel_'.str_replace(' ', '_', $jn['nama_jenis']).'">';
                if (!empty($jn['jenis_nilai'])) {
                    $nomer =1;
                    foreach ($jn['jenis_nilai'] as $jenis) {
                        $html .= '
                                    <tr>
                                            <td>'.$nomer++.'</td>
                                            <td>'.$jenis['jurusan'].'</td>
                                            <td>
                                                <a href="javascript:void(0)"
                                                    data-id="'.$jenis['id'].'"
                                                    data-jenis="'.str_replace(' ', '_', $jn['nama_jenis']).'"
                                                    class="edit text-info">
                                                    <i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)"
                                                    data-id="'.$jenis['id'].'"
                                                    data-jenis="'.str_replace(' ', '_', $jn['nama_jenis']).'"
                                                    class="delete text-danger">
                                                    <i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    ';
                    }
                } else {
                    $html .= '<tr><td class="text-center" colspan="3">Data saat ini tidak tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
                $no++;
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }
}
