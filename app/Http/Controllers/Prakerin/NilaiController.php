<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Prakerin\JenisNilaiApi;
use App\ApiService\Prakerin\NilaiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NilaiController extends Controller
{
    private $nilaiApi;
    private $jenisApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->nilaiApi = new NilaiApi();
        $this->jenisApi = new JenisNilaiApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        Session::put('title', 'Daftar Nilai');
        $jenis = $this->jenisApi->sekolah_tanpa_login(session('id_sekolah'));
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $template = 'default';
        if (session('role') == 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.v_nilai')->with(['template' => $template, 'jenis' => $jenis, 'jurusan' => $jurusan]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => implode(',', $request['nama']),
            'id_jenis_nilai' => $request['jenis'],
            'status' => 1,
            'id_sekolah' => session('id_sekolah')
        );
        $result = $this->nilaiApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $nilai = $this->data_nilai($request['jenis']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'nilai' => $nilai
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->nilaiApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'][0],
            'id_sekolah' => session('id_sekolah'),
            'id_jenis_nilai' => $request['jenis'],
            'status' => 1
        );
        // dd($data);
        $result = $this->nilaiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $nilai = $this->data_nilai($request['jenis']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'nilai' => $nilai
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {

        $delete = $this->nilaiApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $nilai = $this->data_nilai($request['id_jenis']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'nilai' => $nilai
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_jurusan(Request $request)
    {
        // dd($request);
        $jenis = $this->jenisApi->get_by_jurusan($request['id_jurusan']);
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $html = '';
        if (!empty($jenis)) {
            $no = 1;
            foreach ($jenis as $jn) {
                $kelas_nilai = 'danger';
                if ($jn['jumlah_nilai'] > 0) {
                    $kelas_nilai = 'info';
                }
                $html .= '
                <tr>
                                            <td class="text-center">' . $no++ . '</td>
                                            <td class="text-center">' . $jn['nama'] . '</td>
                                            <td class="text-center">' . $jn['jurusan'] . '</td>
                                            <td class="text-center">
                                                <a href="javascript:void(0)" class="btn-sm accordion-toggle text-' . $kelas_nilai . '"
                                                    data-toggle="collapse" data-target="#demo' . $jn['id'] . '"><i
                                                        class="fas fa-info-circle"></i> Lihat Selengkapnya ' . $jn['jumlah_nilai'] . ' Data Nilai</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="hiddenRow">
                                                <div class="accordian-body collapse" id="demo' . $jn['id'] . '">';
                if (session('role') != 'supervisor') {
                    $html .= ' <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-info addNilai pull-right"
                                                            data-id="' . $jn['id'] . '"><i
                                                                class="fa fa-plus-circle"></i> Tambah Nilai</button>
                                                    </div>
                                                </div>';
                }

                $html .=  '<table class="table">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th class="text-center">Nomer</th>
                                                                <th class="text-center">Nama Nilai</th>
                                                                <th class="text-center">Status</th>';
                if (session('role') != 'supervisor') {
                    $html .= '<th class="text-center">Opsi</th>';
                }

                $html .= '</tr>
                                                        </thead>
                                                        <tbody id="data_nilai' . $jn['id'] . '">';
                if (!empty($jn['nilai'])) {
                    $nomer = 1;
                    foreach ($jn['nilai'] as $nl) {
                        $kelas_badge = 'danger';
                        if ($nl['status'] == "Aktif") {
                            $kelas_badge = "success";
                        }
                        $html .= '<tr>
                                  <td class="text-center">' . $nomer++ . '</td>
                                  <td class="text-center">' . $nl['nama'] . '</td>
                                  <td class="text-center">
                                  <span class="badge badge-' . $kelas_badge . ' text-inverse">' . $nl['status'] . '</span>
                                </td>';
                        if (session('role') != 'supervisor') {
                            $html .= '<td class="text-center">
    <a href="javascript:void(0)"
        data-id="' . $nl['id'] . '"
        class="edit text-info">
        <i class="fas fa-pencil-alt"></i></a>
    <a href="javascript:void(0)"
        data-id="' . $nl['id'] . '" data-jenis="' . $jn['id'] . '"
        class="delete text-danger">
        <i class="fas fa-trash"></i></a>
</td>';
                        }

                        $html .= '</tr>';
                    }
                } else {
                    $html .= '<tr><td class="text-center" colspan="4">Data saat ini tidak tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= ' <tr><td colspan="4" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return response()->json($html);
    }

    private function data_nilai($id_jenis)
    {
        $nilai = $this->nilaiApi->get_by_jenis($id_jenis);
        $nilai = $nilai['body']['data'];
        $html = '';
        if (!empty($nilai)) {
            $no = 1;
            foreach ($nilai as $nl) {
                $kelas_badge = 'danger';
                if ($nl['status'] == "Aktif") {
                    $kelas_badge = "success";
                }
                $html .= '<tr>
                            <td class="text-center">' . $no++ . '</td>
                            <td class="text-center">' . $nl['nama'] . '</td>
                            <td class="text-center">
                                <span
                                    class="badge badge-' . $kelas_badge . ' text-inverse">' . $nl['status'] . '</span>
                            </td>
                            <td class="text-center">
                                <a href="javascript:void(0)"
                                    data-id="' . $nl['id'] . '"
                                    class="edit text-info">
                                    <i class="fas fa-pencil-alt"></i></a>
                                <a href="javascript:void(0)"
                                    data-id="' . $nl['id'] . '" data-jenis="' . $id_jenis . '"
                                    class="delete text-danger">
                                    <i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                ';
            }
        } else {
            $html .= '<tr><td class="text-center" colspan="4">Data saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }
}
