<?php

namespace App\Http\Controllers\Prakerin;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\KelompokApi;
use App\ApiService\Prakerin\PembimbingIndustriApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class SiswaPesertaController extends Controller
{
    private $hash;
    private $siswaApi;
    private $jurusanApi;
    private $industriApi;
    private $guruApi;
    private $kelasSiswaApi;
    private $rombelApi;
    private $tahunApi;

    public function __construct()
    {
        $this->tahunApi = new TahunAjaranApi();
        $this->pembimbingApi = new PembimbingIndustriApi();
        $this->kelompokApi = new KelompokApi();
        $this->jurusanApi = new JurusanApi();
        $this->rombelApi = new RombelApi();
        $this->siswaApi = new SiswaPrakerinApi();
        $this->industriApi = new IndustriApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->guruApi = new GuruApi();
        $this->hash = new Hashids();
    }

    public function index()
    {
        Session::put('title', 'Peserta Prakerin');

        $routes = "pkl_siswa-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->siswaApi->get_sekolah(session('id_tahun_ajar'), $search);
        $siswa = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        $industri = $industri['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $template = 'default';
        if (session('role') != 'admin-prakerin') {
            $template = session('template');
        }
        return view('content.prakerin.admin-prakerin.siswa_prakerin.v_siswa_prakerin')->with(['template' => $template, 'siswa' => $siswa, 'search' => $search, 'pagination' => $pagination,
        'routes' => $routes, 'industri' => $industri, 'rombel' => $rombel, 'tahun' => $tahun, 'guru' => $guru]);
    }

    public function create()
    {
        session()->put('title', "Tambah Siswa Prakerin");
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $template = session('template');
        if(session('role') == 'admin-prakerin' || session('role') == 'admin'){
         $template = 'default';   
        }
        return view('content.prakerin.admin-prakerin.siswa_prakerin.v_create_siswa')->with(['template' => $template, 'jurusan' => $jurusan, 'industri' => $industri, 'guru' => $guru]);
    }

    public function store(Request $request)
    {
        // dd($request);
        foreach ($request['kelas_siswa'] as $siswa) {
            $data = array(
                'id_kelas_siswa' => $siswa,
                'id_sekolah' => session('id_sekolah'),
                'tgl_mulai' => date('Y-m-d', strtotime($request->tgl_mulai)),
                'tgl_selesai' => date('Y-m-d', strtotime($request->tgl_selesai)),
                'id_prakerin_kelompok' => $request['kelompok'],
                'id_pemb_sekolah' => $request['guru'],
                'id_pemb_industri' => $request['pembimbing'],
                'id_ta_sm' => $request['tahun_ajar'],
                'status' => 1
            );
            $siswa = $this->siswaApi->create(json_encode($data));
        }
        if ($siswa['code'] == 200) {
            return response()->json([
                'success' => $siswa['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $siswa['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $post  = $this->siswaApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        // dd($result);
        // $result['tgl_selesai'] = date('d-m-Y', strtotime($result['tgl_selesai']));
        // $result['tgl_mulai'] = date('d-m-Y', strtotime($result['tgl_mulai']));
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_kelas_siswa' => $request['id_kelas_siswa'],
            'id_sekolah' => session('id_sekolah'),
            'tgl_mulai' => $request->tgl_mulai,
            'tgl_selesai' => $request->tgl_selesai,
            'id_prakerin_kelompok' => $request['kelompok'],
            'id_pemb_sekolah' => $request['guru'],
            'id_pemb_industri' => $request['pembimbing_industri'],
            'id_ta_sm' => $request['tahun_ajar'],
            'status' => 1
        );
        $siswa = $this->siswaApi->create(json_encode($data));
        if ($siswa['code'] == 200) {
            return response()->json([
                'message' => $siswa['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $siswa['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->siswaApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_kelompok(Request $request)
    {
        $siswa = $this->siswaApi->get_by_kelompok($request['id']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $html = '<div class="row">
                    <div class="col-md-12">
                        <div class="people-nearby">';
        foreach ($siswa as $sw) {
            $html .= '
            <div class="nearby-user">
                <div class="row">
                    <div class="col-md-2 col-sm-2" style="display: flex; align-items: center; justify-content: center;">
                        <img src="' . $sw['file'] . '"
                            alt="user" class="profile-photo-lg">
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <h5><a href="#" class="profile-link">' . ucwords($sw['nama']) . '</a>
                        </h5>
                        <p class="mb-0">' . $sw['alamat'] . '</p>
                        <p class="text-muted mb-0">' . $sw['telepon'] . '</p>
                    </div>
                    <div class="col-md-3 col-sm-3" style="display: flex; align-items: center; justify-content: center;">
                        <a href="/program/sistem_pkl/nilai_siswa/jenis/' . $this->hash->encode($sw['id']) . '" class="btn btn-info pull-right lihatNilai" data-id="' . $sw['id'] . '">Beri Nilai</a>
                    </div>
                </div>
            </div>
            ';
        }
        $html .= '</div></div></div>';
        return response()->json($html);
    }

    public function search_siswa(Request $request)
    {
        if (session('role') == "pembimbing-industri") {
            $nama = array(
                'nama' => $request->siswa
            );
            $siswa = $this->siswaApi->search_pemb_industri(json_encode($nama), session('id'));
            $siswa = $siswa['body']['data'];
            $html = '';
            foreach ($siswa as $ssw) {
                $html .= '
                <li class="bounceInDown">
                <div class="row" style="display: flex; align-items: center; justify-content: center;">
                    <div class="col-7">
                        <a href="javascript:void(0)" class="clearfix">
                            <img src="' . $ssw['file'] . '" alt="" class="img-circle">
                            <div class="friend-name">
                                <strong>' . ucwords($ssw['nama']) . '</strong>
                            </div>
                            <div class="last-message text-muted load_' . $ssw['id'] . '">' . $ssw['nisn'] . '</div>
                        </a>
                    </div>
                    <div class="col-5">
                        <div class="float-right">
                            <a href="javascript:void(0)" onclick="detailTugas(' . $ssw['id'] . ',\'paraf\')"
                                class="btn btn-xs btn-info mr-2 mt-1" style="float: left; padding: 0 7px 0 7px;"><i
                                    class="fa fa-star"></i> Paraf</a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-success mr-2 mt-1"
                            onclick="detailTugas(' . $ssw['id'] . ',\'kegiatan\')"
                                style="float: right; padding: 0 7px 0 7px;"><i class="fa fa-calendar"></i>
                                Kegiatan</a>
                            <a href="javascript:void(0)" onclick="detailTugas(' . $ssw['id'] . ',\'siswa\')"
                                class="btn btn-xs btn-primary mr-2 mt-1"
                                style="float: left; padding: 0 7px 0 7px;"><i class="fa fa-eye"></i> Detail</a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-warning mr-2 mt-1"
                            onclick="detailTugas(' . $ssw['id'] . ',\'guru\')"
                                style="float: right; padding: 0 7px 0 7px;"><i class="fa fa-user"></i> Guru</a>
                        </div>
                    </div>
                </div>
            </li>
                ';
            }
            return response()->json($html);
        }
    }

    public function search_pembimbing_sekolah(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama']
        );
        $siswa = $this->siswaApi->search_pemb_sekolah(json_encode($data), session('id'));
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        foreach ($siswa as $sw) {
            $html .= '
            <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
            <div class="well profile_view">
                <div class="col-sm-12">
                    <h4 class="brief"><i>' . ucwords($sw['nama']) . '</i></h4>
                    <div class="row">
                        <div class="left col-md-7">
                            <h2>' . ucwords($sw['industri']) . '</h2>
                            <p><strong>NISN: </strong>' . $sw['nisn'] . '. </p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-phone"></i> Alamat Perusahaan :
                                    ' . $sw['alamat_industri'] . '</li>
                            </ul>
                        </div>
                        <div class="right col-md-5 text-center" style="padding: 25px;">
                            <img src="' . $sw['file'] . '" alt="" class="img-circle img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 bottom text-center">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 emphasis">
                        </div>
                        <div class="col-xs-12 col-sm-6 emphasis">
                        <button type="button" class="btn btn-primary btn-xs"
                            onclick="detailProfile(' . $sw['id_siswa'] . ',\'siswa\')">
                                <i class="fa fa-user"></i> View Profile
                            </button>
                            <a href="javascript:void(0)" onclick="showAgenda(' . $sw['id'] . ')"
                                class="btn btn-success btn-xs">
                                Kegiatan
                            </a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
            ';
        }
        return response()->json($html);
    }


    //Halama baru penting
    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => Help::decode($request['id_jurusan']),
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '
                <tr>
                    <td>' . $nomer++ . '</td>
                    <td>' . ucfirst($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['nisn'] . '</td>
                    <td>' . $sw['rombel'] . '</td>
                    <td>' . $sw['jurusan'] . '</td>
                    <td>';
                if ($sw['id_prakerin_siswa'] == null) {
                    $html .= '  <div class="form-check">
                                            <input class="form-check-input mx-0" type="checkbox" value="' . $sw['id'] . '" name="id_kelas_siswa[]" id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckDefault">
                                            Pilih Siswa
                                            </label>
                                        </div>';
                } else {
                    $html .= '<a class="text-danger"><i class="fas fa-exclamation-circle"></i> Siswa Sudah ditambahkan</a>';
                }

                $html .= '</td></tr>';
            }
            $html .= '
            <tr>
                <td colspan="6"></td>
                <td>
                    <button type="button" data-toggle="collapse" data-target="#demo1"
                        class="btn btn-info accordion-toggle">Proses Selanjutnya</button>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo1">

                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th class="vertical-middle">Industri</th>
                                    <td>
                                        <select name="id_industri" id="id_industri" data-id="' . $sw['id'] . '"
                                            class="form-control"  onchange="getPembimbing(this);">
                                            <option value="">Pilih Industri</option>';
            $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
            $industri = $industri['body']['data'];
            foreach ($industri as $ind) {
                $html .= '<option value="' . $ind['id'] . '">
                                                ' . $ind['nama'] . '
                                            </option>';
            }
                                $html .= '</select>
                                    </td>
                                    <td></td>
                                    <th class="vertical-middle">Pembimbing Industri</th>
                                    <td>
                                        <select
                                            name="pembimbing_industri" id="pembimbing_industri' . $sw['id'] . '"
                                            class="form-control" disabled>
                                            <option value="">Pilih Industri dulu</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Kelompok Prakerin</th>
                                    <td>
                                        <select
                                            name="kelompok" id="kelompok' . $sw['id'] . '"
                                            class="form-control" disabled>
                                            <option value="">Pilih Industri dulu</option>
                                        </select>
                                    </td>
                                    <td></td>
                                    <th class="vertical-middle">Pembimbing Sekolah</th>
                                    <td>
                                        <select
                                        name="pembimbing_sekolah" id="pembimbing_sekolah"
                                        class="form-control">
                                            <option value="">Pilih Guru</option>';
            $guru = $this->guruApi->get_by_sekolah();
            $guru = $guru['body']['data'];
            foreach ($guru as $gr) {
                $html .= '
                                        <option value="' . $gr['id'] . '">
                                            ' . $gr['nama'] . ' NIP' . $gr['nip'] . '
                                        </option>
                                        ';
            }
            $html .= '
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Tanggal Mulai</th>
                                    <td>
                                        <input type="date" name="tgl_mulai" id="tgl_mulai"
                                                    class="form-control">
                                    </td>
                                    <td></td>
                                    <th class="vertical-middle">Tanggal Selesai</th>
                                    <td>
                                        <input type="date" name="tgl_selesai" id="tgl_selesai"
                                                    class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="vertical-middle">Tahun Ajaran</th>
                                    <td>
                                        <select name="tahun_ajaran" id="tahun_ajaran"
                                                    class="form-control">';
            $tahun = $this->tahunApi->get_by_sekolah();
            // dd($tahun);
            $tahun = $tahun['body']['data'];
            foreach ($tahun as $th) {
                $html .= '
                                        <option value="' . $th['id'] . '">' . $th['tahun_ajaran'] . ' '.$th['semester'].'</option>
                                        ';
            }
                                $html .= '</select>
                                    </td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <button type="submit" id="addPrakerinSiswa"
                                            class="btn btn-info btn-block">Tambahkan</button>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </td>
            </tr>';
        }
        return $html;
    }

    public function save_siswa(Request $request)
    {
        // dd($request);
        if(empty($request['id_kelas_siswa'])){
            return response()->json([
                'icon' => "error",
                'message' => "Harap pilih siswa terlebih dulu ^-^",
                'status' => 'gagal'
            ]);
        }
        $data = array(
            'id_kelas_siswa' => implode(',', $request['id_kelas_siswa']),
            'id_pemb_industri' => $request['pembimbing_industri'],
            'id_pemb_sekolah' => $request['pembimbing_sekolah'],
            'tgl_mulai' => $request['tgl_mulai'],
            'tgl_selesai' => $request['tgl_selesai'],
            'id_ta_sm' => $request['tahun_ajaran'],
            'id_prakerin_kelompok' => $request['kelompok'],
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        $result = $this->siswaApi->create_loop(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'url' => route('pkl_siswa-beranda')
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
