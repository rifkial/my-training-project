<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\KategoriPelanggaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KategoriPelanggaranController extends Controller
{
    private $katPelanggaranApi;

    public function __construct()
    {
        $this->katPelanggaranApi = new KategoriPelanggaranApi();
    }

    public function index()
    {
        $kategori = $this->katPelanggaranApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        Session::put('title', 'Kategori Pelanggaran');
        return view('content.point.admin.v_kategori_pelanggaran')->with(['kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        // dd($request);
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->katPelanggaranApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $kategori = $this->katPelanggaranApi->get_by_id($request['id']);
        $kategori = $kategori['body']['data'];
        return response()->json($kategori);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'][0],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->katPelanggaranApi->update_info(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->katPelanggaranApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    private function data_kategori()
    {
        $kategori = $this->katPelanggaranApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        $html = '';
        if (!empty($kategori)) {
            $no = 1;
            foreach ($kategori as $kt) {
                $checked = '';
                if ($kt['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '
                <tr>
                <td class="vertical-middle text-center">' . $no++ . '</td>
                <td>' . ucwords($kt['nama']) . '</td>
                <td class="vertical-middle">
                    <label class="switch">
                        <input type="checkbox" ' . $checked . '
                            class="kategori_check" data-id="' . $kt['id'] . '">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="vertical-middle text-center">
                    <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                        data-id="' . $kt['id'] . '"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="btn btn-danger btn-sm delete"
                        data-id="' . $kt['id'] . '"><i class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)" data-toggle="collapse"
                        data-target="#pelanggaran' . $kt['id'] . '"
                        class="btn btn-purple btn-sm" data-id="' . $kt['id'] . '"><i
                            class="fas fa-angle-double-right"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="hiddenRow">
                    <div class="accordian-body collapse" id="pelanggaran' . $kt['id'] . '">
                        <button class="btn btn-info btn-sm my-3 pull-right"
                            onclick="tambahPelanggaran(' . $kt['id'] . ')"><i
                                class="fas fa-plus-circle"></i>
                            Tambah Pelanggaran</button>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Bobot Point</th>
                                    <th>Status</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataPelanggaran' . $kt['id'] . '">';
                if (!empty($kt['pelanggaran'])) {
                    $nomer = 1;
                    foreach ($kt['pelanggaran'] as $plg) {
                        $check = '';
                        if ($plg['status'] == 1) {
                            $check = 'checked';
                        }
                        $html .= '<tr>
                        <td>' . $nomer++ . '</td>
                        <td>' . $plg['nama'] . '</td>
                        <td>' . $plg['point'] . '</td>
                        <td>
                            <label class="switch">
                                <input type="checkbox" ' . $check . ' class="pelanggaran_check"
                                    data-id="' . $plg['id'] . '">
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="text-center">
                            <a href="javascript:void(0)"
                                class="btn btn-info btn-sm ePlg"
                                data-id="' . $plg['id'] . '"><i
                                    class="fas fa-pencil-alt"></i></a>
                            <a href="javascript:void(0)"
                                class="btn btn-danger btn-sm dPlg"
                                data-id="' . $plg['id'] . '" data-kategori="'.$plg['id_kategori_pelanggaran'].'"><i
                                    class="fas fa-trash"></i></a>
                        </td>
                    </tr>';
                    }
                } else {
                    $html .= ' <tr>
                    <td colspan="5" class="text-center">Data saat ini
                        tidak tersedia</td>
                </tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '  <tr><td colspan="4" class="text-center">Kategori saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }
}
