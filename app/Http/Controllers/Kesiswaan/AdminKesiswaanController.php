<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\AdminKesiswaanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AdminKesiswaanController extends Controller
{
    private $adminApi;

    public function __construct()
    {
        $this->adminApi = new AdminKesiswaanApi();
    }

    public function index()
    {
        session()->put('title', 'Admin Kesiswaan');
        $admin = $this->adminApi->sekolah();
        $admin = $admin['body']['data'];
        return view('content.admin.kesiswaan.v_admin')->with(['admin' => $admin]);
    }

    public function detail(Request $request)
    {
        $admin = $this->adminApi->get_by_id($request['id']);
        $admin = $admin['body']['data'];
        return response()->json($admin);
    }

    public function edit()
    {
        $admin = $this->adminApi->get_by_id(Help::decode($_GET['code']));
        $admin = $admin['body']['data'];
        return view('content.admin.kesiswaan.v_edit_admin')->with(['admin' => $admin]);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'nip' => $request->nip,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'first_password' => $request->password,
            'id_sekolah' => session('id_sekolah'),
        );

        $result = $this->adminApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->adminApi->update(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        $admin = $this->data_admin();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'admin' => $admin
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function reset_password(Request $request)
    {
        // dd($request);
        $reset = $this->profileApi->reset_password_admin_induk($request['id']);
        // dd($reset);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_admin()
    {
        $admin = $this->adminApi->sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (!empty($admin)) {
            $no = 1;
            foreach ($admin as $adm) {
                $html .= '
                <tr>
                <td>' . $no++ . '</td>
                <td>' . $adm['nama'] . '</td>
                <td>' . $adm['nip'] . '</td>
                <td>' . $adm['telepon'] . '</td>
                <td>' . $adm['email'] . '</td>
                <td>
                    <a href="'.route('kesiswaan_admin-edit', ['code' => Help::encode($adm['id']), 'name' => str_slug($adm['nama'])]).'"><i class="material-icons list-icon md-18 text-info">edit</i></a>
                    <a href="javascript:void(0)" class="delete" data-id="' . $adm['id'] . '"><i class="material-icons list-icon md-18 text-danger">delete</i></a>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function res_pass(Request $request)
    {
        // dd($request);
        if ($request['password'] == $request['confirm_password']) {
            $data = array(
                'id' => $request['id'],
                'password' => $request['password']
            );
            // dd($data);
            $reset = $this->adminApi->reset_pass(json_encode($data));
            if ($reset['code'] == 200) {
                return response()->json([
                    'icon' => "success",
                    'message' => $reset['body']['message'],
                    'status' => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'icon' => "error",
                    'message' => $reset['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        } else {
            return response()->json([
                'icon' => "error",
                'message' => "Mohon maaf, password dan konfirmasi password tidak sesuai",
                'status' => 'gagal'
            ]);
        }
    }

    public function delete_profile(Request $request)
    {
        $delete = $this->adminApi->delete_profile($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'image' => $delete['body']['data']['file']
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload_profile(Request $request)
    {
        $data = array(
            'id' => $request->id,
        );
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/profil/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->adminApi->update_foto(json_encode($data));
        File::delete($path);
        // dd($result);
        if ($result['code'] == 200) {
            $image = $result['body']['data']['file'];
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'image' => $image
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
}
