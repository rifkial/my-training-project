<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    private $dataSiswaApi;
    private $rombelApi;
    private $kelasSiswaApi;

    public function __construct()
    {
        // $this->jurusanApi = new JurusanApi();
        $this->rombelApi = new RombelApi();
        $this->dataSiswaApi = new DataSiswaApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
    }

    public function index()
    {
        // dd(session('template'));
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        session()->put('title', 'Absensi Siswa');
        $template = session('template');
        if (session('role') == 'admin-kesiswaan') {
            $template = 'default';
        }
        return view('content.point.bimbingan_konseling.v_kesiswaan_siswa')->with(['rombel' => $rombel, 'template' => $template]);
    }

    public function get_siswa(Request $request)
    {
        // dd($request);
        $siswa = $this->dataSiswaApi->get_absensi($request['id_mapel'], $request['id_rombel'], session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            $no = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                    <td>' . $no++ . '</td>
                    <td>' . ucfirst($sw['nama']) . '</td>
                    <td>' . $sw['nis'] . '</td>
                    <td>' . $sw['jurusan'] . '</td>
                    <td>' . $sw['kelas'] . '</td>
                    <td>' . $sw['rombel'] . '</td>
                    <td>
                        <a href="javascript:void(0)" data-toggle="collapse"
                        data-target="#demo' . $sw['id'] . '"
                        class="accordion-toggle">
                        <span class="badge badge-pill badge-info fs-12 mr-1 my-auto">Lihat Absensi</span>
                    </td>
                    <td class="text-center">
                        <a href="javascript:void(0)" class="text-info detail" data-id="' . $sw['id_siswa'] . '"><i class="fas fa-info-circle"></i></a>
                        <a href="' . route('kesiswaan_siswa-cetak_absensi', [Help::encode($sw['id']), Help::encode($request['id_mapel']), Help::encode($request['id_rombel'])]) . '" target="_blank" class="text-purple" data-id="' . $sw['id_siswa'] . '"><i class="fas fa-download"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" class="hiddenRow">
                        <div class="accordian-body collapse"
                            id="demo' . $sw['id'] . '">
                            <table class="table table-striped widget-status-table">
                                <thead>
                                    <tr class="bg-purple text-white">
                                        <th class="text-center">Pertemuan
                                        </th>
                                        <th class="text-center">Tanggal
                                        </th>
                                        <th class="text-center">Kehadiran
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>';
                foreach ($sw['pertemuan'] as $pt) {
                    if ($pt['id_data_siswa'] != null) {
                        $kelas_absensi = "info";
                        if ($pt['kehadiran'] == "alfa") {
                            $kelas_absensi = "danger";
                        }
                        $html .= '<tr>
                                        <td class="text-center">
                                            ' . $pt['pertemuan'] . '
                                        </td>
                                        <td class="text-center">
                                            ' . Help::getTanggal($pt['tanggal']) . '
                                        </td>
                                        <td class="text-center">
                                            <span class="badge badge-' . $kelas_absensi . ' text-inverse">' . $pt['kehadiran'] . '</span>
                                        </td>
                                    </tr>';
                    } else {
                        $html .= '<tr class="bg-danger"><td colspan="3" class="text-center">Siswa belum pernah mengikuti pelajaran</td></tr>';
                    }
                }
                $html .= '</tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>';
            }
        } else {
            $html .= '<tr><td colspan="8" class="text-center">Data Siswa dengan filter tersebut kosong</td></tr>';
        }
        return response()->json($html);
    }

    public function cetak_absensi($id_kelas_siswa, $id_mapel, $id_rombel)
    {
        $siswa = $this->kelasSiswaApi->get_by_id(Help::decode($id_kelas_siswa));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $data_absensi = $this->dataSiswaApi->get_detail_absensi(Help::decode($id_kelas_siswa), Help::decode($id_mapel), Help::decode($id_rombel), session('id_tahun_ajar'));
        $absensi = $data_absensi['body']['data'];
        return view('content.point.bimbingan_konseling.v_cetak_absensi')->with(['absensi' => $absensi, 'siswa' => $siswa]);
    }
}
