<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\PelanggaranSiswaApi;
use App\ApiService\Kesiswaan\SanksiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HistoriPelanggaranController extends Controller
{
    private $pelanggaranApi;
    private $sanksiApi;

    public function __construct()
    {
        $this->pelanggaranApi = new PelanggaranSiswaApi();
        $this->sanksiApi = new SanksiApi();
    }

    public function index(Request $request)
    {
        // dd(session('role'));
        $routes = "kesiswaan_histori-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $pelanggarans = $this->pelanggaranApi->get_by_sekolah($search);
        // dd($pelanggarans);
        $pelanggaran = $pelanggarans['body']['data'];
        $meta = $pelanggarans['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        $template = session('template');
        if(session('role') == 'bk' || session('role') == 'admin-kesiswaan'){
            $template = 'default';
        }
        Session::put('title', 'History Pelanggaran');
        $sanksi = $this->sanksiApi->get_by_sekolah();
        // dd($sanksi);
        $sanksi = $sanksi['body']['data'];
        return view('content.point.bimbingan_konseling.v_histori_pelanggaran', compact('template', 'pelanggaran', 'pagination', 'routes', 'search', 'sanksi'));
    }

    public function delete(Request $request)
    {
        $delete = $this->pelanggaranApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
