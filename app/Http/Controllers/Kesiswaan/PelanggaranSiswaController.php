<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\KategoriPelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranApi;
use App\ApiService\Kesiswaan\PelanggaranSiswaApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class PelanggaranSiswaController extends Controller
{
    private $rombelApi;
    private $kelassiswaApi;
    private $pelanggaranApi;
    private $kategoriApi;
    private $pelApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->pelanggaranApi = new PelanggaranSiswaApi();
        $this->pelApi = new PelanggaranApi();
        $this->kategoriApi = new KategoriPelanggaranApi();
    }

    public function index()
    {
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $kategori = $this->kategoriApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        if (session("role") == "ortu") {
            $pelanggarans = $this->pelanggaranApi->get_by_kelas_siswa(session("id_kelas_siswa"));
            $pelanggaran = $pelanggarans['body']['data'];
            $siswa = $this->kelassiswaApi->get_by_id(session('id_kelas_siswa'));
            $siswa = $siswa['body']['data'];
            $siswa['jml_pelanggaran'] = count($pelanggaran);
            $siswa['total_point'] = $pelanggarans['body']['total'];
            return view('content.point.siswa.v_pelanggaran')->with(['template' => session('template'), 'pelanggaran' => $pelanggaran, 'siswa' => $siswa]);
        } elseif (session("role") == "admin") {
            Session::put('title', 'Sanksi Pelanggaran');
            $pelanggaran = $this->pelanggaranApi->get_sekolah();
            // dd($pelanggaran);
            $pelanggaran = $pelanggaran['body']['data'];
            return view('content.point.admin.v_pelanggaran_siswa')->with(['rombel' => $rombel]);
        }
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin' || session('role') == 'bk') {
            $template = 'default';
        }
        return view('content.point.bimbingan_konseling.pelanggaran_siswa.v_pelanggaran_siswa')->with(['rombel' => $rombel, 'template' => $template, 'kategori' => $kategori]);
    }

    public function get_siswa(Request $request)
    {
        // dd($request);
        $id_rombel = $request['id_rombel'];
        $nisn = $request['nisn'];
        if ($nisn == null && $id_rombel != null) {
            $post  = $this->kelassiswaApi->get_by_rombel($id_rombel, session('tahun'));
        // dd($post);
        } elseif ($id_rombel == null && $nisn != null) {
            $post  = $this->kelassiswaApi->get_by_nisn($nisn, session('tahun'));
        } else {
            $post  = $this->kelassiswaApi->get_by_rombel_nisn($id_rombel, $nisn, session('tahun'));
        }
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function get_pelanggaran(Request $request)
    {
        // dd($request);
        $id_kategori = $request['id_kategori'];
        $kategori = $this->kategoriApi->get_by_id($id_kategori);
        $result = $kategori['body']['data'];
        return response()->json($result);
    }

    public function next_input(Request $request)
    {
        // dd($request);
        if ($request['id_kelas_siswa']) {
            $siswa = $request['id_kelas_siswa'];
            $data = [];
            foreach ($siswa as $s) {
                $siswa = $this->kelassiswaApi->get_by_id($s);
                $siswa = $siswa['body']['data'];
                $data[] = $siswa;
            }
            $respon = array(
                'data' => $data,
                'status' => "berhasil",
            );
        } else {
            $respon = array(
                'data' => "silahkan pilih siswa terlebih dahulu",
                'status' => 'gagal'
            );
        }
        return response()->json($respon);
    }

    public function finish_step(Request $request)
    {
        // dd($request);
        $siswa = $request['id_kelas_siswa'];
        $pelanggaran = $request['id_pelanggaran'];
        $data_mentah = [];
        foreach ($pelanggaran as $p) {
            foreach ($siswa as $s) {
                $data_mentah[] = array(
                    'id_kelas_siswa' => (int)$s,
                    'id_pelanggaran' => (int)$p,
                    'tgl_pelanggaran' => date("Y-m-d")
                );
            }
        }

        foreach ($data_mentah as $dm) {
            $point = $this->pelApi->get_by_id($dm['id_pelanggaran']);
            $point = $point['body']['data']['point'];
            // dd($point);
            $data_insert = array(
                'id_kelas_siswa' => $dm['id_kelas_siswa'],
                'id_pelanggaran' => $dm['id_pelanggaran'],
                'tgl_pelanggaran' => $dm['tgl_pelanggaran'],
                'point' => $point,
                'id_sekolah' => session('id_sekolah'),
            );
            $result = $this->pelanggaranApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            $status = "berhasil";
            $icon = "success";
            $message = $result['body']['message'];
        } else {
            $status = "gagal";
            $icon = "error";
            $message = $result['body']['message'];
        }
        return response()->json([
            'status' => $status,
            'icon' => $icon,
            'message' => $message,
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $icon = "success";
            $message = $result['body']['message'];
            $status = "berhasil";
        } else {
            $icon = "error";
            $message = $result['body']['message'];
            $status = "gagal";
        }
        return response()->json([
            'icon' => $icon,
            'message' => $message,
            'status' => $status,
        ]);
    }

    public function edit(Request $request)
    {
        // dd($request);
        $id = $request['id_sanksi'];
        $sanksi = $this->sanksiApi->get_by_id($id);
        $sanksi = $sanksi['body']['data'];
        return response()->json($sanksi);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'nama' => $request['nama'],
            'bobot_dari' => $request['bobot_dari'],
            'bobot_sampai' => $request['bobot_sampai'],
            'sanksi' => $request['sanksi'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->sanksiApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            $icon = "success";
            $message = $result['body']['message'];
            $status = "berhasil";
        } else {
            $icon = "error";
            $message = $result['body']['message'];
            $status = "gagal";
        }
        return response()->json([
            'icon' => $icon,
            'message' => $message,
            'status' => $status,
        ]);
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->pelanggaranApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function getCustomFilterData(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->nama)) {
                $pelanggaran = $this->pelanggaranApi->get_filter_by_data($request->nama, $request->rombel);
                $result = $pelanggaran['body']['data'];
            } else {
                $pelanggaran = $this->pelanggaranApi->get_by_data();
                $result = $pelanggaran['body']['data'];
            }
            // dd($result);
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id_kelas_siswa'] . '" class="btn btn-success btn-sm restore-' . $data['id_kelas_siswa'] . '" onclick="detail(' . $data['id_kelas_siswa'] . ')"><i class="fa fa-eye"></i> Lihat Detail</a>';
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function getDetail(Request $request)
    {
        if (request()->ajax()) {
            $pelanggaran = $this->pelanggaranApi->get_by_kelas_siswa($request['kelas_siswa']);
            $result = $pelanggaran['body']['data'];
            // dd($result);
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    return '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="detail(' . $data['id'] . ')"><i class="fa fa-eye"></i> Lihat Detail</a>';
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function get_filter_siswa(Request $request)
    {
        if (request()->ajax()) {
            if ($request->filter_siswa == "terbanyak") {
                $pelanggaran = $this->pelanggaranApi->get_by_pelanggaran_terbanyak();
                $result = $pelanggaran['body']['data'];
            } elseif ($request->filter_siswa == "berat") {
                $pelanggaran = $this->pelanggaranApi->get_by_pelanggaran_terbanyak();
                $result = $pelanggaran['body']['data'];
            } else {
                $pelanggaran = $this->pelanggaranApi->pelanggaran_warning();
                $result = $pelanggaran['body']['data'];
            }

            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="delete" data-id="' . $data['id_kelas_siswa'] . '" class="delete-' . $data['id_kelas_siswa'] . ' btn btn-danger btn-sm" onclick="detail(' . $data['id_kelas_siswa'] . ')"><i class="fa fa-eye"></i> Lihat Detail</button>';
                    if ($data['wa_ortu'] != null) {
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<a href="' . $data['wa_ortu'] . '" data-toggle="tooltip" class="btn btn-success btn-sm" target="_blank"><i class="fa fa-whatsapp"></i> Hubungi Wali</a>';
                    }
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    public function get_pelanggaran_terbanyak()
    {
        $peringkat_pelanggaran = $this->pelanggaranApi->peringkat_pelanggaran_terbanyak();
        $peringkat_pelanggaran = $peringkat_pelanggaran['body']['data'];
        $pelanggaran_kelas = $this->pelanggaranApi->peringkat_pelanggaran_kelas();
        $pelanggaran_kelas = $pelanggaran_kelas['body']['data'];
        $bar = [];
        if (!empty($pelanggaran_kelas)) {
            foreach ($pelanggaran_kelas as $pk) {
                $bar[] = array(
                    'rombel' => $pk['nama'],
                    'jumlah' => $pk['total_pelanggaran'],
                );
            }
        }

        $donut = [];
        if (!empty($peringkat_pelanggaran)) {
            foreach ($peringkat_pelanggaran as $pe) {
                $donut[] = array(
                    'label' => $pe['nama'],
                    'value' => $pe['jumlah'],
                );
            }
        }
        $output = array(
            'donut' => $donut,
            'bar' => $bar,
        );
        return response()->json($output);
    }

    public function get_pelanggaran_dashboard_siswa()
    {
        $all_pelanggaran = $this->pelanggaranApi->get_by_kelas_siswa(session('id_kelas_siswa'));
        $all_pelanggaran = $all_pelanggaran['body']['data'];
        $sering = $this->pelanggaranApi->get_by_sering_dilakukan(session('id_kelas_siswa'));
        $sering = $sering['body']['data'];
        $bar = [];
        if (!empty($sering)) {
            foreach ($sering as $sr) {
                $bar[] = array(
                    'pelanggaran' => $sr['id_pelanggaran'],
                    'jumlah' => $sr['jumlah'],
                );
            }
        }

        $donut = [];
        if (!empty($all_pelanggaran)) {
            foreach ($all_pelanggaran as $ap) {
                $donut[] = array(
                    'label' => $ap['pelanggaran'],
                    'value' => $ap['point'],
                );
            }
        }
        $output = array(
            'donut' => $donut,
            'bar' => $bar,
        );
        return response()->json($output);
    }

    public function update_sanksi(Request $request)
    {
        // dd($request);
        if ($request['reset']) {
            $reset_point = 'on';
        } else {
            $reset_point = 'off';
        }

        $data = array(
            'id' => $request['id'],
            'id_sanksi_pelanggaran' => $request['sanksi'],
            'reset_point' => $reset_point
        );

        $result = $this->pelanggaranApi->update_sanksi(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
