<?php

namespace App\Http\Controllers\Kesiswaan;

use App\ApiService\Kesiswaan\ProgramKerjaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class ProgramKerjaController extends Controller
{
    private $kerjaApi;
    private $tahunApi;

    public function __construct()
    {
        $this->kerjaApi = new ProgramKerjaApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        $kerja = $this->kerjaApi->get_by_sekolah();
        // dd($kerja);
        $kerja = $kerja['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        session()->put('title', "Program Kerja");
        $template = session('template');
        if (session('role') == 'admin-kesiswaan') {
            $template = 'default';
        }
        return view('content.point.bimbingan_konseling.v_program_kerja')->with(['template' => $template, 'kerja' => $kerja, 'tahun' => $tahun]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'judul' => $request['judul'],
            'id_tahun_ajar' => $request['id_tahun_ajar'],
            'id_sekolah' => session('id_sekolah'),
        );
        $file = $request->file('image');
        $file1 = $request->file('image1');
        $data_file = [];
        $data_file[] = array(
            $file, $file1
        );

        if ($file && empty($file1)) {
            $namaFile = $file->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/program_kerja/";
            Help::check_and_make_dir($basePath);
            $file->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->kerjaApi->create_file(json_encode($data));
            File::delete($path);
        } elseif ($file1 && empty($file)) {
            $namaFile = $file1->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/program_kerja/";
            Help::check_and_make_dir($basePath);
            $file1->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->kerjaApi->create_file1(json_encode($data));
            File::delete($path);
        } elseif ($file && $file1) {
            foreach ($data_file[0] as $fl) {
                // dd($fl);
                $namaFile = $fl->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/program_kerja/";
                Help::check_and_make_dir($basePath);
                $fl->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data['path'][] = $path;
            }
            $result = $this->kerjaApi->create_files(json_encode($data));
            // dd($result)
            ;
            foreach ($data['path'] as $del_path) {
                File::delete($del_path);
            }
        } else {
            $result = $this->kerjaApi->create(json_encode($data));
        }
        // dd($data);
        if ($result['code'] == 200) {
            $kerja = $this->data_kerja();
            return response()->json([
                    'icon' => "success",
                    'message' => $result['body']['message'],
                    'status' => 'berhasil',
                    'kerja' => $kerja
                ]);
        } else {
            return response()->json([
                    'icon' => "error",
                    'message' => $result['body']['message'],
                    'status' => 'gagal'
                ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'judul' => $request['judul'],
            'id' => $request['id'],
            'id_tahun_ajar' => $request['id_tahun_ajar'],
            'id_sekolah' => session('id_sekolah'),
        );
        $file = $request->file('image');
        $file1 = $request->file('image1');
        $data_file = [];
        $data_file[] = array(
            $file, $file1
        );

        if ($file && empty($file1)) {
            $namaFile = $file->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/program_kerja/";
            Help::check_and_make_dir($basePath);
            $file->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->kerjaApi->create_file(json_encode($data));
            File::delete($path);
        } elseif ($file1 && empty($file)) {
            $namaFile = $file1->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/program_kerja/";
            Help::check_and_make_dir($basePath);
            $file1->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->kerjaApi->create_file1(json_encode($data));
            File::delete($path);
        } elseif ($file && $file1) {
            foreach ($data_file[0] as $fl) {
                // dd($fl);
                $namaFile = $fl->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/program_kerja/";
                Help::check_and_make_dir($basePath);
                $fl->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data['path'][] = $path;
            }
            $result = $this->kerjaApi->create_files(json_encode($data));
            // dd($result)
            ;
            foreach ($data['path'] as $del_path) {
                File::delete($del_path);
            }
        } else {
            $result = $this->kerjaApi->create(json_encode($data));
        }
        // dd($data);
        if ($result['code'] == 200) {
            $kerja = $this->data_kerja();
            return response()->json([
                    'icon' => "success",
                    'message' => $result['body']['message'],
                    'status' => 'berhasil',
                    'kerja' => $kerja
                ]);
        } else {
            return response()->json([
                    'icon' => "error",
                    'message' => $result['body']['message'],
                    'status' => 'gagal'
                ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->kerjaApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $kerja = $this->data_kerja();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'kerja' => $kerja
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    public function detail(Request $request)
    {
        // dd($request);
        $kerja = $this->kerjaApi->get_by_id($request['id']);
        // dd($kerja);
        $kerja = $kerja['body']['data'];
        return response()->json($kerja);
    }

    private function data_kerja()
    {
        $kerja = $this->kerjaApi->get_by_sekolah();
        // dd($kerja);
        $kerja = $kerja['body']['data'];
        $html = '';
        if (!empty($kerja)) {
            $no = 1;
            foreach ($kerja as $kj) {


                $html .= '<tr>
                <td class="text-center">'.$no++.'</td>
                <td class="text-center">'.$kj['judul'].'</td>
                <td class="text-center">'.$kj['tahun_ajaran'].'</td>
                <td class="text-center">';
                if ($kj['file'] != null) {
                   $html .= '<a href="'.route('kesiswaan_program_kerja-download', ['file', Help::encode($kj['id'])]).'"
                             target="_blank" class="btn btn-success btn-sm"><i class="fas fa-download"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)"  onclick="alert(`Program Kerja tidak mempunyai file yang dapat diunduh`)" class="btn btn-dark btn-sm"><i class="fas fa-times-circle"></i></a>';
                }
                $html .= '
                </td>
                <td class="text-center">';
                if ($kj['pelaksanaan'] != null) {
                    $html .= ' <a href="'.route('kesiswaan_program_kerja-download', ['pelaksanaan', Help::encode($kj['id'])]).'"
                    target="_blank" class="btn btn-info btn-sm"><i class="fas fa-download"></i></a>';
                } else {
                   $html .= ' <a href="javascript:void(0)" onclick="alert(`Ra ono file sing biso diunduh`)" class="btn btn-dark btn-sm"><i
                   class="fas fa-times-circle"></i></a>';
                }
                $html .= '</td>';
                if (session('role') != 'supervisor') {
                    $html .= '<td class="text-center">
                    <a class="btn btn-sm btn-info edit" href="javascript:void(0)"
                        data-id="'.$kj['id'].'"><i class="fas fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger delete" href="javascript:void(0)"
                        data-id="'.$kj['id'].'"><i class="fas fa-trash-alt"></i></a>
                </td>';
                }

            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function download($slug, $id)
    {
        $bursa_kerja  = $this->kerjaApi->get_by_id(Help::decode(last(request()->segments())));
        // dd($bursa_kerja);
        $result = $bursa_kerja['body']['data'];
        if($slug == 'file'){
            $nama_file = explode('/', $result['file']);
        }else{
            $nama_file = explode('/', $result['pelaksanaan']);
        }
        // dd($nama_file);
        $fileName = end($nama_file);
        // dd($nama);
        if($slug == 'file'){
            $fileSource = $result['file'];
        }else{
            $fileSource = $result['pelaksanaan'];
        }
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        // dd($pathToFile);
        $getContent = $this->curl_get_file_contents($fileSource);
        // dd($getContent);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
