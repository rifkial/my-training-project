<?php

namespace App\Http\Controllers\Ppdb\Peserta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;

class DokumenSupController extends Controller
{

    protected $AdminPpdbApi;

     /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Peserta | Dokumen Pendukung');
        return view('ppdb.components.Peserta.v_filedoc');
    }


        /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request){
        $detail_sample = $this->AdminPpdbApi->get_data_dokumen();
        //dd($detail_sample);
        $result = $detail_sample['body']['data'];
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download doc" data-gambar="'.$data['file'].'"   data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download doc" data-gambar="'.$data['file'].'"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download doc" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
             $table->editColumn('nama_peserta', function ($row) {
                return $row['peserta'];
            });
            $table->editColumn('file', function ($row) {
                $infoPath = pathinfo($row['file']);
                return $infoPath['extension'];
            });
            $table->editColumn('status', function ($row) {
               if($row['status'] == 'Aktif'){
                   return '<i class="fa fa-eye"></i>';
               }else if($row['status']=='Tidak Aktif'){
                   return '<i class="fa fa-eye-slash"></i>';
               }
            });
            $table->rawColumns(['action', 'file','status','nama_peserta']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

          /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_peserta(Request $request){
        $detail_sample = $this->AdminPpdbApi->get_data_peserta();

        $result = $detail_sample['body']['data'];

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download pilih" data-id="' . $data['id'] . '"><i class="fa fa-address-book">Pilih</i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
             $table->editColumn('nama_peserta', function ($row) {
                return $row['nama'];
            });
            $table->editColumn('status', function ($row) {
               if($row['status'] == '1'){
                   return '<i class="fa fa-eye"></i>';
               }else if($row['status']=='0'){
                   return '<i class="fa fa-eye-slash"></i>';
               }
            });
            $table->rawColumns(['action', 'status','nama_peserta']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::put('title', 'Peserta | Tambah Dokumen Pendukung');
        return view('ppdb.components.Peserta.v_filedoc_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validate form save or update
         $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
            'id_peserta'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'id_peserta.required'=>'Form id peserta tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);
        $data = array();
        $data['nama']      = $request->nama;
        $data['id_peserta'] = $request->id_peserta;
        $data['id_sekolah'] = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_dokumenfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->post_data_dokumen(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return redirect()->route('doc-support')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_detail_dokumen_byid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_detail_dokumen_byid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //validate form save or update
         $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
            'id_peserta'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'id_peserta.required'=>'Form id pesrta tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);
        $data = array();
        $data['id']        = $id;
        $data['nama']      = $request->nama;
        $data['id_peserta'] = $request->id_peserta;
        $data['id_sekolah'] = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_data_dokumenfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->update_data_dokumen(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response_show_detail = $this->AdminPpdbApi->delete_data_dokumen($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

        /**
     * restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteForce($id)
    {
        $response_show_detail = $this->AdminPpdbApi->deletepermanent_data_dokumen($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }
}
