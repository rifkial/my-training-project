<?php

namespace App\Http\Controllers\Ppdb;

use App\ApiService\Master\SekolahApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\PublicPpdbApi;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class PublicController extends Controller
{

    protected $PublicPpdbApi;
    private $sekolahApi;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->PublicPpdbApi = new PublicPpdbApi();
        $this->sekolahApi = new SekolahApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domain = request()->getHost();
        $domain = str_replace('www.', '', $domain);
        $sekolah = $this->sekolahApi->search_by_domain($domain);
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        if ($sekolah != null) {
            session()->put('id_sekolah', $sekolah['id']);
        }
        Session::put('title', 'PPDB Online');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $sambutan    = $this->PublicPpdbApi->get_data_setting_sambutan($id);
        $sambutan_parse = $sambutan['body']['data'] ?? '';
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        return view('ppdb.components.public.v_welcome')->with(['banner' => $banner_data, 'sambutan' => $sambutan_parse]);
    }

    /**
     * Display footer
     */

    public function setting_sekolah($id)
    {
        $setting = $this->PublicPpdbApi->get_setting_sekolah($id);
        $settingppdb = $this->PublicPpdbApi->get_data_setting_ppdbbyid($id);

        if (empty($settingppdb)) {

            return ' Silahkan buat setting ppdb';
        } else {
            $ppdb_set    = $settingppdb['body']['data'] ?? '';
            $parse = $setting['body']['data'] ?? '';
            if (empty($parse) || $parse == '' && empty($ppdb_set) || $ppdb_set == '') {
                return redirect()->route('home');
            } else {
                Session::put('sekolah', $parse['nama']);
                Session::put('logo', $ppdb_set['logo1']);
                Session::put('selamat_datang', $ppdb_set['head3'] . '  ' . $ppdb_set['head2']);
                Session::put('footer', $ppdb_set['copyright']);
                Session::put('whatapps', $ppdb_set['whatsapp']);
                Session::put('telepone_cs', $ppdb_set['telepon']);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq()
    {
        Session::put('title', 'FAQ');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $faq    = $this->PublicPpdbApi->get_data_faq($id);
        $faq_data = $faq['body']['data'] ?? '';
        $banner_data = $banner['body']['data'] ?? '';
        return view('ppdb.components.public.v_faq')->with(['banner' => $banner_data, 'faq' => $faq_data]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pengumuman()
    {
        Session::put('title', 'PENGUMUMAN');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        $feed = $this->PublicPpdbApi->feeds($id);
        $feed_data = $this->paginate($feed['body']['data'])->setPath('pengumuman');
        return view('ppdb.components.public.v_pengumuman')->with(['banner' => $banner_data, 'feed' => $feed_data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        Session::put('title', 'DOWNLOAD');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        return view('ppdb.components.public.v_download')->with(['banner' => $banner_data]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rule_ppdb()
    {
        Session::put('title', 'ALUR PPDB');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $data = $this->PublicPpdbApi->get_detail_alur($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        $rules = $data['body']['data'] ?? '';
        return view('ppdb.components.public.v_alur')->with(['rows' => $rules, 'banner' => $banner_data]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function syarat_ppdb()
    {
        Session::put('title', 'SYARAT PPDB');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $syarat = $this->PublicPpdbApi->get_detail_syarat($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        $syarat_data = $syarat['body']['data'] ?? '';
        return view('ppdb.components.public.v_syarat')->with(['syarat' => $syarat_data, 'banner' => $banner_data]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function panduan_ppdb()
    {
        Session::put('title', 'PANDUAN PPDB');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $panduan = $this->PublicPpdbApi->get_detail_panduan($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        $panduan_data = $panduan['body']['data'] ?? '';
        return view('ppdb.components.public.v_panduan')->with(['panduan' => $panduan_data, 'banner' => $banner_data]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kegiatan_ppdb()
    {
        Session::put('title', 'KEGIATAN PPDB');
        Session::put('sidebar', '');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        return view('ppdb.components.public.v_kegiatan')->with(['banner' => $banner_data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_data_jadwal(Request $request)
    {
        $detail_jadwal = $this->PublicPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result = $detail_jadwal['body']['data'] ?? array();
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_file_download(Request $request)
    {
        $detail_file = $this->PublicPpdbApi->file_download_byid(session('id_sekolah'));
        $result = $detail_file['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addColumn('file', function ($data) {
                $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                $button .= '<a class="btn btn-info" href="' . route('force_download', ['id' => $data['file']]) . '"><i class="fa fa-download"></i></a>';
                $button .= '</div>';
                return $button;
            });
            $table->rawColumns(['file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_brosur_download(Request $request)
    {
        $detail_file = $this->PublicPpdbApi->brosur_download_byid(session('id_sekolah'));
        $result = $detail_file['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addColumn('file', function ($data) {
                $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                $button .= '<a class="btn btn-info" href="' . route('force_download', ['id' => $data['file']]) . '"><i class="fa fa-download"></i></a>';
                $button .= '</div>';
                return $button;
            });
            $table->rawColumns(['file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** force download file
     ** @param $id
     */
    public function getdownload($id)
    {
        $download = $this->force_download_curl($id);
        if ($download['code'] == '200') {
            $file = public_path($download['filename']);
            if (!empty($file)) {
                $headers = array(
                    'Content-Type:' . mime_content_type($file),
                );
                return response()->download($file, $download['filename'], $headers)->deleteFileAfterSend(true);
            } else {
                return back()->with('error', 'download Gagal');
            }
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /**
     * curl file download
     * @param $url
     */

    private function force_download_curl($url)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        if (!empty($token)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token,
            ));
        }
        $saveTo = basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_ppdb()
    {
        Session::put('title', 'PENDAFTARAN PPDB');
        Session::put('sidebar', 'show');
        $id = session('id_sekolah');
        $this->setting_sekolah($id);
        $banner = $this->PublicPpdbApi->get_detail_banner($id);
        $banner_data = $banner['body']['data'] ?? '';
        return view('ppdb.components.public.v_daftar')->with(['banner' => $banner_data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store Register a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nisn' => 'required',
            'email' => 'required|email',
            'jenkel' => 'required',
            'password' => 'required',
            'telepon' => 'required '
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'nisn.required' => 'Form nisn tidak boleh kosong!',
            'email.required' => 'Form email tidak boleh kosong!',
            'jenkel.required' => 'Form jenis kelamin harus pilih salah satu Laki-laki / Perempuan!',
            'password.required' => 'Form password tidak boleh kosong!',
        ]);

        $data = array();
        $data['nama']           = $request->nama;
        $data['jenkel']         = $request->jenkel;
        $data['nisn']           = $request->nisn;
        $data['email']          = $request->email;
        $data['first_password'] = $request->password;
        $data['telepon']        = $request->telepon;
        $data['id_sekolah']     = session('id_sekolah');

        $post_reg = $this->PublicPpdbApi->post_register_peserta(json_encode($data));

        if ($post_reg['code'] == '200') {
            return back()->with('success', $post_reg['body']['message'] . ' silahkan klik login di pojok kanan atas untuk proses langkah selanjutnya.');
        } else {
            return back()->with('error', $post_reg['body']['message']);
        }
    }

    /**
     * Display view update info pengumuman  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_view($id)
    {
        $update_view = $this->PublicPpdbApi->view_feeds($id);
        if ($update_view['code'] == '200') {
            return response()->json(
                [
                    'message' => $update_view['body']['message'],
                    'data' => $update_view['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $update_view['body']['message'],
                    'data' => $update_view['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display view update info pengumuman  the specified resource.
     *
     * @param  int  $id
     * @param string $slug
     * @return \Illuminate\Http\Response
     */

    public function detail_feed($id, $slug)
    {
        Session::put('title', 'Detail Pengumuman');
        Session::put('sidebar', '');
        $id_sekolah = session('id_sekolah');
        $this->setting_sekolah($id_sekolah);
        $banner = $this->PublicPpdbApi->get_detail_banner($id_sekolah);
        $banner_data = $banner['body']['data'];
        $detail_feed = $this->PublicPpdbApi->feeds_byid($id);
        $feed_data    = $detail_feed['body']['data'];
        if ($banner['code'] == '200' && $detail_feed['code'] == '200') {
            return view('ppdb.components.public.v_detail_pengumuman')->with(['banner' => $banner_data, 'list' => $feed_data]);
        } else {
            return back()->with('error', $detail_feed['body']['message']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
}
