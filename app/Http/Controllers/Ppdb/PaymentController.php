<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;
use Pusher\Pusher;

class PaymentController extends Controller
{

    protected $AdminPpdbApi;


    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Pembayaran Pending / Menunggu Konfirmasi');
        return view('ppdb.components.Payment.v_payment_pending');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paid()
    {
        Session::put('title', 'Pembayaran Diterima');
        return view('ppdb.components.Payment.v_payment_paid');
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm()
    {
        Session::put('title', 'Pembayaran konfirmasi');
        return view('ppdb.components.Payment.v_payment_confirm');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reject()
    {
        Session::put('title', 'Pembayaran Batal / Ditolak');
        return view('ppdb.components.Payment.v_payment_reject');
    }

    /**
     * Ajax Data payment pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_payment_pending(Request $request)
    {
        $payment_pending = $this->AdminPpdbApi->get_payment_with_pending();
        $result          = $payment_pending['body']['data'] ?? array();

        //dd($payment_pending);

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show payment" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-danger btn-sm reject payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-close"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nominal', function ($row) {
                return number_format($row['tagihan'], 0);
            });
            $table->editColumn('catatan', function ($row) {
                return '-';
            });
            $table->rawColumns(['action', 'catatan', 'bukti']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data payment pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_payment_paid(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_pending = $this->AdminPpdbApi->get_payment_with_paid($pecah_data[0]);
            $result          = $payment_pending['body']['data'] ?? array();
        } else {
            $result          = array();
        }
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show payment" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });
            $table->editColumn('catatan', function ($row) {
                return $row['keterangan'];
            });

            $table->editColumn('status_pembayaran', function ($row) {
                switch($row['status_pembayaran']){
                    case'Konfirmasi':
                        return 'Diterima';
                    break;
                    default:
                    return $row['status_pembayaran'];
                    break;
                }
            });

            $table->editColumn('bukti', function ($row) {
                return '<img src="' . $row['bukti'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'catatan', 'bukti']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

     /**
     * Ajax Data payment pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_payment_confirm(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_pending = $this->AdminPpdbApi->get_payment_with_confirm($pecah_data[0]);
            $result          = $payment_pending['body']['data'] ?? array();
        } else {
            $result          = array();
        }
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        //dd($payment_pending);

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show payment" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    //$button .= '<a  class="btn btn-danger btn-sm reject payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-close"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });
            $table->editColumn('catatan', function ($row) {
                return '-';
            });
            $table->editColumn('bukti', function ($row) {
                return '<img src="' . $row['bukti'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'catatan', 'bukti']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data payment pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_payment_reject(Request $request)
    {
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $payment_pending = $this->AdminPpdbApi->get_payment_with_reject($pecah_data[0]);
            $result          = $payment_pending['body']['data'];
        } else {
            $result          = array();
        }
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show payment" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    //$button .= '<a  class="btn btn-warning btn-sm edit payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    //$button .= '<a  class="btn btn-danger btn-sm reject payment"   data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-close"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });
            $table->editColumn('catatan', function ($row) {
                return '-';
            });
            $table->editColumn('bukti', function ($row) {
                return '<img src="' . $row['bukti'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'catatan', 'bukti']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** force download file
     ** @param $id
     */
    public function getdownload($id)
    {
        $download = $this->force_download_curl($id);
        if ($download['code'] == '200') {
            $file = public_path($download['filename']);
            $headers = array(
                'Content-Type:' . mime_content_type($file),
            );
            return response()->download($file, $download['filename'], $headers)->deleteFileAfterSend(true);
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /**
     * curl file download
     * @param $url
     */

    private function force_download_curl($url)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $saveTo = basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_payment_by_id($id);

        //dd($response_show_detail);

        $bukti_pembayaran = $response_show_detail['body']['data']['bukti'] ?? '';
        if($bukti_pembayaran != ''){
            $download_file    =  '<a class="btn btn-info" href="' . route('force_download_bukti', ['id' => $response_show_detail['body']['data']['bukti']]) . '"><i class="fa fa-download"></i></a>';
        }else{
            $download_file    ='';
        }
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                    'download' =>$download_file
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                    'download' => ''
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_payment_by_id($id);
        $bukti_pembayaran = $response_show_detail['body']['data']['bukti'] ?? '';
        if($bukti_pembayaran != ''){
            $download_file    =  '<a class="btn btn-info" href="' . route('force_download_bukti', ['id' => $response_show_detail['body']['data']['bukti']]) . '"><i class="fa fa-download"></i></a>';
        }else{
            $download_file    ='';
        }
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                    'download' => $download_file
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                    'download' => ''
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();
        $data['id'] = $id;
        $data['status_pembayaran']  = $request->status_pembayaran;
        $data['keterangan']         = $request->catatan;
        $response_update = $this->AdminPpdbApi->update_status_payment(json_encode($data));
        //dd($response_update);
        if ($response_update['code'] == '200') {
            $message = "Pembayaran Pendaftaran  telah ".$response_update['body']['data']['status_pembayaran']." oleh Administrator";
            $notif = $this->config_pusher();
            $notif->trigger('my-channel', 'my_event', $message);
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** setting pusher  */
    public function config_pusher()
    {
        $app_id = env('PUSHER_APP_ID');
        $app_secret = env('PUSHER_APP_SECRET');
        $app_key    = env('PUSHER_APP_KEY');
        $app_cluster = env('PUSHER_APP_CLUSTER');
        $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
        return $pusher;
    }
}
