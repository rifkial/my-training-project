<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\PublicPpdbApi;
use App\ApiService\Ppdb\Akun\PesertaPpdbApi;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use XcS\XcTools;
use Pusher\Pusher;

class UserController extends Controller
{

    protected $PublicPpdbApi;
    protected $PesertaPpdbApi;
    protected $AdminPpdbApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->PublicPpdbApi = new PublicPpdbApi();
        $this->PesertaPpdbApi = new PesertaPpdbApi();
        $this->AdminPpdbApi = new AdminPpdbApi();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'User PPDB Online ');
        $check_tagihan = $this->check_pembayaran();
        $check_access  = $this->check_access_page();
        $profil_user = $this->PesertaPpdbApi->get_info_profil_user();
        $profil_data  = $profil_user['body']['data'] ?? array();

        if ($check_tagihan['status'] == 'Aktif' && $check_tagihan['pembayaran'] == '1' && $check_access == false ) {
            $tagihan = $this->PesertaPpdbApi->get_tagihan_peserta(session('id'));
            $result_tagihan = $tagihan['body']['data'] ?? array();
        }else{
            $result_tagihan = array();
        }

        $sambutan    = $this->PublicPpdbApi->get_data_setting_sambutan(session('id_sekolah'));
        $sambutan_parse = $sambutan['body']['data'] ?? '';

        $params = [
            'tagihan' => $result_tagihan,
            'check_tagihan' => $check_tagihan,
            'profil_data' => $profil_data,
            'check_access'=> $check_access,
            'sambutan'    => $sambutan_parse
        ];

        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        return view('ppdb.components.v_page_user')->with($params);
    }

    /**
     * pengaturan pembayaran atau tagihan
     */

    public function check_pembayaran()
    {
        $setting_pembayaran = $this->AdminPpdbApi->get_data_setting_pembayaran(session('id_sekolah'));
        return $setting_pembayaran['body']['data'] ?? array();
    }

    /**
     * check pembayaran
     */

    public function check_access_page(){
        $pembayaran = $this->PesertaPpdbApi->get_accses_link(session('id'));
        return $pembayaran['body']['data'] ?? array();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function konfirmasi_pembayaran()
    {
        Session::put('title', 'Konfirmasi Pembayaran ');

        $check_tagihan = $this->check_pembayaran();
        $check_access  = $this->check_access_page();
        if ($check_tagihan['status'] == 'Aktif' && $check_tagihan['pembayaran'] == '1') {
            $tagihan = $this->PesertaPpdbApi->get_tagihan_peserta(session('id'));
            $result_tagihan = $tagihan['body']['data'] ?? array();
        }else{
            $result_tagihan = array();
        }

        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        return view('ppdb.components.v_konfirmasi_pembayaran')->with(['tagihan' => $result_tagihan,'check_tagihan' => $check_tagihan,'check_access'=>$check_access]);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function history_payment(){
        Session::put('title', 'Tagihan Pembayaran');
        $tagihan = $this->PesertaPpdbApi->get_tagihan_peserta(session('id'));
        $result_tagihan = $tagihan['body']['data'] ?? array();

        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        return view('ppdb.components.v_history_pembayaran')->with(['tagihan' => $result_tagihan]);
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pengumuman()
    {
        Session::put('title', 'PENGUMUMAN');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $id = session('id_sekolah');
        $feed = $this->PublicPpdbApi->feeds($id);
        $feed_data = $this->paginate($feed['body']['data'])->setPath('pengumuman');
        return view('ppdb.components.v_page_userpengumuman')->with(['feed' => $feed_data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function alur()
    {
        Session::put('title', 'Alur Pendaftaran');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $id = session('id_sekolah');
        $alur = $this->PublicPpdbApi->get_detail_alur($id);
        return view('ppdb.components.v_page_user_alur')->with(['rows' => $alur['body']['data']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function syarat()
    {
        Session::put('title', 'Syarat Pendaftaran');

        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $id = session('id_sekolah');
        $alur = $this->PublicPpdbApi->get_detail_syarat($id);
        return view('ppdb.components.v_page_user_syarat')->with(['rows' => $alur['body']['data']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function panduan()
    {
        Session::put('title', 'Panduan Pendaftaran');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $id = session('id_sekolah');
        $alur = $this->PublicPpdbApi->get_detail_panduan($id);
        return view('ppdb.components.v_page_user_panduan')->with(['rows' => $alur['body']['data']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function jadwal()
    {
        Session::put('title', 'Jadwal Kegiatan Pendaftaran');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        return view('ppdb.components.v_page_user_jadwal');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Faq()
    {
        Session::put('title', 'FAQ');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $id = session('id_sekolah');
        $faq    = $this->PublicPpdbApi->get_data_faq($id);
        $faq_data = $faq['body']['data'];
        return view('ppdb.components.v_page_user_faq')->with(['faq' => $faq_data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function DownloadFile()
    {
        Session::put('title', 'Download File ');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        return view('ppdb.components.v_page_user_downloadfile');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function DownloadBrosur()
    {
        Session::put('title', 'Download Brosur ');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        return view('ppdb.components.v_page_user_downloadbrosur');
    }

    /**
     * Display view update info pengumuman  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_view($id)
    {
        $update_view = $this->PublicPpdbApi->view_feeds($id);
        if ($update_view['code'] == '200') {
            return response()->json(
                [
                    'message' => $update_view['body']['message'],
                    'data' => $update_view['body']['data'],
                    'info' => 'success'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $update_view['body']['message'],
                    'data' => $update_view['body']['data'],
                    'info' => 'error'
                ]
            );
        }
    }

    /**
     * Display view update info pengumuman  the specified resource.
     *
     * @param  int  $id
     * @param string $slug
     * @return \Illuminate\Http\Response
     */

    public function detail_feed($id, $slug)
    {
        Session::put('title', 'Detail Pengumuman');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $detail_feed = $this->PublicPpdbApi->feeds_byid($id);
        $feed_data    = $detail_feed['body']['data'];
        if ($detail_feed['code'] == '200') {
            return view('ppdb.components.v_detail_pengumuman_user')->with(['list' => $feed_data]);
        } else {
            return back()->with('error', $detail_feed['body']['message']);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_data_jadwal(Request $request)
    {
        $detail_jadwal = $this->PublicPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result = $detail_jadwal['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_file_download(Request $request)
    {
        $detail_file = $this->PublicPpdbApi->file_download_byid(session('id_sekolah'));
        $result = $detail_file['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addColumn('file', function ($data) {
                $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                $button .= '<a class="btn btn-info" href="' . route('force_download', ['id' => $data['file']]) . '"><i class="fa fa-download"></i></a>';
                $button .= '</div>';
                return $button;
            });
            $table->rawColumns(['file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajax_brosur_download(Request $request)
    {
        $detail_file = $this->PublicPpdbApi->brosur_download_byid(session('id_sekolah'));
        $result = $detail_file['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result);
            $table->addColumn('file', function ($data) {
                $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                $button .= '<a class="btn btn-info" href="' . route('force_download', ['id' => $data['file']]) . '"><i class="fa fa-download"></i></a>';
                $button .= '</div>';
                return $button;
            });
            $table->rawColumns(['file']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** force download file
     ** @param $id
     */
    public function getdownload($id)
    {
        $download = $this->force_download_curl($id);
        if ($download['code'] == '200') {
            $file = public_path($download['filename']);
            if(!empty($file)){
                $headers = array(
                    'Content-Type:' . mime_content_type($file),
                );
                return response()->download($file, $download['filename'], $headers)->deleteFileAfterSend(true);
            }else{
                return back()->with('error', 'download Gagal');
            }
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /**
     * curl file download
     * @param $url
     */

    private function force_download_curl($url)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        if(!empty($token)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token,
            ));
        }
        $saveTo = basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }

     /**
     * curl file download
     * @param $url
     */

    private function force_download_response($url,$name_status)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $saveTo = $name_status;
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);

        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // confirmasi pembayaran post

        $request->validate([
            'no_rekening' => 'required',
            'tujuan_bank' => 'required',
            'asal_bank' => 'required',
            'tanggal_kirim'=>'required',
            'nominal'=>'required',
            'nama_pengirim' =>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ], [
            'no_rekening.required' => 'Form  Nomor rekening  tidak boleh kosong!',
            'tujuan_bank.required' => 'Form  Nama Bank Tujuan  tidak boleh kosong!',
            'asal_bank.required' => 'Form Nama Bank Asal Pengirim tidak boleh kosong!',
            'tanggal_kirim.required'=>'Form Tanggal Kirim tidak boleh kosong!',
            'nama_pengirim.required'=>'Form Nama Pengirim tidak boleh kosong!',
            'nominal.required'=>'Form nominal uang yang dikirim tidak boleh kosong!',
            'image.required' =>' File Bukti transfer pembayaran tidak boleh kosong',
            'image.mimes' =>' File hanya boleh berextensi jpeg,png,jpg,svg',
            'image.max' =>' file maksimal 2048'
        ]);

        $data = array();
        $data['id_peserta'] = $request->id_peserta;
        $data['nominal']    = $request->nominal;
        $data['tujuan_bank'] = $request->tujuan_bank;
        $data['atas_nama']   = $request->nama_pengirim;
        $data['asal_bank']   = $request->asal_bank;
        $data['no_rekening'] = $request->no_rekening;
        $data['tgl_bayar']   = $request->tanggal_kirim;
        $data['tahun_ajaran'] = $request->tahun_ajaran;
        $data['id_sekolah']   = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bukti_transfer/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->PesertaPpdbApi->post_konfirm_pembayaran(json_encode($data));
            File::delete($path);
        }

        if($response_update['code'] == '200'){
            $message = "Pembayaran pendaftaran telah dilakukan oleh ".session('username')." ";
            $notif = $this->config_pusher();
            $notif->trigger('konfirmasi_pembayaran', 'event_konfirm', $message);
            return redirect()->route('history-pembayaran')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_register(){
        Session::put('title', 'Formulir Pendaftaran');
        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
            $form_daftar = $this->PesertaPpdbApi->get_form_peserta_pendaftaran(session('id'),$pecah_data[0]);
            $result          = $form_daftar['body']['data'];
        } else {
            $result          = array();
        }

        //dd($result);
        $param = [
            'form'=>$result,
        ];

        return view('ppdb.components.Peserta.v_form_register')->with($param);

    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store_register(Request $request){
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $data = array();
        $form_ = array();
        $data['id_sekolah']    = session('id_sekolah');
        $data['tahun_ajaran']  = $pecah_data[0];
        $data['id_peserta']    = session('id');
        foreach ((array)$request->form as $r => $v) {
            $form = array();
            $form['pendaftaran_form'] = $r."-".$v;
            $form_[] = $form;
        }
        $data['forms'] = $form_;
        $response_update = $this->PesertaPpdbApi->registerForm(json_encode($data));
        if($response_update['code'] == '200'){
            return back()->with('success',$response_update['body']['message'].' Silahkan lanjut langkah berikutnya.');
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
     }

     /** print preview */
     public function print_card_printview(){
        Session::put('title', 'Print Kartu Peserta');
        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_update = $this->PesertaPpdbApi->get_preview_print_card(session('id'),$pecah_data[0]);
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_kartu();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows,'jadwal'=>$result_jadwal,'form_setting'=>$result_setting];
        return view('ppdb.components.print_form_card')->with($param);
     }

     /**
      * print kartu peserta dan jadwal
      */

      public function print_kartu_peserta(){
        Session::put('title', 'Print Kartu Peserta');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_update = $this->PesertaPpdbApi->get_preview_print_card(session('id'),$pecah_data[0]);
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_kartu();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows,'jadwal'=>$result_jadwal,'form_setting'=>$result_setting];
        return view('ppdb.components.print_form_card_custom')->with($param);
      }

     /** print form preview  */

     public function print_form_printview(){
        Session::put('title', 'Print Preview Formulir Pendaftaran');
        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_update = $this->PesertaPpdbApi->get_preview_print_form(session('id'),$pecah_data[0]);
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        $detail_sample_sett = $this->AdminPpdbApi->get_data_template_kartu();
        $result_setting = $detail_sample_sett['body']['data'] ?? array();
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows,'jadwal'=>$result_jadwal,'form_setting'=>$result_setting];
        return view('ppdb.components.print_form')->with($param);
     }

     /** print data form pendaftaran */

     public function print_form_p(){
        Session::put('title', 'Print Formulir Pendaftaran');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_update = $this->PesertaPpdbApi->get_preview_print_form(session('id'),$pecah_data[0]);
        $detail_jadwal = $this->AdminPpdbApi->get_data_jadwal_all(session('id_sekolah'));
        $result_jadwal = $detail_jadwal['body']['data'];
        //$detail_sample_sett = $this->AdminPpdbApi->get_data_template_kartu();
        //$result_setting = $detail_sample_sett['body']['data'] ?? array();
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows,'jadwal'=>$result_jadwal];
        return view('ppdb.components.print_form_custom')->with($param);
     }

     /** get pengumuman  */

     public function print_pengumuman(){
        Session::put('title', 'Print Preview Hasil Pengumuman');
        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        $response_update = $this->PesertaPpdbApi->get_pengumuman(session('id'));
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows];
        return view('ppdb.components.print_form_pengumuman')->with($param);
     }

     public function print_pengumuman_cetak(){
        Session::put('title', 'Print Hasil Pengumuman');
        $response_update = $this->PesertaPpdbApi->get_pengumuman(session('id'));
        $datarows = $response_update['body']['data'] ?? array();
        $param = ['form'=>$datarows];
        return view('ppdb.components.print_form_pengumuman_custom')->with($param);
     }



     /** upload Dokumen file  */
     public function uploadfile(){
        Session::put('title', 'Dokumen File Pendukung');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        return view('ppdb.components.v_user_upload_doc');
     }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_filedoc(Request $request){
        $detail_sample = $this->PesertaPpdbApi->get_filedoc(session('id'));
        //dd($detail_sample);
        $result = $detail_sample['body']['data'];
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download doc" data-gambar="'.$data['file'].'"   data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download doc" data-gambar="'.$data['file'].'"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download doc" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('nisn', function ($row) {
                return $row['nisn'];
            });
             $table->editColumn('nama_peserta', function ($row) {
                return $row['peserta'];
            });
            $table->editColumn('file', function ($row) {
                $infoPath = pathinfo($row['file']);
                return $infoPath['extension'];
            });
            $table->editColumn('status', function ($row) {
               if($row['status'] == 'Aktif'){
                   return '<i class="fa fa-eye"></i>';
               }else if($row['status']=='Tidak Aktif'){
                   return '<i class="fa fa-eye-slash"></i>';
               }
            });
            $table->rawColumns(['action', 'file','status','nama_peserta']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** invoice pembayaran biaya pendaftaran  peserta  */

    public function invoice_pembayaran(){
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();
        $tahun_ajaran = $setting_data['tahun_ajaran'] ?? '';
        if (!empty($tahun_ajaran)) {
            $pecah_data   = explode("/", $tahun_ajaran);
        } else {
            $pecah_data   = array();
        }
        $response_download = $this->PesertaPpdbApi->download_invoice(session('id'),$pecah_data[0]);
        $tagihan = $this->PesertaPpdbApi->get_tagihan_peserta(session('id'));
        $result_tagihan = $tagihan['body']['data'] ?? array();
        $status_pembayaran = $result_tagihan['status_pembayaran_kode'] ?? '0';
        $name_status       = $status_pembayaran == '1' ? 'Kwitansi':'Invoice';
        $download = $this->force_download_response($response_download,$name_status);

        if ($download['code'] == '200') {

            $file = public_path($download['filename']);
            $headers = array(
                'Content-Type:application/pdf',
            );
            return response()->download($file,$download['filename'].'.pdf', $headers)->deleteFileAfterSend(true);
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_file_upload()
    {
        Session::put('title', 'Tambah File ');
        $setting = $this->AdminPpdbApi->get_data_setting_ppdbbyid(session('id_sekolah'));
        $setting_data = $setting['body']['data'] ?? array();

        Session::put('whatapps',$setting_data['whatsapp']);
        Session::put('telepone_cs',$setting_data['telepon']);

        $check_access  = $this->check_access_page();
        if($check_access == false){
            return view('ppdb.components.v_no_access');
        }
        return view('ppdb.components.v_user_upload_add');
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUploadfile(Request $request)
    {
         //validate form save or update
         $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);
        $data = array();
        $data['nama']      = $request->nama;
        $data['id_peserta'] = session('id');
        $data['id_sekolah'] = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_dokumenfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->post_data_dokumen(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return redirect()->route('upload-file')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showfile($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_detail_dokumen_byid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editfile($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_detail_dokumen_byid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

      /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatefile(Request $request, $id)
    {
        //
         //validate form save or update
         $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);
        $data = array();
        $data['id']        = $id;
        $data['nama']      = $request->nama;
        $data['id_peserta'] = session('id');
        $data['id_sekolah'] = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/Doc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_data_dokumenfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->update_data_dokumen(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyfile($id)
    {
        $response_show_detail = $this->AdminPpdbApi->delete_data_dokumen($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * ajax notif pembayaran
     */
    public function notif_pembayaran(){
        $tagihan = $this->PesertaPpdbApi->get_tagihan_peserta(session('id'));
        $result_tagihan = $tagihan['body']['data'] ?? array();
        $param = [];
        if($result_tagihan['status_pembayaran'] == 'Konfirmasi'){
            $param['pesan'] = 'Pembayaran telah Dikonfirmasi oleh Administrator PPDB';
            $param['catatan'] = $result_tagihan['keterangan'];
        }else if($result_tagihan['status_pembayaran'] == 'Batal'){
            $param['pesan'] = 'Pembayaran telah Ditolak oleh Administrator PPDB';
            $param['catatan'] = $result_tagihan['keterangan'];
        }
        return response()->json(['data'=>$param,'info'=>'success']);
    }

     /** setting pusher  */
     public function config_pusher()
     {
         $app_id = env('PUSHER_APP_ID');
         $app_secret = env('PUSHER_APP_SECRET');
         $app_key    = env('PUSHER_APP_KEY');
         $app_cluster = env('PUSHER_APP_CLUSTER');
         $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
         return $pusher;
     }

     /** public function forget password */
     public function forget_password(){
        Session::put('title', 'Lupa Password Peserta PPDB ');
        return view('ppdb.components.forget_pass');
     }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store_forget_pass(Request $request){
        $request->validate([
            'email'=>'required |email',
        ],[
            'email.required'=>'Form Email tidak boleh kosong!',
        ]);

        $data=array();
        $data['email'] = $request->email;

        $response_update = $this->PesertaPpdbApi->forget_pass(json_encode($data));
        if($response_update['code'] == '200'){
            return back()->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }
     }

     /** get Token */
     public function token_passwordx($token){
        Session::put('title', 'Reset Password Baru ');
        return view('ppdb.components.forget_pass_reset')->with(['token'=>$token]);
     }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_reset_pass(Request $request){
        $request->validate([
            'password_new'=>'required',
            'password_confirm'=>'required | same:password_new',
        ],[
            'password_new.required'=>'Form Password Baru tidak boleh kosong!',
            'password_confirm.required'=>'Form Password Konfirm tidak boleh kosong!',
        ]);

        $token = $request->token_reset;
        $response_check = $this->PesertaPpdbApi->get_token_pass($token);

        if($response_check['code'] == '200'){
            $data=array();
            $data['as'] = $response_check['body']['data']['as'];
            $data['email'] = $response_check['body']['data']['email'];
            $data['password'] = $request->password_new;
            $data['password_confirmation'] = $request->password_confirm;
            $response_update = $this->PesertaPpdbApi->reset_pass(json_encode($data));
            if($response_update['code'] == '200'){
                Session::put('title', ' Auth Login ');
                $pesan = array(
                    'message' => $response_update['body']['message'],
                    'icon' => 'berhasil',
                    'status' => 'success'
                );
                return redirect()->route('auth.login')->with(['message' => $pesan]);
            }else{
                return back()->with('error',$response_update['body']['message']);
            }

        }else{
            return redirect()->route('forget_password_peserta')->with('error',$response_check['body']['message']);
        }


     }
}
