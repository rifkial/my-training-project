<?php

namespace App\Http\Controllers\Ppdb\Akun;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;
use XcS\XcTools;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;

class AdminUserPpdb extends Controller
{

    /**
     * variabel global
     */

    protected $AdminPpdbApi;
    public $urlApi;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
        $this->urlApi = env("API_URL");
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Manajemen Akun Administrator');
        return view('ppdb.components.Akun.v_page_admin');
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request){
        $detail_sample = $this->AdminPpdbApi->get_data_AdminUser();
        $result = $detail_sample['body']['data'];
        //dd($detail_sample);

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show admin" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit admin" data-gambar="'.$data['file'].'"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove admin" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('photo', function ($row) {
                return '<img src="' .$row['file']. '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('email', function ($row) {
                switch($row['email']){
                    case '':
                        return '-';
                    default:
                    return $row['email'];
                }
            });
            $table->editColumn('nama_admin', function ($row) {
                return $row['nama'];
            });
            $table->editColumn('alamat', function ($row) {
                switch($row['alamat']){
                    case '':
                        return '-';
                    default:
                        return $row['alamat'];
                }
            });

            $table->editColumn('telepon', function ($row) {
                switch($row['telepon']){
                    case '':
                        return '-';
                    default:
                        return $row['telepon'];
                }
            });

            $table->editColumn('status', function ($row) {
                if($row['status'] == 'Aktif'){
                    return '<i class="fa fa-eye"></i>';
                }else if($row['status']=='Tidak Aktif'){
                    return '<i class="fa fa-eye-slash"></i>';
                }
             });

            $table->rawColumns(['action', 'status','nama_admin','photo','alamat']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::put('title', 'Tambah Admin');
        return view('ppdb.components.Akun.v_page_admin_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          //validate form save or update
          $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama'=>'required',
            'username'=>'required',
            'jenkel'=>'required',
            'agama'=>'required',
            //'telepon'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'username.required'=>'Form username tidak boleh kosong!',
            'email.required'=>'Form email tidak boleh kosong!',
            'password.required'=>'Form password tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb!',
            //'telepon.required'=>'Form telepon tidak boleh kosong!',
            'agama.required'=>'Form agama tidak boleh kosong!',
            'jenkel.required'=>'Form jenis kelamin harus pilih satu Laki-laki/Perempuan!',
            'tempat_lahir.required'=>'Form tempat lahir tidak boleh kosong!',
            'tanggal_lahir.required'=>'Form tanggal lahir tidak boleh kosong!'
        ]);

        $data = array();
        $data['nama']           = $request->nama;
        $data['username']       = $request->username;
        $data['jenkel']         = $request->jenkel;
        $data['email']          = $request->email;
        $data['first_password'] = $request->password;
        $data['agama']          = $request->agama;
        $data['id_sekolah']     = session('id_sekolah');
        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profile/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_adminfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->post_data_admin(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return redirect()->route('administrator')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_AdminUserbyid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_show_detail = $this->AdminPpdbApi->get_data_AdminUserbyid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //validate form save or update
            $request->validate([
                'nama'=>'required',
                'username'=>'required',
                'jenkel'=>'required',
                'email'=>'required|email',
                'password'=>'required'
            ],[
                'nama.required'=>'Form nama tidak boleh kosong!',
                'username.required'=>'Form username tidak boleh kosong!',
                'email.required'=>'Form email tidak boleh kosong!',
                'password.required'=>'Form password tidak boleh kosong!',
                'jenkel.required'=>'Form jenis kelamin harus pilih satu Laki-laki/Perempuan!',
            ]);

            $data = array();
            $data['id']             = $id;
            $data['nama']           = $request->nama;
            $data['username']       = $request->username;
            $data['jenkel']         = $request->jenkel;
            $data['email']          = $request->email;
            $data['first_password'] = $request->password;
            $data['agama']          = $request->agama;
            $data['id_sekolah']     = session('id_sekolah');

            if($files = $request->file('image')){
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/profile/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data['path'] = $path;
                $response_update = $this->AdminPpdbApi->update_data_adminfile(json_encode($data));
                File::delete($path);
            }else{
                $response_update = $this->AdminPpdbApi->update_data_admin(json_encode($data));
            }

            if($response_update['code'] == '200'){
                return response()->json(
                    [
                        'message'=>$response_update['body']['message'],
                        'data'=>$response_update['body']['data'],
                        'info'=>'success'
                    ]);
            }else{
                return response()->json(
                    [
                        'message'=>$response_update['body']['message'],
                        'data'=>$response_update['body']['data'],
                        'info'=>'error'
                    ]);
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response_show_detail = $this->AdminPpdbApi->delete_data_AdminUserbyid($id);
        if($response_show_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_show_detail['body']['message'],
                    //'data'=>$response_show_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }
}
