<?php

namespace App\Http\Controllers\Ppdb\Download;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Help;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;
use App\ApiService\Ppdb\Akun\AdminPpdbApi;

class SampledocController extends Controller
{
    protected $AdminPpdbApi;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->AdminPpdbApi = new AdminPpdbApi();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Sample Dokumen');
        return view('ppdb.components.Download.v_sample');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::put('title', 'Add Sample Dokumen');
        return view('ppdb.components.Download.v_sample_add');
    }

      /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request){
        $detail_sample = $this->AdminPpdbApi->get_sampledata_doc();
        //dd($detail_brosur);
        $result = $detail_sample['body']['data'];

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[]= $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show download sample" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit download sample"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove download sample" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->editColumn('file', function ($row) {
                $infoPath = pathinfo($row['file']);
                return $infoPath['extension'];
            });
            $table->editColumn('status', function ($row) {
               if($row['status_kode'] == '1'){
                   return '<i class="fa fa-eye"></i>';
               }else if($row['status_kode']=='0'){
                   return '<i class="fa fa-eye-slash"></i>';
               }
            });
            $table->rawColumns(['action', 'file','status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * get type file
     */
    public function getMIME($extension) {
        $file = "mime.types";
        $in = fopen($file, "r");
        while (($line = fgets($in)) !== false) {
            if (preg_match("/([a-z]+\/[a-z]+)\s+([a-z\s]*)\b($extension)\b/", $line, $match)) {
                return $match[1];
            }
        }
        fclose($in);
        return "error";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form save or update
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.required'=>'Form Gambar tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);

        $data = array();
        $data['nama']      = $request->nama;
        $data['keterangan'] = $request->isi;
        $data['id_sekolah'] = session('id_sekolah');

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/sampleDoc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->post_data_sampledocfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->post_data_sampledoc(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return redirect()->route('sample-doc')->with('success',$response_update['body']['message']);
        }else{
            return back()->with('error',$response_update['body']['message']);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_sample_detail = $this->AdminPpdbApi->get_sampleinfo_doc($id);
        if($response_sample_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    'data'=>$response_sample_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    'data'=>$response_sample_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $response_sample_detail = $this->AdminPpdbApi->get_sampleinfo_doc($id);
        if($response_sample_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    'data'=>$response_sample_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    'data'=>$response_sample_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate form save or update
        $request->validate([
            'image' => 'mimes:jpeg,png,jpg,gif,svg,doc,docx,pdf|max:2048',
            'nama'=>'required',
        ],[
            'nama.required'=>'Form nama tidak boleh kosong!',
            'image.max'=>'Ukuran File Gambar maksimal 2048 kb',
        ]);

        $data = array();
        $data['id']        = $id;
        $data['nama']      = $request->nama;
        $data['keterangan'] = $request->isi;
        $data['status']     = $request->status;
        $data['id_sekolah'] = session('id_sekolah');

        //dd($data);

        if($files = $request->file('image')){
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/sampleDoc/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $response_update = $this->AdminPpdbApi->update_data_sampledocfile(json_encode($data));
            File::delete($path);
        }else{
            $response_update = $this->AdminPpdbApi->update_data_sampledoc(json_encode($data));
        }

        if($response_update['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'success',
                    'icon'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_update['body']['message'],
                    'data'=>$response_update['body']['data'],
                    'info'=>'error',
                    'icon'=>'error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response_sample_detail = $this->AdminPpdbApi->delete_data_sample($id);
        if($response_sample_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    //'data'=>$response_sample_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    //'data'=>$response_sample_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

        /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function restore($id)
    {
        $restore_brosur = $this->AdminPpdbApi->restore_data_sample($id);
        if($restore_brosur['code'] == '200'){
            return response()->json(
                [
                    'message'=>$restore_brosur['body']['message'],
                    'info'=>'success',
                    'icon'=>'success',
                    'data'=>$restore_brosur['body']['data'],
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$restore_brosur['body']['message'],
                    'info'=>'error',
                    'icon'=>'error',
                    'data'=>$restore_brosur['body']['data'],
                ]);
        }
    }

    /**
     * Ajax Trash   a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *  @param  \Illuminate\Http\Request  $request
     */

    public function ajaxtrash(Request $request){
        $detail_brosur = $this->AdminPpdbApi->trash_data_sample();
        $result = $detail_brosur['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-default btn-sm restore download sample " data-id="' . $data['id'] . '"><i class="fa fa-undo"></i></button> &nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove_force download sample" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
     }


        /**
     * Remove force  the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteForce($id)
    {
        $response_sample_detail = $this->AdminPpdbApi->deletepermanent_data_sample($id);
        if($response_sample_detail['code'] == '200'){
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                    //'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'success'
                ]);
        }else{
            return response()->json(
                [
                    'message'=>$response_sample_detail['body']['message'],
                   // 'data'=>$response_brosur_detail['body']['data'],
                    'info'=>'error'
                ]);
        }
    }

}
