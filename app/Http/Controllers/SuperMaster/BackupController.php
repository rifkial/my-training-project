<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\SuperMaster\DatabaseApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BackupController extends Controller
{
    private $databaseApi;

    public function __construct()
    {
        $this->databaseApi = new DatabaseApi();
    }

    public function index(Request $request)
    {
        // dd("backup database");
        Session::put('title', 'Database Program');
        $database = $this->databaseApi->get_all();
        // dd($database);
        $result = $database['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.superadmin.master.v_database')->with([]);
    }

    public function create(Request $request)
    {
        // dd("ping");
        $backup = $this->databaseApi->create();
        dd($backup);
        if ($backup['code'] == 200) {
            return response()->json([
                'success' => $backup['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $backup['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->kategoriProgramApi->delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
