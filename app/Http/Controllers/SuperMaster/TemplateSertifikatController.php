<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\Prakerin\TemplateSertifikatApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class TemplateSertifikatController extends Controller
{
    private $templateApi;

    public function __construct()
    {
        $this->templateApi = new TemplateSertifikatApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Template Sertifikat');
        $template = $this->templateApi->get_all();
        // dd($template);
        $data = $template['body']['data'];
        if (session('role') == 'guru') {
            return view('content.prakerin.guru.v_template')->with(['template' => session('template'), 'data' => $data]);
        } else {
            return view('content.superadmin.master.v_template')->with([]);
        }


    }

    public function store(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'nama' => $request->nama,
            'sourcekode' => $request->sourcecode,
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move(public_path($basePath), $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->templateApi->create_file(json_encode($data));
            $code = $result['kode'];
            $sukses = $result['data']['message'];
            File::delete($path);
        } else {
            $result = $this->templateApi->create(json_encode($data));
            $code = $result['code'];
            $sukses = $result['body']['message'];
        }
        // dd($result);
        if ($code == 200) {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->templateApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'sourcekode' => $request->sourcecode,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->templateApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->templateApi->update_info(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->templateApi->update_info(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function delete($id)
    {
        $delete = $this->templateApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data()
    {
        return view('content.prakerin.template_sertifikat.v_atlet')->with([]);
    }

    public function detail($id)
    {
        $template = $this->templateApi->get_by_id($id);
        $template = $template['body']['data'];
        // dd($template);
        return view('content.prakerin.template_sertifikat.sample.v_'.$template['nama'])->with(['siswa' => []]);
    }
}
