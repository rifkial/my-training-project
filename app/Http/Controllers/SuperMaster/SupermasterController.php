<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\SuperMaster\SuperAdminApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class SupermasterController extends Controller
{
    private $superAdminApi;

    public function __construct()
    {
        $this->superAdminApi = new SuperAdminApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'User Super Admin');
        $super = $this->superAdminApi->get_all();
        // dd($super);
        $result = $super['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                // $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                // $button .= '&nbsp;&nbsp;';
                $button = '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" class="key-'.$data['id'].' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'guru\')"><i class="fa fa-key"></i> Reset Password</button>';
                return $button;
            });
            $table->editColumn('gambar', function ($row) {
                return '<img src="'.$row['file'].'" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action','gambar']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.superadmin.master.v_superadmin')->with([]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move(public_path($basePath), $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->superAdminApi->create_file(json_encode($data));
            $code = $result['kode'];
            $sukses = $result['data']['message'];
            File::delete($path);
        } else {
            $result = $this->superAdminApi->create(json_encode($data));
            $code = $result['code'];
            $sukses = $result['body']['message'];
        }
        // dd($result);
        if ($code == 200) {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $sukses,
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id'];
        $super  = $this->superAdminApi->get_by_id($id);
        $result = $super['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        $result['tanggal'] = date('d-m-Y', strtotime($result['tgl_lahir']));
        // dd($result);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'jenkel' => $request->jenkel,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->superAdminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->superAdminApi->update_info(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->superAdminApi->update_info(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->mapelApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function delete($id)
    {
        $delete = $this->superAdminApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
