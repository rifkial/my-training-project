<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\Master\ProgramApi;
use App\ApiService\SuperMaster\KategoriProgramApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use XcS\XcTools;

class ProgramController extends Controller
{
    private $programApi;
    private $kategoriApi;

    public function __construct()
    {
        $this->programApi = new ProgramApi();
        $this->kategoriApi = new KategoriProgramApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Program');
        $program = $this->programApi->get_all();
        $result = $program['body']['data'];
        $first = reset($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
            $table->editColumn('gambar', function ($row) {
                return '<img src="'.$row['file'].'" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('sort_order', function ($rows) {
                // dd($rows);
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-sort="up" onclick="updateSort(\'' . $rows['id'].  '\',\'up\')" class="btn btn-success btn-sm"><i class="fa fa-arrow-up"></i></a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<a href="javascript:void(0)"  data-sort="down" class="btn btn-warning btn-sm" onclick="updateSort(\'' . $rows['id'].  '\',\'down\')"><i class="fa fa-arrow-down"></i></a>';
                return $button;
            });
            $table->editColumn('status', function ($row) {
                if ($row['status'] != 0) {
                    return ' <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" onclick="changeStatus(1,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="Y">ON</a>
                                    <a class="btn btn-primary btn-sm notActive" onclick="changeStatus(0,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="N">OFF</a>
                                </div>
                            </div>';
                } else {
                    return ' <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm notActive" onclick="changeStatus(1,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="Y">ON</a>
                                    <a class="btn btn-primary btn-sm active" onclick="changeStatus(0,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="N">OFF</a>
                                </div>
                            </div>';
                }
            });
            $table->rawColumns(['action','gambar', 'sort_order', 'status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
        return view('content.superadmin.master.v_program')->with([]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'id_kategori_program' => 1,
            'kode' => $request->kode,
            'nama' => $request->nama,
            'link' => $request->link,
            'deskripsi' => $request->deskripsi,
            'status' => 2
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move(public_path($basePath), $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->programApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->programApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_program'];
        $post  = $this->programApi->get_by_id($id);
        $result = $post['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'kode' => $request->kode,
            'link' => $request->link,
            'deskripsi' => $request->deskripsi,
            'id_kategori_program' => $request->id_kategori
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->programApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->programApi->update_info(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->programApi->update_info(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->mapelApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function delete($id)
    {
        $delete = $this->programApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
    public function update_sort(Request $request){
        if($request->params == "up"){
            $sort = $this->programApi->up((int)$request->id);
        }else{
            $sort = $this->programApi->down((int)$request->id);
        }
        if ($sort['code'] == 200) {
            return response()->json([
                'message' => $sort['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $sort['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update = $this->programApi->update_status(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

}
