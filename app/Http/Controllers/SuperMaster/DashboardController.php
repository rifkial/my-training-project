<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\Master\SekolahApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    private $sekolahApi;
    private $hash;

    public function __construct()
    {
        $this->sekolahApi = new SekolahApi();
        $this->hash = new Hashids();
    }

    public function index($id)
    {
        Session::put('title', 'Data Sekolah');
        // dd($id);
        $sekolah = $this->sekolahApi->get_by_id($this->hash->decode($id)[0]);
        $sekolah = $sekolah['body']['data'];
        return view('content.master.sekolah.dashboard')->with(['sekolah' => $sekolah]);
    }

    public function store(Request $request)
    {
        // dd($request);
        if ($request->file('logo') != null) {
            $image = $request->file('logo');
            $mimetype = $image->getMimeType();
            $image_base64 = base64_encode(file_get_contents($request->file('logo')));
            $data['logo'] = "data:" . $mimetype . ";base64," . $image_base64;
        }
        $data =
            [
                'nama' => $request->nama,
                'npsn' => $request->npsn,
                'jenjang' => $request->jenjang,
                'status_sekolah' => $request->status_sekolah,
                'sk_pendirian' => $request->sk_pendirian,
                'tgl_sk_pendirian' => $request->tgl_sk_pendirian,
                'email' => $request->email,
                'fax' => $request->fax,
                'website' => $request->website,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kode_pos' => $request->kode_pos,
                'status' => 1
            ];

        $result = $this->sekolahApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit($id)
    {
        // dd('ping');
        $post  = $this->sekolahApi->get_by_id($id);
        $result = $post['body']['data'];
        // dd($post);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'npsn' => $request->npsn,
                'jenjang' => $request->jenjang,
                'status_sekolah' => $request->status_sekolah,
                'sk_pendirian' => $request->sk_pendirian,
                'tgl_sk_pendirian' => $request->tgl_sk_pendirian,
                'email' => $request->email,
                'fax' => $request->fax,
                'website' => $request->website,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kode_pos' => $request->kode_pos,
                'status' => 1
            ];

        $result = $this->sekolahApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function filter(Request $request)
    {
        // dd($request);
        $search = array(
            'nama' => $request['nama']
        );
        $sekolah = $this->sekolahApi->filter(json_encode($search));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        $html = '
            <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                <a class="card" style="position: relative;" href="javascript:void(0)" id="createNewCustomer">
                    <div class="card-body text-center">
                        <i class="fa fa-plus-circle" style="font-size: 189px;"></i>
                    </div>
                </a>
            </div>
        ';
        if (!empty($sekolah)) {
            foreach ($sekolah as $sk) {
                $html .= '
                <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                <div class="card" style="position: relative;">
                    <img src="' . $sk['file'] . '" class="box-shadow mx-auto text-center" alt=""
                        style="width: 90px; height: 90px; margin-top: -45px;">
                    <div style="position: absolute; right: 7px; top: 7px;">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                                data-id="' . $sk['id'] . '"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    <div class="card-body text-center">
                        <h3 class="card-title pt-1">' . $sk['nama'] . '</h3>
                        <p class="card-text text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip.</p>
                        <a class="text-sm text-uppercase font-weight-bold" href="#">Lihat Selengkapnya&nbsp;<i
                                class="fe-icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
                ';
            }
        }
        return response()->json($html);
    }
}
