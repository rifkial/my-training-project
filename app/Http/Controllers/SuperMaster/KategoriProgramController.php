<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\SuperMaster\KategoriProgramApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KategoriProgramController extends Controller
{
    private $kategoriProgramApi;

    public function __construct()
    {
        $this->kategoriProgramApi = new KategoriProgramApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Kategori Program');
        $kategori = $this->kategoriProgramApi->get_all();
        // dd($kategori);
        $result = $kategori['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.superadmin.master.v_kategori_program')->with([]);
    }

    public function store(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $data =
            [
                'nama' => $request->nama,
                'id_sekolah' => $id_sekolah,
                'status' => 1
            ];

        $kategori = $this->kategoriProgramApi->create(json_encode($data));
        if ($kategori['code'] == 200) {
            return response()->json([
                'success' => $kategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $kategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_kategori'];
        $post  = $this->kategoriProgramApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'id_sekolah' => $id_sekolah,
                'status' => 1
            ];

        $kategori = $this->kategoriProgramApi->create(json_encode($data));
        if ($kategori['code'] == 200) {
            return response()->json([
                'success' => $kategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $kategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function trash($id)
    {
        $delete = $this->kategoriProgramApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function delete($id)
    {
        $delete = $this->kategoriProgramApi->delete($id);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
