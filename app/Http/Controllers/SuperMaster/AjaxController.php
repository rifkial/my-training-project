<?php

namespace App\Http\Controllers\SuperMaster;

use App\ApiService\Master\ProgramSekolahApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\User\AdminApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    private $adminApi;
    private $programApi;
    private $sekolahApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
        $this->programApi = new ProgramSekolahApi();
        $this->sekolahApi = new SekolahApi();
    }

    public function load_admin(Request $request)
    {
        $admin = $this->adminApi->get_by_sekolah($request['id']);
        $result = $admin['body']['data'];
        $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    if ($data['status'] == 1) {
                        return '<div class="input-group">
                                    <div id="radioBtn" class="btn-group">
                                        <a class="btn btn-primary btn-sm active" onclick="changeStatusAdmin(1,'.$data['id'].')"
                                            data-toggle="pilihan_check_admin'.$data['id'].'" data-title="Y">ON</a>
                                        <a class="btn btn-primary btn-sm notActive" onclick="changeStatusAdmin(0,'.$data['id'].')"
                                            data-toggle="pilihan_check_admin'.$data['id'].'" data-title="N">OFF</a>
                                    </div>
                                </div>';
                    } else {
                        return '<div class="input-group">
                                    <div id="radioBtn" class="btn-group">
                                        <a class="btn btn-primary btn-sm notActive" onclick="changeStatusAdmin(1,'.$data['id'].')"
                                            data-toggle="pilihan_check_admin'.$data['id'].'" data-title="Y">ON</a>
                                        <a class="btn btn-primary btn-sm active" onclick="changeStatusAdmin(0,'.$data['id'].')"
                                            data-toggle="pilihan_check_admin'.$data['id'].'" data-title="N">OFF</a>
                                    </div>
                                </div>';
                    }
                });
        $table->editColumn('status', function ($row) {
            if ($row['status'] == 1) {
                return '<span class="badge badge-success text-inverse">Aktif</span>';
            } else {
                return '<span class="badge badge-danger text-inverse">Tidak Aktif</span>';
            }
        });
        $table->rawColumns(['action', 'status']);
        $table->addIndexColumn();

        return $table->make(true);
    }
    
    public function load_program(Request $request)
    {
        $id_sekolah = $request['id'];
        $all_program = $this->sekolahApi->program_sekolah($id_sekolah);
        // dd($all_program);
        $result = $all_program['body']['data'];
        $table = datatables()->of($result)
                ->addColumn('aktifasi', function ($data) {
                    if ($data['status'] == 1) {
                        return '<div class="input-group">
                                    <div id="radioBtn" class="btn-group">
                                        <a class="btn btn-primary btn-sm active" onclick="changeStatusProgram(1,'.$data['id'].')"
                                            data-toggle="pilihan_check_program'.$data['id'].'" data-title="Y">ON</a>
                                        <a class="btn btn-primary btn-sm notActive" onclick="changeStatusProgram(0,'.$data['id'].')"
                                            data-toggle="pilihan_check_program'.$data['id'].'" data-title="N">OFF</a>
                                    </div>
                                </div>';
                    } else {
                        // dd("tidak aktif");
                        return ' <div class="input-group">
                                    <div id="radioBtn" class="btn-group">
                                        <a class="btn btn-primary btn-sm notActive" onclick="changeStatusProgram(1,'.$data['id'].')"
                                            data-toggle="pilihan_check_program'.$data['id'].'" data-title="Y">ON</a>
                                        <a class="btn btn-primary btn-sm active" onclick="changeStatusProgram(0,'.$data['id'].')"
                                            data-toggle="pilihan_check_program'.$data['id'].'" data-title="N">OFF</a>
                                    </div>
                                </div>';
                    }
                });
        $table->editColumn('status', function ($row) {
            if ($row['status'] == 1) {
                return '<span class="badge badge-success text-inverse">Aktif</span>';
            } else {
                return '<span class="badge badge-danger text-inverse">Tidak Aktif</span>';
            }
        });
        $table->editColumn('template', function ($tem) {
            $template = '<select onchange="changeTemplate(this.value, '.$tem['id'].')">';
            foreach ($tem['template'] as $temp) {
                if ($temp['terpasang'] == true) {
                    $template .= '<option value="'.$temp['id'].'" selected>'.$temp['nama'].'</option>';
                } else {
                    $template .= '<option value="'.$temp['id'].'">'.$temp['nama'].'</option>';
                }
            }
            $template .= '</select>';
            return $template;
        });
        $table->rawColumns(['aktifasi', 'status', 'template']);
        $table->addIndexColumn();

        return $table->make(true);
    }
}
