<?php

namespace App\Http\Controllers\PenjaminMutu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\ApiService\PenjaminMutu\DataDokumen\KategoriApi;
use App\ApiService\PenjaminMutu\DataDokumen\SubKategoriApi;
use App\ApiService\Sekolah\KejuruanApi;

use App\ApiService\PenjaminMutu\DokumenApi;

class DokumenController extends Controller
{
    public $KategoriApi;
    public $SubKategoriApi;
    public $KejuruanApi;
    public $DokumenApi;


    public function __construct()
    {
        $this->KategoriApi = new KategoriApi("api/data/mutu/kategori");
        $this->SubKategoriApi = new SubKategoriApi("api/data/mutu/sub-kategori");
        $this->KejuruanApi = new KejuruanApi("api/data/mutu/kejuruan");
        $this->DokumenApi = new DokumenApi();
    }

    public function index(Request $request){

        $id = $request->id;
        $dataDokumenPublish = $this->DokumenApi->get_by_sekolah();
        $dataDokumenUnpublish = $this->DokumenApi->get_all_unpublish();
        $dataKategori = $this->KategoriApi->get_by_sekolah();
        $dataSubKategori = $this->SubKategoriApi->get_by_sekolah();
        $dataKejuruan = $this->KejuruanApi->get_by_sekolah();

        $dataDokumenPublish = $dataDokumenPublish['body']['data'];
        $dataDokumenUnpublish = $dataDokumenUnpublish['body']['data'];
        $dataKejuruan = $dataKejuruan['body']['data'];
        $dataKategori = $dataKategori['body']['data'];
        $dataSubKategori = $dataSubKategori['body']['data'];

        $dataDokumen = $id == 1 ? $dataDokumenPublish : $dataDokumenUnpublish;
        
        if ($request->ajax()) {
            $table = datatables()->of($dataDokumen);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="Edit('.$row['id'].')" class="btn btn-success btn-sm text-white mt-0 mr-1">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm text-white">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table->editColumn('files', function ($row) {
                
                return '<a href="'.$row['file'].'">'.$row['nama_file'].'</a>';

            });

            
            $table->rawColumns(['aksi','files']);

            return $table->make(true);

            return response()->json([
                'data' => json_encode($table->data),
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
            
        }
        
        return view('content.penjamin_mutu.main-dokumen',compact('dataKategori','dataSubKategori','dataKejuruan'));
    }

    public function publish_docs(Request $request){

        $dataDokumen = $this->DokumenApi->get_by_sekolah();
        $dataKategori = $this->KategoriApi->get_by_sekolah();
        $dataSubKategori = $this->SubKategoriApi->get_by_sekolah();
        $dataKejuruan = $this->KejuruanApi->get_by_sekolah();

        $dataDokumen = $dataDokumen['body']['data'];
        $dataKejuruan = $dataKejuruan['body']['data'];
        $dataKategori = $dataKategori['body']['data'];
        $dataSubKategori = $dataSubKategori['body']['data'];

        if ($request->ajax()) {
            $table = datatables()->of($dataDokumen);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                return '<div class="input-group">
                    <a onclick="Edit('.$row['id'].')" class="btn btn-success btn-sm text-white mt-0 mr-1">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm text-white">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table->editColumn('files', function ($row) {
                
                return '<a href="'.$row['file'].'">'.$row['nama_file'].'</a>';

            });

            
            $table->rawColumns(['aksi','files']);

            return $table->make(true);
            
        }
        
        return view('content.penjamin_mutu.dokumen',compact('dataKategori','dataSubKategori','dataKejuruan'));
    }

    public function unpublish_docs(Request $request){

        $dataDokumen = $this->DokumenApi->get_all_unpublish();
        $dataKategori = $this->KategoriApi->get_by_sekolah();
        $dataSubKategori = $this->SubKategoriApi->get_by_sekolah();
        $dataKejuruan = $this->KejuruanApi->get_by_sekolah();

        $dataDokumen = $dataDokumen['body']['data'];
        $dataKejuruan = $dataKejuruan['body']['data'];
        $dataKategori = $dataKategori['body']['data'];
        $dataSubKategori = $dataSubKategori['body']['data'];

        if ($request->ajax()) {
            $table = datatables()->of($dataDokumen);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="Publish('.$row['id'].')" class="btn btn-primary btn-sm text-white">Publish</a>
                </div>';
            });

            $table->editColumn('files', function ($row) {
                
                return '<a href="'.$row['file'].'">'.$row['nama_file'].'</a>';

            });

            
            $table->rawColumns(['aksi','files']);

            return $table->make(true);
        }
        
        return view('content.penjamin_mutu.un_dokumen',compact('dataKategori','dataSubKategori','dataKejuruan'));
    }

    public function my_dokumen(){

    }

    public function create(Request $request){

        $data_dokumen = array(
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'tgl_awal' => $request->tgl_buat,
            'tgl_akhir'=> $request->tgl_keluar,
            'id_kategori'=> $request->id_kategori,
            'id_sub_kategori'=> $request->id_subkategori,
            'id_kejuruan'=> $request->id_kejuruan,
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $fileName = $namaFile . '.'.$ext;
            //dd($fileName);
            $basePath = "file/bkk/dokumen/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data_dokumen['path'] = $path;
            //dd($data_dokumen);
            $result = $this->DokumenApi->create_file(json_encode($data_dokumen));
            File::delete($path);
            //dd($result);
            $message = $result['body']['message'];
            Session::flash('message',$message);
        }else{
            //$result = $this->DokumenApi->create(json_encode($data_dokumen));
            //dd($result);
            Session::flash('message','Gagal mengirim ! Dokumen tidak terdeteksi');
        }
        return redirect()->back();
    }

    public function update(Request $request){
        $data_dokumen = array(
            'id'=>$request->id,
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'tgl_awal' => $request->tgl_buat,
            'tgl_akhir'=> $request->tgl_keluar,
            'id_kategori'=> $request->id_kategori,
            'id_sub_kategori'=> $request->id_subkategori,
            'id_kejuruan'=> $request->id_kejuruan,
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/bkk/dokumen/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data_dokumen['path'] = $path;
            //dd($data_dokumen);
            $result = $this->DokumenApi->create_file(json_encode($data_dokumen));
            File::delete($path);
            $kode = $result['kode'];
            $message = $result['data']['message'];
        } else {
            $result = $this->DokumenApi->create(json_encode($data_dokumen));
            //dd($result);
            $kode = $result['code'];
            $message = $result['body']['message'];
        }
        return redirect()->back();
    }

    public function detail(Request $request){
        $id = $request->id;

        $detailDocs = $this->DokumenApi->get_by_id($id);
        return $detailDocs;
    }

    public function edit(Request $request){
        $id = $request->id;

        $detailDocs = $this->DokumenApi->get_by_id($id);
        return view('content.penjamin_mutu.edit_dokumen',compact('detailDocs'));
    }

    public function publish(Request $request){
        $id = $request->id;
        $result = $this->DokumenApi->publish($id);
        $code = $result['code'];
        $message = $result['body']['message'];

        if ($code == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function unpublish(Request $request){
        $id = $request->id;
        $result = $this->DokumenApi->unpublish($id);
        $code = $result['code'];
        $message = $result['body']['message'];

        if ($code == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $message,
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
    
}
