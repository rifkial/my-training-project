<?php

namespace App\Http\Controllers\PenjaminMutu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\PenjaminMutu\ProfileUser\UserMutuApi;

class UserController extends Controller
{
    
    public $UserApi;

    public function __construct()
    {
        $this->UserApi = new UserMutuApi();
    }

    public function index(Request $request)
    {
        $dataUser = $this->UserApi->get_by_sekolah();
        $dataUser = $dataUser['body']['data'];

        if ($request->ajax()) {
            $table = datatables()->of($dataUser);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                        <a onclick="Edit('.$row['id'].')" class="btn btn-success btn-sm mt-0 mr-1">
                            <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                        </a>
                        <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                            <i class="fa fa-info ml-2" aria-hidden="true"></i>
                        </a>
                        <a onclick="ResetPass('.$row['id'].')" class="btn btn-warning btn-sm ml-1 data-toggle="tooltip" data-placement="top" title="Reset password"">
                            <i class="fa fa-key ml-1" aria-hidden="true"></i>
                        </a>

                        </div>
                ';
            });

            $table->rawColumns(['aksi']);

            return $table->make(true);


        };
        //dd($dataUser);
        return view('content.penjamin_mutu.user_mutu');
    }


    public function create(Request $request)
    {
        $tgl_lahir = $request->tgl_lahir == null ? null : date('Y-m-d', strtotime($tgl_lahir));
        //dd($tgl_lahir);
        $data = array(
                'id' => $request->id,
                'nama' => $request->nama,
                'first_password' => $request->password,
                'no_induk' => $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => $tgl_lahir,
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'alamat' => $request->alamat,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
        //dd($data);
        $userNew = $this->UserApi->create(json_encode($data));
        
        if ($userNew['code'] == 200) {
            return response()->json([
                'message' => $userNew['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $userNew['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        
    }

    
    public function show(Request $request)
    {
        $id = $request->id;

        $detailUser = $this->UserApi->get_by_id($id);
        return $detailUser;
    }

    
    public function update(Request $request)
    {
        $data = array(
                'id' => $request->id,
                'nama' => $request->nama,
                'role' => $request->role,
                'no_induk'=> $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
        //dd($data);

        $result = $this->UserApi->update_profile_user(json_encode($data));
        //dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

        return redirect()->back();


    }

    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
