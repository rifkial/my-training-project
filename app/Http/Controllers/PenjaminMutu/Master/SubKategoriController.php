<?php

namespace App\Http\Controllers\PenjaminMutu\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\PenjaminMutu\DataDokumen\SubKategoriApi;
use App\ApiService\PenjaminMutu\DataDokumen\KategoriApi;

class SubKategoriController extends Controller
{

    public $SubKategoriApi;
    public $KategoriApi;


    public function __construct()
    {
        $this->SubKategoriApi = new SubKategoriApi("api/data/mutu/sub-kategori");
        $this->KategoriApi = new KategoriApi("api/data/mutu/kategori");
    }
    


    public function index(Request $request){
        $dataSubKategori = $this->SubKategoriApi->get_by_sekolah();
        $dataSubKategori = $dataSubKategori['body']['data'];

        $dataKategori = $this->KategoriApi->get_by_sekolah();
        $dataKategori = $dataKategori['body']['data'];
        
        if ($request->ajax()) {
            $table = datatables()->of($dataSubKategori);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="ShowModal('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Delete('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            
            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.penjamin_mutu.master.subkategori_dokumen')->with(["dataKategori" => $dataKategori]);
    }


    public function create(Request $request){

        $keterangan = $request->keterangan;
        $id_sekolah = session('id_sekolah');
        $nama = $request->nama;
        $id_kategori = $request->id_kategori;

        $data = array(
            "id_sekolah" => $id_sekolah,
            "keterangan" => $keterangan,
            "nama" => $nama,
            "id_kategori" => $id_kategori,
        );

        $SubKategori = $this->SubKategoriApi->create(json_encode($data));

        if ($SubKategori['code'] == 200) {
            return response()->json([
                'message' => $SubKategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $SubKategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function store(Request $request)
    {
        //
    }

    public function edit(Request $request)
    {
        $id = $request->id_item;
        $id_kategori = $request->id_kategori;
        $keterangan = $request->keterangan;
        $nama = $request->nama;

        $data = array(
            "id_sekolah" => session('id_sekolah'),
            "keterangan" => $keterangan,
            "nama" => $nama,
            "id_kategori" => $id_kategori,
        );

        //dd($data);
        $SubKategori = $this->SubKategoriApi->update($id,json_encode($data));
        
        if ($SubKategori['code'] == 200) {
            return response()->json([
                'message' => $SubKategori['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $SubKategori['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
