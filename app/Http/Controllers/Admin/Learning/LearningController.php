<?php

namespace App\Http\Controllers\Admin\Learning;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\RombelApi;
// use App\ApiService\Master\KelasApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LearningController extends Controller
{
    private $rombelApi;
    private $hash;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Room');
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $rombels = [];
        foreach($rombel as $rmb){
            $rombels[] = array(
                'id' => $rmb['id'],
                'id_code' => $this->hash->encode($rmb['id']),
                'nama' => $rmb['nama'],
                'id_kelas' => $rmb['id_kelas'],
                'kelas' => $rmb['kelas'],
                'kelas_romawi' => $rmb['kelas_romawi'],
                'jurusan' => $rmb['jurusan'],
                'id_jurusan' => $rmb['id_jurusan'],
                'id_sekolah' => $rmb['id_sekolah'],
            );
        }
        return view('content.admin.learning.v_user')->with(['rombel' => $rombels]);
    }

    public function store(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'id_guru' => (int)$request->id_guru,
            'id_mapel' => (int)$request->id_mapel,
            'id_rombel' => (int)$request->id_rombel,
            'id_kelas' => (int)$request->id_kelas,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            $code = $result['kode'];
            $sukses = $result['data']['message'];
            File::delete($path);
        } else {
            $result = $this->roomApi->create(json_encode($data));
            $code = $result['code'];
            $sukses = $result['body']['message'];
        }

        if ($code == 200) {
            return response()->json(['success' => $sukses,'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $sukses,'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_room'];
        $room  = $this->roomApi->get_by_id($id);
        $result = $room['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'id_guru' => (int)$request->id_guru,
            'id_mapel' => (int)$request->id_mapel,
            'id_rombel' => (int)$request->id_rombel,
            'id_kelas' => (int)$request->id_kelas,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            $code = $result['kode'];
            $message = $result['data']['message'];
            File::delete($path);
        } else {
            $result = $this->roomApi->update_info(json_encode($data));
            $code = $result['code'];
            $message = $result['body']['message'];
        }
        // dd($result);
        if ($code == 200) {
            return response()->json(['success' => $message,'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $message,'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash(Request $request)
    {
        $id = $request['id_room'];
        $delete = $this->roomApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
