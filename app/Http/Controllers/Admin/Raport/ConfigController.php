<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class ConfigController extends Controller
{
    private $configApi;
    private $tahunApi;

    public function __construct()
    {
        $this->configApi = new ConfigApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index()
    {
        // dd($request);
        Session::put('title', 'Settingan Config');
        $tahun = $this->tahunApi->get_by_sekolah();
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->by_tahun();
        // dd($config);
        $config = $config['body']['data'];
        return view('content.admin.raport.v_config')->with(['tahun' => $tahun, 'config' => $config]);
    }

    public function edit(Request $request)
    {
        $post  = $this->configApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        $result['tgl_raport'] = date('d-m-Y', strtotime($result['tgl_raport']));
        $result['tgl_raport_kelas_akhir'] = date('d-m-Y', strtotime($result['tgl_raport_kelas_akhir']));
        return response()->json($result);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'kepala_sekolah' => $request->kepsek,
            'nip_kepala_sekolah' => $request->nip_kepsek,
            'tgl_raport' =>  date('Y-m-d', strtotime($request->tanggal)),
            'tgl_raport_kelas_akhir' =>  date('Y-m-d', strtotime($request->tanggal_kelas_akhir)),
            'jenis' => $request->jenis,
            'id_ta_sm' => $request->id_tahun_ajar,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->configApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->configApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $config = $this->data_config($request['id_tahun_ajar']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'config' => $config
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'kepala_sekolah' => $request->kepsek,
            'nip_kepala_sekolah' => $request->nip_kepsek,
            'tgl_raport' =>  date('Y-m-d', strtotime($request->tanggal)),
            'tgl_raport_kelas_akhir' =>  date('Y-m-d', strtotime($request->tanggal_kelas_akhir)),
            'jenis' => $request->jenis,
            'id_ta_sm' => $request->id_tahun_ajar,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->configApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->configApi->create(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->configApi->create(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            $config = $this->data_config($request['id_tahun_ajar']);
            return response()->json([
                'message' => $result['body']['message'], 
                'icon'  => 'success', 
                'status' => 'berhasil',
                'config' => $config
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->configApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $config = $this->data_config($request['id_tahun']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'config' => $config
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_config($id)
    {
        $config = $this->configApi->by_all_tahun($id);
        // dd($config);
        $config = $config['body']['data'];
        $html = '';
        if (empty($config)) {
            $html .= '<tr><td colspan="7" class="text-center">Belum ada Config Raport yang tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($config as $cfg) {
                $html .= '
                <tr>
                <td>'.$no++.'</td>
                <td>'.$cfg['jenis'].'</td>
                <td>'.$cfg['kepsek'].'</td>
                <td>'.Help::getTanggal($cfg['tgl_raport']).'</td>
                <td>'.Help::getTanggal($cfg['tgl_raport_kelas_akhir']).'</td>
                <td><img src="'.$cfg['paraf'].'" alt="" width="100"></td>
                <td>
                    <a href="javascript:void(0)"
                        class="btn btn-info btn-sm edit" data-id="'.$cfg['id'].'" data-tahun="'.$id.'"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)"
                        class="btn btn-danger btn-sm delete" data-id="'.$cfg['id'].'" data-tahun="'.$id.'"><i class="fas fa-trash"></i></a>
                </td>
                </tr>
                ';
            }
        }
        return $html;
    }
}
