<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Master\SekolahApi;
use App\ApiService\Raport\KopSampulApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class KopSampulController extends Controller
{
    private $kopApi;
    private $sekolahApi;

    public function __construct()
    {
        $this->kopApi = new KopSampulApi();
        $this->sekolahApi = new SekolahApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Settingan Kop Raport');
        $sekolah = $this->sekolahApi->get_by_id(session('id'));
        $sekolah = $sekolah['body']['data'];
        $kop = $this->kopApi->get_by_sekolah();
        // dd($kop);
        $kop = $kop['body']['data'];
        return view('content.admin.raport.v_kop_sampul')->with(['sekolah' => $sekolah, 'kop' => $kop]);
    }

    public function edit(Request $request)
    {
        // dd($request);
        $id = $request['id_kop'];
        $post  = $this->kopApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'teks1' => $request->teks1,
            'teks2' => $request->teks2,
            'teks3' => $request->teks3,
            'teks4' => $request->teks4,
            'teks5' => $request->teks5,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            if (!empty($files[1]) && !empty($files[0])) {
                $data['path'] = [];
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'][] = $path;
                }
                $result = $this->kopApi->create_file(json_encode($data));
                foreach ($data['path'] as $del_path) {
                    File::delete($del_path);
                }
            } elseif (empty($files[1]) && !empty($files[0])) {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->kopApi->createone_file(json_encode($data));
                    File::delete($path);
                }
            } else {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->kopApi->createones_file(json_encode($data));
                    File::delete($path);
                }
            }
        } else {
            $result = $this->kopApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $response = array(
                'message' => $result['body']['message'],
                'icon' => 'success',
                'status' => 'berhasil',
            );
        } else {
            $response = array(
                'message' => $result['body']['message'],
                'icon' => 'warning',
                'status' => 'gagal',
            );
        }
        // dd
        return redirect()->route('raport-kop_sampul')->with('respons', $response);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/raport/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->kopApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->kopApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->kopApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash($id)
    {
        $delete = $this->kopApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->kopApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $datas = $this->kopApi->all_trash();
        // dd($trash);
        $result = $datas['body']['data'];
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->kopApi->restore($id);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
