<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Raport\TemplateSampulApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class TemplateSampulController extends Controller
{
    private $sampulApi;

    public function __construct()
    {
        $this->sampulApi = new TemplateSampulApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Settingan Config');
        $sampul = $this->sampulApi->get_by_sekolah();
        $result = $sampul['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('gambar1', function ($rows) {
                return '<img src="' . $rows['file1'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'gambar', 'gambar1']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.raport.v_sampul')->with([]);
    }

    public function edit(Request $request)
    {
        $id = $request['id_sampul'];
        $post  = $this->sampulApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'footer' => $request->footer,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            if (!empty($files[1]) && !empty($files[0])) {
                $data['path'] = [];
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'][] = $path;
                }
                $result = $this->sampulApi->create_file(json_encode($data));
                foreach ($data['path'] as $del_path) {
                    File::delete($del_path);
                }
            } elseif (empty($files[1]) && !empty($files[0])) {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->sampulApi->createone_file(json_encode($data));
                    File::delete($path);
                }
            } else {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->sampulApi->createones_file(json_encode($data));
                    File::delete($path);
                }
            }
        } else {
            $result = $this->sampulApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'footer' => $request->footer,
            'id_sekolah' => session('id_sekolah'),
        );
        if ($files = $request->file('image')) {
            if (!empty($files[1]) && !empty($files[0])) {
                $data['path'] = [];
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'][] = $path;
                }
                $result = $this->sampulApi->create_file(json_encode($data));
                foreach ($data['path'] as $del_path) {
                    File::delete($del_path);
                }
            } elseif (empty($files[1]) && !empty($files[0])) {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->sampulApi->createone_file(json_encode($data));
                    File::delete($path);
                }
            } else {
                foreach ($files as $fl) {
                    $namaFile = $fl->getClientOriginalName();
                    $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                    $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                    $basePath = "file/raport/";
                    Help::check_and_make_dir($basePath);
                    $fl->move($basePath, $imageName);
                    $path = $basePath . $imageName;
                    $data['path'] = $path;
                    $result = $this->sampulApi->createones_file(json_encode($data));
                    File::delete($path);
                }
            }
        } else {
            if ($request['remove_photo'] && $request['remove_photo1']) {
                $data['hapus'] = "hapus";
                $result = $this->sampulApi->update_info(json_encode($data));
            } elseif ($request['remove_photo']) {
                $data['hapus'] = "file";
                $result = $this->sampulApi->update_info(json_encode($data));
            } elseif ($request['remove_photo1']) {
                $data['hapus'] = "file1";
                $result = $this->sampulApi->update_info(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->sampulApi->update_info(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash($id)
    {
        $delete = $this->sampulApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
