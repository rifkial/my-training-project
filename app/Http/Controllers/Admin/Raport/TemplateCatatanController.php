<?php

namespace App\Http\Controllers\Admin\Raport;

use App\ApiService\Raport\TemplateCatatanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TemplateCatatanController extends Controller
{
    private $templateApi;

    public function __construct()
    {
        $this->templateApi = new TemplateCatatanApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Template Catatan');
        $catatan= $this->templateApi->get_by_sekolah();
        // dd($catatan);
        $result = $catatan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.raport.v_template_catatan')->with([]);
    }

    public function edit(Request $request)
    {
        $id = $request['id_template'];
        $post  = $this->templateApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $catatan= $this->templateApi->create(json_encode($data));
        if ($catatan['code'] == 200) {
            return response()->json([
                'success' => $catatan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $catatan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $catatan= $this->templateApi->update_info(json_encode($data));
        // dd($catatan);
        if ($catatan['code'] == 200) {
            return response()->json([
                'success' => $catatan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $catatan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->templateApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
