<?php

namespace App\Http\Controllers\Admin;

use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class RoomController extends Controller
{
    private $roomApi;
    private $guruApi;
    private $mapelApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->roomApi = new RoomApi();
        $this->mapelApi = new MapelApi();
        $this->guruApi = new GuruApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Room');
       
        $jurusan = $this->jurusanApi->get_by_sekolah();
        // dd($jurusan);
        $jurusan = $jurusan['body']['data'];
        $mapel = $this->mapelApi->get_by_sekolah();
        // dd($mapel);
        $mapel = $mapel['body']['data'];
        
        return view('content.admin.learning.v_room')->with(["mapel" => $mapel, "jurusan" => $jurusan]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'id_guru' => $request->id_guru,
            'id_mapel' => $request->id_mapel,
            'id_rombel' => $request->id_rombel,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->roomApi->create(json_encode($data));
        }

        if ($result['code'] == 200) {
            $room = $this->data_room($request['id_rombel']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'room' => $room
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $room  = $this->roomApi->get_by_id($request['id']);
        $result = $room['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'id_guru' => $request->id_guru,
            'id_mapel' => $request->id_mapel,
            'id_rombel' => $request->id_rombel,
            'id_sekolah' => session('id_sekolah'),
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->roomApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->roomApi->update_info(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $room = $this->data_room($request['id_rombel']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success', 
                'status' => 'berhasil',
                'room' => $room
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error', 
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        // dd($request);
        $delete = $this->roomApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $room = $this->data_room($request['id_rombel']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'room' => $room
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function updateNonAktif(Request $request)
    {
        $update = $this->roomApi->update_status_nonAktif($request['id']);
        return redirect()->back()->with('success', $update['body']['message']);
    }
    
    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'value' => $request['hasil']
        );
        $update = $this->roomApi->update_status_custom(json_encode($data));
        if ($update['code'] == 200) {
            $room = $this->data_room($request['id_rombel']);
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil',
                'room' => $room
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_kelas(Request $request)
    {
        $rombel = $this->roomApi->get_rombel_by_kelas($request['id_kelas']);
        // dd($rombel);
        $rombel = $rombel['body']['data'];
        $html = '';
        if (empty($rombel)) {
            $html .= '<tr><td colspan="5" class="text-center">Data untuk saat ini belum tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($rombel as $rmb) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$rmb['jurusan'].'</td>
                    <td>'.$rmb['kelas'].'</td>
                    <td>'.$rmb['nama'].'</td>
                    <td>
                        <button data-toggle="collapse" class="btn btn-success btn-sm" data-target="#room'.$rmb['id'].'"><i
                                class="fas fa-eye"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse" id="room'.$rmb['id'].'">
                            <button class="btn btn-info btn-sm my-3 pull-right"
                                onclick="tambahRoom('.$rmb['id'].')"><i
                                    class="fas fa-plus-circle"></i>
                                Tambah Room</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-success">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Guru</th>
                                        <th class="text-center">Mapel</th>
                                        <th class="text-center">Rombel</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_room'.$rmb['id'].'">';
                if (!empty($rmb['rooms'])) {
                    $nomer = 1;
                    foreach ($rmb['rooms'] as $room) {
                        $status = '<i class="fas fa-lock-open"></i>';
                        $kelas = 'danger';
                        if ($room['status_kode'] != 1) {
                            $status = '<i class="fas fa-lock"></i>';
                            $kelas = 'info';
                        }
                        $html .= '
                                        <tr>
                                            <td>'.$nomer++.'</td>
                                            <td>'.$room['guru'].'</td>
                                            <td>'.$room['mapel'].'</td>
                                            <td>'.$room['rombel'].'</td>
                                            <td class="text-center"><a href="javascript:void(0)" class="open_key text-'.$kelas.'" data-id="'.$room['id'].'" data-rombel="'.$room['id_rombel'].'" data-value="'.$room['status_kode'].'">'.$status.'</a></td>
                                            <td>
                                                <a href="javacript:void(0)" data-id="'.$room['id'].'" data-rombel="'.$room['id_rombel'].'" class="edit btn btn-purple btn-sm"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                                <a href="javacript:void(0)" data-id="'.$room['id'].'" data-rombel="'.$room['id_rombel'].'" class="delete btn btn-danger btn-sm"><i
                                                    class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        ';
                    }
                } else {
                    $html .= '<tr><td colspan="6" class="text-center">Data Room untuk saat ini tidak tersedia</td></tr>';
                }
                                
                $html .= '</tbody></table></div></td></tr>';
            }
        }
        return response()->json($html);
    }

    private function data_room($id_rombel)
    {
        $room = $this->roomApi->get_by_rombel($id_rombel);
        $room = $room['body']['data'];
        $html = '';
        if (!empty($room)) {
            $no = 1;
            foreach ($room as $rm) {
                $status = '<i class="fas fa-lock-open"></i>';
                $kelas = 'danger';
                if ($rm['status_kode'] != 1) {
                    $status = '<i class="fas fa-lock"></i>';
                    $kelas = 'info';
                }
                $html .= '
                <tr>
                <td>'.$no++.'</td>
                <td>'.$rm['guru'].'</td>
                <td>'.$rm['mapel'].'</td>
                <td>'.$rm['rombel'].'</td>
                <td class="text-center"><a href="javascript:void(0)" class="open_key text-'.$kelas.'" data-id="'.$rm['id'].'" data-rombel="'.$rm['id_rombel'].'" data-value="'.$rm['status_kode'].'">'.$status.'</a></td>
                <td>
                    <a href="javacript:void(0)" data-id="'.$rm['id'].'" data-rombel="'.$rm['id_rombel'].'" class="edit btn btn-purple btn-sm"><i
                        class="fas fa-pencil-alt"></i></a>
                    <a href="javacript:void(0)" data-id="'.$rm['id'].'" data-rombel="'.$rm['id_rombel'].'" class="delete btn btn-danger btn-sm"><i
                        class="fas fa-trash"></i></a>
                </td>
            </tr>
                ';
            }
        } else {
            $html .= '
            <tr><td colspan="6" class="text-center">Data Room untuk saat ini tidak tersedia</td></tr>
            ';
        }
        return $html;
    }
}
