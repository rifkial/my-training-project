<?php

namespace App\Http\Controllers\Perpus\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ApiService\Perpus\Buku\BukuApi;
use App\ApiService\Perpus\PustakaDigital\PustakaDigitalApi;
use App\ApiService\Perpus\Master\TopikApi;
use App\ApiService\Perpus\Master\TipeAnggotaApi;
use App\ApiService\Perpus\Dashboard\DashboardApi;
use App\ApiService\Perpus\Pengumuman\PengumumanApi;

class HomePerpusController extends Controller
{

    private $bukuApi,$topikApi,$pusdigApi,$dashboardApi,$pengumumanApi;

    function __construct(){
    	$this->bukuApi = new BukuApi();
        $this->topikApi = new TopikApi();
        $this->pusdigApi = new PustakaDigitalApi();
        $this->dashboardApi = new DashboardApi();
        $this->pengumumanApi = new PengumumanApi();
    }

    public function public_area(){
        $bukus = $this->bukuApi->get_home();
        $bukus = $bukus['body']['data'];

        $buku_fav = $this->bukuApi->get_favorite_home();
    	$buku_fav = $buku_fav['body']['data'];

        $pusdigs = $this->pusdigApi->get_home();
        $pusdigs = $pusdigs['body']['data'];

        $topiks = $this->topikApi->get_5();
        $topiks = $topiks['body']['data'];

        $pengumumans = $this->pengumumanApi->getAll();
        $pengumumans = $pengumumans['body']['data'];

        return view('content.perpus.home.home',compact('bukus','topiks','pusdigs','pengumumans','buku_fav'));

    }

    public function membershipInfo(){
        $tipeAnggota = new TipeAnggotaApi();
        $tipeAnggota = $tipeAnggota->get_all();
        $data = $tipeAnggota['body']['data'];

        return view('content.perpus.public.membership',compact('data'));
    }

    public function tentang(){
        return view('content.perpus.public.peraturan');
    }

    public function index(){
    	$bukus = $this->bukuApi->get_home();
    	$bukus = $bukus['body']['data'];

        $pusdigs = $this->pusdigApi->get_home();
        $pusdigs = $pusdigs['body']['data'];

        $topiks = $this->topikApi->get_5();
        $topiks = $topiks['body']['data'];

        if(session('role-perpus') == 'admin'){
            $peminjaman = $this->dashboardApi->get_peminjaman();
            $peminjaman = $peminjaman['body']['data'];

            $pengadaan = $this->dashboardApi->get_pengadaan();
            $pengadaan = $pengadaan['body']['data'];

            $user = $this->dashboardApi->get_user();
            $user = $user['body']['data'];

            $koleksi = $this->dashboardApi->get_koleksi();
            $koleksi = $koleksi['body']['data'];

            $koleksis = [];
            $koleksis['label']= [];
            $koleksis['value']= [];

            $user = $this->dashboardApi->get_user();
            $user = $user['body']['data'];


            foreach ($koleksi as $key => $value) {
                 array_push($koleksis['label'],$value['nama']);
                 array_push($koleksis['value'],$value['jumlah_pustaka']);
            }

            return view('content.perpus.home.home-admin')->with([
                'peminjaman' => json_encode($peminjaman),
                'pengadaan' => json_encode($pengadaan),
                'koleksi' => json_encode($koleksis),
                'user' => json_encode($user)
            ]);
        }else{
            return view('content.perpus.home.home',compact('bukus','topiks','pusdigs'));
        }


    }

    public function buku_latest(Request $request){
        $data = [
            'page' => $request->page
        ];

        $bukus = $this->bukuApi->get_home_all(json_encode($data));
        //dd($bukus['body']['data']);

        return view('content.perpus.buku.semua_buku')->with([
            'bukus' => $bukus['body']['datas'],
            'title' => 'terbaru',
            'data' => $bukus['body']['data']
        ]);
    }

    public function buku_fav(Request $request){
        $data = [
            'page' => $request->page
        ];

        $bukus = $this->bukuApi->get_favorite();

        return view('content.perpus.buku.semua_buku')->with([
            'bukus' => $bukus['body']['datas'],
            'title' => 'paling banyak dipinjam',
            'data' => $bukus['body']['data']
        ]);
    }

    public function pusdig_all(Request $request){
        $data = [
            'page' => $request->page
        ];

        $bukus = $this->pusdigApi->get_home_all();

        return view('content.perpus.pusdig.semua_pusdig')->with([
            'pusdigs' => $bukus['body']['datas'],
            'data' => $bukus['body']['data']
        ]);
    }
}
