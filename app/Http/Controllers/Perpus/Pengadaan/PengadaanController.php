<?php

namespace App\Http\Controllers\Perpus\Pengadaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Perpus\Pengadaan\PengadaanApi;
use App\ApiService\Perpus\Buku\BukuApi;
use Session;

class PengadaanController extends Controller
{
    private $pengadaanApi;
    private $bukuApi;

    function __construct(){
    	$this->pengadaanApi = new PengadaanApi();
    }

    function get_active(Request $request){
    	$pengadaans = $this->pengadaanApi->get_active();
    	$pengadaans = $pengadaans['body']['data'];

    	$this->bukuApi = new BukuApi();
    	$bukus = $this->bukuApi->get_all();
    	$bukus = $bukus['body']['data'];

    	if($request->ajax()){
            $table = datatables()->of($pengadaans);
            $table->addIndexColumn();

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
	                       		</button>
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                       		<button type="button" onclick="action_button('.$row['id'].')" class="tabledit-action-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-exclamation-circle"></i>
	                       		</button>
	                       	</div>
	                       	<span class="action-btn-'.$row['id'].'" style="float: none; display: none;">
		                       	<button type="button" onclick="submit_accept('.$row['id'].')" class="tabledit-accept-button-'.$row['id'].' btn btn-sm btn-success" >terima</button>
		                       	<button type="button" onclick="submit_reject('.$row['id'].')" class="tabledit-reject-button-'.$row['id'].' btn btn-sm btn-danger" >tolak</button>
	                       	</span>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi']);
            return $table->make(true);
        }

        return view('content.perpus.admin.pengadaan.data_pengadaan',compact('bukus'));
    }

    function get_accepted(Request $request){
    	$pengadaans = $this->pengadaanApi->get_accepted();
    	$pengadaans = $pengadaans['body']['data'];

    	if($request->ajax()){
            $table = datatables()->of($pengadaans);
            $table->addIndexColumn();

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi']);
            return $table->make(true);
        }

        return view('content.perpus.admin.pengadaan.pengadaan_diterima');
    }

    function get_rejected(Request $request){
    	$pengadaans = $this->pengadaanApi->get_rejected();
    	$pengadaans = $pengadaans['body']['data'];

    	if($request->ajax()){
            $table = datatables()->of($pengadaans);
            $table->addIndexColumn();

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi']);
            return $table->make(true);
        }

        return view('content.perpus.admin.pengadaan.pengadaan_ditolak');
    }

   

    function get_one(Request $request){
    	$id = $request->id;

    	$data = $this->pengadaanApi->get_detail($id);
    	//$data = $data['body']['data'];

    	if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function store(Request $request){
    	$data = [
    		'id_buku' => $request->id_buku,
    		'nama' => $request->nama,
    		'keterangan' => $request->keterangan,
    		'jumlah' => $request->jumlah,
    		'harga' => $request->harga,
    		'prioritas' => $request->prioritas,
    		'id_sekolah' => session('id_sekolah'),
    	];

    	if($request->id != null){
    		$data['id'] = $request->id;
    		$result = $this->pengadaanApi->update(json_encode($data));
    	}else{
    		$result = $this->pengadaanApi->create(json_encode($data));
    	}

    	

    	if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function action(Request $request){
    	$id = $request->id;
    	$act = $request->act;
    	$result = $this->pengadaanApi->action($id,$act);

    	if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function delete(Request $request){
    	$id = $request->id;
    	$result = $this->pengadaanApi->soft_delete($id);

    	if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

}
