<?php

namespace App\Http\Controllers\Perpus\Profile;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Perpus\User\UserPerpusApi;
use App\ApiService\Perpus\Master\TipeAnggotaApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\Perpus\Kartu\KartuApi;
use App\ApiService\Perpus\Peminjaman\PeminjamanApi;
use Image;


class UserPerpusController extends Controller
{
    private $userApi,$profileApi;

    function __construct(){
    	$this->userApi = new UserPerpusApi();
    	$this->profileApi = new ProfileApi();
    }

    function index(){
    	$user = $this->userApi->get_by_id(session('id'));
    	$user = $user['body']['data'];
    	//keangggotaan
    	$anggota = $this->userApi->get_keanggotaan( session('id') );
    	$anggota = $anggota['body']['data'];


    	return view('content.perpus.profile.profile_user',compact('user','anggota'));
    }

    function update_profile(Request $request){
    	$data = array(
    		'id' => session('id'),
    		'nama' => $request->nama,
            'username' => $request->username,
            'no_induk' => $request->no_induk,
            'no_anggota' => $request->no_anggota,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'tempat_lahir' =>$request->tempat_lahir,
            'tgl_lahir' =>$request->tgl_lahir,
            'jenkel' => $request->jenkel,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah')
    	);

    	if ($files = $request->file('foto')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $basePath = "file/perpus/profile/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;

            $result = $this->profileApi->update_profile_image(json_encode($data));
            File::delete($path);
            //dd($result);
            $code = $result['code'];

        }else{
            $result = $this->profileApi->update_profile(json_encode($data));
        }

        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();
    }

    function update_password(Request $request){

    	$data = array(
    		'current_password' => $request->current_password,
    		'new_password' => $request->new_password,
            'confirm_password' => $request->confirm_password
    	);

    	$result = $this->profileApi->update_profile(json_encode($data));

    	$message = $result['body']['message'];

    	Session::flash('message', $message);

        return redirect()->back();
    }

    function peminjaman(Request $request){
        $peminjamanApi = new PeminjamanApi();
        $datas = $peminjamanApi->get_user(session('id'));
        $datas = $datas['body']['data'];

        if($request->ajax()){
            $table = datatables()->of($datas);
            $table->addIndexColumn();

            $table->editColumn('buku', function ($row) {
                if($row['status'] == 'terlambat'){
                    $terlambat = '
                    <div class="text-danger">
                        terlambat '.$row['terlambat'].' hari
                    </div>';
                }else{
                    $terlambat = '';
                }

                return '
                    <h6 class="my-0">'.$row['buku'].'</h6>
                    '.$terlambat.'
                ';
            });

            $table->editColumn('denda',function($row){
                return '
                    <p> '.$row['denda'].'</p>
                ';
            });

            $table->rawColumns(['buku','denda']);

            return $table->make(true);
        }

    	return view('content.perpus.profile.user.peminjaman_user',compact('datas'));
    }

    function denda(Request $request){
    	$datas = $this->userApi->get_denda( session('id') );
    	$datas = $datas['body']['data'];

        $data = $this->userApi->get_denda_berjalan( session('id') );

        $total = $data['body']['total'];

    	if($request->ajax()){
            $table = datatables()->of($datas);
            $table->addIndexColumn();
            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="modalDetailDenda('.$row['id'].')" id="btnInfo-'.$row['id'].'" class="btn btn-light btn-sm mt-0 mr-1">
                         <i class="fas fa-info-circle"></i></i>
                    </a>
                </div>';
            });
            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }

    	return view('content.perpus.profile.user.denda_user',compact('datas','data','total'));
    }


    function getAll(Request $request){
        $user = $this->userApi->get_all();
        $user = $user['body']['data'];

        $tipeApi = new TipeAnggotaApi();
        $tipe = $tipeApi->get_all();
        $tipe = $tipe['body']['data'];
        //dd($user);

        if($request->ajax()){
            $table = datatables()->of($user);
            $table->addIndexColumn();

            $table->editColumn('checkbox',function ($row) {
                return '
                <input type="checkbox" class="check_print" name="data[]" value="'.$row['id'].'">
                ';
            });

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                           <div class="btn-group btn-group-sm" style="float: none;">
                                <button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
                                </button>
                                <a href="'.route("user-perpus-details",$row['id']).'" class="tabledit-detail-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;">
                                    <i class="fas fa-info-circle"></i>
                                </a>
                                <button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                       </div>
                   </td>
                ';
            });

            $table->rawColumns(['aksi','checkbox']);

            return $table->make(true);
        }

        return view('content.perpus.admin.user.user',compact('tipe'));

    }

    function get_by_nama(Request $request){
        $data = [
            'nama' => $request->nama
        ];

        $data = $this->userApi->get_by_nama(json_encode($data));

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    //perpanjang masa aktif keanggotan
    function extends(Request $request){
        $id = $request->id;
        $data = $this->userApi->extend($id);

        return response()->json([
            'message' => "proses perpanjangan berhasil ",
            'icon'  => 'success',
            'status'  => 'berhasil'
        ]);
    }

    public function store(Request $request){

        if( $request->hasFile( 'image' ) ) {
            $image = $request->file( 'image' );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }

        $data = [
            'nama' => $request->nama,
            'no_induk' => $request->no_induk ,
            'username' => $request->username ,
            'id_tipe_anggota' => $request->tipe_anggota ,
            'email' => $request->email ,
            'jenkel' => $request->jenkel ,
            'telepon' => $request->telepon ,
            'tempat_lahir' => $request->tempat_lahir ,
            'tgl_lahir' => $request->tgl_lahir ,
            'alamat' => $request->alamat ,
            'role' => $request->role ,
            'foto' => $fullImage,
            'id_sekolah' => session('id_sekolah')
        ];

        if($request->id != null){
            $data['id'] = $request->id;
            $result = $this->userApi->update(json_encode($data));
        }else{
            $data['first_password'] = $request->password ;
            $result = $this->userApi->create(json_encode($data));
        }



        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();

    }

    function detail($id){
        //data user
        $user = $this->userApi->get_by_id($id);

        //data peminjaman
        $peminjamans = $this->userApi->get_peminjaman($id);
        //dd($peminjamans);

        //kartu
        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $keanggotaan = $this->userApi->get_keanggotaan($id);

        Session::put('id_peminjam', $id);

        $user = $user['body']['data'];
        $peminjamans = $peminjamans['body']['data'];
        $keanggotaan = $keanggotaan['body']['data'];

        return view('content.perpus.admin.user.user-detail',compact(['user','peminjamans','keanggotaan','kartu']));
    }

    public function getOne(Request $request){
        $id = $request->id;

        $data = $this->userApi->get_by_id($id);

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    public function findMany(Request $request){
        $data = [
            'id' => $request->id
        ];

        $data = $this->userApi->get_many(json_encode($data));

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    public function printMany(Request $request){
        $kartuApi = new KartuApi();
        $kartu = $kartuApi->get_detail(1);
        $kartu = $kartu['body']['data'];

        $data = [
            'id' => $request->data
        ];

        $data = $this->userApi->get_many(json_encode($data));
        $data = $data["body"]["data"];
        //dd($data);

        return view('content.perpus.admin.user.cetak',compact('kartu','data'));


    }

    public function delete(Request $request){
        $id = $request->id;

        $data = $this->userApi->soft_delete($id);

        if ($data['code'] == 200) {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

}
