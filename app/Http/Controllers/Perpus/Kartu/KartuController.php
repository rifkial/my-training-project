<?php

namespace App\Http\Controllers\Perpus\Kartu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\ApiService\Perpus\Kartu\KartuApi;
use App\ApiService\Perpus\User\UserPerpusApi;
use Image;

class KartuController extends Controller
{
    private $kartuApi;

    function __construct(){
    	$this->kartuApi = new KartuApi();
    }

    function index(){
    	$kartu = $this->kartuApi->get_detail(1);
    	$kartu = $kartu['body']['data'];
    	//dd($kartu);

    	return view('content.perpus.admin.kartu.kartu',compact('kartu'));
    }

    function image($request,$data){
    	if( $request->hasFile( $data ) ) {
            $image = $request->file( $data );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }

        return $fullImage;
    }

    function store(Request $request){
    	$cover = $this->image($request,'cover');
    	$stempel = $this->image($request,'stempel');
    	$ttd = $this->image($request,'ttd');
    	$logo = $this->image($request,'logo');


    	$data = [
    		'id' => $request->id,
    		'nama_kartu' => $request->nama_kartu,
    		'nama_sistem' => $request->nama_sistem,
    		'nama_instansi' => $request->nama_instansi,
    		'nama_jabatan' => $request->nama_jabatan,
            'peraturan' => $request->peraturan,
    		'jabatan' => $request->jabatan,
    		'nip_jabatan' => $request->nip_jabatan,
    		'cover' => $cover,
    		'stempel' => $stempel,
    		'ttd' => $ttd,
    		'logo' => $logo,
    		'id_sekolah' => session('id_sekolah'),
    	];

    	//dd($data);

    	$result = $this->kartuApi->update(json_encode($data));
    	//dd($result);

    	$message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();
    }

    function print_page(){
        return view('content.perpus.cetak.cetak');
    }

    function get_user(Request $request){
        $userApi = new UserPerpusApi();
        $users = $userApi->get_by_nama($request->nama);
        $users = $users['body']['data'];
    }

    function get_one(){
        $kartu = $this->kartuApi->get_detail(1);
        //$kartu = $kartu['body']['data'];

        if ($kartu['code'] == 200) {
            return response()->json([
                'data' => $kartu["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $kartu["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

}
