<?php

namespace App\Http\Controllers\Perpus\Eskemplar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Item\ItemApi;
use App\ApiService\Perpus\Buku\BukuApi;
use App\ApiService\Perpus\Label\LabelApi;
use App\ApiService\Perpus\Peminjaman\PeminjamanApi;
use App\ApiService\Perpus\Master\RakApi;
use Session;

class EskemplarController extends Controller
{
    private $itemApi;

    function __construct(){
    	$this->itemApi = new ItemApi();
    	$this->bukuApi = new BukuApi();
    }

    function get_by_book(Request $request,$id){
        $peminjamanApi = new PeminjamanApi();
        $peminjaman = $peminjamanApi->get_book($id);
        $peminjaman = $peminjaman['body']['data'];

        if($request->ajax()){
            $table = datatables()->of($peminjaman);
            $table->addIndexColumn();

            return $table->make(true);
        }

    	$buku = $this->bukuApi->get_detail($id);
    	$buku = $buku['body']['data'];

    	$items = $this->itemApi->get_by_buku($id);
        //dd($items);
        $items = $items['body']['data'];

        $labelApi = new LabelApi();
        $label = $labelApi->get_detail(1);
        $label = $label['body']['data'];

        //dd($items);
    	return view('content.perpus.admin.eskemplar.eskemplar_book',compact('buku','items','label'));
    }

    function get_by_kode(Request $request){
        $data = [
            'kode' => $request->kode
        ];

        $data = $this->itemApi->get_by_kode(json_encode($data));


        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data,
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    //tidak dipakai (error)
    function find_many(Request $request){
        $data = [
            'id' => $request->id
        ];

        $data = $this->itemApi->findMany(json_encode($data));


        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data,
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function print_many(Request $request){

        $data = [
            'id' => $request->items
        ];

        $labelApi = new LabelApi();
        $label = $labelApi->get_detail(1);
        $label = $label['body']['data'];

        $data = $this->itemApi->findMany(json_encode($data));
        $data = $data['body']['data'];

        return view('components.perpus.buku.cetak',compact('label','data'));

    }

    function print_items(Request $request){
        $datas = $request->data;
        $data =[];
        foreach($datas as $dt){
            $data[] = [
                'kode' => $dt
            ];
        }

        //dd($data);
        //dd($data);
        $labelApi = new LabelApi();
        $label = $labelApi->get_detail(1);
        $label = $label['body']['data'];
        return view('components.perpus.buku.cetak',compact('label','data'));
    }

    function get_one(Request $request){
        $id = $request->id;

        $data = $this->itemApi->get_detail($id);

        if ($data['code'] == 200) {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $data["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function delete(Request $request){
        $id = $request->id;

        $data = $this->itemApi->soft_delete($id);

        if ($data['code'] == 200) {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

     }


    function store(Request $request){
        $data = [
            'id' => $request->id,
            'kode' => $request->kode,
            'id_status' => $request->kondisi,
            'tgl_masuk' => $request->tanggal
        ];

        $data = $this->itemApi->update(json_encode($data));

        if ($data['code'] == 200) {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $data["body"]["message"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }
}
