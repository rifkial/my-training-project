<?php

namespace App\Http\Controllers\Perpus\Peminjaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Peminjaman\PeminjamanApi;
use App\ApiService\Perpus\User\UserPerpusApi;
use Session;

class PeminjamanController extends Controller
{
    private $peminjamanApi,$userApi;

    function __construct(){
    	$this->peminjamanApi = new PeminjamanApi();
        $this->userApi = new UserPerpusApi();
    }

    //peminjaman berjalan
    function get_active(Request $request){
    	$peminjamans = $this->peminjamanApi->get_dipinjam();
    	$peminjamans = $peminjamans['body']['data'];

        //dd($peminjamans);

    	if($request->ajax()){
            $table = datatables()->of($peminjamans);
            $table->addIndexColumn();

            $table->editColumn('peminjam', function ($row) {
                if($row['status'] == 'terlambat'){
                    $terlambat = '
                    <div class="text-danger">
                        terlambat '.$row['terlambat'].' hari
                    </div>';
                }else{
                    $terlambat = '';
                }

                return '
                    <h6 class="my-0">'.$row['peminjam'].'</h6>
                    '.$terlambat.'
                ';
            });

            $table->editColumn('aksi',function($row){

                if($row['status_dipinjam'] == 'dipinjam'){
                    return '
                    <button id="btnReturn-'.$row['id'].'" onclick="returned('.$row['id'].')"  type="button" class="btn btn-primary btn-sm">Kembali</button>
                    <button id="btnExtend-'.$row['id'].'" onclick="perpanjang('.$row['id'].')"  type="button" class="btn btn-success btn-sm">Perpanjang</button>
                    ';
                }else{
                    return '';
                }

            });

            $table->rawColumns(['peminjam','aksi']);

            return $table->make(true);
        }

    	return view('content.perpus.admin.peminjaman.data_peminjaman');
    }

    //peminjaman kembali
    function get_return(Request $request){
        $peminjamans = $this->peminjamanApi->get_kembali();
        $peminjamans = $peminjamans['body']['data'];

        if($request->ajax()){
            $table = datatables()->of($peminjamans);
            $table->addIndexColumn();

            return $table->make(true);
        }

        return view('content.perpus.admin.peminjaman.data_pengembalian');
    }

    //perpanjang masa aktif keanggotan
    function extends(Request $request){
        $id = $request->id;
        $data = $this->peminjamanApi->extend($id);

        if($data['code'] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else{
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }


    }

    function add_new(Request $request){
        $users = $this->userApi->get_by_sekolah();
        $users = $users['body']['data'];

        return view('content.perpus.admin.peminjaman.peminjaman',compact('users'));
    }

    function pengembalian_page(){
        $users = $this->userApi->get_by_sekolah();
        $users = $users['body']['data'];

        return view('content.perpus.admin.peminjaman.data_peminjaman_user',compact('users'));
    }

    function get_peminjaman_buku(Request $request){
        $id = $request->id;
        $data = $this->peminjamanApi->get_book($id);
        $data = $data['body']['data'];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    function get_peminjaman_user(Request $request){

        $data = $this->peminjamanApi->get_user(session('id_peminjam'));
        $data = $data['body']['data'];


        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('buku', function ($row) {
                if($row['status'] == 'terlambat'){
                    $terlambat = '
                    <div class="text-danger">
                        terlambat '.$row['terlambat'].' hari
                    </div>';
                }else{
                    $terlambat = '';
                }

                return '
                    <h6 class="my-0">'.$row['buku'].'</h6>
                    '.$terlambat.'
                ';
            });

            $table->editColumn('denda',function($row){
                return '
                    <p> '.$row['denda'].'</p>
                ';
            });

            $table->editColumn('aksi',function($row){

                if($row['status_peminjaman'] == 'dipinjam'){
                    return '
                    <button id="btnReturn-'.$row['id'].'" onclick="returned('.$row['id'].')"  type="button" class="btn btn-primary btn-sm">Kembali</button>
                    <button id="btnExtend-'.$row['id'].'" onclick="perpanjang('.$row['id'].')"  type="button" class="btn btn-success btn-sm">Perpanjang</button>
                    ';
                }else{
                    return '';
                }

            });

            $table->rawColumns(['buku','denda','aksi']);

            return $table->make(true);
        }

    }

    function sirkulasi(){
        return view('content.perpus.admin.peminjaman.sirkulasi');
    }

    function get_by_kode(Request $request){
        $data = [
            'kode' => $request->kode
        ];

        $data = $this->peminjamanApi->create(json_encode($data));
    }

    function store(Request $request){
        $data = [
            'id_item' => $request->id_item,
            'id_user' => session('id_peminjam'),
            'id_admin' => session('id'),
            'keterangan' => $request->keterangan,
            'tgl_pinjam' => $request->tgl_pinjam,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->peminjamanApi->create(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function pengembalian(Request $request){
        $id = $request->id;
        $data = $this->peminjamanApi->pengembalian($id);

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

}
