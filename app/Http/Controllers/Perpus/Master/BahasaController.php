<?php

namespace App\Http\Controllers\Perpus\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Master\BahasaApi;


class BahasaController extends Controller
{
    private $bahasaApi;

    function __construct(){
    	$this->bahasaApi = new BahasaApi();
    }

    function get_all(Request $request){
    	$bahasa = $this->bahasaApi->get_all();
    	$bahasa = $bahasa['body']['data'];

    	//dd($bahasa);

    	if($request->ajax()){
    		$table = datatables()->of($bahasa);
            $table->addIndexColumn();

            $table->editColumn('kode',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-kode-'.$row['id'].'" >'.$row['kode'].'</span>
                   	<input class="tabledit-input form-control input-sm input-kode-'.$row['id'].'" type="text" name="kode" value="'.$row['kode'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('nama',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-nama-'.$row['id'].'">'.$row['nama'].'</span>
                   	<input class="tabledit-input form-control input-sm input-nama-'.$row['id'].'" type="text" name="nama" value="'.$row['nama'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
	                       		</button>
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                       	</div>
	                       <button type="button" onclick="submit_edit('.$row['id'].')" class="tabledit-save-button-'.$row['id'].' btn btn-sm btn-success" style="float: none; display: none;">Save</button>
	                       <button type="button" class="tabledit-confirm-button btn btn-sm btn-danger" style="display: none; float: none;">Confirm</button>
	                       <button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi','kode','nama']);
            return $table->make(true);
    	}

    	return view('content.perpus.admin.bahasa.bahasa');
    }

    function get_one(Request $request){
    	$id = $request->id;
    	$bahasa = $this->bahasaApi->get_detail($id);

    	if ($bahasa['code'] == 200) {
            return response()->json([
                'data' => $bahasa["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $bahasa["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function store(Request $request){
        $data = [
            'kode' => $request->kode,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->bahasaApi->create(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function update(Request $request){
        $data = [
        	'id' => $request->id,
            'kode' => $request->kode,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->bahasaApi->update(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function delete(Request $request){
        $id = $request->id;

        $result = $this->bahasaApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


}
