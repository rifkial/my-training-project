<?php

namespace App\Http\Controllers\Perpus\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Perpus\Master\AgenApi;


class AgenController extends Controller
{
    private $agenApi;

    function __construct(){
    	$this->agenApi = new AgenApi();
    }

    function get_all(Request $request){
    	$agen = $this->agenApi->get_all();
    	$agen = $agen['body']['data'];

    	//dd($agen);

    	if($request->ajax()){
    		$table = datatables()->of($agen);
            $table->addIndexColumn();

            $table->editColumn('alamat',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-alamat-'.$row['id'].'" >'.$row['alamat'].'</span>
                   	<input class="tabledit-input form-control input-sm input-alamat-'.$row['id'].'" type="text" name="alamat" value="'.$row['alamat'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('kontak',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-kontak-'.$row['id'].'" >'.$row['kontak'].'</span>
                   	<input class="tabledit-input form-control input-sm input-kontak-'.$row['id'].'" type="text" name="kontak" value="'.$row['kontak'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('email',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-email-'.$row['id'].'" >'.$row['email'].'</span>
                   	<input class="tabledit-input form-control input-sm input-email-'.$row['id'].'" type="email" name="email" value="'.$row['email'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('telepon',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-telepon-'.$row['id'].'" >'.$row['telepon'].'</span>
                   	<input class="tabledit-input form-control input-sm input-telepon-'.$row['id'].'" type="number" name="telepon" value="'.$row['telepon'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('nama',function ($row) {
                return '
                   <td class="tabledit-view-mode">
                   	<span class="tabledit-span show-nama-'.$row['id'].'">'.$row['nama'].'</span>
                   	<input class="tabledit-input form-control input-sm input-nama-'.$row['id'].'" type="text" name="nama" value="'.$row['nama'].'" style="display: none;" >
                   </td>
                ';
            });

            $table->editColumn('aksi',function ($row) {
                return '
                   <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                       <div class="btn-group btn-group-sm" style="float: none;">
	                       		<button onclick="edit_button('.$row['id'].')" type="button" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-pencil-alt"></i>
	                       		</button>
	                       		<button type="button" onclick="submit_delete('.$row['id'].')" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-default" style="float: none;"><i class="fas fa-trash-alt"></i>
	                       		</button>
	                       	</div>
	                       <button type="button" onclick="submit_edit('.$row['id'].')" class="tabledit-save-button-'.$row['id'].' btn btn-sm btn-success" style="float: none; display: none;">Save</button>
	                       <button type="button" class="tabledit-confirm-button btn btn-sm btn-danger" style="display: none; float: none;">Confirm</button>
	                       <button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button>
	                   </div>
	               </td>
                ';
            });

            $table->rawColumns(['aksi','email','nama','telepon','alamat','kontak']);
            return $table->make(true);
    	}

    	return view('content.perpus.admin.agen.agen');
    }

    function get_one(Request $request){
    	$id = $request->id;
    	$agen = $this->agenApi->get_detail($id);

    	if ($agen['code'] == 200) {
            return response()->json([
                'data' => $agen["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $agen["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function store(Request $request){
        $data = [
            'kontak' => $request->kontak,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->agenApi->create(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function update(Request $request){
        $data = [
        	'id' => $request->id,
            'kontak' => $request->kontak,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah')
        ];

        $data = $this->agenApi->update(json_encode($data));

        if($data["code"] == 200){
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $data['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }  
    }

    function delete(Request $request){
        $id = $request->id;

        $result = $this->agenApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


}
