<?php

namespace App\Http\Controllers\Perpus\PustakaDigital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\ApiService\Perpus\PustakaDigital\PustakaDigitalApi;
use App\ApiService\Perpus\Master;
use Image;


class PustakaDigitalController extends Controller
{
    private $bukuApi;
    private $agen,$bahasa,$gmd,$jenis,$koleksi,$penerbit,$pengarang,$rak,$topik;

    function __construct(){
        $this->middleware('check.api.token')->only(['store','ajax_detail','admin_get_pusdig','store']);
    	$this->bukuApi = new PustakaDigitalApi();
    }

    function detail_pusdig($id){
    	$buku = $this->bukuApi->get_detail($id);
    	$buku = $buku['body']['data'];

        return view('content.perpus.pusdig.detail_pusdig',compact('buku'));

    }

    function ajax_detail(Request $request){
        $id = $request->id;

        $buku = $this->bukuApi->get_detail($id);
        if($buku['code'] == 200){
            return response()->json([
                'data' => $buku['body']['data'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $buku['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);

        }
    }

    function read_pdf($id){
        return view('components.perpus.pusdig.baca');
    }

    function search_judul(Request $request){
        $data = [
            'value' => $request->nama
        ];

        $bukus = $this->bukuApi->search_judul(json_encode($data));
        $bukus = $bukus['body']['data'];
    }

    function init_master(){
        $bahasaApi = new Master\BahasaApi();
        $jenisApi = new Master\JenisApi();
        $koleksiApi = new Master\KoleksiApi();
        $penerbitApi = new Master\PenerbitApi();
        $pengarangApi = new Master\PengarangApi();
        $topikApi = new Master\TopikApi();

        $this->bahasa = $bahasaApi->get_all();
        $this->jenis = $jenisApi->get_all();
        $this->koleksi = $koleksiApi->get_all();
        $this->penerbit = $penerbitApi->get_all();
        $this->pengarang = $pengarangApi->get_all();
        $this->topik = $topikApi->get_all();

        $this->bahasa = $this->bahasa['body']['data'];
        $this->jenis = $this->jenis['body']['data'];
        $this->koleksi = $this->koleksi['body']['data'];
        $this->penerbit = $this->penerbit['body']['data'];
        $this->pengarang = $this->pengarang['body']['data'];
        $this->topik = $this->topik['body']['data'];
    }

    function admin_get_pusdig(Request $request){

    	$bukus = $this->bukuApi->get_all();
        $bukus = $bukus['body']['data'];


        $this->init_master();

        if($request->ajax()){
            $table = datatables()->of($bukus);
            $table->addIndexColumn();

            $table->editColumn('lampiran', function ($row) {
                if($row['lampiran'] != null){
                    return '<a href="'.$row['lampiran'].'">lampiran</a>';
                }else{
                    return '<a>Tidak ada</a>';
                }

            });

            $table->editColumn('aksi',function ($row) {
                return '
                    <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                        <div class="btn-group btn-group-sm" style="float: none;">
                            <button type="button" onclick="modalDetailBuku('.$row['id'].')" class="tabledit-detail-button-'.$row['id'].' btn btn-sm btn-secondary" style="float: none;">
                                <i class="fas fa-info-circle"></i>
                            </button>
                            <button type="button" onclick="modalEditBuku('.$row['id'].')" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-success" style="float: none;">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                        </div>
                    </td>
                ';
            });

            $table->rawColumns(['lampiran','aksi']);

            return $table->make(true);
        }

        return view('content.perpus.admin.pusdig.pusdig_list',
            ['bukus' => $bukus ,
             'bahasa' => $this->bahasa ,
             'jenis' => $this->jenis , 'koleksi' => $this->koleksi ,
             'penerbit' => $this->penerbit , 'pengarang' => $this->pengarang ,
             'topik' => $this->topik
            ]
        );

    }

    function store(Request $request){

        if( $request->hasFile( 'image' ) ) {
            $image = $request->file( 'image' );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }

        $penulis = implode(",",$request->id_penulis);
        $topik = implode(",",$request->id_topik);

        $data = [
            'isbn' => $request->isbn,
            'kode' => $request->kode,
            'judul' => $request->judul,
            'halaman' => $request->halaman,
            'tahun_terbit' => $request->tahun_terbit,
            'foto' => $fullImage,
            'deskripsi' => $request->deskripsi,
            'id_bahasa' => $request->id_bahasa,
            'id_jenis' => $request->id_jenis,
            'id_koleksi' => $request->id_koleksi,
            'id_penerbit' => $request->id_penerbit,
            'id_topik' => $topik,
            'id_penulis' => $penulis,
            'id_sekolah' => session('id_sekolah')
        ];



        if($request->ids != null){
            $data['id'] = $request->ids;

            if ($files = $request->file('lampiran')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $basePath = "file/perpus/lampiran/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $namaFile);
                $path = $basePath . $namaFile;
                $data['path'] = $path;
                $result = $this->bukuApi->update_file(json_encode($data));
                File::delete($path);

            }else{
                $result = $this->bukuApi->update(json_encode($data));
            }

        }else{

            if ($files = $request->file('lampiran')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $basePath = "file/perpus/lampiran/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $namaFile);
                $path = $basePath . $namaFile;
                $data['path'] = $path;
                $result = $this->bukuApi->create_file(json_encode($data));
                File::delete($path);

            }else{

                $result = $this->bukuApi->create(json_encode($data));
            }
        }

        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();

    }



}
