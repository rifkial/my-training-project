<?php

namespace App\Http\Controllers\Perpus\Buku;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\ApiService\Perpus\Buku\BukuApi;
use App\ApiService\Perpus\PustakaDigital\PustakaDigitalApi;
use App\ApiService\Perpus\Item\ItemApi;
use App\ApiService\Perpus\Master;
use Image;


class BukuController extends Controller
{
    private $bukuApi,$itemApi;
    private $agen,$bahasa,$gmd,$jenis,$koleksi,$penerbit,$pengarang,$rak,$topik;

    function __construct(){
        $this->middleware('check.api.token')->only(['store','ajax_detail','admin_get_buku','store']);
    	$this->bukuApi = new BukuApi();
        $this->itemApi = new ItemApi();
    }

    //public
    function detail_buku($id){
    	$buku = $this->bukuApi->get_detail($id);
    	$buku = $buku['body']['data'];

        $items = $this->itemApi->get_by_buku($id);
        $items = $items['body']['data'];

    	return view('content.perpus.buku.detail_buku',compact('buku','items'));
    }

    function ajax_detail(Request $request){
        $id = $request->id;

        $buku = $this->bukuApi->get_detail($id);
        if($buku['code'] == 200){
            return response()->json([
                'data' => $buku['body']['data'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $buku['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);

        }
    }

    function search_judul(Request $request){
        $pusdigApi = new PustakaDigitalApi();

        $data = [
            'value' => $request->nama
        ];

        $bukus = $this->bukuApi->search_judul(json_encode($data));
        $bukus = $bukus['body']['data'];

        $pusdigs = $pusdigApi->search_judul(json_encode($data));
        $pusdigs = $pusdigs['body']['data'];

        return view('content.perpus.buku.pencarian_buku',compact('bukus','pusdigs'));
    }

    function search_advance(Request $request){
        $pusdigApi = new PustakaDigitalApi();

        $data = [
            'judul' => $request->judul,
            'ISBN' => $request->isbn,
            'tahun_terbit' => $request->tahun,
            'id_jenis' => $request->id_jenis,
            'id_gmd' => $request->id_gmd,
            'id_koleksi' => $request->id_koleksi,
            'id_bahasa' => $request->id_bahasa,
        ];

        $bukus = $this->bukuApi->search_advance(json_encode($data));
        $bukus = $bukus['body']['data'];

        $pusdigs = $pusdigApi->search_advance(json_encode($data));
        $pusdigs = $pusdigs['body']['data'];

        return view('content.perpus.buku.pencarian_buku',compact('bukus','pusdigs'));
    }

    function init_master(){
        $agenApi = new Master\AgenApi();
        $bahasaApi = new Master\BahasaApi();
        $gmdApi = new Master\GmdApi();
        $jenisApi = new Master\JenisApi();
        $koleksiApi = new Master\KoleksiApi();
        $penerbitApi = new Master\PenerbitApi();
        $pengarangApi = new Master\PengarangApi();
        $rakApi = new Master\RakApi();
        $topikApi = new Master\TopikApi();

        $this->agen = $agenApi->get_all();
        $this->bahasa = $bahasaApi->get_all();
        $this->gmd = $gmdApi->get_all();
        $this->jenis = $jenisApi->get_all();
        $this->koleksi = $koleksiApi->get_all();
        $this->penerbit = $penerbitApi->get_all();
        $this->pengarang = $pengarangApi->get_all();
        $this->rak = $rakApi->get_all();
        $this->topik = $topikApi->get_all();

        $this->agen = $this->agen['body']['data'];
        $this->bahasa = $this->bahasa['body']['data'];
        $this->gmd = $this->gmd['body']['data'];
        $this->jenis = $this->jenis['body']['data'];
        $this->koleksi = $this->koleksi['body']['data'];
        $this->penerbit = $this->penerbit['body']['data'];
        $this->pengarang = $this->pengarang['body']['data'];
        $this->rak = $this->rak['body']['data'];
        $this->topik = $this->topik['body']['data'];
    }

    function admin_get_buku(Request $request){

        $bukus = $this->bukuApi->get_all();
        $bukus = $bukus['body']['data'];


        //Session::put('kode', 'ini session');

        $this->init_master();

        if($request->ajax()){
            $table = datatables()->of($bukus);
            $table->addIndexColumn();

            $table->editColumn('checkbox',function ($row) {
                return '
                <input type="checkbox" class="check_print" name="items[]" value="'.$row['id'].'">
                ';
            });

            $table->editColumn('lampiran', function ($row) {
                if($row['lampiran'] != null){
                    return '<a href="'.$row['lampiran'].'">lampiran</a>';
                }else{
                    return '<a>Tidak ada</a>';
                }

            });

            $table->editColumn('aksi',function ($row) {
                return '
                    <td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                        <div class="btn-group btn-group-sm" style="float: none;">
                            <button type="button" onclick="modalDetailBuku('.$row['id'].')" class="tabledit-detail-button-'.$row['id'].' btn btn-sm btn-secondary" style="float: none;">
                                <i class="fas fa-info-circle"></i>
                            </button>
                            <button type="button" onclick="modalEditBuku('.$row['id'].')" class="tabledit-edit-button-'.$row['id'].' btn btn-sm btn-success" style="float: none;">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                            <a href="'.route("item-by-book",$row['id']).'" type="button" class="tabledit-delete-button-'.$row['id'].' btn btn-sm btn-danger mt-0" style="float: none;">
                                <i class="fas fa-book"></i>
                            </a>
                        </div>
                    </td>
                ';
            });

            $table->rawColumns(['checkbox','lampiran','aksi']);

            return $table->make(true);
        }

        return view('content.perpus.admin.buku.buku_list',
            ['bukus' => $bukus , 'agen' => $this->agen ,
             'bahasa' => $this->bahasa , 'gmd' => $this->gmd ,
             'jenis' => $this->jenis , 'koleksi' => $this->koleksi ,
             'penerbit' => $this->penerbit , 'pengarang' => $this->pengarang ,
             'rak' => $this->rak , 'topik' => $this->topik
            ]
        );

    }

    function store(Request $request){

        if( $request->hasFile( 'image' ) ) {
            $image = $request->file( 'image' );
            //dd($image);
            $imageType = $image->getClientOriginalExtension();
            $imageStr = (string) Image::make( $image )->
                                     resize( 300, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );

            $image = base64_encode( $imageStr );
            $imageType = $imageType;
            $fullImage = 'data:image/'.$imageType.';base64,'.$image;
        }else{
            $fullImage = '';
        }

        if(!empty($request->id_penulis)){
            $penulis = implode(",",$request->id_penulis);
        }else{
            $penulis = null;
        }

        if(!empty($request->id_topik)){
            $topik = implode(",",$request->id_topik);
        }else{
            $topik = null;
        }

        $data = [
            'isbn' => $request->isbn ,
            'kode' => $request->kode,
            'judul' => $request->judul,
            'halaman' => $request->halaman,
            'tahun_terbit' => $request->tahun_terbit,
            'foto' => $fullImage,
            'desc_fisik' => $request->desc_fisik,
            'desc_buku' => $request->desc_buku,
            'id_gmd' => $request->id_gmd,
            'id_bahasa' => $request->id_bahasa,
            'id_rak' => $request->id_lokasi,
            'id_agen' => $request->id_agen,
            'id_jenis' => $request->id_jenis,
            'id_koleksi' => $request->id_koleksi,
            'id_penerbit' => $request->id_penerbit,
            'id_topik' => $topik,
            'id_penulis' => $penulis,
            'id_sekolah' => session('id_sekolah')
        ];


        if($request->ids != null){
            $data['id'] = $request->ids;

            if ($files = $request->file('lampiran')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $basePath = "file/perpus/lampiran/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $namaFile);
                $path = $basePath . $namaFile;
                $data['path'] = $path;
                $result = $this->bukuApi->update_file(json_encode($data));
                File::delete($path);

            }else{
                $result = $this->bukuApi->update(json_encode($data));
            }

        }else{
            $data['jumlah'] = $request->jumlah;

            if ($files = $request->file('lampiran')) {
                $namaFile = $files->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $basePath = "file/perpus/lampiran/";
                Help::check_and_make_dir($basePath);
                $files->move($basePath, $namaFile);
                $path = $basePath . $namaFile;
                $data['path'] = $path;
                $result = $this->bukuApi->create_file(json_encode($data));
                File::delete($path);

            }else{

                $result = $this->bukuApi->create(json_encode($data));
            }
        }

        $message = $result['body']['message'];

        Session::flash('message', $message);

        return redirect()->back();

    }


}
