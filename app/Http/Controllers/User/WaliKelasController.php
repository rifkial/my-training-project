<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WaliKelasController extends Controller
{
    private $jurusanApi;
    private $walikelasApi;
    private $profileApi;
    private $guruApi;
    private $tahunajaranApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->walikelasApi = new WaliKelasApi();
        $this->guruApi = new GuruApi();
        $this->profileApi = new ProfileApi();
        $this->tahunajaranApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        // dd(session()->all());
        Session::put('title', 'Data Wali Kelas');
        $tahun = $this->tahunajaranApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        // dd($jurusan);
        $jurusan = $jurusan['body']['data'];
        $walikelas = $this->walikelasApi->get_by_sekolah();
        // dd($walikelas);
        if ($walikelas['code'] != 200) {
            $pesan = array(
                'message' => $walikelas['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $walikelas['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" class="key-' . $data['id'] . ' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'walikelas\')"><i class="fa fa-key"></i> Reset Password</button>';
                    return $button;
                });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->editColumn('ttl', function ($item) {
                return $item['tempat_lahir'] . ", " . Help::getTanggal($item['tgl_lahir']);
            });
            $table->rawColumns(['action', 'gambar', 'ttl']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.user.v_wali_kelas')->with(['data' => $result, 'template' => session('template'), 'tahun' => $tahun, 'jurusan' => $jurusan, 'guru' => $guru]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Wali kelas berhasil ditambahkan',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $data_insert = array(
            'id_guru' => $request['id_guru'],
            'id_rombel' => $request['rombel'],
            'id_tahun_ajaran' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->walikelasApi->create_biasa(json_encode($data_insert));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function edit(Request $request)
    {
        $id = $request['id_wali_kelas'];
        $post  = $this->walikelasApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Wali kelas berhasil diupdate',
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        }

        $data_insert = array(
            'id' => $request->id,
            'id_guru' => $request['id_guru'],
            'id_rombel' => $request['rombel'],
            'id_tahun_ajaran' => $request['tahun'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->walikelasApi->update(json_encode($data_insert));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function trash(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'message' => 'Wali kelas berhasil dihapus',
                'success'  => 'success',
                'status'  => 'berhasil',
            ]);
        }

        $id = $request['id_wali_kelas'];
        $delete = $this->walikelasApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'message' => $delete['body']['message'],
                'success'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'message' => $delete['body']['message'],
                'success'  => 'error',
                'status'  => 'gagal',
            ]);
        }


    }

    public function delete($id)
    {
        $delete = $this->walikelasApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function restore($id)
    {
        $restore = $this->walikelasApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
    public function data_trash(Request $request)
    {
        Session::put('title', 'Data Trash Wali Kelas');
        $wali_kelas = $this->walikelasApi->all_trash();
        // dd($wali_kelas);
        $result = $wali_kelas['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm " onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function reset_password(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'icon' => "success",
                'message' => "Password berhasil direset",
                'status' => 'berhasil'
            ]);
        }

        $reset = $this->profileApi->reset_password_walikelas($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }
}
