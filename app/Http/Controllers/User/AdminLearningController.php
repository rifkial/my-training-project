<?php

namespace App\Http\Controllers\User;

use App\ApiService\User\AdminLearningApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use XcS\XcTools;

class AdminLearningController extends Controller
{
    private $adminApi;
    private $profileApi;

    public function __construct()
    {
        $this->adminApi = new AdminLearningApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Admin Elearning');
        $url = $this->adminApi->import();
        $admin = $this->adminApi->get_id_sekolah();
        // dd($admin);
        $admin = $admin['body']['data'];
        // dd($admin);
        // if ($admin['code'] != 200) {
        //     $pesan = array(
        //         'message' => $admin['body']['message'],
        //         'icon' => 'error'
        //     );
        //     return redirect()->back()->with('error_api', $pesan);
        // }
        // $result = $admin['body']['data'];
        // // dd($result);
        // if ($request->ajax()) {
        //     $table = datatables()->of($result)
        //         ->addColumn('action', function ($data) {
        //             $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
        //             $button .= '&nbsp;&nbsp;';
        //             $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
        //             $button .= '&nbsp;&nbsp;';
        //             $button .= '<button type="button" class="key-'.$data['id'].' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'admin_learning\')"><i class="fa fa-key"></i> Reset Password</button>';
        //             return $button;
        //         });
        //     $table->editColumn('gambar', function ($row) {
        //         return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
        //     });
        //     $table->editColumn('ttl', function ($item) {
        //         return $item['tempat_lahir'] . ", " . Help::getTanggal($item['tgl_lahir']);
        //     });
        //     $table->rawColumns(['action', 'gambar', 'ttl']);
        //     $table->addIndexColumn();

        //     return $table->make(true);
        // }
        return view('content.admin.learning.v_admin')->with(['url' => $url, 'admin' => $admin]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'nik' => $request->nik,
            'nip' => $request->nip,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'first_password' => Str::random(5),
            'status' => $request->status
        );
        // dd($data);

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->adminApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        $exp = explode('/', $result['file']);
        $result['file_edit'] = end($exp);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => $request->status
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->adminApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->adminApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

   

    public function reset_password(Request $request)
    {
        $reset = $this->profileApi->reset_password_admin_learning($request['id']);
        $admin = $this->data_admin();
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_admin()
    {
        $admin = $this->adminApi->get_id_sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (!empty($admin)) {
            $no = 1;
            foreach ($admin as $adm) {
                $kelas = 'danger';
                $enable = 'Disabled';
                if ($adm["status"] != 0) {
                    $kelas = 'success';
                    $enable = 'Enabled';
                }
                $jenkel = "Laki - laki";
                if ($adm['jenkel'] != 'l') {
                    $jenkel = "Perempuan";
                }
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$adm['nip'].'</td>
                    <td>'.Str::upper($adm['nama']).'</td>
                    <td>'.$adm['email'].'</td>
                    <td>'.$adm['telepon'].'</td>
                    <td>
                        <button data-toggle="collapse" data-target="#detail'.$adm['id'].'"
                            class="btn btn-info btn-sm">
                            <i class="fas fa-info-circle"></i>
                        </button>
                        <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                            class="edit btn btn-purple btn-sm">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                            class="delete btn btn-danger btn-sm">
                            <i class="fas fa-trash"></i>
                        </a>
                        <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                            class="reset_pass btn btn-warning btn-sm">
                            <i class="fas fa-key"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="hiddenRow">
                        <div class="accordian-body collapse" id="detail'.$adm['id'].'">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td rowspan="5" colspan="3"
                                            class="vertical-middle text-center"><img
                                                src="'.$adm['file'].'" alt="" height="134px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle">Nama</th>
                                        <td class="vertical-middle">'.$adm['nama'].'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle">NIK</th>
                                        <td class="vertical-middle">'.$adm['nik'].'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle">NIP</th>
                                        <td class="vertical-middle">'.$adm['nip'].'</td>

                                    </tr>
                                    <tr>
                                        <th class="vertical-middle">NUPTK</th>
                                        <td class="vertical-middle">'.$adm['nuptk'].'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle" class="vertical-middle">Agama</th>
                                        <td class="vertical-middle" class="vertical-middle">'.Str::ucfirst($adm['agama']).'</td>
                                        <td></td>
                                        <th class="vertical-middle" class="vertical-middle">Jenis Kelamin</th>
                                        <td class="vertical-middle" class="vertical-middle">
                                            '.$jenkel.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle" class="vertical-middle">Telepon</th>
                                        <td class="vertical-middle" class="vertical-middle">'.$adm['telepon'].'</td>
                                        <td></td>
                                        <th class="vertical-middle" class="vertical-middle">Email</th>
                                        <td class="vertical-middle" class="vertical-middle">'.$adm['email'].'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle" class="vertical-middle">Tempat lahir</th>
                                        <td class="vertical-middle" class="vertical-middle">'.$adm['tempat_lahir'].'</td>
                                        <td></td>
                                        <th class="vertical-middle" class="vertical-middle">Tanggal Lahir</th>
                                        <td class="vertical-middle" class="vertical-middle">'.Help::getTanggal($adm['tgl_lahir']).'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle" class="vertical-middle">Password Reset</th>
                                        <td class="vertical-middle" class="vertical-middle">'.$adm['first_password'].'</td>
                                        <td></td>
                                        <th class="vertical-middle" class="vertical-middle">Terakhir Login</th>
                                        <td class="vertical-middle" class="vertical-middle">'.Help::getTanggal($adm['last_login']).'</td>
                                    </tr>
                                    <tr>
                                        <th class="vertical-middle">Status</th>
                                        <td class="vertical-middle">
                                            <p class="mb-0 text-'.$kelas.'">'.$enable.'</p>
                                        </td>
                                        <td></td>
                                        <th class="vertical-middle" class="vertical-middle">Dibuat</th>
                                        <td class="vertical-middle" class="vertical-middle">'.Help::getTanggalLengkap($adm['created_at']).'</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini kosong</td></tr>';
        }
        return $html;
    }
}
