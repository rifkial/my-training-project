<?php

namespace App\Http\Controllers\User;

use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use XcS\XcTools;

class GuruController extends Controller
{
    private $guruApi;
    private $profileApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        $url = $this->guruApi->import();
        Session::put('title', 'Data Guru');
        $guru = $this->guruApi->get_by_sekolah();
        if ($guru['code'] != 200) {
            $pesan = array(
                'message' => $guru['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $guru['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" class="key-' . $data['id'] . ' btn btn-warning btn-sm" onclick="resetPass(' . $data['id'] . ',\'guru\')"><i class="fa fa-key"></i> Reset Password</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button class="dt-' . $data['id'] . ' btn btn-sm btn-default" onclick="detailGuru('.$data['id'].')"><i class="fa fa-info-circle"></i> Detail</button>';
                    return $button;
                });
            $table->editColumn('ttl', function ($item) {
                return ucfirst($item['tempat_lahir']) . ", " . Help::getTanggal($item['tgl_lahir']);
            });
            $table->editColumn('gambar', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });
            $table->rawColumns(['action', 'gambar', 'ttl']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        if (session('role') == 'supervisor') {
            return view('content.admin.user.v_s_guru')->with(['url' => $url, 'template' => session('template')]);
        }
        return view('content.admin.user.v_guru')->with(['url' => $url, 'template' => session('template')]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json(
                [
                    'success' => 'Guru berhasil ditambahkan',
                    'icon'  => 'success',
                    'status' => 'berhasil'
                ]
            );
        }
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'informasi_lain' => $request->informasi_lain,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => Str::random(6),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->guruApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->guruApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_guru'];
        $post  = $this->guruApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        $result['jenkel'] = $result['jenkel'] == "Perempuan" ? "p" : "l";
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'tahun_masuk' => $request->tahun_masuk,
            'informasi_lain' => $request->informasi_lain,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'password' => 12345,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->guruApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->guruApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->guruApi->update(json_encode($data));
            }
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash($id)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Guru berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }
        $delete = $this->guruApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->guruApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash(Request $request)
    {
        $guru = $this->guruApi->all_trash();
        // dd($guru);
        $result = $guru['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->guruApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->guruApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        // dd($message);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'icon' => "success",
                'message' => "Password berhasil direset",
                'status' => 'berhasil'
            ]);
        }
        $reset = $this->profileApi->reset_password_guru($request['id']);
        if ($reset['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }
}
