<?php

namespace App\Http\Controllers\User;

use App\ApiService\Master\JurusanApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\KaprodiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class KaprodiController extends Controller
{
    private $hash;
    private $jurusanApi;
    private $kaprodiApi;
    private $guruApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
        $this->jurusanApi = new JurusanApi();
        $this->kaprodiApi = new KaprodiApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        session()->put('title', 'User Kaprodi');
        $kaprodi = $this->kaprodiApi->get_all();
        $kaprodi = $kaprodi['body']['data'];
        // dd($kaprodi);
        $guru = $this->guruApi->get_by_sekolah();
        // dd($guru);
        $guru = $guru['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        // dd($guru);
        $template = 'default';
        return view('content.admin.user.v_kaprodi', compact('kaprodi', 'template', 'guru', 'jurusan'));
    }

    public function store(Request $request)
    {
        $data = array(
            'id_guru' => $request['id_guru'],
            'id_jurusan' => $request['id_jurusan']
        );
        $result = $this->kaprodiApi->create(json_encode($data));
        $kaprodi = $this->data_kaprodi();
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'html' => $kaprodi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal',
                'html' => $kaprodi
            ]);
        }
    }

    public function detail()
    {
        // dd("tes");
        $kaprodi = $this->kaprodiApi->get_by_id(Help::decode($_GET['code']));
        // dd($kaprodi);
        $kaprodi = $kaprodi['body']['data'];
        // dd($kaprodi);
        $template = session('template');
        return view('content.admin.user.v_detail_kaprodi', compact('template', 'kaprodi'));

    }

    public function edit(Request $request)
    {
        $post  = $this->kaprodiApi->get_by_id($request['id']);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_guru' => $request['id_guru'],
            'id_jurusan' => $request['id_jurusan']
        );
        $result = $this->kaprodiApi->update(json_encode($data));
        // dd($result);
        $kaprodi = $this->data_kaprodi();
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'html' => $kaprodi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal',
                'html' => $kaprodi
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->kaprodiApi->soft_delete($request['id']);
        // dd($delete);
        $kaprodi = $this->data_kaprodi();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $kaprodi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $kaprodi
            ]);
        }
    }

    private function data_kaprodi()
    {
        $kaprodi = $this->kaprodiApi->get_all();
        $kaprodi = $kaprodi['body']['data'];
        $html = '';
        if ($kaprodi == null) {
            $html .= '<tr><td colspan="4">Data Kaprodi saat ini kosong</td></tr>';
        } else {
            $nomer = 1;
            foreach ($kaprodi as $kd) {
                $html .= '<tr>
                <td>'.$nomer++.'</td>
                <td>'.$kd['jurusan'].'</td>
                <td>'.$kd['nama'].'</td>
                <td>'.$kd['nip'].'</td>
                <td>'. $kd['nik'].'</td>
                <td>'.$kd['nuptk'].'</td>
                <td>
                    <a href="#"><i class="material-icons list-icon md-18 text-success">info</i></a>
                    <a href="javascript:void(0)" data-id="'.$kd['id'].'" class="edit"><i
                            class="material-icons list-icon md-18 text-info">edit</i></a>
                    <a href="javascript:void(0)" onclick="deleteKaprodi('. $kd['id'] .')"><i
                            class="material-icons list-icon md-18 text-danger">delete</i></a>
                </td>
                </tr>';
            }
        }
        return $html;
    }
}
