<?php

namespace App\Http\Controllers\User;

use App\ApiService\Alumni\JurusanApi;
use App\ApiService\BKK\UserApi;
use App\ApiService\User\AdminApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\SiswaApi;
use App\ApiService\User\SupervisorApi;
//user mutu
use App\ApiService\PenjaminMutu\ProfileUser\UserMutuApi;
//sarana
use App\ApiService\Sarana\User\AdminSaranaApi;
use App\ApiService\Sarana\User\UserSaranaApi;

use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class ProfileController extends Controller
{
    private $guruApi;
    private $waliApi;
    private $supervisorApi;
    private $siswaApi;
    private $adminApi;
    private $bkkApi;
    private $usermutuApi;
    private $jurusanAlumniApi;
    private $usersaranaApi;
    private $adminsaranaApi;
    private $profileApi;
    private $pelamarApi;

    public function __construct(){
        $this->profileApi = new ProfileApi();
        $this->guruApi = new GuruApi();
        $this->supervisorApi = new SupervisorApi();
        $this->waliApi = new WaliKelasApi();
        $this->siswaApi = new SiswaApi();
        $this->adminApi = new AdminApi();
        $this->jurusanAlumniApi = new JurusanApi();
        $this->pelamarApi = new UserApi();
        $this->bkkApi = new UserApi();
        $this->usermutuApi = new UserMutuApi();
        $this->jurusanAlumniApi = new JurusanApi();
        $this->usersaranaApi = new UserSaranaApi();
        $this->adminsaranaApi = new AdminSaranaApi();
    }

    public function edit(Request $request)
    {
        // dd(session('role'));
        $role = session('role');
        $id = session('id');
        // dd($role);
        // dd(session()->all());
        Session::put('title', 'Data Sekolah');
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $result = $profile['body']['data'];
        // dd($result);
        if (session('role') == 'admin' || session('role') == 'induk-admin' || session('role') == 'admin-absensi') {
            $template = 'default';
        } elseif ($role == 'bkk-admin') {
            $template = 'default';
            $request->session()->put('config', 'bursa_kerja');
        } elseif($role == 'bkk-pelamar') {
            $pelamar = $this->pelamarApi->get_by_id(session('id'));
            // dd($pelamar);
            $result = $pelamar['body']['data'];
            return view('content.bkk.dashboard.bkk-pelamar.profile.v_preview')->with(['data' => $result]);
        } elseif ($role == 'ortu') {
            $template = session('template');
        } elseif ($role == 'user-mutu'){
            $data = $this->usermutuApi->get_by_id($id);
            $data = $data['body']['data'];
            //dd($data);
            return view('content.profile.profile_penjamin-mutu.profile_penjamin-mutu')->with(['data' => $data]);
        } else if($role == 'user-sarpras' || $role == 'admin-sarpras'){
            if(session('role-sarana') == 'admin'){
                $data = $this->profileApi->get_profile();
            }else{
                $data = $this->profileApi->get_profile();
            }
            $data = $data['body']['data'];
            //dd($data);
            return view('content.profile.profile_sarana.profile_sarana',compact('data'));
        }else {
            $template = session('template');
            $result['tanggal'] = date('d/m/Y', strtotime($result['tgl_lahir']));
        }
        if (session('role') == 'alumni-alumni') {
            $jurusan = $this->jurusanAlumniApi->id_sekolah(session('id_sekolah'));
            $jurusan = $jurusan['body']['data'];
        } else {
            $jurusan = [];
        }
        // dd()
        if (session('config') == 'point' && session('role') == 'supervisor') {
            $template = 'default';
        }
        // dd($template);
        return view('content.profile.profile_' . $role.'.profile_' . $role)->with(['template' => $template, 'data' => $result, 'jurusan' => $jurusan]);

    }

    public function edit_pelamar()
    {
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $result = $profile['body']['data'];
        $template = session('template');
        return view('content.profile.profile_bkk-pelamar.profile_bkk-pelamar')->with(['template' => $template, 'data' => $result]);
    }

    public function update(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $profile = $this->profileApi->get_profile();
            $data_profile = $profile['body']['data'];
            unset($data_profile['file']);
            unset($data_profile['password']);
            unset($data_profile['first_password']);
            $data_profile['nama'] = $request['nama'];
            $data_profile['email'] = $request['email'];
            $data_profile['telepon'] = $request['telepon'];
            $data_profile['tempat_lahir'] = $request['tempat_lahir'];
            $data_profile['tgl_lahir'] = $request['tahun']."-".$request['bulan']."-".$request['tanggal'];
            $data_profile['alamat'] = $request['alamat'];
            $update = $this->profileApi->update_profile(json_encode($data_profile));
            if ($update['code'] == 200) {
                return response()->json([
                    'success' => $update['body']['message'],
                    'icon'  => 'success',
                    'status'  => 'berhasil'
                ]);
            } else {
                return response()->json([
                    'success' => $update['body']['message'],
                    'icon'  => 'error',
                    'status'  => 'gagal'
                ]);
            }
        }
        elseif(session('role') == 'user-mutu'){
            $data = array(
                'nama' => $request->nama,
                'no_induk' => $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
            //dd($data);
        } elseif(session('role') == 'user-sarpras' || session('role') == 'admin-sarpras'){
            $data = array(
                'nama' => $request->nama,
                'username' => $request->username,
                'no_induk' => $request->no_induk,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'alamat' => $request->alamat,
                'role' => $request->role,
                'agama' => $request->agama,
                'sosial' => '',
                'id_sekolah' => session('id_sekolah')
            );
            //dd($data);
        }else {
            $data = array(
                'id' => $request->id,
                'nik' => $request->nik,
                'nis' => $request->nis,
                'nisn' => $request->nisn,
                'tahun_angkatan' => $request->tahun_angkatan,
                'nuptk' => $request->nuptk,
                'pendidikan_akhir' => $request->pendidikan_akhir,
                'tahun_masuk' => $request->tahun_masuk,
                'informasi_lain' => $request->informasi_lain,
                'nama' => $request->nama,
                'pekerjaan' => $request->pekerjaan,
                'nip' => $request->nip,
                'jenkel' => $request->jenkel,
                'agama' => $request->agama,
                'telepon' => $request->telepon,
                'email' => $request->email,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
                'alamat' => $request->alamat,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
        }


        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;

            if (session('role') == 'bkk-perusahaan') {
                $result = $this->bkkApi->update_info(json_encode($data));
            }
            elseif (session('role') == 'user-mutu') {
                //dd("ada foto");
                $result = $this->usermutuApi->update_profile_image(json_encode($data));
                return redirect()->back();
            }
            elseif (session('role') == 'user-sarpras' || session('role') == 'admin-sarpras') {
                //dd("ada foto");
                $result = $this->profileApi->update_profile_image(json_encode($data));
                return redirect()->back();
            }
            else {
                $result = $this->profileApi->update_profile_image(json_encode($data));
            }
            File::delete($path);
            if ($result['code'] == 200) {
                $pesan = array(
                    'status' => 'berhasil',
                    'message' => $result['body']['message'],
                    'icon' => 'success'
                );
            } else {
                $pesan = array(
                    'status' => 'gagal',
                    'message' => $result['body']['message'],
                    'icon' => 'error'
                );
            }
            if(session('role') == 'alumni-alumni'){
                return redirect()->back()->with(['message' => $pesan]);
            }
            return response()->json($pesan);
        } else {
            if (session("role") == "bkk-perusahaan") {
                $result = $this->bkkApi->update(json_encode($data));
            }
            elseif (session('role') == 'user-mutu') {

                $result = $this->usermutuApi->update_profile(json_encode($data));
                //dd($result);
                return redirect()->back();
            }else if(session('role') == 'user-sarpras' || session('role') == 'admin-sarpras'){
                $result = $this->profileApi->update_profile(json_encode($data));
                return redirect()->back();
            }
            else {
                $result = $this->profileApi->update_profile(json_encode($data));
            }

            // dd($result);
            if ($result['code'] == 200) {
                $pesan = array(
                    'status' => 'berhasil',
                    'message' => $result['body']['message'],
                    'icon' => 'success'
                );
            } else {
                $pesan = array(
                    'status' => 'gagal',
                    'message' => $result['body']['message'],
                    'icon' => 'error'
                );
            }
            if(session('role') == 'alumni-alumni'){
                return redirect()->back()->with(['message' => $pesan]);
            }
            return response()->json($pesan);
        }
    }

    public function change_password(Request $request)
    {
        $data_update = array(
            'current_password' => $request['current_password'],
            'new_password' => $request['new_password'],
            'confirm_password' => $request['confirm_password'],
        );
        $update = $this->profileApi->change_password(json_encode($data_update));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function v_change_password()
    {
        // dd(session('role'));
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        if(session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar'){
            return view('content.profile.profile_bkk-perusahaan.v_change_password')->with(['data' => $profile]);
        }else{
            return view('content.profile.v_change_password')->with(['data' => $profile]);
        }

    }

    public function reset_password()
    {
        if(session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar'){
            return view('content.profile.profile_bkk-perusahaan.v_reset_password')->with([]);
        }else{
            return view('content.profile.v_reset_password')->with([]);
        }
    }

    public function upload_paraf()
    {
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $profile = $profile['body']['data'];
        return view('content.profile.v_upload_paraf')->with(['profile' => $profile]);
    }

    public function update_paraf(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id' => session('id'),
        );

        $files = $request->file('image');
        if ($files) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            if (session('role') == 'walikelas') {
                $result = $this->waliApi->update_paraf(json_encode($data));
            } else {
                $result = $this->supervisorApi->update_paraf(json_encode($data));
            }
            File::delete($path);
        } else {
            $result = $this->guruApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            );
        } else {
            $message = array(
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            );
        }

        return redirect()->back()->with(['message' => $message]);
    }

    public function reset_passwords()
    {
        // dd(session('role'));
        if (session('role') == "admin") {
            $reset = $this->profileApi->reset_password_admin(session('id'));
        } elseif (session('role') == 'guru') {
            $reset = $this->profileApi->reset_password_guru(session('id'));
        } elseif (session('role') == 'siswa') {
            $reset = $this->profileApi->reset_password_siswa(session('id'));
        } elseif (session('role') == 'walikelas') {
            $reset = $this->profileApi->reset_password_walikelas(session('id'));
        } elseif (session('role') == 'superadmin') {
            $reset = $this->profileApi->reset_password_superadmin(session('id'));
        }elseif(session('role') == 'bkk-perusahaan' || session('role') == 'bkk-pelamar'){
            $reset = $this->profileApi->reset_password_user_bkk(session('id'));
        }

        // dd($reset);
        if ($reset['code'] == 200) {
            return response()->json([
                'success' => $reset['body']['message'],
                'data' => $reset['body']['data'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $reset['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function preview()
    {
        if ((session('role') == 'bkk-pelamar')) {
            $profile = $this->bkkApi->get_by_id(session('id'));
            // dd($profile);
            $result = $profile['body']['data'];
            return view('content.bkk.dashboard.' . session('role') . '.profile.v_preview')->with(['data' => $result]);
        }
    }

    public function update_image(Request $request)
    {
        $data = array(
            'id' => $request['id']
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->bkkApi->update_image(json_encode($data));
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->bkkApi->update_info(json_encode($data));
            } else {
                return response()->json([
                    'success' => "Anda belum memilih gambar",
                    'icon'  => 'error',
                    'status'  => 'belum'
                ]);
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
