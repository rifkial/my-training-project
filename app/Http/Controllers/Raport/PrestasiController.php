<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiPrestasiApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PrestasiController extends Controller
{
    private $prestasiApi;
    private $kelasSiswaApi;

    public function __construct()
    {
        $this->prestasiApi = new NilaiPrestasiApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Prestasi');
        $siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
        $siswa = $siswa['body']['data'];
        $prestasi = $this->prestasiApi->get_by_wali_kelas(session('id_tahun_ajar'));
        // dd($prestasi);
        $result = $prestasi['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.raport.guru.prestasi.v_prestasi')->with(['template' => session('template'), 'siswa' => $siswa]);
    }

    public function store(Request $request)
    {
        $data_insert = array(
            "id_wali_kelas" => session('id'),
            "id_kelas_siswa" => $request['id_siswa'],
            "id_rombel" => session('id_rombel'),
            "id_ta_sm" => session('id_tahun_ajar'),
            "nama" => $request['jenis'],
            "keterangan" => $request['keterangan'],
            "id_sekolah" => session('id_sekolah'),
        );
        // dd($data_insert);
        $result = $this->prestasiApi->create(json_encode($data_insert));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'warning'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_prestasi'];
        $prestasi = $this->prestasiApi->get_by_id($id);
        // dd($prestasi);
        $prestasi = $prestasi['body']['data'];
        return response()->json($prestasi);
    }

    public function update(Request $request)
    {
        $data_update = array(
                "id" => $request->id,
                "id_wali_kelas" => session('id'),
                "id_kelas_siswa" => $request['id_siswa'],
                "id_rombel" => session('id_rombel'),
                "id_ta_sm" => session('id_tahun_ajar'),
                "nama" => $request['jenis'],
                "keterangan" => $request['keterangan'],
                "id_sekolah" => session('id_sekolah'),
        );
        $result = $this->prestasiApi->update_info(json_encode($data_update));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'warning'
            ]);
        }
    }

    public function trash($id)
    {
        // dd($id);
        $delete = $this->prestasiApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = $delete['body']['message'];
        } else {
            $success = false;
            $message =  $delete['body']['message'];
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
