<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\EkstrakurikulerApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiEkskulApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NilaiEkstrakurikulerController extends Controller
{
    private $ekstraApi;
    private $NilaiEkskulApi;
    private $kelassiswaApi;
    private $profilApi;

    public function __construct()
    {
        $this->ekstraApi = new EkstrakurikulerApi();
        $this->NilaiEkskulApi = new NilaiEkskulApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->profilApi = new ProfileApi();
    }

    public function index()
    {
        $profile = $this->profilApi->get_profile();
        // dd($profile);
        $id_rombel = $profile['body']['data']['id_rombel'];
        $ekstra = $this->ekstraApi->get_by_sekolah();
        $ekstra = $ekstra['body']['data'];
        return view('content.raport.guru.nilai_ekstrakurikuler.v_nilai_ekstrakurikuler')->with(['template' => session('template'), "ekstra" => $ekstra, 'id_rombel' => $id_rombel]);
    }

    // public function store(Request $request)
    // {
    //     // dd($request);
    //     $data = [];
    //     $nilai = $request['nilai'];
    //     $deskripsi = $request['nilai_d'];
    //     $i = 0;
    //     $a = 0;
    //     foreach ($request['id_siswa'] as $siswa) {
    //         $data[] = array(
    //                 "siswa" => $siswa,
    //                 "nilai" => $nilai[$i],
    //                 "deskripsi" => $deskripsi[$a],
    //             );
    //         $i++;
    //         $a++;
    //     }
    //     foreach ($data as $dt) {
    //         $data_insert = array(
    //             'id_kelas_siswa' => $dt['siswa'],
    //             'nilai' => $dt['nilai'],
    //             'deskripsi' => $dt['deskripsi'],
    //             'id_wali_kelas' => session('id'),
    //             'id_th_sm' => session('id_tahun_ajar'),
    //             'id_ekskul' => $request['id_ekstra'],
    //             'id_sekolah' => session('id_sekolah')
    //         );
    //         if ($data_insert['deskripsi'] != null && $data_insert['nilai'] != '-') {
    //             $result = $this->NilaiEkskulApi->create(json_encode($data_insert));
    //         }
    //     }
    //     if ($result['code'] == 200) {
    //         return response()->json(['success' => $result['body']['message'],'icon'  => 'success']);
    //     } else {
    //         return response()->json(['success' => $result['body']['message'],'icon'  => 'warning']);
    //     }
    // }
    
    public function store_revisi(Request $request)
    {
        // dd($request);
        $data = [];
        $nilai = $request['nilai'];
        $deskripsi = $request['nilai_d'];
        $i = 0;
        $a = 0;
        foreach ($request['id_siswa'] as $siswa) {
            $data[] = array(
                    "siswa" => $siswa,
                    "nilai" => $nilai[$i],
                    "deskripsi" => $deskripsi[$a],
                );
            $i++;
            $a++;
        }
        foreach ($data as $dt) {
            $data_insert = array(
                'id_kelas_siswa' => $dt['siswa'],
                'nilai' => $dt['nilai'],
                'deskripsi' => $dt['deskripsi'],
                'id_wali_kelas' => session('id'),
                'id_ta_sm' => session('id_tahun_ajar'),
                'id_ekskul' => $request['id_ekstra'],
                'id_sekolah' => session('id_sekolah')
            );
            if ($data_insert['deskripsi'] != null && $data_insert['nilai'] != '-') {
                $result = $this->NilaiEkskulApi->create(json_encode($data_insert));
                // dd($result);
            }
        }
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'success']);
        } else {
            return response()->json(['success' => $result['body']['message'],'icon'  => 'error']);
        }
    }

    // public function get_siswa(Request $request)
    // {
    //     $list_data = array();
    //     $sudah_ada = $this->NilaiEkskulApi->get_by_ekskul(session('id_tahun_ajar'), $request['id_ekstra']);
    //     $sudah_ada = $sudah_ada['body']['data'];

    //     $belum_adas = $this->kelassiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
    //     $belum_adas = $belum_adas['body']['data'];
    //     $belum_ada = [];
    //     foreach ($belum_adas as $ba) {
    //         $belum_ada[] = array(
    //             'id' => $ba['id'],
    //             'nama' => $ba['nama'],
    //             'deskripsi' => '',
    //         );
    //     }

    //     if (empty($sudah_ada)) {
    //         $list_data = $belum_ada;
    //         $d['ambil_mana'] = "data nilai kosong";
    //     } else {
    //         $list_data = $sudah_ada;
    //         $d['ambil_mana'] = "data nilai real";
    //     }

    //     $d['status'] = "ok";
    //     $d['data'] = $list_data;
    //     return response()->json($d);
    // }
    
    public function get_siswa_revisi(Request $request)
    {
        $sudah_ada = $this->NilaiEkskulApi->get_by_gabungan(session('tahun'), session('id_tahun_ajar'), $request['id_ekstra']);
        // dd($sudah_ada);
        $sudah_ada = $sudah_ada['body']['data'];
        $data = [];
        foreach ($sudah_ada as $sa) {
            if ($sa['deskripsi'] != null) {
                $deskripsi = $sa['deskripsi'];
            } else {
                $deskripsi = '-';
            }
            $data[] = array(
                'id_kelas_siswa' => $sa['id_kelas_siswa'],
                'nis' => $sa['nis'],
                'nisn' => $sa['nisn'],
                'nama' => $sa['nama'],
                'id_ekskul' => $sa['id_ekskul'],
                'ekskul' => $sa['ekskul'],
                'nilai' => $sa['nilai'],
                'deskripsi' => $deskripsi,
            );
        }
        $d['data'] = $data;
        return response()->json($d);
    }
}
