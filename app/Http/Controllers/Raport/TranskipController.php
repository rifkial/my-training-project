<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\TranskipApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class TranskipController extends Controller
{
    private $jurusanApi;
    private $kelasSiswaApi;
    private $tahunApi;
    private $transkipApi;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->transkipApi = new TranskipApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Transkip Nilai');
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $routes = "raport-transkip";
        $search = (isset($_GET["nis"])) ? $_GET["nis"] : "";
        if (isset($_GET["jurusan"]) && $_GET["jurusan"] != '') {
            $jurusans = Help::decode($_GET["jurusan"]);
            $data_jr = $_GET['jurusan'];
        }else{
            $jurusans = "";
            $data_jr = "";
        }
        $tahuns = (isset($_GET["tahun"])) ? $_GET["tahun"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->kelasSiswaApi->data_siswa($page, $tahuns, $jurusans, $search);
        // dd($result);
        $siswa = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::dataSiswa($data_jr, $tahuns, $meta, $routes, $search);
        $template = 'default';
        if (session('role') != 'induk-admin' && session('role') != 'supervisor') {
            $template = session('template');
        }
        return view('content.raport.guru.transkip.v_transkip')->with(['template' => $template, 'tahun'=> $tahun, 'jurusan' => $jurusan, 'routes' => $routes,
        // return view('content.admin.induk.v_siswa')->with(['tahun'=> $tahun, 'jurusan' => $jurusan, 'routes' => $routes,
        'pagination' => $pagination, 'siswa' => $siswa, 'search' => $search, 'select_jurusan' => $data_jr, 'tahuns' => $tahuns
        ]);
    }

    public function get_siswa(Request $request)
    {
        session()->put('request_point_temporary', [
            'id_jurusan' => $request['id_jurusan'],
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                <td>' . $nomer++ . '</td>
                <td>' . ucfirst($sw['nama']) . '</td>
                <td>' . $sw['nis'] . '</td>
                <td>' . $sw['nisn'] . '</td>
                <td>' . $sw['rombel'] . '</td>
                <td>' . $sw['jurusan'] . '</td>
                <td>
                    <button data-toggle="collapse" data-target="#demo' . $sw['id'] . '"
                        class="btn btn-sm btn-success accordion-toggle"><i
                            class="fas fa-info-circle"></i></button>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo' . $sw['id'] . '">
                        <button class="btn btn-info btn-sm my-3 pull-right"
                            onclick="tambahTranskip(' . $sw['id'] . ')"><i class="fas fa-plus-circle"></i>
                            Tambah Transkip</button>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Keterangan</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataTranskrip' . $sw['id'] . '">';
                if (empty($sw['transkrips'])) {
                    $html .= ' <tr>
                                <td colspan="5" class="text-center">Data saat ini kosong</td>
                            </tr>';
                } else {
                    $no = 1;
                    foreach ($sw['transkrips'] as $tr) {
                        if ($tr['file'] == null) {
                            $download = '-';
                        } else {
                            $download = '<a href="' . route('raport_transkip-download', $tr['id']) . '"  target="_blank" class="btn btn-warning btn-xs">download file <i class="fa fa-download"></i></a>';
                        }
                        $html .= '<tr>
                                    <td class="text-center">' . $no++ . '</td>
                                    <td class="text-center">' . $tr['nama'] . '</td>
                                    <td class="text-center">' . $download . '</td>
                                    <td class="text-center">' . $tr['keterangan'] . '</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" data-id="' . $tr['id'] . '"
                                            class="btn btn-info btn-sm editTranskrip"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <button class="btn btn-danger btn-sm"
                                            onclick="deleteTranskrip(' . $tr['id'] . ', ' . $sw['id'] . ')"><i
                                                class="fas fa-trash"></i></button>
                                    </td>
                                </tr>';
                    }
                }
                $html .= ' </tbody>
                            </table>
                        </div>
                    </td>
                </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function store(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'id_kelas_siswa' => $request['id_kelas_siswa'],
            'id_ta_sm' => $request['id_ta_sm'],
            'status' => 1,
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/transkrip/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->transkipApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->transkipApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $transkrip = $this->data_transkip($request['id_kelas_siswa']);
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'html' => $transkrip,
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'id_kelas_siswa' => $request['id_kelas_siswa'],
            'id_ta_sm' => $request['id_ta_sm'],
            'status' => 1,
            'id_sekolah' => session('id_sekolah')
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/tugas/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->transkipApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->transkipApi->update_info(json_encode($data));
        }
        $transkrip = $this->data_transkip($request['id_kelas_siswa']);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'html' => $transkrip,
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'html' => $transkrip,
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $transkrip = $this->transkipApi->get_by_id($request['id']);
        // dd($transkrip);
        $transkrip = $transkrip['body']['data'];
        return response()->json($transkrip);
    }

    public function delete(Request $request)
    {
        // dd($request);
        $delete = $this->transkipApi->soft_delete($request['id']);
        $html = $this->data_transkip($request['id_kelas_siswa']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    private function data_transkip($id)
    {
        // dd($transkrip);
        $transkrip = $this->transkipApi->get_by_kelas_siswa($id);
        $transkrip = $transkrip['body']['data'];
        $html = '';
        if (!empty($transkrip)) {
            $no = 1;
            foreach ($transkrip as $tr) {
                $html .= '<tr>
                    <td class="text-center">' . $no++ . '</td>
                    <td class="text-center">'.$tr['nama'].'</td>
                    <td class="text-center">'.$tr['keterangan'].'</td>
                    <td class="text-center">
                        <a href="javascript:void(0)"
                            data-id="'.$tr['id'].'"
                            data-siswa="'.$id.'"
                            class="btn btn-info btn-sm edit"><i
                                class="fas fa-pencil-alt"></i></a>
                        <button class="btn btn-danger btn-sm delete"
                            data-id="'.$tr['id'].'"
                            data-siswa="'.$id.'">
                            <i class="fas fa-trash"></i></button>
                        <a href="'.route('raport_transkip-download', Help::encode($tr['id'])).'" target="_blank"
                            class="btn btn-purple btn-sm"><i
                                class="fas fa-file-download"></i></a>
                    </td>
                </tr>';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">data transkip untuk saat ini tidak tersedia</td></tr>';
        }

        return $html;
    }

    public function download($id)
    {
        $url = request()->segments();
        $post  = $this->transkipApi->get_by_id(Help::decode(end($url)));
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['file']);
        $fileName = end($nama_file);
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($result['file']);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
