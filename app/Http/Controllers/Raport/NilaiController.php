<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiApi;
use App\ApiService\Raport\NilaiConfigApi;
use App\ApiService\Raport\PredikatApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    private $gurupelajaranApi;
    private $kelasSiswaApi;
    private $nilaiApi;
    private $configApi;
    private $predikatApi;
    private $hashids;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->configApi = new NilaiConfigApi();
        $this->nilaiApi = new NilaiApi();
        $this->predikatApi = new PredikatApi();
        $this->hashids = new Hashids();
    }
    
    public function index($id_code)
    {
        $id = $this->hashids->decode($id_code)[0];
        $config = $this->configApi->get_by_sekolah_tahun_ajar(session('id_tahun_ajar'));
        // dd($config);
        $config = $config['body']['data'];
        $guru = $this->gurupelajaranApi->get_by_id($id);
        $guru = $guru['body']['data'];
        // $id_rombel = $guru['id_rombel'];
        $siswa = $this->nilaiApi->get_gabungan(session('tahun'), $id, session('id_tahun_ajar'));
        $siswa = $siswa['body']['data'];
        return view('content.raport.guru.mapel.v_nilai_pengetahuan_revisi')->with(['template' => session('template'), 'guru' => $guru, 'siswa' => $siswa, 'config' => $config]);
    }

    public function simpan_manual(Request $request)
    {
        // dd($reque)
        $p = $request;
        for ($i = 1; $i <= $p['jumlah']; $i++) {
            $id_siswa = $p['id_siswa_'.$i];
            $harian = $p['harian_'.$i];
            $uts = $p['uts_'.$i];
            $uas = $p['uas_'.$i];
            $akhir = $p['akhir_'.$i];
            $deskripsi = $p['deskripsi_'.$i];
            $predikat = $p['predikat_'.$i];
            $data_mentah[] = array(
                "siswa" => $id_siswa,
                "nilai_uh_rata" => $harian,
                "nilai_uts" => $uts,
                "nilai_uas" => $uas,
                "nilai_akhir" => $akhir,
                "deskripsi" => $deskripsi,
                "predikat" => $predikat,
                "id_guru_pelajaran" => $p['id_guru_pelajaran'],
            );
        }
        foreach ($data_mentah as $mnth) {
            $data_insert = array(
                    'id_ta_sm' => session('id_tahun_ajar'),
                    'id_kelas_siswa' => $mnth['siswa'],
                    'id_guru_pelajaran' => $mnth['id_guru_pelajaran'],
                    'nilai_uh_rata' => $mnth['nilai_uh_rata'],
                    'nilai_uts' => $mnth['nilai_uts'],
                    'nilai_uas' => $mnth['nilai_uas'],
                    'nilai_akhir' => $mnth['nilai_akhir'],
                    'deskripsi' => $mnth['deskripsi'],
                    'predikat' => $mnth['predikat'],
                    'id_sekolah' => session('id_sekolah'),
                );
            if ($data_insert['nilai_akhir'] != null) {
                $result = $this->nilaiApi->create(json_encode($data_insert));
            }
        }
        if ($result['code'] == 200) {
            $d['status'] = "berhasil";
            $d['icon'] = "success";
            $d['message'] = $result['body']['message'];
        } else {
            $d['status'] = "gagal";
            $d['icon'] = "danger";
            $d['message'] = $result['body']['message'];
        }
        return response()->json($d);
        // dd($result_update);
    }

    public function get_range(Request $request)
    {
        // dd($request);
        $nilai = $request['hasil'];
        $predikat = $this->predikatApi->get_range($nilai);
        $predikat = $predikat['body']['data'];
        return response()->json($predikat);
    }
}
