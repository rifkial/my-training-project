<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Raport\NilaiConfigApi;
use App\ApiService\Raport\NilaiK16Api;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Nilaik16Controller extends Controller
{

    private $gurupelajaranApi;
    private $nilaiApi;
    private $configApi;
    private $predikatApi;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->configApi = new NilaiConfigApi();
        $this->nilaiApi = new NilaiK16Api();
    }

    public function index($id)
    {
        $config = $this->configApi->get_by_sekolah_tahun_ajar(session('id_tahun_ajar'));
        // dd($config);
        $config = $config['body']['data'];
        $guru = $this->gurupelajaranApi->get_by_id(Help::decode($id));
        $guru = $guru['body']['data'];
        // $id_rombel = $guru['id_rombel'];
        $siswa = $this->nilaiApi->get_gabungan(session('tahun'), Help::decode($id), session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        return view('content.raport.guru.mapel.v_nilai_k16')->with(['template' => session('template'), 'guru' => $guru, 'siswa' => $siswa, 'config' => $config]);
    }

    public function simpan(Request $request)
    {
        // dd($request);
        $p = $request;
        for ($i = 1; $i <= $p['jumlah']; $i++) {
            $id_siswa = $p['id_siswa_'.$i];
            $harian = $p['harian_'.$i];
            $uts = $p['uts_'.$i];
            $uas = $p['uas_'.$i];
            $akhir = $p['akhir_'.$i];
            $deskripsi = $p['deskripsi_'.$i];
            $data_mentah[] = array(
                "siswa" => $id_siswa,
                "nilai_uh_rata" => $harian,
                "nilai_uts" => $uts,
                "nilai_uas" => $uas,
                "nilai_akhir" => $akhir,
                "capaian_hasil" => $deskripsi,
                "id_guru_pelajaran" => $p['id_guru_pelajaran'],
            );
        }
        foreach ($data_mentah as $mnth) {
            $data_insert = array(
                    'id_ta_sm' => session('id_tahun_ajar'),
                    'id_kelas_siswa' => $mnth['siswa'],
                    'id_guru_pelajaran' => $mnth['id_guru_pelajaran'],
                    'nilai_uh_rata' => $mnth['nilai_uh_rata'],
                    'nilai_uts' => $mnth['nilai_uts'],
                    'nilai_uas' => $mnth['nilai_uas'],
                    'nilai_akhir' => $mnth['nilai_akhir'],
                    'capaian_hasil' => $mnth['capaian_hasil'],
                    'id_sekolah' => session('id_sekolah'),
                );
            if ($data_insert['nilai_akhir'] != null) {
                $result = $this->nilaiApi->create(json_encode($data_insert));
            }
        }
        if ($result['code'] == 200) {
            $d['status'] = "berhasil";
            $d['icon'] = "success";
            $d['message'] = $result['body']['message'];
        } else {
            $d['status'] = "gagal";
            $d['icon'] = "danger";
            $d['message'] = $result['body']['message'];
        }
        return response()->json($d);
        // dd($result_update);
    }

    public function get_range(Request $request)
    {
        // dd($request);
        $nilai = $request['hasil'];
        $predikat = $this->predikatApi->get_range($nilai);
        $predikat = $predikat['body']['data'];
        return response()->json($predikat);
    }
}
