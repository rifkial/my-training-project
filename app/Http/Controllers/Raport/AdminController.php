<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Raport\AdminApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AdminController extends Controller
{
    private $adminApi;
    private $profileApi;

    public function __construct()
    {
        $this->adminApi = new AdminApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Admin Raport');
        $admin = $this->adminApi->sekolah();
        $admin = $admin['body']['data'];
        return view('content.admin.raport.v_admin')->with(['admin' => $admin]);
    }

    public function edit(Request $request)
    {
        // dd($request);
        $admin = $this->adminApi->get_by_id($request['id']);
        // dd($admin);
        $admin = $admin['body']['data'];
        $ex_file = explode('/', $admin['file']);
        $admin['file_edit'] = end($ex_file);
        return response()->json($admin);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
        $data = array(
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->adminApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nik' => $request->nik,
            'nuptk' => $request->nuptk,
            'nama' => $request->nama,
            'nip' => $request->nip,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->adminApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->adminApi->update_info(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->adminApi->update_info(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }


    public function delete(Request $request)
    {
        $delete = $this->adminApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    

    public function reset_password(Request $request)
    {
        $reset = $this->profileApi->reset_password_admin_raport($request['id']);
        if ($reset['code'] == 200) {
            $admin = $this->data_admin();
            return response()->json([
                'icon' => "success",
                'message' => $reset['body']['message'],
                'status' => 'berhasil',
                'admin' => $admin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $reset['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_admin()
    {
        $admin = $this->adminApi->sekolah();
        $admin = $admin['body']['data'];
        $html = '';
        if (empty($admin)) {
            $html .= '<tr><td colspan="6" class="text-center">Data saat ini tidak tersedia</td></tr>';
        } else {
            $no = 1;
            foreach ($admin as $adm) {
                $jenkel = "Perempuan";
                $status = 'Disabled';
                if($adm['jenkel'] == 'l'){
                    $jenkel = "Laki - laki";
                }
                if ($adm['status'] != 0) {
                    $status = 'Enabled';
                }
                $html .= '
                <tr>
                <td>'.$no++.'</td>
                <td>'.strtoupper($adm['nama']).'</td>
                <td>'.$adm['telepon'].'</td>
                <td>'.$adm['email'].'</td>
                <td>'.$adm['first_password'].'</td>
                <td>
                    <button data-toggle="collapse" data-target="#detail'.$adm['id'].'"
                        class="btn btn-sm btn-purple"><i class="fas fa-info-circle"></i></button>
                    <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                        class="edit btn btn-sm btn-info"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                        class="delete btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)" data-id="'.$adm['id'].'"
                        class="reset_pass btn btn-sm btn-warning"><i class="fas fa-key"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="detail'.$adm['id'].'">
                        <table class="table table-striped">
                            <tr>
                                <th class="text-center vertical-middle" colspan="2" rowspan="5">
                                    <img src="'.$adm['file'].'" alt="" height="200px">
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <th class="text-center">Nama</th>
                                <td>'.ucfirst($adm['nama']).'</td>                                                            
                            </tr>
                            <tr>
                                <td></td>
                                <th class="text-center">NIP</th>
                                <td>'.$adm['nip'].'</td>
                            </tr>
                            <tr>
                                <td></td>
                                <th class="text-center">NIK</th>
                                <td>'.ucfirst($adm['nik']).'</td>                                                           
                            </tr>
                            <tr>
                                <td></td>
                                <th class="text-center">NUPTK</th>
                                <td>'.$adm['nuptk'].'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Jenis Kelamin</th>
                                <td>'.$jenkel.'</td>
                                <td></td>
                                <th class="text-center">Agama</th>
                                <td>'.ucfirst($adm['agama']).'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Telepon</th>
                                <td>'.$adm['telepon'].'</td>
                                <td></td>
                                <th class="text-center">Email</th>
                                <td>'.$adm['email'].'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Tempat Lahir</th>
                                <td>'.$adm['tempat_lahir'].'</td>
                                <td></td>
                                <th class="text-center">Tanggal Lahir</th>
                                <td>'.Help::getTanggal($adm['tgl_lahir']).'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Alamat</th>
                                <td>'.$adm['alamat'].'</td>
                                <td></td>
                                <th class="text-center">Reset Password</th>
                                <td>'.$adm['first_password'].'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Terakhir Login</th>
                                <td>'.Help::getTanggal($adm['last_login']).'</td>
                                <td></td>
                                <th class="text-center">Status</th>
                                <td>'.$status.'</td>
                            </tr>
                            <tr>
                                <th class="text-center">Dibuat</th>
                                <td>'.Help::getTanggal($adm['created_at']).'</td>
                                <td></td>
                                <th class="text-center">Terakhir di update</th>
                                <td>'.Help::getTanggalLengkap($adm['updated_at']).'</td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
                ';
            }
        }
        return $html;
    }
}
