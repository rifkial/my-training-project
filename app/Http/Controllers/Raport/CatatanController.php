<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiCatatanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CatatanController extends Controller
{
    private $kelasSiswaApi;
    private $nilaiCatatanApi;

    public function __construct()
    {
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->nilaiCatatanApi = new NilaiCatatanApi();
    }

    public function index()
    {
        $naik = array(
            "Y"=>"Ya",
            "N"=>"Tidak"
        );
        // dd($naik);
        $siswa = $this->nilaiCatatanApi->get_by_tahun(session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        if (empty($siswa)) {
            $all_siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
            // dd($all_siswa);
            $all_siswa = $all_siswa['body']['data'];
            $siswa = [];
            foreach($all_siswa as $all){
                $siswa[] = array(
                    "id" => $all['id'],
                    "nama" => $all['nama'],
                    "nisn" => $all['nisn'],
                    "isi" => "-",
                    "naik_kelas" => "Y",
                ) ;
            }
            $form = "add";
        } else {
            $form = "edit";
        }
        // dd($siswa);
        return view('content.raport.guru.catatan.v_catatan')->with(['template' => session('template'), 'form' => $form, "siswa" => $siswa, "naik" => $naik]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $p = $request;

        $mode_form = $p['mode_form'];

        for ($i = 1; $i < $p['jumlah']; $i++) {
            $id_siswa = $p['id_siswa_'.$i];
            $naik = $p['naik_'.$i];
            $catatan = $p['catatan_'.$i] == "" ? "-" : $p['catatan_'.$i];

            $data_mentah[] = array(
                'id_siswa' => $id_siswa,
                'naik' => $naik,
                'catatan' => $catatan,
            );
        }
        foreach ($data_mentah as $mnth) {
            if ($mode_form == "add") {
                $data_insert = array(
                    'id_ta_sm' => session('id_tahun_ajar'),
                    'id_kelas_siswa' => $mnth['id_siswa'],
                    'naik_kelas' => $mnth['naik'],
                    'isi' => $mnth['catatan'],
                    'id_sekolah' => session('id_sekolah'),
                );
                $result_add = $this->nilaiCatatanApi->create(json_encode($data_insert));
            } else {
                // dd($mnth);
                $prets = $this->nilaiCatatanApi->get_by_id($mnth['id_siswa']);
                // dd($prets);
                $tes_kelas = $prets['body']['data'];
                $preeet = $tes_kelas['id_kelas_siswa'];
                // dd($preeet);
                $data_update = array(
                    'id_ta_sm' => session('id_tahun_ajar'),
                    'id' => $mnth['id_siswa'],
                    'id_kelas_siswa' => $preeet,
                    'naik_kelas' => $mnth['naik'],
                    'isi' => $mnth['catatan'],
                    'id_sekolah' => session('id_sekolah'),
                );
                $result_update = $this->nilaiCatatanApi->update_info(json_encode($data_update));
            }
        }
        // dd($data_mentah);

        $d['status'] = "ok";
        $d['data'] = "Data berhasil disimpan..";

        return response()->json($d);
    }
}
