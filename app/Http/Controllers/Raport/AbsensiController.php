<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiAbsensiApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AbsensiController extends Controller
{
    private $kelasSiswaApi;
    private $nilaiAbsensiApi;

    public function __construct()
    {
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->nilaiAbsensiApi = new NilaiAbsensiApi();
    }


    public function index()
    {
        // dd(session()->all());
        $data_siswa = $this->nilaiAbsensiApi->get_gabungan(session('tahun'), session('id_tahun_ajar'), session('id_rombel'));
        // dd($data_siswa);
        $data_siswa = $data_siswa['body']['data'];
        $url = $this->nilaiAbsensiApi->import(session('tahun'), session('id_tahun_ajar'), session('id_rombel'));
        $siswa = [];
        foreach ($data_siswa as $ds) {
            $siswa[] = array(
                'id_kelas_siswa' => $ds['id_kelas_siswa'],
                'nis' => $ds['nis'],
                'nisn' => $ds['nisn'],
                'nama' => $ds['nama'],
                'alpha' => $ds['alpha'] == null ? 0 : $ds['alpha'],
                'sakit' => $ds['sakit'] == null ? 0 : $ds['sakit'],
                'izin' => $ds['izin'] == null ? 0 : $ds['izin'],
            );
        }
        return view('content.raport.guru.absensi.v_absensi')->with(['template' => session('template'), 'siswa' => $siswa, 'url' => $url]);
    }

    public function store(Request $request)
    {
        $p = $request;

        $mode_form = $p['mode_form'];
        for ($i = 1; $i < $p['jumlah']; $i++) {
            $id_siswa = $p['id_siswa_'.$i];
            $as = $p['s_'.$i];
            $ai = $p['i_'.$i];
            $aa = $p['a_'.$i];
            $data_mentah[] = array(
                "siswa" => $id_siswa,
                "sakit" => $as,
                "izin" => $ai,
                "alfa" => $aa,
            );
        }
        foreach ($data_mentah as $mnth) {
            // if ($mode_form == "add") {
            $data_insert = array(
                        'id_ta_sm' => session('id_tahun_ajar'),
                        'id_kelas_siswa' => $mnth['siswa'],
                        'sakit' => $mnth['sakit'],
                        'izin' => $mnth['izin'],
                        'alpha' => $mnth['alfa'],
                        'id_sekolah' => session('id_sekolah'),
                    );
            $result_add = $this->nilaiAbsensiApi->create(json_encode($data_insert));
        }
        $d['status'] = "ok";
        $d['data'] = "Data berhasil disimpan..";
        return response()->json($d);
    }

    public function get_template()
    {
        $template = $this->nilaiAbsensiApi->import();
        return ($template);
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->nilaiAbsensiApi->upload_excel(json_encode($data));
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
