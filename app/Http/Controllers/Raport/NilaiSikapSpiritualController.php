<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiSpiritualApi;
use App\ApiService\Raport\SikapSpiritualApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NilaiSikapSpiritualController extends Controller
{
    private $kelasSiswaApi;
    private $sikapSpiritualApi;
    private $nilaispiritualApi;

    public function __construct()
    {
        $this->profileApi = new ProfileApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->sikapSpiritualApi = new SikapSpiritualApi();
        $this->nilaispiritualApi = new NilaiSpiritualApi();
    }

    public function index()
    {
        // dd(session()->all());
        $siswa = $this->nilaispiritualApi->get_by_gabungan(session('tahun'), session('id_tahun_ajar'));
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $sikap = $this->sikapSpiritualApi->get_by_sekolah();
        // dd($sikap);
        $form = '';
        $sikap = $sikap['body']['data'];
        return view('content.raport.guru.nilai_sikap_spiritual.v_nilai_sikap_spiritual_revisi')->with(['template' => session('template'), "siswa" => $siswa, "sikap" => $sikap, 'form' => $form]);
    }

    public function store(Request $request)
    {
        $p = $request;
        if ($p['mode_form'] == "add") {
            $sql = array();
            for ($i = 1; $i < $p['jumlah']; $i++) {
                $selalu = $p['ssp1_'.$i]."<br>".$p['ssp2_'.$i];
                $meningkat = $p['ssp3_'.$i];
                $sql[] = array(
                    "id_wali_kelas" => session('id'),
                    "id_kelas_siswa" => (int)$p['id_siswa_'.$i],
                    "id_ta_sm" => session('id_tahun_ajar'),
                    "selalu" => $selalu,
                    "meningkat" => $meningkat,
                    "id_sekolah" => session('id_sekolah'),
                );
            }
            foreach ($sql as $tes) {
                $data = array(
                    "id_wali_kelas" => $tes['id_wali_kelas'],
                    "id_kelas_siswa" => $tes['id_kelas_siswa'],
                    "id_ta_sm" => $tes['id_ta_sm'],
                    "selalu" => $tes['selalu'],
                    "meningkat" => $tes['meningkat'],
                    "id_sekolah" => $tes['id_sekolah'],
                );
                $result = $this->nilaispiritualApi->create(json_encode($data));
                // dd($result);
            }
        } elseif ($p['mode_form'] == "edit") {
            for ($i = 1; $i < $p['jumlah']; $i++) {
                $selalu = $p['ssp1_'.$i]."<br>".$p['ssp2_'.$i];
                $meningkat = $p['ssp3_'.$i];
                $sql[] = array(
                    "id_wali_kelas" => session('id'),
                    "id" => (int)$p['id_siswa_'.$i],
                    "id_ta_sm" => session('id_tahun_ajar'),
                    "selalu" => $selalu,
                    "meningkat" => $meningkat,
                    "id_sekolah" => session('id_sekolah'),
                );
            }
            foreach ($sql as $tes) {
                $nilai_sikap = $this->nilaispiritualApi->get_by_id($tes['id']);
                $nilai_sikap = $nilai_sikap['body']['data'];
                $data_update = array(
                    "id_wali_kelas" => $tes['id_wali_kelas'],
                    "id_kelas_siswa" => $nilai_sikap['id_kelas_siswa'],
                    "id" => $tes['id'],
                    "id_ta_sm" => $tes['id_ta_sm'],
                    "selalu" => $tes['selalu'],
                    "meningkat" => $tes['meningkat'],
                    "id_sekolah" => $tes['id_sekolah'],
                );
                $result = $this->nilaispiritualApi->update_info(json_encode($data_update));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'warning'
            ]);
        }
    }

    public function simpan_manual(Request $request)
    {
        $p = $request;
        $sql = array();
        for ($i = 1; $i <= $p['jumlah']; $i++) {
            $selalu = $p['ssp1_'.$i];
            $meningkat = $p['ssp2_'.$i];
            $sql[] = array(
                    "id_wali_kelas" => session('id'),
                    "id_kelas_siswa" => (int)$p['id_siswa_'.$i],
                    "id_ta_sm" => session('id_tahun_ajar'),
                    "selalu" => $selalu,
                    "meningkat" => $meningkat,
                    "id_sekolah" => session('id_sekolah'),
                );
        }
        // dd($sql);
        foreach ($sql as $tes) {
            $data = array(
                    "id_wali_kelas" => $tes['id_wali_kelas'],
                    "id_kelas_siswa" => $tes['id_kelas_siswa'],
                    "id_ta_sm" => $tes['id_ta_sm'],
                    "selalu" => $tes['selalu'],
                    "meningkat" => $tes['meningkat'],
                    "id_sekolah" => $tes['id_sekolah'],
                );
            if ($data['selalu'] != null || $data['meningkat'] != null) {
                $result =  $this->nilaispiritualApi->create(json_encode($data));
                // dd($result);
            }
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error'
            ]);
        }
    }

    public function cetak()
    {
        $profile = $this->profileApi->get_profile();
        // dd($profile);
        $profile = $profile['body']['data'];
        $siswa = $this->nilaispiritualApi->get_by_gabungan(session('tahun'), session('id_tahun_ajar'));
        $siswa = $siswa['body']['data'];
        return view('content.raport.guru.nilai_sikap_spiritual.v_cetak')->with(['siswa' => $siswa, 'profile' => $profile]);
    }
}
