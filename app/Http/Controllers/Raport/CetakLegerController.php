<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Raport\ConfigApi;
use App\ApiService\Raport\LihatRaportApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CetakLegerController extends Controller
{
    private $lihatApi;
    private $configApi;

    public function __construct()
    {
        $this->lihatApi = new LihatRaportApi();
        $this->configApi = new ConfigApi();
    }

    public function index()
    {
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        $config = $config['body']['data'];
        session()->put('title', 'Cetak Leger');
        // dd($config);
        return view('content.raport.guru.cetak_leger.v_cetak_leger')->with(['template' => session('template'), 'config' => $config]);
    }

    public function print_leger()
    {
        $cetak = $this->lihatApi->print_leger_kd(session('id_tahun_ajar'), session('id_rombel'));
        dd($cetak);
    }
}
