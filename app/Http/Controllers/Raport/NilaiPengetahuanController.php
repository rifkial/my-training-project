<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\KompetensiDasarApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Raport\NilaiApi;
use App\ApiService\Raport\NilaiConfigApi;
use App\ApiService\Raport\NilaiDasarApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class NilaiPengetahuanController extends Controller
{
    private $gurupelajaranApi;
    private $dasarApi;
    private $nilaiDasarApi;
    private $kelasSiswaApi;
    private $nilaiApi;
    private $configApi;
    private $intiApi;
    private $hashids;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->dasarApi = new KompetensiDasarApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->nilaiDasarApi = new NilaiDasarApi();
        $this->nilaiApi = new NilaiApi();
        $this->configApi = new NilaiConfigApi();
        $this->intiApi = new KompetensiApi();
        $this->hashids = new Hashids();
    }

    public function index($id_code)
    {
        $idcode = request()->segment(4);
        $id = $this->hashids->decode($idcode)[0];
        $guru = $this->gurupelajaranApi->get_by_id($id);
        // dd($guru);
        $guru = $guru['body']['data'];
        $inti = $this->intiApi->by_mapel_kelas_jenis($guru['id_mapel'], $guru['id_kelas'], "pengetahuan");
        // dd($inti);
        $inti = $inti['body']['data'];
        return view('content.raport.guru.mapel.v_nilai_pengetahuan')->with(['template' => session('template'), 'guru' => $guru, 'inti' => $inti]);
        // return view('content.raport.guru.mapel.v_nilai_pengetahuan_revisi')->with(['template' => session('template'), 'guru' => $guru, 'inti' => $inti]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $id_guru = $request['id_guru_mapel'];
        $id_kd = $request['id_mapel_kd'];
        $id_kelas = $request['id_kelas'];
        $id_rombel = $request['id_rombel'];
        $id_mapel = $request['id_mapel'];
        $jenis = $request['jenis'];
        $id_tahun_ajar = session('id_tahun_ajar');
        $i = 0;
        $data = [];
        foreach ($request['nilai'] as $s) {
            $data[] = array(
                    'id' => $request['id_siswa'][$i],
                    'jenis' => $jenis,
                    'id_ta_sm' => $id_tahun_ajar,
                    'id_kelas_siswa' => $request['id_siswa'][$i],
                    'id_kd' => $id_kd,
                    'id_mapel' => $id_mapel,
                    'id_kelas' => $id_kelas,
                    'id_rombel' => $id_rombel,
                    'id_guru' => $id_guru,
                    'nilai' => $s,
                    'id_sekolah' => session('id_sekolah'),
                );
            $i++;
        }
        foreach ($data as $dt) {
            $data_proccess = array(
                'jenis' => $dt['jenis'],
                'id_kelas_siswa' => $dt['id_kelas_siswa'],
                'id_ta_sm' => $dt['id_ta_sm'],
                'id_kd' => $dt['id_kd'],
                'id_mapel' => $dt['id_mapel'],
                'id_kelas' => $dt['id_kelas'],
                'id_rombel' => $dt['id_rombel'],
                'id_guru' => $dt['id_guru'],
                'nilai' => $dt['nilai'],
                'id_sekolah' => $dt['id_sekolah'],
            );
            if ($data_proccess['nilai'] != null) {
                $result = $this->nilaiDasarApi->create(json_encode($data_proccess));
            }
        }
        if ($result['code'] == 200) {
            $d['status'] = "berhasil";
            $d['icon'] = "success";
            $d['message'] = $result['body']['message'];
        } else {
            $d['status'] = "gagal";
            $d['icon'] = "danger";
            $d['message'] = $result['body']['message'];
        }
        return response()->json($d);
    }



    public function edit($id)
    {
        $q = $this->db->query("SELECT *, 'edit' AS mode FROM t_mapel_kd WHERE id = '$id'")->row_array();

        $d = array();
        $d['status'] = "ok";

        if (empty($q)) {
            $d['data']['id'] = "";
            $d['data']['mode'] = "add";
            $d['data']['id_guru'] = "";
            $d['data']['id_mapel'] = "";
            $d['data']['tingkat'] = "";
            $d['data']['no_kd'] = "";
            $d['data']['jenis'] = "";
            $d['data']['jenis'] = "";
            $d['data']['nama_kd'] = "";
        } else {
            $d['data'] = $q;
        }

        j($d);
    }

    public function list_kd(Request $request)
    {
        // dd("tes");
        // dd($request);
        $dasar = $this->dasarApi->by_mapel_kelas_jenis($request['id_mapel'], $request['id_kelas'], $request['jenis']);
        $dasar = $dasar['body']['data'];
        return response()->json($dasar);
    }
}
