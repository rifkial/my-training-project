<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Raport\ConfigApi;
use App\ApiService\Raport\LihatRaportApi;
use App\ApiService\Raport\NilaiCatatanApi;
use App\ApiService\Raport\NilaiPrestasiApi;
use App\ApiService\Raport\TemplateSampulApi;
use App\ApiService\User\WaliKelasApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use PDF;
use Illuminate\Http\Request;

class CetakRaportController extends Controller
{
    private $kelasSiswaApi;
    private $tahunAjarApi;
    private $prestasiApi;
    private $catatanApi;
    private $sampulApi;
    private $sekolahApi;
    private $configApi;
    private $waliKelasApi;
    private $lihatApi;
    private $hashId;

    public function __construct()
    {
        $this->WaliKelasApi = new WaliKelasApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->tahunAjarApi = new TahunAjaranApi();
        $this->prestasiApi = new NilaiPrestasiApi();
        $this->catatanApi = new NilaiCatatanApi();
        $this->sampulApi = new TemplateSampulApi();
        $this->sekolahApi = new SekolahApi();
        $this->configApi = new ConfigApi();
        $this->waliKelasApi = new WaliKelasApi();
        $this->lihatApi = new LihatRaportApi();
        $this->hashId = new Hashids();
    }

    public function index()
    {
        // dd(session()->all());
        session()->put('title', 'Cetak Raport');
        $tahun = $this->tahunAjarApi->get_by_semester(session('tahun'));
        $tahun = $tahun['body']['data'];
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        // dd($config);
        $config = $config['body']['data'];
        // dd($tahun);
        if (session("role") == "siswa" || session('role') == 'ortu') {
            $siswa = $this->kelasSiswaApi->get_by_id(session('id'));
            // dd($siswa);
            $siswa = $siswa['body']['data'];
            $siswa['id_code'] = $this->hashId->encode($siswa['id']);
            return view('content.raport.guru.cetak_raport.v_cetak_raport_siswa')->with(['template' => session('template'), 'siswa' => $siswa, 'tahun' => $tahun]);
        } else {
            $siswa = $this->kelasSiswaApi->get_by_rombel(session('id_rombel'), session('tahun'));
            // dd($siswa);
            $siswa = $siswa['body']['data'];
            return view('content.raport.guru.cetak_raport.v_cetak_raport')->with(['template' => session('template'), 'siswa' => $siswa, 'config' => $config]);
        }
    }

    public function cetak_sampul1($id)
    {
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        if ($config['code'] != 200) {
            $message = array(
                'message' => "Mohon maaf, admin belum set Config raport untuk tahun ajaran yang aktif",
                'status' => 'gagal',
                'icon' => 'error',
            );
            return redirect()->back()->with(['message' => $message]);
        }
        $config = $config['body']['data'];
        // dd($config);
        if (session('role') == "siswa") {
            if ($config['jenis'] == "kd") {
                $lihat = $this->lihatApi->kd_auth_siswa(session('id_tahun_ajar'));
            } elseif($config['jenis'] == "k16"){
                $lihat = $this->lihatApi->k16_auth_siswa(session('id_tahun_ajar'));
            }else {
                $lihat = $this->lihatApi->manual_auth_siswa(session('id_tahun_ajar'));
            }
            $kop = $this->sampulApi->get_by_S4(session('id_kelas_siswa'));
            $catatan = $this->catatanApi->get_by_siswa(session('id_kelas_siswa'), session('id_tahun_ajar'));
        } else {
            if ($config['jenis'] == "kd") {
                $lihat = $this->lihatApi->kd_kelas_siswa(Help::decode(last(request()->segments())), session('id_tahun_ajar'));
            } elseif($config['jenis'] == "k16"){
                $lihat = $this->lihatApi->k16_kelas_siswa(Help::decode(last(request()->segments())), session('id_tahun_ajar'));
            } else {
                $lihat = $this->lihatApi->manual_kelas_siswa(Help::decode(last(request()->segments())), session('id_tahun_ajar'));
            }
            $kop = $this->sampulApi->get_by_S4(Help::decode(last(request()->segments())));
            $catatan = $this->catatanApi->get_by_siswa(Help::decode(last(request()->segments())), session('id_tahun_ajar'));
        }
        // dd($lihat);


        $lihat = $lihat['body']['data'];
        // dd($lihat);
        session()->put('title', str_replace(' ', '-', $lihat['nama']));
        $kop = $kop['body']['data'];
        $catatan = $catatan['body']['data'];
        if ($config['jenis'] == "kd") {
            // if (session('role') == 'siswa' || session('role') == 'ortu') {
            $pdf = PDF::loadview('content.raport.guru.cetak_raport.v_print_kd', ['lihat' => $lihat, 'kop' => $kop, 'catatan' => $catatan]);
            return $pdf->stream();
        } elseif ($config['jenis'] == "k16") {
            $pdf = PDF::loadview('content.raport.guru.cetak_raport.v_print_k16', ['lihat' => $lihat, 'kop' => $kop, 'catatan' => $catatan]);
            return $pdf->stream();
        } else {
            return view('content.raport.guru.cetak_raport.v_cetak_manual', compact('lihat', 'kop', 'catatan'));
        }
    }

    public function cetak_sampul2($id)
    {
        if (session("role") == "siswa") {
            $siswa = $this->sampulApi->get_S2();
        } else {
            $siswa = $this->kelasSiswaApi->get_by_id($this->hashId->decode(request()->segment(5))[0]);
        }
        $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
        $sekolah = $sekolah['body']['data'];
        // dd($sekolah);


        $siswa = $siswa['body']['data'];
        return view('content.raport.guru.cetak_raport.v_sampul2')->with(['siswa' => $siswa, 'sekolah' => $sekolah]);
    }

    public function cetak_sampul3($id)
    {
        if (session('role') == "siswa") {
            $data = $this->sampulApi->get_S4();
        } else {
            $data = $this->sampulApi->get_by_S4($this->hashId->decode(request()->segment(5))[0]);
        }
        // dd($data);
        $data = $data['body']['data'];
        $sekolah = $this->sekolahApi->get_by_id(session('id'));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        // dd($sekolah);
        $kop = $data['kop'];
        $siswa = $data['siswa'];
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        $config = $config['body']['data'];
    }

    public function cetak_sampul_raport($id)
    {
        // dd(session()->all());
        $tahun = $this->tahunAjarApi->get_by_id(session('id_tahun_ajar'));
        $semester = $tahun['body']['data']['angka_semester'];
        if (session("role") == "siswa") {
            $siswa = $this->kelasSiswaApi->cetak_raport(session('id'), request()->segment(5));
        } else {
            $siswa = $this->kelasSiswaApi->cetak_raport($this->hashId->decode(request()->segment(5))[0], session('id_tahun_ajar'));
        }
        // dd($siswa);
        if ($siswa['code'] != 200) {
            $pesan = array(
                'message' => $siswa['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }

        $siswa = $siswa['body']['data'];
        $spiritual = $siswa['nilai_spiritual'];
        if ($spiritual['selalu'] != null) {
            $nilai_sikap_spiritual = 'Selalu melakukan sikap : ' . $spiritual['selalu'] . '; Mulai meningkat pada sikap : ' . $spiritual['meningkat'];
        } else {
            $nilai_sikap_spiritual = 'Belum diinput';
        }

        $nilai_sosial = $siswa['nilai_sosial'];
        $selalu = $nilai_sosial['selalu'];
        // dd($selalu);
        if ($selalu == null) {
            $nilai_sikap_sosial = 'Belum diinput';
        } else {
            $ruwet = array();
            foreach ($selalu as $sl) {
                $ruwet[] = $sl['nama'];
            }
            $so_text_selalu = implode(", ", $ruwet);
        }
        $wali_kelas = $siswa['wali_kelas'];
        $nilai_mapel = $siswa['nilai_mapel'];
        $config = $this->configApi->by_tahun_aktif(session('id_tahun_ajar'));
        $config = $config['body']['data'];
        return view('content.raport.guru.cetak_raport.v_sampul_raport')->with([
            'siswa' => $siswa, 'sosial' => $nilai_sikap_sosial, 'spiritual' => $nilai_sikap_spiritual,
            'ekstra' => $nilai_ekstra, 'absensi' => $nilai_absensi, 'semester' => $semester, 'wali' => $wali_kelas, 'nilai_utama' => $nilai_mapel, 'config' => $config
        ]);
    }

    public function cetak_prestasi($id)
    {
        $siswa = $this->kelasSiswaApi->get_by_id($id);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $prestasi = $this->prestasiApi->get_by_siswa($id, session('id_tahun_ajar'));
        $prestasi = $prestasi['body']['data'];
        $catatan = $this->catatanApi->get_by_siswa($id, session('id_tahun_ajar'));
        // dd($catatan);
        $catatan = $catatan['body']['data'];
        // dd($prestasi);
        return view('content.raport.guru.cetak_raport.v_cetak_prestasi')->with(['siswa' => $siswa, 'prestasi' => $prestasi, 'catatan' => $catatan]);
    }
}
