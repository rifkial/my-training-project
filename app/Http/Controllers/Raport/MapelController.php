<?php

namespace App\Http\Controllers\Raport;

use App\ApiService\Master\GuruPelajaranApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    private $gurupelajaranApi;
    private $hashids;

    public function __construct()
    {
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->hashids = new Hashids();
    }
    public function index()
    {
        $mapel = $this->gurupelajaranApi->get_by_guru(session('id_tahun_ajar'));
        // dd($mapels);
        $mapel = $mapel['body']['data'];
        session()->put('title', "Dafar Pelajaran");
        return view('content.raport.guru.mapel.v_mapel')->with(['template' => session('template'), 'mapel' => $mapel]);
    }
}
