<?php

namespace App\Http\Controllers\Sarana\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
//use Carbon\Carbon;

use App\ApiService\Sarana\Laporan\LaporanApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Sarana\Master\LokasiApi;
use Session;
use PDF;

class LaporanController extends Controller
{
 
	private $laporanApi;
    private $sekolahApi;
    private $lokasiApi;

	function __construct(){
    	$this->laporanApi = new LaporanApi();
        $this->sekolahApi = new SekolahApi();
        $this->lokasiApi = new LokasiApi();
    }

    //BARANG
    public function barang(Request $request){
    	$barangs = $this->laporanApi->get_barang();
    	$barangs = $barangs["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "barang";
        //dd($barangs);
    
        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.barang.pdf_laporan_barang',compact("sekolah","barangs","keterangan"));
            return $pdf->download('laporan_sarana_barang.pdf');
        }

        return view('content.sarpras.laporan.barang.laporan_barang',compact("sekolah","barangs","keterangan"));
    }

    public function item(Request $request){
    	$datas = $this->laporanApi->get_item();
    	$datas = $datas["body"]["data"];
    	$id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua item";
        //dd($datas);

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.item.pdf_laporan_item',compact("sekolah","datas","keterangan"));
            return $pdf->download('laporan_sarana_item.pdf');
        }

        return view('content.sarpras.laporan.item.laporan_item',compact("sekolah","datas"));
    }

    public function user(Request $request){
        $users = $this->laporanApi->get_user();
        $users = $users["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua user";
        //dd($users);

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.user.pdf_laporan_user',compact("sekolah","users","keterangan"));
            return $pdf->download('laporan_sarana_user.pdf');
        }

        return view('content.sarpras.laporan.user.laporan_user',compact("sekolah","users"));
    }

    public function item_by_lokasi(Request $request){
    	$datas = $this->laporanApi->get_item();
        $datas = $datas["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $lokasis = $this->lokasiApi->get_by_sekolah();
        $lokasis = $lokasis["body"]["data"];
        $keterangan = "item berdasarkan lokasi";
        //dd($datas);

        return view('content.sarpras.laporan.item.laporan_item_by_lokasi',compact("sekolah","datas","lokasis"));
    }

    public function print_item_by_lokasi($id,Request $request){
        $items = $this->laporanApi->get_item_by_lokasi($id);
        $items = $items["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "Item di ".$items[0]["lokasi"];
        $filter = "lokasi";
        
        $pdf = PDF::loadView('content.sarpras.laporan.item.pdf_laporan_item_filter',compact("sekolah","items","keterangan","filter"));
        return $pdf->download('laporan_sarana_item.pdf');
        
    }

    public function item_by_kondisi(Request $request){
    	$datas = $this->laporanApi->get_item();
        $datas = $datas["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "item berdasarkan kondisi";


        return view('content.sarpras.laporan.item.laporan_item_by_kondisi',compact("sekolah","datas"));
    }

    public function print_item_by_kondisi($kondisi,Request $request){
        $items = $this->laporanApi->get_item_by_kondisi($kondisi);
        $items = $items["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        //dd($items);
        $keterangan = "Item kondisi ".$items[0]["kondisi"];
        $filter = "kondisi";

        $pdf = PDF::loadView('content.sarpras.laporan.item.pdf_laporan_item_filter',compact("sekolah","items","keterangan","filter"));
        return $pdf->download('laporan_sarana_item.pdf');
        
    }


    public function pengadaan(Request $request){
    	$pengadaans = $this->laporanApi->get_pengadaan();
    	$pengadaans = $pengadaans["body"]["data"];
    	
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua pengadaan";
        
        //HITUNG JUMLAH HARGA
        $harga = 0;
        foreach($pengadaans as $x){
            $harga = $harga + $x["total_harga"];
        }

        //dd($harga);

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.pengadaan.pdf_laporan_pengadaan',compact("sekolah","pengadaans","keterangan","harga"));
            return $pdf->download('laporan_sarana_pengadaan.pdf');
        }

        return view('content.sarpras.laporan.pengadaan.laporan_pengadaan',compact("sekolah","pengadaans","harga"));
    }

    public function pengadaan_diterima(Request $request){
    	$pengadaans = $this->laporanApi->get_pengadaan_diterima();
    	$pengadaans = $pengadaans["body"]["data"];
    	$id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "pengadaan diterima";
        //dd($pengadaans);

        //HITUNG JUMLAH HARGA
        $harga = 0;
        foreach($pengadaans as $x){
            $harga = $harga + $x["total_harga"];
        }


        if($request->ajax()){
            $table = datatables()->of($pengadaans);
            $table->addIndexColumn();
            return $table->make(true);
        }

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.pengadaan.pdf_laporan_pengadaan',compact("sekolah","pengadaans","keterangan","harga"));
            return $pdf->download('laporan_sarana_pengadaan.pdf');
        }

        return view('content.sarpras.laporan.pengadaan.laporan_pengadaan_diterima',compact("sekolah","pengadaans","harga"));
    }

    public function pemusnahan(Request $request){
    	$pemusnahans = $this->laporanApi->get_pemusnahan();
    	$pemusnahans = $pemusnahans["body"]["data"];
    	//dd($pemusnahans);

        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua pemusnahan";
        //dd($pemusnahan);

        if($request->ajax()){
            $table = datatables()->of($pemusnahans);
            $table->addIndexColumn();
            return $table->make(true);
        }

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.pemusnahan.pdf_laporan_pemusnahan',compact("sekolah","pemusnahans","keterangan"));
            return $pdf->download('laporan_sarana_pemusnahan.pdf');
        }

        return view('content.sarpras.laporan.pemusnahan.laporan_pemusnahan',compact("sekolah","pemusnahans"));
    }

    public function pemusnahan_diterima(Request $request){
    	$pemusnahans = $this->laporanApi->get_pemusnahan_diterima();
    	$pemusnahans = $pemusnahans["body"]["data"];
    	$id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "pemusnahan diterima";
        //dd($pemusnahan);

        if($request->ajax()){
            $table = datatables()->of($pemusnahans);
            $table->addIndexColumn();
            return $table->make(true);
        }

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.pemusnahan.pdf_laporan_pemusnahan',compact("sekolah","pemusnahans","keterangan"));
            return $pdf->download('laporan_sarana_pemusnahan_diterima.pdf');
        }

        return view('content.sarpras.laporan.pemusnahan.laporan_pemusnahan_diterima',compact("sekolah","pemusnahans"));
    }

    //BELUM
    public function peminjaman_with_time(Request $request){
        $start = $request->start;
        $end = $request->end;
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];

        if($start == null || $end == null){
            $peminjamans = $this->laporanApi->get_all_penyewaan();
            $keterangan = "semua peminjaman";
        }else{
            $peminjamans = $this->laporanApi->get_penyewaan($start,$end);
            $keterangan = "peminjaman dari ".$start." sampai ".$end;
        }

        $peminjamans = $peminjamans["body"]["data"];
        
        
        $pdf = PDF::loadView('content.sarpras.laporan.peminjaman.pdf_laporan_peminjaman',compact("sekolah","peminjamans","keterangan"));
        return $pdf->download('laporan_sarana_peminjaman_interval.pdf');
    }

    public function peminjaman(Request $request){
    	$peminjamans = $this->laporanApi->get_all_penyewaan();
        $peminjamans = $peminjamans["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua peminjaman belum kembali";
        //dd($peminjamans);

        if($request->ajax()){
            $table = datatables()->of($peminjamans);
            $table->addIndexColumn();
            return $table->make(true);
        }

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.peminjaman.pdf_laporan_peminjaman',compact("sekolah","peminjamans","keterangan"));
            return $pdf->download('laporan_sarana_peminjaman_belum.pdf');
        }

        return view('content.sarpras.laporan.peminjaman.laporan_peminjaman',compact("sekolah","peminjamans"));
    }

    

    public function peminjaman_dipinjam(Request $request){
    	$peminjamans = $this->laporanApi->get_penyewaan_pinjam();
    	$peminjamans = $peminjamans["body"]["data"];
        $id_sekolah = session('id_sekolah');
        $sekolah =  $this->sekolahApi->get_detail($id_sekolah);
        $sekolah = $sekolah["body"]["data"];
        $keterangan = "semua peminjaman belum kembali";
        //dd($peminjamans);

        if($request->ajax()){
            $table = datatables()->of($peminjamans);
            $table->addIndexColumn();
            return $table->make(true);
        }

        if($request->has('download'))
        {
            $pdf = PDF::loadView('content.sarpras.laporan.peminjaman.pdf_laporan_peminjaman',compact("sekolah","peminjamans","keterangan"));
            return $pdf->download('laporan_sarana_peminjaman_belum.pdf');
        }

        return view('content.sarpras.laporan.peminjaman.laporan_peminjaman_dipinjam',compact("sekolah","peminjamans"));
    }


}
