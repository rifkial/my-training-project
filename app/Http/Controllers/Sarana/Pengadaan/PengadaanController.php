<?php

namespace App\Http\Controllers\Sarana\Pengadaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\Barang\BarangApi;
use App\ApiService\Sarana\Pengadaan\PengadaanApi;

class PengadaanController extends Controller
{

	private $barangApi;
	private $pengadaanApi;

	function __construct(){
    	$this->barangApi = new BarangApi();
    	$this->pengadaanApi = new PengadaanApi();
    }

    public function index(Request $request)
    {
    	$barangs = $this->barangApi->get_by_sekolah();
    	$barangs = $barangs["body"]["data"];

    	$data = $this->pengadaanApi->status_pengadaan(0);
    	$data = $data["body"]["data"];

        //dd($data);

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">

                    <a onclick="modalEditPengadaan('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deletePengadaan('.$row['id'].')" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash ml-2" aria-hidden="true"></i>
                    </a>
                    <a onclick="detailPengadaan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pengadaan.pengadaan',compact("data","barangs"));
    }

    public function admin_page(Request $request)
    {
        $barangs = $this->barangApi->get_by_sekolah();
        $barangs = $barangs["body"]["data"];

        $data = $this->pengadaanApi->status_pengadaan(0);
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="aksi('.$row['id'].')" class="btn btn-secondary btn-sm mx-1">
                         <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i>
                    </a>
                    
                    <a onclick="approve('.$row['id'].')" class="btn btn-success btn-sm mx-1">
                         <i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i>
                    </a>
                    <a onclick="reject('.$row['id'].')" class="btn btn-danger btn-sm">
                        <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i>
                    </a>
                    
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pengadaan.admin_pengadaan',compact("data","barangs"));
    }

    public function approved(Request $request){

        $barangs = $this->barangApi->get_by_sekolah();
        $barangs = $barangs["body"]["data"];

        $data = $this->pengadaanApi->status_pengadaan(1);
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPengadaan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pengadaan.pengadaan_diterima',compact("data","barangs"));

    }

    public function rejected(Request $request){

        $barangs = $this->barangApi->get_by_sekolah();
        $barangs = $barangs["body"]["data"];

        $data = $this->pengadaanApi->status_pengadaan(2);
        $data = $data["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPengadaan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pengadaan.pengadaan_ditolak',compact("data","barangs"));

    }

    
    public function my(Request $request)
    {
        return view('content.sarpras.pengadaan.user_pengadaan');
    }

    public function request(Request $request)
    {
        $data = array(
            'kode' => $request->kode,
            'id_barang' => $request->id_barang,
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'jumlah' => $request->jumlah,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->harga_satuan * $request->jumlah,
            'tgl_pengajuan' => now()->toDateString(),
            'tgl_diterima' => $request->tgl_diterima,
            'prioritas' => $request->prioritas,
            'id_sekolah' => session('id_sekolah')
        );

        // return response()->json([
        //         'message' => $data,
        //         'icon'  => 'success',
        //         'status'  => 'berhasil'
        //     ]);

        //dd($data);
        $result = $this->pengadaanApi->create(json_encode($data));

        //dd($result);


        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function riwayat(Request $request){
        $data = $this->pengadaanApi->get_by_sekolah();
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPengadaan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pengadaan.riwayat_pengadaan',compact("data"));
    }

    public function detail(Request $request){
        $id = $request->id;

        $result = $this->pengadaanApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request){

        $id = $request->id;

        $data = array(
            'kode' => $request->kode,
            'id_barang' => $request->id_barang,
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'jumlah' => $request->jumlah,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->harga_satuan * $request->jumlah,
            'prioritas' => $request->prioritas,
            'tgl_diterima' => $request->tgl_diterima,
            'tgl_pengajuan' => now()->toDateString(),
            'id_sekolah' => session('id_sekolah')
        );

        // return response()->json([
        //         'message' => $data,
        //         'icon'  => 'success',
        //         'status'  => 'berhasil'
        //     ]);


        $result = $this->pengadaanApi->update($id,json_encode($data));

        //dd($result);


        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function aksi(Request $request){ //melakukan aksi,apakah diterima atau tidak. Proses awalnya ialah menyimpan input tgl diterima lalu approve/reject penjgajuan ( gk jadi , nunggu alur jelas)

        $id = $request->id_aksi;
        $id_pengadaan = $request->id_pengadaan;

        $data_tgl = array(
            'kode' => $request->kode_pengadaan,
            'tgl_diterima' => $request->tgl_diterima
        );

        $hasil = $this->pengadaanApi->update($id_pengadaan,json_encode($data_tgl)); //input diterima

        return response()->json([
                'message' => $hasil,
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);



        if($id == 1){
            $data = array(
                'id' => $request->id_pengadaan,
                'diterima' => 1 
            );
        }else{
            $data = array(
                'id' => $request->id_pengadaan,
                'diterima' => 2 
            );
        }

        $result = $this->pengadaanApi->action(json_encode($data));

        // if($result["code"] == 200){
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'success',
        //         'status'  => 'berhasil'
        //     ]);
        // }else {
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'error',
        //         'status'  => 'gagal'
        //     ]);
        // }



    }

    public function delete(Request $request){
        $id = $request->id;

        $result = $this->pengadaanApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function approve(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id' => $request->id,
            'diterima' => 1,
            'tgl_diterima' => now()->toDateString()
        );


        $result = $this->pengadaanApi->action(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function reject(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id' => $request->id,
            'diterima' => 2,
            'tgl_diterima' => now()->toDateString()    
        );

        $result = $this->pengadaanApi->action(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    

}
