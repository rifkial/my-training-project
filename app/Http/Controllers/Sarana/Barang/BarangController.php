<?php

namespace App\Http\Controllers\Sarana\Barang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\ApiService\Sarana\Barang\BarangApi;
use App\ApiService\Sarana\Master\KategoriApi;
use App\ApiService\Sarana\Master\JenisApi;
use App\ApiService\Sarana\Master\SatuanApi;
use App\ApiService\Sarana\Master\SupllyerApi;
use App\ApiService\Sarana\Master\LokasiApi;
use App\ApiService\Sarana\Item\ItemApi;

class BarangController extends Controller
{
	private $barangApi;
    private $itemApi;
	private $kategoriApi;
    private $jenisApi;
    private $satuanApi;
    private $supllyerApi;
    private $lokasiApi;

    public $detail = 'detail-item-barang';

    function __construct(){
        $this->itemApi = new ItemApi();
        $this->lokasiApi = new LokasiApi();
    	$this->barangApi = new BarangApi();
    	$this->kategoriApi = new KategoriApi();
        $this->jenisApi = new JenisApi();
        $this->satuanApi = new SatuanApi();
        $this->supllyerApi = new SupllyerApi();
    }

    function index(Request $request){
    	$satuans = $this->satuanApi->get_by_sekolah();
        $satuans = $satuans["body"]["data"];

        $supllyers = $this->supllyerApi->get_by_sekolah();
        $supllyers = $supllyers["body"]["data"];

    	$kategoris = $this->kategoriApi->get_by_sekolah();
        $kategoris = $kategoris["body"]["data"];

        $jeniss = $this->jenisApi->get_by_sekolah();
        $jeniss = $jeniss["body"]["data"];

    	$data = $this->barangApi->get_by_sekolah();
    	$data = $data["body"]["data"];

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="modalEditBarang('.$row['id'].')" id="btnEdit" class="btn btn-success btn-sm mt-0 mr-1">
                         <i class="fa fa-pencil ml-0 mt-0" aria-hidden="true"></i>
                    </a>
                    <a onclick="detailBarang('.$row['id'].')" id="btnDetail" class="btn btn-warning btn-sm mt-0">
                         <i class="fa fa-info ml-2 mt-1" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.barang.barang',compact("jeniss","kategoris","satuans","supllyers"));
    }

    function create(Request $request){
    	$data = array(
    		'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'kode' => $request->kode,
            'id_satuan' => $request->id_satuan,
            'id_jenis' => $request->id_jenis,
            'id_kategori' =>$request->id_kategori,
            'id_suplayer' => $request->id_supllyer,
            'jumlah' => $request->jumlah,
            'tarif' => $request->tarif,
            'id_sekolah' => session('id_sekolah')
    	);

    	$result = $this->barangApi->create(json_encode($data));

    	if($result["code"] == 200){
    		return response()->json([
				'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
    		]);
    	}else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    function detail(Request $request){
        $id = $request->id;

        $result = $this->barangApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    function redirect(Request $request){
        $id = $request->id;
        return route('detail-item-barang', $id);
    }

    function detail_item($id,Request $request){
        $id = $id;

        $lokasis = $this->lokasiApi->get_by_sekolah();
        $lokasis = $lokasis["body"]["data"];

        $data = $this->barangApi->get_detail($id);
        $data = $data["body"]["data"];
        //dd($data);

        if (empty($data)) {
             Session::flash('message',"Tidak ada item pada barang pilihan Anda");
            return redirect()->back();
        }
        

        if($request->ajax()){
            $table = datatables()->of($data["items"]);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                <div class="input-group">
                    <a onclick="modalEditItem('.$row['id'].')" class="btn btn-success btn-sm mt-0 mr-1">
                         <i class="fa fa-pencil ml-0" aria-hidden="true"></i>
                    </a>
                    <a onclick="riwayatItem('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-1" aria-hidden="true"></i>
                    </a>
                </div>
                 ';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.barang.items-barang',compact("data","id","lokasis"));
    }

    function edit(Request $request){

        $id = $request->id;

        $data = array(
            'nama' => $request->nama,
            'kode' => $request->kode,
            'keterangan' => $request->keterangan,
            'id_satuan' => $request->id_satuan,
            'id_jenis' => $request->id_jenis,
            'id_kategori' =>$request->id_kategori,
            'id_suplayer' => $request->id_supllyer,
            'jumlah' => $request->jumlah,
            'tarif' => $request->tarif,
            'id_sekolah' => session('id_sekolah')
        );

        $result = $this->barangApi->update($id,json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->barangApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }


}


