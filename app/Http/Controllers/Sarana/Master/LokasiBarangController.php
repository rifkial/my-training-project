<?php

namespace App\Http\Controllers\Sarana\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Sarana\Master\LokasiApi;

class LokasiBarangController extends Controller
{
    private $lokasiApi;

    public function __construct(){
        $this->lokasiApi = new LokasiApi();
    }

    public function index(Request $request)
    {
        $data = $this->lokasiApi->get_by_sekolah();
        $data = $data["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                <div class="input-group">
                    <a onclick="modalEditLokasi('.$row['id'].')" class="btn btn-success btn-sm mt-0 mx-1">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deleteLokasi('.$row['id'].')" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash-o ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.master.lokasi');
    }

   public function create(Request $request)
    {
        
        $data = array(
            'nama' => $request->nama,
            'id_sekolah' => session('id_sekolah'),
        );

        $result = $this->lokasiApi->create(json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        $result = $this->lokasiApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

    
    public function edit(Request $request)
    {
        $id = $request->id;
        $data = array(
            
            'nama'=> $request->nama,
        );

        $result = $this->lokasiApi->update($id,json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->lokasiApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }
}
