<?php

namespace App\Http\Controllers\Sarana\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Sarana\Master\KategoriApi;
use App\ApiService\Sarana\Master\JenisApi;


class KategoriBarangController extends Controller
{
    private $kategoriApi;
    private $jenisApi;

    public function __construct(){
        $this->kategoriApi = new KategoriApi();
         $this->jenisApi = new JenisApi();
    }

    public function index(Request $request)
    {
        $data = $this->kategoriApi->get_by_sekolah();
        $data = $data["body"]["data"];

        $jenis = $this->jenisApi->get_by_sekolah();
        $jenis = $jenis["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="modalEditKategori('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="deleteKategori('.$row['id'].')" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-2" aria-hidden="true"></i>
                    </a>
                    <a onclick="modalDetailKategori('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.master.kategori',compact("jenis"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $data = array(
            'nama' => $request->nama,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
            'id_jenis' => $request->id_jenis,
        );

        $result = $this->kategoriApi->create(json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        $result = $this->kategoriApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }

    
    public function edit(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id_jenis'=> $request->id_jenis,
            'nama'=> $request->nama,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        );

        $result = $this->kategoriApi->update($id,json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->kategoriApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

}
