<?php

namespace App\Http\Controllers\Sarana\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\ApiService\Sarana\User\UserSaranaApi;
use App\ApiService\Sarana\Peminjaman\PeminjamanApi;

class UserBarangController extends Controller
{
    private $userApi;
    private $peminjamanApi;

    public function __construct(){
        $this->userApi = new UserSaranaApi();
        $this->peminjamanApi = new PeminjamanApi();
    }

    public function index(Request $request)
    {
        $data = $this->userApi->get_by_sekolah();
        $data = $data["body"]["data"];


        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                <div class="input-group">
                    <a onclick="modalEditUser('.$row['id'].')" class="btn btn-success btn-sm mx-1 mt-0">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="modalDetailUser( 2 ,'.$row['id'].')" class="btn btn-info btn-sm mx-1">
                         <i class="fa fa-info mx-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="resetPassUser( 2 ,'.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-key ml-1" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.user.user_sarana');
    }


    public function create(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'first_password' => $request->first_password,
            'tgl_lahir' => $request->tgl_lahir,
            'role' => $request->role,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $fileName = $namaFile . '.'.$ext;
            $basePath = "file/profile/user/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            //dd($data);
            $result = $this->userApi->create_file(json_encode($data));
            //dd($result);
            File::delete($path);
        }else{
            $result = $this->userApi->create(json_encode($data));
            //dd($result);
            
        }

        Session::flash('message',$result["body"]["message"]);
        return redirect()->back();

        // if ($request->file('image') != null) {
        //     $image = $request->file('image');
        //     $mimetype = $image->getMimeType();
        //     $image_base64 = base64_encode(file_get_contents($request->file('image')));
        //     //// Data yang dikirim ke API
        //     $data['file'] = "data:" . $mimetype . ";base64," . $image_base64;
        // } else {
        //     //// Jika Gambar tidak di inputkan Data yang dikirim ke API menggunakan gambar No Image
        //     $data['file'] = Help::no_img_base64();
        // }

        //dd($data);

        //$result = $this->userApi->create(json_encode($data));

        // dd($result);

        // if ($result['code'] == 200) {
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'success',
        //         'status'  => 'berhasil'
        //     ]);
        // } else {
        //     return response()->json([
        //         'message' => $result['body']['message'],
        //         'icon'  => 'error',
        //         'status'  => 'gagal'
        //     ]);
        // }
        
    }

    
    public function detail(Request $request)
    {
        $id = $request->id;
        $result = $this->userApi->get_by_id($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }

    }
 
    public function edit(Request $request)
    {
        $data = array(
        	'id'=> $request->id,
            'username' => $request->username,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'role' => $request->role,
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $fileName = $namaFile . '.'.$ext;
            $basePath = "file/profile/user/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            //dd($data);
            $result = $this->userApi->update_profile_image(json_encode($data));
            //dd($result);
            File::delete($path);
            
        }else{

            $result = $this->userApi->update_profile(json_encode($data));
        }

        Session::flash('message',$result["body"]["message"]);
        return redirect()->back();

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $result = $this->userApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }

    }

    function redirect(Request $request){
        $id = $request->id;
        return route('riwayat-user', $id);
    }

    function history($id,Request $request){
        $id = $id;

        $user = $this->userApi->get_by_id($id);
        $user = $user["body"]["data"];

        $data = $this->peminjamanApi->get_riwayat_user($id);
        $data = $data["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.sarpras.user.riwayat_peminjaman',compact("data","user"));
    }
}
