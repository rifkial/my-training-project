<?php

namespace App\Http\Controllers\Sarana\Barcode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\Barang\BarangApi;
use App\ApiService\Sarana\Master\KategoriApi;
use App\ApiService\Sarana\Master\JenisApi;
use App\ApiService\Sarana\Master\SatuanApi;
use App\ApiService\Sarana\Master\SupllyerApi;
use App\ApiService\Sarana\Master\LokasiApi;
use App\ApiService\Sarana\Item\ItemApi;
use PDF;

class BarcodeController extends Controller
{
    private $barangApi;
    private $itemApi;
	private $kategoriApi;
    private $jenisApi;
    private $satuanApi;
    private $supllyerApi;
    private $lokasiApi;

    function __construct(){
    	$this->itemApi = new ItemApi();
        $this->lokasiApi = new LokasiApi();
    	$this->barangApi = new BarangApi();
    	$this->kategoriApi = new KategoriApi();
        $this->jenisApi = new JenisApi();
        $this->satuanApi = new SatuanApi();
        $this->supllyerApi = new SupllyerApi();
    }

    function index(Request $request){
    	$satuans = $this->satuanApi->get_by_sekolah();
        $satuans = $satuans["body"]["data"];

        $supllyers = $this->supllyerApi->get_by_sekolah();
        $supllyers = $supllyers["body"]["data"];

    	$kategoris = $this->kategoriApi->get_by_sekolah();
        $kategoris = $kategoris["body"]["data"];

        $jeniss = $this->jenisApi->get_by_sekolah();
        $jeniss = $jeniss["body"]["data"];

    	$data = $this->barangApi->get_by_sekolah();
    	$data = $data["body"]["data"];

    	//dd($data);

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailBarang('.$row['id'].')" id="btnDetail" class="btn btn-success btn-sm mt-0">
                         <i class="fa fa-print ml-1 mt-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }
        return view('content.sarpras.barcode.daftar_cetak_barang',compact("jeniss","kategoris","satuans","supllyers"));
    }

    function redirect(Request $request){
        $id = $request->id;
        return route('cetak-item-barang', $id);
    }

     function cetak_item($id,Request $request){
        $id = $id;

        $lokasis = $this->lokasiApi->get_by_sekolah();
        $lokasis = $lokasis["body"]["data"];

        $data = $this->barangApi->get_detail($id);
        $data = $data["body"]["data"];
        $datas = $data["items"];

        //return view('content.sarpras.barcode.pdf_cetak_barang',compact("datas"));
        
        $pdf = PDF::loadView('content.sarpras.barcode.pdf_cetak_barang',compact("datas"));
        return $pdf->stream('barcode_sarana.pdf');

    }


}
