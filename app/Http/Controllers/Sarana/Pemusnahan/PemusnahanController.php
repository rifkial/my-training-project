<?php

namespace App\Http\Controllers\Sarana\Pemusnahan;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use XcS\XcTools;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ApiService\Sarana\Item\ItemApi;
use App\ApiService\Sarana\Pemusnahan\PemusnahanApi;

class PemusnahanController extends Controller
{
    private $itemApi;
	private $pemusnahanApi;

	function __construct(){
    	$this->itemApi = new ItemApi();
    	$this->pemusnahanApi = new PemusnahanApi();
    }

    public function index(Request $request)
    {
    	$items = $this->itemApi->get_by_sekolah();
    	$items = $items["body"]["data"];

    	$data = $this->pemusnahanApi->status_pemusnahan(0);
    	$data = $data["body"]["data"];

        //dd($data);

    	if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">

                    <a onclick="modalEditPemusnahan('.$row['id'].')" class="btn btn-success btn-sm mt-0 mr-1">
                         <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="detailPemusnahan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }

        //dd(session('message'));


        return view('content.sarpras.pemusnahan.pemusnahan',compact("data","items"));
    }

    public function request(Request $request)
    {
        $data = array(
            'id_item' => $request->id_item,
            'keterangan' => $request->keterangan,
            'tgl_pemusnahan' => $request->tgl_pemusnahan,
            'id_sekolah' => session('id_sekolah')
        );

		if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/sarana/pemusnahan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            //dd($data);
            $result = $this->pemusnahanApi->create_file(json_encode($data));
            File::delete($path);
        }else{
            $result = $this->pemusnahanApi->create(json_encode($data));
        }

        //dd($result);

        $message = $result['body']['message'];
        
        Session::flash('message', $message);
        
        return redirect()->back();
    }

    public function detail(Request $request)
    {
        $id = $request->id;

        $result = $this->pemusnahanApi->get_detail($id);

        if ($result['code'] == 200) {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'data' => $result["body"]["data"],
                'icon'  => 'success',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $data = array(
        	'id' => $request->id,
            'id_item' => $request->id_item,
            'keterangan' => $request->keterangan,
            'tgl_pemusnahan' => $request->tgl_pemusnahan,
            'id_sekolah' => session('id_sekolah')
        );

		if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            //$fileName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            //$fileName = $namaFile . '.'.$ext;
            $basePath = "file/sarana/pemusnahan/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $namaFile);
            $path = $basePath . $namaFile;
            $data['path'] = $path;
            //dd($data_pemusnahan);
            $result = $this->pemusnahanApi->update_file(json_encode($data));
            File::delete($path);
            
        }else{
            $result = $this->pemusnahanApi->update(json_encode($data));
        }
        //dd($result);

        $result = $result["body"]["message"];
        //dd($result);

        Session::flash('message', $result);
        
        return redirect()->back();
    }

    public function delete(Request $request){
        $id = $request->id;

        $result = $this->pemusnahanApi->soft_delete($id);

        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function approve(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id' => $request->id,
            'status_pemusnahan' => 1
        );


        $result = $this->pemusnahanApi->action(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function reject(Request $request)
    {
        $id = $request->id;
        $data = array(
            'id' => $request->id,
            'status_pemusnahan' => 2 
        );

        $result = $this->pemusnahanApi->action(json_encode($data));

        if($result["code"] == 200){
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function approved(Request $request){

        $data = $this->pemusnahanApi->status_pemusnahan(1);
        $data = $data["body"]["data"];

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPemusnahan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pemusnahan.pemusnahan_diterima',compact("data"));

    }

    public function riwayat(Request $request){
        $data = $this->pemusnahanApi->get_by_sekolah();
        $data = $data["body"]["data"];

        //dd($data);

        if($request->ajax()){
            $table = datatables()->of($data);
            $table->addIndexColumn();

            $table->editColumn('aksi',function($row){
                return '
                 <div class="input-group">
                    <a onclick="detailPemusnahan('.$row['id'].')" class="btn btn-warning btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            $table -> rawColumns(['aksi']);
            return $table->make(true);
        }


        return view('content.sarpras.pemusnahan.riwayat_pemusnahan',compact("data"));
    }

}
