<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\JadwalApi;
use App\ApiService\Cbt\JenisApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    private $jadwalApi;
    private $jenisApi;
    private $guruApi;

    public function __construct()
    {
        $this->jadwalApi = new JadwalApi();
        $this->jenisApi = new JenisApi();
        $this->guruApi = new GuruApi();
    }

    public function index()
    {
        $jenis = $this->jenisApi->by_sekolah();
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        // dd($jenis);
        $template = session('template');
        if (session('role') == 'admin-cbt') {
            $template = 'default';
        }
        session()->put('title', 'Data Soal');
        return view('content.cbt.jadwal.v_jadwal')->with(['template' => $template, 'jenis' => $jenis]);
    }


    public function add()
    {
        $jenis = $this->jenisApi->by_sekolah();
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-cbt') {
            $template = 'default';
        }
        session()->put('title', 'Buat Jadwal');
        return view('content.cbt.jadwal.v_add_jadwal')->with(['template' => $template, 'jenis' => $jenis, 'guru' => $guru, 'action' => 'add']);
    }

    public function edit()
    {
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $jadwal = $this->jadwalApi->get_by_id(Help::decode($_GET['k']));
        // dd($jadwal);
        $jadwal = $jadwal['body']['data'];
        $jenis = $this->jenisApi->by_sekolah();
        // dd($jenis);
        $jenis = $jenis['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-cbt') {
            $template = 'default';
        }
        return view('content.cbt.jadwal.v_add_jadwal')->with(['template' => $template, 'jenis' => $jenis, 'guru' => $guru, 'action' => 'edit', 'jadwal' => $jadwal]);
    }

    public function store(Request $request)
    {
        $data = array(
            'id_bank' => $request['bank_id'],
            'id_jenis' => $request['jenis_id'],
            'id_guru' => $request['id_guru'],
            'id_ta_sm' => session('id_tahun_ajar'),
            'tgl_mulai' => date('Y-m-d', strtotime($request['tgl_mulai'])),
            'tgl_selesai' => date('Y-m-d', strtotime($request['tgl_selesai'])),
            'durasi_ujian' => $request['durasi_ujian'],
            'pengawas' => implode('###', $request['pengawas']),
            'acak_soal' => !$request['acak_soal'] ? 0 : 1,
            'token' => !$request['token'] ? 0 : 1,
            'acak_opsi' => !$request['acak_opsi'] ? 0 : 1,
            'hasil_tampil' => !$request['hasil_tampil'] ? 0 : 1,
            'reset_login' => !$request['reset_login'] ? 0 : 1,
            'id_sekolah' => session('id_sekolah'),
        );
        // dd($data);
        $result = $this->jadwalApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
