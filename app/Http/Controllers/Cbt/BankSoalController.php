<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\BankSoalApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\MapelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BankSoalController extends Controller
{
    private $bankApi;
    private $mapelApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->bankApi = new BankSoalApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index()
    {
        $soal = $this->bankApi->by_sekolah(session('id_tahun_ajar'));
        // dd($soal);
        $soal = $soal['body']['data'];
        session()->put('title', 'Bank Soal');
        $template = 'default';
        return view('content.cbt.bank.v_bank_soal')->with(['template' => $template, 'soal' => $soal]);
    }

    public function create()
    {
        session()->put('title', 'Buat Bank Soal');
        $template = 'default';
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        return view('content.cbt.bank.v_add_bank_soal')->with(['template' => $template, 'mapel' => $mapel, 'jurusan' => $jurusan]);
    }

    public function store(Request $request)
    {
        $data = array(
            'kode' => $request['kode'],
            'id_mapel' => $request['mapel'],
            'id_kelas' => $request['id_kelas'],
            'id_guru' => implode('###', $request['id_guru']),
            'id_rombel' => implode('###', $request['id_rombel']),
            'id_ta_sm' => session('id_tahun_ajar'),
            // 'nama' => $request['id_kelas'],
            'jml_pilgan' => $request['tampil_pg'],
            'bobot_pilgan' => $request['bobot_pg'],
            'opsi' => $request['opsi'],
            'jml_kompleks' => $request['tampil_komplek'],
            'bobot_kompleks' => $request['bobot_komplek'],
            'jml_jodohkan' => $request['tampil_jodoh'],
            'bobot_jodohkan' => $request['bobot_jodoh'],
            'jml_isian' => $request['tampil_es'],
            'bobot_isian' => $request['bobot_es'],
            'jml_essay' => $request['tampil_uraian'],
            'bobot_essay' => $request['bobot_uraian'],
            'status' => $request['status'],
            'id_sekolah' => session('id_sekolah')
        );

        $result = $this->bankApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail()
    {
        $soal = $this->bankApi->get_by_id(Help::decode($_GET['code']));
        $soal = $soal['body']['data'];
        // dd($soal);
        $template = 'default';
        session()->put('title', 'Detail Soal');
        return view('content.cbt.bank.v_detail_bank_soal')->with(['template' => $template, 'soal' => $soal]);
    }

    public function by_guru(Request $request)
    {
        $bank = $this->bankApi->by_guru($request['id'], session('id_tahun_ajar'));
        $bank = $bank['body']['data'];
        return response()->json($bank);
    }
}
