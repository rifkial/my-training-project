<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    private $jurusanApi;
    private $kelasApi;
    private $rombelApi;
    private $siswaApi;

    public function __construct()
    {
        $this->siswaApi = new KelasSiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
        $this->rombelApi = new RombelApi();
    }

    public function index()
    {
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $kelas = $this->kelasApi->get_by_sekolah();
        $kelas = $kelas['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $template = 'default';
        session()->put('title', 'Data Siswa');
        return view('content.cbt.v_siswa')->with(['template' => $template, 'jurusan' => $jurusan, 'kelas' => $kelas, 'rombel' => $rombel]);
    }

    public function get_siswa(Request $request)
    {
        // dd($request);
        session()->put('request_point_temporary', [
            'id_jurusan' => $request['id_jurusan'],
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['rombel'],
        ]);
        $siswa = $this->data_siswa();

        return response()->json($siswa);
    }

    private function data_siswa()
    {
        $siswa = $this->siswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_point_temporary')['id_jurusan'], session('request_point_temporary')['id_kelas'], session('request_point_temporary')['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        // dd($siswa);
        $html = '';
        if (!empty($siswa)) {
            $no = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr>
                <td class="vertical-middle">
                    ' . $no++ . '
                    <input type="hidden" value="' . $sw['id_siswa'] . '" class="check_siswa" name="id_siswa[]">
                </td>
                <td>
                    <b>' . ucwords($sw['nama']) . '</b>
                    <p class="m-0">NIS. ' . $sw['nis'] . ' NISN. ' . $sw['nisn'] . '</p>
                    <small>Alamat ' . $sw['alamat'] . '</small>
                </td>
                <td class="vertical-middle">' . $sw['rombel'] . '</td>
                <td class="vertical-middle">' . $sw['jurusan'] . '</td>
                <td class="vertical-middle">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info" data-toggle="collapse"
                        data-target="#siswa' . $sw['id'] . '"><i class="fas fa-info-circle"></i></a>';
                if ($sw['status'] == 1) {
                    $html .= '<a href="javascript:void(0)" class="update_status btn btn-sm btn-danger mx-1" data-id="' . $sw['id'] . '" data-status="0"><i class="fas fa-user-times"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)" class="update_status btn btn-sm btn-success mx-1" data-id="' . $sw['id'] . '" data-status="1"><i class="fas fa-lightbulb"></i></a>';
                }

                $html .= '</td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="siswa' . $sw['id'] . '">
                        <table class="table table-striped">
                            <tr>
                                <th colspan="6"><i class="fas fa-info-circle"></i> Detail Info</th>
                            </tr>
                            <tr>
                                <td rowspan="6" colspan="2" class="text-center vertical-middle">
                                    <img src="' . $sw['file'] . '" alt="" height="200">
                                </td>
                                <td></td>
                                <td>Nama</td>
                                <td>' . ucwords($sw['nama']) . '</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>NIS</td>
                                <td>' . $sw['nis'] . '</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>NISN</td>
                                <td>' . $sw['nisn'] . '</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Email</td>
                                <td>' . $sw['email'] . '</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Telepon</td>
                                <td>' . $sw['telepon'] . '</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Jenis Kelamin</td>
                                <td>' . $sw['jenkel'] . '</td>
                            </tr>
                            <tr>
                                <td>Tempat, Tanggal Lahir</td>
                                <td>' . ucfirst($sw['tempat_lahir']) . ', ' . Help::getTanggal($sw['tgl_lahir']) . '</td>
                                <td></td>
                                <td>Agama</td>
                                <td>' . $sw['agama'] . '</td>
                            </tr>
                            <tr>
                                <td>Rombel</td>
                                <td>' . $sw['rombel'] . '</td>
                                <td></td>
                                <td>Jurusan</td>
                                <td>' . $sw['jurusan'] . '</td>
                            </tr>
                            <tr>
                                <td>Nama Wali</td>
                                <td>' . $sw['nama_wali'] . '</td>
                                <td></td>
                                <td>Kontak Wali</td>
                                <td>' . $sw['telp_wali'] . '</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>' . $sw['alamat'] . '</td>
                                <td></td>
                                <td>Published</td>
                                <td>' . Help::getTanggalLengkap($sw['created_at']) . '</td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function update_status(Request $request)
    {
        // dd($request);
        foreach ($request['id_siswa'] as $id) {
            $data = array(
                'id' => $id,
                'status' => $request['status'],
            );
            $update = $this->siswaApi->update_status(json_encode($data));
        }

        if ($update['code'] == 200) {
            $siswa =  $this->data_siswa();
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'siswa' => $siswa,
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
