<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    private $guruApi;

    public function __construct()
    {
        $this->guruApi = new GuruApi();
    }

    public function index()
    {
        $guru = $this->guruApi->get_by_sekolah();
        // dd($guru);
        $guru = $guru['body']['data'];
        $template = 'default';
        session()->put('title', 'Data Guru');
        return view('content.cbt.v_guru')->with(['template' => $template, 'guru' => $guru]);
    }

    private function data_guru($param = null)
    {
        if (isset($param)) {
            $guru = $this->guruApi->filter($param);
        } else {
            $guru = $this->guruApi->get_by_sekolah();
        }
        // dd($guru);
        $guru = $guru['body']['data'];
        $html = '';
        if (!empty($guru)) {
            $no = 1;
            foreach ($guru as $gr) {
                $checked = '';
                if ($gr['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td class="vertical-middle">' . $no++ . '</td>
                <td><b>' . $gr['nama'] . '</b>
                    <p class="m-0">NIP. ' . $gr['nip'] . '</p><small>Alamat.
                        ' . $gr['alamat'] . '</small>
                </td>
                <td class="vertical-middle">' . $gr['email'] . '</td>
                <td class="vertical-middle">' . $gr['telepon'] . '</td>
                <td class="vertical-middle">
                    <label class="switch">
                        <input type="checkbox" ' . $checked . '
                            class="guru_check" data-id="' . $gr['id'] . '">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="vertical-middle text-center">
                    <a href="javascript:void(0)" class="btn btn-sm btn-facebook"
                        data-toggle="collapse"
                        data-target="#guru' . $gr['id'] . '"><i
                            class="fas fa-book"></i></a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-info edit"
                        data-id="' . $gr['id'] . '"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                        data-id="' . $gr['id'] . '"><i
                            class="fas fa-trash"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="guru' . $gr['id'] . '">
                        <button class="btn btn-facebook btn-sm my-3 pull-right"
                            onclick="tambahPelajaran(' . $gr['id'] . ')"><i
                                class="fas fa-plus-circle"></i>
                            Tambah Pelajaran</button>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Mapel</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataPelajaran' . $gr['id'] . '">';
                if (!empty($gr['pelajaran'])) {
                    $nomor = 1;
                    foreach ($gr['pelajaran'] as $pj) {
                        $checked_m = '';
                        if ($pj['status'] == 1) {
                            $checked_m = 'checked';
                        }
                        $html .= '<tr>
                                    <td class="vertical-middle">' . $nomor++ . '</td>
                                    <td class="vertical-middle"> ' . $pj['mapel'] . '</td>
                                    <td><b>' . $pj['rombel'] . '</b> - ' . Help::getKelas($pj['kelas']) . '
                                        <br>
                                        <small>Jurusan. ' . $pj['jurusan'] . '</small>
                                    </td>
                                    <td class="vertical-middle text-center">
                                        <label class="switch">
                                            <input type="checkbox" ' . $checked_m . '
                                                class="mapel_check"  data-id="' . $pj['id'] . '">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="vertical-middle text-center">
                                        <a href="javascript:void(0)"
                                            class="btn btn-sm btn-info editPelajaran"
                                            data-id="' . $pj['id'] . '"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"
                                            class="btn btn-sm btn-danger deletePelajaran"
                                            data-id="' . $pj['id'] . '"><i
                                                class="fas fa-trash"></i></a>
                                    </td>
                                </tr>';
                    }
                } else {
                    $html .= '<tr><td colspan="5" class="text-center">Pelajaran yg terkoneksi belum tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="7" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function store(Request $request)
    {
        $data = array(
            'role' => $request['role'],
            'id' => $request['id'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->akunApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $guru = $this->data_guru();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'guru' => $guru
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->guruApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            $guru = $this->data_guru();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'guru' => $guru
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->guruApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function filter_name(Request $request)
    {
        $key = str_replace('-', ' ', $request['search']);
        $guru = $this->data_guru($key);
        return response()->json($guru);
    }
}
