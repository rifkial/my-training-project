<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Master\MapelApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    private $mapelApi;

    public function __construct()
    {
        $this->mapelApi = new MapelApi();
    }

    public function index()
    {
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        // $template = session('template');
        // if (session('role') == 'admin-absensi') {
        //     $template = 'default';
        // }
        session()->put('title', 'Data Mata Pelajaran');
        return view('content.cbt.v_mapel')->with(['template' => 'default', 'mapel' => $mapel]);
    }

    public function store(Request $request)
    {
        $kode_mapel = $request['kode_mapel'];
        $nama = $request['nama'];
        $kelompok = $request['kelompok'];
        $res = [];
        foreach ($nama as $key => $value) {
            foreach ($kode_mapel as $ke => $val) {
                foreach ($kelompok as $k => $v) {
                    if ($key == $k && $k == $ke) {
                        $res[$key]['kode_mapel'] = $val;
                        $res[$key]['kelompok'] = $v;
                        $res[$key]['nama'] = $value;
                    }
                }
            }
        }

        foreach ($res as $r) {
            $data_insert = array(
                    'id_sekolah' => session('id_sekolah'),
                    'kelompok' => $r['kelompok'],
                    'kode_mapel' => $r['kode_mapel'],
                    'nama' => $r['nama'],
                    'status' => 1,
                );
            $result = $this->mapelApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            $mapel = $this->data_mapel();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'mapel' => $mapel
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'kode_mapel' => $request->kode_mapel[0],
            'nama' => $request->nama[0],
            'ruang' => $request->ruang,
            'kelompok' => $request->kelompok[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        $result = $this->mapelApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $mapel = $this->data_mapel();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'mapel' => $mapel
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->mapelApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $mapel = $this->data_mapel();
            return response()->json([
                'mapel' => $mapel,
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    private function data_mapel()
    {
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        $html = '';
        if (!empty($mapel)) {
            $no = 1;
            foreach ($mapel as $mp) {
                $checked = '';
                if ($mp['status'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td class="vertical-middle">'.$no++.'</td>
                <td>
                    <b>'.ucwords($mp['nama']).' - Kode. '.$mp['kode_mapel'].'</b>
                    <p class="m-0">Kelompok '.$mp['kelompok'].'</p>
                </td>
                <td>
                    <label class="switch">
                        <input type="checkbox" '.$checked.'
                            class="mapel_check" data-id="'.$mp['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info edit"
                        data-id="'.$mp['id'].'"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                        data-id="'.$mp['id'].'"><i class="fas fa-trash"></i></a>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Data saat ini belum tersedia</td>
        </tr>';
        }
        return $html;
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'value' => $request['value'],
        );
        $update = $this->mapelApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
