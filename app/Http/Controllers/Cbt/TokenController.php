<?php

namespace App\Http\Controllers\Cbt;

use App\ApiService\Cbt\TokenApi;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    private $tokenApi;

    public function __construct()
    {
        $this->tokenApi = new TokenApi();
    }

    public function index()
    {
        $token = $this->tokenApi->by_sekolah(session('id_sekolah'));
        // dd($token);
        $token = $token['body']['data'];
        // dd($token);
        $template = session('template');
        if (session('role') == 'admin-cbt') {
            $template = 'default';
        }
        session()->put('title', 'Data Token');
        return view('content.cbt.v_token')->with(['template' => $template, 'token' => $token]);
    }


    public function generate()
    {
        $token = strtoupper(Str::random(5));
        $data = array(
            'token' => $token,
            'id_sekolah' => session('id_sekolah')
        );
        $update = $this->tokenApi->create(json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'token' => $token
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
}
