<?php

namespace App\Http\Controllers\Help;

use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DetailModalController extends Controller
{
    private $industriApi;
    private $siswaApi;
    private $guruApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->siswaApi = new SiswaApi();
        $this->guruApi = new GuruApi();
    }

    public function detail_industri(Request $request)
    {
        $industri = $this->industriApi->get_by_id($request['id']);
        $industri = $industri['body']['data'];
        $html = '
            <div class="row">
                <div class="col-sm-6">
                    <div align="center"> <img alt="User Pic"
                            src="'.$industri['file'].'"
                            id="profile-image1" class="img-responsive">
                    </div>
                    <br>
                </div>
                <div class="col-sm-6">
                    <h4 style="color:#00b1b1;">'.$industri['nama'].'</h4></span>
                    <span>
                        <p>'.$industri['alamat'].'</p>
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr style="margin:5px 0 5px 0;">

            <div class="row">
                <div class="col-sm-5 col-xs-6 tital ">Pimpinan:</div>
                <div class="col-sm-7 col-xs-6 ">'.$industri['pimpinan'].'</div>
            </div>
            <div class="clearfix"></div>
            <div class="bot-border"></div>
            <div class="row">
                <div class="col-sm-5 col-xs-6 tital ">telepon:</div>
                <div class="col-sm-7 col-xs-6 ">'.$industri['telepon'].'</div>
            </div>
            <div class="clearfix"></div>
            <div class="bot-border"></div>
            <div class="row">
                <div class="col-sm-5 col-xs-6 tital ">email:</div>
                <div class="col-sm-7 col-xs-6 ">'.$industri['email'].'</div>
            </div>
            <div class="clearfix"></div>
            <div class="bot-border"></div>
        ';

        return response()->json($html);
    }

    public function detail_siswa(Request $request)
    {
        $siswa = $this->siswaApi->get_by_id($request['id']);
        $siswa = $siswa['body']['data'];
        $html = '
                <div class="row">
                    <div class="col-sm-6">
                        <div class="align-items-center" align="center"> <img alt="User Pic"
                                src="'.$siswa['file'].'"
                                id="profile-image1" class="img-circle img-responsive">
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-6">
                        <h4 style="color:#00b1b1;">'.ucfirst($siswa['nama']).'</h4></span>
                        <span>
                            <p> NISN: '.$siswa['nisn'].'</p>
                        </span>
                    </div>
                </div>
                <div class="custom-scroll-content scrollbar-enabled" style="height: 400px;">
                    <div class="clearfix"></div>
                    <hr style="margin:5px 0 5px 0;">

                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">NIS:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['nis'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">NIK:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['nik'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Telepon:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['telepon'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">email:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['email'].'</div>
                    </div>                                            
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Alamat:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['alamat'].'</div>
                    </div>                                            
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tahun Angkatan:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['tahun_angkatan'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tahun Ijazah:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['th_ijazah'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Kelamin:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['jenkel'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Agama:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['agama'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tempat Tanggal Lahir:</div>
                        <div class="col-sm-7 col-xs-6 ">'.ucwords($siswa['tempat_lahir']).','.Help::getTanggal($siswa['tgl_lahir']).'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Anak ke:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['anak_ke'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Kelas diterima:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['kls_diterima'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tanggal diterima:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['tgl_diterima'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Pekerjaan Ayah:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['pekerjaan_ayah'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Pekerjaan Ibu:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$siswa['pekerjaan_ibu'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                </div>
            ';

        return response()->json($html);
    }

    public function detail_guru(Request $request){
        $guru = $this->guruApi->get_by_id($request['id']);
        $guru = $guru['body']['data'];
        $html = '
                <div class="row">
                    <div class="col-sm-6">
                        <div class="align-items-center" align="center"> <img alt="User Pic"
                                src="'.$guru['file'].'"
                                id="profile-image1" class="img-circle img-responsive">
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-6">
                        <h4 style="color:#00b1b1;">'.ucwords($guru['nama']).'</h4></span>
                        <span>
                            <p>NIP: '.$guru['nip'].'</p>
                        </span>
                    </div>
                </div>
                <div class="custom-scroll-content scrollbar-enabled" style="height: 400px;">
                    <div class="clearfix"></div>
                    <hr style="margin:5px 0 5px 0;">

                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">NIK:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['nik'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">NUPTK:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['nuptk'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Jenkel:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['jenkel'].'</div>
                    </div>                                                
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Email:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['email'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Telepon:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['telepon'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tempat Tanggal Lahir:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['tempat_lahir'].','.Help::getTanggal($guru['tgl_lahir']).'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Agama:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['agama'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Tahun Masuk:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['tahun_masuk'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Alamat:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['alamat'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6 tital ">Informasi Lain:</div>
                        <div class="col-sm-7 col-xs-6 ">'.$guru['informasi_lain'].'</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="bot-border"></div>
                </div>
        ';
        return response()->json($html);
    }
}
