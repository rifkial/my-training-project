<?php

namespace App\Http\Controllers\Induk;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Hashids\Hashids;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    private $kelasSiswaApi;
    private $jurusanApi;
    private $tahunApi;
    private $hash;

    public function __construct()
    {
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->jurusanApi = new JurusanApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Siswa');
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        $routes = "siswa_induk-beranda";
        $search = (isset($_GET["nis"])) ? $_GET["nis"] : "";
        if (isset($_GET["jurusan"]) && $_GET["jurusan"] != '') {
            $jurusans = $this->hash->decode($_GET["jurusan"])[0];
            $data_jr = $_GET['jurusan'];
        }else{
            $jurusans = "";
            $data_jr = "";
        }
        $tahuns = (isset($_GET["tahun"])) ? $_GET["tahun"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->kelasSiswaApi->data_siswa($page, $tahuns, $jurusans, $search);
        // dd($result);
        $siswa = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::dataSiswa($data_jr, $tahuns, $meta, $routes, $search);
        return view('content.admin.induk.v_siswa')->with(['tahun'=> $tahun, 'jurusan' => $jurusan, 'routes' => $routes,
        'pagination' => $pagination, 'siswa' => $siswa, 'search' => $search, 'select_jurusan' => $data_jr, 'tahuns' => $tahuns
        ]);
    }
}
