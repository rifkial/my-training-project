<?php

namespace App\Http\Controllers\Induk;

use App\ApiService\User\TataUsahaApi;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;

class TataUsahaController extends Controller
{
    private $guruApi;

    public function __construct()
    {
        $this->guruApi = new TataUsahaApi();
    }

    public function index(Request $request)
    {
        session()->put('title', 'Data Tata Usaha');
        $routes = "tata_usaha_induk-beranda";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $result = $this->guruApi->get_sekolah_pagination($page, $search);
        // dd($result);
        $tata_usaha = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::filterSimple($meta, $routes, $search);
        return view('content.admin.induk.v_tata_usaha')->with(['routes' => $routes, 'pagination' => $pagination, 'tata_usaha' => $tata_usaha, 'search' => $search]);
    }
}
