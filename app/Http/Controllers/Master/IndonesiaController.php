<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\IndonesiaApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndonesiaController extends Controller
{
    private $indonesiaApi;

    public function __construct()
    {
        $this->indonesiaApi = new IndonesiaApi();
    }

    public function get_kabupaten(Request $request)
    {
        $kabupaten = $this->indonesiaApi->get_kabupaten_by_provinsi($request['id']);
        $kabupaten = $kabupaten['body']['data']['cities'];
        return json_encode($kabupaten);
    }

    public function get_kecamatan($id_kabupaten)
    {
        $kecamatan = $this->indonesiaApi->get_kecamatan_by_kabupaten($id_kabupaten);
        $kecamatan = $kecamatan['body']['data'];
        return json_encode($kecamatan);
    }

    public function get_desa($id_kecamatan)
    {
        $desa = $this->indonesiaApi->get_desa_by_kecamatan($id_kecamatan);
        $desa = $desa['body']['data'];
        return json_encode($desa);
    }

    public function get_detail_desa($id_desa)
    {
        $desa = $this->indonesiaApi->get_detail_desa($id_desa);
        $desa = $desa['body']['data'];
        return json_encode($desa);
    }

    public function edit_kab(Request $request)
    {
        $id_provinsi = $request['id_prov'];
        $id_kabupaten = $request['id_kab'];
        $post  = $this->indonesiaApi->get_kabupaten_by_provinsi($id_provinsi);
        $result = $post['body']['data']['cities'];
        $output = '';
        foreach ($result as $kab) {
            $kab_id = $kab['id'];
            $kab_name = $kab['name'];
            $output .= '<option value="' . $kab_id . '" ' . (($kab_id == $id_kabupaten) ? 'selected="selected"' : "") . '>' . $kab_name . '</option>';
        }
        return $output;
    }

    public function edit_kec(Request $request)
    {
        $id_kabupaten = $request['id_kab'];
        $id_kecamatan = $request['id_kec'];
        $post  = $this->indonesiaApi->get_kecamatan_by_kabupaten($id_kabupaten);
        $result = $post['body']['data']['districts'];
        $output = '';
        foreach ($result as $kec) {
            $kec_id = $kec['id'];
            $kec_name = $kec['name'];
            $output .= '<option value="' . $kec_id . '" ' . (($kec_id == $id_kecamatan) ? 'selected="selected"' : "") . '>' . $kec_name . '</option>';
        }
        return $output;
    }

    public function edit_des(Request $request)
    {
        $id_kecamatan = $request['id_kec'];
        $id_desa = $request['id_des'];
        // dd($id_desa);
        $post  = $this->indonesiaApi->get_desa_by_kecamatan($id_kecamatan);
        $result = $post['body']['data']['villages'];
        $output = '';
        foreach ($result as $des) {
            $selected = '';
            if ($des['id'] == $id_desa) {
                $selected = 'selected';
            }
            $des_name = $des['name'];
            $output .= '<option value="' . $des['id'] . '" ' . $selected . '>' . $des_name . '</option>';
        }
        return $output;
    }
}
