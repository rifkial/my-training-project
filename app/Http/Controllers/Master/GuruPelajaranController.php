<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\MapelApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class GuruPelajaranController extends Controller
{
    private $gurupelajaranaApi;
    private $guruApi;
    private $mapelApi;
    private $jurusanApi;
    private $tahunApi;
    private $roomApi;

    public function __construct()
    {
        $this->gurupelajaranaApi = new GuruPelajaranApi();
        $this->guruApi = new GuruApi();
        $this->mapelApi = new MapelApi();
        $this->jurusanApi = new JurusanApi();
        $this->roomApi = new RoomApi();
        $this->rombelApi = new RombelApi();
        $this->tahunApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Guru Pelajaran');
        $url = $this->gurupelajaranaApi->import(session('id_sekolah'));
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $mapel = $this->mapelApi->get_by_sekolah();
        $mapel = $mapel['body']['data'];
        // $jurusan = $this->jurusanApi->get_by_sekolah();
        // $jurusan = $jurusan['body']['data'];
        // $jurusan = $this->jurusanApi->get_by_sekolah();
        // $jurusan = $jurusan['body']['data'];
        // $rombel = $this->rombelApi->get_by_sekolah();
        // $rombel = $rombel['body']['data'];
        $guru_pelajaran = $this->gurupelajaranaApi->get_by_collection_rombel();
        $guru_pelajaran = $guru_pelajaran['body']['data'];
        $tahun = $this->tahunApi->get_by_sekolah();
        $tahun = $tahun['body']['data'];
        // if ($guru_pelajaran['code'] != 200) {
        //     $pesan = array(
        //         'message' => $guru_pelajaran['body']['message'],
        //         'icon' => 'error'
        //     );
        //     return redirect()->back()->with('error_api', $pesan);
        // }
        // dd($result);
        // if ($request->ajax()) {
        //     $table = datatables()->of($result)
        //         ->addColumn('action', function ($data) {
        //             $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="[' . $data['id_guru'] . ',' . $data['id_mapel'] . ',' . $data['id_kelas'] . ']" data-original-title="Edit" class="edit editData-' . $data['id_guru'] . $data['id_mapel'] . $data['id_kelas'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
        //             $button .= '&nbsp;&nbsp;';
        //             $button .= '<button type="button" name="delete" data-id="[' . $data['id_guru'] . ',' . $data['id_mapel'] . ',' . $data['id_kelas'] . ']" class="delete-' . $data['id_guru'] . $data['id_mapel'] . $data['id_kelas'] . ' btn btn-danger btn-sm" onclick="deleteData([' . $data['id_guru'] . ',' . $data['id_mapel'] . ',' . $data['id_kelas'] . '])"><i class="fa fa-trash"></i> Delete</button>';
        //             return $button;
        //         });
        //     $table->editColumn('rombels', function ($row) {
        //         $rombl = '';
        //         foreach ($row['rombel'] as $rows) {
        //             $rombl .= $rows['rombel'] . '  ';
        //         }
        //         return $rombl;
        //     });
        //     $table->rawColumns(['action', 'rombels']);
        //     $table->addIndexColumn();

        //     return $table->make(true);
        // }
        return view('content.master.guru_pelajaran.v_data_pelajaran')->with(['url' => $url, 'kelas' => $guru_pelajaran, 'tahun' => $tahun, 'guru' => $guru, 'mapel' => $mapel]);
        // return view('content.master.guru_pelajaran.v_data_pelajaran')->with(['url' => $url, 'kelas' => $guru_pelajaran, 'mapel' => $mapel, 'jurusan' => $jurusan, 'rombel' => $rombel,]);
    }

    public function store(Request $request)
    {
        $mapel = $request['mapel'];
        $i = 0;
        foreach ($request['id_guru'] as $guru) {
            $data = array(
                    "id_guru" => $guru,
                    "id_mapel" => $mapel[$i],
                    'id_rombel' => $request['id_rombel'],
                    'id_ta_sm' => $request['id_tahun_ajaran'],
                    'id_sekolah' => session('id_sekolah'),
                );
            $result = $this->gurupelajaranaApi->create(json_encode($data));
            $i++;
        }
        if ($result['code'] == 200) {
            $guru_pelajaran = $this->data_guru_pelajaran($request['id_rombel']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'guru' => $guru_pelajaran
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $post  = $this->gurupelajaranaApi->get_detail_all($request['id_guru'], $request['id_mapel'], $request['id_kelas'], session('id_tahun_ajar'));
        $result = $post['body']['data'][0];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Guru pelajaran berhasil diupdate',
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        }

        $data_edit = $this->gurupelajaranaApi->get_detail_all($request['id_guru'], $request['mapel'], $request['kelas'], session('id_tahun_ajar'));
        $data_edit = $data_edit['body']['data'];
        foreach ($data_edit as $dedit) {
            $delete = $this->gurupelajaranaApi->delete($dedit['id']);
            // dd($delete);
            if ($delete['code'] == 200) {
                $room_delete = $this->roomApi->get_delete_guru_mapel($request['id_guru'], $request['mapel'], $request['kelas']);
                $room_delete = $room_delete['body']['data'];
                foreach ($room_delete as $deleted) {
                    $delete_room = $this->roomApi->delete($deleted['id']);
                }
            }
        }

        // dd($room_delete);
        foreach ($request->rombel as $key) {
            $data_insert = array(
                'id_guru' => $request->id_guru,
                'id_mapel' => $request->mapel,
                'id_kelas' => $request->kelas,
                'id_ta_sm' => session('id_tahun_ajar'),
                'id_sekolah' => session('id_sekolah'),
                'id_rombel' => $key,
            );
            $result = $this->gurupelajaranaApi->create(json_encode($data_insert));
            $room = $this->roomApi->create(json_encode($data_insert));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }


    public function trash(Request $request)
    {
        $delete = $this->gurupelajaranaApi->delete($request['id']);
        if ($delete['code'] == 200) {
            $guru_pelajaran = $this->data_guru_pelajaran($request['id_rombel']);
            return response()->json([
                'guru' => $guru_pelajaran,
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_mapel(Request $request)
    {
        $id_mapel = $request['id_mapel'];
        $data = $this->gurupelajaranaApi->get_by_mapel($id_mapel);
        $result = $data['body']['data'];
        return response()->json($result);
    }

    public function load_mapel_select(Request $request)
    {
        $id_guru = $request['id_guru'];
        $id_mapel = $request['id_mapel'];
        $post  = $this->gurupelajaranaApi->get_by_mapel($id_mapel);
        $result = $post['body']['data'];
        // dd($result);
        $output = '';
        foreach ($result as $brand) {
            $brand_id = $brand['id_guru'];
            $brand_name = $brand['guru'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id_guru) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->gurupelajaranaApi->upload_excel(json_encode($data), session('id_tahun_ajar'));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_guru_pelajaran($id_rombel)
    {
        $guru = $this->gurupelajaranaApi->get_rombel($id_rombel);
        $guru = $guru['body']['data'];
        $html = '';
        if (empty($guru)) {
            $html .= ' <tr><td class="text-center" colspan="6">Data saat ini tidak tersedia</td> </tr>';
        } else {
            $no = 1;
            foreach ($guru as $gp) {
                $html .= '<tr>
                <td>'.$no++.'</td>
                <td>'.$gp['mapel'].'</td>
                <td>'.$gp['nip'].'</td>
                <td>'.$gp['guru'].'</td>
                <td>'.$gp['tahun_ajaran'] . ' ' . $gp['semester'].'</td>
                <td class="text-center">
                    <a href="javascript:void(0)"
                        class="btn btn-info btn-sm edit"
                        data-id="'.$gp['id'].'"
                        data-rombel="'.$id_rombel.'"><i
                            class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)"
                        class="btn btn-danger btn-sm delete"
                        data-id="'.$gp['id'].'"
                        data-rombel="'.$id_rombel.'"><i
                            class="fas fa-trash-alt"></i></a>
                </td>
            </tr>';
            }
        }
        return $html;
    }
}
