<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\MapelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class MapelController extends Controller
{
    private $mapelApi;

    public function __construct()
    {
        $this->mapelApi = new MapelApi();
    }

    public function index(Request $request)
    {
        $url = $this->mapelApi->import();
        // dd($url);
        Session::put('title', 'Data Mapel');
        $mapel = $this->mapelApi->get_by_sekolah();
        if ($mapel['code'] != 200) {
            $pesan = array(
                'message' => $mapel['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $mapel['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.master.v_mapel')->with(['template' => session('template'), 'mapel' => $mapel, 'url' => $url]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "Mata pelajaran berhasil ditambah",
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $kode_mapel = $request['kode_mapel'];
        $nama = $request['nama'];
        $kelompok = $request['kelompok'];
        $res = [];
        foreach ($nama as $key => $value) {
            foreach ($kode_mapel as $ke => $val) {
                foreach ($kelompok as $k => $v) {
                    if ($key == $k && $k == $ke) {
                        $res[$key]['kode_mapel'] = $val;
                        $res[$key]['kelompok'] = $v;
                        $res[$key]['nama'] = $value;
                    }
                }
            }
        }
        // dd($res);

        foreach ($res as $r) {
            $data_insert = array(
                    'id_sekolah' => session('id_sekolah'),
                    'kelompok' => $r['kelompok'],
                    'kode_mapel' => $r['kode_mapel'],
                    'nama' => $r['nama'],
                    'status' => 1,
                );
            $result = $this->mapelApi->create(json_encode($data_insert));
        }

        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $rombel  = $this->mapelApi->get_by_id($request['id']);
        $result = $rombel['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Mata  pelajaran berhasil diupdate',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $data =
        [
            'id' => $request->id,
            'kode_mapel' => $request->kode_mapel[0],
            'nama' => $request->nama[0],
            'ruang' => $request->ruang,
            'kelompok' => $request->kelompok[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        ];

        $result = $this->mapelApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Mata  pelajaran berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }

        $delete = $this->mapelApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->mapelApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $mapel = $this->mapelApi->all_trash();
        $result = $mapel['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->mapelApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->mapelApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
