<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Absensi\AkunApi;
use App\ApiService\Absensi\HadirApi;
use App\ApiService\Absensi\IzinApi;
use App\ApiService\Absensi\LiburApi;
use App\ApiService\Alumni\DashboardApi;
use App\ApiService\Alumni\JawabanSurveyApi;
use App\ApiService\BKK\AgendaApi;
use App\ApiService\BKK\BidangLokerApi;
use App\ApiService\BKK\FeedApi;
use App\ApiService\BKK\IndustriApi;
use App\ApiService\BKK\LokerApi;
use App\ApiService\Kesiswaan\PelanggaranSiswaApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\ConfigApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\IndonesiaApi;
use App\ApiService\Master\JadwalApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\MutasiApi;
use App\ApiService\Master\ProgramApi;
use App\ApiService\Master\ProgramSekolahApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\SettingApi;
use App\ApiService\Master\SliderApi;
use App\ApiService\Master\TemplateApi;
use App\ApiService\Prakerin\SiswaPrakerinApi;
use App\ApiService\Prakerin\TugasApi;
use App\ApiService\User\AlumniApi;
use App\ApiService\User\BKApi;
use App\ApiService\User\GuruApi;
use App\ApiService\User\ProfileApi;
use App\ApiService\User\SiswaApi;
use App\ApiService\User\TataUsahaApi;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SPP\SPPController;
//penjamin mutu
use App\ApiService\PenjaminMutu\DashboardApi as PMDashboardApi;

//perpus
use App\Http\Controllers\Perpus\Home\HomePerpusController;

use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Support\Facades\URL;
use App\Charts\AlumniChart;
use App\Util\Utils;

class DashboardController extends Controller
{
    private $tugasApi;
    private $programApi;
    private $kelassiswaApi;
    private $pelanggaranApi;
    private $jadwalApi;
    private $templateApi;
    private $lokerApi;
    private $indonesiaApi;
    private $profileApi;
    private $bidangLokerApi;
    private $guruApi;
    private $siswaApi;
    private $tataUsahaApi;
    private $alumniApi;
    private $bkApi;
    private $roomApi;
    private $jurusanApi;
    private $prakerinSiswaApi;
    private $rombelApi;
    private $sekolahApi;
    private $feedBkkApi;
    private $hashids;
    private $sppController;
    private $dashboardApi;
    private $katSurveyApi;
    private $mutasiApi;
    private $guruPelajaranApi;
    private $alumniDashboardApi;
    private $masterSliderApi;
    private $masterConfigApi;
    private $perpusController;
    private $hadirApi;
    private $izinApi;
    private $liburApi;
    private $akunAbsensiApi;

    public function __construct()
    {
        $this->bkApi = new BKApi();
        $this->feedBkkApi = new FeedApi();
        $this->mutasiApi = new MutasiApi();
        $this->tugasApi = new TugasApi();
        $this->industriApi = new IndustriApi();
        $this->lokerApi = new LokerApi();
        $this->agendaApi = new AgendaApi();
        $this->alumniApi = new AlumniApi();
        $this->tataUsahaApi = new TataUsahaApi();
        $this->prakerinSiswaApi = new SiswaPrakerinApi();
        $this->siswaApi = new SiswaApi();
        $this->guruApi = new GuruApi();
        $this->programApi = new ProgramApi();
        $this->roomApi = new RoomApi();
        $this->templateApi = new ProgramSekolahApi();
        $this->kelassiswaApi = new KelasSiswaApi();
        $this->profileApi = new ProfileApi();
        $this->jadwalApi = new JadwalApi();
        $this->pelanggaranApi = new PelanggaranSiswaApi();
        $this->alltemplateApi = new TemplateApi();
        $this->lokerApi = new LokerApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->bidangLokerApi = new BidangLokerApi();
        $this->jurusanApi = new JurusanApi();
        $this->rombelApi = new RombelApi();
        $this->sekolahApi = new SekolahApi();
        $this->hashids = new Hashids();
        $this->sppController = new SPPController();
        $this->dashboardApi = new PMDashboardApi();
        $this->katSurveyApi = new JawabanSurveyApi();
        $this->guruPelajaranApi = new GuruPelajaranApi();
        $this->alumniDashboardApi = new DashboardApi();
        $this->masterSliderApi = new SliderApi();
        $this->masterConfigApi = new SettingApi();
        $this->perpusController = new HomePerpusController();
        $this->izinApi = new IzinApi();
        $this->hadirApi = new HadirApi();
        $this->liburApi = new LiburApi();
        $this->akunAbsensiApi = new AkunApi();
    }

    /**
     * Request ke Dashboard untuk semua program dihandle dari sini.
     * Program apa yang dibuka dikenali dari parameter $slug dan dicek ke backend.
     * Tampilan juga ditentukan menurut role user yang terotentikasi.
     */
    public function index(Request $request, $slug)
    {
        // dd(session()->all());
        $slug = $this->programApi->get_by_slug($slug);
        $id_program = $slug['body']['data']['id'];
        $nama_program = $slug['body']['data']['kode'];
        $id_sekolah = session('id_sekolah');
        $template = $this->templateApi->get_by_id_program_sekolah($id_sekolah, $id_program);

        if ($request->segment(2) != "bursa_kerja") {
            if (session('role') != 'admin') {
                if (session('role') != 'admin-prakerin') {
                    $template = $template['body']['data']['kode_template'];
                } else {
                    $template = "default";
                }
                session()->put('template', $template);
            }
        }

        session()->put('id_program', $id_program);
        session()->put('config', $nama_program);
        // dd(session()->all());
        session()->put('title', 'Master');

        /**
         * Menentukan tampilan menurut role usernya admin atau selain admin...
         * Q: `admin` adalah admin sekolah?
         */
        if (session('role') == 'admin') {
            $tahun_lulus = $this->alumniDashboardApi->jumlah_tahun_lulus();
            $tahun_lulus = $tahun_lulus['body']['data'];
            $jurusan = $this->alumniDashboardApi->jumlah_jurusan();
            $jurusan = $jurusan['body']['data'];
            $survey = $this->alumniDashboardApi->survey();
            $survey = $survey['body']['data'];
            $stats_data = $this->alumniDashboardApi->statistik_data();
            $stats_data = $stats_data['body']['data'];
            $top = $this->alumniDashboardApi->statisctic_alumni();
            $top = $top['body']['data'];
            return view('content.dashboard.alumni.admin-alumni')->with([
                'template' => session('template'), 'top' => $top,
                'tahun_lulus' => $tahun_lulus, 'jurusan' => $jurusan, 'survey' => $survey, 'stats_data' => $stats_data
            ]);
        } else {
            if ($request->segment(2) == "raport") {
                if (session('role') != "siswa" && session('role') != "guru"  && session('role') != "walikelas" && session('role') != "admin-raport" && session('role') != "ortu") {
                    $error = array(
                        'message' => 'Role anda tidak terdaftar di sistem!',
                        'icon' => 'error'
                    );
                    return redirect()->back()->with('error_api', $error);
                }
                return view('content.dashboard.learning')->with(['template' => $template]);
            } elseif ($request->segment(2) == "point") {
                if (session('role') == "ortu" || session('role') == "siswa") {
                    session()->put('title', 'Dashboard Konseling');
                    $pelanggaran = $this->pelanggaranApi->get_by_kelas_siswa(session('id_kelas_siswa'));
                    // dd($pelanggaran);
                    $pelanggaran = $pelanggaran['body']['data'];
                    return view('content.dashboard.point.bimbingan_konseling_siswa')->with(['template' => 'default', 'pelanggaran' => $pelanggaran]);
                } else {
                    $jurusan = $this->jurusanApi->get_by_sekolah();
                    $jurusan = $jurusan['body']['data'];
                    $mutasi = $this->mutasiApi->get_by_sekolah();
                    $mutasi = $mutasi['body']['data'];
                    $rombel = $this->rombelApi->get_by_sekolah();
                    $rombel = $rombel['body']['data'];
                    $dashboard = $this->rombelApi->dashboard_gender(session('tahun'));
                    $dashboard = $dashboard['body']['data'];
                    $peringkat = $this->pelanggaranApi->peringkat_pelanggaran_terbanyak();
                    $peringkat = $peringkat['body']['data'];
                    $warning = $this->pelanggaranApi->pelanggaran_warning();
                    // dd($warning);
                    $warning = $warning['body']['data'];
                    $pelanggaran = $this->pelanggaranApi->get_by_pelanggaran_terbanyak();
                    // dd($pelanggaran);
                    $pelanggaran = $pelanggaran['body']['data'];
                    return view('content.dashboard.point.bimbingan_konseling')->with(['template' => 'default', 'dashboard' => $dashboard, 'pelanggaran' => $pelanggaran, 'peringkat' => $peringkat, "warning" => $warning, 'rombel' => $rombel, 'jurusan' => $jurusan, 'mutasi' => $mutasi]);
                }
            } elseif ($request->segment(2) == "bursa_kerja") {
                // dd(session()->all());
                $first_loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
                // dd($first_loker);
                $first_loker = $first_loker['body']['data'];
                $loker = [];
                foreach ($first_loker as $fl) {
                    $loker[] = array(
                        'file_industri' => $fl['file_industri'],
                        'judul' => $fl['judul'],
                        'industri' => $fl['industri'],
                        'diposting' => $fl['diposting'],
                        'id_encode' =>  $this->hashids->encode($fl['id']),
                    );
                }
                $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
                // dd($industri);
                $industri = count($industri['body']['data']);
                $lokers = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
                // dd($lokers);
                $lokers = count($lokers['body']['data']);
                $jumlah = array(
                    'industri' => $industri,
                    'loker' => $lokers,
                );
                $provinsi = $this->indonesiaApi->get_provinsi();
                $provinsi = $provinsi['body']['data'];
                $bidang = $this->bidangLokerApi->sekolah_tanpa_login(session('id_sekolah'));
                // dd($bidang);
                $bidang = $bidang['body']['data'];
                $template = session('template');
                if (session('role') == 'bkk-admin' || session('role') == 'admin') {
                    $template = 'default';
                    $feed = $this->feedBkkApi->admin_all();
                    // dd($feed);
                    $feed = $feed['body']['data'];
                    return view('content.bkk.dashboard.bkk-admin.v_dashboard')->with(['feed' => $feed, 'jumlah' => $jumlah]);
                }
                if (request()->ajax()) {
                    if (session('role') == 'bkk-perusahaan') {
                        $agenda = $this->agendaApi->get_by_industri();
                        $agenda = $agenda['body']['data'];
                    } else {
                        $agenda = [];
                    }
                    return response()->json($agenda);
                }
                return view('content.bkk.dashboard.' . session('role') . '.v_dashboard')->with(['template' => $template, 'loker' => $loker, 'provinsi' => $provinsi, 'bidang' => $bidang, 'jumlah' => $jumlah]);
            } elseif ($request->segment(2) == "e_learning") {
                if (session('role') == 'siswa') {
                    $kelas_today = $this->get_jadwal_hari_ini_card();
                    $profile = $this->profileApi->get_profile();
                    $profile = $profile['body']['data'];
                    return view('content.dashboard.learning_siswa')->with(['template' => $template, 'kelas' => $kelas_today, 'profile' => $profile]);
                } elseif (session('role') == 'guru') {
                    return view('content.dashboard.learning')->with(['template' => $template]);
                } elseif (session("role") == "learning-admin") {
                    $room = $this->roomApi->get_aktif();
                    // dd($room);
                    $room = $room['body']['data'];
                    // dd($room);
                    return view('content.dashboard.learning.admin-learning')->with(['template' => "default", 'room' => $room]);
                } else {
                    return view('content.dashboard.learning')->with(['template' => $template]);
                }

                if (session('role') != "siswa" && session('role') != "guru"  && session('role') != "walikelas" && session('role') != "supervisor" && session('role') == "learning-admin") {
                    $error = array(
                        'message' => 'Role anda tidak terdaftar di sistem!',
                        'icon' => 'error'
                    );
                    return redirect()->back()->with('error_api', $error);
                }
            } elseif ($request->segment(2) == "sistem_pkl") {
                // dd(session()->all());
                session()->put('title', "Halaman Dashboard");
                if (session('role') == 'admin-prakerin') {
                    return view('content.dashboard.prakerin.dashboard_admin')->with(['template' => "default"]);
                } elseif (session('role') == 'guru') {
                    // dd(session()->all());
                    $peserta = $this->prakerinSiswaApi->get_by_pembimbing_sekolah(session('id'));
                    // dd($peserta);
                    $peserta = $peserta['body']['data'];
                    return view('content.dashboard.prakerin.dashboard_guru')->with(['template' => $template, 'peserta' => $peserta]);
                } else {
                    return view('content.dashboard.prakerin.dashboard_' . session('role'))->with(['template' => $template]);
                }
            } elseif ($request->segment(2) == "spp") {
                $data_jurusan = $this->sppController->get_jurusan();
                //dd($data_jurusan);
                return view('content.spp.dashboard', ['data_jurusan' => $data_jurusan]);
            } elseif ($request->segment(2) == "mutu") {
                $dataDashboard = $this->dashboardApi->get_dashboard();
                $dataKategoriDashboard = $this->dashboardApi->get_dashboard_kategori();

                $dataDashboard = $dataDashboard['body']['data'];
                $dataKategoriDashboard = $dataKategoriDashboard['body']['data'];
                return view('content.penjamin_mutu.main', compact('dataKategoriDashboard', 'dataDashboard'));
            } elseif ($request->segment(2) == "sarpras") {
                return view('content.sarpras.dashboard');
            } elseif ($request->segment(2) == "induk") {
                // dd("masuk ke program data induk");
                $dashboard = $this->rombelApi->dashboard_gender(session('tahun'));
                $dashboard = $dashboard['body']['data'];
                return view('content.dashboard.induk.admin-induk')->with(['template' => "default", 'dashboard' => $dashboard]);
            } elseif ($request->segment(2) == 'alumni') {
                if (session('role') == "admin-alumni") {
                    $tahun_lulus = $this->alumniDashboardApi->jumlah_tahun_lulus();
                    $tahun_lulus = $tahun_lulus['body']['data'];
                    $jurusan = $this->alumniDashboardApi->jumlah_jurusan();
                    $jurusan = $jurusan['body']['data'];
                    $survey = $this->alumniDashboardApi->survey();
                    $survey = $survey['body']['data'];
                    $stats_data = $this->alumniDashboardApi->statistik_data();
                    $stats_data = $stats_data['body']['data'];
                    $top = $this->alumniDashboardApi->statisctic_alumni();
                    $top = $top['body']['data'];
                    return view('content.dashboard.alumni.admin-alumni')->with([
                        'template' => session('template'), 'top' => $top,
                        'tahun_lulus' => $tahun_lulus, 'jurusan' => $jurusan, 'survey' => $survey, 'stats_data' => $stats_data
                    ]);
                } else {
                    $kategori = $this->katSurveyApi->cek_kategori();
                    //dd($kategori);
                    $kategori = $kategori['body']['data'];
                    return view('content.dashboard.alumni.alumni-alumni')->with(['template' => session('template'), 'kategori' => $kategori]);
                }
            } elseif ($request->segment(2) == 'kesiswaan') {
                $dashboard = $this->rombelApi->dashboard_gender(session('tahun'));
                $dashboard = $dashboard['body']['data'];
                $jurusan = $this->jurusanApi->get_by_sekolah();
                $jurusan = $jurusan['body']['data'];
                $mutasi = $this->mutasiApi->get_by_sekolah();
                $mutasi = $mutasi['body']['data'];
                // dd($mutasi);
                $rombel = $this->rombelApi->get_by_sekolah();
                // dd($rombel);
                $rombel = $rombel['body']['data'];
                if (session('role') == 'admin-kesiswaan') {
                    $template = 'default';
                }
                if (session('role') == 'walikelas') {
                    $mapel = $this->guruPelajaranApi->get_by_rombel(session('id_rombel'));
                    $mapel = $mapel['body']['data'];
                    $routes = "program/kesiswaan";
                    $search = (isset($_GET["search"])) ? $_GET["search"] : "";
                    $pelanggarans = $this->pelanggaranApi->get_by_id_rombel(session('id_rombel'), $search);
                    $pelanggaran = $pelanggarans['body']['data'];
                    $meta = $pelanggarans['body']['meta'];
                    $pagination = Utils::filterSimple($meta, $routes, $search);
                    return view('content.dashboard.wali_kelas_kesiswaan')->with(['template' => $template, 'mapel' => $mapel, 'pelanggaran' => $pelanggaran, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search]);
                }
                return view('content.dashboard.kesiswaan')->with(['template' => $template, 'dashboard' => $dashboard, 'jurusan' => $jurusan, 'mutasi' => $mutasi, 'rombel' => $rombel]);
            } elseif ($request->segment(2) == 'absensi') {
                if (session('role') == 'admin-absensi') {
                    // dd(date('m'));
                    $libur = $this->liburApi->get_bulan(session('id_sekolah'), date('m'), date('Y'));
                    // dd($libur);
                    $libur = $libur['body']['data'];
                    $izin_siswa = $this->izinApi->statistic_siswa_bulan(session('tahun'), date('m'), date('Y'));
                    // dd($izin_siswa);
                    $izin_siswa = $izin_siswa['body']['data'];
                    $izin_guru = $this->izinApi->get_filter_guru(date('m'), date('Y'));
                    // dd($izin_guru);
                    $izin_guru = $izin_guru['body']['data'];
                    $dashboard = $this->akunAbsensiApi->get_dashboard(session('id_tahun_ajar'));
                    // dd($dashboard);
                    $dashboard = $dashboard['body']['data'];
                    $template = 'default';
                    return view('content.dashboard.absensi.v_admin')->with(['template' => $template, 'libur' => $libur, 'siswa' => $izin_siswa, 'guru' => $izin_guru, 'dashboard' => $dashboard]);
                } else {
                    $informasi = $this->hadirApi->form_sekolah(session('id_sekolah'));
                    // dd($informasi);
                    $informasi = $informasi['body']['data'];
                    if (session('role') == 'siswa') {
                        $detail = $this->hadirApi->my_izin(session('id'), date('Y-m-d'));
                        $profile = $this->kelassiswaApi->get_by_id(session('id_kelas_siswa'));
                    } else {
                        $profile = $this->guruApi->get_by_id(session('id'));
                        $detail = $this->hadirApi->my_izin_guru(session('id'), date('Y-m-d'));
                    }
                    // dd($profile);
                    $detail = $detail['body']['data'];
                    $profile = $profile['body']['data'];
                    if (session('role') == 'siswa') {
                        $riwayat = $this->izinApi->riwayat_siswa_date(session('id'), date('Y-m-d'));
                    } else {
                        $riwayat = $this->izinApi->riwayat_guru_date(session('id'), date('Y-m-d'));
                    }
                    $riwayat = $riwayat['body']['data'];
                    // dd($riwayat);
                    $template = session('template');
                    if (session('role') == 'siswa') {
                        return view('content.dashboard.absensi.v_siswa')->with(['template' => $template, 'profile' => $profile, 'riwayat' => $riwayat, 'informasi' => $informasi, 'detail' => $detail]);
                    } else {
                        return view('content.dashboard.absensi.v_guru')->with(['template' => $template, 'profile' => $profile, 'riwayat' => $riwayat, 'informasi' => $informasi, 'detail' => $detail]);
                    }
                }
            } elseif ($request->segment(2) == 'perpustakaan') {
                return $this->perpusController->index();
            } elseif ($request->segment(2) == 'cbt') {
                return view('content.cbt.v_dashboard')->with(['template' => 'default']);
            } else {
                // Default-nya E-Learning...
                return view('content.dashboard.learning')->with(['template' => $template]);
            }
        }
    }
    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    public function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function program()
    {
        $domain = request()->getHost();
        $domain = str_replace('www.', '', $domain);
        $sekolah = $this->sekolahApi->search_by_domain($domain);
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        if ($sekolah != null) {
            session()->put('id_sekolah', $sekolah['id']);
            $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
            // dd($sekolah);
            $sekolah = $sekolah['body']['data'];
            session()->put('logo_sekolah', $sekolah['file']);
        }
        $program = $this->programApi->get_aktif();
        //dd($program);
        $result = $program['body']['data'];
        $slider = $this->masterSliderApi->sekolah(session('id_sekolah'));
        //dd($slider);
        $slider = $slider['body']['data'];
        $config = $this->masterConfigApi->get_by_sekolah(session('id_sekolah'));
        // dd($config);
        $config = $config['body']['data'];
        return view('content/v_dashboard')->with(['data' => $result, 'slider' => $slider, 'config' => $config, 'sekolah' => $sekolah]);
    }

    public function bkk()
    {
        // dd("tes");
        session()->put('url.intended', URL::previous());
        $program = $this->programApi->get_all();
        // dd($program);
        $result = $program['body']['data'];
        session()->put('title', 'Master');
        return view('content/v_dashboard')->with(['data' => $result]);
    }

    public function admin()
    {
        //dd("admin");
    }

    public function get_jadwal(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $kelas = $this->jadwalApi->get_by_rombel($id_rombel, session('id_tahun_ajar'));
        $result = $kelas['body']['data'];
        if ($id_rombel == 0) {
            $result = [];
        }
        $table = datatables()->of($result);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function get_jadwal_hari_ini(Request $request)
    {
        $id_rombel = $request['id_rombel'];

        Carbon::setLocale('id');
        $hari = Carbon::today()->translatedFormat('l'); // expected hari ini dalam bahasa Indonesia
        $hari = 'Senin';
        $result = $this->jadwalApi->get_by_rombel_hari($id_rombel, $hari, session('id_tahun_ajar'));

        $kelas_hari_ini = $result['body']['data'];
        $table = datatables()->of($kelas_hari_ini);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function get_jadwal_hari_ini_card()
    {
        $id_rombel = session('id_rombel');
        Carbon::setLocale('id');
        $hari = Carbon::today()->translatedFormat('l'); // expected hari ini dalam bahasa Indonesia
        // $hari = 'Senin';
        //dd($hari);
        $result = $this->jadwalApi->get_by_rombel_hari($id_rombel, $hari, session('id_tahun_ajar'));

        $kelas_hari_ini = $result['body']['data'];
        return $kelas_hari_ini;
    }

    public function get_jadwal_guru(Request $request)
    {
        $id_guru = $request['id_guru'];
        $guru = $this->jadwalApi->get_by_guru($id_guru, session('id_tahun_ajar'));
        $result = $guru['body']['data'];
        if ($id_guru == 0) {
            $result = [];
        }
        $table = datatables()->of($result);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_jadwal_guru_hari_ini(Request $request)
    {
        $id_guru = $request['id_guru'];

        Carbon::setLocale('id');
        $hari_ini = Carbon::today()->translatedFormat('l'); // expected hari ini dalam bahasa Indonesia
        // dd($hari_ini);
        $result = $this->jadwalApi->get_by_guru_hari_ini($id_guru, session('id_tahun_ajar'), $hari_ini);

        $jadwal_hari_ini = $result['body']['data'];
        $table = datatables()->of($jadwal_hari_ini);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function siswa_rombel(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $kelas = $this->kelassiswaApi->get_by_rombel($id_rombel, session('tahun'));
        // dd($kelas);
        $result = $kelas['body']['data'];
        if ($id_rombel == 0) {
            $result = [];
        }
        $table = datatables()->of($result);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function count_siswa()
    {
        $kelas = $this->kelassiswaApi->count_siswa();
        // dd($kelas);
        $result = $kelas['body']['data'];
        // dd($result);
        $table = datatables()->of($result);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_dashboard_admin(Request $request)
    {
        $jumlah_guru = count($this->guruApi->get_by_sekolah()['body']['data']);
        $jumlah_siswa = count($this->siswaApi->get_by_sekolah()['body']['data']);
        $tu = count($this->tataUsahaApi->get_by_sekolah()['body']['data']);
        $alumni = count($this->alumniApi->get_by_sekolah()['body']['data']);
        $bk = count($this->bkApi->get_by_sekolah()['body']['data']);
        $jurusan = count($this->jurusanApi->get_by_sekolah()['body']['data']);
        $rombel = count($this->rombelApi->get_by_sekolah()['body']['data']);
        $donut = array(
            [
                'label' => 'jurusan',
                'value' => $jurusan,
            ],
            [
                'label' => 'kelas',
                'value' => $rombel,
            ],
        );
        $bar = array(
            [
                'label' => 'Guru',
                'jumlah' => $jumlah_guru
            ],
            [
                'label' => 'Siswa',
                'jumlah' => $jumlah_siswa
            ],
            [
                'label' => 'Tata Usaha',
                'jumlah' => $tu
            ],
            [
                'label' => 'Alumni',
                'jumlah' => $alumni
            ],
            [
                'label' => 'Bimbingan Konseling',
                'jumlah' => $bk
            ],
        );

        $output = array(
            'donut' => $donut,
            'bar' => $bar,
        );
        return response()->json($output);
    }

    public function pkl_dashboard_guru()
    {
        if (session('role') == 'guru') {
            $siswa = $this->prakerinSiswaApi->get_by_pembimbing_sekolah(session('id'));
        } else {
            $siswa = $this->prakerinSiswaApi->get_by_pembimbing_industri(session('id'));
        }

        $siswa = $siswa['body']['data'];
        $data = [];
        $label = [];
        $value = [];
        foreach ($siswa as $ssw) {
            $tugas = $this->tugasApi->get_by_user_siswa($ssw['id']);
            $tugas = $tugas['body']['data'];
            $label[] = ucwords($ssw['nama']);
            $value[] = count($tugas);
        }
        $data['label'] = $label;
        $data['value'] = $value;
        return response()->json($data);
    }

    public function session_template(Request $request)
    {
        // dd($request);
        session()->put('media_template', $request['template']);
        if (session()->has('media_template') == 'true') {
            return response()->json([
                'success' => "success",
                'message' => "template " . session('media_template') . " berhasil terpasang",
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => "belum ada template yg dipilih",
                'status' => 'gagal'
            ]);
        }
    }

    public function dashboard_alumni()
    {
        $alumni = $this->alumniDashboardApi->get_pekerjaan();
        // dd($alumni);
        $alumni = $alumni['body']['data'];
        // dd($alumni);
        $data = [];
        foreach ($alumni as $al) {
            $data[] = array(
                "label" =>  $al['nama'],
                "y" =>  $al['jumlah']
            );
        }
        return response()->json($data);
    }
}
