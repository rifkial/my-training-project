<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\PesanApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Illuminate\Support\Facades\Session;

class PesanController extends Controller
{
    private $pesanApi;
    private $profilApi;
    private $hashid;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
        $this->pesanApi = new PesanApi();
        $this->profilApi = new ProfileApi();
        $this->hashid = new Hashids();
    }

    public function index()
    {
        $profile = $this->profilApi->get_profile();
        $email = $profile['body']['data']['email'];
        // dd($email);
        return view('content.master.pesan.v_chat_email')->with(['template' => session('template'), 'my_email' => $email]);
    }

    public function sent()
    {
        $send = $this->pesanApi->terkirim();
        $send = $send['body']['data'];
        return view('content.master.pesan.v_send')->with(['template' => session('template'), 'send' => $send]);
    }

    public function new_crate(Request $request)
    {
        $profile = $this->pesanApi->tampil_email($request['email_penerima']);
        if ($profile['code'] != 200) {
            $pesan = array(
                'message' => "Maaf alamat email yang anda masukan, tidak terdaftarr",
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        // dd($profile);
        $profile = $profile['body']['data'];
        $tampil_chat = $this->pesanApi->data_chat($request['email_penerima']);
        $chat = $tampil_chat['body']['data'];
        $file = $this->profilApi->get_profile();
        $file = $file['body']['data']['file'];
        // dd($chat);
        return view('content.master.pesan.v_chat')->with(['template' => session('template'), 'chat' => $chat, 'profile' => $profile, 'file' => $file]);
    }

    public function to_email($email_encode)
    {
        $mail_code = request()->segment(5);
        $email = Crypt::decrypt($mail_code);
        $profile = $this->pesanApi->tampil_email($email);
        // dd($profile);
        $profile = $profile['body']['data'];
        $tampil_chat = $this->pesanApi->data_chat($email);
        $chat = $tampil_chat['body']['data'];
        $file = $this->profilApi->get_profile();
        $file = $file['body']['data']['file'];
        // dd($chat);
        return view('content.master.pesan.v_chat')->with(['template' => session('template'), 'chat' => $chat, 'profile' => $profile, 'file' => $file]);
    }

    public function save_fcm(Request $request)
    {
        $data['fcm_token'] = $request['fcm_token'];
        $result = $this->pesanApi->update_fcm(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function all_email()
    {
        $email = $this->pesanApi->all_email();
        $email = $email['body'];
        return response()->json($email);
        // dd($email);
    }

    public function createChat(Request $request)
    {
        // dd($request);
        $text = '';
        $encr = encrypt($request['email_penerima']);
        // $text = '/program/' . request()->segment(2) . '/pesan/create/'.$encr;
        // dd($text);
        // dd('/program/' . request()->segment(2) . '/pesan/create/', $encr);
        // dd($encr);
        $data = array(
            'email_penerima' => $request['email_penerima'],
            'pesan' => $request['pesan'],
            'role' => session('role'),
        );
        $tampil_chat = $this->pesanApi->tampil_email($request['email_penerima']);
        $nama = $tampil_chat['body']['data']['nama'];
        // dd($this->broadcastMessage($nama, $request['pesan']));
        $this->broadcastMessage($nama, $request['pesan']);
        $result = $this->pesanApi->create(json_encode($data));
        return redirect('/program/' . request()->segment(2) . '/pesan/create/'. $encr);
        // return redirect()->url('program/' . request()->segment(2) . '/pesan/create/e', $encr);
    }

    private function broadcastMessage($senderName, $message)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('New message from : ' . $senderName);
        $notificationBuilder->setBody($message)
            ->setSound('default')
            ->setClickAction(url()->current());
        $dataBuilder = new PayLoadDataBuilder();
        $dataBuilder->addData([
            'sender_name' => $senderName,
            'message' => $message
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        // $tokens = User::all()->pluck('fcm_token')->toArray();
        $tokens = $this->pesanApi->tes();
        $tokens = $tokens['body'];
        // dd($tokens);
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
        // dd($downstreamResponse);

        return $downstreamResponse->numberSuccess();
    }

}
