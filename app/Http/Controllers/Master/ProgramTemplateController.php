<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\ProgramApi;
use App\ApiService\Master\TemplateApi;
use App\ApiService\Master\ProgramSekolahApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ProgramTemplateController extends Controller
{
    private $programSekolahApi;
    private $templateApi;
    private $programApi;

    public function __construct()
    {
        $this->programSekolahApi = new ProgramSekolahApi();
        $this->programApi = new ProgramApi();
        $this->templateApi = new TemplateApi();
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'id_program' => $request['id_program'],
            'id_template' => $request['id_template'],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        $result = $this->programSekolahApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Program');
        $program = $this->programSekolahApi->get_by_sekolah(session('id_sekolah'));
        if ($program['code'] != 200) {
            $pesan = array(
                'message' => $program['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $program['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.setting.v_program_sekolah')->with([]);
    }

    public function edit(Request $request)
    {
        $post  = $this->programSekolahApi->get_by_id($request['id']);
        $result = $post['body']['data'];
        // dd($result);
        $html = '';
        $template = $this->templateApi->get_all();
        // dd($template);
        $template = $template['body']['data'];
        foreach ($template as $tem) {
            if ($result['id_template'] == $tem['id']) {
                $tombol = '<a href="#" data-id="'.$tem['id'].'" onclick="terapkan('.$result['id'].','.$tem['id'].','.$result['id_program'].')" class="btn btn-success btn-block ter'.$result['id'].$tem['id'].$result['id_program'].'">Terpasang</a>';
            } else {
                $tombol = '<a href="#" onclick="terapkan('.$result['id'].','.$tem['id'].','.$result['id_program'].')" class="btn btn-info btn-block ter'.$result['id'].$tem['id'].$result['id_program'].'">Terapkan</a>';
            }

            $html .= '
            <div class="col-lg-4 col-xs-12 text-center mt-4">
            <div class="box">
                <div class="box-title">
                    <h5>'.$tem['nama'].'</h5>
                </div>
                <div class="box-text">
                    <img src="'.$tem['file'].'"
                        alt="" style="height: 143px;">
                </div>
                <div class="box-btn mt-1">
                    '.$tombol.'
                </div>
            </div>
        </div>
            ';
        }
        return response()->json($html);
    }

    public function update_info(Request $request)
    {
        // dd($request);
        $data =
            [
                'id' => $request->id,
                'id_program' => $request->program,
                'id_template' => $request->id_template,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1,
            ];

        $result = $this->programSekolahApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update_program(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update = $this->programSekolahApi->update_status(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update_template(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'id_template' => $request['value'],
        );
        $update = $this->programSekolahApi->update_template(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
