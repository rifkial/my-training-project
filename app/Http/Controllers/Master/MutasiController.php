<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\MutasiApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MutasiController extends Controller
{
    private $mutasiApi;
    private $kelasSiswaApi;

    public function __construct()
    {
        $this->mutasiApi = new MutasiApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
    }

    public function data_siswa(Request $request)
    {
        // dd($request);
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), $request['id_jurusan'], $request['id_kelas'], $request['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        session()->put('request_temporary', [
            'id_jurusan' => $request['id_jurusan'],
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $mutasi = $this->search_mutasi();

        return response()->json($mutasi);
    }

    public function store(Request $request)
    {
        $data = array(
            'id_siswa' => $request['id_siswa'],
            'tgl_mutasi' => $request['tgl_mutasi'],
            'id_rombel' => $request['id_rombel'],
            'keterangan' => $request['keterangan'],
            'tahun_ajaran' => $request['tahun_ajaran'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->mutasiApi->create_out(json_encode($data));
        $mutasi = $this->data_mutasi();
        $search = $this->search_mutasi();
        // dd($mutasi);
        if ($result['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'mutasi' => $mutasi,
                'search' => $search,
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal',
                'mutasi' => $mutasi,
                'search' => $search,
            ]);
        }
    }

    public function store_in(Request $request)
    {
        // dd($request);
        $data = array(
            'nik' => $request->nik,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'anak_ke' => $request->anak_ke,
            'status_keluarga' => $request->status_keluarga,
            'tahun_angkatan' => $request->tahun_angkatan,
            'id_rombel' => $request->kelas_diterima,
            'tgl_diterima' => $request->tanggal_diterima,
            'no_ijazah' => $request->no_ijazah,
            'no_skhun' => $request->no_skhun,
            'tahun_skhun' => $request->tahun_skhun,
            'nama_ayah' => $request->nama_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'nama_ibu' => $request->nama_ibu,
            'nama_wali' => $request->nama_wali,
            'telepon_wali' => $request->telepon_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'alamat_wali' => $request->alamat_wali,
            'tahun_ajaran' => $request->tahun_ajaran,
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );
        // dd($data);

        if ($request->file('image') != null) {
            $image = $request->file('image');
            $mimetype = $image->getMimeType();
            $image_base64 = base64_encode(file_get_contents($request->file('image')));
            $data['image'] = "data:" . $mimetype . ";base64," . $image_base64;
        } else {
            $data['image'] = Help::no_img_base64();
        }
        // dd($data);
        $result = $this->mutasiApi->create_in(json_encode($data));
        // dd($result);
        $mutasi = $this->data_mutasi();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'mutasi' => $mutasi
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'mutasi' => $mutasi
            ]);
        }
    }

    private function data_mutasi()
    {
        $mutasi = $this->mutasiApi->get_by_sekolah();
        $mutasi = $mutasi['body']['data'];
        // dd($mutasi);
        $html = '';
        if (!empty($mutasi)) {
            $nomer = 1;
            foreach ($mutasi as $mt) {
                $keluar = $mt['jenis'] == "keluar" ? $mt['tgl_mutasi'] : '-';
                $masuk = $mt['jenis'] == "masuk" ? $mt['tgl_mutasi'] : '-';
                $html .= '
                <tr>
                    <td class="text-center">'.$nomer++.'</td>
                    <td class="text-center">'.ucwords($mt['nama']).'</td>
                    <td class="text-center">'.$mt['rombel'].'</td>
                    <td class="text-center">'.$keluar.'</td>
                    <td class="text-center">'.$masuk.'</td>
                    <td class="text-center">'.$mt['keterangan'].'</td>
                    <td class="text-center">
                    <a href="#"><i class="material-icons list-icon md-18 text-success">info</i></a>
                    <a href="javascript:void(0)" onclick="editMutasi('.$mt['id'].')"><i class="material-icons list-icon md-18 text-info">edit</i></a>
                    <a href="javacript:void(0)" onclick="deleteMutasi('.$mt['id'].')"><i class="material-icons list-icon md-18 text-danger">backspace</i></a>
                    </td>
                </tr>
                ';
            }
        } else {
            $html .= '
            <tr>
            <td colspan="6">Data mutasi kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    private function search_mutasi()
    {
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), session('request_temporary')['id_jurusan'], session('request_temporary')['id_kelas'], session('request_temporary')['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        $html = '';
        if (!empty($siswa)) {
            $nomer = 1;
            foreach ($siswa as $sw) {
                $html .= '<tr data-toggle="collapse" data-target="#demo'.$sw['id'].'" class="accordion-toggle">
                <td>'.$nomer++.'</td>
                <td>'.$sw['nisn'].'</td>
                <td>'.$sw['nis'].'</td>
                <td>'.ucwords($sw['nama']).'</td>
                <td>'.$sw['rombel'].'</td>
                <td>
                    <i class="fa fa-plus-circle text-info"></i>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo'.$sw['id'].'">
                        <form action="javascript:void(0)" id="formMutasi'.$sw['id'].'" onsubmit="tambahForm(this)" method="post">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Tanggal Mutasi</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Tahun Ajaran</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                    <tr>
                                        <td class="align-middle">
                                        <input type="hidden" name="id_siswa" value="'.$sw['id_siswa'].'">
                                        <input type="hidden" name="id_rombel" value="'.$sw['id_rombel'].'">
                                            <input type="date" name="tgl_mutasi" id="tgl_mutasi"
                                                class="form-control">
                                        </td>
                                        <td class="align-middle">
                                            <input name="keterangan" id="keterangan"
                                                class="form-control">
                                        </td>
                                        <td class="align-middle">
                                            <input type="text" class="form-control"
                                                name="tahun_ajaran" value="'.session('tahun').'">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <button type="submit" class="btn btn-success btn-block">Tambahkan ke
                                                Mutasi</button>
                                        </td>
                                    </tr>
                                </tbody>
                            
                            </table>
                        </form>
                    </div>
                </td>
            </tr>';
            }
        } else {
            $html .= '
            <tr>
                <td colspan="6" class="text-center">Data siswa saat ini kosong</td>
            </tr>
            ';
        }
        return $html;
    }

    public function detail(Request $request)
    {
        $mutasi = $this->mutasiApi->get_by_id($request['id']);
        $mutasi = $mutasi['body']['data'];
        $mutasi['tgl_mutasi'] = date('d-m-Y', strtotime($mutasi['tgl_mutasi']));
        return response()->json($mutasi);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'jenis' => $request['jenis_mutasi'],
            'tgl_mutasi' => $request['tgl_mutasi'],
            'id_rombel' => $request['id_rombel'],
            'tahun_ajaran' => $request['tahun_ajaran'],
            'keterangan' => $request['keterangan'],
        );
        $result = $this->mutasiApi->update_info(json_encode($data));
        // dd($result);
        $mutasi = $this->data_mutasi();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'mutasi' => $mutasi
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal',
                'mutasi' => $mutasi
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->mutasiApi->soft_delete($request['id']);
        $mutasi = $this->data_mutasi();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'mutasi' => $mutasi
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'mutasi' => $mutasi
            ]);
        }
    }
}
