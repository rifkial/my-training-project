<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\JabatanSupervisorApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JabatanSupervisorController extends Controller
{
    private $jabatanApi;

    public function __construct()
    {
        $this->jabatanApi = new JabatanSupervisorApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Jabatan Supervisor');
        $jabatan = $this->jabatanApi->get_by_sekolah();
        if ($jabatan['code'] != 200) {
            $pesan = array(
                'message' => $jabatan['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jabatan['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('content.admin.master.v_jabatan_supervisor')->with([]);
    }


    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Jabatan supervisor berhasil ditambahkan',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $data = array(
            'nama' => $request->nama,
            'id_sekolah' =>  session('id_sekolah'),
            'status' => 1
        );
        $result = $this->jabatanApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_jabatan'];
        $post  = $this->jabatanApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Jabatan supervisor berhasil diupdate',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            ];
        $result = $this->jabatanApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Jabatan supervisor berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }

        $delete = $this->jabatanApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function hard_delete($id)
    {
        $delete = $this->jabatanApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $mapel = $this->jabatanApi->all_trash();
        $result = $mapel['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->jabatanApi->restore($id);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
