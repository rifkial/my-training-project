<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\KelasApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Foreach_;
use XcS\XcTools;

class RombelController extends Controller
{
    private $kelasApi;
    private $rombelApi;
    private $jurusanApi;
    private $kelasSiswaApi;

    public function __construct()
    {
        $this->rombelApi = new RombelApi();
        $this->jurusanApi = new JurusanApi();
        $this->kelasApi = new KelasApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Rombel');
        $url = $this->rombelApi->import(session('id_sekolah'));
        $rombel = $this->rombelApi->get_by_sekolah();
        // dd($rombel);
        if ($rombel['code'] != 200) {
            $pesan = array(
                'message' => $rombel['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $rombel['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        $kelas = $this->kelasApi->get_jurusan_null();
        // dd($kelas);
        $kelas = $kelas['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('checkbox', function ($item) {
                return '<input type="checkbox" id="manual_entry_'.$item['id'].'"  name="rombel[]" class="manual_entry_cb"  value="' . $item['id'] . '" />';
            });
            $table->rawColumns(['action', 'checkbox']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.master.v_rombel')->with(['jurusan' => $jurusan, 'url' => $url, 'kelas' => $kelas]);
    }

    public function store(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Rombel berhasil ditambahkan',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $id_sekolah = session('id_sekolah');
        $id_jurusan = '';
        if ($request['haveJurusan'] == "N") {
            $id_jurusan == null;
        } else {
            $id_jurusan == $request['id_jurusan'];
        }

        foreach ($request['nama'] as $nama) {
            $data =
                [
                    'nama' => $nama,
                    'ruang' => $request->ruang,
                    'id_kelas' => $request->id_kelas,
                    'id_jurusan' => $id_jurusan,
                    'id_sekolah' => $id_sekolah,
                    'status' => 1
                ];
            $result = $this->rombelApi->create(json_encode($data));
        }

        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function deletes(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => "Rombel berhasil dihapus",
                'status' => 'berhasil'
            ]);
        }
        foreach ($request['id_rombel'] as $rombel) {
            $delete = $this->rombelApi->soft_delete($rombel);
        }
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        // dd($request);
        $id = $request['id_rombel'];
        $post  = $this->rombelApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => 'Rombel berhasil diupdate',
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $id_jurusan = '';
        if ($request['haveJurusan'] == "N") {
            $id_jurusan == null;
        } else {
            $id_jurusan == $request['id_jurusan'];
        }
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'ruang' => $request->ruang,
                'id_kelas' => $request->id_kelas,
                'id_jurusan' => $id_jurusan,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            ];
        $result = $this->rombelApi->create(json_encode($data));

        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "success",
                'message' => 'Rombel berhasil dihapus',
                'status' => 'berhasil'
            ]);
        }

        $delete = $this->rombelApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->rombelApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $rombel = $this->rombelApi->all_trash();
        // dd($rombel);
        $result = $rombel['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->rombelApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }


    public function get_kelas(Request $request)
    {
        $id = $request['id_kelas'];
        $kelas = $this->rombelApi->get_by_kelas($id);
        // dd($kelas);
        $kelas = $kelas['body']['data'];
        return json_encode($kelas);
    }

    public function get_kelas_edit(Request $request)
    {
        $id_kelas = $request['id_kelas'];
        $rombel = $request['rombel'];
        $post  = $this->rombelApi->get_by_kelas($id_kelas);
        $result = $post['body']['data'];
        $output = '';
        foreach ($result as $brand) {
            $selected = '';
            $brand_id = $brand['id'];
            $brand_name = $brand['nama'];
            foreach ($rombel as $rom) {
                if ($brand_id == $rom['id']) {
                    $selected = 'selected';
                    break;
                }
            }
            $output .= '<option value="' . $brand_id . '" ' . $selected . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function edit_kelas_select(Request $request)
    {
        $id_kelas = $request['id_kelas'];
        $id_rombel = $request['id_rombel'];
        $post  = $this->rombelApi->get_by_kelas($id_kelas);
        // dd($post);
        $result = $post['body']['data'];
        $output = '';
        foreach ($result as $brand) {
            $brand_id = $brand['id'];
            $brand_name = $brand['nama'];
            $output .= '<option value="' . $brand_id . '" ' . (($brand_id == $id_rombel) ? 'selected="selected"' : "") . '>' . $brand_name . '</option>';
        }
        return $output;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->rombelApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function load_siswa_by_rombel(Request $request)
    {
        $id_rombel = $request['id_rombel'];
        $id_kelas_siswa = $request['id_kelas_siswa'];
        $post  = $this->kelasSiswaApi->get_by_rombel($id_rombel, session('tahun'));
        // dd($post);
        $result = $post['body']['data'];
        $output = '';
        foreach ($result as $brand) {
            $brand_id = $brand['id_siswa'];
            $output .= '<option value="'.$brand_id.'" '.(($brand['id'] == $id_kelas_siswa) ? 'selected="selected"':"").'>'.$brand['nama'].' NISN '.$brand['nisn'].'</option>';
        }
        return $output;
    }

}
