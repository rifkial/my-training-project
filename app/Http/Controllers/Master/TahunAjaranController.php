<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\TahunAjaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TahunAjaranController extends Controller
{
    private $tahunajaranApi;

    public function __construct()
    {
        $this->tahunajaranApi = new TahunAjaranApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Tahun Ajaran');
        
        $tahun_ajaran = $this->tahunajaranApi->get_by_sekolah();
        if ($tahun_ajaran['code'] != 200) {
            $pesan = array(
                'message' => $tahun_ajaran['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $tahun_ajaran['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    if ($data['status_kode'] == 1) {
                        $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    } else {
                        $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-' . $data['id'] . ' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="aktif spinner-' . $data['id'] . ' btn btn-success btn-sm"><i class="fa fa-star"></i> Aktifkan</a>';
                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.admin.setting.v_tahun_ajaran')->with(['data' => $result]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data =
            [
                'nama' => $request->tahun_ajaran ."/".$request->tahun_ajaran2.$request->semester,
                'id_sekolah' => session('id_sekolah'),
            ];

        $result = $this->tahunajaranApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_tahun_ajaran'];
        $post  = $this->tahunajaranApi->get_by_id($id);
        $result = $post['body']['data'];
        // dd($result);
        $exp_tahun = explode('/', $result['tahun_ajaran']);
        $result['start_year'] = $exp_tahun[0];
        $result['end_year'] = $exp_tahun[1];
        return response()->json($result);
    }

    public function update_aktif(Request $request)
    {
        // dd($request);
        $id = $request['id_tahun_ajaran'];
        $result = $this->tahunajaranApi->set_aktif($id);
        // dd(\vars)
        Session::put('id_tahun_ajar', $id);
        Session::put('id_tahun_ajar', $id);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update(Request $request)
    {
        $data =  [
            'id' => $request->id,
            'nama' => $request->tahun_ajaran ."/".$request->tahun_ajaran2.$request->semester,
            'id_sekolah' => session('id_sekolah'),
        ];

        $result = $this->tahunajaranApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function trash(Request $request)
    {
        $id = $request['id_tahun_ajaran'];
        $delete = $this->tahunajaranApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->tahunajaranApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $tahun = $this->tahunajaranApi->all_trash();
        // dd($tahun);
        $result = $tahun['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->tahunajaranApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
