<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\KategoriLombaApi;
use App\ApiService\Master\PrestasiSiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class KategoriLombaController extends Controller
{
    private $katLombaApi;
    private $prestasiApi;

    public function __construct()
    {
        $this->katLombaApi = new KategoriLombaApi();
        $this->prestasiApi = new PrestasiSiswaApi();
    }

    public function index()
    {
        session()->put('title', 'Kategori Lomba');
        $kategori = $this->katLombaApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        return view('content.admin.master.v_lomba')->with(['kategori' => $kategori]);
    }


    public function data_siswa(Request $request)
    {
        // dd($request);
        $siswa = $this->kelasSiswaApi->get_by_tahun_jurusan_kelas_rombel(session('tahun'), $request['id_jurusan'], $request['id_kelas'], $request['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        session()->put('request_temporary', [
            'id_jurusan' => $request['id_jurusan'],
            'id_kelas' => $request['id_kelas'],
            'id_rombel' => $request['id_rombel'],
        ]);
        $kategori = $this->search_mutasi();

        return response()->json($kategori);
    }

    public function store(Request $request)
    {
        $keterangan = $request['keterangan'];
        $i = 0;
        foreach ($request['nama'] as $nama) {
            $data = array(
                "nama" => $nama,
                "keterangan" => $keterangan[$i],
                "id_sekolah" => session('id_sekolah'),
            );
            $i++;
            $result = $this->katLombaApi->create(json_encode($data));
        }
        $kategori = $this->data_kategori();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $kategori
            ]);
        }
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'keterangan' => $request->keterangan[0],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->katLombaApi->update_info(json_encode($data));
        $kategori = $this->data_kategori();
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
                'html' => $kategori
            ]);
        }
    }


    private function data_kategori()
    {
        $kategori = $this->katLombaApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        $html = '';
        if (!empty($kategori)) {
            $nomer = 1;
            foreach ($kategori as $kt) {
                $html .= '
                <tr>
                    <td>' . $nomer++ . '</td>
                    <td>' . ucfirst($kt['nama']) . '</td>
                    <td>' . $kt['keterangan'] . '</td>
                    <td>Aktif</td>
                    <td>
                        <button data-toggle="collapse" data-target="#demo' . $kt['id'] . '"
                            class="btn btn-sm btn-success accordion-toggle"><i
                                class="fas fa-info-circle"></i></button>
                        <a href="javascript:void(0)" data-id="' . $kt['id'] . '"
                            class="btn btn-info btn-sm editKategori"><i class="fas fa-pencil-alt"></i></a>
                        <button class="btn btn-danger btn-sm" onclick="deleteKategori(' . $kt['id'] . ')"><i
                                class="fas fa-trash"></i></button>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="hiddenRow">
                        <div class="accordian-body collapse" id="demo' . $kt['id'] . '">
                            <button class="btn btn-info btn-sm my-3 pull-right"
                                onclick="tambahPeserta(' . $kt['id'] . ')"><i class="fas fa-plus-circle"></i>
                                Tambah Peserta</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr class="bg-success">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Lomba</th>
                                        <th class="text-center">Peserta</th>
                                        <th class="text-center">Kelas</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="dataPeserta' . $kt['id'] . '">';
                if (empty($kt['prestasi'])) {
                    $html .= '<tr><td colspan="5" class="text-center">Data saat ini kosong</td></tr>';
                } else {
                    $no = 1;
                    foreach ($kt['prestasi'] as $pt) {
                        $html .= '<tr>
                                    <td class="text-center">' . $no++ . '</td>
                                    <td class="text-center">' . $pt['nama_lomba'] . '</td>
                                    <td class="text-center">' . $pt['nama_peserta'] . '</td>
                                    <td class="text-center">' . $pt['kelas'] . '</td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-info"><i
                                                class="fas fa-info-circle"></i></button>
                                        <a href="javascript:void(0)" data-id="' . $pt['id'] . '"
                                            class="btn btn-info btn-sm editPrestasi"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <button class="btn btn-danger btn-sm"
                                            onclick="deletePrestasi(' . $pt['id'] . ', ' . $kt['id'] . ')"><i
                                                class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            ';
                    }
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '
            <tr>
            <td colspan="5">Data kategori kosong</td>
            </tr>
            ';
        }
        return $html;
    }



    public function detail(Request $request)
    {
        $detail = $this->katLombaApi->get_by_id($request['id']);
        $detail = $detail['body']['data'];
        // dd($detail);
        return response()->json($detail);
    }

    public function trash(Request $request)
    {
        $delete = $this->katLombaApi->soft_delete($request['id']);
        $kategori = $this->data_kategori();
        // $kategori = $this->data_mutasi();
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'html' => $kategori
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
                'html' => $kategori
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->katLombaApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        $kategori = $this->data_kategori();
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'html' => $kategori
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal',
                'html' => $kategori
            ]);
        }
    }

    public function import_prestasi(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->prestasiApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        $kategori = $this->data_kategori();
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil',
                'html' => $kategori
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal',
                'html' => $kategori
            ]);
        }
    }

    public function sample()
    {
        $download = $this->katLombaApi->import();
        return redirect($download);
    }
}
