<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Learning\KalenderWaliKelasApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class KalenderRombelController extends Controller
{
    private $kalenderApi;
    private $jurusanApi;
    private $profileApi;

    public function __construct()
    {
        $this->kalenderApi = new KalenderWaliKelasApi();
        $this->jurusanApi = new JurusanApi();
        $this->profileApi = new ProfileApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Kalender Rombel');
        if (request()->ajax()) {
            $kalender = $this->kalenderApi->get_by_rombel(session('id_rombel'));
            $data = $kalender['body']['data'];
            return Response::json($data);
        }
        if (session("role") == "admin") {
            return view('content.admin.master.v_kalender');
        } elseif (session("role") == "siswa") {
            $kalendar = $this->kalenderApi->get_by_rombel(session('id_rombel'));
            // dd($kalendar);
            $kalendar = $kalendar['body']['data'];
            return view('content.master.kalender_rombel.v_kalendar_siswa')->with(['template' => session('template'), 'kalender' => $kalendar]);
        } elseif (session("role") == "supervisor") {
            $menu = $this->jurusanApi->menu();
            // dd($menu);
            $menu = $menu['body']['data'];
            // dd($menu);
            return view('content.supervisor.learning.v_kalender')->with(['template' => session('template'), 'menu' => $menu]);
        } else {
            return view('content.master.kalender_rombel.v_kalendar')->with(['template' => session('template')]);
        }
    }

    public function kalender_supervisor(Request $request)
    {
        // dd($request);
        $start = (!empty($request["start"])) ? ($request["start"]) : ('');
        $end = (!empty($request["end"])) ? ($request["end"]) : ('');
        $kalender = $this->kalenderApi->get_acara($start, $end, $request['id_rombel']);
        // dd($kalender);
        $data = $kalender['body']['data'];
        return Response::json($data);
    }

    public function store(Request $request)
    {
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        $insertArr = [
            'title' => $request->title,
            'isi' => $request->isi,
            'start' => $request->start,
            'end' => $request->end,
            'id_rombel' => session('id_rombel'),
            'pembuat' => $profile['nama'],
            'role' => session('role'),
            'id_sekolah' => session('id_sekolah'),
        ];
        // dd($insertArr);
        $result = $this->kalenderApi->create(json_encode($insertArr));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $kalender = $this->kalenderApi->get_by_id($request->id);
        // dd($kalender);
        $kalender = $kalender['body']['data'];

        $updateArr = [
            'id' => $request->id,
            'title' => $request->title,
            'isi' => $request->isi,
            'start' => $request->start,
            'end' => $request->end,
            'id_rombel' => session('id_rombel'),
            'pembuat' => session('username'),
            'role' => session('role'),
            'id_sekolah' => session('id_sekolah'),
        ];
        $result  = $this->kalenderApi->update_info(json_encode($updateArr));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->kalenderApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit($id)
    {
        $kalender = $this->kalenderApi->get_by_id($id);
        $kalender = $kalender['body']['data'];
        $html = '
        <div class="g-mb-15">
            <h5 class="h5 g-color-gray-dark-v1 mb-0">'.ucwords($kalender['pembuat']).'</h5>
            <span class="g-color-gray-dark-v4 g-font-size-12">'.$kalender['dibuat'].'</span>
        </div>
        <h5 class="h5 g-color-gray-dark-v1 mb-0">'.$kalender['judul'].'</h5>
        <p>'.$kalender['isi'].'.</p>

        <ul class="list-inline d-sm-flex my-0">
            <li class="list-inline-item ml-auto">
                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                    <i class="fa fa-reply g-pos-rel g-top-1 g-mr-3"></i>
                    Reply
                </a>
            </li>
        </ul>
        ';
        return response(json_encode($html));
    }
}
