<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\EkstrakurikulerApi;
use App\ApiService\Master\KategoriEkstrakurikulerApi;
use App\ApiService\User\GuruApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class KategoriEkstraController extends Controller
{
    private $kategoriApi;
    private $ekstraApi;
    private $guruApi;

    public function __construct()
    {
        $this->kategoriApi = new KategoriEkstrakurikulerApi();
        $this->ekstraApi = new EkstrakurikulerApi();
        $this->guruApi = new GuruApi();
    }

    public function index()
    {
        session()->put('title', 'Kategori Ekstakurikuler');
        $kategori = $this->kategoriApi->get_by_sekolah();
        // dd($kategori);
        $kategori = $kategori['body']['data'];
        $guru = $this->guruApi->get_by_sekolah();
        $guru = $guru['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.admin.master.v_kategori_ekstrakurikuler')->with(['template'=> $template, 'kategori'=>$kategori, 'url_kategori' => $this->kategoriApi->import(), 'url_ekstra' => $this->ekstraApi->import(session('id_sekolah')), 'guru' => $guru]);
    }

    public function store(Request $request)
    {
        // dd($request);
        foreach ($request['nama'] as $nama) {
            $data = array(
                'nama' => $nama,
                'id_sekolah' => session('id_sekolah'),
                'status' => 1
            );
            $result = $this->kategoriApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function edit(Request $request)
    {
        $kategori  = $this->kategoriApi->get_by_id($request['id']);
        $result = $kategori['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama[0],
            'id_sekolah' => session('id_sekolah'),
            'status' => 1
        );

        $update = $this->kategoriApi->update_info(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->kategoriApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            $kategori = $this->data_kategori();
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    private function data_kategori()
    {
        $kategori = $this->kategoriApi->get_by_sekolah();
        $kategori = $kategori['body']['data'];
        $html = '';
        if (!empty($kategori)) {
            $no = 1;
            foreach ($kategori as $kt) {
                $html .= ' <tr>
                <td>'.$no++.'</td>
                <td>'.$kt['nama'].'</td>
                <td class="text-center">
                    <a href="javaript:void(0)" data-toggle="collapse"
                        data-target="#ekstra'.$kt['id'].'"
                        class="text-purple accordion-toggle"><i
                            class="fas fa-info-circle"></i></a>
                    <a href="javascript:void(0)" class="edit text-info"
                        data-id="'.$kt['id'].'"><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" class="delete text-danger"
                        data-id="'.$kt['id'].'"><i class="fas fa-trash-alt"></i></a>

                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="ekstra'.$kt['id'].'">
                        <button class="btn btn-purple btn-sm my-3 pull-right"
                            onclick="tambahEkstra('.$kt['id'].')"><i
                                class="fas fa-plus-circle"></i>
                            Tambah Ekstra</button>
                        <table class="table table-striped">
                            <thead>
                                <tr class="bg-purple text-white">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Ekstrakurikuler</th>
                                    <th class="text-center">Guru Pendamping / NIP</th>
                                    <th class="text-center">File Absensi</th>
                                    <th class="text-center">File Jurnal</th>
                                    <th class="text-center">File Nilai</th>
                                    <th class="text-center">File Dokumentasi</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="dataEkstra'.$kt['id'].'">';
                if (!empty($kt['ekstra'])) {
                    $nomer = 1;
                    foreach ($kt['ekstra'] as $ekt) {
                        // dd($ekt);
                        $html .= '<tr>
                                        <td>'.$nomer++.'</td>
                                        <td>'.$ekt['nama'].'</td>
                                        <td>'.$ekt['pembimbing'].'</td>
                                        <td class="text-center">';
                        if (!empty($ekt['absen'])) {
                            $html .= '<a href="'.route('download-file', ['absen', Help::encode($ekt['id'])]).'" target="_blank"
                                                    class="text-purple"><i
                                                        class="fas fa-file-download"></i></a>
                                                <a href="javascript:void(0)"
                                                    data-id="'.$ekt['id'].'"
                                                    class="text-danger delFile"  data-based="absen" data-kategori="'.$kt['id'].'"><i
                                                        class="fas fa-trash" ></i></a>
                                                <a href="javascript:void(0)"
                                                    data-id="'.$ekt['id'].'"
                                                    class="text-info edit_file" data-based="absen" data-kategori="'.$kt['id'].'"><i
                                                        class="fas fa-exchange-alt"></i></a>';
                        } else {
                            $html .= ' <a href="javascript:void(0)"
                                            data-id="'.$ekt['id'].'" data-based="absen" data-kategori="'.$kt['id'].'" class="text-success upload_file"><i
                                                class="fas fa-file-upload"></i></a>';
                        }
                        $html .= ' </td><td class="text-center">';
                        if (!empty($ekt['jurnal'])) {
                            $html .= '
                                           <a href="'.route('download-file', ['jurnal', Help::encode($ekt['id'])]).'" target="_blank"
                                                    class="text-purple"><i
                                                        class="fas fa-file-download"></i></a>
                                                <a href="javascript:void(0)" data-id="'.$ekt['id'].'"
                                                    class="text-danger delFile"  data-based="jurnal" data-kategori="'.$kt['id'].'"><i
                                                        class="fas fa-trash"></i></a>
                                                <a href="javascript:void(0)" data-id="'.$ekt['id'].'"class="text-info edit_file" data-based="jurnal" data-kategori="'.$kt['id'].'"><i
                                                        class="fas fa-exchange-alt"></i></a>
                                           ';
                        } else {
                            $html .= '<a href="javascript:void(0)" data-id="'.$ekt['id'].'" data-based="jurnal" data-kategori="'.$kt['id'].'"
                                           class="text-success upload_file"><i class="fas fa-file-upload"></i></a>';
                        }
                        $html .= '</td><td class="text-center">';
                        if (!empty($ekt['nilai'])) {
                            $html .= ' <a href="'.route('download-file', ['nilai', Help::encode($ekt['id'])]).'" target="_blank" class="text-purple"><i class="fas fa-file-download"></i></a>
                                        <a href="javascript:void(0)" data-id="'.$ekt['id'].'"
                                            class="text-danger delFile"  data-based="nilai" data-kategori="'.$kt['id'].'"><i
                                                class="fas fa-trash"></i></a>
                                        <a href="javascript:void(0)"
                                            data-id="'.$ekt['id'].'"class="text-info edit_file" data-based="nilai" data-kategori="'.$kt['id'].'"><i class="fas fa-exchange-alt"></i></a>';
                        } else {
                            $html .= '<a href="javascript:void(0)"
                                            data-id="'.$ekt['id'].'" data-based="nilai" data-kategori="'.$kt['id'].'" class="text-success upload_file"><i
                                                class="fas fa-file-upload"></i></a>';
                        }
                        $html .= ' </td><td class="text-center">';
                        if (!empty($ekt['dokumentasi'])) {
                            $html .= ' <a href="'.route('download-file', ['dokumentasi', Help::encode($ekt['id'])]).'" target="_blank"
                                            class="text-purple"><i class="fas fa-file-download"></i></a>
                                        <a href="javascript:void(0)"
                                            data-id="'.$ekt['id'].'"
                                            class="text-danger delFile"  data-based="dokumentasi" data-kategori="'.$kt['id'].'"><i
                                                class="fas fa-trash"></i></a>
                                        <a href="javascript:void(0)" data-id="'.$ekt['id'].'"
                                            class="text-info edit_file" data-based="dokumentasi" data-kategori="'.$kt['id'].'"><i
                                                class="fas fa-exchange-alt"></i></a>';
                        } else {
                            $html .= '<a href="javascript:void(0)" data-id="'.$ekt['id'].'" data-based="dokumentasi" data-kategori="'.$kt['id'].'"
                                            class="text-success upload_file"><i class="fas fa-file-upload"></i></a>';
                        }
                        $html .= '</td>
                                        <td>
                                            <a href="javascript:void(0)"
                                                data-id="'.$ekt['id'].'"
                                                class="btn btn-sm btn-info editEkstra" data-kategori="'.$kt['id'].'"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                            <a href="javascript:void(0)"
                                                data-id="'.$ekt['id'].'"
                                                class="btn btn-sm btn-danger deleteEkstra" data-kategori="'.$kt['id'].'"><i
                                                    class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>';
                    }
                } else {
                    $html .= ' <tr><td colspan="8" class="text-center">Data saat ini tidak tersedia</td></tr>';
                }
                $html .= '</tbody></table></div></td></tr>';
            }
        } else {
            $html .= '<tr><td class="text-center" colspan="3">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->kategoriApi->upload_excel(json_encode($data));
        File::delete($path);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
