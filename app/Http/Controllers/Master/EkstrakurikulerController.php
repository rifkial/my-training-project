<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\EkstrakurikulerApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class EkstrakurikulerController extends Controller
{
    private $ekstraApi;

    public function __construct()
    {
        $this->ekstraApi = new EkstrakurikulerApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Ekstakurikuler');
        $ekstra = $this->ekstraApi->get_by_sekolah();
        $ekstra = $ekstra['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-kesiswaan' || session('role') == 'admin') {
            $template = 'default';
        }
        return view('content.admin.master.v_ekstrakurikuler')->with(['template'=> $template, 'ekstra'=>$ekstra, 'url' => $this->ekstraApi->import()]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id_kategori_ekstra' => $request['id_kategori'],
            'nama' => $request['ekstra'],
            'pembimbing' => $request['pembimbing'],
            'id_sekolah' => session('id_sekolah'),
        );
        $result = $this->ekstraApi->create(json_encode($data));
        if ($result['code'] == 200) {
            $ekstra = $this->ektra_by_kategori($request['id_kategori']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'ekstra' => $ekstra
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function edit(Request $request)
    {
        $ekstra  = $this->ekstraApi->get_by_id($request['id']);
        $result = $ekstra['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data = array(
            'id' => $request->id,
            'id_kategori_ekstra' => $request['id_kategori'],
            'nama' => $request['ekstra'],
            'pembimbing' => $request['pembimbing'],
            'id_sekolah' => session('id_sekolah'),
        );

        $update = $this->ekstraApi->update_info(json_encode($data));
        if ($update['code'] == 200) {
            $ekstra = $this->ektra_by_kategori($request['id_kategori']);
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'ekstra' => $ekstra
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal',
            ]);
        }
    }

    public function trash(Request $request)
    {
        $delete = $this->ekstraApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            $ekstra = $this->ektra_by_kategori($request['id_kategori']);
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil',
                'ekstra' => $ekstra
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal',
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->ekstraApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $ekstra = $this->ekstraApi->all_trash();
        // dd($ekstra);
        $result = $ekstra['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->ekstraApi->restore($id);
        // dd($restore);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function upload(Request $request)
    {
        // dd($request);
        $base = $request->based;
        $based = 'file_dokumentasi';
        if ($base == 'absen') {
            $based = 'file_absen';
        } elseif ($base == 'jurnal') {
            $based = 'file_jurnal';
        } elseif ($base == 'nilai') {
            $based = 'file_nilai';
        }
        
        $data = array(
            'id' => $request->id,
            'base' => $based,
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bahan_ajar/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->ekstraApi->update_file(json_encode($data));
            File::delete($path);
        } else {
            return response()->json([
                'message' => "Harap masukan file dahulu",
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
        if ($result['code'] == 200) {
            $ekstra = $this->ektra_by_kategori($request['id_kategori']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'ekstra' => $ekstra
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }
    
    public function delete_file(Request $request)
    {
        // dd($request);
        $base = $request->based;
        $based = 'file_dokumentasi';
        if ($base == 'absen') {
            $based = 'file_absen';
        } elseif ($base == 'jurnal') {
            $based = 'file_jurnal';
        } elseif ($base == 'nilai') {
            $based = 'file_nilai';
        }
        
        $data = array(
            'id' => $request->id,
            'base' => $based,
        );        
        $result = $this->ekstraApi->delete_file(json_encode($data));
       
        if ($result['code'] == 200) {
            $ekstra = $this->ektra_by_kategori($request['id_kategori']);
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'ekstra' => $ekstra
            ]);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->ekstraApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'message' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'message' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function ektra_by_kategori($id)
    {
        $ekstra = $this->ekstraApi->get_by_kategori($id);
        // dd($ekstra);
        $ekstra = $ekstra['body']['data'];
        $html = '';
        if (!empty($ekstra)) {
            $no = 1;
            foreach ($ekstra as $ekt) {
                $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$ekt['nama'].'</td>
                    <td>'.$ekt['pembimbing'] . '</td>
                    <td class="text-center">';
                if (!empty($ekt['absen'])) {
                    $html .= '<a href="'.route('download-file', ['absen', Help::encode($ekt['id'])]).'" target="_blank" 
                    class="text-purple"><i
                        class="fas fa-file-download"></i></a>
                    <a href="javascript:void(0)" data-id="'.$ekt['id'].'" class="text-danger  delFile"  data-based="absen" data-kategori="'.$id.'"><i
                            class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)" data-id="'.$ekt['id'].'" class="text-info edit_file" data-based="absen" data-kategori="'.$id.'"><i
                            class="fas fa-exchange-alt"></i></a>';
                } else {
                    $html .= ' <a href="javascript:void(0)" data-id="'.$ekt['id'].'" data-based="absen" data-kategori="'.$id.'" class="text-success upload_file"><i
                            class="fas fa-file-upload"></i></a>';
                }
                $html .= '</td><td class="text-center">';
                if (!empty($ekt['jurnal'])) {
                    $html .= '<a href="'.route('download-file', ['jurnal', Help::encode($ekt['id'])]).'" target="_blank"
                       class="text-purple"><i
                           class="fas fa-file-download"></i></a>
                   <a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       class="text-danger delFile"  data-based="jurnal" data-kategori="'.$id.'"><i
                           class="fas fa-trash"></i></a>
                   <a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       class="text-info edit_file" data-based="jurnal" data-kategori="'.$id.'"><i
                           class="fas fa-exchange-alt"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       data-based="jurnal"
                       class="text-success upload_file" data-kategori="'.$id.'"><i
                           class="fas fa-file-upload"></i></a>';
                }
                $html .= '</td><td class="text-center">';
                if (!empty($ekt['nilai'])) {
                    $html .= '<a href="'.route('download-file', ['nilai', Help::encode($ekt['id'])]).'" target="_blank"
                       class="text-purple"><i
                           class="fas fa-file-download"></i></a>
                   <a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       class="text-danger delFile" data-based="nilai" data-kategori="'.$id.'"><i
                           class="fas fa-trash"></i></a>
                   <a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       class="text-info edit_file" data-based="nilai" data-kategori="'.$id.'"><i
                           class="fas fa-exchange-alt"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)"
                       data-id="'.$ekt['id'].'"
                       data-based="nilai" data-kategori="'.$id.'"
                       class="text-success upload_file"><i
                           class="fas fa-file-upload"></i></a>';
                }
                $html .= '</td><td class="text-center">';
                if (!empty($ekt['dokumentasi'])) {
                    $html .= '<a href="'.route('download-file', ['dokumentasi', Help::encode($ekt['id'])]).'" target="_blank"
                        class="text-purple"><i
                            class="fas fa-file-download"></i></a>
                    <a href="javascript:void(0)"
                        data-id="'.$ekt['id'].'"
                        class="text-danger delFile" data-based="dokumentasi" data-kategori="'.$id.'"><i
                            class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)"
                        data-id="'.$ekt['id'].'"
                        class="text-info edit_file" data-based="dokumentasi" data-kategori="'.$id.'"><i
                            class="fas fa-exchange-alt"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)"
                        data-id="'.$ekt['id'].'"
                        data-based="dokumentasi" data-kategori="'.$id.'"
                        class="text-success upload_file"><i
                            class="fas fa-file-upload"></i></a>';
                }
                $html .='
                    </td>
                    <td>
                        <a href="javascript:void(0)"
                            data-id="'.$ekt['id'].'"
                            class="btn btn-sm btn-info editEkstra" data-kategori="'.$id.'"><i
                                class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)"
                            data-id="'.$ekt['id'].'"
                            class="btn btn-sm btn-danger deleteEkstra" data-kategori="'.$id.'"><i
                                class="fas fa-trash"></i></a>
                    </td>
                </tr>
                    ';
            }
        } else {
            $html .= '<tr><td colspan="8" class="text-center">Data saat initidak tersedia</td></tr>';
        }

        return $html;
    }


    public function download_file($slug, $id)
    {
        // dd($slug);
        $ekstra  = $this->ekstraApi->get_by_id(Help::decode(last(request()->segments())));
        // dd($ekstra);
        $result = $ekstra['body']['data'];
        if($slug == 'absen'){
            $nama_file = explode('/', $result['absen']);
        }elseif($slug == 'jurnal'){
            $nama_file = explode('/', $result['jurnal']);
        }elseif($slug == 'nilai'){
            $nama_file = explode('/', $result['nilai']);
        }else{
            $nama_file = explode('/', $result['dokumentasi']);
        }       
        // dd($nama_file);
        $fileName = end($nama_file);
        if($slug == 'absen'){
            $fileSource = $result['absen'];
        }elseif($slug == 'jurnal'){
            $fileSource = $result['jurnal'];
        }elseif($slug == 'nilai'){
            $fileSource = $result['nilai'];
        }else{
            $fileSource = $result['dokumentasi'];
        }  
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        // dd($getContent);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }
}
