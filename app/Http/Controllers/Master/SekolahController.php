<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\IndonesiaApi;
use App\ApiService\Master\SekolahApi;
use App\Exports\SekolahExport;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class SekolahController extends Controller
{
    private $sekolahApi;
    private $indonesiaApi;
    private $hash;

    public function __construct()
    {
        $this->sekolahApi = new SekolahApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->hash = new Hashids();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Sekolah');
        $sekolah = $this->sekolahApi->get_all();
        // dd($sekolah);
        if ($sekolah['code'] != 200) {
            $pesan = array(
                'message' => $sekolah['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $sekolah['body']['data'];
        //dd($result);
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit btn btn-info"><i class="fa fa-pencil-square-o"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" class="delete-'.$data['id'].' btn btn-danger" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="user_admin btn btn-success"><i class="fa fa-user"></i></button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" data-id="' . $data['id'] . '" class="program_sekolah btn btn-warning"><i class="fa fa-laptop"></i></button>';
                    return $button;
                });
            $table->editColumn('aktifasi', function ($row) {
                if ($row['status'] == 1) {
                    return ' <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" onclick="changeStatus(1,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="Y">ON</a>
                                    <a class="btn btn-primary btn-sm notActive" onclick="changeStatus(0,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="N">OFF</a>
                                </div>
                            </div>';
                } else {
                    return ' <div class="input-group">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm notActive" onclick="changeStatus(1,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="Y">ON</a>
                                    <a class="btn btn-primary btn-sm active" onclick="changeStatus(0,'.$row['id'].')"
                                        data-toggle="pilihan_check'.$row['id'].'" data-title="N">OFF</a>
                                </div>
                            </div>';
                }
            });
            $table->editColumn('status', function ($row) {
                if ($row['status'] == 1) {
                    return '<span class="badge badge-success text-inverse">Aktif</span>';
                } else {
                    return '<span class="badge badge-danger text-inverse">Mati</span>';
                }
            });
            $table->rawColumns(['action', 'aktifasi', 'status']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.master.sekolah.v_tabel_sekolah')->with([]);
    }

    public function store(Request $request)
    {
        $data =
            [
                'nama' => $request->nama,
                'npsn' => $request->npsn,
                'jenjang' => $request->jenjang,
                'status_sekolah' => $request->status_sekolah,
                'sk_pendirian' => $request->sk_pendirian,
                'tgl_sk_pendirian' => $request->tgl_sk_pendirian,
                'email' => $request->email,
                'fax' => $request->fax,
                'website' => $request->website,
                'subdomain' => $request->subdomain,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kode_pos' => $request->kode_pos,
            ];

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->sekolahApi->tambah_program_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->sekolahApi->tambah_program(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit($id)
    {
        // dd('ping');
        $post  = $this->sekolahApi->get_by_id($id);
        $result = $post['body']['data'];
        // dd($result);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama,
                'npsn' => $request->npsn,
                'jenjang' => $request->jenjang,
                'subdomain' => $request->subdomain,
                'status_sekolah' => $request->status_sekolah,
                'sk_pendirian' => $request->sk_pendirian,
                'tgl_sk_pendirian' => $request->tgl_sk_pendirian,
                'email' => $request->email,
                'fax' => $request->fax,
                'website' => $request->website,
                'provinsi' => $request->provinsi,
                'kabupaten' => $request->kabupaten,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kode_pos' => $request->kode_pos,
                'status' => 1
            ];

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->sekolahApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->sekolahApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'status' => $request['value'],
        );
        $update = $this->sekolahApi->update_status(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function filter(Request $request)
    {
        // dd($request);
        $search = array(
            'nama' => $request['nama']
        );
        $sekolah = $this->sekolahApi->filter(json_encode($search));
        // dd($sekolah);
        $sekolah = $sekolah['body']['data'];
        $html = '
            <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                <a class="card" style="position: relative;" href="javascript:void(0)" onclick="addData()">
                    <div class="card-body text-center">
                        <i class="fa fa-plus-circle" style="font-size: 189px; color: #03a9f3;"></i>
                    </div>
                </a>
            </div>
        ';
        if (!empty($sekolah)) {
            foreach ($sekolah as $sk) {
                $html .= '
                <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                <div class="card" style="position: relative;">
                    <img src="' . $sk['file'] . '" class="box-shadow mx-auto text-center" alt=""
                        style="width: 90px; height: 90px; margin-top: -45px;">
                    <div style="position: absolute; right: 7px; top: 7px;">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                                data-id="' . $sk['id'] . '"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    <div class="card-body text-center">
                        <h3 class="card-title pt-1">' . $sk['nama'] . '</h3>
                        <p class="card-text text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip.</p>
                        <a class="text-sm text-uppercase font-weight-bold" href="/super_admin/master/sekolah/' . $this->hash->encode($sk['id']) . '">Lihat Selengkapnya&nbsp;<i
                                class="fe-icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
                ';
            }
        }
        return response()->json($html);
    }

    public function delete($id)
    {
        $delete = $this->sekolahApi->soft_delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
