<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\JurusanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class JurusanController extends Controller
{
    private $jurusanApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
        $this->jurusanApi = new JurusanApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Jurusan');
        $url = $this->jurusanApi->import();
        $jurusan = $this->jurusanApi->get_by_sekolah();
        if ($jurusan['code'] != 200) {
            $pesan = array(
                'message' => $jurusan['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }
        $result = $jurusan['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
            $table->editColumn('tombol', function ($row) {
                return '<a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-sm btn-success data" data-id="' . $row['id'] . '"><i class="fa fa-bolt"></i> Pintasan Data</a>';
            });
            $table->rawColumns(['action', 'tombol']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.master.v_jurusan')->with(['url' => $url]);
    }

    public function store(Request $request)
    {
        $id_sekolah = session('id_sekolah');
        if (session('demo')) {
            return response()->json([
                'success' => "Jurusan Berhasil ditambahkan",
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            //cek invalid foreach
            /*foreach ( $request['nama'] as $nama) {
                $data =array(
                    'nama' => $nama,
                    'id_sekolah' => $id_sekolah,
                    'status' => 1
                );
                $jurusan = $this->jurusanApi->create(json_encode($data));
            }*/

            //validate
            $request->validate(['nama'=>'required']);

            foreach ( (array) $request['nama'] as $nama) {
                $data =array(
                    'nama' => $nama,
                    'id_sekolah' => $id_sekolah,
                    'status' => 1
                );
                $jurusan = $this->jurusanApi->create(json_encode($data));
            }
        }
        if ($jurusan['code'] == 200) {
            return response()->json([
                'success' => $jurusan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $jurusan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_jurusan'];
        $post  = $this->jurusanApi->get_by_id($id);
        $result = $post['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if (session('demo')) {
            return response()->json([
                'success' => "Jurusan Berhasil diupdate",
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $id_sekolah = session('id_sekolah');
        $data =
            [
                'id' => $request->id,
                'nama' => $request->nama[0],
                'id_sekolah' => $id_sekolah,
                'status' => 1
            ];

        $jurusan = $this->jurusanApi->create(json_encode($data));
        // dd($jurusan);
        if ($jurusan['code'] == 200) {
            return response()->json([
                'success' => $jurusan['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $jurusan['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function trash($id)
    {
        $delete = $this->jurusanApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->jurusanApi->delete($id);
        //dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->jurusanApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->jurusanApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->jurusanApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
