<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\KelasApi;
use App\ApiService\Master\JurusanApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Foreach_;
use XcS\XcTools;

class KelasController extends Controller
{
    private $kelasApi;
    private $jurusanApi;

    public function __construct()
    {
        $this->kelasApi = new KelasApi();
        $this->jurusanApi = new JurusanApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Tingkat');
        $url = $this->kelasApi->import(session('id_sekolah'));
        $kelas = $this->kelasApi->get_by_sekolah();
        // dd($kelas);
        if($kelas['code'] != 200){
            $pesan = array(
                'message' => $kelas['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('error_api', $pesan);
        }

        $result = $kelas['body']['data'];
        $jurusan = $this->jurusanApi->get_by_sekolah();
        $jurusan = $jurusan['body']['data'];
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.admin.master.v_kelas')->with(['jurusan' => $jurusan, 'url' => $url]);
    }


    public function store(Request $request)
    {
        if(session('demo')){
            return response()->json([
                'success' => "data kelas berhasil ditambahkan",
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }

        $id_sekolah = session('id_sekolah');
        foreach ($request['nama'] as $nama) {
            $data = array(
                'id_jurusan' => $request->id_jurusan,
                'nama' => $nama,
                'id_sekolah' => $id_sekolah,
                'status' => 1
            );
            $result = $this->kelasApi->create(json_encode($data));
        }

        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }       
    }

    public function edit(Request $request)
    {
        $id = $request['id_kelas'];
        $kelas  = $this->kelasApi->get_by_id($id);
        $result = $kelas['body']['data'];
        return response()->json($result);
    }

    public function update(Request $request)
    {
        if(session('demo')){
            return response()->json([
                'success' => "data kelas berhasil diupdate",
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        }
        $id_sekolah = session('id_sekolah');
        $data = array(
            'id' => $request->id,
            'id_jurusan' => $request->id_jurusan,
            'nama' => $request->nama[0],
            'id_sekolah' => $id_sekolah,
            'status' => 1
        );
        $result = $this->kelasApi->create(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }


    }

    public function delete($id)
    {
        if(session('demo')){
            return response()->json([
                'success' => "success",
                'message' => "Kelas berhasil dihapus",
                'status' => 'berhasil'
            ]);
        }

        $delete = $this->kelasApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }

    }

    public function data_trash()
    {
        $jurusan = $this->kelasApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-'.$data['id'].'" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-'.$data['id'].'" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function get_by_jurusan(Request $request)
    {

        //$kelas = $this->kelasApi->get_by_jurusan($request['id_jurusan']);

        // dd($request);
        $kelas = $this->kelasApi->get_by_jurusan(Help::decode($request['id']));
        // dd($kelas);

        $kelas = $kelas['body']['data'];
        return json_encode($kelas);
    }

    public function get_jurusan_kosong(Request $request)
    {
        // dd("tes");
        $kelas = $this->kelasApi->get_jurusan_null();
        // dd($kelas);
        $kelas = $kelas['body']['data'];
        return json_encode($kelas);
    }

    public function get_jurusan(Request $request)
    {
        // dd($request);
        $id = $request['id_jurusan'];
        // return $id;
        $kelas = $this->kelasApi->get_by_jurusan($id);
        $kelas = $kelas['body']['data'];
        return json_encode($kelas);
    }

    public function load_jurusan(Request $request)
    {
        $id_jurusan = $request['id_jurusan'];
        $id_kelas = $request['id_kelas'];
        if ($request['id_jurusan'] != 0) {
            $post  = $this->kelasApi->get_by_jurusan($id_jurusan);
        }else{
            $post  = $this->kelasApi->get_jurusan_null();
        }
            $result = $post['body']['data'];
            $output = '';
            foreach ($result as $brand) {
                $brand_id = $brand['id'];
                $brand_name = $brand['nama_romawi'];
                $output .= '<option value="'.$brand_id.'" '.(($brand_id == $id_kelas) ? 'selected="selected"':"").'>'.$brand_name.'</option>';
            }
        return $output;
    }

    public function restore($id)
    {
        $restore = $this->kelasApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        }else{
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function hard_delete($id)
    {
        // dd($id);
        $delete = $this->kelasApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        }else{
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->kelasApi->upload_excel(json_encode($data));
        // dd($result);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
