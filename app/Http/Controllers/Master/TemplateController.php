<?php

namespace App\Http\Controllers\Master;

use App\ApiService\Master\TemplateApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use XcS\XcTools;

class TemplateController extends Controller
{
    private $templateApi;

    public function __construct()
    {
        $this->templateApi = new TemplateApi();
    }

    public function index(Request $request)
    {
        Session::put('title', 'Data Template');
        $template = $this->templateApi->get_all();
        // dd($template);
        $result = $template['body']['data'];
        $template = session('template');
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    // $button .= '&nbsp;&nbsp;';
                    // $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                    return $button;
                });
                $table->editColumn('gambar', function ($row) {
                    return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
                });
            $table->rawColumns(['action','gambar']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        return view('content.admin.master.v_template')->with([]);
    }

    public function edit(Request $request)
    {
        $template  = $this->templateApi->get_by_id($request['id']);
        $result = $template['body']['data'];
        $ex = explode('/', $result['file']);
        $result['file_edit'] = end($ex);
        return response()->json($result);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'status' => 1
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/image/";
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->templateApi->update_info(json_encode($data));
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->templateApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->templateApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

}
