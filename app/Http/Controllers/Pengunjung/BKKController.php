<?php

namespace App\Http\Controllers\Pengunjung;

use App\ApiService\BKK\BidangLokerApi;
use App\ApiService\BKK\ConfigApi;
use App\ApiService\BKK\FeedApi;
use App\ApiService\BKK\IndustriApi;
use App\ApiService\BKK\LokerApi;
use App\ApiService\Master\IndonesiaApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\User\AlumniApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class BKKController extends Controller
{
    private $jurusanApi;
    private $alumniApi;
    private $configApi;
    private $lokerApi;
    private $indonesiaApi;
    private $bidangLokerApi;
    private $sekolahApi;
    private $industriApi;
    private $feedApi;
    private $hashids;
    public $perPage = 10;

    public function __construct()
    {
        $this->jurusanApi = new JurusanApi();
        $this->alumniApi = new AlumniApi();
        $this->configApi = new ConfigApi();
        $this->lokerApi = new LokerApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->bidangLokerApi = new BidangLokerApi();
        $this->industriApi = new IndustriApi();
        $this->sekolahApi = new SekolahApi();
        $this->feedApi = new FeedApi();
        $this->hashids = new Hashids();
    }

    public function index()
    {
        if (session('demo')) {
            $first_loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
        } else {
            $first_loker = $this->lokerApi->get_all();
        }
        $loker = $first_loker['body']['data'];

        // dd($loker);
        $provinsi = $this->indonesiaApi->get_provinsi();
        $provinsi = $provinsi['body']['data'];

        if (session('demo')) {
            $bidang = $this->bidangLokerApi->sekolah_tanpa_login(session('id_sekolah'));
        } else {
            $bidang = $this->bidangLokerApi->get_all();
        }

        $bidang = $bidang['body']['data'];
        Session::put('id_sekolah', session('id_sekolah'));
        return view('content.bkk.pengunjung.v_beranda')->with(['provinsi' => $provinsi, 'bidang' => $bidang, 'loker' => $loker]);
    }
    public function home()
    {
        $config = $this->configApi->get_by_sekolah(session('id_sekolah'));
        $config = $config['body']['data'];
        if ($config != null) {
            Session::put('favicon', $config['fav_icon']);
        }
        session()->forget('title');
        return view('content.bkk.pengunjung.v_home')->with(['config' => $config]);
    }

    public function profile()
    {
        return view('content.bkk.pengunjung.v_profil')->with([]);
    }

    public function diskusi()
    {
        Session::put('title', 'Tempat Diskusi');
        Session::put('feed', 'diskusi');
        $feed = $this->feedApi->get_diskusi();
        $feed = $feed['body']['data'];
        $myCollectionObj = collect($feed);
        $feed = $this->paginate($myCollectionObj);
        $feed->withPath('');
        return view('content.bkk.pengunjung.faq.v_diskusi')->with(['feed' => $feed]);
    }

    public function question()
    {
        Session::put('title', 'Faq Question');
        Session::put('feed', 'admin');
        $feed = $this->feedApi->admin();
        $feed = $feed['body']['data'];
        $myCollectionObj = collect($feed);
        $feed = $this->paginate($myCollectionObj);
        $feed->withPath('');
        return view('content.bkk.pengunjung.faq.v_diskusi')->with(['feed' => $feed]);
    }

    public function paginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $this->perPage), $items->count(), $this->perPage, $page, $options);
    }

    public function store_diskusi(Request $request)
    {
        if (session('feed') == 'admin') {
            $status_feed = 2;
            $status = 0;
        } else {
            $status_feed = 1;
            $status = 1;
        }

        $data = array(
            'nama' => $request['nama'],
            'email' => $request['email'],
            'title' => $request['title'],
            'berita' => $request['diskusi'],
            'id_sekolah' => session('id_sekolah'),
            'status_feed' => $status_feed,
            'status' => $status,
        );

        $result = $this->feedApi->create(json_encode($data));

        if ($result['code'] == 200) {
            $message =  [
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ];
        } else {
            $message = [
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ];
        }
        return redirect()->back()->with(['message' => $message]);
    }

    public function perusahaan()
    {
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        if (request()->segment(2) == "more-perusahaan") {
            return view('content.bkk.pengunjung.v_more_perusahaan')->with(['industri' => $industri]);
        } else {
            return view('content.bkk.pengunjung.v_perusahaan')->with(['industri' => $industri]);
        }
    }

    public function detail_loker()
    {
        // dd("tes");
        $loker = $this->lokerApi->get_by_id(Help::decode($_GET['code']));
        // dd($loker);
        $loker = $loker['body']['data'];
        $semua = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
        $semua = $semua['body']['data'];
        return view('content.bkk.pengunjung.v_detail_loker')->with(['loker' => $loker, 'semua' => $semua]);
    }

    public function detailPerusahaan()
    {

        $perusahaan = $this->industriApi->get_by_id(Help::decode($_GET['code']));
        // dd($perusahaan);
        $perusahaan = $perusahaan['body']['data'];
        return view('content.bkk.pengunjung.v_detail_perusahaan')->with(['industri' => $perusahaan]);
    }

    public function lulusan()
    {
        if (session('demo')) {
            $jurusan = $this->jurusanApi->get_by_sekolah_with_id(session('id_sekolah'));
        } else {
            $jurusan = $this->jurusanApi->get_all();
        }
        $jurusan = $jurusan['body']['data'];
        return view('content.bkk.pengunjung.v_lulusan')->with(['jurusan' => $jurusan]);
    }

    public function store_alumni(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request['nama'],
            'nik' => $request['nik'],
            'tempat_lahir' => $request['tempat_lahir'],
            'tgl_lahir' => date('Y-m-d', strtotime($request['tgl_lahir'])),
            'jenkel' => $request['jenkel'],
            'angkatan' => $request['tahun_lulus'],
            'id_jurusan' => $request['id_jurusan'],
            'alamat' => $request['alamat'],
            'ponsel' => $request['no_hp'],
            'email' => $request['email'],
            'pekerjaan' => $request['pekerjaan'],
            'sesuai' => $request['sesuai'] == 0 ? null : $request['sesuai'],
            'bidang_usaha' => $request['bidang_usaha'],
            'universitas' => $request['universitas'],
            'program_studi' => $request['studi'],
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->alumniApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->alumniApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['message' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function search(Request $request)
    {
        $cari = $request->cari;
        $data = array(
            'keyword' => $request->cari,
            'id_sekolah' => session('id_sekolah')
        );
        // dd($data);
        $result = $this->industriApi->search_name_industri(json_encode($data));
        $industri = $result['body']['data'];
        return view('content.bkk.pengunjung.v_perusahaan')->with(['industri' => $industri]);
    }
}
