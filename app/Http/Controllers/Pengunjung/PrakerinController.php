<?php

namespace App\Http\Controllers\Pengunjung;

use App\ApiService\Master\SekolahApi;
use App\ApiService\Prakerin\ConfigApi;
use App\ApiService\Prakerin\IndustriApi;
use App\ApiService\Prakerin\InformasiApi;
use App\ApiService\Prakerin\PedomanApi;
use App\ApiService\Prakerin\SliderApi;
use App\ApiService\User\SiswaApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class PrakerinController extends Controller
{
    private $industriApi;
    private $informasiApi;
    private $sliderApi;
    private $configApi;
    private $pedomanApi;

    public function __construct()
    {
        $this->industriApi = new IndustriApi();
        $this->configApi = new ConfigApi();
        $this->informasiApi = new InformasiApi();
        $this->pedomanApi = new PedomanApi();
        $this->sliderApi = new SliderApi();
    }

    public function index()
    {
        $slider = $this->sliderApi->sekolah(session('id_sekolah'));
        // dd($slider);
        $slider = $slider['body']['data'];
        $config = $this->configApi->get_by_sekolah(session('id_sekolah'));
        // dd($config);
        $config = $config['body']['data'];
        if ($config != null) {
            Session::put('favicon', $config['fav_icon']);
            Session::put('footer', $config['footer']);
        }
        session()->forget('title');
        return view('content.prakerin.pengunjung.v_beranda')->with(['slider' => $slider, 'config' => $config]);
    }

    public function informasi()
    {

        $response = Http::get('https://newsapi.org/v2/top-headlines?country=id&apiKey=623b44fc27e94958bbf094e4f7fbed5f');
        $sports = Http::get('https://newsapi.org/v2/top-headlines?country=id&category=sports&apiKey=623b44fc27e94958bbf094e4f7fbed5f');
        $data = $response->json();
        // dd($data);
        $sport = $sports->json();
        // dd($sport);
        $sport = array_slice($sport['articles'], 0, 4);
        $data1 = array_slice($data['articles'], 0, 2);
        $data2 = array_slice($data['articles'], -2, 4);
        $slider = Http::get('https://newsapi.org/v2/top-headlines?country=id&apiKey=623b44fc27e94958bbf094e4f7fbed5f');
        $slider = $slider->json();
        $slider = array_slice($slider['articles'], 0, 4);
        return view('content.prakerin.pengunjung.v_informasi')->with(['data1' => $data1, 'data2' => $data2, 'sport' => $sport, 'slider' => $slider]);
    }

    public function detail_informasi($slug)
    {
        // dd($slug);
        $title = str_replace('-', ' ', $slug);
        $informasi = $this->informasiApi->get_title(json_encode(['title' => $title]));
        // dd($informasi);
        $informasi = $informasi['body']['data'];
        return view('content.prakerin.pengunjung.v_detail_informasi')->with(['informasi' => $informasi]);

    }

    public function detail_pedoman($slug)
    {
        // dd($slug);

        $nama = str_replace('-', ' ', $slug);
        $pedoman = $this->pedomanApi->get_title(json_encode(['nama' => $nama]));
        // dd($pedoman);
        $pedoman = $pedoman['body']['data'];
        return view('content.prakerin.pengunjung.v_detail_pedoman')->with(['pedoman' => $pedoman]);

    }

    public function industri()
    {
        $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        // dd($industri);
        $industri = $industri['body']['data'];
        return view('content.prakerin.pengunjung.v_industri')->with(['industri' => $industri]);
    }

    public function pedoman()
    {
        return view('content.prakerin.pengunjung.v_pedoman')->with([]);
    }
}
