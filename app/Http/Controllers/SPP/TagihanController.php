<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TagihanController extends Controller
{
    public function index(Request $request)
    {
        $mockDataListTagihan = (new MockDataTagihan())->getData();

        return view('content.spp.tagihan')->with([
            'currentPage' => 'tagihan',
            'dataListTagihan' => $mockDataListTagihan,
        ]);
    }

    public function tagihan(Request $request)
    {
        $mockDataListTagihan = (new MockDataTagihan())->getData();

        $id_tagihan = $request->route('id');

        $dataItem = null;
        foreach ($mockDataListTagihan as $item => $data) {
            if ($data['id'] == $id_tagihan) {
                $dataItem = $data;
                break;
            }
        }

        return view('content.spp.tagihan-detail')->with([
            'currentPage' => 'tagihan',
            'id_tagihan' => $id_tagihan,
            'data' => $dataItem,
        ]);
    }
}

class MockDataTagihan
{

    private $data;

    public function __construct()
    {
        $this->data = [
            [
                "id" => 3,
                "id_siswa" => 1,
                "bulan" => "Juni",
                "besaran_spp" => 350000,
                "status" => "belum",
            ],
            [
                "id" => 2,
                "id_siswa" => 1,
                "bulan" => "Mei",
                "besaran_spp" => 250000,
                "status" => "konfirmasi",
            ],
            [
                "id" => 1,
                "id_siswa" => 1,
                "bulan" => "April",
                "besaran_spp" => 150000,
                "status" => "lunas",
            ],
        ];
    }

    public function getData()
    {
        return $this->data;
    }
}
