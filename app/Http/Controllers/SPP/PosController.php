<?php

namespace App\Http\Controllers\SPP;

use App\Helpers\ApiService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PosController extends Controller
{
    public function index(Request $req)
    {
        $responApi = $this->getPos();
        if ($req->ajax()) {
            return response()
                ->json($responApi)
                ->setStatusCode($responApi['code']);
        }
        return view('content.spp.pos')->with([
            'dataPos' => $responApi['body']['data'],
        ]);
    }

    public function single($id)
    {
        $responApi = $this->getPos($id);
        return response()
            ->json($responApi)
            ->setStatusCode($responApi['code']);
    }

    public function create(Request $request)
    {
        $payload = [
            'id_sekolah' => $request->session()->get('id_sekolah'),
            'kode' => $request->kode,
            'nama' => $request->nama,
        ];
        $responApi = $this->postPos($payload);
        return response()
            ->json($responApi)
            ->setStatusCode($responApi['code']);
    }

    public function edit(Request $request, $id)
    {
        $payload = [
            'id' => $request->id,
            'id_sekolah' => $request->session()->get('id_sekolah'),
            'status' => $request->status,
            'nama' => $request->nama,
            'kode' => $request->kode,
        ];
        $responPut = $this->putPos($payload, $id);
        return response()
            ->json($responPut)
            ->setStatusCode($responPut['code']);
    }

    private function getPos($id = null)
    {
        $endpoint = $id ?
            ('api/data/spp/pos/' . $id)
            : 'api/data/spp/pos/sekolah/profile';

        $responApi = ApiService::request(
            env("API_URL") . $endpoint,
            "GET",
            null
        );
        return $responApi;
    }

    private function postPos($payload)
    {
        $responApi = ApiService::request(
            env("API_URL") . 'api/data/spp/pos',
            "POST",
            json_encode($payload)
        );
        return $responApi;
    }

    private function putPos($payload, $id)
    {
        $responPut = ApiService::request(
            env("API_URL") . 'api/data/spp/pos/' . $id,
            "PUT",
            json_encode($payload)
        );
        return $responPut;
    }
}
