<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ApiService;

class PosPemasukanController extends Controller
{
    public function index()
    {
        $prefixApiSPP = 'api/data/spp';
        $endpointTahunAjaran = env("API_URL") . 'api/data/master/tahun_ajaran/sekolah/tahun_ajaran';
        $responTahunAjaran = ApiService::request($endpointTahunAjaran, "GET", null);
        $tahunDesc = collect($responTahunAjaran['body']['data'])
            ->sortDesc()
            ->map(function ($item) {
                list($tahunAwal,) = explode('/', $item['tahun_ajaran']);
                return [
                    'value' => $tahunAwal,
                    'tahun_ajaran' => $item['tahun_ajaran'],
                ];
            })
            ->values()->all();

        list($tahun_ajaran,) = explode('/', $tahunDesc[0]['tahun_ajaran']);
        $endpoint = env("API_URL") . $prefixApiSPP . '/pos_pemasukan/sekolah/tahun_ajaran/' . $tahun_ajaran;
        $responPosPemasukan = ApiService::request($endpoint, "GET", null);

        // Data pos pemasukan yang ditampilkan secara default adalah Tahun Ajaran terakhir.
        // TA sebelumnya bisa diset/dipilih manual dengan select form atau lainnya.
        return view('content.spp.pos_pemasukan')->with([
            'dataTahunAjaran' => $tahunDesc,
            'tahunAjaranDefault' => $tahunDesc[0]['tahun_ajaran'],
            'dataPosPemasukan' => $responPosPemasukan['body']['data'],
        ]);
    }

    public function show_tahun($tahun)
    {
        $prefixApiSPP = 'api/data/spp';
        $endpoint = env("API_URL") . $prefixApiSPP . '/pos_pemasukan/sekolah/tahun_ajaran/' . $tahun;
        $responPosPemasukan = ApiService::request($endpoint, "GET", null);

        return response()->json($responPosPemasukan);
    }

    public function create(Request $request)
    {
        $payload = json_encode(array(
            // required
            'id_sekolah' => $request->session()->get('id_sekolah'),
            // field yang diisi
            'kode' => $request->kode,
            'id_pos' => $request->id_pos,
            'target' => $request->target,
            'tahun_ajaran' => $request->tahun_ajaran,
        ));

        $prefixApiSPP = 'api/data/spp';
        $responApi = ApiService::request(env("API_URL") . $prefixApiSPP . '/pos_pemasukan', "POST", $payload);

        return response()
            ->json($responApi)
            ->setStatusCode($responApi['code']);
    }

    public function edit(Request $request, $id)
    {
        $payload = json_encode(array(
            // required
            'id' => $id,
            'id_sekolah' => $request->session()->get('id_sekolah'),
            'id_pos' => $request->id_pos,
            'tahun_ajaran' => $request->tahun_ajaran,
            // field yang diedit
            'target' => $request->target,
        ));

        $prefixApiSPP = 'api/data/spp';
        $responApi = ApiService::request(env("API_URL") . $prefixApiSPP . '/pos_pemasukan/' . $request->id, "PUT", $payload);

        return response()
            ->json($responApi)
            ->setStatusCode($responApi['code']);
    }
}
