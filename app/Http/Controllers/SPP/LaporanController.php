<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        return view('content.spp.laporan', [
            'currentPage' => 'laporan',
        ]);
    }

    public function penerimaanSPP(Request $request)
    {
        return view('content.spp.laporan-penerimaan', [
            'currentPage' => 'laporan',
        ]);
    }
}
