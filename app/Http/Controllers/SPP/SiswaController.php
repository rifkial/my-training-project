<?php

namespace App\Http\Controllers\SPP;
use App\ApiService\User\SiswaApi;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Helpers\Help;
use Exception;

use Carbon\Carbon;

use App\Helpers\ApiService;
use App\ApiService\Master\KelasSiswaApi;
use Illuminate\Support\Str;
use XcS\XcTools;

class SiswaController extends Controller
{
    private $siswaApi;

    public function __construct()
    {
        $this->siswaApi = new SiswaApi();
    }

    public function index(Request $request)
    {
        //dd($request);
        $dataSiswa = (new SiswaApi)->get_by_sekolah();   
        $dataSiswa = $dataSiswa["body"]["data"];
        //dd($dataSiswa);
        $export = (new SiswaApi)->import();  

        if ($request->ajax()) {
            $responSiswa = (new KelasSiswaApi)->get_by_sekolah(session('tahun'));
            // Respon untuk datatables-nya
            if (array_key_exists('draw', $request->all())) {
                return datatables()
                    ->of($responSiswa['body']['data'])
                    ->make(true);
            }
            // Respon untuk get collection resource siswa
            return response()->json($responSiswa)
                ->setStatusCode($responSiswa['code']);
        }
        
        return view('content.spp.siswa',['allSiswa' => $dataSiswa , 'export'=> $export]);
    }

    public function create(){
        $template = (new SiswaApi)->import();

        return view('content.spp.create-siswa',['export'=> $template]);
    }

    public function import(Request $request)
    {
        //dd($request);
        $files = $request->file('image');
        //dd($files);
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        //dd($data);
        $result = $this->siswaApi->upload_excel(json_encode($data));
        // dd($result);
        // File::delete($path);
        //dd($result);
        $code = $result['kode'];
        //dd($code);
        $message = $result['data']['message'];
        // if ($code == 200) {
        //     return response()->json([
        //         'success' => $message,
        //         'icon'  => 'success',
        //         'status' => 'berhasil'
        //     ]);
        //     File::delete($path);
        // } else {
        //     return response()->json([
        //         'success' => $message[0]['error'],
        //         'icon'  => 'error',
        //         'status' => 'gagal'
        //     ]);
        // }
        return view('content.spp.siswa');
    }

    public function store(Request $request)
    {
        $this->siswaApi = new SiswaApi();

        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $kodes = $request['nisn'].$request['nama'];
        $data = array(
            'nik' => $request->nik,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'tahun_angkatan' => $request->tahun_angkatan,
            'nama' => $request->nama,
            'jenkel' => $request->jenkel,
            'agama' => $request->agama,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
            'alamat' => $request->alamat,
            'lintang' => $request->lintang,
            'bujur' => $request->bujur,
            'anak_ke' => $request->anak_ke,
            'status_keluarga' => $request->status_keluarga,
            'kls_diterima' => $request->kelas_diterima,
            'tgl_diterima' => date('Y-m-d', strtotime($request->tgl_diterima)),
            'no_ijazah' => $request->no_ijazah,
            'th_ijazah' => $request->tahun_ijazah,
            'no_skhun' => $request->no_skhun,
            'th_skhun' => $request->th_skhun,
            'nama_ayah' => $request->nama_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'nama_ibu' => $request->nama_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'nama_wali' => $request->nama_wali,
            'alamat_wali' => $request->alamat_wali,
            'telp_wali' => $request->telepon_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'id_sekolah' => session('id_sekolah'),
            'password' => Str::random(5),
            'status' => 1
        );
        

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->siswaApi->create_file(json_encode($data));
            File::delete($path);
            $code = $result['kode'];
            $message = $result['data']['message'];
        } else {
            $result = $this->siswaApi->create(json_encode($data));
            $code = $result['code'];
            $message = $result['body']['message'];
        }
        // if ($code == 200) {
        //     return response()->json(['success' => $message, 'icon'  => 'success', 'status' => 'berhasil']);
        // } else {
        //     return response()->json(['success' => $message, 'icon'  => 'error', 'status' => 'gagal']);
        // }
        $import = (new SiswaApi)->import_siswa();
        return view('content.spp.siswa')->with((['import' => $import]));

    }

    public function siswa(Request $request, $id_kelas_siswa)
    {
        //dd($this);
        $responSiswa = $this->getTagihanByIdKelasSiswa($id_kelas_siswa);
        //dd($responSiswa);
        if ($request->ajax()) {
            // Respon untuk datatables-nya
            if (array_key_exists('draw', $request->all())) {
                return datatables()
                    ->of($responSiswa['body']['data']['tagihan'])
                    ->make(true);
            }
            // Respon untuk get resouce siswa tunggal by id
            return response()->json($responSiswa)
                ->setStatusCode($responSiswa['code']);
        }
        $request->session()->put('tanggalHariIni', Carbon::today()->format('m/d/Y'));
        return view('content.spp.siswa-detail')->with([
            'dataSiswa' => $responSiswa['body']['data'],
            'dataTagihanBulanIni' => $this->getTagihanSiswaBulanIni($id_kelas_siswa),
            'dataOrtu' => $this->getOrtu($id_kelas_siswa),
            'dataTahunAjaran' => $this->getTahunAjaran(),
        ]);
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $payload = [
                'jenis' => $request->jenis,
                'value' => $request->katakunci,
            ];
            $hasilSearch = $this->getSearchSiswa($payload);
            return datatables()
                ->of($hasilSearch)
                ->make(true);
        }
    }

    public function getTagihanSiswa($id_kelas_siswa)
    {
        $respon = $this->getTagihanByIdKelasSiswa($id_kelas_siswa);
        $respon['body']['data'] = $respon['body']['data']['tagihan'];
        return $respon;
    }

    public function tagihanBySiswa($id_kelas_siswa)
    {
        $listTransaksi = $this->getTransaksiByKelasSiswa($id_kelas_siswa);
        return datatables()
            ->of($listTransaksi)
            ->make(true);
    }

    private function getTransaksiByKelasSiswa($id_kelas_siswa)
    {
        $respon = ApiService::request(
            env("API_URL") . 'api/data/spp/transaksi/siswa/' . $id_kelas_siswa,
            "GET",
            null
        );
        return $respon['body']['data'];
    }

    private function getSearchSiswa($payload)
    {
        $responSearch = ApiService::request(
            env("API_URL") . 'api/data/master/kelas_siswa/search/siswa/tahun/' . session('tahun'),
            "POST",
            json_encode($payload)
        );
        return $responSearch['code'] === 200 ? $responSearch['body']['data'] : [];
    }

    private function getTagihanByIdKelasSiswa($id)
    {
        $respon = ApiService::request(
            env('API_URL') . 'api/data/spp/tagihan/kelas/siswa/' . $id,
            "GET",
            null
        );
        if ($respon['code'] !== 200) {
            throw new Exception("Error mengambil data tagihan siswa", 1);
        }
        //dd($respon);
        return $respon;
    }

    private function getTagihanSiswaBulanIni($id_kelas_siswa)
    {
        $endpoint = env('API_URL') . 'api/data/spp/tagihan/kelas/siswa/' . $id_kelas_siswa . '/bulan/' . Carbon::today()->monthName;
        $responTagihan = ApiService::request($endpoint, "GET", null);
        return $responTagihan['body']['data'];
    }

    private function getOrtu($id_siswa)
    {
        $respon = ApiService::request(
            env('API_URL') . 'api/data/user/ortu/siswa/' . $id_siswa,
            "GET",
            null
        );

        
        return  $respon['body']['data'];
        
        
    }

    private function getTahunAjaran()
    {
        $responTahunAjaran = ApiService::request(
            env("API_URL") . 'api/data/master/tahun_ajaran/sekolah/tahun_ajaran',
            "GET",
            null
        );
        $tahunDesc = collect($responTahunAjaran['body']['data'])
            ->sortDesc()
            ->map(function ($item) {
                list($taAwal, $taAkhir) = explode('/', $item['tahun_ajaran']);
                return array(
                    'ta_mulai' => $taAwal,
                    'ta_akhir' => $taAkhir,
                    'tahun_ajaran' => $item['tahun_ajaran'],
                );
            })
            ->values()->all();
        return $tahunDesc;
    }
}
