<?php

namespace App\Http\Controllers\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ApiService;
use App\ApiService\Master\KelasSiswaApi;

class KonfirmasiController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $listKonfirmasi = $this->getKonfirmasi();
            return datatables()
                ->of($listKonfirmasi)
                ->make(true);
        }
        $responSiswa = (new KelasSiswaApi)->get_by_sekolah(session('tahun'));
        return view('content.spp.konfirmasi')->with([
            'dataSiswa' => $responSiswa['body']['data'],
            'dataTahunAjaran' => $this->getTahunAjaran(),
        ]);
    }

    public function create(Request $request)
    {
        $payload = [
            'id_sekolah' => $request->session()->get('id_sekolah'),
            'id_kelas_siswa' => $request->id_kelas_siswa,
            'nis' => $request->nis,
            'bentuk' => $request->bentuk,
            'tahun_ajaran' => $request->tahun_ajaran,
            'details' => $request->details,
        ];
        $responPost = $this->postKonfirmasi($payload);
        return response()->json($responPost)
            ->setStatusCode($responPost['code']);
    }

    private function postKonfirmasi($payload)
    {
        $responKonfirmasi = ApiService::request(
            env("API_URL") . 'api/data/spp/transaksi_konfirmasi',
            "POST",
            json_encode($payload)
        );
        return $responKonfirmasi;
    }

    public function detail(Request $request, $id_konfirmasi)
    {
        $responGetDetail = $this->getKonfirmasiDetail($request->query()['kode_transaksi']);
        return response()
            ->json($responGetDetail)
            ->setStatusCode($responGetDetail['code']);
    }

    private function getKonfirmasi()
    {
        $respon = ApiService::request(
            env("API_URL") . 'api/data/spp/transaksi_konfirmasi/sekolah/profile',
            "GET",
            null
        );
        return $respon['body']['data'];
    }

    private function getKonfirmasiDetail($kode_transaksi)
    {
        $respon = ApiService::request(
            env("API_URL") . 'api/data/spp/detail_transaksi/transaksi/' . urlencode($kode_transaksi),
            "GET",
            null
        );

        // Get nama tagihan untuk label
        if (!empty($respon['body']['data'])) {
            foreach ($respon['body']['data'] as $key => $value) {
                $responTagihan = ApiService::request(
                    env("API_URL") . 'api/data/spp/tagihan/' . $value['id_tagihan'],
                    "GET",
                    null
                );
                $respon['body']['data'][$key]['nama_tagihan'] = $responTagihan['body']['data']['nama'];
            }
        }

        return $respon;
    }

    private function getTahunAjaran()
    {
        $responTahunAjaran = ApiService::request(
            env("API_URL") . 'api/data/master/tahun_ajaran/sekolah/tahun_ajaran',
            "GET",
            null
        );
        $tahunDesc = collect($responTahunAjaran['body']['data'])
            ->sortDesc()
            ->map(function ($item) {
                list($taAwal, $taAkhir) = explode('/', $item['tahun_ajaran']);
                return array(
                    'ta_mulai' => $taAwal,
                    'ta_akhir' => $taAkhir,
                    'tahun_ajaran' => $item['tahun_ajaran'],
                );
            })
            ->values()->all();
        return $tahunDesc;
    }
}
