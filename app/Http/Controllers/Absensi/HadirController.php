<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\HadirApi;
use App\ApiService\Master\RombelApi;
use App\ApiService\Master\TahunAjaranApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class HadirController extends Controller
{
    private $hadirApi;
    private $tahunApi;
    private $rombelApi;

    public function __construct()
    {
        $this->hadirApi = new HadirApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->rombelApi = new RombelApi();
    }

    public function index()
    {
        $guru = $this->hadirApi->izin_guru(session('id_tahun_ajar'));
        // dd($hadir);
        $guru = $guru['body']['data'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        // dd($tahun);
        $tahun = $tahun['body']['data'];
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $template = session('template');
        session()->put('title', "Kehadiran");
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_kehadiran')->with(['template' => $template, 'guru' => $guru, 'tahun' => $tahun, 'rombel' => $rombel]);
    }

    public function cetak()
    {
        // dd($_GET['rombel']);
        $based = $_GET['based'];
        $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
        $tahun = $tahun['body']['data'];
        // dd($tahun);
        if ($based == 'guru') {
            $guru = $this->hadirApi->izin_guru(session('id_tahun_ajar'));
            $guru = $guru['body']['data'];
          
            $pdf = PDF::loadview('content.absensi.cetak.v_guru', ['guru' => $guru, 'tahun' => $tahun]);
            return $pdf->stream();
        } else {
            $siswa = $this->hadirApi->hadir_siswa_semester($_GET['rombel'], session('tahun'), session('id_tahun_ajar'));
            if ($siswa['code'] == 200) {
                $siswa = $siswa['body']['data'];
            } else {
                $siswa = [];
            }
            $pdf = PDF::loadview('content.absensi.cetak.v_siswa', ['siswa' => $siswa, 'tahun' => $tahun]);
            return $pdf->stream();
        }
    }

    public function siswa()
    {
        // $tanggal = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $template = session('template');
        session()->put('title', "Kehadiran Siswa");
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_hadir')->with(['template' => $template, 'rombel' => $rombel]);
    }
    
    public function guru()
    {
        $template = session('template');
        session()->put('title', "Kehadiran Guru");
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_kehadiran_guru')->with(['template' => $template]);
    }

    public function store(Request $request)
    {
        $data = array(
            'long' => $request->long,
            'lat' => $request->lat,
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_sekolah' => session('id_sekolah'),
            'role' => session('role'),
            'id' => session('id'),
        );
        if ($request['image'] != null) {
            $data['image'] = $request['image'];
        } else {
            //// Jika Gambar tidak di inputkan Data yang dikirim ke API menggunakan gambar No Image
            $data['image'] = Help::no_img_base64();
        }

        $result = $this->hadirApi->create_login(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $hadir = $this->data_riwayat_siswa();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'hadir' => $hadir
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    private function data_riwayat_siswa()
    {
        $hadir = $this->hadirApi->my_izin(session('id'), date('Y-m-d'));
        // dd($detail);
        $hadir = $hadir['body']['data'];
        $html = '';
        if (!empty($hadir)) {
            $jam_masuk = '-';
            $jam_keluar = '-';
            $kelas = 'danger';
            if ($hadir['jam_masuk'] != null) {
                $jam_masuk = Help::getTime($hadir['jam_masuk']);
            }
            if ($hadir['jam_keluar'] != null) {
                $jam_keluar = Help::getTime($hadir['jam_keluar']);
            }

            if ($hadir['status_kehadiran'] == 'hadir') {
                $kelas = 'success';
            }
            
            $html .= '<tr data-toggle="collapse" data-target="#detail_hadir"
            style="cursor: pointer">
            <td class="text-center">
                <b>'.$jam_masuk.'</b>
            </td>
            <td class="text-center">
                <b>'.$jam_keluar.'</b>
            </td>
            <td class="text-center"><span
                    class="badge badge-'.$kelas.' text-inverse">'.$hadir['status_kehadiran'].'</span>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="hiddenRow">
                <div class="accordian-body collapse" id="detail_hadir">
                    <table class="table table-bordered">
                        <tr>
                            <th class="vertical-middle">Tanggal</th>
                            <td class="text-center">
                                '.Help::getTanggal($hadir['tgl_kehadiran']).'
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-middle">Bukti Masuk</th>
                            <td class="text-center"> <img
                                    src="'.$hadir['bukti_masuk'].'" alt=""
                                    height="150"></td>
                        </tr>
                        <tr>
                            <th class="vertical-middle">Bukti Keluar</th>
                            <td class="text-center"> <img
                                    src="'.$hadir['bukti_keluar'].'" alt=""
                                    height="150"></td>
                        </tr>
                        <tr>
                            <th class="vertical-middle">Keterangan</th>
                            <td class="text-center">
                                <p>'.$hadir['keterangan'].'</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>';
        } else {
            $html .= '<tr>
            <td colspan="3" class="text-center">Belum ada Absensi untuk hari ini
            </td>
        </tr>';
        }
        return $html;
    }

    public function filter_siswa(Request $request)
    {
        $hadir = $this->hadirApi->hadir_siswa_semester($request['id_rombel'], session('tahun'), session('id_tahun_ajar'));
        // dd($hadir);
        if ($hadir['code'] == 200) {
            $hadir = $hadir['body']['data'];
        } else {
            $hadir = [];
        }
       
        $html = '';
        if (!empty($hadir)) {
            foreach ($hadir as $hd) {
                $html .= '<tr>
                <td>'.ucwords($hd['nama']).'</td>
                <td>'.$hd['nis'].'</td>';
                if (!empty($hd['kehadiran'])) {
                    $html .= '<td class="text-center">'.$hd['kehadiran']['hadir'].'</td>
                    <td class="text-center">'.$hd['kehadiran']['sakit'].'</td>
                    <td class="text-center">'.$hd['kehadiran']['izin'].'</td>
                    <td class="text-center">'.$hd['kehadiran']['alpha'].'</td>';
                } else {
                    $html .= ' <td colspan="4" class="text-center">Data saat ini tidak tersedia
                    </td>';
                }
                $html .= '</tr>';
            }
        } else {
            $html .= '<tr><td colspan="6" class="text-center">Saat ini data tidak tersedia</td></tr>';
        }
        return response()->json($html);
    }

    public function fil_siswa(Request $request)
    {
        // dd($request);
        if ($request['based'] == 'guru') {
            $hadir = $this->hadirApi->rekap_guru($request['bulan'], $request['tahun']);
        } else {
            $hadir = $this->hadirApi->rekap_siswa($request['rombel'], session('tahun'), $request['bulan'], $request['tahun']);
        }
        // dd($hadir);
        $hadir = $hadir['body']['data'];
        $html = '';
        $tanggal = cal_days_in_month(CAL_GREGORIAN, $request['bulan'], $request['tahun']);
        $count_tanggal = $tanggal+ 2;
        $html .= '<h3 class="m-0 text-center">DAFTAR KEHADIRAN</h3>
                <h5 class="box-title mr-b-0 text-center">Bulan '. strftime('%B', mktime(0, 0, 0, $request['bulan'])).'</h5>
                <p class="text-muted text-center">Tahun : '.$request['tahun'].'</p>';
        if ($request['based'] == 'guru') {
            $html .= ' <div class="w-100"><a href="'.route('absensi-hadir_cetak_rekap_pdf', ['base' => 'guru', 'bulan' => $request['bulan'], 'tahun' => $request['tahun']]).'" target="_blank" class="btn btn-pinterest pull-right my-2"><i class="fas fa-file-pdf"></i> Cetak PDF</a></div>';
        } else {
            $html .= ' <div class="w-100"><a href="'.route('absensi-hadir_cetak_rekap_pdf', ['base' => 'sisa', 'rombel' => $request['rombel'], 'bulan' => $request['bulan'], 'tahun' => $request['tahun']]).'" target="_blank" class="btn btn-pinterest pull-right my-2"><i class="fas fa-file-pdf"></i> Cetak PDF</a></div>';
        }
                
        $html .= '<div class="table-responsive recentOrderTable">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="vertical-middle" rowspan="2">No</th>
                                <th scope="col" class="vertical-middle" rowspan="2">Informasi</th>
                                <th scope="col"  colspan="'.$tanggal.'" class="text-center vertical-middle">Tanggal</th>
                                </tr>
                                <tr>';
        for ($i=1; $i <= $tanggal ; $i++) {
            $html .= '<th class="text-center">'.$i.'</th>';
        }
        $html .= '
                            </tr>
                        </thead>
                        <tbody>';
        if (!empty($hadir)) {
            $html .= '';
            $nomer = 1;
            foreach ($hadir as $hd) {
                $html .= '<tr>
                                    <td class="vertical-middle">'.$nomer++.'</td>';
                if ($request['based'] == 'guru') {
                    $html .= '<td><b>'.ucwords($hd['nama']).'</b><p class="m-0">NIP. '.$hd['nip'].'</p></td>';
                } else {
                    $html .= '<td><b>'.ucwords($hd['nama']).'</b><p class="m-0">NIS/NISN. '.$hd['nis'].'/'.$hd['nisn'].'</p></td>';
                }
                foreach ($hd['kehadiran'] as $kd) {
                    if ($kd['absensi'] == null) {
                        $html .= '<td class="bg-warning vertical-middle text-center">LIBUR</td>';
                    } else {
                        $kelas_absen = 'white';
                        if ($kd['absensi'] == 'izin') {
                            $kelas_absen = 'info';
                        } elseif ($kd['absensi'] == 'alpha') {
                            $kelas_absen = 'danger';
                        } elseif ($kd['absensi'] == 'sakit') {
                            $kelas_absen = 'success';
                        }
                        $html .= '<td class="vertical-middle bg-'.$kelas_absen.' text-center">'.ucwords($kd['absensi']).'</td>';
                    }
                }
                                    
                $html .= '</tr>';
            }
        } else {
            $html .= '<tr><td colspan="'.$count_tanggal.'" class="text-center">Tidak ada data yang tersedia</td></tr>';
        }
        $html .='</tbody></table></div>';
        

        return response()->json($html);
    }

    public function cetak_rakap()
    {
        // dd($_GET['base']);
        $waktu = array(
            'bulan' => date("F", mktime(0, 0, 0, $_GET['bulan'], 10)),
            'no_bulan' => $_GET['bulan'],
            'tahun' => $_GET['tahun'],
        );
        if ($_GET['base'] == 'guru') {
            $hadir = $this->hadirApi->rekap_guru($_GET['bulan'], $_GET['tahun']);
            $waktu['role'] = 'guru';
        } else {
            $hadir = $this->hadirApi->rekap_siswa($_GET['rombel'], session('tahun'), $_GET['bulan'], $_GET['tahun']);
            $rombel = $this->rombelApi->get_by_id($_GET['rombel']);
            $rombel = $rombel['body']['data'];
            // dd($rombel);
            $waktu['rombel'] = $rombel['nama'];
            $waktu['role'] = 'siswa';
        }
        // dd($hadir);
        $hadir = $hadir['body']['data'];
        // return view('content.absensi.cetak.v_rekap_absensi')->with(['hadir' => $hadir, 'waktu' => $waktu]);
        $pdf = PDF::loadview('content.absensi.cetak.v_rekap_absensi', ['hadir' => $hadir, 'waktu' => $waktu, ])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
