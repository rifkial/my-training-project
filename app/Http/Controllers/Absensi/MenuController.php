<?php

namespace App\Http\Controllers\Absensi;

use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function index()
    {
        $template = session('template');
        if(session('role') == 'admin-absensi'){
            $template = 'default';
        }
        return view('content.absensi.v_akses')->with(['template' => $template]);
    }
    
    public function submenu()
    {
        $template = session('template');
        if(session('role') == 'admin-absensi'){
            $template = 'default';
        }
        return view('content.absensi.v_submenu')->with(['template' => $template]);
    }
}
