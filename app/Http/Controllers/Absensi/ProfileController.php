<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\HadirApi;
use App\ApiService\Absensi\IzinApi;
use App\ApiService\Master\EnvironmentApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\User\GuruApi;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    private $hadirApi;
    private $envApi;
    private $izinApi;
    private $kelasSiswaApi;
    private $guruApi;

    public function __construct()
    {
        $this->hadirApi = new HadirApi();
        $this->izinApi = new IzinApi();
        $this->envApi = new EnvironmentApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->guruApi = new GuruApi();
    }

    public function index()
    {
        $informasi = $this->hadirApi->form_sekolah(session('id_sekolah'));
        $informasi = $informasi['body']['data'];
        if (session('role') == 'siswa') {
            $detail = $this->hadirApi->my_izin(session('id'), date('Y-m-d'));
        } else {
            $detail = $this->hadirApi->my_izin_guru(session('id'), date('Y-m-d'));
        }
        $detail = $detail['body']['data'];

        $lokasi = $this->envApi->by_jenis_sekolah('maps', session('id_sekolah'));
        $lokasi = $lokasi['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        session()->put('title', 'Data Siswa');
        return view('content.dashboard.absensi.absen.v_absensi')->with(['template' => $template, 'informasi' => $informasi, 'detail' => $detail, 'lokasi' => $lokasi]);
    }

    public function izin()
    {
        $informasi = $this->hadirApi->form_sekolah(session('id_sekolah'));
        $informasi = $informasi['body']['data'];
        if (session('role') == 'siswa') {
            $riwayat = $this->izinApi->riwayat_siswa_date(session('id'), date('Y-m-d'));
            $profile = $this->kelasSiswaApi->get_by_id(session('id_kelas_siswa'));
        } else {
            $riwayat = $this->izinApi->riwayat_guru_date(session('id'), date('Y-m-d'));
            $profile = $this->guruApi->get_by_id(session('id'));
        }
        $profile = $profile['body']['data'];
        $riwayat = $riwayat['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        $lokasi = $this->envApi->by_jenis_sekolah('maps', session('id_sekolah'));
        $lokasi = $lokasi['body']['data'];
        session()->put('title', 'Izin ' . ucwords(session('role')));
        return view('content.dashboard.absensi.absen.v_izin')->with(['template' => $template, 'riwayat' => $riwayat, 'informasi' => $informasi, 'profile' => $profile, 'lokasi' => $lokasi]);

        // dd("izin profile");
    }
}
