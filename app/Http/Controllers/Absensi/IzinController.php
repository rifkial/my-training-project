<?php

namespace App\Http\Controllers\Absensi;

use App\ApiService\Absensi\IzinApi;
use App\ApiService\Master\RombelApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class IzinController extends Controller
{
    private $izinApi;
    private $rombelApi;

    public function __construct()
    {
        $this->izinApi = new IzinApi();
        $this->rombelApi = new RombelApi();
    }

    public function index()
    {
        $rombel = $this->rombelApi->get_by_sekolah();
        $rombel = $rombel['body']['data'];
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_izin')->with(['template' => $template, 'rombel' => $rombel]);
    }

    public function guru()
    {
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_izin_guru')->with(['template' => $template]);
    }

    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'tgl_kehadiran' => $request->tanggal,
            'keterangan' => $request->keterangan,
            'status_kehadiran' => $request->alasan,
            'long' => $request->longtitude,
            'lat' => $request->latitude,
            'id_ta_sm' => session('id_tahun_ajar'),
            'id_sekolah' => session('id_sekolah'),
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->izinApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->izinApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $riwayat_siswa = $this->data_riwayat_siswa();
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil',
                'riwayat_siswa' => $riwayat_siswa
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $izin = $this->izinApi->get_by_id($request['id']);
        $izin = $izin['body']['data'];
        return response()->json($izin);
    }

    private function data_riwayat_siswa()
    {
        $riwayat = $this->izinApi->riwayat_siswa_date(session('id'), date('Y-m-d'));
        // dd($riwayat);
        $riwayat = $riwayat['body']['data'];
        $html = '';
        if (!empty($riwayat)) {
            $html .= '<tr>
            <td class="text-center"><span class="badge badge-danger text-inverse">Izin</span></td>
            <td class="text-center"> '.Help::getTanggalLengkap($riwayat['created_at']).'</td>
                <td class="text-center"><span class="text-danger"><i class="fas fa-exclamation-circle"></i> Belum Diverifikasi</span></td>
            </tr>';
        } else {
            $html .= '<tr>
            <td class="text-center" colspan="3"><span
            class="badge badge-success text-inverse">Belum ada pengajuan Izin
            kamu hari ini</span></td>
        </tr>';
        }
        return $html;
    }

    public function rekap_filter(Request $request)
    {
        // dd($request);
        if ($request['based'] == 'guru') {
            $izin = $this->izinApi->get_filter_guru($request['bulan'], $request['tahun']);
        } else {
            $izin = $this->izinApi->get_filter_mont_year($request['rombel'], $request['bulan'], $request['tahun']);
        }
        // dd($izin);
        $izin = $izin['body']['data'];
        $html = '';
        if (!empty($izin)) {
            foreach ($izin as $iz) {
                $checked = '';
                if ($iz['konfirmasi'] == 1) {
                    $checked = 'checked';
                }
                $html .= '<tr>
                <td>
                    <b>'.strtoupper($iz['nama']).'</b>
                    <br><small>Role : '.$iz['role'].'</small>
                    <p class="m-0">Kode Akun : '.$iz['kode_akun'].'</p>
                </td>
                <td class="vertical-middle">'.Help::getTanggal($iz['tgl_kehadiran']).'</td>
                <td class="vertical-middle">'.$iz['status_kehadiran'].'</td>
                <td class="vertical-middle">
                    <label class="switch">
                        <input type="checkbox" '.$checked.'
                            class="check_konfirmasi" data-id="'.$iz['id'].'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td class="vertical-middle">
                    <a href="javascript:void(0)" class="btn btn-purple btn-sm lihatMaps" data-id="'.$iz['id'].'"><i class="fas fa-location-arrow"></i></a>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#detail'.$iz['id'].'" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></a>
                </td>
            </tr>
            <tr>
                <td colspan="7" class="hiddenRow">
                    <div class="accordian-body collapse" id="detail'.$iz['id'].'">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="vertical-middle">Bukti</th>
                                    <td class="vertical-middle" width="200"><img src="'.$iz['bukti'].'" alt="" width="200"></td>
                                    <th class="text-center">Keterangan</th>
                                    <td class="text-justify vertical-middle">'.$iz['keterangan'].'</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </td>
            </tr>';
            }
        } else {
            $html .= '<tr><td colspan="5" class="text-center">Saat ini data belum tersedia</td></tr>';
        }

        return response()->json($html);
    }

    public function izin_hari_ini(Request $request)
    {
        // dd($request);
        $izin = $this->data_izin_hari_ini($request['base']);
        return response()->json($izin);
    }

    private function data_izin_hari_ini($base)
    {
        // dd($base);
        if ($base == 'guru') {
            $izin = $this->izinApi->admin_all_guru_date(date('Y-m-d'));
        } else {
            $izin = $this->izinApi->admin_all_siswa_date(session('tahun'), date('Y-m-d'));
        }
        // dd($izin);
        $izin = $izin['body']['data'];
        // dd($izin);
        $html = '';
        if (!empty($izin)) {
            $no = 1;
            foreach ($izin as $iz) {
                $check = '';
                if ($iz['konfirmasi'] == 1) {
                    $check = 'checked';
                }
                $html .= '<tr>
                <td>'.$no++.'</td>
                <td>
                    <b>'.strtoupper($iz['nama']).'</b> <br>
                    <small>Kode Akun. '.$iz['kode_akun'].'</small>';
                if ($base == 'siswa') {
                    $html .= '<p class="m-0">NIS/NISN. '.$iz['nis'].'/'.$iz['nisn'].'</p>';
                } else {
                    $html .= '<p class="m-0">NIP. '.$iz['nip'].'</p>';
                }
                $html .= '</td>
                <td>
                    <b>'.$iz['status_kehadiran'].'</b> <br>
                    <small>Keterangan. '.$iz['keterangan'].'</small>
                </td>';
                if ($base == 'siswa') {
                    $html .= ' <td>
                    <b>'.$iz['rombel'].'</b>
                    <br> <small>'.$iz['jurusan'].'</small>
                </td>';
                }
                $html .= '<td>'.Help::getDay($iz['tgl_kehadiran']).'</td>
                <td>';
                if ($iz['konfirmasi'] == 1) {
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-warning m-1 batal" data-id="'.$iz['id'].'"><i class="fas fa-undo"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-info m-1 izin_check" data-id="'.$iz['id'].'"><i class="fas fa-clipboard-check"></i></a>';
                }
                // $html .= '</td></tr>';
                if ($iz['konfirmasi'] == 2) {
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-warning m-1 batal" data-id="'.$iz['id'].'"><i class="fas fa-undo"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger m-1 reject" data-id="'.$iz['id'].'"><i class="fas fa-times-circle"></i></a>';
                }
                $html .= '</td></tr>';
            }
        } else {
            $html .= '<tr><td colspan="7" class="text-center">Data saat ini tidak tersedia</td></tr>';
        }
        return $html;
    }

    public function update_status(Request $request)
    {
        $data = array(
            'id' => $request['id'],
            'konfirmasi' => $request['value'],
        );
        $update = $this->izinApi->update_status(json_encode($data));
        if ($update['code'] == 200) {
            $izin = $this->data_izin_hari_ini($request['base']);
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil',
                'izin' => $izin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function statistic_izin()
    {
        $template = session('template');
        if (session('role') == 'admin-absensi') {
            $template = 'default';
        }
        return view('content.absensi.v_statistic_izin')->with(['template' => $template]);
    }

    public function update_izin(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'konfirmasi' => $request['value'],
        );
        $update = $this->izinApi->update_status(json_encode($data));
        // dd($update);
        if ($update['code'] == 200) {
            $izin = $this->dasboard_izin_admin($request['based']);
            return response()->json([
                'icon' => "success",
                'message' => $update['body']['message'],
                'status' => 'berhasil',
                'izin' => $izin
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $update['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }


    private function dasboard_izin_admin($base)
    {
        if ($base == 'guru') {
            $izin =  $this->izinApi->get_filter_guru(date('m'), date('Y'));
        } else {
            $izin =  $this->izinApi->statistic_siswa_bulan(session('tahun'), date('m'), date('Y'));
        }
        // dd($izin);
        $izin = $izin['body']['data'];
        $html = '';
        if (!empty($izin)) {
            $no = 1;
            foreach ($izin as $iz) {
                if ($iz['konfirmasi'] == 0) {
                    $kelas = 'danger';
                    $status = 'Belum terverifikasi';
                } elseif ($iz['konfirmasi'] == 1) {
                    $kelas = 'success';
                    $status = 'Sudah terverifikasi';
                } else {
                    $kelas = 'danger';
                    $status = 'Ditolak';
                }
                // dd($status);
                $html .= '<tr>
                <td class="vertical-middle">'.$no++.'</td>
                <td class="vertical-middle">
                    '.Help::getDay($iz['tgl_kehadiran']).'</td>
                <td>
                    <b>'.ucwords($iz['nama']).'</b>';
                if ($base == "guru") {
                    $html .= '<p class="m-0">NIP : '.$iz['nip'].'</p>';
                } else {
                    $html .= '<p class="m-0">NIS/NISN : '.$iz['nis'] . '/' . $iz['nisn'].'</p>';
                }


                $html .= '</td><td class="vertical-middle">'.$iz['status_kehadiran'].'</td>';
                if ($base == 'siswa') {
                    $html .= ' <td><b>'.$iz['rombel'].'</b><br><small>'.$iz['jurusan'].'</small></td>';
                }
                $html .= '<td class="vertical-middle">
                    <span class="badge badge-'.$kelas.' text-inverse p-1 rounded">'.$status.'</span>
                </td>
                <td>';
                if ($iz['konfirmasi'] == 0) {
                    $html .= '<a href="javascript:void(0)" data-id="'.$iz['id'].'"
                    data-base="'.$base.'" class="btn btn-sm btn-success konfirmasi"><i
                        class="fas fa-clipboard-check"></i></a>
                    <a href="javascript:void(0)" data-id="'.$iz['id'].'"
                    data-base="'.$base.'" class="btn btn-sm btn-danger reject"><i
                        class="fas fa-times-circle"></i></a>';
                } else {
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-secondary"><i
                    class="fas fa-stop-circle"></i></a>';
                }
                $html .= '</td></tr>';
            }
        } else {
            $html .= '<tr><td colspan="4" class="text-center">Tidak ada data di bulan ini</td></tr>';
        }

        return $html;
    }
}
