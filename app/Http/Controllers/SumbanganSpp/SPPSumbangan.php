<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class SPPSumbangan extends Controller
{

    protected $SppApiservice;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Dashboard SPP Online');
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        $tahun  = session('tahun');
        $datachart = array();

        if(session('role') == 'siswa'){
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        }else if(session('role') == 'ortu'){
            $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id_kelas_siswa'), $bulan);
        }else if(session('role') == 'tata-usaha'){
            $realisasi = $this->SppApiservice->report_realisasi($tahun);
        }

        $parse_data     = $tagihanspp['body']['data'] ?? array();
        $realisasi_data = $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        //check data chart
         if(count($realisasi_data) > 0){
            foreach($realisasi_data as $key => $val){
                foreach($val['bulan'] as $x => $k){
                    if (!isset($datachart[$x])) {
                        $datachart[$x] = 0;
                    }
                    $datachart[$x] += $k['nominal'];
                }
            }
         }

        $param = [
            'bulan' => $bulan, 'tagihan' => $parse_data,
            'realisasi'=>$realisasi_data,'data_chart' => $datachart,
            'list_bulan' =>$list_bulan,
            'tahun'=>$tahun
        ];

        return view('spp.components.v_spp_dashboard')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profilUser()
    {
        Session::put('title', 'My Profile');
        $data_profile = $this->SppApiservice->get_info_profil();
        $parse_profil = $data_profile['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param  = [
            'profil_data' => $parse_profil,
        ];
        return view('spp.components.my_profil')->with(array_merge($param, $configApi));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required|max:255',
            'alamat' => 'required|max:255',
            'kota' => 'required',
            'telephone' => 'required|numeric|digits:13',
            'email' => 'required|email|unique:users'
        ], [
            'nama.required' => 'Form nama tidak boleh kosong!',
            'alamat.required' => 'Form alamat tidak boleh kosong!',
            'kota.required' => 'Form kota tidak boleh kosong!',
            'telephone.required' => 'Form telephone tidak boleh kosong!',
            'telephone.digits' => 'Nomor telephone maksimal 13 digit!',
            'telephone.numeric' => 'Nomor telephone hanya diperbolehkan berupa angka!',
            'email.required' => 'Form email tidak boleh kosong!',
            'email.email' => 'Format email tidak valid ! ',
            'image.max' => 'Ukuran File Gambar maksimal 2048 kb',
        ]);

        // from request input form
        $data_post = array();
        $data_post['username']     = session('username');
        $data_post['nama']         = $request->nama;
        $data_post['alamat']       = $request->alamat;
        $data_post['tempat_lahir'] = $request->kota;
        $data_post['telepon']      = $request->telephone;
        $data_post['email']        = $request->email;

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data_post['path'] = $path;
            $json_body = json_encode($data_post);
            $response_update = $this->SppApiservice->update_profil_withfile($json_body);
            File::delete($path);
        } else {
            $json_body = json_encode($data_post);
            //update ke api backend
            $response_update = $this->SppApiservice->update_profil($json_body);
        }

        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->nama);
            return back()->with('success', 'Successfully Profil Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Profil Admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_password()
    {
        Session::put('title', 'Ubah Password ');
        $configApi  = $this->configApi();
        return view('spp.components.v_password')->with(array_merge($configApi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update_password(Request $request)
    {
        $request->validate([
            //'username'=>'required|max:255',
            'password_lama' => 'required|max:255',
            'password_baru' => 'required|required_with:password_confirm_baru|same:password_confirm_baru',
            'password_confirm_baru' => 'required',
        ], [
            //'username.required'=>'Form username tidak boleh kosong!',
            'password_lama.required' => 'Form password lama tidak boleh kosong!',
            'password_baru.required' => 'Form password baru tidak boleh kosong!',
            'password_confirm_baru.required' => 'Form konfirm password baru  tidak boleh kosong!',
            'password_baru.same' => 'Form password konfirm dengan harus sama dengan password baru ! '
        ]);

        // from request input form
        $data_post = array();
        //$data_post['username']         = $request->username;
        $data_post['current_password'] = $request->password_lama;
        $data_post['new_password']     = $request->password_baru;
        $data_post['confirm_password'] = $request->password_confirm_baru;

        $json_body = json_encode($data_post);

        $response_update = $this->SppApiservice->update_password($json_body);

        //dd($response_update);

        if ($response_update['code'] == '200') {
            //create session update username than redirect back
            session()->put('username', $request->username);
            return back()->with('success', 'Successfully Password Admin is saved!');
        } else {
            return back()->with('error', ' Error Api Update Password Admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** public function forget password */
    public function forget_password()
    {
        Session::put('title', 'Lupa Password');
        $configApi  = $this->configApi();
        return view('spp.components.forget_pass')->with(array_merge($configApi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_forget_pass(Request $request)
    {
        $request->validate([
            'email' => 'required |email',
        ], [
            'email.required' => 'Form Email tidak boleh kosong!',
        ]);

        $data = array();
        $data['email'] = $request->email;

        $response_update = $this->SppApiservice->forget_pass(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /** get Token */
    public function token_passwordx($token)
    {
        Session::put('title', 'Reset Password Baru ');
        $configApi  = $this->configApi();
        $param = ['token' => $token];
        return view('spp.components.forget_pass_reset')->with(array_merge($configApi,$param));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_reset_pass(Request $request)
    {
        $request->validate([
            'password_new' => 'required',
            'password_confirm' => 'required | same:password_new',
        ], [
            'password_new.required' => 'Form Password Baru tidak boleh kosong!',
            'password_confirm.required' => 'Form Password Konfirm tidak boleh kosong!',
        ]);

        $token = $request->token_reset;
        $response_check = $this->SppApiservice->get_token_pass($token);

        if ($response_check['code'] == '200') {
            $data = array();
            $data['as'] = $response_check['body']['data']['as'];
            $data['email'] = $response_check['body']['data']['email'];
            $data['password'] = $request->password_new;
            $data['password_confirmation'] = $request->password_confirm;
            $response_update = $this->SppApiservice->reset_pass(json_encode($data));
            if ($response_update['code'] == '200') {
                Session::put('title', ' Auth Login ');
                $pesan = array(
                    'message' => $response_update['body']['message'],
                    'icon' => 'berhasil',
                    'status' => 'success'
                );
                return redirect()->route('auth.login')->with(['message' => $pesan]);
            } else {
                return back()->with('error', $response_update['body']['message']);
            }
        } else {
            return redirect()->route('forget_passwordx')->with('error', $response_check['body']['message']);
        }
    }
}
