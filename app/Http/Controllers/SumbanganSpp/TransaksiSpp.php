<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Helpers\Help;

class TransaksiSpp extends Controller
{
    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Transaksi SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $kelas        = $this->SppApiservice->kelasApi();
        $parse_kelas  = $kelas['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = ['tahun_ajaran' => $parse_tahun_ajaran, 'kelas' => $parse_kelas];
        return view('spp.components.v_transaksi_spp')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_pemasukan()
    {
        Session::put('title', 'Histori Transaksi SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
        return view('spp.components.v_transaksi_history')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function keringananspp()
    {
        Session::put('title', 'Keringanan SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $tagihan = $this->SppApiservice->search_tagihan(session('tahun'));
        $parse_tagihan = $tagihan['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran, 'tagihan' => $parse_tagihan];
        return view('spp.components.v_transaksi_keringanan')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function konfirmasi_spp()
    {
        Session::put('title', 'Konfirmasi Pembayaran SPP');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $template_pesan     = $this->SppApiservice->setting_pesan();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_pesan        = $template_pesan['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param =  [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'pesan' => $parse_pesan,
        ];

        return view('spp.components.v_transaksi_konfirmasi')->with(array_merge($param, $configApi));
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show_transaction_konfirmasi($id)
    {
        $response_show_detail = $this->SppApiservice->get_transconfirm_detail($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request)
    {
        $pengaturan = $this->SppApiservice->siswaApi(session('tahun'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-primary btn-sm create tagihan" title="Transaksi Bulan ini " data-nama="' . $data['nama'] . '" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-exchange"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-info btn-sm show riwayatx" title="Lihat Riwayat Transaksi" data-nama="' . $data['nama'] . '" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-eye"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-success btn-sm more transaction " title="Transaksi Lainnya" data-nama="' . $data['nama'] . '" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-fire"></i> </a>&nbsp;';
                    $button .= '<a  href="' . route('print_tagihan_siswax', ['id' => $data['id']]) . '" target="_blank" class="btn btn-warning btn-sm print tagihanx " title="Print Tagihan yang belum dibayar " data-nama="' . $data['nama'] . '" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });

            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     ** @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function print_tagihan_siswa($id)
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan($id, $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data];
        return view('spp.components.v_tagihan_siswa_printx')->with(array_merge($configApi, $param));
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_pemasukan($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_pemasukan_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_siswa($id)
    {
        $response_show_detail = $this->SppApiservice->siswaApibyid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  transaction history  the specified resource.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_riwayat_transaksi($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->get_transaksi_siswa_id($id);
        $result = $response_show_detail['body']['data'] ?? array();
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-info btn-sm show riwayat"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-eye"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-success btn-sm print riwayat"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->rawColumns(['action', 'nominal']);
            $table->addIndexColumn();
            return  $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_siswa(Request $request)
    {
        $pengaturan = $this->SppApiservice->siswaApi(session('tahun'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-primary btn-sm choose"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" data-nisn="' . $data['nisn'] . '" data-rombel="' . $data['rombel'] . '"  data-nama_siswa="' . $data['nama'] . '" style="color: #fff"><i class="fa fa-plus"></i> </a>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });

            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_tagihan_kelas($id)
    {
        $response_show_detail = $this->SppApiservice->tagihan_siswa_by_kelas_id($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tagihan($id)
    {
        $r                   = explode('|', $id);
        $response_show_detail = $this->SppApiservice->show_tagihan($r[0]);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_konfirm_pesan(Request $request)
    {
        $request->validate([
            'bulan' => 'required',
            'pesan' => 'required',
        ], [
            'bulan.required' => 'Pilih bulan terlebih dahulu, tidak boleh kosong!',
            'pesan.required' => 'Pilih pesan terlebih dahulu, tidak boleh kosong!',
        ]);

        $list_bulan = [
            '-',
            'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
            'Januari', 'Februari', 'Maret',
            'April', 'Mei', 'Juni'
        ];

        $bulan = $list_bulan[$request->bulan];
        $create_konfirmasi = $this->SppApiservice->post_store_siswa_konfirm($request->id_siswa, $bulan, $request->pesan);
        //dd($create_konfirmasi);
        if ($create_konfirmasi['code'] == '200') {
            if (empty($create_konfirmasi['body']['data'])) {
                return response()->json(
                    [
                        'message' => $create_konfirmasi['body']['message'],
                        'data' => $create_konfirmasi['body']['data'] ?? array(),
                        'info' => 'error',
                    ]
                );
            } else {

                $message  = [
                    'id_siswa' => $request->id_siswa,
                    'message'  => 'Tagihan bulan ' . $list_bulan[$request->bulan] . ' dengan kode transaksi ' . $create_konfirmasi['body']['data']['kode'] . ' harap segera melakukan pembayaran !',
                    'link'     => $create_konfirmasi['body']['data']['link'],
                    'kode_trans' => $create_konfirmasi['body']['data']['kode'],
                    'tolak' => false
                ];
                $this->SppApiservice->send_notif('spp-channel-ortu', 'spp_event_ortu', $message);

                //push save table
                $data_notikasi = array();
                $data_notikasi['id_kelas_siswa'] = $request->id_siswa;
                $data_notikasi['url'] = $message['link'];
                $data_notikasi['isi'] = $message['message'];
                $data_notikasi['id_sekolah'] = session('id_sekolah');
                $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

                return response()->json(
                    [
                        'message' => $create_konfirmasi['body']['message'],
                        'data' => $create_konfirmasi['body']['data'] ?? array(),
                        'info' => 'success',
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'message' => $create_konfirmasi['body']['message'],
                    'data' => $create_konfirmasi['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_ajaran' => 'required',
            'tgl_bayar' => 'required',
        ], [
            'tahun_ajaran.required' => 'Pilih Tahun ajaran terlebih dahulu, tidak boleh kosong!',
            'tgl_bayar.required' => 'Tanggal Pembayaran tidak boleh kosong!',
        ]);

        $data_transaksi = array();
        $data_transaksi['id_kelas_siswa'] = $request->id_siswa;
        $data_transaksi['tgl_bayar']      = $request->tgl_bayar;
        $data_transaksi['tahun_ajaran']   = $request->tahun_ajaran;

        $data_transaksi['nis']            = $request->nisn;
        $data_transaksi['id_sekolah']     = session('id_sekolah');

        $nominal    = str_replace(",", "", $request->nominal);
        $cekarray = array();

        $list_bulan = [
            '-',
            'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember',
            'Januari', 'Februari', 'Maret',
            'April', 'Mei', 'Juni'
        ];

        $bulanini = \Carbon\Carbon::today()->isoFormat('MMMM');
        $array_key = array_keys($list_bulan, $bulanini);

        if (!empty($request->id_tagihan)) {
            foreach ($request->id_tagihan as $key => $n) {
                $array = array();
                $array['id_tagihan'] = $n;
                if (empty($request->bulan)) {
                    $array['bulan']  = $array_key[0];
                } else {
                    $array['bulan']      = $request->bulan[$key];
                }

                $array['nominal']    = $nominal[$key];
                $cekarray[] = $array;
            }

            $data_transaksi['details'] = $cekarray;

            $response_update_transaksi = $this->SppApiservice->transaksi_tagihan(json_encode($data_transaksi));

            if ($response_update_transaksi['code'] == '200') {
                //kirim notification
                $message  = "Pembayaran SPP Tagihan oleh " . $request->nisn . " telah diterima oleh Administrator Tata Usaha";
                $send_ortu = $this->SppApiservice->send_notif('spp-channel-ortu', 'spp_event_ortu', $message);
                $send_siswa = $this->SppApiservice->send_notif('spp-channel', 'spp_event', $message);

                //push save table
                $data_notikasi = array();
                $data_notikasi['id_kelas_siswa'] = $request->id_siswa;
                $data_notikasi['url'] = route('history-pembayaran-ortu');
                $data_notikasi['isi'] = $message;
                $data_notikasi['id_sekolah'] = session('id_sekolah');
                $this->SppApiservice->send_notifikasi(json_encode($data_notikasi));

                return response()->json(
                    [
                        'message' => $response_update_transaksi['body']['message'],
                        'data' => $response_update_transaksi['body']['data'] ?? array(),
                        'info' => 'success',
                        'bayar' => $request->bayar,
                        'kembalian' => $request->kembalian
                    ]
                );
            } else {
                return response()->json(
                    [
                        'message' => $response_update_transaksi['body']['message'],
                        'data' => $response_update_transaksi['body']['data'] ?? array(),
                        'info' => 'error',
                        'bayar' => $request->bayar,
                        'kembalian' => $request->kembalian
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'message' => 'List Tagihan Tidak Boleh Kosong!',
                    'data' => '',
                    'info' => 'error',
                    'bayar' => $request->bayar,
                    'kembalian' => $request->kembalian
                ]
            );
        }
    }


    /**
     * Display the specified resource.  check tagihan bulan ini
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_tagihan_bulan($id)
    {
        $bulan_ini = \Carbon\Carbon::today()->isoFormat('MMMM');
        $cek_tagihan_bulan_ini = $this->SppApiservice->search_tagihan_by_bulan($id, ucwords($bulan_ini));
        if ($cek_tagihan_bulan_ini['code'] == '200') {
            return response()->json(
                [
                    'message' => $cek_tagihan_bulan_ini['body']['message'],
                    'data' => $cek_tagihan_bulan_ini['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $cek_tagihan_bulan_ini['body']['message'],
                    'data' => $cek_tagihan_bulan_ini['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response_show_detail = $this->SppApiservice->get_transaksi_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_last_transaksi($id)
    {
        $response_show_detail = $this->SppApiservice->get_transaksi_byid($id);

        $kode_transaksi       = $response_show_detail['body']['data']['kode'] ?? '';
        if ($kode_transaksi != '') {
            $response_detail_item = $this->SppApiservice->get_detail_transaksi_bycode($kode_transaksi);
        } else {
            $response_detail_item = array();
        }

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'details' => $response_detail_item['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'details' => $response_detail_item,
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $data_transaksi = array();
        $data_transaksi['id_kelas_siswa'] = $request->id_siswa;
        $data_transaksi['tgl_bayar']      = $request->tgl_bayar;
        $data_transaksi['tahun_ajaran']   = $request->tahun_ajaran;

        $data_transaksi['nis']            = $request->nisn;
        $data_transaksi['id_sekolah']     = session('id_sekolah');

        $nominal    = str_replace(",", "", $request->nominal);
        $cekarray = array();

        if (is_array($request->id_tagihan)) {
            foreach ($request->id_tagihan as $key => $n) {
                $array = array();
                $array['id']         = $request->id_detail[$key];
                $array['id_tagihan'] = $n;
                $array['bulan']      = $request->bulan[$key];
                $array['nominal']    = $nominal[$key];
                $cekarray[] = $array;
            }

            $data_transaksi['details'] = $cekarray;
        }

        $response_show_detail = $this->SppApiservice->update_transaksi($id, json_encode($data_transaksi));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                    'bayar' => $request->bayar ?? '',
                    'kembalian' => $request->kembalian ?? ''
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                    'bayar' => $request->bayar ?? '',
                    'kembalian' => $request->kembalian ?? ''
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $response_show_detail = $this->SppApiservice->delete_transaksi($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    //'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_history_transaksi(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " title="lihat histori transaksi" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    //$button .= '<a  class="btn btn-warning btn-sm edit history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-success btn-sm jurnal history transaksi" title="Buat Jurnal"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-check"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print history transaksi" title="print histori transaksi "  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove history transaksi" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_keringanan_spp(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_keringan_spp();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    //$button .= '<a  class="btn btn-default btn-sm print history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove history transaksi" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_konfirmasi_spp(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_konfirmasi_spp();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show konfirmasi transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit konfirmasi transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print konfirmasi transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_paid_konfirmasi_spp(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_konfirmasi_spp_paid();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show konfirmasi transaksi " data-id="' . $data['id'] . '" data-tanggal="' . $data['tgl_bayar'] . '"  data-kode_trans="' . $data['kode'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_keringanan(Request $request)
    {
        $request->validate([
            'id_tagihan' => 'required',
            'nominal' => 'required',
            'tahun_ajaran' => 'required',
            'id_siswa' => 'required',
            'keterangan' => 'required',
            'nisn'      => 'required'
        ], [
            'tahun_ajaran.required' => 'Pilih Tahun ajaran terlebih dahulu, tidak boleh kosong!',
            'nominal.required' => 'Nominal tidak boleh kosong!',
            'id_siswa.required' => 'id siswa tidak boleh kosong!',
            'keterangan'        => 'keterangan tidak boleh kosong!'
        ]);

        $data = [
            'id_tagihan' => $request->id_tagihan,
            'id_kelas_siswa' => $request->id_siswa,
            'nominal' => str_replace(",", "", $request->nominal),
            'nisn' => $request->nisn,
            'tahun_ajaran' => $request->tahun_ajaran,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        ];

        $response_show_detail = $this->SppApiservice->post_keringananspp(json_encode($data));

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_siswa_search($id)
    {
        $response_show_detail = $this->SppApiservice->search_tagihan_by_nisn($id, session('tahun'));
        if (!empty($response_show_detail['body']['data'])) {
            $profil = $this->SppApiservice->search_siswa($response_show_detail['body']['data']['nisn'], session('tahun'));
        } else {
            $profil = array();
        }

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'profil' => $profil['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'profil' => $profil['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_siswa_search_by_tahun($id, Request $request)
    {
        $response_show_detail = $this->SppApiservice->siswaApi($id);
        $result = $response_show_detail['body']['data'] ?? array();
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-primary btn-sm create tagihan" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-plus"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });

            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_siswa_search_by_kelas($id, $tahun, Request $request)
    {
        $response_show_detail = $this->SppApiservice->search_siswa_by_kelas($id, $tahun);
        $result = $response_show_detail['body']['data'] ?? array();
        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= '<a  class="btn btn-primary btn-sm create tagihan" data-nis="' . $data['nis'] . '"  data-id="' . $data['id'] . '" data-id_kelas="' . $data['id_kelas'] . '" data-nama_kelas="' . $data['kelas'] . '"  data-jurusan="' . $data['jurusan'] . '" style="color: #fff"><i class="fa fa-plus"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('photo', function ($row) {
                return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
            });

            $table->rawColumns(['action', 'photo']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }


    /**
     * Display ajax  the specified resource.
     * print nota transaksi
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print_nota($id)
    {
        Session::put('title', 'Print Nota Transaksi');
        $detail_transaksi = $this->SppApiservice->get_transaksi_byid($id);
        $param = ['detail' => $detail_transaksi['body']['data'] ?? array()];
        return view('spp.components.v_print_detail_trans')->with($param);
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show_diskon_spp($id)
    {
        $response_show_detail = $this->SppApiservice->get_keringan_spp_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit_diskon_spp($id)
    {
        $response_show_detail = $this->SppApiservice->get_keringan_spp_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function update_keringanan(Request $request, $id)
    {
        $request->validate([
            'id_tagihan' => 'required',
            'nominal' => 'required',
            'tahun_ajaran' => 'required',
            'id_siswa' => 'required',
            'keterangan' => 'required',
            'nisn'      => 'required'
        ], [
            'tahun_ajaran.required' => 'Pilih Tahun ajaran terlebih dahulu, tidak boleh kosong!',
            'nominal.required' => 'Nominal tidak boleh kosong!',
            'id_siswa.required' => 'id siswa tidak boleh kosong!',
            'keterangan'        => 'keterangan tidak boleh kosong!'
        ]);

        $data = [
            'id_tagihan' => $request->id_tagihan,
            'id_kelas_siswa' => $request->id_siswa,
            'nominal' => str_replace(",", "", $request->nominal),
            'nisn' => $request->nisn,
            'tahun_ajaran' => $request->tahun_ajaran,
            'keterangan' => $request->keterangan,
            'id_sekolah' => session('id_sekolah'),
        ];

        $response_show_detail = $this->SppApiservice->update_keringananspp(json_encode($data), $id);

        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /** force download file
     ** @param $id
     */
    public function getdownload($id)
    {
        $download = $this->force_download_curl($id);
        if ($download['code'] == '200') {
            $file = public_path($download['filename']);
            $headers = array(
                'Content-Type:' . mime_content_type($file),
            );
            return response()->download($file, $download['filename'], $headers)->deleteFileAfterSend(true);
        } else {
            return back()->with('error', 'download Gagal');
        }
    }

    /**
     * curl file download
     * @param $url
     */

    private function force_download_curl($url)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $saveTo = basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }

    /**
     * Display a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function notifikasi_trans($id)
    {
        $notif = $this->SppApiservice->get_notifikasi_ortus($id);
        $sumnotif = $notif['body']['data'] ?? array();
        //dd($sumnotif);
        $sum_array = array();
        if (!empty($sumnotif)) {
            foreach($sumnotif as $key => $val){
                if ($val['dibaca'] == 0) {
                    array_push($sum_array,$sumnotif);
                }
            }
        }

        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => count($sum_array),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => count($sum_array),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function pesan_ortu()
    {
        Session::put('title', 'Pemberitahuan');
        $configApi  = $this->configApi();
        return view('spp.components.v_pesan_ortu')->with(array_merge($configApi));
    }

    /**
     * Ajax Data Pesan
     * * @param $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_pesanortu($id, Request $request)
    {
        $pengaturan =  $this->SppApiservice->get_notifikasi_ortus($id);

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    if($data['dibaca'] == '0'){
                        $button .= '<a  class="btn btn-primary btn-sm opentab" title="Pesan"   data-id="' . $data['id'] . '" data-id_kelas_siswa="' . $data['id_kelas_siswa'] . '" data-link="' . $data['url'] . '" style="color: #fff"><i class="fa fa-eye"></i></a>';
                    }else{
                        $button .= '-';
                    }
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('dibaca', function ($row) {
               switch($row['dibaca']){
                   case '1':
                    return 'sudah dibaca';
                    break;
                   case '0':
                    return 'belum dibaca';
               }

            });
            $table->rawColumns(['action', 'dibaca']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * update read status a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * * @param $id_kelas_siswa
     * @return \Illuminate\Http\Response
     */

    public function read_notifikasi_trans($id, $id_kelas_siswa)
    {
        $notif = $this->SppApiservice->read_notifikasi($id, $id_kelas_siswa);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }


    /**
     * Display a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function pesan_admin()
    {
        Session::put('title', 'Pemberitahuan');
        $configApi  = $this->configApi();
        return view('spp.components.v_pesan_admin')->with(array_merge($configApi));
    }

     /**
     * Display a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function notifikasiadmin_trans($id)
    {
        $notif = $this->SppApiservice->get_notifikasi_admin($id);
        $sumnotif = $notif['body']['data'] ?? array();
        if (!empty($sumnotif)) {
            foreach($sumnotif as $key => $val){
                if(!empty($val['id_kelas_siswa'])){
                    unset($sumnotif[$key]);
                }else if(empty($val['id_kelas_siswa']) && $val['dibaca2'] == 1){
                    unset($sumnotif[$key]);
                }
            }
        }

        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => count($sumnotif),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => count($sumnotif),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * update read status a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * * @param $id_kelas_siswa
     * @return \Illuminate\Http\Response
     */

    public function read_notifikasi_transadmin($id)
    {
        $notif = $this->SppApiservice->read_notifikasi_admin($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

     /**
     * Ajax Data Pesan
     * * @param $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_pesanadmin($id, Request $request)
    {
        $pengaturan =  $this->SppApiservice->get_notifikasi_admin($id);

        $result = $pengaturan['body']['data'] ?? array();

        foreach($result as $key => $val){
            if (!empty($val['id_kelas_siswa'])) {
                unset($result[$key]);
            }
        }

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    if($data['dibaca2'] == '0' && $data['dibaca'] == '0' && $data['id_kelas_siswa'] == 0){
                        $button .= '<a  class="btn btn-primary btn-sm opentab" title="Pesan"   data-id="' . $data['id'] . '" data-id_kelas_siswa="' . $data['id_kelas_siswa'] . '" data-link="' . $data['url'] . '" style="color: #fff"><i class="fa fa-eye"></i></a>';
                    }else{
                        $button .= '-';
                    }
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('dibaca', function ($row) {

                if($row['dibaca'] == '0' &&  $row['dibaca2'] == '0'){
                    return 'Belum dibaca';
                }else{
                    return 'Sudah dibaca';
                }
            });
            $table->rawColumns(['action', 'dibaca']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }
}
