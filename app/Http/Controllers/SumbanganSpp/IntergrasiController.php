<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;

class IntergrasiController extends Controller
{
    protected $SppApiservice;

    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('title', 'Pengaturan Intergrasi Pembayaran Spp Online ');
        $configApi  = $this->configApi();
        $id         = session('id_sekolah');
        if(!empty($id)){
            $payment_setting    = $this->SppApiservice->setting_environment_pembayaran('payment',$id);
            $notifikasi_setting = $this->SppApiservice->setting_environment_pembayaran('notification',$id);
            $parse_payment      = $payment_setting['body']['data'] ?? array();
            $parse_notifikasi   = $notifikasi_setting['body']['data'] ?? array();
        }
        $param      = [
            'payment_gateway' => $parse_payment,
            'notifikasi'      => $parse_notifikasi
        ];
        return view('spp.components.v_intergasi_index')->with(array_merge($configApi,$param));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = array();
        $data['kode'] = 'MIDTRANS_SERVER_KEY';
        $data['jenis'] = 'payment';
        $data['nama']  = 'Server Key';
        $data['token']  = $request->server_key_midtrans;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail1 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data));

        $data2 = array();
        $data2['kode'] = 'MIDTRANS_CLIENT_KEY';
        $data2['jenis'] = 'payment';
        $data2['nama']  = 'Server Key';
        $data2['token']  = $request->client_key_midtrans;
        $data2['id_sekolah'] = session('id_sekolah');

        $response_show_detail2 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data2));

        $data3 = array();
        $data3['kode'] = 'PRODUCTION';
        $data3['jenis'] = 'payment';
        $data3['nama']  = 'Server Key';
        $data3['token']  = $request->production;
        $data3['id_sekolah'] = session('id_sekolah');

        $response_show_detail3 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data3));

        $data4 = array();
        $data4['kode'] = 'PUSHER_APP_ID';
        $data4['jenis'] = 'notification';
        $data4['nama']  = 'App ID';
        $data4['token']  = $request->app_id_pusher;
        $data4['id_sekolah'] = session('id_sekolah');

        $response_show_detail4 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data4));

        $data5 = array();
        $data5['kode'] = 'PUSHER_APP_KEY';
        $data5['jenis'] = 'notification';
        $data5['nama']  = 'App Key';
        $data5['token']  = $request->app_key_pusher;
        $data5['id_sekolah'] = session('id_sekolah');

        $response_show_detail5 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data5));

        $data6 = array();
        $data6['kode'] = 'PUSHER_APP_SECRET';
        $data6['jenis'] = 'notification';
        $data6['nama']  = 'App Secret';
        $data6['token']  = $request->app_secret_pusher;
        $data6['id_sekolah'] = session('id_sekolah');

        $response_show_detail6 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data6));

        $data7 = array();
        $data7['kode'] = 'PUSHER_APP_CLUSTER';
        $data7['jenis'] = 'notification';
        $data7['nama']  = 'App Cluster';
        $data7['token']  = $request->app_cluster_pusher;
        $data7['id_sekolah'] = session('id_sekolah');

        $response_show_detail7 = $this->SppApiservice->setting_environment_pembayaranpost(json_encode($data7));

        if (  $response_show_detail1['code'] == '200'
             && $response_show_detail2['code'] == '200'
             &&
             $response_show_detail3['code'] == '200'
             &&
             $response_show_detail4['code'] == '200'
             &&
             $response_show_detail5['code'] == '200'
             &&
             $response_show_detail6['code'] == '200'
             &&
             $response_show_detail7['code'] == '200'
           ) {
            return response()->json(
                [
                    'message' => 'Update Successfull',
                    'data' => array_merge($data,$data2,$data3),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'Update Gagal',
                    'data' => '',
                    'info' => 'error',
                ]
            );
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
