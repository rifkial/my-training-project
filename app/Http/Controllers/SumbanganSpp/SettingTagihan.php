<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class SettingTagihan extends Controller
{

    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Pengaturan Tagihan SPP Online');
        $pos_pemasukan = $this->SppApiservice->get_data_pos();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $kelas        = $this->SppApiservice->get_data_kelas();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_kelas         = $kelas['body']['data'] ?? array();
        $parse_pemasukan = $pos_pemasukan['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = ['tahun_ajaran' => $parse_tahun_ajaran, 'kelas' => $parse_kelas, 'pemasukan' => $parse_pemasukan];
        return view('spp.components.v_spp_setting')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function template()
    {
        Session::put('title', 'Pengaturan SPP Online');
        $setting = $this->SppApiservice->setting_spp(session('id_sekolah'));
        $parse_setting = $setting['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['setting' => $parse_setting];
        return view('spp.components.setting_spp_rekening')->with(array_merge($configApi, $param));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function store_setting_spp(Request $request, $id)
    {
        $request->validate([
            'nama_bank' => 'required',
            'rekening_bank' => 'required',
            'atas_nama' => 'required'
        ], [
            'nama_bank.required' => 'Nama Bank tidak boleh kosong!',
            'rekening_bank.required' => 'Rekening bank tidak boleh kosong!',
            'atas_nama.required' => 'Atas Nama Pemilik tidak boleh kosong!'
        ]);

        $data = array();
        $data['nama_bank']     = $request->nama_bank;
        $data['rekening_bank'] = $request->rekening_bank;
        $data['atas_nama']     = $request->atas_nama;
        $data['id_sekolah']    = session('id_sekolah');

        $response_update = $this->SppApiservice->update_spp(json_encode($data), $id);

        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data(Request $request)
    {
        $pengaturan = $this->SppApiservice->setting_template();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting tagihan" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit setting tagihan"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove setting tagihan" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('bulan', function ($row) {
                /*$arraybulan = array();
                $bulan = $this->createMonth();
                $bulan_explode = explode(",", $row['bulan']);
                $bulan_ = collect(json_decode($bulan))->whereIn('id', $bulan_explode);
                $arraybulan[] = $bulan_->implode('nama', ', ');
                return $arraybulan;*/
                return $row['nama_bulan'] ?? '-';
            });

            $table->rawColumns(['action', 'nominal', 'bulan']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /** create month active now  */
    public function createMonth()
    {
        $nama_bulan = array();
        for ($i = 1; $i <= 12; $i++) {
            $x = array();
            $month = \Carbon\Carbon::today()->startOfMonth()->subMonth($i);
            $x['id'] = $month->month;
            $x['nama'] = $month->isoFormat('MMMM');
            $nama_bulan[] = $x;
        }
        return json_encode($nama_bulan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Session::put('title', 'Tambah Pengaturan Tagihan SPP Online');
        $pos_pemasukan = $this->SppApiservice->get_data_pos();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $kelas        = $this->SppApiservice->get_data_kelas();
        $parse_pemasukan = $pos_pemasukan['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_kelas         = $kelas['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
            'tahun_ajaran' => $parse_tahun_ajaran,
            'kelas' => $parse_kelas,
            'pemasukan' => $parse_pemasukan
        ];
        return view('spp.components.create_setting_tagihan')->with(
            array_merge($param, $configApi)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_tagihan(Request $request)
    {
        $request->validate([
            'id_pemasukan' => 'required',
            'kode' => 'required',
            'id_kelas' => 'required',
            'nama' => 'required',
            'periode' => 'required',
            'bulan' => 'required',
            'tahun_ajaran' => 'required',
            'nominal' => 'required',
        ], [
            'id_pemasukan.required' => 'Pilih Pemasukan terlebih dahulu, tidak boleh kosong!',
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
            'nominal.required' => 'Nominal tidak boleh kosong!',
            'bulan.required' => 'Bulan tidak boleh kosong,ilahkan pilih Periode terlebih dahulu!!',
            'tahun_ajaran.required' => 'Tahun ajaran tidak boleh kosong,ilahkan pilih tahun ajaran terlebih dahulu!!',
            'periode.required' => 'Periode tidak boleh kosong,ilahkan pilih Periode terlebih dahulu!!',
            'id_kelas.required' => 'Kelas tidak boleh kosong, silahkan pilih kelas terlebih dahulu!'
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['id_pos_pemasukan'] = $request->id_pemasukan;
        $data['nama']         = $request->nama;
        $data['id_kelas']     = $request->id_kelas;
        $data['periode']      = $request->periode;
        $data['bulan']        = implode(",", $request->bulan);
        $data['tahun_ajaran'] = $request->tahun_ajaran;
        $data['nominal']      = str_replace(",", "", $request->nominal);
        $data['id_sekolah']   = session('id_sekolah');
        $response_update = $this->SppApiservice->post_tagihan(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display ajax  the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function ajax_pemasukan($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_pemasukan_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$r                   = explode('|', $id);
        $response_show_detail = $this->SppApiservice->show_tagihan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response_show_detail = $this->SppApiservice->show_tagihan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($request->all());

        $request->validate([
            'id_pemasukan' => 'required',
            'kode' => 'required',
            'id_kelas' => 'required',
            'nama' => 'required',
            'periodeedit' => 'required',
            'bulanhidden' => 'required',
            'tahun_ajaran_edit' => 'required',
            'nominal' => 'required',
        ], [
            'id_pemasukan.required' => 'Pilih Pemasukan terlebih dahulu, tidak boleh kosong!',
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
            'nominal.required' => 'Nominal tidak boleh kosong!',
            'bulanhidden.required' => 'Bulan tidak boleh kosong,silahkan pilih Bulan terlebih dahulu!!',
            'tahun_ajaran_edit.required' => 'Tahun ajaran tidak boleh kosong,silahkan pilih tahun ajaran terlebih dahulu!!',
            'periodeedit.required' => 'Periode tidak boleh kosong,silahkan pilih Periode terlebih dahulu!!',
            'id_kelas.required' => 'Kelas tidak boleh kosong, silahkan pilih kelas terlebih dahulu!'
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['id_pos_pemasukan'] = $request->id_pemasukan;
        $data['nama']         = $request->nama;
        $data['id_kelas']     = $request->id_kelas;
        $data['periode']      = $request->periodeedit;
        $data['bulan']        = implode(",", (array) $request->bulanhidden);
        $data['tahun_ajaran'] = $request->tahun_ajaran_edit;
        $data['nominal']      = str_replace(",", "", $request->nominal);
        $data['id_sekolah']   = session('id_sekolah');

        $response_update = $this->SppApiservice->update_tagihan(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response_show_detail = $this->SppApiservice->remove_tagihan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_pos(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_data_posx();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting pos" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit setting pos"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove setting pos" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pos_index()
    {
        Session::put('title', 'Pengaturan POS Akun  SPP Online');
        $configApi  = $this->configApi();
        return view('spp.components.v_pos_index')->with(array_merge($configApi));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_pos($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_by_id($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_pos($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_by_id($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_pos(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
        ], [
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['nama']         = $request->nama;
        $data['id_sekolah']   = session('id_sekolah');
        $response_update = $this->SppApiservice->post_data_pos(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function store_pos_update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
        ], [
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['nama']         = $request->nama;
        $data['id_sekolah']   = session('id_sekolah');
        $response_update = $this->SppApiservice->update_data_pos(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_pos($id)
    {
        $response_show_detail = $this->SppApiservice->remove_pos_by_id($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create_pos()
    {
        Session::put('title', 'Tambah Pengaturan POS SPP Online');
        $configApi  = $this->configApi();
        return view('spp.components.v_create_pos')->with(array_merge($configApi));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function target_pos()
    {
        Session::put('title', ' Pengaturan Target POS SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ??  array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
        return view('spp.components.v_target_pos')->with(array_merge($param, $configApi));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create_target()
    {
        Session::put('title', 'Tambah Pengaturan Target SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $pos_data     = $this->SppApiservice->get_data_pos();
        $parse_pos    = $pos_data['body']['data'] ?? array();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ??  array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran, 'pos' => $parse_pos];
        return view('spp.components.v_create_target')->with(array_merge($param, $configApi));
    }


    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_target(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_data_pos_pemasukanx(session('tahun'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting pos target" title=" Lihat Pos Pemasukan" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit setting pos target" title="Edit Pos Pemasukan"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '<a  class="btn btn-success btn-sm real setting pos target" title="Realisasi Anggaran Pemasukan" data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-check"></i> </a>&nbsp;';
                    //$button .= ' <button type="button" class="btn btn-danger btn-sm remove setting pos target" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('target', function ($row) {
                return number_format($row['target'], 0);
            });

            $table->editColumn('realisasi', function ($row) {
                return number_format($row['realisasi'], 0) ?? '-';
            });

            $table->rawColumns(['action', 'target']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_target($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_pemasukan_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_target($id)
    {
        $response_show_detail = $this->SppApiservice->get_data_pos_pemasukan_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function store_target_update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'target' => 'required',
            'tahun_ajaran' => 'required'
        ], [
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
            'target.required' => 'Target nominal tidak boleh kosong!',
            'tahun_ajaran.required' => 'Tahun AJaran tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['nama']         = $request->nama;
        $data['tahun_ajaran'] = $request->tahun_ajaran;
        $data['id_pos']      = $request->id_pos;
        $data['target']       = str_replace(',', "", $request->target);
        $data['id_sekolah']   = session('id_sekolah');

        //dd($data);

        $response_update = $this->SppApiservice->update_data_pos_pemasukan(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data' => $response_update['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    //'data' => $response_update['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     * @return \Illuminate\Http\Response
     */

    public function store_target_realisasi(Request $request, $id){
        $request->validate([
            'realisasi' => 'required',
        ], [
            'realisasi.required' => 'realisasi tidak boleh kosong!',
        ]);

        $data = array();
        $data['realisasi'] = str_replace(',', "", $request->realisasi);

        $response_update = $this->SppApiservice->update_data_pos_pemasukanreal(json_encode($data), $id);
        if ($response_update['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                    'data' => $response_update['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_update['body']['message'],
                     'data' => $response_update['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_target($id)
    {
        $response_show_detail = $this->SppApiservice->remove_pos_pemasukan($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_target_post(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'target' => 'required',
            'tahun_ajaran' => 'required'
        ], [
            'kode.required' => 'Kode tidak boleh kosong!',
            'target.required' => 'Target nominal tidak boleh kosong!',
            'tahun_ajaran.required' => 'Tahun AJaran tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode']         = $request->kode;
        $data['nama']         = $request->nama;
        $data['tahun_ajaran'] = $request->tahun_ajaran;
        $data['id_pos']       = $request->id_pos;
        $data['target']       = str_replace(',', "", $request->target);
        $data['id_sekolah']   = session('id_sekolah');

        $response_update = $this->SppApiservice->post_data_pos_pemasukan(json_encode($data));
        if ($response_update['code'] == '200') {
            return back()->with('success', $response_update['body']['message']);
        } else {
            return back()->with('error', $response_update['body']['message']);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function setting_Pesan()
    {
        Session::put('title', 'Pengaturan Pesan SPP Online');
        $configApi  = $this->configApi();
        return view('spp.components.setting_spp_pesan')->with(array_merge($configApi));
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_settingpesan(Request $request)
    {
        $pengaturan = $this->SppApiservice->setting_pesan();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show setting pesan" data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= ' <button type="button" class="btn btn-danger btn-sm remove setting pesan" data-id="' . $data['id'] . '"><i class="fa fa-trash-o"></i></button>';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('isi', function ($row) {
                return Str::limit($row['isi'], 150, '...');
            });

            $table->editColumn('perihal', function ($row) {
                switch ($row['perihal']) {
                    case '':
                        return '-';
                        break;
                    default:
                        return $row['perihal'] ?? '-';
                }
            });

            $table->editColumn('status', function ($row) {
                if ($row['status'] == '1') {
                    return '<i class="fa fa-eye"></i>';
                } else if ($row['status'] == '0') {
                    return '<i class="fa fa-eye-slash"></i>';
                }
            });

            $table->rawColumns(['action', 'isi', 'perihal', 'status']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_settingpesan($id)
    {
        $response_show_detail = $this->SppApiservice->setting_pesan_byid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_settingpesan($id)
    {
        $response_show_detail = $this->SppApiservice->setting_pesan_byremoveid($id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_setting_pesan(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
            'perihal' => 'required',
            'salampembuka' => 'required',
            'isi' => 'required',
            'salampenutup' => 'required',
            'atas_nama' => 'required',
            'kode_pesan.required',
        ], [
            'kategori.required' => 'Kategori tidak boleh kosong!',
            'perihal.required' => 'Perihal  tidak boleh kosong!',
            'salampembuka.required' => 'Salam Pembuka tidak boleh kosong!',
            'isi.required' => 'Isi Pesan tidak boleh Kosong!',
            'salampenutup.required' => 'Salam Penutup tidak boleh kosong',
            'atas_nama.required' => 'Atas Nama tidak boleh kosong!',
            'kode_pesan.required' => 'Kode pesan tidak boleh kosong'
        ]);

        $data = array();
        $data['kode']          = $request->kode_pesan;
        $data['perihal']       = $request->perihal;
        $data['kategori']      = $request->kategori;
        $data['salam_pembuka'] = $request->salampembuka;
        $data['isi']           =  $request->isi;
        $data['salam_penutup'] = $request->salampenutup;
        $data['atas_nama']     = $request->atas_nama;
        $data['id_sekolah']    = session('id_sekolah');
        $response_show_detail = $this->SppApiservice->setting_pesan_post(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'info' => 'error',
                ]
            );
        }
    }
}
