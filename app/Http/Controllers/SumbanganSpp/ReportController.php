<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{
    protected $SppApiservice;
    /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Session::put('title', 'Laporan Tagihan SPP ');
        if (empty($request->id_rombel) && empty($request->tahun_ajaran)) {
            $tahun = session('tahun');
            $sample = $this->SppApiservice->report_tagihan('1', $tahun);
        } else {
            $tahun_get = explode("/", $request->tahun_ajaran);
            $tahun = $tahun_get[0];
            $sample = $this->SppApiservice->report_tagihan($request->id_rombel, $tahun);
        }
        $datapos = $this->SppApiservice->get_data_pos();
        $data_rombel = $this->SppApiservice->rombel_data();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_rombel = $data_rombel['body']['data'] ?? array();
        $parse_pos = $datapos['body']['data'] ?? array();
        $data_sample = $sample['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = [
            'report_tagihan' => $data_sample,
            'pos' => $parse_pos,
            'rombelx' => $parse_rombel,
            'tahun_ajaran' => $parse_tahun,
            'request_tahun' => $tahun,
            'request_rombel' => $request->id_rombel ?? '1'
        ];
        return view('spp.components.v_report_tagihan')->with(array_merge($configApi, $param));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function print_report_tagihan($id, $tahun, $name_kelas, $name_tahun)
    {
        Session::put('title', 'Print Laporan Tagihan SPP ');
        $sample = $this->SppApiservice->report_tagihan($id, $tahun);
        $datapos = $this->SppApiservice->get_data_pos();
        $data_rombel = $this->SppApiservice->rombel_data();
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_rombel = $data_rombel['body']['data'] ?? array();
        $parse_pos = $datapos['body']['data'] ?? array();
        $data_sample = $sample['body']['data'] ?? array();
        $parse_tahun = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param = [
            'report_tagihan' => $data_sample,
            'pos' => $parse_pos,
            'rombelx' => $parse_rombel,
            'tahun_ajaran' => $parse_tahun,
            'nama_kelas' => $name_kelas,
            'nama_tahun_ajaran' => $name_tahun
        ];
        return view('spp.components.v_report_tagihan_print')->with(array_merge($configApi, $param));
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_history_transaksi(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi_siswa_id(session('id'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

      /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_history_transaksiortu(Request $request)
    {
        $pengaturan = $this->SppApiservice->get_transaksi_siswa_id(session('id_kelas_siswa'));

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show history transaksi " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-default btn-sm print history transaksi"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-print"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->editColumn('nominal', function ($row) {
                return number_format($row['nominal'], 0);
            });

            $table->editColumn('tgl_bayar', function ($row) {
                return \Carbon\Carbon::parse($row['tgl_bayar'])->isoFormat('dddd, D MMMM Y');
            });

            $table->rawColumns(['action', 'nominal', 'tgl_bayar']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_transaksi()
    {
        Session::put('title', 'Histori Transaksi SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
        return view('spp.components.v_transaksi_history_siswa')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_transaksiortu()
    {
        Session::put('title', 'Histori Transaksi SPP Online');
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['tahun_ajaran' => $parse_tahun_ajaran];
        return view('spp.components.v_transaksi_history_ortu')->with(array_merge($param, $configApi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tagihan_siswa_bulan()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data];
        return view('spp.components.v_tagihan_siswa')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tagihan_ortu_bulan()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id_kelas_siswa'), $bulan);
        $data_ortu  = $this->SppApiservice->get_data_ortu(session('id_kelas_siswa'));
        $data_rekening = $this->SppApiservice->rekeningspp(session('id_sekolah'));
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $parse_datax = $data_ortu['body']['data'] ?? array();
        $parse_rekening = $data_rekening['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data,'ortu'=>$parse_datax,'rekening'=>$parse_rekening];
        return view('spp.components.v_tagihan_ortu')->with(array_merge($configApi, $param));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function print_tagihan_siswa()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data];
        return view('spp.components.v_tagihan_siswa_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function print_tagihan_ortu()
    {
        $bulan =  \Carbon\Carbon::now()->isoFormat('MMMM');
        Session::put('title', 'Tagihan Bulan ' . $bulan);
        $tagihanspp = $this->SppApiservice->search_tagihan_by_bulan(session('id'), $bulan);
        $data_ortu  = $this->SppApiservice->get_data_ortu(session('id_kelas_siswa'));
        $parse_data = $tagihanspp['body']['data'] ?? array();
        $parse_datax = $data_ortu['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['bulan' => $bulan, 'tagihan' => $parse_data,'ortu'=>$parse_datax];
        return view('spp.components.v_tagihan_ortu_print')->with(array_merge($configApi, $param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function report_realiasi(){
         $tahun  = session('tahun');
         Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN '.$tahun);
         $bulan  = '';
         $realisasi = $this->SppApiservice->report_realisasi($tahun);
         $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
         $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
         $parse_data= $realisasi['body']['data'] ?? array();
         $configApi  = $this->configApi();
         $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
         $param      = ['realisasi' => $parse_data,'list_bulan'=>$list_bulan,'tahun_ajaran'=>$parse_tahun_ajaran,'request_tahun'=>$tahun,'request_bulan'=>$bulan];
         return view('spp.components.realisasi_anggaran')->with(array_merge($configApi,$param));
     }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasiTahun($tahun){
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN '.$tahun);
        $bulan  = '';
        $realisasi = $this->SppApiservice->report_realisasi($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data= $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data,'list_bulan'=>$list_bulan,'tahun_ajaran'=>$parse_tahun_ajaran,'request_tahun'=>$tahun,'request_bulan'=>$bulan];
        return view('spp.components.realisasi_anggaran')->with(array_merge($configApi,$param));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasiTahunprint($tahun){
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE TAHUN '.$tahun);
        $bulan  = '';
        $realisasi = $this->SppApiservice->report_realisasi($tahun);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data= $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $list_bulan = ['Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'];
        $param      = ['realisasi' => $parse_data,'list_bulan'=>$list_bulan,'tahun_ajaran'=>$parse_tahun_ajaran,'request_tahun'=>$tahun,'request_bulan'=>$bulan];
        return view('spp.components.realisasi_anggaran_print')->with(array_merge($configApi,$param));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_filter($tahun,$bulan){
        $list_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $realisasi = $this->SppApiservice->report_realisasi_filter($tahun,$list_bulan[intval($bulan)-1]);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data= $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
                        'realisasi' => $parse_data,
                        'list_bulan'=>$list_bulan,
                        'tahun_ajaran'=>$parse_tahun_ajaran,
                        'request_tahun'=>$tahun,
                        'request_bulan'=>$bulan,
                        'nama_bulan'  => $list_bulan[intval($bulan)-1]
                    ];
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE BULAN '.ucwords($list_bulan[intval($bulan)-1]).' TAHUN '.$tahun);
        return view('spp.components.realisasi_anggaran_filter')->with(array_merge($configApi,$param));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_realiasi_filterprint($tahun,$bulan){
        $list_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $realisasi = $this->SppApiservice->report_realisasi_filter($tahun,$list_bulan[intval($bulan)-1]);
        $tahun_ajaran = $this->SppApiservice->get_tahun_ajaran();
        $parse_tahun_ajaran = $tahun_ajaran['body']['data'] ?? array();
        $parse_data= $realisasi['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = [
                        'realisasi' => $parse_data,
                        'list_bulan'=>$list_bulan,
                        'tahun_ajaran'=>$parse_tahun_ajaran,
                        'request_tahun'=>$tahun,
                        'request_bulan'=>$bulan,
                        'nama_bulan'  => $list_bulan[intval($bulan)-1]
                    ];
        Session::put('title', 'LAPORAN REALISASI PENERIMAAN ANGGARAN PERIODE BULAN '.ucwords($list_bulan[intval($bulan)-1]).' TAHUN '.$tahun);
        return view('spp.components.realisasi_anggaran_filter_print')->with(array_merge($configApi,$param));
    }
}
