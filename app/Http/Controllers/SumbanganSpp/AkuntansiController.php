<?php

namespace App\Http\Controllers\SumbanganSpp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\SumbanganSpp\SppApiservice;
use Illuminate\Support\Facades\Session;
use App\Helpers\Help;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class AkuntansiController extends Controller
{
    protected $SppApiservice;

     /**
     * __construct function new class Api
     */

    public function __construct()
    {
        $this->SppApiservice = new SppApiservice();
    }

    /** config api server key && secret key api */
    public function configApi()
    {
        $seetingApi = $this->SppApiservice->intergration_api(session('id_sekolah'));
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $MIDTRANS_CLIENT_KEY = '';
        $PUSHER_APP_KEY      = '';
        $PUSHER_APP_CLUSTER  = '';
        $MIDTRANS_PRODUCTION = '';

        if (!empty($parseApi)) {
            foreach ($parseApi as $key => $val) {
                if ($val['jenis'] == 'payment') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'MIDTRANS_CLIENT_KEY') {
                            $MIDTRANS_CLIENT_KEY = $val['token'];
                        } else if ($val['kode'] == 'PRODUCTION') {
                            $MIDTRANS_PRODUCTION  =  $val['token'];
                        }
                    }
                } else if ($val['jenis'] == 'notification') {
                    if ($val['token'] != '-' && $val['status'] == '1') {
                        if ($val['kode'] == 'PUSHER_APP_KEY') {
                            $PUSHER_APP_KEY = $val['token'];
                        } else if ($val['kode'] == 'PUSHER_APP_CLUSTER') {
                            $PUSHER_APP_CLUSTER = $val['token'];
                        }
                    }
                }
            }
        }

        $param = [
            'MIDTRANS_CLIENT_KEY' => $MIDTRANS_CLIENT_KEY,
            'PUSHER_APP_KEY' => $PUSHER_APP_KEY,
            'PUSHER_APP_CLUSTER' => $PUSHER_APP_CLUSTER,
            'MIDTRANS_PRODUCTION' => $MIDTRANS_PRODUCTION
        ];

        return $param;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        Session::put('title', 'Akun Kategori');
        $configApi  = $this->configApi();
        $param      = [];
        return view('spp.components.v_akun_kategori_index')->with(array_merge($configApi,$param));
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexsub()
    {
        //
        Session::put('title', 'Akun Sub Kategori');
        $kategori_akun = $this->SppApiservice->akuntansi_kategori();
        $parse_kategori = $kategori_akun['body']['data'] ?? array();
        $configApi  = $this->configApi();
        $param      = ['kategori'=>$parse_kategori];
        return view('spp.components.v_akun_subkategori_index')->with(array_merge($configApi,$param));
    }

    /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_kategori(Request $request)
    {
        $pengaturan = $this->SppApiservice->akuntansi_kategori();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi kategori " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi kategori"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

       /**
     * Ajax Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function ajax_data_subkategori(Request $request)
    {
        $pengaturan = $this->SppApiservice->akuntansi_subkategori();

        $result = $pengaturan['body']['data'] ?? array();

        $size = 0; // size counter
        $chunkno = 1; // chunk number
        $maxbytes = 50000; // 50000-byte chunks
        $chunks = []; // for array chunks

        foreach ($result as $set) {
            // if over the limit, move on to next chunk
            if ($size > $maxbytes) {
                $size = 0;
                $chunkno++;
            }
            $size += strlen(json_encode($set)) + 1; // add a comma's length!
            $chunks[] = $set;
        }

        if ($request->ajax()) {
            $table = datatables()->of($chunks)
                ->addColumn('action', function ($data) {
                    $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                    $button .= ' <button type="button" class="btn btn-info btn-sm show akuntansi subkategori " data-id="' . $data['id'] . '"><i class="fa fa-eye"></i></button>&nbsp;';
                    $button .= '<a  class="btn btn-warning btn-sm edit akuntansi subkategori"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>&nbsp;';
                    $button .= '</div>';
                    return $button;
                });

            $table->rawColumns(['action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_kategori(Request $request)
    {
        //
        $request->validate([
            'kodepost' => 'required',
            'namapost' => 'required'
        ],[
            'kodepost.required' => 'Kode tidak boleh kosong!',
            'namapost.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode'] = $request->kodepost;
        $data['nama'] = $request->namapost;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->post_akuntansi_kategori(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }


    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_subkategori(Request $request)
    {
        $request->validate([
            'id_kategori' => 'required',
            'namapost' => 'required'
        ],[
            'id_kategori.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'namapost.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['id_akun_kategori'] = $request->id_kategori;
        $data['nama'] = $request->namapost;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->post_akuntansi_subkategori(json_encode($data));
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display Akun Kategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_kategori($id)
    {
        $notif = $this->SppApiservice->show_akuntansi_kategori($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Display Akun subKategori the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_subkategori($id)
    {
        $notif = $this->SppApiservice->show_akuntansi_subkategori($id);
        if ($notif['code'] == '200') {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $notif['body']['message'],
                    'data' => $notif['body']['data'] ?? array(),
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_kategori(Request $request, $id)
    {
        //
        $request->validate([
            'kode' => 'required',
            'nama' => 'required'
        ],[
            'kode.required' => 'Kode tidak boleh kosong!',
            'nama.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['kode'] = $request->kode;
        $data['nama'] = $request->nama;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->update_akuntansi_kategori(json_encode($data),$id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_subkategori(Request $request, $id)
    {
        $request->validate([
            'id_kategori' => 'required',
            'nama' => 'required'
        ],[
            'id_kategori.required' => 'Pilih terlebih dahulu Akun Kategori !',
            'nama.required' => 'Nama tidak boleh kosong!',
        ]);

        $data = array();
        $data['id_akun_kategori'] = $request->id_kategori;
        $data['nama'] = $request->nama;
        $data['id_sekolah'] = session('id_sekolah');

        $response_show_detail = $this->SppApiservice->update_akuntansi_subkategori(json_encode($data),$id);
        if ($response_show_detail['code'] == '200') {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'success',
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => $response_show_detail['body']['message'],
                    'data' => $response_show_detail['body']['data'],
                    'info' => 'error',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
