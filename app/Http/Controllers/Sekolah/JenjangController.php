<?php

namespace App\Http\Controllers\Sekolah;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiService\Sekolah\JenjangApi;

class JenjangController extends Controller
{
    
    public $jenjangApi;

    public function __construct()
    {
        $this->jenjangApi = new JenjangApi("api/data/mutu/jenjang");
    }

    public function index(Request $request)
    {
        $dataJenjang = $this->jenjangApi->get_by_sekolah();
        $dataJenjang = $dataJenjang['body']['data'];
        
        if ($request->ajax()) {
            $table = datatables()->of($dataJenjang);
            $table->addIndexColumn();
            
            $table->editColumn('aksi', function ($row) {
                
                return '<div class="input-group">
                    <a onclick="ShowModal('.$row['id'].')" class="btn btn-success btn-sm mt-0">
                        <i class="fa fa-pencil ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Delete('.$row['id'].')" id="delJenjang" class="btn btn-danger btn-sm mx-1">
                        <i class="fa fa-trash-o ml-1" aria-hidden="true"></i>
                    </a>
                    <a onclick="Detail('.$row['id'].')" class="btn btn-secondary btn-sm">
                        <i class="fa fa-info ml-2" aria-hidden="true"></i>
                    </a>
                </div>';
            });

            
            $table->rawColumns(['aksi']);

            return $table->make(true);
        }

        return view('content.penjamin_mutu.master.jenjang')->with([]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $kode = $request->kode;
        $id_sekolah = 1;
        $nama = $request->nama;

        $data = array(
            "id_sekolah" => 1,
            "kode" => $kode,
            "nama" => $nama,
        );

        $jenjang = $this->jenjangApi->create(json_encode($data));
        
        if ($jenjang['code'] == 200) {
            return response()->json([
                'message' => $jenjang['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $jenjang['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
