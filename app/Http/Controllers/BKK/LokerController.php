<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\AgendaApi;
use App\ApiService\BKK\BidangLokerApi;
use App\ApiService\BKK\IndustriApi;
use App\ApiService\BKK\LokerApi;
use App\ApiService\BKK\LokerPelamarApi;
use App\ApiService\Master\IndonesiaApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\RequestStack;
use XcS\XcTools;

class LokerController extends Controller
{
    private $lokerApi;
    private $industriApi;
    private $bidangLokerApi;
    private $indonesiaApi;
    private $hashids;
    private $pelamarApi;
    private $agendaApi;
    private $profileApi;

    public function __construct()
    {
        $this->lokerApi = new LokerApi();
        $this->industriApi = new IndustriApi();
        $this->bidangLokerApi = new BidangLokerApi();
        $this->indonesiaApi = new IndonesiaApi();
        $this->profileApi = new ProfileApi();
        $this->pelamarApi = new LokerPelamarApi();
        $this->agendaApi = new AgendaApi();
        $this->hashids = new Hashids();
    }

    public function index()
    {
        if (session('role') == 'bkk-perusahaan') {
            $lokers = $this->lokerApi->get_by_auth_perusahaan();
            // dd($lokers);
            $lokers = $lokers['body']['data'];
            $loker = [];
            foreach ($lokers as $lok) {
                // dd($this->pelamarApi->lamaran_terkirim($lok['id']));
                $loker[] = array(
                    'pelamar' => $lok['pelamar'],
                    'tgl_tutup' => $lok['tgl_tutup'],
                    'jumlah_wawancara' => !empty($this->pelamarApi->lamaran_proses_wawancara($lok['id'])['body']['data']) ? count($this->pelamarApi->lamaran_proses_wawancara($lok['id'])['body']['data']) : 0,
                    'jumlah_terpilih' => !empty($this->pelamarApi->lamaran_proses_terpilih($lok['id'])['body']['data']) ? count($this->pelamarApi->lamaran_proses_terpilih($lok['id'])['body']['data']) : 0,
                    'jumlah_terkirim' => !empty($this->pelamarApi->lamaran_terkirim($lok['id'])['body']['data']) ? count($this->pelamarApi->lamaran_terkirim($lok['id'])['body']['data']) : 0,
                    'judul' => $lok['judul'],
                    'id' => $lok['id'],
                    'id_code' => $this->hashids->encode($lok['id']),
                );
            }
            return view('content.bkk.dashboard.bkk-perusahaan.v_lowongan_revisi')->with(['loker' => $loker]);
        } else {
            $loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
            // dd($loker);
            $loker = $loker['body']['data'];
            // dd($loker);
            $industri = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
            $industri = $industri['body']['data'];
            $provinsi = $this->indonesiaApi->get_provinsi();
            $provinsi = $provinsi['body']['data'];
            $bidang = $this->bidangLokerApi->sekolah_tanpa_login(session('id_sekolah'));
            $bidang = $bidang['body']['data'];
            if (session('role') == 'admin') {
                return view('content.admin.bkk.v_lowongan')->with(['loker' => $loker, 'provinsi' => $provinsi, 'bidang' => $bidang, 'industri' => $industri, 'template' => 'default']);
            } elseif (session('role') == "supervisor") {
                return view('content.bkk.dashboard.bkk-admin.v_lowongan')->with(['loker' => $loker, 'provinsi' => $provinsi, 'bidang' => $bidang, 'industri' => $industri, 'template' => session('template')]);
            }
            return view('content.bkk.dashboard.' . session('role') . '.v_lowongan')->with(['loker' => $loker, 'provinsi' => $provinsi, 'bidang' => $bidang, 'industri' => $industri, 'template' => 'default']);
        }
    }

    public function create()
    {
        if (session('role') == 'bkk-admin' || session('role') == 'admin') {
            $perusahaan = $this->industriApi->sekolahWithoutLogin(session('id_sekolah'));
        } else {
            $perusahaan = $this->industriApi->get_by_user(session('id'));
        }
        // dd($perusahaan);
        $perusahaan = $perusahaan['body']['data'];
        $provinsi = $this->indonesiaApi->get_provinsi();
        $provinsi = $provinsi['body']['data'];
        $bidang = $this->bidangLokerApi->sekolah_tanpa_login(session('id_sekolah'));
        // dd($bidang);
        $bidang = $bidang['body']['data'];
        if (session('role') == 'admin') {
            return view('content.admin.bkk.create_lowongan')->with(['bidang' => $bidang, 'provinsi' => $provinsi, 'perusahaan' => $perusahaan]);
        }
        return view('content.bkk.dashboard.' . session('role') . '.create_lowongan')->with(['bidang' => $bidang, 'provinsi' => $provinsi, 'perusahaan' => $perusahaan]);
        // return view('content.bkk.dashboard.'.session('role').'.v_create_lowongan')->with(['bidang' => $bidang, 'provinsi' => $provinsi, 'perusahaan' => $perusahaan]);
    }

    public function deskIndustri($id)
    {
        // dd($id);
        $loker = $this->lokerApi->get_by_id($this->hashids->decode($id)[0]);
        // dd($loker);
        $loker = $loker['body']['data'];
        $loker['id_code'] = $id;
        return view('content.bkk.dashboard.' . session('role') . '.create_next_lowongan')->with(['loker' => $loker]);
        // return view('content.bkk.dashboard.'.session('role').'.v_create_lowongan')->with(['bidang' => $bidang, 'provinsi' => $provinsi, 'perusahaan' => $perusahaan]);
    }

    public function store(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id_industri' => $request['id_industri'],
            'id_bidang_loker' => $request['id_bidang_loker'],
            'judul' => strtolower($request['judul']),
            'deskripsi' => $request['deskripsi'],
            'link' => $request['link'],
            'quota' => $request['quota'],
            'syarat' => $request['syarat'],
            'recruitment' => $request['recruitment'],
            'gaji_awal' => str_replace(',', '', $request['gaji_awal']),
            'gaji_sampai' => str_replace(',', '', $request['gaji_sampai']),
            'tgl_buka' => date('Y-m-d', strtotime($request['tgl_buka'])),
            'tgl_tutup' => date('Y-m-d', strtotime($request['tgl_tutup'])),
            'id_sekolah' => session('id_sekolah'),
            'status' => 1,
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->lokerApi->create_file(json_encode($data));
            File::delete($path);
            // $first_id = $result['data'];
        } else {
            $result = $this->lokerApi->create(json_encode($data));
        }


        if ($result['code'] == 200) {
            $first_id = $result['body']['data'];
            if (session('role') == 'bkk-admin' || session('role') == 'admin') {
                $id_code = $this->hashids->encode($first_id);
                $shareComponent = \Share::page(
                    url("/bursa_kerja/detail_loker/{$id_code}"),
                    'Your share text comes here'
                )
                    ->facebook()
                    ->twitter()
                    ->linkedin()
                    ->telegram()
                    ->whatsapp()
                    ->reddit();
                $html = '<div id="isi_sosial">
                        ' . $shareComponent . '
                        </div>';
                return response()->json([
                    'message' => $result['body']['message'],
                    'success'  => 'success',
                    'status' => 'berhasil',
                    'share' => $html,
                    'link_url' => url("/bursa_kerja/detail_loker/{$id_code}"),
                ]);
            }
        } else {
            return response()->json(['message' => $result['body']['message'], 'success'  => 'error', 'status' => 'gagal']);
        }
    }

    public function update_deskripsi(Request $request)
    {
        if (session('role') == 'bkk-perusahaan') {
            $data_loker['deskripsi_perusahaan'] = $request['deskripsi_perusahaan'];
            $result = $this->lokerApi->update_deskripsi(json_encode($data_loker), $this->hashids->decode($request['id_loker'])[0]);
            // dd($result);
            if ($result['code'] == 200) {
                $id_code = $this->hashids->encode($result['body']['data']);
                return response()->json([
                    'success' => "success",
                    'message' => $result['body']['message'],
                    'status' => 'berhasil',
                    'id_code' => $id_code,
                ]);
            } else {
                return response()->json([
                    'success' => "error",
                    'message' => $result['body']['message'],
                    'status' => 'gagal'
                ]);
            }
        }
    }

    public function finish($id_code)
    {
        if (session('role') == 'bkk-perusahaan') {
            $loker = $this->lokerApi->get_by_id($this->hashids->decode($id_code)[0]);
            // dd($loker);
            $loker = $loker['body']['data'];
            $profile = $this->profileApi->get_profile();
            // dd($profile);
            $profile = $profile['body']['data'];
            return view('content.bkk.dashboard.' . session('role') . '.finish_step_lowongan')->with(['loker' => $loker, 'profile' => $profile]);
        }
    }

    public function edit($id)
    {
        // dd($id);
        $loker = $this->lokerApi->get_by_id($this->hashids->decode($id)[0]);
        // dd($loker);
        $loker = $loker['body']['data'];
        $bidang = $this->bidangLokerApi->get_all();
        $bidang = $bidang['body']['data'];
        return view('content.bkk.dashboard.' . session('role') . '.create_lowongan')->with(['loker' => $loker, 'bidang' => $bidang]);
    }

    public function update(Request $request)
    {
        if ($request['aksi_update']) {
            $loker = $this->lokerApi->get_by_id($request['id']);
            // dd($loker);
            $loker = $loker['body']['data'];
            $data_loker = $loker;
            $data_loker['id_bidang_loker'] = $loker['id_bidang'];
            unset($data_loker['tgl_buka']);
            unset($data_loker['tgl_tutup']);
            $data_loker['tgl_buka'] = date('Y-m-d', strtotime($request['tgl_buka']));
            $data_loker['tgl_tutup'] = date('Y-m-d', strtotime($request['tgl_tutup']));
            $data_loker['id_sekolah'] = session('id_sekolah');
            $data_loker['status'] = 1;
        } else {
            $data_loker = array(
                'id' => $request['id'],
                'id_industri' => session('id_industri'),
                'id_bidang_loker' => $request['id_bidang_loker'],
                'judul' => strtolower($request['judul']),
                'deskripsi' => $request['deskripsi'],
                'syarat' => $request['syarat'],
                'quota' => $request['quota'],
                'gaji_awal' => str_replace(',', '', $request['gaji_awal']),
                'gaji_sampai' => str_replace(',', '', $request['gaji_sampai']),
                'tgl_buka' => date('Y-m-d', strtotime($request['tgl_buka'])),
                'tgl_tutup' => date('Y-m-d', strtotime($request['tgl_tutup'])),
                'id_sekolah' => session('id_sekolah'),
                'status' => 0,
            );
        }
        $result = $this->lokerApi->create(json_encode($data_loker));
        if ($result['code'] == 200) {
            $id_code = $this->hashids->encode($result['body']['data']);
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil',
                'id_code' => $id_code,
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->lokerApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function hard_delete($id)
    {
        $delete = $this->bidangLokerApi->delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function data_trash()
    {
        $jurusan = $this->bidangLokerApi->all_trash();
        $result = $jurusan['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function restore($id)
    {
        $restore = $this->bidangLokerApi->restore($id);
        if ($restore['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $restore['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $restore['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_by_sekolah_datatable()
    {
        if (session('role') == 'bkk-pelamar') {
            $loker = $this->lokerApi->get_by_auth_pelamar();
            $result = $loker['body']['data'];
        } else {
            $loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
            $result = $loker['body']['data'];
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                if (session('role') == 'bkk-pelamar') {
                    if ($data['status_pelamaran'] != 0) {
                        $button = '<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit" class="edit lamar-' . $data['id'] . ' btn btn-primary btn-xs edit" disabled><i class="fa fa-check"></i> Anda sudah melamar</a>';
                    } else {
                        $button = '<a href="javascript:void(0)" data-toggle="tooltip"  onclick="lamarLoker(' . $data['id'] . ')" data-original-title="Edit" class="edit lamar-' . $data['id'] . ' btn btn-success btn-xs edit"><i class="fa fa-send-o"></i> Lamar sekarang</a>';
                    }
                } else {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  onclick="lamarLoker(' . $data['id'] . ')" data-original-title="Edit" class="edit lamar-' . $data['id'] . ' btn btn-success btn-xs edit"><i class="fa fa-send-o"></i> Lamar sekarang</a>';
                }
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" class="btn btn-info btn-xs" onclick="detailLoker(' . $data['id'] . ')"><i class="fa fa-info-circle"></i> Detail</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function get_all()
    {
        $loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
        // dd($loker);
        $result = $loker['body']['data'];
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  onclick="register()" data-original-title="Edit" class="edit lamar-' . $data['id'] . ' btn btn-success btn-xs edit"><i class="fa fa-send-o"></i> Lamar sekarang</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-' . $data['id'] . ' btn btn-info btn-xs" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-info-circle"></i> Detail</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function search(Request $request)
    {
        if ($request['vertical']) {
            $limit = 6;
        } else {
            $limit = 8;
        }
        if ($request['judul'] != null) {
            $judul = strtolower($request['judul']);
        } else {
            $judul = 0;
        }
        if ($request->id > 0) {
            $data_search = array(
                'id' => $request['id'],
                'judul' => $judul,
                'id_provinsi' => $request['id_provinsi'],
                'id_bidang' => $request['id_bidang'],
                'id_sekolah' => session('id_sekolah'),
                'limited' => $limit,
            );
        } else {
            $data_search = array(
                'judul' => $judul,
                'id_provinsi' => $request['id_provinsi'],
                'id_bidang' => $request['id_bidang'],
                'id_sekolah' => session('id_sekolah'),
                'limited' => $limit,
            );
        }
        $loker = $this->lokerApi->get_filter(json_encode($data_search));
        // dd($loker);
        $last_data = $loker['body']['last_id'];
        $loker = $loker['body']['data'];
        $output = '';
        $last_id = '';
        if ($request['vertical']) {
            if (!empty($loker)) {
                foreach ($loker as $lk) {
                    $output .= '
                    <div class="col-md-4 my-3">
                    <div class="card rounded p-2">
                        <div class="d-flex justify-content-end px-3 pt-1"><i
                                class="mdi mdi-star-outline pr-1 star"></i><i
                                class="mdi mdi-dots-horizontal dot"></i>
                        </div>
                        <div class="px-3">
                            <div class="round d-flex justify-content-between m-2">
                                <img src="' . $lk['file_industri'] . '" width="32" class="imgfix" />
                                <span>Diposting ' . $lk['diposting'] . '</span>
                            </div>
                        </div>
                        <div class="px-3 pt-3">
                            <h3 class="name mb-0">' . ucwords($lk['judul']) . '<span
                                    class="badge badge-success pull-right" style="font-size: small;">' . $lk['pelamar'] . ' Pelamar</span></h3>
                            <p class="mt-0">
                                <b>' . $lk['kabupaten'] . '</b>
                            </p>
                            <p class="quote2">' . str_limit($lk['deskripsi'], 200, '...') . '
                            </p>
                        </div>
                        <div class="d-flex justify-content-start px-3 align-items-center"> <i
                                class="mdi mdi-view-comfy task"></i>
                            <span class="quote2 pl-2">Perusahaan: ' . $lk['industri'] . '</span>
                        </div>
                        <div class="px-3 align-items-center pb-3">
                            <div class="d-flex justify-content-start align-items-center"> <i
                                    class="mdi mdi-calendar-clock date"></i> <span
                                    class="quote2 pl-2">Berakhir:
                                    ' . Help::getTanggal($lk['tgl_tutup']) . '</span>
                            </div>

                        </div>
                        <div class="px-3 pt-3 w-100">
                            <center>
                                <a href="' . route('bkk_pelamar-create', Help::encode($lk['id'])) . '"
                                    class="btn btn-success"><i class="fa fa-paper-plane"></i> Lamar</a>
                                <a class="btn btn-info"
                                    href="' . route('bursa_kerja-detail_lowongan', ['code' => Help::encode($lk['id']), 'title' => str_slug($lk['judul'])]) . '"><i
                                        class="fa fa-info-circle"></i> Detail</a>
                            </center>
                        </div>
                    </div>
                </div>
                    ';
                    $last_id = $lk['id'];
                    $display = '';
                    if ($last_id < $last_data) {
                        $display = 'block';
                    } else {
                        $display = 'none';
                    }
                }
                $output .= '
                        <div id="load_more" style="width: 100%; display: ' . $display . '">
                            <button type="button" name="load_more_button" class="btn btn-success mt-3 pull-right" data-id="' . $last_id . '" id="load_more_button">Lowongan Lain <i class="fa fa-arrow-right"></i></button>
                        </div>
                    ';
            } else {
                $output = '
                <div class="col-md-12 mt-5">
                    <p class="lead mb-4">Tak ada loker yang tersedia.</p>
                </div>
                ';
            }
        } else {
            if (!empty($loker)) {
                foreach ($loker as $loke) {
                    $output .= '<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <a href="' . route('bkk_loker-get_detail', ['code' => Help::encode($loke['id']), 'key' =>  str_slug($loke['judul'])]) . '">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <img src="' . $loke['file_industri'] . '" alt="Image 1">
                                    </div>
                                    <div class="col-lg-6">
                                        <i class="fa fa-bookmark" style="float: right; font-size: 23px;"></i>
                                    </div>
                                </div>
                                <div class="col-lg-12 well well-add-card">
                                    <h4 class="mt-0">' . ucwords($loke['judul']) . '</h4>
                                </div>
                                <div class="col-lg-12">
                                    <p>' . ucwords($loke['industri']) . '</p>
                                    <p class"text-muted">' . Help::getDayMonth($loke['tgl_tutup']) . '</p>
                                </div>
                                <div class="col-lg-12">
                                    <p>' . $loke['diposting'] . '</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>';
                    $last_id = $loke['id'];
                    $display = '';
                    if ($last_id < $last_data) {
                        $display = 'block';
                    } else {
                        $display = 'none';
                    }
                }
            } else {
                $display = 'none';
            }
            $output .= '
                    <div id="load_more" style="width: 100%; display: ' . $display . '">
                        <button type="button" name="load_more_button" class="btn btn-info btn-block mt-3 pull-right" data-id="' . $last_id . '" id="load_more_button">Lowongan Lain <i class="fa fa-arrow-right"></i></button>
                    </div>
                ';
        }
        // return response()->json($output);
        $data_result = array(
            "html" => $output,
            "count" => count($loker),
        );
        return response()->json($data_result);
    }

    public function admin_search(Request $request)
    {
        // dd($request);
        $params = array(
            'id_sekolah' => session('id_sekolah'),
            'judul' => $request['judul'],
            'id_industri' => $request['id_industri'],
        );
        $loker = $this->lokerApi->get_filter_admin(json_encode($params));
        // dd($loker);
        $loker = $loker['body']['data'];
        $output = '';
        foreach ($loker as $lok) {
            $output .= '
                <div class="well well-sm widget-bg p-3 mt-2">
                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <h4 class="m-0"><b>' . ucwords($lok['judul']) . '</b></h4>
                            <div>
                                <b>
                                    ' . ucwords($lok['industri']) . ' |
                                    ' . Help::getTanggal($lok['diposting']) . '</b>
                            </div>
                            <div class="syarat mt-2 tes" style="text-align: justify;">Syarat : <p
                                    class="text-block m-0">
                                    ' . $lok['syarat'] . '</p>

                            </div>
                            <div class="mt-2">
                                <a href="javascript:void(0)" onclick="agenda(' . $lok['id'] . ')"><i class="fa fa-calendar"
                                    style="font-size: 26px;"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="share(' . $lok['id'] . ')"><i style="font-size: 26px;" class="fa fa-share-alt"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="pelamar(' . $lok['id'] . ')"><i style="font-size: 26px;" class="fa fa-users"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 text-center" style="position: relative;">
                            <div class="vertical-center" style="width: 100%">
                                <div>Range Gaji ' . number_format($lok['gaji_awal']) . ' >
                                ' . number_format($lok['gaji_sampai']) . '}</div>';
            if (session('role') != 'supervisor') {
                $output .= '<div class="mt-3">
                                    <a href="javascript:void(0)"
                                        onclick="addPelamar(' . $lok['id'] . ')"
                                        class="btn btn-sm btn-success"><i class="fa fa-user-plus"></i>
                                        Tambahkan Pelamar</a>
                                </div>';
            }
            $output .= '<div class="mt-2">
                                <a href="javascript:void(0)"
                                onclick="detailLoker(' . $lok['id'] . ')"
                                class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View
                                Detail</a>';
            if (session('role') != 'supervisor') {
                $output .= ' <a href="javascript:void(0)"
                                    onclick="deleteLoker(' . $lok['id'] . ')"
                                    class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>
                                    Hapus Loker</a>';
            }

            $output .= '</div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }

        $data_result = array(
            "html" => $output,
            "count" => count($loker),
        );
        return response()->json($data_result);
    }

    public function data_status(Request $request)
    {
        if ($request['id'] == 1) {
            $loker =  $this->lokerApi->get_by_auth_perusahaan();
        } else {
            $loker =  $this->lokerApi->get_by_auth_perusahaan_draft();
        }
        // dd($loker);
        $loker = $loker['body']['data'];
        $output = '<div class=""> <div class="widget-body clearfix"> <div class="tabs tabs-bordered"> <div class="tab-content p-0"> <div class="tab-pane active" id="home-tab-bordered-1"> <div class="row" id="tampilLoker">';
        foreach ($loker as $lo) {

            $now = date('Y-m-d');
            $date = $lo['tgl_tutup'];
            if ($request['id'] == 1) {
                if ($date < $now) {
                    $title = '<h4 class="m-0"><b>' . ucwords($lo['judul']) . '</b> - Berakhir</h4>';
                    $aksi = '<a href="javascript:void(0)" onclick="detailLoker(' . $lo['id'] . ')">Lihat</a> &nbsp;&nbsp;<a href="javascript:void(0)" onclick="tayangKembali(' . $lo['id'] . ')">Tayangkan kembali</a>&nbsp;&nbsp; <a href="javascript:void(0)" onclick="showAgenda(' . $lo['id'] . ')">Agenda</a>';
                } else {
                    $title = '<h4 class="m-0"><b>' . ucwords($lo['judul']) . '</b> - Berlangsung</h4>';
                    $aksi = '<a href="javascript:void(0)" onclick="detailLoker(' . $lo['id'] . ')">Lihat</a> &nbsp;&nbsp;
                            <a href="javascript:void(0)" onclick="showAgenda(' . $lo['id'] . ')">Agenda</a> &nbsp;&nbsp
                            <a href="javascript:void(0)" onclick="shareLoker(' . $lo['id'] . ')">Bagikan</a>

                            ';
                }
                !empty($this->pelamarApi->lamaran_proses_terpilih($lo['id'])['body']['data']) ? $terpilih = count($this->pelamarApi->lamaran_proses_terpilih($lo['id'])['body']['data']) : $terpilih = 0;
                !empty($this->pelamarApi->lamaran_terkirim($lo['id'])['body']['data']) ? $terkirim = count($this->pelamarApi->lamaran_terkirim($lo['id'])['body']['data']) : $terkirim = 0;
                !empty($this->pelamarApi->lamaran_proses_wawancara($lo['id'])['body']['data']) ? $wawancara = count($this->pelamarApi->lamaran_proses_wawancara($lo['id'])['body']['data']) : $wawancara = 0;
                $akhir = '<div>Masa Tayang berakhir, ' . Help::getTanggal($lo['tgl_tutup']) . '</div>';

                $preview = '<div class="col-xs-12 col-md-2 text-center">
                    <a href="lowongan/detail/' . $this->hashids->encode($lo['id']) . '">
                        <h1 class="rating-num m-0">
                            ' . $terkirim . '</h1>
                        <div style="color: #000000">
                            Belum diproses
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-2 text-center">
                    <a href="lowongan/detail/' . $this->hashids->encode($lo['id']) . '">
                        <h1 class="rating-num m-0">
                            ' . $terpilih . '</h1>
                        <div style="color: #000000">
                            Terpilih
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-2 text-center">
                    <a href="lowongan/detail/' . $this->hashids->encode($lo['id']) . '">
                        <h1 class="rating-num m-0">
                            ' . $wawancara . '</h1>
                        <div style="color: #000000">
                            Wawancara
                        </div>
                    </a>
                </div>';
            } else {
                $aksi = '<a href="lowongan/edit/' . $this->hashids->encode($lo['id']) . '">Edit</a> &nbsp;&nbsp;<a href="javascript:void(0)" onclick="deleteLoker(' . $lo['id'] . ')">Delete</a>';
                $preview = '';
                $title = '<h4 class="m-0"><b>' . ucwords($lo['judul']) . '</b></h4>';
                $akhir = '<div>Terakhir diupdate, ' . $lo['diposting'] . '</div>';
            }

            $output .= '<div class="col-xs-12 col-md-12 mt-3">
                <div class="well well-sm widget-bg p-3">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            ' . $title . '
                            ' . $akhir . '
                            ' . $aksi . '
                        </div>
                        ' . $preview . '
                    </div>
                </div>
            </div>';
        }
        $output .= '</div></div></div></div></div></div>';
        $data_result = array(
            "html" => $output,
            "count" => count($loker),
        );
        return response()->json($data_result);
    }

    public function side_detail(Request $request)
    {
        if ($request['id'] == 0) {
            if (session('demo')) {
                $count_loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
            } else {
                $count_loker = $this->lokerApi->get_all();
            }

            $count_loker = $count_loker['body']['data'];
            if ($count_loker == null) {
                $count = 0;
            } else {
                $count = count($count_loker);
            }

            $output = '
            <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <div class="fileFound">
                                            <img src="' . asset('asset/img/foundit.png') . '" alt="" style="width: 200px">
                                            <p class="mb-0" id="jumlah_count">Ada ' . $count . ' lowongan untuk kamu</p>
                                            <p>Pilih lowongan untuk melihat lebih detail</p>
                                        </div>
                                    </center>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            ';
        } else {
            $loker = $this->lokerApi->get_by_id($request['id']);
            // dd($loker);
            $loker = $loker['body']['data'];
            if ($request->session()->has('id')) {
                $cek_loker = count($this->pelamarApi->check_loker_user($request['id'], session('id'))['body']['data']);
                if ($cek_loker > 0) {
                    $lamar = '<a href="javascript:void" class="btn btn-success">Anda sudah melamar</>';
                } else {
                    $lamar = '<a href="/program/bursa_kerja/pelamar/create/' . $this->hashids->encode($loker['id']) . '" class="btn btn-info">Lamar Sekarang</>';
                }
            } else {
                // $lamar = '<a href="javascript:void" class="btn" style="background-color: red; color: #fff">Silahkan login dahulu untuk melamar</>';
                $lamar = '<a href="/program/bursa_kerja/pelamar/create/' . $this->hashids->encode($loker['id']) . '" class="btn btn-info">Lamar Sekarang</>';
            }

            $output = '
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                ' . $lamar . '
                                <a href="lowongan/detail/' . $this->hashids->encode($loker['id']) . '"  target="_blank" style="color: blue">  Lihat di tab Baru</a>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" onclick="lihatDetail(0)" class="btn pull-right"><i class="fa fa-times-circle" style="color: red"></i> Tutup</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="detailLoker p-3" style="overflow-y: scroll; height: 650px;">
                        <div class="row">
                            <div class="col-md-12">
                                <img src="' . $loker['file_industri'] . '" alt="Image 1"
                                    style="height: 90px;">
                                <h4 class="mt-0">' . ucwords($loker['judul']) . '</h4>
                                <p class="mb-0"><a href="#" style="color: #03a9f3">' . ucwords($loker['industri']) . '</a>
                                </p>
                                <p class="mb-0">' . $loker['kabupaten'] . '</p>
                                <p class="mb-0">Posted on ' . Help::getTanggal($loker['tanggal_post']) . '</p>
                            </div>
                            <div class="col-md-12">
                            <h4 class="mt-4">Link Sumber</h4>
                            <a href="' . $loker['link'] . '" target="_blank"><p>' . $loker['link'] . '</p></a>
                            </div>
                            <div class="col-md-12">
                                <h4>Job Description</h4>
                                <p>' . $loker['deskripsi'] . '</p>
                                <h6><b>Syarat : </b></h6>
                                <p>' . $loker['syarat'] . '.</p>
                            </div>
                            <div class="col-md-12" style="margin-top: 20px">
                                <h4 class="mt-0">Additional Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6><b>Years of Experience : </b></h6>
                                        <p>- years</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6><b>Job Type : </b></h6>
                                        <p>-</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6><b>Job Specializations : </b></h6>
                                        <p><a href="#">' . $loker['bidang'] . '</a></p>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-12" style="margin-top: 20px">
                                <h4 class="mt-0">Tentang Perusahaan</h4>
                                <p>' . $loker['deskripsi_perusahaan'] . '.</p>
                            </div>
                            <div class="col-md-12" style="margin-top: 20px">
                                <h4 class="mt-0">Informasi Tambahan Perusahaan</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6><b>Gaji: </b></h6>
                                        <p>' . $loker['gaji_awal'] . ' - ' . $loker['gaji_sampai'] . '</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h6><b>Lokasi : </b></h6>
                                        <p>' . $loker['alamat'] . '</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            ';
        }
        return response()->json($output);
    }

    public function detail()
    {
        // dd("tes");
        // $id_murni = $this->hashids->decode($id)[0];
        // dd($id_murni);
        $loker = $this->lokerApi->get_by_id(Help::decode($_GET['code']));
        // dd($loker);
        $loker = $loker['body']['data'];
        $loker['tanggal_post'] = Help::getTanggal($loker['tanggal_post']);
        $loker['id_code'] = $_GET['code'];
        // dd($this->pelamarApi->lamaran_terkirim($id_murni));
        $loker['belum_diproses'] = count($this->pelamarApi->lamaran_terkirim(Help::decode($_GET['code']))['body']['data']);
        $loker['terpilih'] = count($this->pelamarApi->lamaran_proses_terpilih(Help::decode($_GET['code']))['body']['data']);
        $loker['wawancara'] = count($this->pelamarApi->lamaran_proses_wawancara(Help::decode($_GET['code']))['body']['data']);
        $loker['ditolak'] = count($this->pelamarApi->perusahaan_ditolak(Help::decode($_GET['code']))['body']['data']);
        if (session('role') == 'bkk-pelamar') {
            $cek_loker = count($this->pelamarApi->check_loker_user(Help::decode($_GET['code']), session('id'))['body']['data']);
            if ($cek_loker > 0) {
                $loker['sudah_melamar'] = 'sudah';
            } else {
                $loker['sudah_melamar'] = 'belum';
            }
        }
        // dd($loker);
        $pelamaran =  $this->pelamarApi->lamaran_terkirim(Help::decode($_GET['code']));
        // dd($pelamaran);
        $pelamaran = $pelamaran['body']['data'];
        $pelamar = [];
        foreach ($pelamaran as $plm) {
            $pelamar[] = array(
                'nama' => $plm['nama'],
                'pengalaman' => $plm['pengalaman'],
                'telepon' => $plm['telepon'],
                'email' => $plm['email'],
                'id_code' => $this->hashids->encode($plm['id_user']),
                'id' => $plm['id_user'],
            );
        }
        // dd($pelamar);
        return view('content.bkk.dashboard.' . session('role') . '.v_detail_lowongan')->with(['loker' => $loker, 'pelamar' => $pelamar]);
    }

    public function draft(Request $request)
    {
        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = array(
            'id_industri' => session('id_industri'),
            'id_bidang_loker' => $request['id_bidang_loker'],
            'judul' => strtolower($request['judul']),
            'link' => $request['link'],
            'deskripsi' => $request['deskripsi'],
            'syarat' => $request['syarat'],
            'recruitment' => $request['recruitment'],
            'gaji_awal' => str_replace(',', '', $request['gaji_awal']),
            'gaji_sampai' => str_replace(',', '', $request['gaji_sampai']),
            'tgl_buka' => date('Y-m-d', strtotime($request['tgl_buka'])),
            'tgl_tutup' => date('Y-m-d', strtotime($request['tgl_tutup'])),
            'id_sekolah' => session('id_sekolah'),
            'status' => 1,
        );

        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->lokerApi->create_file(json_encode($data));
            File::delete($path);
        } else {
            $result = $this->lokerApi->create(json_encode($data));
        }
        if ($result['code'] == 200) {
            $id_code = $this->hashids->encode($result['body']['data']);
            return response()->json([
                'message' => $result['body']['message'],
                'success'  => 'success',
                'status' => 'berhasil',
                'id_code' => $id_code
            ]);
        } else {
            return response()->json([
                'message' => $result['body']['message'],
                'success'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function detail_by_pelamar(Request $request)
    {
        $loker = $this->lokerApi->get_by_id($request['id']);
        $loker = $loker['body']['data'];
        if (!empty($loker)) {
            $agenda = $this->agendaApi->get_by_loker($loker['id']);
            $agenda = $agenda['body']['data'];
            $html = '
            <div id="content" class="content content-full-width">
                <div class="profile">
                    <div class="profile-header">
                        <div class="profile-header-cover"></div>
                        <div class="profile-header-content">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div class="profile-header-img m-0">
                                            <img src="' . $loker['file_industri'] . '" alt=""
                                                style="width: 148px;">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="profile-header-info p-2">
                                            <h4 class="mt-0 m-b-5">' . ucwords($loker['judul']) . '</h4>
                                            <p class="m-b-10">' . ucwords($loker['industri']) . '</p>
                                            <p class="mb-0">Bandung</p>
                                            <p class="mb-0">Posted on ' . Help::getTanggal($loker['tanggal_post']) . '</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="profile-content">
                    <div class="tab-content p-0">
                        <section class="row custom-scroll-content scrollbar-enabled" style="height: 470px;">
                            <div class="tab-pane fade in active show" id="profile-about"
                                style="width: 100%; padding: 15px;">
                                <h5 class="box-title"><i class="fa fa-info-circle"></i> Detail Lowongan Kerja
                                </h5>
                                <div>
                                    <p class="mb-0"><b>Deskripsi Pekerjaan</b></p>
                                    <p>' . $loker['deskripsi'] . '</p>
                                    <hr>
                                    <p class="mb-0"><b>Syarat Pekerjaan</b></p>
                                    <p>' . $loker['syarat'] . '</p>
                                    <div class="table-responsive">
                                        <hr>
                                        <table style="width: 100%">
                                            <tbody id="outputPengalaman">
                                                <tr class="mt-2">
                                                    <td style="vertical-align: top">
                                                        Gaji
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        ' . number_format($loker['gaji_awal']) . ' - ' . number_format($loker['gaji_sampai']) . '
                                                    <td>
                                                </tr>
                                                <tr class="mt-2">
                                                    <td style="vertical-align: top">
                                                        Tanggal Buka
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        ' . Help::getTanggal($loker['tgl_buka']) . ' - ' . Help::getTanggal($loker['tgl_tutup']) . '
                                                    <td>
                                                </tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="box-title"><i class="fa fa-calendar"></i> Agenda Lowongan
                                    Pekerjaan
                                </h5>
                                <div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered" style="width: 100%">
                                            <tr>
                                                <th>Acara</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Lokasi Acara</th>
                                            </tr>';
            if (!empty($agenda)) {
                foreach ($agenda as $ag) {
                    $html .= '<tr>
                                <td>' . $ag['judul'] . '</td>
                                <td>' . Help::getTanggal($ag['tgl_mulai']) . '</td>
                                <td>' . Help::getTanggal($ag['tgl_selesai']) . '</td>
                                <td>' . $ag['alamat'] . '</td>
                            </tr>';
                }
            } else {
                $html .= ' <tr>
                                <td colspan="4" style="text-align: center">Mohon maaf, saat ini agenda belum tersedia</td>
                            </tr>';
            }
            $html .= '</table></div></div></div></section></div></div></div>';
        } else {
            $html = '';
        }
        return response()->json($html);
    }

    public function load_more(Request $request)
    {
        if ($request->id > 0) {
            $loker = $this->lokerApi->get_pagination_before($request->id, session('id_sekolah'), 3);
            $data = $loker['body']['data'];
        } else {
            $loker = $this->lokerApi->get_pagination(session('id_sekolah'), 3);
            $data = $loker['body']['data'];
        }
        // dd($data);
        $output = '';
        $last_id = '';
        if (!empty($data)) {
            foreach ($data as $lok) {
                $output .= '
                <div class="well well-sm widget-bg p-3 mt-2">
                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <h4 class="m-0"><b>' . ucwords($lok['judul']) . '</b></h4>
                            <div>
                                <b>
                                    ' . ucwords($lok['industri']) . ' </b>
                            </div>
                            <div class="syarat mt-2 tes" style="text-align: justify;">Syarat : <p
                                    class="text-block m-0">
                                    ' . $lok['syarat'] . '</p>

                            </div>
                            <div class="mt-2">
                                <a href="javascript:void(0)" onclick="agenda(' . $lok['id'] . ')"><i class="fa fa-calendar"
                                    style="font-size: 26px;"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="share(' . $lok['id'] . ')"><i style="font-size: 26px;" class="fa fa-share-alt"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="pelamar(' . $lok['id'] . ')"><i
                                    style="font-size: 26px;" class="fa fa-users"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 text-center" style="position: relative;">
                            <div class="vertical-center" style="width: 100%">
                                <div>Range Gaji ' . number_format($lok['gaji_awal']) . ' >
                                    ' . number_format($lok['gaji_sampai']) . '}
                                </div>';
                if (session('role') != 'supervisor') {
                    $output .= '<div class="mt-3">
                                    <a href="javascript:void(0)"
                                        onclick="addPelamar(' . $lok['id'] . ')"
                                        class="btn btn-sm btn-success"><i class="fa fa-user-plus"></i>
                                        Tambahkan Pelamar
                                    </a>
                                </div>';
                }

                $output .= '<div class="mt-2">
                                    <a href="javascript:void(0)" onclick="detailLoker(' . $lok['id'] . ')"
                                        class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View
                                        Detail
                                    </a>';
                if (session('role') != 'supervisor') {
                    $output .= ' <a href="javascript:void(0)" onclick="deleteLoker(' . $lok['id'] . ')"
                                        class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                                        Hapus Loker
                                    </a>';
                }
                $output .= '</div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
                $last_id = $lok['id'];
            }
            $output .= '
                <div id="load_more">
                    <center>
                        <button type="button" name="load_more_button" class="btn btn-success mt-3" data-id="' . $last_id . '" id="load_more_button">Load More <i class="fa fa-arrow-right"></i></button>
                    </center>
                </div>
            ';
        } else {
            $output .= '
                <div id="load_more">
                    <center>
                        <button type="button" name="load_more_button" class="btn btn-danger mt-3"><i class="fa fa-pause"></i> No Data Found</button>
                    </center>
                </div>
            ';
        }
        return response()->json($output);
    }

    public function share(Request $request)
    {
        // dd($request);
        $id_code = $this->hashids->encode($request['id']);
        $shareComponent = \Share::page(
            url("/bursa_kerja/detail_loker/{$id_code}"),
            'Ada pekerjaan untuk anda nih'
        )
            ->facebook()
            ->twitter()
            ->linkedin()
            ->telegram()
            ->whatsapp()
            ->reddit();

        $html = '
        <div class="modal-body" id="isi_sosials">
            <div id="isi_sosial">
                ' . $shareComponent . '
            </div>
        </div>

        <div class="modal-footer"> <label style="font-weight: 600">Page Link <span
                    class="message"></span></label><br />
            <div class="row" style="width:90%">
                <input class="col-10 ur" type="url" value="' . url("/bursa_kerja/detail_loker/{$id_code}") . '" id="myInput"
                    aria-describedby="inputGroup-sizing-default" readonly style="height: 40px;">
                <button class="cpy" onclick="myFunction()"><i class="far fa-clone"></i></button>
            </div>
        </div>
        ';

        return response()->json($html);
    }
}
