<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\FeedCommentApi;
use App\ApiService\User\ProfileApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedCommentController extends Controller
{
    private $commentApi;
    private $profileApi;

    public function __construct()
    {
        $this->commentApi = new FeedCommentApi();
        $this->profileApi = new ProfileApi();
    }


    public function store(Request $request)
    {
        $profil = $this->profileApi->get_profile();
        // dd($profil);
        $profile = $profil['body']['data'];
        if(session('role') == 'bkk-admin'){
            $data = array(
                'id_feed' => $request['id_feed'],
                'komentar' => $request['pesan'],
                'nama' => $profile['nama'],
                'email' => $profile['email'],
                'id_sekolah' => $profile['id_sekolah'],
            );
        }else{
            $data = array(
                'id_feed' => $request['id_feed'],
                'komentar' => $request['pesan'],
                'nama' => $request['nama'],
                'email' => $request['email'],
                'id_sekolah' => session('id_sekolah'),
            );
        }
        $result = $this->commentApi->create(json_encode($data));
        // dd($result);
        if ($result['code'] == 200) {
            $data = $result['body']['data'];
            $html = '<div class="row support-content-comment"><div class="col-md-12"><p>Posted by '.$data['nama'].' on ' . $data['ditulis'] . ' </p><p>' .$data['komentar'].'.</p></div></div>';
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil',
                'html' => $html
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        

    }
}
