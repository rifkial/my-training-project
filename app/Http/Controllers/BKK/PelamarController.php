<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\DokumenApi;
use App\ApiService\BKK\LokerApi;
use App\ApiService\BKK\LokerPelamarApi;
use App\ApiService\BKK\PengalamanApi;
use App\ApiService\BKK\RiwayatPelamarApi;
use App\ApiService\BKK\UserApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hashids\Hashids;

class PelamarController extends Controller
{
    private $pelamarApi;
    private $hashids;
    private $profileApi;
    private $userApi;
    private $lokerApi;
    private $pengalamanApi;
    private $dokumenApi;
    private $riwayatApi;

    public function __construct()
    {
        $this->pelamarApi = new LokerPelamarApi();
        $this->profileApi = new ProfileApi();
        $this->dokumenApi = new DokumenApi();
        $this->pengalamanApi = new PengalamanApi();
        $this->lokerApi = new LokerApi();
        $this->userApi = new UserApi();
        $this->hashids = new Hashids();
        $this->riwayatApi = new RiwayatPelamarApi();
    }

    public function index()
    {
        if (session('role') == 'bkk-pelamar') {
            $pelamars = $this->pelamarApi->semua(session('id'));
            // dd($pelamars);
            $pelamars = $pelamars['body']['data'];
            $pelamar = [];
            foreach ($pelamars as $pel) {
                $loker = $this->lokerApi->get_by_id($pel['id_loker']);
                $loker = $loker['body']['data'];
                $pelamar[] = array(
                    'id' => $pel['id'],
                    'id_loker' => $pel['id_loker'],
                    'waktu' => $pel['waktu'],
                    'status' => $pel['status'],
                    'industri' => $pel['industri'],
                    'loker' => $pel['loker'],
                    'file' => $pel['file'],
                    'pelamar' => $loker['pelamar'],
                    'agenda' => $loker['agenda'],
                );
            }
            // dd($pelamar);
            return view('content.bkk.dashboard.' . session('role') . '.v_lamaran')->with(['pelamar' => $pelamar]);
        }
    }

    public function get_datatable_pelamar(Request $request)
    {
        // dd($request);
        if ($request['params'] == "semua") {
            $pelamar = $this->pelamarApi->get_by_login_pelamar();
            $result = $pelamar['body']['data'];
        } else {
            $result = [];
        }
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        $table->rawColumns(['action']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function create(Request $request, $id_lowongan)
    {
        if (session('role') == "bkk-pelamar") {
            $lowongan = $this->lokerApi->get_by_id($this->hashids->decode($id_lowongan)[0]);
            $lowongan = $lowongan['body']['data'];
            $resume = $this->dokumenApi->get_by_login_pelamar();
            // dd($resume);
            $resume = $resume['body']['data'];
            if ($resume == null) {
                // dd($request->getUri());
                $request->session()->put("URL_PWD", $request->getUri());
                $pesan = array(
                    'message' => "anda harus mengupload resume terlebih dahulu",
                    'icon' => 'error'
                );

                return redirect()->to('program/bursa_kerja/dokumen/resume')->with(['error_api' => $pesan]);
            }
            $profile = $this->profileApi->get_profile();
            // dd($profile);
            $profile = $profile['body']['data'];

            return view('content.bkk.dashboard.' . session('role') . '.lamaran.v_create_lamaran')->with(['profile' => $profile, 'resume' => $resume, 'lowongan' => $lowongan]);
        }
    }

    public function store(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $data_insert = array(
                'id_loker' => $request['id_loker'],
                'lamaran' => $request['lamaran'],
                'id_user' => session('id'),
                'id_sekolah' => session('id_sekolah'),
                'status' => 1,
            );
            $result = $this->pelamarApi->create(json_encode($data_insert));
        } elseif (session('role') == 'bkk-admin') {
            $data_insert = array(
                'id_loker' => $request['id_loker'],
                'lamaran' => 'admin sekolah yang merekomendasikan',
                'id_user' => $request['id_user'],
                'id_sekolah' => session('id_sekolah'),
                'status' => 1,
            );
            $result = $this->pelamarApi->create(json_encode($data_insert));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $pelamar = $this->pelamarApi->get_by_id($request['id_pelamar']);
        $pelamar = $pelamar['body']['data'];
        if (session('role') == 'bkk-perusahaan') {
            if ($pelamar['dokumen'] != null) {
                $document = [];
                foreach ($pelamar['dokumen'] as $doc) {
                    $document[] = array(
                        'id_code' =>  $this->hashids->encode($doc['id']),
                        'jenis' =>  $doc['jenis'],
                    );
                    $pelamar['documents'] = $document;
                }
            } else {
                $pelamar['documents'] = [];
            }
            return response()->json($pelamar);
        }
    }

    public function get_data(Request $request)
    {
        // dd($request);
        if ($request['id'] == 10) {
            $loker = $this->pelamarApi->semua(session('id'));
        } elseif ($request['id'] == 20) {
            $loker = $this->pelamarApi->aktif(session('id'));
        } elseif ($request['id'] == 1) {
            $loker = $this->pelamarApi->terkirim(session('id'));
        } elseif ($request['id'] == 2) {
            $loker = $this->pelamarApi->diproses(session('id'));
        } elseif ($request['id'] == 3) {
            $loker = $this->pelamarApi->ditolak(session('id'));
        } else {
            $loker = $this->pelamarApi->diterima(session('id'));
        }
        // dd($loker);
        $loker = $loker['body']['data'];
        $output = '';
        foreach ($loker as $lok) {
            $lokers = $this->lokerApi->get_by_id($lok['id_loker']);
            $lokers = $lokers['body']['data'];
            // dd($lokers);
            $icon = '';
            $color = '';
            $aksi = '';
            if ($lok['status'] == "Terkirim") {
                $icon = '-check-circle';
                $color = 'blue';
            } elseif ($lok['status'] == "Diproses") {
                $icon = '-clock-o';
                $color = 'red';
            } elseif ($lok['status'] == "Diterima") {
                $icon = '-thumbs-up';
                $color = 'green';
            } else {
                $icon = '-times-circle';
                $color = '#556080';
            }

            if ($lok['status'] != 'Dibatalkan') {
                $aksi = '<div class="dropdown-menu dropdown-menu-right p-0"
                style="width: 13.85714em">
                <a class="dropdown-item" href="javascript:void(0);">
                    Lihat Profil yang diunggah </a>
                <a class="dropdown-item" href="javascript:void(0);">
                    Batalkan Aplikasi </a>
            </div>';
            } else {
                $aksi = '<div class="dropdown-menu dropdown-menu-right p-0"
                style="width: 13.85714em">
                <a class="dropdown-item" href="javascript:void(0);">
                    Lihat Profil yang diunggah </a>';
            }


            $output .= '
                        <div class="col-sm-6 mt-3">
                            <div class="card">
                                <header class="text-center">
                                    <div class="float-right dropdown mr-1">
                                        <a href="javascript:void(0);" data-toggle="dropdown"
                                            class="dropdown-toggle">
                                            <i class="material-icons list-icon text-color-scheme-dark">menu</i>
                                        </a>
                                        ' . $aksi . '
                                    </div>
                                </header>
                                <div class="card-body row">
                                    <div class="col-sm-8">
                                        <h5 class="card-title mb-1">' . $lok['loker'] . '&nbsp;&nbsp; <a href="javascript:void(0)"
                                        onclick="detailLoker(' . $lok['id_loker'] . ')" id="detail-loader" class="detail"><small> <i
                                                class="fa fa-eye"></i> detail</small></a></h5>
                                        <p>' . $lok['industri'] . '</p>
                                        <p class="card-text mb-0"><i class="fa fa' . $icon . '" style="color:' . $color . '"></i> ' . $lok['status'] . '
                                            ' . $lok['waktu'] . '.</p>
                                        <p class="card-text mb-0"><i class="fa fa-user"></i> ' . $lokers['pelamar'] . ' pelamar
                                            <small>Bandingkan</small>.</p>
                                        <p class="card-text"><i class="fa fa-calendar"></i>
                                            ' . $lokers['agenda'] . ' Agenda.
                                        </p>

                                        <a href="#"><small>Sudah melamar tanggal ' . $lok['waktu'] . '</small></a>
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="http://localhost/smart_school/public/images/default.png" alt=""
                                            style="height: 150px">
                                    </div>
                                </div>
                            </div>
                        </div>
        ';
        }
        $data_result = array(
            "html" => $output,
            "count" => count($loker),
        );
        return response()->json($data_result);
    }

    public function data_status(Request $request)
    {
        // dd($request);
        $id_loker = $request['id_loker'];
        if ($request['params'] == "belum_proses") {
            $pelamar =  $this->pelamarApi->lamaran_terkirim($this->hashids->decode($id_loker)[0]);
        } elseif ($request['params'] == "terpilih") {
            $pelamar =  $this->pelamarApi->lamaran_proses_terpilih($this->hashids->decode($id_loker)[0]);
        } elseif ($request['params'] == "wawancara") {
            $pelamar =  $this->pelamarApi->lamaran_proses_wawancara($this->hashids->decode($id_loker)[0]);
        } else {
            $pelamar =  $this->pelamarApi->perusahaan_ditolak($this->hashids->decode($id_loker)[0]);
        }
        $pelamar = $pelamar['body']['data'];
        $output = '';
        foreach ($pelamar as $pel) {
            if ($request['params'] == "belum_proses") {
                $aksi = "<div class='nextAksi mt-2'>
                            <a href='javascript:void(0)'
                                onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",terpilih\")'
                                id='terpilih'>Terpilih</a>
                            &nbsp;&nbsp;<a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",wawancara\")'
                                id='wawancara'>Wawancara</a>
                            &nbsp;&nbsp;
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",tidak_sesuai\")'
                                id='tidak_sesuai'>Tidak
                                sesuai</a>
                        </div>";
            } elseif ($request['params'] == "terpilih") {
                $aksi = "<div class='nextAksi mt-2'>
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",wawancara\")'
                                id='wawancara'>Wawancara</a>
                            &nbsp;&nbsp;
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",tidak_sesuai\")'
                                id='tidak_sesuai'>Tidak
                                sesuai</a>
                        </div>";
            } elseif ($request['params'] == "wawancara") {
                $aksi = "<div class='nextAksi mt-2'>
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",diterima\")'
                                id='wawancara'>Diterima</a>
                            &nbsp;&nbsp;
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",tidak_sesuai\")'
                                id='tidak_sesuai'>Tidak
                                sesuai</a>
                        </div>";
            } else {
                $aksi = "<div class='nextAksi mt-2'>
                            <a href='javascript:void(0)'
                            onclick='movePilih(\"" . $this->hashids->encode($pel['id']) . ",diterima\")'
                                id='wawancara'>Diterima</a>
                        </div>";
            }

            if (!empty($pel['pengalaman'])) {
                $pengalaman = '';
                foreach ($pel['pengalaman'] as $peng) {
                    $pengalaman .= '
                    <p class="mb-0">
                        ' . ucwords($peng['nama']) . '
                         (' . Help::getMonthYear($peng['tgl_mulai']) . ' - ' . Help::getMonthYear($peng['tgl_akhir']) . ' )
                    </p>
                    <p>' . ucwords($peng['industri']) . '
                    </p>
                    ';
                }
            } else {
                $pengalaman = '<div style="min-height: 100px;">
                    <p>Belum punya penglaman (Fresh
                        Graduate)</p>
                </div>';
            }

            $output .= "
                <div class='col-xs-12 col-md-6 mt-3 pr-0'>
                    <div class='well well-sm widget-bg p-3'>
                        <div class='row m-0'>
                            <table style='width: 100%'>
                                <tr>
                                    <td style='width: 10%; vertical-align: top;'>
                                        <input class='form-check-input ml-0'
                                            type='checkbox'  id='defaultCheck1'>
                                    </td>
                                    <td>
                                        <h5 class='card-title mb-1'>
                                            " . ucwords($pel['nama']) . "</h5>
                                            <a href='javascript:void(0)' data-id='" . $pel['id_user'] . "' id='detail'>lihat detail</a>
                                        " . $pengalaman . "
                                        <i class'fa fa-phone'></i>
                                        " . $pel['telepon'] . " |
                                        <i class='fa fa-envelope'></i>
                                        " . $pel['email'] . " |
                                        <i class='fa fa-dollar'></i> IDR 2,500,000
                                        " . $aksi . "
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            ";
        }
        $data_result = array(
            "html" => $output,
            "count" => count($pelamar),
        );
        return response()->json($data_result);
    }

    public function update_status(Request $request)
    {
        // dd($request);
        // dd(session()->all());
        if ($request['aksi'] == 'terpilih') {
            $status_lamaran = 2;
            $status_proses = 1;
        } elseif ($request['aksi'] == 'wawancara') {
            $status_lamaran = 2;
            $status_proses = 2;
        } elseif ($request['aksi'] == 'diterima') {
            $status_lamaran = 4;
            $status_proses = null;
        } else {
            $status_lamaran = 3;
            $status_proses = null;
        }
        $pelamar = $this->pelamarApi->get_by_id($this->hashids->decode($request['id_code_pelamar'])[0]);
        // dd($pelamar);
        $data_update = $pelamar['body']['data'];
        $data_update['status_proses'] = $status_proses;
        $data_update['status'] = $status_lamaran;
        $data_update['id_sekolah'] = session('id_sekolah');
        $result = $this->pelamarApi->update_info(json_encode($data_update));
        if ($result['code'] == 200) {
            if (session('role') == 'bkk-perusahaan') {
                if ($status_lamaran == 4) {
                    $data = array(
                        'id_user' => Help::decode($request['id_code_pelamar']),
                        'id_industri' => session('id_industri'),
                        'tgl_diterima' => date('Y-m-d H:i:s'),
                        'id_sekolah' => session('id_sekolah')
                    );
                    $insert = $this->riwayatApi->create(json_encode($data));
                    // dd($insert);
                    if ($insert['code'] == 200) {
                        return response()->json([
                            'success' => "success",
                            'message' => "Pelamar berhasil diupdate dan masuk ke riwayat",
                            'status' => 'berhasil'
                        ]);
                    } else {
                        return response()->json([
                            'success' => "error",
                            'message' => 'Status berhasil diupdate, namun pelamar belum masuk ke riwayat',
                            'status' => 'berhasil'
                        ]);
                    }
                }
            }
            return response()->json([
                'success' => "success",
                'message' => 'Status berhasil diupdate, namun pelamar belum masuk ke riwayat',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function detailPelamar(Request $request)
    {
        // dd($request);
        $pelamar = $this->userApi->get_by_id($request['id']);
        $pelamar = $pelamar['body']['data'];
        $pengalaman = $this->pengalamanApi->get_by_user($pelamar['id']);

        // dd($pengalaman);
        $pengalaman = $pengalaman['body']['data'];
        // dd($pengalaman);
        if (!empty($pengalaman)) {
            $last_pengalman = end($pengalaman);
            // dd($last_pengalman);
            $riwayat_pengalaman = '<p class="mb-0">' . ucwords($last_pengalman['nama']) . ' (' . Help::getMonthYear($last_pengalman['tgl_mulai']) . ' - ' . Help::getMonthYear($last_pengalman['tgl_akhir']) . ')</p>
            <p class="m-b-10">' . ucwords($last_pengalman['industri']) . '</p>';
        } else {
            $riwayat_pengalaman = '<p class="mb-0">Pelamar belum punya pengalaman kerja (FreshGraduate)</p>';
        }

        $html = '
            <div id="content" class="content content-full-width">
                <div class="profile">
                    <div class="profile-header">
                        <div class="profile-header-cover"></div>
                        <div class="profile-header-content">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div class="profile-header-img m-0">
                                            <img src="' . $pelamar['file'] . '"
                                                alt="">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="profile-header-info p-2">
                                            <h4 class="mt-0 m-b-5">' . ucwords($pelamar['nama']) . '</h4>
                                            ' . $riwayat_pengalaman . '
                                            <p class="mb-0"><i class="fa fa-phone"></i> ' . $pelamar['telepon'] . ' | <i
                                                    class="fa fa-envelope"></i>
                                                ' . $pelamar['email'] . ' | <i class="fa fa-dollar"></i> IDR
                                                2,500,000</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="profile-aksi-info">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button"
                                                    class="btn btn-info btn-sm" onclick="movePilih(\'' . $this->hashids->encode($pelamar['id']) . ',wawancara\')">Wawancara</button>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="movePilih(\'' . $this->hashids->encode($pelamar['id']) . ',tidak_sesuai\')">Tidak
                                                    sesuai</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="profile-content">
                    <div class="tab-content p-0">
                        <section class="row custom-scroll-content scrollbar-enabled" style="height: 470px;">
                            <div class="tab-pane fade in active show" id="profile-about"
                                style="width: 100%; padding: 15px;">

        ';
        if (!empty($pengalaman)) {
            $html .= '<h5 class="box-title"><i class="fa fa-shopping-bag"></i> Pengalaman
            </h5>
            <div>
                <div class="table-responsive">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 20%">Tingkat Pengalaman</td>
                            <td>Saya telah bekerja sejak Februari 2019
                            <td>
                        </tr>
                    </table>
                    <hr>
                    <table style="width: 100%">
                        <tbody id="outputPengalaman">';
            foreach ($pengalaman as $peng) {
                $html .= '
                <tr class="mt-2">
                    <td style="vertical-align: top">
                        ' . Help::getMonthYear($peng['tgl_mulai']) . ' - ' . Help::getMonthYear($peng['tgl_akhir']) . '
                        <br><small>6 months</small>
                    </td>
                    <td style="vertical-align: top">
                        <h5 class="mt-0">' . ucwords($peng['nama']) . '</h5>
                        <b>' . ucwords($peng['industri']) . '
                            | ' . $peng['provinsi'] . ', INDONESIA</b>
                        <table style="width: 100%">
                            <tr>
                                <td>Industri</td>
                                <td>Travel / Pariwisata</td>
                            </tr>
                            <tr>
                                <td>Spesialisasi</td>
                                <td>IT/Komputer - Perangkat Keras</td>
                            </tr>
                            <tr>
                                <td>Bidang pekerjaan</td>
                                <td>Teknisi Perangkat Keras Komputer</td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td>Pegawai (non-manajemen & non-supervisor)
                                </td>
                            </tr>
                            <tr>
                                <td>Gaji bulanan</td>
                                <td>IDR 1,600,000</td>
                            </tr>
                        </table>
                        <small>Job desc : ' . $peng['deskripsi'] . '</small>
                    <td>
                </tr>
                ';
            }
            $html .= '</tbody> </table> </div> </div>';
        }

        foreach ($pelamar['dokumen'] as $dok) {
            $html .= '<h5 class="box-title"><i class="fa fa-shopping-bag"></i> Resume/CV
            </h5>
            <div>
                <div class="table-responsive">
                    <table style="width: 100%">
                        <tbody id="outputPengalaman">
                            <tr class="mt-2">
                                <td style="vertical-align: middle; width: 20%">
                                    ' . $dok['jenis'] . '.pdf
                                </td>
                                <td style="vertical-align: top">
                                    <a href="/program/bursa_kerja/dokumen/download/' . $this->hashids->encode($dok['id']) . '"
                                        class="btn btn-info btn-success btn-sm"><i
                                            class="fa fa-eye"></i> Lihat</a>
                                <td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>';
        }
        $html .= '</div> </section> </div> </div> </div>';
        return response()->json($html);
    }

    public function admin_datatable(Request $request)
    {
        $id = $request['id'];
        if ($request['aksi'] == "belum_melamar") {
            $pelamar = $this->userApi->all_pelamar_apply($id);
        } else {
            $pelamar = $this->pelamarApi->get_by_loker($id);
        }
        // dd($pelamar);

        $pelamar = $pelamar['body']['data'];
        $result = [];
        foreach ($pelamar as $pl) {
            if ($request['aksi'] == "belum_melamar") {
                $result[] = array(
                    'id' => $pl['id'],
                    'nama' => ucwords($pl['nama']),
                    'telepon' => $pl['telepon'],
                    'alamat' => $pl['alamat'],
                    'status' => $pl['jenkel'],
                    'status' => $pl['jenkel'],
                    'status_lamaran' => $pl['status_lamaran'],
                    'id_loker' => $id,
                );
            } else {
                $result[] = array(
                    'id' => $pl['id'],
                    'nama' => ucwords($pl['nama']),
                    'telepon' => $pl['telepon'],
                    'alamat' => $pl['alamat'],
                    'status' => $pl['status'],
                );
            }
        }
        $table = datatables()->of($result);
        if ($request['aksi'] != "belum_melamar") {
            $table->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm restore-' . $data['id'] . '" onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm hardDelete-' . $data['id'] . '" onclick="forceDelete(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                return $button;
            });
        } else {
            $table->addColumn('action', function ($data) {
                if ($data['status_lamaran'] < 1) {
                    return '<a href="javascript:void(0)" id="applyLamaran_' . $data['id'] . '" data-toggle="tooltip"  class="btn btn-info btn-sm apply" onclick="applyJob(' . $data['id'] . ',' . $data['id_loker'] . ')"><i class="fa fa-paper-plane"></i> Apply</a>';
                } else {
                    return '<a href="javascript:void(0)"  data-toggle="tooltip"  class="btn btn-success btn-sm apply" ><i class="fa fa-check"></i> Anda Sudah melamar</a>';
                }
            });
        }
        if ($request['aksi'] != "belum_melamar") {
            $table->editColumn('status', function ($stts) {
                if ($stts['status'] == "Terkirim") {
                    $stat = '<span class="badge badge-info">' . $stts['status'] . '</span> &nbsp;';
                } elseif ($stts['status'] == "Diproses") {
                    $stat = '<span class="badge badge-warning">' . $stts['status'] . '</span> &nbsp;';
                } else {
                    $stat = '<span class="badge badge-success">' . $stts['status'] . '</span> &nbsp;';
                }
                return $stat;
            });
            $table->rawColumns(['action', 'status']);
        } else {
            $table->rawColumns(['action']);
        }

        $table->addIndexColumn();
        return $table->make(true);
    }

    public function riwayat_pelamar(Request $request)
    {
        $id_user = $request['id_user'];
        $pelamar = $this->pelamarApi->riwayat_pelamar($id_user);
        // dd($pelamar);
        $pelamar = $pelamar['body']['data'];
        $result = [];
        foreach ($pelamar as $pel) {
            $result[] = array(
                'nama' => ucwords($pel['nama']),
                'loker' => ucwords($pel['loker']),
                'industri' => $pel['industri'],
                'status' => $pel['status'],
                'waktu' => $pel['waktu'],
            );
        }
        $table = datatables()->of($result);
        $table->addColumn('status', function ($stts) {
            if ($stts['status'] == "Terkirim") {
                $stat = '<span class="badge badge-info">' . $stts['status'] . '</span> &nbsp;';
            } elseif ($stts['status'] == "Diproses") {
                $stat = '<span class="badge badge-warning">' . $stts['status'] . '</span> &nbsp;';
            } else {
                $stat = '<span class="badge badge-success">' . $stts['status'] . '</span> &nbsp;';
            }
            return $stat;
        });
        $table->rawColumns(['status']);
        $table->addIndexColumn();
        return $table->make(true);
    }

    public function filter_name(Request $request)
    {
        // dd($request);
        $data = array(
            'keyword' => $request['keyword'],
            'role' => 'industri',
            'id_sekolah' => session('id_sekolah')
        );
        $user = $this->userApi->filter_name(json_encode($data));
        // dd($user);
        $user = $user['body']['data'];
        $html = '';
        if (!empty($user)) {
            foreach ($user as $us) {
                $html .= '<div class="col-md-6 my-2">
                <div class="card">
                    <div class="card-body">
                        <table>
                            <tr>
                                <td width="20"><input type="radio" name="id_user"
                                        value="' . $us['id'] . '"></td>
                                <td width="120"><img src="' . $us['file'] . '" alt=""
                                        width="100"></td>
                                <td>
                                    <b>' . ucwords($us['nama']) . '</b>
                                    <p class="m-0">' . $us['email'] . '</p>
                                    <small><i class="fas fa-phone"></i>' . $us['telepon'] . '</small>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>';
            }
        } else {
            $html .= '<div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="w-100">
                        <tr>
                            <td class="text-center">
                                <h3>Belum ada user industri yang tersedia</h3>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>';
        }

        return response()->json($html);
    }
}
