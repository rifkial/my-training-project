<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\KetrampilanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KetrampilanController extends Controller
{
    private $keterampilanApi;

    public function __construct()
    {
        $this->keterampilanApi = new KetrampilanApi();
    }

    public function index(Request $request)
    {
        if (session("role") == 'bkk-pelamar') {
            $keterampilan = $this->keterampilanApi->get_by_user(session('id'));
            // dd($keterampilan);
            $keterampilan = $keterampilan['body']['data'];
            // dd($keterampilan);
            return view('content.bkk.dashboard.' . session('role') . '.profile.v_ketrampilan')->with(['keterampilan' => $keterampilan]);
        }
    }

    public function store(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $keterampilan = $this->keterampilanApi->get_by_user(session('id'));
            // dd($keterampilan);
            $keterampilan = $keterampilan['body']['data'];
            if (count($keterampilan) > 1) {
                // dd("data ada");
                foreach ($keterampilan as $kt) {
                    $delete = $this->keterampilanApi->delete($kt['id']);
                }
                // dd($delete);
            }
            $nama = $request['name'];
            $tingkat = $request['tingkat'];
            $res = [];
            foreach($nama as $key => $value)
            {
                foreach($tingkat as $k => $v)
                {
                    if($key == $k)
                    {
                        $res[$key]['nama'] = $value;
                        $res[$key]['tingkat'] = $v;
                    }
                }
            }

            foreach($res as $r){
                $data_insert = array(
                    'id_sekolah' => session('id_sekolah'),
                    'id_user' => session('id'),
                    'nama' => $r['nama'],
                    'tingkat' => $r['tingkat'],
                );
                $result = $this->keterampilanApi->create(json_encode($data_insert));
            }

        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $data_update = array(
                'id' => $request['id'],
                'id_user' => $request['id_user'],
                'nama' => $request['nama'],
                'industri' => $request['industri'],
                'alamat' => $request['alamat'],
                'deskripsi' => $request['deskripsi'],
                'provinsi' => $request['provinsi'],
                'id_sekolah' => session('id_sekolah'),
                'tgl_mulai' => $request['tahun_dari']."-".$request['bulan_dari']."-01",
                'tgl_akhir' => $request['tahun_sampai']."-".$request['bulan_sampai']."-01",
            );
            $result = $this->keterampilanApi->update_info(json_encode($data_update));
        }
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $keterampilan = $this->keterampilanApi->get_by_id($request['id']);
        $keterampilan = $keterampilan['body']['data'];
        $keterampilan['bulan_mulai'] = date('m', strtotime($keterampilan['tgl_mulai']));
        $keterampilan['tahun_mulai'] = date('Y', strtotime($keterampilan['tgl_mulai']));
        $keterampilan['bulan_akhir'] = date('m', strtotime($keterampilan['tgl_akhir']));
        $keterampilan['tahun_akhir'] = date('Y', strtotime($keterampilan['tgl_akhir']));
        return response()->json($keterampilan);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->keterampilanApi->soft_delete($request['id']);
        // dd($delete);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
