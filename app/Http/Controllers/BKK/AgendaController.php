<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\AgendaApi;
use App\ApiService\BKK\LokerApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class AgendaController extends Controller
{
    private $agendaApi;
    private $lokerApi;

    public function __construct()
    {
        $this->agendaApi = new AgendaApi();
        $this->lokerApi = new LokerApi();
    }

    public function index()
    {
        // dd("halaman agenda");
        Session::put('title', 'Jadwal Recruitment');
        if (request()->ajax()) {
            $kalender = $this->agendaApi->get_by_industri();
            // dd($kalender);
            $data = $kalender['body']['data'];
            return Response::json($data);
        }
        $loker = $this->lokerApi->get_by_sekolah(session('id_sekolah'));
        // dd($loker);
        $loker = $loker['body']['data'];
        $template = 'default';
        if (session('role') != 'bkk-admin') {
            $template = session('template');
        }
        if (session('role') == "bkk-admin") {
            return view('content.bkk.dashboard.bkk-admin.v_agenda')->with(['template' => $template, 'loker' => $loker]);
        }else{
            return view('content.bkk.dashboard.bkk-admin.v_s_agenda')->with(['template' => $template, 'loker' => $loker]);
        }
    }


    public function get_datatable(Request $request)
    {
        if (session('role') == 'bkk-pelamar') {
            $loker = $this->agendaApi->get_by_auth_pelamar();
            $result = $loker['body']['data'];
        } else {
            if ($request['id_loker'] != 0 && $request['id_pelamar'] != 0) {
                $loker = $this->agendaApi->get_by_loker_pelamar($request['id_loker'], $request['id_pelamar']);
                $result = $loker['body']['data'];
            } else {
                $result = [];
            }
        }
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="editAgenda btn btn-success btn-xs edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<a type="button" class="btn btn-info btn-xs" onclick="detailAgenda(' . $data['id'] . ')" style="margin-top: 5px; color: #fff;"><i class="fa fa-info-circle"></i> Detail</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" class="btn btn-danger btn-xs" onclick="deleteAgenda(' . $data['id'] . ')" style="margin-top: 5px"><i class="fa fa-trash"></i> Hapus</button>';
                return $button;
            });
        $table->editColumn('tanggal', function ($row) {
            return Help::getTanggal($row['tgl_mulai'])." - ".Help::getTanggal($row['tgl_selesai']);
        });
        $table->rawColumns(['action', 'tanggal']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function store(Request $request)
    {
        if (session('role') == 'bkk-admin') {
            // dd($request);
            $data_insert = array(
                'id_loker' => $request['id_loker'],
                'nama' => $request['nama'],
                'alamat' => $request['alamat'],
                'tgl_mulai' => date('Y-m-d', strtotime($request['tanggal_mulai']))." ".$request['waktu_mulai'],
                'tgl_selesai' => date('Y-m-d', strtotime($request['tanggal_selesai']))." ".$request['waktu_selesai'],
                'id_sekolah' => session('id_sekolah'),
            );
        } else {
            $data_insert = array(
                'id_loker' => $request['id_loker'],
                'nama' => $request['title'],
                'alamat' => $request['alamat'],
                'tgl_mulai' => date('Y-m-d', strtotime($request['start'])),
                'tgl_selesai' => date('Y-m-d', strtotime($request['end'])),
                'id_sekolah' => session('id_sekolah'),
            );
        }
        $result = $this->agendaApi->create(json_encode($data_insert));
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $result['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $result['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        if (session('role') == 'bkk-admin') {
            $data = array(
                'id' => $request['id_agenda'],
                'id_loker' => $request['id_loker'],
                'nama' => $request['nama'],
                'alamat' => $request['alamat'],
                'tgl_mulai' => date('Y-m-d', strtotime($request['tanggal_mulai']))." ".$request['waktu_mulai'],
                'tgl_selesai' => date('Y-m-d', strtotime($request['tanggal_selesai']))." ".$request['waktu_selesai'],
                'id_sekolah' => session('id_sekolah'),
           );
        } else {
            $data = array(
                'id' => $request->id,
                'id_loker' => $request['id_loker'],
                'nama' => $request['title'],
                'alamat' => $request['alamat'],
                'tgl_mulai' => date('Y-m-d', strtotime($request['start'])),
                'tgl_selesai' => date('Y-m-d', strtotime($request['end'])),
                'id_sekolah' => session('id_sekolah'),
            );
        }
        $result = $this->agendaApi->update_info(json_encode($data));
        if ($result['code'] == 200) {
            return response()->json([
            'message' => $result['body']['message'],
            'success'  => 'success',
            'status'  => 'berhasil'
        ]);
        } else {
            return response()->json([
            'message' => $result['body']['message'],
            'success'  => 'error',
            'status'  => 'gagal'
        ]);
        }
    }

    public function edit(Request $request)
    {
        $agenda = $this->agendaApi->get_by_id($request['id_agenda']);
        $agenda = $agenda['body']['data'];
        $agenda['tanggal_mulai'] =  date('d-m-Y', strtotime($agenda['tgl_mulai']));
        $agenda['tanggal_selesai'] =  date('d-m-Y', strtotime($agenda['tgl_selesai']));
        return response()->json($agenda);
    }


    public function by_loker($id)
    {
        $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
        $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
        $agenda = $this->agendaApi->get_acara($start, $end, $id, session('id_sekolah'));
        $data = $agenda['body']['data'];
        $result = [];
        foreach ($data as $dt) {
            $result[] = array(
                'id' => $dt['id'],
                'id_loker' => $dt['id_loker'],
                'title' => $dt['nama'],
                'alamat' => $dt['alamat'],
                'start' => $dt['tgl_mulai'],
                'end' => $dt['tgl_selesai'],
            );
        }
        return Response::json($result);
    }

    public function by_user_perusahaan()
    {
        $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
        $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
        $agenda = $this->agendaApi->get_by_industri($start, $end);
        // dd($agenda);
        $data = $agenda['body']['data'];
        $result = [];
        foreach ($data as $dt) {
            $result[] = array(
                'id' => $dt['id'],
                'id_loker' => $dt['id_loker'],
                'title' => $dt['nama'],
                'judul' => $dt['judul'],
                'alamat' => $dt['alamat'],
                'start' => $dt['tgl_mulai'],
                'end' => $dt['tgl_selesai'],
            );
        }
        return Response::json($result);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->agendaApi->soft_delete($request->id);
        if ($delete['code'] == 200) {
            $status = "berhasil";
            $message = $delete['body']['message'];
            $icon = 'success';
        } else {
            $status = "gagal";
            $message =  $delete['body']['message'];
            $icon = "error";
        }
        return Response::json([
            'status' => $status,
            'message' => $message,
            'icon' => $icon
        ]);
    }

    public function soft_del($id)
    {
        $delete = $this->agendaApi->soft_delete($id);
        if ($delete['code'] == 200) {
            $message = array(
                'status' => "berhasil",
                'message' => $delete['body']['message'],
                'icon' => 'success',
            );
        } else {
            $message = array(
                'status' => "gagal",
                'message' => $delete['body']['message'],
                'icon' => 'error',
            );
        }
       return redirect()->back()->with(['message' => $message]);
    }
}
