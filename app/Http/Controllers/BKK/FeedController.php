<?php

namespace App\Http\Controllers\BKK;

use App\ApiService\BKK\FeedApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    private $feedApi;

    public function __construct()
    {
        $this->feedApi = new FeedApi();
    }


    public function detail(Request $request)
    {
        // dd($request);
        $feed = $this->feedApi->get_by_id($request['id']);
        $feed = $feed['body']['data'];
        return response()->json($feed);
    }

    public function update_status(Request $request)
    {
        // dd($request);
        $data = array(
            'status' => $request['value']
        );
        $update = $this->feedApi->update_status(json_encode($data), $request['id']);
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        // dd($request);
        $delete = $this->feedApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }
}
