<?php

namespace App\Http\Controllers\BKK\Admin;

use App\ApiService\BKK\UserApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use App\Util\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use XcS\XcTools;

class UserBKKController extends Controller
{
    private $userApi;

    public function __construct()
    {
        $this->userApi = new UserApi();
    }

    public function index()
    {
        session()->put('title', "Data Pelamar");
        $routes = "user_bkk-beranda";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        $url = $this->userApi->import();
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->userApi->pagination_role($status, $page, $search, "pelamar");
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('message', $pesan);
        }
        $pelamar = $result['body']['data'];
        $meta = $result['body']['meta'];
        $pagination = Utils::simpleWithStatus($meta, $routes, $status, $search);
        $template = 'default';
        if (session('role') != 'bkk-admin') {
            $template = session('template');
        }
        return view('content.admin.bkk.v_user')->with(['url', 'template' => $template, 'pelamar' => $pelamar, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'status' => $status, 'url' => $url]);
    }

    public function user_perusahaan()
    {
        $url = $this->userApi->import();
        session()->put('title', "Mitra User Industri");
        $routes = "user_bkk-perusahaan";
        $status = (isset($_GET["status"])) ? $_GET["status"] : "";
        // dd($status);
        $page = (isset($_GET["page"])) ? $_GET["page"] : "1";
        $search = (isset($_GET["search"])) ? $_GET["search"] : "";
        $result = $this->userApi->pagination_role($status, $page, $search, "industri");
        if ($result['code'] != 200) {
            $pesan = array(
                'message' => $result['body']['message'],
                'icon' => 'error'
            );
            return redirect()->back()->with('message', $pesan);
        }
        $pelamar = $result['body']['data'];
        // dd($pelamar);
        $meta = $result['body']['meta'];
        $pagination = Utils::simpleWithStatus($meta, $routes, $status, $search);
        $template = session('template');
        if (session('role') == 'bkk-admin' || session('role') == 'admin') {
            $template = 'default';
        }

        return view('content.admin.bkk.v_user_perusahaan')->with(['url' => $url, 'template' => $template, 'pelamar' => $pelamar, 'pagination' => $pagination, 'routes' => $routes, 'search' => $search, 'status' => $status]);




        // if (session('role') == 'admin') {
        //     return view('content.admin.bkk.v_user_perusahaan')->with([]);
        // }
        // if (session('role') == 'supervisor') {
        //     return view('content.bkk.dashboard.supervisor.v_user_perusahaan')->with([]);
        // }
        // return view('content.bkk.dashboard.bkk-admin.v_user_perusahaan')->with(['url' => $url]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'agama' => $request->agama,
            'jenkel' => $request->jenkel,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'tempat_lahir' => $request->tempat_lahir,
            'pendidikan_akhir' => $request->pendidikan_akhir,
            'role' => $request->role,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'first_password' => Str::random(6),
            'status' => 1
        );
        // dd($data);
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            $result = $this->userApi->create_file(json_encode($data));
            // dd($result);
            File::delete($path);
        } else {
            $result = $this->userApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $result['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request->id,
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'agama' => $request->agama,
            'jenkel' => $request->jenkel,
            'tgl_lahir' => date('Y-m-d', strtotime($request->tanggal_lahir)),
            'tempat_lahir' => $request->tempat_lahir,
            'pendidikan_akhir' => $request->pendidikan_akhir,
            'role' => $request->role,
            'alamat' => $request->alamat,
            'id_sekolah' => session('id_sekolah'),
            'status' => $request['status'],
        );
        if ($files = $request->file('image')) {
            $namaFile = $files->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/profil/";
            Help::check_and_make_dir($basePath);
            $files->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path'] = $path;
            // dd($data);
            $result = $this->userApi->update_file(json_encode($data));
            // dd($result);
            File::delete($path);
        } else {
            if ($request['remove_photo']) {
                $data['hapus'] = "hapus";
                $result = $this->userApi->update(json_encode($data));
            } else {
                $data['hapus'] = "tidak";
                $result = $this->userApi->update(json_encode($data));
            }
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

    public function edit(Request $request)
    {
        $pelamar = $this->userApi->get_by_id($request['id']);
        $pelamar = $pelamar['body']['data'];
        // dd($pelamar);
        $file = explode('/', $pelamar['file']);
        $pelamar['file_check'] = end($file);
        // dd($file);
        $pelamar['tgl_lhr'] = date('d-m-Y', strtotime($pelamar['tgl_lahir']));
        if ($pelamar['jenkel'] == "Perempuan") {
            $pelamar['jk'] = 'l';
        } else {
            $pelamar['jk'] = 'p';
        }
        return response()->json($pelamar);
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->userApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'icon' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'icon' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function get_datatable(Request $request)
    {
        // dd($request);
        $status_kode = $request['status'];
        $aksi = $request['aksi'];
        if ($aksi == 'pelamar') {
            if ($status_kode == null) {
                $pelamar = $this->userApi->Allpelamar(session('id_sekolah'));
            } else {
                $pelamar = $this->userApi->pelamar(session('id_sekolah'), $status_kode);
            }
            // dd($pelamar);
            $result = $pelamar['body']['data'];
        } else {
            if ($status_kode == null) {
                $industri =  $this->userApi->perusahaan(session('id_sekolah'));
            } else {
                $industri = $this->userApi->industri(session('id_sekolah'), $status_kode);
            }
            $result = $industri['body']['data'];
        }
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<div class="btn-group" role="group" aria-label="Horizontal Button Group">';
                $button .= '<a  class="btn btn-info btn-sm edit"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-edit"></i> </a>';
                $button .= '<a  class="btn btn-success btn-sm riwayat"  data-id="' . $data['id'] . '" style="color: #fff"><i class="fa fa-history"></i> </a>';
                $button .= ' <button type="button" class="btn btn-danger btn-sm delete-' . $data['id'] . '" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash-o"></i></button>';
                $button .= '</div>';
                return $button;
            });
        $table->editColumn('gambar', function ($row) {
            return '<img src="' . $row['file'] . '" border="0" width="40" class="img-rounded" align="center" />';
        });
        $table->editColumn('aksi_supervisor', function ($row) {
            return '<a class="btn btn-success btn-sm riwayat"  data-id="' . $row['id'] . '" style="color: #fff"><i class="fa fa-history"></i> </a>';
        });
        $table->editColumn('status', function ($row) {
            if ($row['status'] == 1) {
                return '<span class="badge badge-success">Aktif</span>';
            } else {
                return '<span class="badge badge-danger">Tidak aktif</span>';
            }
        });
        $table->rawColumns(['action', 'gambar', 'status', 'aksi_supervisor']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function import(Request $request)
    {
        $files = $request->file('image');
        $namaFile = $files->getClientOriginalName();
        $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
        $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
        $basePath = "file/import/";
        Help::check_and_make_dir($basePath);
        $files->move($basePath, $imageName);
        $path = $basePath . $imageName;
        $data['path'] = $path;
        $result = $this->userApi->upload_excel(json_encode($data), $request['role']);
        $message = $result['body']['message'];
        if ($result['code'] == 200) {
            return response()->json([
                'success' => $message,
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
            File::delete($path);
        } else {
            return response()->json([
                'success' => $message[0]['error'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }
}
