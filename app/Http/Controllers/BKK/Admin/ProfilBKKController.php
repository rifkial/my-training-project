<?php

namespace App\Http\Controllers\BKK\Admin;

use App\ApiService\BKK\ConfigApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use XcS\XcTools;

class ProfilBKKController extends Controller
{
    private $configApi;

    public function __construct()
    {
        $this->configApi = new ConfigApi();
    }

    
    public function index(Request $request)
    {
        $config = $this->configApi->get_by_sekolah(session('id_sekolah'));
        $config = $config['body']['data'];
        if ($config != null) {
            $aksi = "edit";
        } else {
            $aksi = "add";
        }
        session()->put('title', "Settingan Website");
        return view('content.bkk.dashboard.bkk-admin.v_profile')->with(['aksi' => $aksi, 'config' => $config]);
    }
   
    public function layout(Request $request)
    {
        return view('content.bkk.dashboard.' . session('role') . '.v_layout')->with([]);
    }
    
    public function store(Request $request)
    {
        // dd($request);
        $data = array(
            'id' => $request['id'],
            'judul' => $request['judul'],
            'deskripsi' => $request['deskripsi'],
            'footer' => $request['footer'],
            'id_sekolah' => session('id_sekolah'),
        );
        if ($request['remove_photo']) {
            if ($request['remove_photo'][0] == "file1" && empty($request['remove_photo'][1])) {
                $data['hapus'] = 'hapus';
            } elseif ($request['remove_photo'][0] == "file" && empty($request['remove_photo'][1])) {
                $data['hapus'] = 'file';
            } elseif ($request['remove_photo'][0] && $request['remove_photo'][1]) {
                $data['hapus'] = 'file1';
            } else {
                $data['hapus'] = 'aman';
            }
        }
        // dd($data);
       
        
        $file = $request->file('image');
        $file1 = $request->file('image1');
        // dd($file1);
        $data_file = [];
        $data_file[] = array(
            $file, $file1
        );

        if ($file && empty($file1)) {
            $namaFile = $file->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/config/";
            Help::check_and_make_dir($basePath);
            $file->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->configApi->create_file(json_encode($data));
            File::delete($path);
        } elseif ($file1 && empty($file)) {
            $namaFile = $file1->getClientOriginalName();
            $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
            $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
            $basePath = "file/bkk/config/";
            Help::check_and_make_dir($basePath);
            $file1->move($basePath, $imageName);
            $path = $basePath . $imageName;
            $data['path']= $path;
            $result = $this->configApi->create_file1(json_encode($data));
            File::delete($path);
        } elseif ($file && $file1) {
            foreach ($data_file[0] as $fl) {
                // dd($fl);
                $namaFile = $fl->getClientOriginalName();
                $ext = pathinfo($namaFile, PATHINFO_EXTENSION);
                $imageName = md5(XcTools::stringAndNumber(4)) . '.' . $ext;
                $basePath = "file/bkk/config/";
                Help::check_and_make_dir($basePath);
                $fl->move($basePath, $imageName);
                $path = $basePath . $imageName;
                $data['path'][] = $path;
            }
            $result = $this->configApi->create_files(json_encode($data));
            foreach ($data['path'] as $del_path) {
                File::delete($del_path);
            }
        } else {
            $result = $this->configApi->create(json_encode($data));
        }
        // dd($result);
        if ($result['code'] == 200) {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'success', 'status' => 'berhasil']);
        } else {
            return response()->json(['success' => $result['body']['message'], 'icon'  => 'error', 'status' => 'gagal']);
        }
    }

   
}
