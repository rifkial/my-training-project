<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\JurnalApi;
use App\ApiService\Learning\KompetensiApi;
use App\ApiService\Learning\KompetensiDasarApi;
use App\ApiService\Learning\PertemuanApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Hashids\Hashids;

class JurnalController extends Controller
{
    private $jurnalApi;
    private $kompetensiApi;
    private $pertemuanApi;
    private $kompetensidasarApi;

    public function __construct()
    {
        $this->jurnalApi = new JurnalApi();
        $this->kompetensiApi = new KompetensiApi();
        $this->pertemuanApi = new PertemuanApi();
        $this->kompetensidasarApi = new KompetensiDasarApi();
        $this->hashids = new Hashids();
    }

    public function index(Request $request)
    {
        $hashids = new Hashids();
        $pertemuan = $this->pertemuanApi->get_all();
        // dd($pertemuan);
        $pertemuan = $pertemuan['body']['data'];
        $jurnal = $this->jurnalApi->get_by_room(session('id_room('.request()->segment(4).').id_mapel'),session('id_room('.request()->segment(4).').id_kelas'), session('id_room('.request()->segment(4).').id_rombel'),session('id_tahun_ajar'));
        if ($jurnal['code'] != 200) {
            $pesan = array(
                'message' => "mohon maaf, session anda di room telah habis, silahkan pilih room lagi",
                'icon' => 'error'
            );
            return redirect('/program/e_learning/rombel')->with('error_api', $pesan);
        }
        // dd($jurnal);
        $result = $jurnal['body']['data'];
        if ($request->ajax()) {
            $table = datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' .$data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i></button>';
                    return $button;
                });
            $table->rawColumns(['action']);
            $table->addIndexColumn();

            return $table->make(true);
        }
        Session::put('title', 'Jurnal Guru');
        if (session("role") == "siswa") {
            return view('content.e_learning.room.v_jurnal_guru_siswa')->with(['template' => session('template'),]);
        }
        return view('content.e_learning.room.v_jurnal_guru')->with(['template' => session('template'),  "pertemuan" => $pertemuan]);
    }

    public function get_by_id(Request $request)
    {
        $id = $request['id_kompetensi'];
        $kompetensi = $this->kompetensidasarApi->get_by_inti($id);
        $kompetensi = $kompetensi['body']['data'];
        return json_encode($kompetensi);
    }

    public function store(Request $request)
    {

        $data =
            [
                'tanggal' => date('Y-m-d', strtotime($request->tanggal)),
                'pertemuan' => $request->pertemuan,
                'bahan_ajar' => $request->materi,
                'jam' => $request->jam,
                'hambatan' => $request->hambatan,
                'id_ta_sm' => session('id_tahun_ajar'),
                'pemecahan' => $request->pemecahan,
                'id_sekolah' => session('id_sekolah'),
                'id_kelas' => session('id_room('.request()->segment(4).').id_kelas'),
                'id_rombel' => session('id_room('.request()->segment(4).').id_rombel'),
                'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
                'status' => 1
            ];
        $tes = $this->jurnalApi->create(json_encode($data));
        if ($tes['code'] == 200) {
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        }else{
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function edit(Request $request)
    {
        $id = $request['id_jurnal'];
        $post  = $this->jurnalApi->get_by_id($id);
        // dd($post);
        $result = $post['body']['data'];
        return response()->json($result);
    }


    public function update(Request $request)
    {
        $data =
            [
                'id' => $request->id,
                'tanggal' => date('Y-m-d', strtotime($request->tanggal)),
                'pertemuan' => $request->pertemuan,
                'bahan_ajar' => $request->materi,
                'jam' => $request->jam,
                'hambatan' => $request->hambatan,
                'id_ta_sm' => session('id_tahun_ajar'),
                'pemecahan' => $request->pemecahan,
                'id_sekolah' => session('id_sekolah'),
                'id_kelas' => session('id_room('.request()->segment(4).').id_kelas'),
                'id_rombel' => session('id_room('.request()->segment(4).').id_rombel'),
                'id_mapel' => session('id_room('.request()->segment(4).').id_mapel'),
                'status' => 1
            ];
        $tes = $this->jurnalApi->update_info(json_encode($data));
        if ($tes['code'] == 200) {
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        }else{
            return response()->json([
                'success' => $tes['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function trash(Request $request)
    {
        $id = $request['id_jurnal'];
        $delete = $this->jurnalApi->soft_delete($id);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => "success",
                'message' => $delete['body']['message'],
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => "error",
                'message' => $delete['body']['message'],
                'status' => 'gagal'
            ]);
        }
    }

    public function delete($id)
    {
        $delete = $this->jurnalApi->delete($id);
        // dd($delete);
        if ($delete['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function data_trash(Request $request)
    {
        Session::put('title', 'Data Trash Jurusan');
        $jurusan = $this->kompetensiApi->all_trash();
        $result = $jurusan['body']['data'];
        $template = session('template');
        // dd($template);
        if ($request->ajax()) {
            return datatables()->of($result)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" class="btn btn-success btn-sm " onclick="restoreData(' . $data['id'] . ')"><i class="fa fa-undo"></i> Pulihkan</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="btn btn-danger btn-sm" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete Permanent</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.master.jurusan.v_trash_jurusan')->with(['data' => $result, 'template' => $template]);
    }

    public function restore($id)
    {

        $restore = $this->kompetensiApi->restore($id);
        if ($restore['code'] == 200) {
            $success = true;
            $message = "data berhasil di hapus";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
