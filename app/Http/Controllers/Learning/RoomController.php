<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\CommentApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Learning\FeedApi;
use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\ApiService\Master\KelasSiswaApi;
use App\ApiService\Master\NotifikasiApi;
use App\ApiService\User\ProfileApi;
use App\Helpers\Help;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Hashids\Hashids;
use Illuminate\Support\Facades\URL;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class RoomController extends Controller
{
    private $notifApi;
    private $commentApi;
    private $feedApi;
    private $roomApi;
    private $profileApi;
    private $gurupelajaranApi;
    private $dataSiswaApi;
    private $kelasSiswaApi;
    private $hashids;
    public $perPage = 5;

    public function __construct()
    {
        $this->notifApi = new NotifikasiApi();
        $this->roomApi = new RoomApi();
        $this->kelasSiswaApi = new KelasSiswaApi();
        $this->dataSiswaApi = new DataSiswaApi();
        $this->gurupelajaranApi = new GuruPelajaranApi();
        $this->feedApi = new FeedApi();
        $this->commentApi = new CommentApi();
        $this->profileApi = new ProfileApi();
        $this->hashids = new Hashids();
    }

    public function index(Request $request, $id)
    {
        $id_room = $this->hashids->decode($id)[0];
        $feed = $this->feedApi->get_by_room($id_room);
        // dd($feed);
        $feed = $feed['body']['data'];
        $my_feed = $this->feedApi->profile();
        // dd($my_feed);
        $my_feed = end($my_feed['body']['data']);
        if (empty($my_feed)) {
            $my_feed['id_sosial'] = '';
        }
        $room = $this->roomApi->get_by_id($id_room);
        $detail = $room['body']['data'];
        // dd($detail);
        $id_code = $id;
        $profile = $this->profileApi->get_profile();
        $profile = $profile['body']['data'];
        // dd($profile);
        $template = session('template');
        if (session('role') == "siswa") {
            $id_mapel = $room['body']['data']['id_mapel'];
            Session::put('id_room(' . $id_code . ')', ['id_mapel' => $room['body']['data']['id_mapel']]);
            Session::put('id_room(' . $id_code . ').id_kelas', $room['body']['data']['id_kelas']);
            Session::put('id_room(' . $id_code . ').id_rombel', $room['body']['data']['id_rombel']);
            Session::put('id_room(' . $id_code . ').id_room', $id_room);
            Session::put('title', 'Data Sekolah');
        } elseif (session('role') == "guru") {
            Session::put('id_room(' . $id . ')', ['id_mapel' => $room['body']['data']['id_mapel']]);
            Session::put('id_room(' . $id . ').id_kelas', $room['body']['data']['id_kelas']);
            Session::put('id_room(' . $id . ').id_rombel', $room['body']['data']['id_rombel']);
            Session::put('id_room(' . $id . ').id_room', $id_room);
        }
        $this->perPage = $request->paginate ? $request->paginate : 5;
        $myCollectionObj = collect($feed);
        //dd($myCollectionObj);
        $feed = $this->paginate($myCollectionObj);
        $feed->withPath('');

        // dd($room);
        return view('content.e_learning.room.v_dashboard')->with(['template' => $template, 'feed' => $feed, 'profile' => $profile, 'my_feed' => $my_feed, 'detail' => $detail]);
    }

    public function paginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $this->perPage), $items->count(), $this->perPage, $page, $options);
    }

    public function siswa($id)
    {
        $profile = $profile['body']['data'];
        $id_room = $this->hashids->decode($id)[0];
        $room = $this->roomApi->get_by_id($id_room);
        $template = session('template');
        $feed = $this->feedApi->get_by_room($id_room);
        $feed = $feed['body']['data'];

        // dd($update);
        Session::put('title', 'Data Sekolah');
        return view('content.e_learning.room.v_dashboard')->with(['template' => $template, 'feed' => $feed, 'profile' => $profile]);
    }

    public function update_room(Request $request)
    {
        $id_code = Help::encode($request['id']);
        $data = array(
            'id' => $request['id'],
            'tahun' => session('tahun')
        );
        $aktifasi = $this->roomApi->aktifasi(json_encode($data));
        if ($aktifasi['code'] == 200) {
            $result = $aktifasi['body']['data'];
            Session::put('id_room(' . $id_code . ').id_pertemuan', $result['pertemuan_terakhir']);
            Session::put('id_room(' . $id_code . ').id_mapel', $result['id_mapel']);
            Session::put('id_room(' . $id_code . ').id_kelas', $result['id_kelas']);
            Session::put('id_room(' . $id_code . ').id_rombel', $result['id_rombel']);
            Session::put('id_room(' . $id_code . ').id_room', $result['id']);
            return response()->json([
                'message' => $aktifasi['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $aktifasi['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function guru(Request $request, $id)
    {
        $template = session('template');
        $id_room = $this->hashids->decode($id)[0];
        Session::put('id_encode', $id);
        Session::put('id_room(' . $id_room . ')', ['pertemuan' => $request['pertemuan']]);
        $room = $this->roomApi->get_by_id($id_room);
        $result = $room['body']['data'];
        Session::put('id_room(' . $id_room . ').id_rombel', $result['id_rombel']);
        Session::put('id_room(' . $id_room . ').id_mapel_kelas', $result['id_mapel_kelas']);
        Session::put('id_room(' . $id_room . ').id_room_encode', $id);
        // Session::put('id_room('.$id_room.').tes', 'dicoba');
        // dd(session('id_room('.$id_room.').pertemuan'));
        $feed = $this->feedApi->get_by_room($id_room);
        $feed = $feed['body']['data'];
        $room_data = array(
            'id' => $id_room,
            'id_rombel' => $result['id_rombel'],
            'id_sekolah' => $result['id_sekolah'],
            'id_guru' => $result['id_guru'],
            'id_mapel_kelas' => $result['id_mapel_kelas'],
            'gambar' => $result['gambar'],
            'status' => 1,
        );
        $update = $this->roomApi->update_info(json_encode($room_data));
        $siswa = $this->kelasSiswaApi->get_by_rombel($result['id_rombel']);
        // dd($siswa);
        $siswa = $siswa['body']['data'];
        foreach ($siswa as $siswas) {
            $data = [
                'id_kelas_siswa' => $siswas['id'],
                'id_room' => $id_room,
                'pertemuan' => session('id_room(' . $id_room . ').pertemuan'),
                'id_guru_pelajaran' => 5,
                'absensi' => "alfa",
                'id_mapel_kelas' => $result['id_mapel_kelas'],
                'id_sekolah' => 1,
                'status' => 3
            ];
            $tes = $this->dataSiswaApi->create(json_encode($data));
        }
        Session::put('title', 'Data Sekolah');
        return view('content.e_learning.room.v_dashboard')->with(['template' => $template, 'feed' => $feed]);
    }

    public function store(Request $request)
    {
        $insert_data = array(
            "berita" => $request['berita'],
            "id_rombel" => session('id_room(' . request()->segment(4) . ').id_rombel'),
            "id_room" => session('id_room(' . request()->segment(4) . ').id_room'),
            "nama" => session("username"),
            "role" => session("role"),
            "id_user" => session("id"),
            "id_sekolah" => session("id_sekolah"),
            "id_sosial" => "dicoba1234",
        );
        $update = $this->feedApi->create(json_encode($insert_data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        // dd($tes);
        // $key = $tes['body']['data'];
        // $data = '<div class="bg-white border mt-2">
        //     <div>
        //         <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
        //             <div class="d-flex flex-row align-items-center feed-text px-2"><img class="rounded-circle" src="' . $key['user_profil'] . '" width="45" height="45">
        //                 <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">' . $key['pengirim'] . '</span><span class="text-black-50 time">' . $key['terbit'] . '</span></div>
        //             </div>
        //             <div class="feed-icon px-2"><i class="fa fa-ellipsis-v text-black-50"></i></div>
        //         </div>
        //     </div>
        //     <div class="p-2 px-3"><span>' . $key['berita'] . '</span></div>
        //     <div class="d-flex justify-content-end socials p-2 py-3"><i class="fa fa-thumbs-up"></i><a href="' . url('program/e_learning/room/' . request()->segment(4) . '/postingan/detail', $this->hashids->encode($key['id'])) . '"><i class="fa fa-comments-o"><span class="count">' . $key['jumlah_komentar'] . '</span></a></i> <i class="fa fa-share"></i></div>
        // </div>';
        // return response()->json($data);
    }

    public function update(Request $request)
    {
        // dd($request);
        $data_update = array(
            "id" => $request['id'],
            "berita" => $request['title'],
            "id_rombel" => session('id_room(' . request()->segment(4) . ').id_rombel'),
            "id_room" => session('id_room(' . request()->segment(4) . ').id_room'),
            "nama" => session("username"),
            "role" => session("role"),
            "id_user" => session("id"),
            "id_sekolah" => session("id_sekolah"),
        );
        // dd($data_update);
        $update = $this->feedApi->create(json_encode($data_update));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function soft_delete(Request $request)
    {
        $delete = $this->feedApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function delete_comment(Request $request)
    {
        $delete = $this->commentApi->soft_delete($request['id']);
        if ($delete['code'] == 200) {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $delete['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detail_post($id)
    {
        $my_feed = $this->feedApi->profile();
        // dd($my_feed);
        $my_feed = end($my_feed['body']['data']);
        if (empty($my_feed)) {
            $my_feed['id_sosial'] = '';
        }
        $ids = request()->segment(7);
        // dd(url()->current());
        $hashids = new Hashids();
        $id_post = $hashids->decode($ids)[0];
        $feed = $this->feedApi->get_by_id($id_post);
        // dd($feed);
        $feed = $feed['body']['data'];
        Session::put('title', 'Data Sekolah');
        return view('content.e_learning.room.v_post_detail')->with(['template' => session('template'), 'feed' => $feed, 'my_feed' => $my_feed]);
    }

    public function comment(Request $request)
    {
        $idfeed = intval($request['id_feed']);
        $feed = $this->feedApi->get_by_id($request['id_feed']);
        // dd($feed);
        $feed = $feed['body']['data'];
        $insert_data = array(
            "id_feed" => $idfeed,
            "komentar" => $request['komentar'],
            "nama" => session("username"),
            "id_user" => $feed['id_user'],
            "role_feed" => $feed['role'],
            "id_rombel" => session('id_room(' . request()->segment(4) . ').id_rombel'),
            "role" => session("role"),
            "id_sekolah" => session("id_sekolah"),
            "id_sosial" => "tes",
            "id_room" => session('id_room(' . request()->segment(4) . ').id_room'),
            "url" => url()->previous(),
        );
        $update = $this->commentApi->create(json_encode($insert_data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
        // // dd($feed);
        // $feed = $feed['body']['data'];
        // $data = '
        // <ul class="comments-list">
        //       <li class="comment">
        //           <a class="pull-left" href="#">
        //               <img class="avatar" src="' . $feed['user_profil'] . '" alt="avatar">
        //           </a>
        //           <div class="comment-body">
        //               <div class="comment-heading">
        //                   <h4 class="user">' . $feed['nama'] . '</h4>
        //                   <h5 class="time">' . $feed['terbit'] . '</h5>
        //               </div>
        //               <p>' . $feed['komentar'] . '</p>
        //           </div>
        //       </li>
        //   </ul>
        // ';
        // return response()->json($data);
    }

    public function comment_update(Request $request)
    {
        $comment = $this->commentApi->get_by_id($request['id']);
        $comment = $comment['body']['data'];
        // dd($comment);
        $data_update = array(
            "id" => $request['id'],
            "komentar" => $request['title'],
            "nama" => $comment['nama'],
            "role" => session("role"),
            "id_feed" => $comment['id_feed'],
            "id_sekolah" => session("id_sekolah"),
            "id_sosial" => $comment['id_sosial'],
            "id_room" => $comment['id_room'],
        );
        // dd($data_update);
        $update = $this->commentApi->update_info(json_encode($data_update));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function absensi(Request $request)
    {
        // dd($request);
        $update = $this->dataSiswaApi->update_status_absen($request['id_room']);
        // dd($update);
        if ($update['code'] == 200) {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'success',
                'status' => 'berhasil'
            ]);
        } else {
            return response()->json([
                'message' => $update['body']['message'],
                'icon'  => 'error',
                'status' => 'gagal'
            ]);
        }
    }

    public function logout_session($id)
    {
        $hashids = new Hashids();
        $id_room = $hashids->decode($id)[0];
        session()->forget('id_encode');
        $room = $this->roomApi->get_by_id($id_room);
        $result = $room['body']['data'];
        $data = array(
            'id' => $id_room,
            'kode' => $result['kode'],
            'id_kelas' => $result['id_kelas'],
            'id_guru' => $result['id_guru'],
            'id_rombel' => $result['id_rombel'],
            'id_mapel' => $result['id_mapel'],
            'id_sekolah' => $result['id_sekolah'],
            'status' => 2
        );
        $room = $this->roomApi->update_info(json_encode($data));
        return redirect()->route('room-rombel');
    }

    public function read(Request $request)
    {
        // dd($request);
        $read = $this->notifApi->baca($request['id']);
        if ($read['code'] == 200) {
            return response()->json([
                'success' => $read['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $read['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }
   
    public function read_all()
    {
        $data_insert = array(
            'role' => session('role')
        );
        $this->notifApi->read_all(json_encode($data_insert), session('id'));
        return redirect()->back();
    }

    public function update_virtual(Request $request)
    {
        // dd($request);
        if ($request['params'] == 'update') {
            $data = array(
                'link_virtual' => $request['link_virtual']
            );
        } else {
            $data = array(
                'link_virtual' => null
            );
        }
        
        $update = $this->roomApi->update_virtual($request['id_room'], json_encode($data));
        if ($update['code'] == 200) {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'success',
                'status'  => 'berhasil'
            ]);
        } else {
            return response()->json([
                'success' => $update['body']['message'],
                'icon'  => 'error',
                'status'  => 'gagal'
            ]);
        }
    }

    public function detailRoom(Request $request)
    {
        $room = $this->roomApi->get_by_id($request['id']);
        $room = $room['body']['data'];
        return response()->json($room);
        // dd($room);
    }
}
