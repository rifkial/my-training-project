<?php

namespace App\Http\Controllers\Learning;

use App\ApiService\Learning\BahanAjarApi;
use App\ApiService\Master\GuruPelajaranApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BahanAjarWaliKelasController extends Controller
{
    private $bahanAjarApi;
    private $gurupelajaranApi;

    public function __construct()
    {
        $this->bahanAjarApi = new BahanAjarApi();
        $this->gurupelajaranApi = new GuruPelajaranApi();
    }

    public function index(Request $request)
    {
        $pelajaran = $this->gurupelajaranApi->get_by_rombel(session('id_rombel'));
        // dd($pelajaran);
        $pelajaran = $pelajaran['body']['data'];
        Session::put('title', 'Bahan Ajar');
        return view('content.e_learning.rombel.bahan_ajar.v_bahan_ajar')->with(['template' => session('template'), 'mapel' => $pelajaran]);
    }

    public function get_datatable(Request $request)
    {
        // dd($request);
        $id_mapel = $request['id_mapel'];
        if ($id_mapel != null) {
            $kelas = $this->bahanAjarApi->get_by_rombel_tahun(session('id_rombel'), $id_mapel, session('id_tahun_ajar'));
            $result = $kelas['body']['data'];
        } else {
            $result = [];
        }
        // dd($result);
        $table = datatables()->of($result)
            ->addColumn('action', function ($data) {
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data['id'] . '" data-original-title="Edit" class="edit editData-'.$data['id'].' btn btn-info btn-xs edit"><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<button type="button" name="delete" data-id="' . $data['id'] . '" class="delete-'.$data['id'].' btn btn-danger btn-xs" onclick="deleteData(' . $data['id'] . ')"><i class="fa fa-trash"></i> Delete</button>';
                return $button;
            });
        $table->editColumn('download', function ($rows) {
            return '<a href="bahan_ajar/download/'.$rows['id'].'" class="btn btn-warning btn-sm">'.$rows['nama_file'].' <i class="fa fa-download"></i></a>';
        });
        $table->rawColumns(['action', 'download']);
        $table->addIndexColumn();

        return $table->make(true);
    }

    public function download($id)
    {
        // dd($id);
        $id_bahan_ajar = request()->segment(6);
        $post  = $this->bahanAjarApi->get_by_id($id_bahan_ajar);
        $result = $post['body']['data'];
        $nama_file = explode('/', $result['nama_file']);
        $nama = $nama_file[1];
        $fileSource = $result['file'];
        $fileName   = $nama;
        $headers = ['Content-Type: application/pdf'];
        $pathToFile = storage_path('app/' . $fileName);
        $getContent = $this->curl_get_file_contents($fileSource);
        file_put_contents($pathToFile, $getContent);
        return response()->download($pathToFile, $fileName, $headers);
    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) {
            return $contents;
        } else {
            return false;
        }
    }
}
