<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
//use Session;

class Api
{
    public $urlApi;

    public static function ApiGET($url)
    {
        // dd($url);
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);
        return $result;
    }

    public static function ApiPOST($url, $data)
    {
        $token = Session::get('token');
        $string = json_encode($data);
        // var_dump($string);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));

        $result_curl = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);

        return $result;
    }

    public static function ApiPUT($url, $data)
    {
        $token = Session::get('token');
        $string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $result_curl = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);
        return $result;
    }

    public static function ApiDELETE($url)
    {
        $token = Session::get('token');
        //$string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $result_curl = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);
        return $result;
    }

    public function __construct()
    {
        $this->urlApi = env("API_URL", "http://localhost:8100/");
    }

    public function login($username, $password, $role)
    {
        $data1 = [
            'username' => $username,
            'password' => $password,
            'role' => $role,
            'last_ip' => request()->getClientIp()
        ];

        //dd($data1);

        $url = ($role == "superadmin") ? $this->urlApi . "api/super/login"  : $this->urlApi . "api/auth/login";
        // dd($url);
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data1),
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "content-type: application/json",
            ),
        ));
        $result_curl = curl_exec($ch);
        // dd($result_curl);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);
        // dd($result);
        return $result;
    }

    // public function firstLogin($id)
    // {
    //     $token = Session::get('token');
    //     $url = $this->urlApi . "api/auth/first_login/" . $id;
    //     $ch = curl_init($url);
    //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //         'Accept: application/json',
    //         'Content-Type: application/json',
    //         'Authorization: Bearer ' . $token,
    //     ));
    //     $result_curl = curl_exec($ch);
    //     $err = curl_error($ch);
    //     curl_close($ch);
    //     $result = json_decode($result_curl, true);

    //     return $result;
    // }

    public function savePassword($data, $id)
    {
        $url = $this->urlApi . "api/auth/change_password/" . $id;
        return $this->ApiPUT($url, $data);
    }

    public function resetPassword($data)
    {
        $url = $this->urlApi . "api/auth/reset_password";
        return $this->ApiPOST($url, $data);
    }

    public function checkUserEmail($data)
    {
        $url = $this->urlApi . "api/auth/check_user_email";
        return $this->ApiPOST($url, $data);
    }

    public function getUserByToken()
    {
        $token = Session::get('token');
        $url = $this->urlApi . "api/data/user/";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $result_curl = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);

        return $result;
    }

    public function logout()
    {
        // dd("ping");
        $token = Session::get('token');
        $url = $this->urlApi . "api/auth/logout";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $result_curl = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result_curl, true);

        return $result;
    }

    public function signup($data)
    {
        $url = $this->urlApi . "api/auth/signup";
        return $this->ApiPOST($url, $data);
    }

    public function getAllUser($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/user?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        return Api::ApiGET($url);
    }

    public function getUserById($id)
    {
        $url = $this->urlApi . "api/data/user/" . $id;
        return Api::ApiGET($url);
    }

    public function saveUpdate($data, $id)
    {
        $url = $this->urlApi . "api/data/user/" . $id;
        return $this->ApiPUT($url, $data);
    }

    public function getEnabledUser()
    {
        $url = $this->urlApi . "api/data/enabled/user";
        return Api::ApiGET($url);
    }

    
}
