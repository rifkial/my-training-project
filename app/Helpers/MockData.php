<?php

namespace App\Helpers;

class MockData
{
    private static $mockInstance = null;

    private $dataSiswa;
    private $dataSPP;

    private function __construct()
    {
        $this->dataSiswa = $this->factorySiswa();
        $this->dataSPP = $this->factorySPPSiswa($this->dataSiswa);
    }

    public static function getMockData()
    {
        if (!isset(self::$mockInstance)) {
            self::$mockInstance = new MockData();
        }
        return self::$mockInstance;
    }

    public function getDataSiswa()
    {
        return $this->dataSiswa;
    }

    public function getOneDataSiswa($id_siswa)
    {
        foreach ($this->getDataSiswa() as $siswa) {
            if ($siswa['id'] == $id_siswa) {
                return $siswa;
            }
        }
    }

    public function getDataSPP($dataSiswa)
    {
        $spp = [];
        foreach ($this->dataSPP as $data) {
            if ($data['id_siswa'] === $dataSiswa['id']) {
                array_push($spp, $data);
            }
        }
        return $spp;
    }

    public function getOneDataSPP($id_spp)
    {
        foreach ($this->dataSPP as $spp) {
            if ($spp['id'] == $id_spp) {
                return $spp;
            }
        }
    }

    private function factorySiswa()
    {
        $data = [];

        $nama = [
            "Ahmad Dani",
            "Mamas Mamad",
            "Husen Bahesti",
            "Jenang Kudus",
            "Mas Ikan",
            "Stussy Susanti",
            "Wandy Chang",
            "Nelly Sasa",
            "Tama Gochi",
            "Ilham Walillah",
        ];

        function createNIS($i)
        {
            $nisInitial = 111;
            return "" . ($nisInitial + $i) . "";
        }

        function createKelas($index)
        {
            if ($index < 4) {
                return "XII";
            } else if ($index < 7) {
                return "XI";
            }

            return "X";
        }

        foreach ($nama as $i => $item) {
            array_push($data, [
                "id" => $i + 1,
                "nis" => createNIS($i),
                "nama" => $item,
                "kelas" => createKelas($i),
                // TODO:
            ]);
        }

        return $data;
    }

    private function factorySPPSiswa($listSiswa)
    {
        $data = [];

        $lastIdSPP = 0;

        $bulanTahunAjaran = [
            "Juli", "Agustus", "September", "Oktober", "November", "Desember", "Januari",
            "Februari", "Maret", "April", "Mei", "Juni"
        ];

        foreach ($listSiswa as $siswa) {
            foreach ($bulanTahunAjaran as $i => $bulan) {
                array_push($data, [
                    "id" => $lastIdSPP + 1,
                    "id_siswa" => $siswa['id'],
                    "label" => "$bulan " . ($i < 6 ? 2020 : 2021),
                    "jumlah" => 100000,
                ]);

                $lastIdSPP++;
            }
        }
        return $data;
    }
}
