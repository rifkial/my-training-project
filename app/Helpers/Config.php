<?php

namespace App\Helpers;;

use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\ProgramSekolahApi;
use App\Exports\SekolahExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
// use Excel;
use PDF;

class Config
{
    public static function template()
    {
        // $program = (new static)->ProgramSekolahApi->get_by_id_program_sekolah();
        $program = App\ApiService\Master\ProgramSekolahApi::get_by_id_program_sekolah();
        // dd($sekolah);
        $result = $program['body']['data'];
        // dd($result);

        return $result;
    }
}
