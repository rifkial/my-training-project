<?php

namespace App\Helpers;

use CURLFile;
use Exception;
use MultipleIterator;
//use Session;
use Illuminate\Support\Facades\Session;
use Pusher\Pusher;

class ApiService
{

    public static function request($endPoint, $method, $body = null)
    {
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        $url = $endPoint;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            // 'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result_curl = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        $data = json_decode($result_curl, true);

        if ($info == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $info,
            "body" => $data
        );
        // dd("ping");
        // dd($result);
        return $result;
    }

    public static function request_image($endPoint, $method, $body = null)
    {
        $url = $endPoint;
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        $tes = json_decode($body);
        $filePath =  $tes->path;
        $pecah_array = get_object_vars(json_decode($body));
        $get_label = array_keys($pecah_array);
        $get_value = array_values($pecah_array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $postFields = array();
        if (function_exists('curl_file_create')) {
            $filePath = new CURLFile($filePath);
        } else {
            $filePath = '@' . realpath($filePath);
        }
        $postFields["file"] = $filePath;
        foreach (array_combine($get_label, $get_value) as $f => $n) {
            $postFields[$f] = $n;
        }
        //dd($postFields);
        unset($postFields['path']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($response, true);
        return array(
            'code' => $httpcode,
            'body' => $result
        );
    }

    public static function request_image_sampul($endPoint, $method, $body = null)
    {
        $url = $endPoint;
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        $tes = json_decode($body);
        $filePath =  $tes->path;
        $pecah_array = get_object_vars(json_decode($body));
        $get_label = array_keys($pecah_array);
        $get_value = array_values($pecah_array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $postFields = array();
        if (function_exists('curl_file_create')) {
            $filePath = new CURLFile($filePath);
        } else {
            $filePath = '@' . realpath($filePath);
        }
        $postFields["file1"] = $filePath;
        foreach (array_combine($get_label, $get_value) as $f => $n) {
            $postFields[$f] = $n;
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($response, true);
        return array(
            'code' => $httpcode,
            'body' => $result
        );
    }

    public static function request_images($endPoint, $method, $body = null)
    {
        $url = $endPoint;
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        $tes = json_decode($body);
        $filePath =  $tes->path[0];
        $filePaths =  $tes->path[1];
        $pecah_array = get_object_vars(json_decode($body));
        $get_label = array_keys($pecah_array);
        $get_value = array_values($pecah_array);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $postFields = array();
        if (function_exists('curl_file_create')) {
            $filePath = new CURLFile($filePath);
            $filePaths = new CURLFile($filePaths);
        } else {
            $filePath = '@' . realpath($filePath);
            $filePaths = '@' . realpath($filePaths);
        }

        $postFields["file"] = $filePath;
        $postFields["file1"] = $filePaths;
        foreach (array_combine($get_label, $get_value) as $f => $n) {
            $postFields[$f] = $n;
        }
        $postFields["path"] = null;
        // dd($postFields);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($response, true);
        return array(
            'code' => $httpcode,
            'body' => $result
        );
    }

    function apiUploadFile($endPoint, $full_path)
    {
        $ci = &get_instance();
        $token = ifNull($ci->session->userdata("token_cms_dpg"), "xx");
        $apiUrl = $ci->config->item("api_url") . $endPoint;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('file' => new CURLFILE($full_path)),
            CURLOPT_HTTPHEADER => array(
                "content-type: multipart/form-data",
                "token: $token"
            ),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $result = json_decode($response, true);
        return array(
            'kode' => $httpcode,
            'data' => $result
        );
    }

    public static function requestToken($endPoint, $baseAuth)
    {
        $url = $endPoint;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "content-type: application/json",
            "Content-Length: 0",
            "Authorization: Basic $baseAuth"
        ));

        $result_curl = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        $data = json_decode($result_curl, true);

        $result = array(
            "code" => $info,
            "body" => $data
        );
        return $result;
    }

    public static function request_image1($endPoint, $method, $body = null)
    {
        $url = $endPoint;
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        $tes = json_decode($body);
        $filePath =  $tes->path;
        $pecah_array = get_object_vars(json_decode($body));
        $get_label = array_keys($pecah_array);
        $get_value = array_values($pecah_array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $postFields = array();
        if (function_exists('curl_file_create')) {
            $filePath = new CURLFile($filePath);
        } else {
            $filePath = '@' . realpath($filePath);
        }
        $postFields["file1"] = $filePath;
        foreach (array_combine($get_label, $get_value) as $f => $n) {
            $postFields[$f] = $n;
        }
        unset($postFields['path']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($response, true);
        return array(
            'code' => $httpcode,
            'body' => $result
        );
    }

    public static function request_images_setting($endPoint, $method, $body = null)
    {
        $url = $endPoint;
        $token = (Session::get('token') <> NULL) ? Session::get('token') : "token";
        //$tes = json_decode($body);

        $pecah_array = get_object_vars(json_decode($body));
        $get_label = array_keys($pecah_array);
        $get_value = array_values($pecah_array);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token,
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $postFields = array();
        foreach (array_combine($get_label, $get_value) as $f => $n) {
            $postFields[$f] = $n;
        }

       if((!empty($postFields['logo_pertama'])) && (!empty($postFields['logo_kedua']))
           && (!empty($postFields['stempel'])) && (!empty($postFields['ttd_kepsek']))
        ){
            if (function_exists('curl_file_create')) {
                $filePath = new CURLFile($postFields['logo_pertama']);
                $filePaths = new CURLFile($postFields['logo_kedua']);
                $filePathx = new CURLFile($postFields['stempel']);
                $filePathv = new CURLFile($postFields['ttd_kepsek']);
            } else {
                $filePathv = '@' . realpath($postFields['logo_pertama']);
                $filePaths = '@' . realpath($postFields['logo_kedua']);
                $filePathx = '@' . realpath($postFields['stempel']);
                $filePath = '@' . realpath($postFields['ttd_kepsek']);
            }

            $postFields["ttd_kepsek"] = $filePathv;
            $postFields["logo1"] = $filePath;
            $postFields["logo2"] = $filePaths;
            $postFields["stempel"] = $filePathx;

            //unset

            unset($postFields['logo_pertama']);
            unset($postFields['logo_kedua']);

        }else{
            if(!empty($postFields['logo_pertama'])){
                if (function_exists('curl_file_create')) {
                    $filePath = new CURLFile($postFields['logo_pertama']);
                } else {
                    $filePath = '@' . realpath($postFields['logo_pertama']);
                }
                $postFields["logo1"] = $filePath;
                unset($postFields['logo_pertama']);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
            if(!empty($postFields['logo_kedua'])){
                if (function_exists('curl_file_create')) {
                    $filePaths = new CURLFile($postFields['logo_kedua']);
                } else {
                    $filePaths = '@' . realpath($postFields['logo_kedua']);
                }
                $postFields["logo2"] = $filePaths;
                unset($postFields['logo_kedua']);

            }
            if(!empty($postFields['stempel'])){
                if (function_exists('curl_file_create')) {
                    $filePathx = new CURLFile($postFields['stempel']);
                } else {
                    $filePathx = '@' . realpath($postFields['stempel']);
                }
                $postFields["stempel"] = $filePathx;
            }
            if(!empty($postFields['ttd_kepsek'])){
                if (function_exists('curl_file_create')) {
                    $filePathv = new CURLFile($postFields['ttd_kepsek']);
                } else {
                    $filePathv = '@' . realpath($postFields['ttd_kepsek']);
                }
                $postFields["ttd_kepsek"] = $filePathv;
            }
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($response, true);
        return array(
            'code' => $httpcode,
            'body' => $result
        );
    }

     /** setting pusher  */
     public static function config_pusher()
     {
        $url = env("API_URL")."api/data/master/environment/sekolah/".Session::get('id_sekolah');
        $seetingApi = ApiService::request($url, "GET", null);
        $parseApi   = $seetingApi['body']['data'] ?? array();
        $app_id ='';
        $app_secret ='';
        $app_key    = '';
        $app_cluster ='';

        if(!empty($parseApi)){
            foreach($parseApi as $key => $val){
                if($val['jenis'] == 'notification'){
                    if($val['token'] !='-' && $val['status'] == '1'){
                        if($val['kode'] == 'PUSHER_APP_KEY'){
                            $app_key = $val['token'];
                        }else if($val['kode'] == 'PUSHER_APP_SECRET'){
                            $app_secret = $val['token'];
                        }else if($val['kode'] == 'PUSHER_APP_CLUSTER'){
                            $app_cluster = $val['token'];
                        }else if($val['kode'] == 'PUSHER_APP_ID'){
                            $app_id = $val['token'];
                        }
                    }
                }
            }

        }else{
            $app_id = env('PUSHER_APP_ID');
            $app_secret = env('PUSHER_APP_SECRET');
            $app_key    = env('PUSHER_APP_KEY');
            $app_cluster = env('PUSHER_APP_CLUSTER');
        }

         $pusher = new Pusher($app_key, $app_secret, $app_id, ['cluster' => $app_cluster]);
         return $pusher;
     }

     /** push message notification  */
     public static function push_notif($channel,$event,$message){
        $notif = ApiService::config_pusher();
        $notif->trigger($channel, $event, $message);
     }

    /**
     * curl file download
     * @param $url
     */

    public function force_download_curl($url)
    {
        $token = Session::get('token');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token,
        ));
        $saveTo = basename($url);
        //Open file handler.
        $fp = fopen($saveTo, 'w+');
        //Pass our file handle to cURL.
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $result_curl = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);
        //Close the file handler.
        fclose($fp);
        $data = json_decode($result_curl, true);
        if ($httpcode == 401) {
            return redirect(route('auth.login'));
        }

        $result = array(
            "code" => $httpcode,
            "body" => $data,
            "filename" => $saveTo
        );

        return $result;
    }
}
