<?php
namespace App\Util;

class Utils
{
    public static function createLinksMetronic($result, $route, $search, $status, $sort)
    {
        $limit = $result["per_page"];
        $last = $result['last_page'];
        $page = $result['current_page'];
        $links = $result['total'];


        $page_no = $result['current_page'];
        $total_records_per_page = $result["per_page"];
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = '2';

        $total_records = $result['total'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1;


        $html = '<div style="padding: 10px 20px 0px; border-top: dotted 1px #CCC;">';
        $html .= '<ul class="pagination">';

        $page_no <= 1 ? $dis = 'disabled' : $dis = '';
        $html .= '<li class="'.$dis.'">';
        if ($page_no > 1) {
            $html .= '<a href="' . route($route) . '?per_page=' . $limit . '&page=' . $previous_page . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . '">Previous</a>';
        }
        $html .= '</li>';

        if ($total_no_of_pages <= 10) {
            for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                if ($counter == $page_no) {
                    $html .= "<li class='active'><a>$counter</a></li>";
                } else {
                    $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $counter . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$counter</a></li>";
                }
            }
        } elseif ($total_no_of_pages > 10) {
            if ($page_no <= 4) {
                for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $counter . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $second_last . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $total_no_of_pages . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$total_no_of_pages</a></li>";
            } elseif ($page_no > 4 && $page_no < $total_no_of_pages -4) {
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=1&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=2&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $counter . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $second_last . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $total_no_of_pages . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$total_no_of_pages</a></li>";
            } else {
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=1&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=2&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $counter . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>$counter</a></li>";
                    }
                }
            }
        }
        $page_no >= $total_no_of_pages ? $disb = "disabled" : $disb = "";
        $html .= '<li class="'.$disb.'">';
        if ($page_no < $total_no_of_pages) {
            $html .= "<a href='" . route($route) . '?per_page=' . $limit . '&page=' . $next_page . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>Next</a>";
        } else {
            $html .= "<a>Next</a>";
        }
        $html .= '</li>';

        if ($page_no < $total_no_of_pages) {
            $html .= "<li><a href='" . route($route) . '?per_page=' . $limit . '&page=' . $total_no_of_pages . '&search=' . $search . '&status=' . $status . '&sort=' . $sort . "'>Last &rsaquo;&rsaquo;</a></li>";
        }


        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }

    public static function filterSimple($result, $route, $search)
    {
        $page_no = $result['current_page'];
        $total_records_per_page = $result["per_page"];
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = '2';

        $total_records = $result['total'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1;


        $html = '<div style="padding: 10px 20px 0px; border-top: dotted 1px #CCC;">';
        $html .= '<ul class="pagination">';

        $page_no <= 1 ? $dis = 'disabled' : $dis = '';
        $html .= '<li class="'.$dis.'">';
        if ($page_no > 1) {
            $html .= '<a href="' . route($route) . '?page=' . $previous_page . '&search=' . $search . '">Previous</a>';
        }
        $html .= '</li>';

        if ($total_no_of_pages <= 10) {
            for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                if ($counter == $page_no) {
                    $html .= "<li class='active'><a>$counter</a></li>";
                } else {
                    $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                }
            }
        } elseif ($total_no_of_pages > 10) {
            if ($page_no <= 4) {
                for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } elseif ($page_no > 4 && $page_no < $total_no_of_pages -4) {
                $html .= "<li><a href='" . route($route) . '?page=1&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&search=' . $search  . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } else {
                $html .= "<li><a href='" . route($route) . '?page=1&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
            }
        }
        $page_no >= $total_no_of_pages ? $disb = "disabled" : $disb = "";
        $html .= '<li class="'.$disb.'">';
        if ($page_no < $total_no_of_pages) {
            $html .= "<a href='" . route($route) . '?page=' . $next_page . '&search=' . $search . "'>Next</a>";
        } else {
            $html .= "<a>Next</a>";
        }
        $html .= '</li>';

        if ($page_no < $total_no_of_pages) {
            $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&search=' . $search . "'>Last &rsaquo;&rsaquo;</a></li>";
        }


        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }
    
    public static function filterAlumni($result, $route, $id_fakultas, $id_jurusan, $angkatan,  $search)
    {
        $page_no = $result['current_page'];
        $total_records_per_page = $result["per_page"];
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = '2';

        $total_records = $result['total'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1;


        $html = '<div style="padding: 10px 20px 0px; border-top: dotted 1px #CCC;">';
        $html .= '<ul class="pagination">';

        $page_no <= 1 ? $dis = 'disabled' : $dis = '';
        $html .= '<li class="'.$dis.'">';
        if ($page_no > 1) {
            $html .= '<a href="' . route($route) . '?page=' . $previous_page . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . '">Previous</a>';
        }
        $html .= '</li>';

        if ($total_no_of_pages <= 10) {
            for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                if ($counter == $page_no) {
                    $html .= "<li class='active'><a>$counter</a></li>";
                } else {
                    $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>$counter</a></li>";
                }
            }
        } elseif ($total_no_of_pages > 10) {
            if ($page_no <= 4) {
                for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } elseif ($page_no > 4 && $page_no < $total_no_of_pages -4) {
                $html .= "<li><a href='" . route($route) . '?page=1&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search  . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } else {
                $html .= "<li><a href='" . route($route) . '?page=1&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
            }
        }
        $page_no >= $total_no_of_pages ? $disb = "disabled" : $disb = "";
        $html .= '<li class="'.$disb.'">';
        if ($page_no < $total_no_of_pages) {
            $html .= "<a href='" . route($route) . '?page=' . $next_page . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>Next</a>";
        } else {
            $html .= "<a>Next</a>";
        }
        $html .= '</li>';

        if ($page_no < $total_no_of_pages) {
            $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&fakultas=' . $id_fakultas . '&jurusan=' . $id_jurusan . '&angkatan=' . $angkatan . '&search=' . $search . "'>Last &rsaquo;&rsaquo;</a></li>";
        }
        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }
   
    public static function simpleWithStatus($result, $route, $status, $search)
    {
        $page_no = $result['current_page'];
        $total_records_per_page = $result["per_page"];
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = '2';

        $total_records = $result['total'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1;


        $html = '<div style="padding: 10px 20px 0px; border-top: dotted 1px #CCC;">';
        $html .= '<ul class="pagination">';

        $page_no <= 1 ? $dis = 'disabled' : $dis = '';
        $html .= '<li class="'.$dis.'">';
        if ($page_no > 1) {
            $html .= '<a href="' . route($route) . '?page=' . $previous_page . '&status=' . $status .'&search=' . $search . '">Previous</a>';
        }
        $html .= '</li>';

        if ($total_no_of_pages <= 10) {
            for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                if ($counter == $page_no) {
                    $html .= "<li class='active'><a>$counter</a></li>";
                } else {
                    $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&status=' . $status .'&search=' . $search . "'>$counter</a></li>";
                }
            }
        } elseif ($total_no_of_pages > 10) {
            if ($page_no <= 4) {
                for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&status=' . $status .'&search=' . $search . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&status=' . $status .'&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&status=' . $status .'&search=' . $search . "'>$total_no_of_pages</a></li>";
            } elseif ($page_no > 4 && $page_no < $total_no_of_pages -4) {
                $html .= "<li><a href='" . route($route) . '?page=1&status=' . $status .'&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&status=' . $status .'&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&status=' . $status .'&search=' . $search  . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?page=' . $second_last . '&status=' . $status .'&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&status=' . $status .'&search=' . $search . "'>$total_no_of_pages</a></li>";
            } else {
                $html .= "<li><a href='" . route($route) . '?page=1&status=' . $status .'&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?page=2&status=' . $status .'&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?page=' . $counter . '&status=' . $status .'&search=' . $search . "'>$counter</a></li>";
                    }
                }
            }
        }
        $page_no >= $total_no_of_pages ? $disb = "disabled" : $disb = "";
        $html .= '<li class="'.$disb.'">';
        if ($page_no < $total_no_of_pages) {
            $html .= "<a href='" . route($route) . '?page=' . $next_page . '&status=' . $status .'&search=' . $search . "'>Next</a>";
        } else {
            $html .= "<a>Next</a>";
        }
        $html .= '</li>';

        if ($page_no < $total_no_of_pages) {
            $html .= "<li><a href='" . route($route) . '?page=' . $total_no_of_pages . '&status=' . $status .'&search=' . $search . "'>Last &rsaquo;&rsaquo;</a></li>";
        }


        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }
    
    public static function dataSiswa($jurusans, $tahuns, $result, $route, $search)
    {
        $page_no = $result['current_page'];
        $total_records_per_page = $result["per_page"];
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = '2';

        $total_records = $result['total'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1;


        $html = '<div style="padding: 10px 20px 0px; border-top: dotted 1px #CCC;">';
        $html .= '<ul class="pagination">';

        $page_no <= 1 ? $dis = 'disabled' : $dis = '';
        $html .= '<li class="'.$dis.'">';
        if ($page_no > 1) {
            $html .= '<a href="' . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $previous_page . '&search=' . $search . '">Previous</a>';
        }
        $html .= '</li>';

        if ($total_no_of_pages <= 10) {
            for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                if ($counter == $page_no) {
                    $html .= "<li class='active'><a>$counter</a></li>";
                } else {
                    $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                }
            }
        } elseif ($total_no_of_pages > 10) {
            if ($page_no <= 4) {
                for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $second_last . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $total_no_of_pages . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } elseif ($page_no > 4 && $page_no < $total_no_of_pages -4) {
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=1&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=2&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $counter . '&search=' . $search  . "'>$counter</a></li>";
                    }
                }
                $html .= '<li><a>...</a></li>';
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $second_last . '&search=' . $search . "'>$second_last</a></li>";
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $total_no_of_pages . '&search=' . $search . "'>$total_no_of_pages</a></li>";
            } else {
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=1&search=' . $search . "'>1</a></li>";
                $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=2&search=' . $search . "'>2</a></li>";
                $html .= '<li><a>...</a></li>';

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                        $html .= "<li class='active'><a>$counter</a></li>";
                    } else {
                        $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $counter . '&search=' . $search . "'>$counter</a></li>";
                    }
                }
            }
        }
        $page_no >= $total_no_of_pages ? $disb = "disabled" : $disb = "";
        $html .= '<li class="'.$disb.'">';
        if ($page_no < $total_no_of_pages) {
            $html .= "<a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $next_page . '&search=' . $search . "'>Next</a>";
        } else {
            $html .= "<a>Next</a>";
        }
        $html .= '</li>';

        if ($page_no < $total_no_of_pages) {
            $html .= "<li><a href='" . route($route) . '?tahun=' . $tahuns . '&jurusan=' . $jurusans . '&page=' . $total_no_of_pages . '&search=' . $search . "'>Last &rsaquo;&rsaquo;</a></li>";
        }
        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }


    


    public static function getPerPage()
    {
        $pages = array(5, 10, 25, 50, 100);
        return $pages;
    }
    public static function getSort()
    {
        $sorts = array("asc", "desc");
        return $sorts;
    }
    public static function getStatus()
    {
        $statuses = array(
            array('label' => 'All', 'value' => ''),
            array('label' => 'Tidak Aktif', 'value' => '0'),
            array('label' => 'Aktif', 'value' => '1'),
        );
        return $statuses;
    }
    public static function imageToBase64($image, $namaFile)
    {
        $image->move('img/data/', $namaFile);
        $data = file_get_contents('img/data/' . $namaFile);
        $base64 = base64_encode($data);
        @unlink('img/data/' . $namaFile);
        return $base64;
    }

    public static function generateRandomString($length)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function errorHandle($result)
    {
        if (!isset($result['data'])) {
            return 0;
        }
        return 1;
    }
}
