<?php

namespace App\Exports;

use App\ApiService\Master\SekolahApi;
use Maatwebsite\Excel\Concerns\FromCollection;

class SekolahExport implements FromCollection
{
    private $sekolahApi;

    public function __construct()
    {
        $this->sekolahApi = new SekolahApi();
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $sekolah = $this->sekolahApi->get_all();
        // dd($sekolah);
        $result = $sekolah['body']['data'];
        // dd($result);
        return $result;
    }
}
