<?php

namespace App\ApiService\Sarana\Pengadaan;

use App\Helpers\ApiService;

class PengadaanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/pengadaan";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/pengadaan/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function status_pengadaan($id){
        $url = $this->urlApi . "api/data/sarpras/pengadaan/pengadaan/diterima/". $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function action($body){
        $url = $this->urlApi . "api/data/sarpras/pengadaan/pengadaan/items";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/pengadaan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/pengadaan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/sarpras/pengadaan/update";
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/pengadaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/pengadaan/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>