<?php

namespace App\ApiService\Sarana\Pemusnahan;

use App\Helpers\ApiService;

class PemusnahanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function create($body){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/info";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/info";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function status_pemusnahan($id){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/pemusnahan/diterima/". $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function action($body){
        $url = $this->urlApi . "api/data/sarpras/pemusnahan/status/pemusnahan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
    

    


}

?>