<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/absensi/akun";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/akun";
        return ApiService::request($url, "POST", $body);
    }    
    
    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/absensi/akun/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
