<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SessiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/sesi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/sesi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/sesi";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/sesi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/sesi/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/sesi/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}
