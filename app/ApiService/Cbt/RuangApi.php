<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class RuangApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/ruang";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/ruang/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/ruang";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/ruang/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/ruang/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/ruang/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}
