<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class TokenApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/token";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/cbt/token/sekolah/" . $id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/token";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/token/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/token/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/token/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
}
