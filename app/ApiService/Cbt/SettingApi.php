<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class SettingApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/setting";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/setting/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/setting";
        return ApiService::request($url, "POST", $body);
    }    
    
    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/setting/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
