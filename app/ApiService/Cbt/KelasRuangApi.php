<?php

namespace App\ApiService\Cbt;

use App\Helpers\ApiService;

class KelasRuangApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_sekolah()
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang";
        return ApiService::request($url, "POST", $body);
    }

    public function create_many($body, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang/many/ta_sm/".$id_tahun_ajar;
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/cbt/kelas_ruang/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}
