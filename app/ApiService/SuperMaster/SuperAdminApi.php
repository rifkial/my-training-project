<?php

namespace App\ApiService\SuperMaster;

use App\Helpers\ApiService;

class SuperAdminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/superadmin";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/superadmin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/superadmin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/superadmin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/superadmin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/superadmin/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/programs/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/superadmin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/superadmin/update_program";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/superadmin/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/superadmin/trash/admin";
        return ApiService::request($url, "GET", null);
    }
}
