<?php

namespace App\ApiService\Sekolah;

use App\Helpers\ApiService;

class DefaultApi{

    public $urlApi;
    public $jenisApi;
    public $url;

    public function __construct($jenisApi)
    {
        $this->urlApi = env("API_URL");
        $this->jenisApi = $jenisApi;
        $this->url = $this->urlApi . $this->jenisApi;
    }

    public function get_all(){
        $url = $this->urlApi . $this->jenisApi;
        //dd($url);
        //$url = "http://localhost/smart_school/api/data/mutu/fakultas";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . $this->jenisApi .$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . $this->jenisApi ."/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . $this->jenisApi;
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . $this->jenisApi ."/info/". $id;
        //dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . $this->jenisApi ."delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . $this->jenisApi ."permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>