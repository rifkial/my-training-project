<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class BidangLokerApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function sekolah_tanpa_login($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/sekolah/belum_login/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/trash/profile/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bkk/bidang_loker/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
