<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class UserApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/user";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bkk/user";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function pagination_role($status, $page, $search, $role)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/role/all/pagination/".$role."?page=$page&status=$status&search=$search";
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/info";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/user/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/info";
        return ApiService::request($url, "POST", $body);
    }


    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/info";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update_image($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/info/update_image";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/user/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function pelamar($id_sekolah, $status)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/pelamar/".$id_sekolah."/".$status;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function Allpelamar($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/pelamar/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function industri($id_sekolah, $status)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/user/industri/".$id_sekolah."/".$status;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function perusahaan()
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/industri";
        return ApiService::request($url, "GET", null);
    }

    public function all_pelamar_apply($id_loker)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/pelamar/loker/daftar/".$id_loker;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/bkk/user/file/excel-example";
        return $url;
    }

    public function upload_excel($body, $role)
    {
        $url = $this->urlApi . "api/data/bkk/user/file/excel-import/".$role;
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function filter_name($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/filter/name";
        return ApiService::request($url, "POST", $body);
    }
}
