<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class LokerApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/loker";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/loker";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bkk/loker";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/loker/profile/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/loker/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function update_deskripsi($body, $id)
    {
        $url = $this->urlApi . "api/data/bkk/loker/update/deskripsi/only/".$id;
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/loker/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_auth_perusahaan()
    {
        $url = $this->urlApi . "api/data/bkk/loker/profile/perusahaan";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_auth_perusahaan_draft()
    {
        $url = $this->urlApi . "api/data/bkk/loker/profile/perusahaan/draft";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_auth_pelamar()
    {
        $url = $this->urlApi . "api/data/bkk/loker/dashboard/pelamar";
        return ApiService::request($url, "GET", null);
    }

    public function option_filter()
    {
        $url = $this->urlApi . "api/data/bkk/loker/bidang/group_by/loker";
        return ApiService::request($url, "GET", null);
    }


    public function get_filter($body)
    {
        $url = $this->urlApi . "api/data/bkk/loker/filter/data";
        return ApiService::request($url, "POST", $body);
    }

    public function get_filter_admin($body)
    {
        $url = $this->urlApi . "api/data/bkk/loker/filter/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function get_pagination_before($id, $id_sekolah, $limit)
    {
        $url = $this->urlApi . "api/data/bkk/loker/profile/sekolah/".$id."/".$id_sekolah."/".$limit;
        return ApiService::request($url, "GET", null);
    }

    public function get_pagination($id_sekolah, $limit)
    {
        $url = $this->urlApi . "api/data/bkk/loker/profile/sekolah/".$id_sekolah."/".$limit;
        return ApiService::request($url, "GET", null);
    }

}
