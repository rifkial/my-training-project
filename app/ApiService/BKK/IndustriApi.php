<?php

namespace App\ApiService\BKK;

use App\Helpers\ApiService;

class IndustriApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/industri";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/industri";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bkk/industri";
        return ApiService::request_image($url, "POST", $body);
    }

    public function search_name_industri($body)
    {
        $url = $this->urlApi . "api/data/bkk/industri/search/name/and/sekolah";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/industri/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function sekolahWithoutLogin($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/industri/sekolah/belum_login/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bkk/industri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/industri/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function search($page, $status, $keyword, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/bkk/industri/search/name/and/sekolah?page=$page&status=$status&search=$keyword&sekolah=$id_sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/bkk/industri/update/foto/profile";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function delete_profile($id)
    {
        $url = $this->urlApi . "api/data/bkk/industri/delete/profile/".$id;
        return ApiService::request($url, "GET", null);
    }
}
