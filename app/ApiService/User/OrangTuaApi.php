<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class OrangTuaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/ortu";
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/ortu";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/ortu";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/ortu/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/ortu/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($jurusan, $tahun, $page, $search)
    {
        $url = $this->urlApi . "api/data/user/ortu/sekolah/profile?page=$page&jurusan=$jurusan&tahun=$tahun&search=$search";
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/ortus/details/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_id_sekolah()
    {
        $url = $this->urlApi . "api/data/user/ortu/sekolah/all/get/profile";
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/ortu/update_ortu";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/ortu/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/ortu/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/ortu/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/ortu/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/user/ortu/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/ortu/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
