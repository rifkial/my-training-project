<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class BKKApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/bkk/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bkk/user";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bkk/user";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function pagination_role($page, $search, $role)
    {
        $url = $this->urlApi . "api/data/bkk/user/sekolah/role/all/pagination/".$role."?page=$page&search=$search";
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bkk/users/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/info";
        return ApiService::request($url, "POST", $body);
    }


    public function import()
    {
        $url = $this->urlApi . "api/data/bkk/user/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bkk/user/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bkk/user/trash/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/restore/".$id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bkk/user/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


}
