<?php

namespace App\ApiService\User;

use Abraham\TwitterOAuth\Request;
use App\Helpers\ApiService;

class GuruApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/guru";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/guru";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/guru";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/guru/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/guru/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }    

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/user/guru/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah_pagination($page, $search)
    {
        $url = $this->urlApi . "api/data/user/guru/get/profile/sekolah/pagination?page=$page&search=$search";
        return ApiService::request($url, "GET", null);
    }
    
    public function filter($search)
    {
        $url = $this->urlApi . "api/data/user/guru/get/filter/sekolah?search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/gurus/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/guru/info";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/guru/info";
        return ApiService::request($url, "POST", $body);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/guru/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/user/guru/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/guru/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/guru/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/guru/restore/".$id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/guru/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/user/guru/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    
}
