<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class TataUsahaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/tata_usaha";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/tata_usahas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/update_tata_usaha";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/update_tata_usaha";
        return ApiService::request($url, "POST", $body);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function import()
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/file/excel-example";
        return $url;
    }

    public function get_sekolah_pagination($page, $search)
    {
        $url = $this->urlApi . "api/data/user/tata_usaha/get/profile/sekolah/pagination?page=$page&search=$search";
        return ApiService::request($url, "GET", null);
    }

}
