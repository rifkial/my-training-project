<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class SupervisorApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/supervisor";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/supervisor/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/supervisor/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }   

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/user/supervisor/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/supervisors/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor/update_supervisor";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor/info";
        return ApiService::request($url, "POST", $body);
    }
   
    public function update_paraf($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor/update/paraf/only";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/supervisor/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/supervisor/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/supervisor/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/supervisor/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/user/supervisor/file/excel-example/".$id_sekolah;
        return $url;
    }

}
