<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class ProfileApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_profile_mutu($id){
        $url = $this->urlApi ."api/data/mutu/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_profile_sarana_user($id){
        $url = $this->urlApi ."api/data/sarpras/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_profile_sarana_admin($id){
        $url = $this->urlApi ."api/data/sarpras/admin/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_profile()
    {
        $url = $this->urlApi . "api/auth/profile";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_rombel($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/learning/room/guru/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_profile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function update_profile_image($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update_profile_image1($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image1($url, "POST", $body);
    }

    public function update_profile_images($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_images($url, "POST", $body);
    }

    public function get_profil_not_image($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_mapel()
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/room";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    public function reset_password_admin($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_guru($id)
    {
        $url = $this->urlApi . "api/password/reset-password/guru/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_user_bkk($id)
    {
        $url = $this->urlApi . "api/password/reset-password/bkk/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_walikelas($id)
    {
        $url = $this->urlApi . "api/password/reset-password/wali-kelas/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_supervisor($id)
    {
        $url = $this->urlApi . "api/password/reset-password/supervisor/".$id;
        return ApiService::request($url, "PUT", null);
    }
    public function reset_password_admin_bkk($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin-bkk/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_admin_learning($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin-learning/".$id;
        return ApiService::request($url, "PUT", null);
    }
    
    public function reset_password_admin_prakerin($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin-prakerin/".$id;
        return ApiService::request($url, "PUT", null);
    }
    
    public function reset_password_admin_raport($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin-raport/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_admin_induk($id)
    {
        $url = $this->urlApi . "api/password/reset-password/admin-induk/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_siswa($id)
    {
        $url = $this->urlApi . "api/password/reset-password/siswa/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_bk($id)
    {
        $url = $this->urlApi . "api/password/reset-password/bk/".$id;
        return ApiService::request($url, "PUT", null);
    }
    public function reset_password_ortu($id)
    {
        $url = $this->urlApi . "api/password/reset-password/ortu/".$id;
        return ApiService::request($url, "PUT", null);
    }
    public function reset_password_tu($id)
    {
        $url = $this->urlApi . "api/password/reset-password/tata-usaha/".$id;
        return ApiService::request($url, "PUT", null);
    }

    public function reset_password_superadmin($body)
    {
        $url = $this->urlApi . "api/mail/reset-password/superadmin/";
        return ApiService::request($url, "POST", $body);
    }

    public function reset_password_email($body)
    {
        $url = $this->urlApi . "api/mail/reset-password/email";
        return ApiService::request($url, "POST", $body);
    }

    public function reset_password_alumni($body)
    {
        $url = $this->urlApi . "api/forgot-password/email";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }


}
