<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class SiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/user/siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/update/foto/profile";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/user/siswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/user/siswa/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/user/siswa/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }



    public function get_by_sekolah()
    {

        $url = $this->urlApi . "api/data/user/siswa/profile";
        //dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_id_sekolah()
    {
        $url = $this->urlApi . "api/data/user/siswa/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_sekolah($page, $search)
    {
        $url = $this->urlApi . "api/data/user/siswa/profile/sekolah?page=$page&search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/siswas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/update_siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/info";
        return ApiService::request($url, "POST", $body);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/user/siswa/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_tahun_ajar($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/user/siswa/kelas/siswa/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/user/siswa/file/template/import-siswa";
        return $url;
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/siswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/user/siswa/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/user/siswa/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function not_kelas_siswa()
    {
        $url = $this->urlApi . "api/data/user/siswa/siswa/tanpa/kelas/siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function delete_profile($id_siswa)
    {
        $url = $this->urlApi . "api/data/user/siswa/delete/profile/".$id_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/user/siswa/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function import_siswa()
    {
        $url = $this->urlApi . "api/data/user/siswa/file/excel-import";
        return $url;
    }

}
