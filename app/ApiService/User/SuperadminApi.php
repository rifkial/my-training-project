<?php

namespace App\ApiService\User;

use App\Helpers\ApiService;

class SuperadminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "/api/data/user/superadmin";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "/api/data/user/superadmin";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "/api/data/user/superadmin/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "/api/data/user/superadmins/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "/api/data/user/superadmins/info";
        return ApiService::request($url, "PUT", $body);
    }
}
