<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class AdminApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/admin";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function register_form($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin/register/form";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin/info";
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/admin/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/admin/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/admin/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function sekolah($search, $page)
    {
        $url = $this->urlApi . "api/data/alumni/admin/sekolah/profile/data/paginat?search=$search&page=$page";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/admin/sekolah/profile/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/admin/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/admin/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/admin/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/admin/update/status";
        return ApiService::request($url, "POST", $body);
    }
}
