<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class AlumniApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/alumni";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/alumni/alumni";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
   
    public function create_file($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/alumni/alumni";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function register_form($body)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/register/form";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/alumni/alumni/info";
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/alumni/alumni/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function sekolah($page, $fakultas, $jurusan, $angkatan, $search)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/sekolah/profile/data/pag?page=$page&fakultas=$fakultas&jurusan=$jurusan&angkatan=$angkatan&search=$search";
        return ApiService::request($url, "GET", null);
    }

    // public function sekolah($search)
    // {
    //     $url = $this->urlApi . "api/data/alumni/alumni/sekolah/profile/paginate/full?search=$search";
    //     // dd($url);
    //     return ApiService::request($url, "GET", null);
    // }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/alumni/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/update/status";
        return ApiService::request($url, "POST", $body);
    }

    public function get_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/alumni/get/status";
        return ApiService::request($url, "POST", $body);
    }

    public function reset_password($body)
    {
        $url = $this->urlApi . "api/password/reset-password/alumni-alumni";
        return ApiService::request($url, "POST", $body);
    }

    public function tahun_angkatan($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/tahun-angkatan/sekolah/".$id_sekolah;
        return ApiService::request($url, "GET", null);
    }
}
