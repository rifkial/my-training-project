<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class OrganisasiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/organisasi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function sekolah()
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function by_id_alumni($id_alumni)
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/alumni/data/get/by/".$id_alumni;
        return ApiService::request($url, "GET", null);
    }
    
    public function by_auth_alumni()
    {
        $url = $this->urlApi . "api/data/alumni/organisasi/alumni/profile";
        return ApiService::request($url, "GET", null);
    }
}
