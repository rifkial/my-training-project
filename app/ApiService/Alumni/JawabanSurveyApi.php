<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class JawabanSurveyApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei";
        // dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function sekolah($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/sekolah/profile?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        return ApiService::request($url, "GET", null);
    }


    public function get_id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/sekolah/" . $id_sekolah;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_id_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/data/kategori/" . $id_kategori;
        return ApiService::request($url, "GET", null);
    }
   
    public function statistik_jawaban($id_kategori)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/data/kategori-survei/total/participant/" . $id_kategori;
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }
    
    public function cek_kategori()
    {
        $url = $this->urlApi . "api/data/alumni/jawaban-survei/cek-kategori/alumni";
        return ApiService::request($url, "GET", null);
    }
}
