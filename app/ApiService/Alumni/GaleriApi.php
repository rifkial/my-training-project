<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class GaleriApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/galeri";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri";
        return ApiService::request($url, "POST", $body);
    }
    
    public function upload($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/upload/image/base64";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function id_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/sekolah/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function sekolah($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/sekolah/data/page/sekolah?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/galeri/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/update/status";
        return ApiService::request($url, "POST", $body);
    }
    
    public function get_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/get/status";
        return ApiService::request($url, "POST", $body);
    }
    
    public function get_by_parameter($status, $page, $search)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/get/status/".$status."?page=$page&search=$search";
        return ApiService::request($url, "GET", $status);
    }
   
    public function get_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/alumni/galeri/kategori/".$id_kategori;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_my_galeri()
    {
        $url = $this->urlApi . "api/data/alumni/galeri/data/my-galeri";
        return ApiService::request($url, "GET", null);
    }
}
