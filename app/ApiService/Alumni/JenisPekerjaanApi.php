<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class JenisPekerjaanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/sekolah/profile/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function Pagination_sekolah()
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/sekolah/profile/data/pag";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/jenis_pekerjaan/update/status";
        return ApiService::request($url, "POST", $body);
    }
}
