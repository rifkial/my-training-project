<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class PendidikanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function by_id_alumni($id_alumni)
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/alumni/data/get/by/".$id_alumni;
        return ApiService::request($url, "GET", null);
    }
    
    public function by_auth_alumni()
    {
        $url = $this->urlApi . "api/data/alumni/pendidikan/alumni/profile";
        return ApiService::request($url, "GET", null);
    }
}
