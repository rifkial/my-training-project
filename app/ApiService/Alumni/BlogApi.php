<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class BlogApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/alumni/blog";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog";
        return ApiService::request_image($url, "POST", $body);
    }
    

    public function update($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/info";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/dilihat/" . $id;
        // dd($url);
        return ApiService::request($url, "PUT", null);
    }

    public function id_sekolah($id, $page, $search)
    {
        $url = $this->urlApi . "api/data/alumni/blog/sekolah/" . $id."?page=$page&search=$search";;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/get/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }
    
    public function get_status_param($status, $page, $search)
    {
        $url = $this->urlApi . "api/data/alumni/blog/get/status/".$status."?page=$page&search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/force/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function sekolah($search, $status, $sort, $perPage, $page)
    {
        $url = $this->urlApi . "api/data/alumni/blog/sekolah/profile/data/pag?search=$search&status=$status&sort=$sort&per_page=$perPage&page=$page";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/alumni/blog/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/alumni/blog/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/update/status";
        return ApiService::request($url, "POST", $body);
    }
    
    public function search_name($body)
    {
        $url = $this->urlApi . "api/data/alumni/blog/search/data/by_name";
        // dd($body);
        return ApiService::request($url, "POST", $body);
    }
    
    public function get_sosial($sosial)
    {
        $url = $this->urlApi . "api/data/alumni/blog/user/sosial/".$sosial;
        // dd($body);
        return ApiService::request($url, "GET", null);
    }
   
    public function my_blog()
    {
        $url = $this->urlApi . "api/data/alumni/blog/data/my-blog";
        // dd($body);
        return ApiService::request($url, "GET", null);
    }
}
