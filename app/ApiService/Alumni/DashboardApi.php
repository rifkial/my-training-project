<?php

namespace App\ApiService\Alumni;

use App\Helpers\ApiService;

class DashboardApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function statisctic_alumni()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard1";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function jumlah_tahun_lulus()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard2";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function jumlah_jurusan()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard3";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function survey()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard4";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function statistik_data()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard5";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pekerjaan()
    {
        $url = $this->urlApi . "api/data/alumni/dashboard6";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
