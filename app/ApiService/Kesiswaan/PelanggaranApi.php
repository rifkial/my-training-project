<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class PelanggaranApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/trash/admin";
        return ApiService::request($url, "GET", null);
    }
   
    public function by_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/pelanggaran/kategori/".$id_kategori;
        return ApiService::request($url, "GET", null);
    }
}
