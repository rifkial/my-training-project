<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class SanksiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/sanksi/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }
}
