<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class BeasiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function by_jenis($id_jenis_beasiswa)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/jenis/beasiswa/".$id_jenis_beasiswa;
        return ApiService::request($url, "GET", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/file/excel-example/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/beasiswa/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
