<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class AdminKesiswaanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/info";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/info";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/trash/admin-kesiswaan";
        return ApiService::request($url, "GET", null);
    }

    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/update/profile/password";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function delete_profile($id_admin)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/delete/profile/" . $id_admin;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_foto($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/admin-kesiswaan/update/foto/profile";
        return ApiService::request_image($url, "POST", $body);
    }
}
