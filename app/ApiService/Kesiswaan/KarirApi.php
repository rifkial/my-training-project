<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class KarirApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }
   
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/update/status";
        return ApiService::request($url, "POST", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/trash/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/file/excel-example/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/karir/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
