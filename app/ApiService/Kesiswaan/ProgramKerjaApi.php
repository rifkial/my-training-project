<?php

namespace App\ApiService\Kesiswaan;

use App\Helpers\ApiService;

class ProgramKerjaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create_file1($body)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja";
        return ApiService::request_image1($url, "POST", $body);
    }

    public function create_files($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja";
        return ApiService::request_images($url, "POST", $body);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/bimbingan-konseling/program_kerja/" . $id;
        return ApiService::request($url, "GET", null);
    }
}
