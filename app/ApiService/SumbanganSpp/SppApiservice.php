<?php

namespace App\ApiService\SumbanganSpp;

use App\Helpers\ApiService;

class SppApiservice
{
    /**
     * variabel global
     */
    public $urlApi;

    /**
     * __construct function with variable global for url domain
     */

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    /** get profil  */
    public function get_info_profil()
    {
        $url = $this->urlApi . "api/auth/profile";
        return ApiService::request($url, "GET", null);
    }

    /** profil setting sekolah template spp */
    public function setting_template()
    {
        $url = $this->urlApi . "api/data/spp/tagihan/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get tahun ajaran  */
    public function get_tahun_ajaran()
    {
        $url = $this->urlApi . "api/data/master/tahun_ajaran/sekolah/tahun_ajaran";
        return ApiService::request($url, "GET", null);
    }

    /** get data kelas  */
    public function get_data_kelas()
    {
        $url = $this->urlApi . "api/data/master/kelas";
        return ApiService::request($url, "GET", null);
    }

    /**akun pos */

    public function get_data_posx()
    {
        $url = $this->urlApi . "api/data/spp/pos/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    /** get data pos pemasukan  */
    public function get_data_pos_pemasukan($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos/sekolah/tahun_ajaran/" . $tahun . "";
        return ApiService::request($url, "GET", null);
    }
    //pos by id
    public function get_data_pos_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos/" . $id;
        return ApiService::request($url, "GET", null);
    }

    // simpan pos
    public function post_data_pos($body)
    {
        $url = $this->urlApi . "api/data/spp/pos";
        return ApiService::request($url, "POST", $body);
    }

    // update pos
    public function update_data_pos($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    //remove pos by id
    public function remove_pos_by_id($id)
    {
        $url = $this->urlApi . "api/data/spp/pos/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** get data detail pos by id  */

    public function get_data_pos_pemasukan_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_data_pos_pemasukanx($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/sekolah/tahun_ajaran/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    // update pos
    public function update_data_pos_pemasukan($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }


    // update pos real
    public function update_data_pos_pemasukanreal($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/pos/realisasi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    // post pos
    public function post_data_pos_pemasukan($body)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan";
        return ApiService::request($url, "POST", $body);
    }

    //remove pos pemasukan

    public function remove_pos_pemasukan($id)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** get data pos pemasukan  */
    public function post_tagihan($body)
    {
        $url = $this->urlApi . "api/data/spp/tagihan";
        return ApiService::request($url, "POST", $body);
    }

    /** get data show tagihan by id   */
    public function show_tagihan($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data show tagihan by id   */
    public function update_tagihan($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** remove tagihan setting */
    public function remove_tagihan($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** setting spp */
    public function setting_spp($id)
    {
        $url = $this->urlApi . "api/data/spp/setting/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** setting spp */
    public function rekeningspp($id)
    {
        $url = $this->urlApi . "api/data/spp/setting/sekolah/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** update rekening spp */
    public function update_spp($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/setting/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** get template setting pesan  */
    public function setting_pesan()
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** detail template setting pesan by id */
    public function setting_pesan_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** remove template setting pesan  */
    public function setting_pesan_byremoveid($id)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** post template setting pesan  */
    public function setting_pesan_post($body)
    {
        $url = $this->urlApi . "api/data/spp/template_pesan";
        return ApiService::request($url, "POST", $body);
    }

    /** get data siswa */
    public function siswaApi($tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/profile/paginate/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get siswa by  id  */

    public function siswaApibyid($id)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/siswa/get_by_siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/nisn/" . $id . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** search siswa */
    public function search_siswa_by_kelas($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/kelas_siswa/get/tahun/" . $tahun . "/kelas/siswa/" . $id . "";
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan years */
    public function search_tagihan($tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/sekolah/tahun_ajaran/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan bulan ini */
    public function search_tagihan_by_bulan($id, $bulan)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id . "/bulan/" . $bulan;
        return ApiService::request($url, "GET", NULL);
    }

    /** search tagihan years and nisn */
    public function search_tagihan_by_nisn($nis, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/siswa/nis/" . $nis . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get data siswa */
    public function kelasApi()
    {
        $url = $this->urlApi . "api/data/master/kelas/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** get data siswa */
    public function jurusanApi()
    {
        $url = $this->urlApi . "api/data/master/jurusan/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** get tagihan id siswa  */
    public function tagihan_siswa_by_kelas_id($id)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** tagihan bulan  */
    public function tagihan_siswa_bulan($id, $nama_bulan)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/kelas/siswa/" . $id . "/bulan/" . $nama_bulan . "";
        return ApiService::request($url, "GET", NULL);
    }


    /** TRANSAKSI    */
    public function transaksi_tagihan($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi";
        return ApiService::request($url, "POST", $body);
    }

    /** TRANSAKSI  detail   */
    public function transaksi_tagihan_detail($body)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi";
        return ApiService::request($url, "POST", $body);
    }

    /** get data tagihan siswa  */
    public function get_transaksi()
    {
        $url = $this->urlApi . "api/data/spp/transaksi";
        return ApiService::request($url, "GET", NULL);
    }

    /** update data TRANSAKSI siswa  */
    public function update_transaksi($id, $body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** delete data TRANSAKSI siswa  */
    public function delete_transaksi($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/delete/" . $id;
        return ApiService::request($url, "DELETE", NULL);
    }

    /** transaksi by id */
    public function get_transaksi_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** transaksi by id */
    public function get_transaksi_siswa_id($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi/siswa/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** detail transaksi by code */
    public function get_detail_transaksi_bycode($id)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi/transaksi/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** keringanan spp */
    public function get_keringan_spp()
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan";
        return ApiService::request($url, "GET", NULL);
    }

    /** show keringan spp */
    public function get_keringan_spp_byid($id)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** konfirmasi pembayaran spp */
    public function get_konfirmasi_spp()
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** konfirmasi pembayaran spp lunas */
    public function get_konfirmasi_spp_paid()
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/sekolah/profile/paid";
        return ApiService::request($url, "GET", NULL);
    }

    /** post keringanan  */
    public function post_keringananspp($body)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan";
        return ApiService::request($url, "POST", $body);
    }

    /** update keringanan  */
    public function update_keringananspp($body, $id)
    {
        $url = $this->urlApi . "api/data/spp/keringanan_tagihan/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    //pos by id
    public function get_data_pos()
    {
        $url = $this->urlApi . "api/data/spp/pos/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    //download export file
    public function get_data_filepos()
    {
        $url = $this->urlApi . "api/data/spp/pos/file/excel-example";
        return ApiService::request($url, "GET", null);
    }

    //get report
    public function report_tagihan($id, $tahun)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/profile/piutang/rombel/" . $id . "/tahun/" . $tahun;
        return ApiService::request($url, "GET", null);
    }

    /** rombel get  */
    public function rombel_data()
    {
        $url = $this->urlApi . "api/data/master/rombel/profile";
        return ApiService::request($url, "GET", null);
    }

    /** transaksi konfirmasi post */
    public function post_upload_konfirm($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/bukti/konfirmasi";
        return ApiService::request($url, "POST", $body);
    }

    /** transaksi konfirmasi post */
    public function post_upload_konfirmfile($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/bukti/konfirmasi";
        return ApiService::request_image($url, "POST", $body);
    }

    /** transaksi konfirmasi post */
    public function post_store_siswa_konfirm($id_kelas_siswa, $bulan, $id_pesan)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/siswa/" . $id_kelas_siswa . "/bulan/" . $bulan . "/pesan/" . $id_pesan;
        return ApiService::request($url, "POST", null);
    }

    /** create tagihan online  */
    public function create_transconfirm($body)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi";
        return ApiService::request($url, "POST", $body);
    }

    /** get confirmasi by code  */
    public function get_transconfirm($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/kode/" . $id . "/tagihan";
        return ApiService::request($url, "GET", null);
    }

    /** change Metode bayar  */
    public function change_metode($kode, $metode)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/kode/" . $kode . "/metode/" . $metode;
        return ApiService::request($url, "PUT", NULL);
    }

    /** show detail konfirmasi */

    public function get_transconfirm_detail($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** show detail konfirmasi */

    public function get_transconfirm_detail_bycode($id)
    {
        $url = $this->urlApi . "api/data/spp/detail_transaksi/transaksi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** approve pembayaran */
    public function approve_konfirmasi_pembayaran($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    /** reject pembayaran */
    public function reject_konfirmasi_pembayaran($id)
    {
        $url = $this->urlApi . "api/data/spp/transaksi_konfirmasi/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }


    /** get statsus order pembayaran midtrans  */

    public function get_cek_statusOrder($id, $idx)
    {
        $url = $this->urlApi . "api/data/spp/tagihan/order/" . $id . "/status/sekolah/" . $idx;
        return ApiService::request($url, "GET", null);
    }

    /** forget password  */
    public function forget_pass($body)
    {
        $url = $this->urlApi . "api/forgot-password/email";
        return ApiService::request($url, "POST", $body);
    }

    /** get Token password */
    public function get_token_pass($token)
    {
        $url = $this->urlApi . "api/forgot-password/" . $token . "/check";
        return ApiService::request($url, "GET", null);
    }

    /** reset password  */
    public function reset_pass($body)
    {
        $url = $this->urlApi . "api/forgot-password/reset";
        return ApiService::request($url, "POST", $body);
    }


    /**
     * update profil admin
     * parameter json body
     */

    public function update_profil($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request($url, "POST", $body);
    }

    /**
     * update profil admin with image
     * parameter json body
     */

    public function update_profil_withfile($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request_image($url, "POST", $body);
    }

    /** update password
     *  parameter json body
     */

    public function update_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    /** send notifkasi  */
    public function send_notif($channel, $event, $message)
    {
        return ApiService::push_notif($channel, $event, $message);
    }

    /** config environment api key */
    public function intergration_api($id)
    {
        $url = $this->urlApi . "api/data/master/environment/sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    /** get data ortu siswa by id_siswa */
    public function get_data_ortu($id_siswa)
    {
        $url = $this->urlApi . "api/data/user/ortu/siswa/" . $id_siswa;
        return ApiService::request($url, "GET", null);
    }

    /** get realisasi  */
    public function report_realisasi($tahun)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/profile/tahun/" . $tahun;
        return ApiService::request($url, "GET", NULL);
    }

    /** get realisasi  */
    public function report_realisasi_filter($tahun, $bulan)
    {
        $url = $this->urlApi . "api/data/spp/pos_pemasukan/profile/tahun/" . $tahun . "/bulan/" . $bulan;
        return ApiService::request($url, "GET", NULL);
    }

    /** notifikasi  */
    public function notifikasi()
    {
        $url = $this->urlApi . "api/data/spp/notifikasi";
        return ApiService::request($url, "GET", NULL);
    }

    /** post notifikasi */
    public function send_notifikasi($body)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi";
        return ApiService::request($url, "POST", $body);
    }

    /** get notifikasi ortu atau siswa */
    public function get_notifikasi_ortus($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/ortu/kelas_siswa/" . $id_kelas_siswa;
        return ApiService::request($url, "GET", NULL);
    }

    /** update read post notifikasi */
    public function read_notifikasi($id, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/ortu/notif/" . $id . "/kelas_siswa/" . $id_kelas_siswa;
        return ApiService::request($url, "PUT", NULL);
    }

    /** get notifikasi admin */
    public function get_notifikasi_admin()
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/sekolah/notif";
        return ApiService::request($url, "GET", NULL);
    }

    /** update read post notifikasi */
    public function read_notifikasi_admin($id)
    {
        $url = $this->urlApi . "api/data/spp/notifikasi/sekolah/notif/" . $id;
        return ApiService::request($url, "PUT", NULL);
    }

    /** akun kategori */
    public function akuntansi_kategori()
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function show_akuntansi_kategori($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function post_akuntansi_kategori($body)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori";
        return ApiService::request($url, "POST", $body);
    }

    /** update akun kategori */
    public function update_akuntansi_kategori($body,$id)
    {
        $url = $this->urlApi . "api/data/spp/akun_kategori/" . $id;
        return ApiService::request($url, "PUT", $body);
    }

    /** akun sub kategori */
    public function akuntansi_subkategori()
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/sekolah/profile";
        return ApiService::request($url, "GET", NULL);
    }

    /** show akun kategori */
    public function show_akuntansi_subkategori($id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/" . $id;
        return ApiService::request($url, "GET", NULL);
    }


    /** show akun kategori */
    public function post_akuntansi_subkategori($body)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub";
        return ApiService::request($url, "POST", $body);
    }

    /** update akun kategori */
    public function update_akuntansi_subkategori($body,$id)
    {
        $url = $this->urlApi . "api/data/spp/akun_sub/".$id;
        return ApiService::request($url, "PUT",$body);
    }

    /** integrasi pembayaran setting  */
    public function setting_environment_pembayaran($jenis,$id){
        $url = $this->urlApi . "api/data/master/environment/jenis/".$jenis."/sekolah/" . $id;
        return ApiService::request($url, "GET", NULL);
    }

    /** integrasi pembayaran setting  */
    public function setting_environment_pembayaranpost($body){
        $url = $this->urlApi . "api/data/master/environment";
        return ApiService::request($url, "POST", $body);
    }
}
