<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class TugasKetrampilanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_wali_kelas()
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/wali-kelas/tugas";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_mapel_wali_kelas($id_rombel, $id_mapel)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/wali-kelas/tugas/mapel/".$id_mapel."/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_room($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_room_by_siswa($id)
    {
        $url = $this->urlApi . "api/data/learning/tugas_keterampilan/siswa/room/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
