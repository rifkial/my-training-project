<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class FeedApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/feed";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/learning/feed_rombel/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/feed";
        return ApiService::request($url, "POST", $body);
    }

    public function create_rombel($body)
    {
        $url = $this->urlApi . "api/data/learning/feed_rombel";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/feed/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_id_feed_rombel($id)
    {
        $url = $this->urlApi . "api/data/learning/feed_rombel/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel_wali_kelas($id_rombel)
    {
        $url = $this->urlApi . "api/data/learning/feed_rombel/rombel/" . $id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function rombel_wali_kelas_detail($id_feed)
    {
        $url = $this->urlApi . "api/data/learning/feed_rombel/" . $id_feed;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room)
    {
        $url = $this->urlApi . "api/data/learning/feed/room/" . $id_room;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/feed/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/feed/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/feed/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/feeds/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/feed/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/feed/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/feed/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function profile()
    {
        $url = $this->urlApi . "api/data/learning/feed/profile";
        return ApiService::request($url, "GET", null);
    }
}
