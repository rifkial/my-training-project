<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class BahanAjarApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel_tahun($id_rombel, $id_mapel, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/rombel/".$id_rombel."/mapel/".$id_mapel."/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }



    public function get_by_walkel($id_mapel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/file/wali-kelas/mapel/".$id_mapel."/tahun/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_wali_kelas()
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/file/wali-kelas";
        return ApiService::request($url, "GET", null);
    }

    public function get_detail_wali_kelas($id_mapel)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/file/wali-kelas/mapel/" .$id_mapel;
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajars/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/bahan_ajar/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
