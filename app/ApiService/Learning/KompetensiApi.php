<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class KompetensiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($jenis)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/jenis/" . $jenis;
        return ApiService::request($url, "GET", null);
    }

    public function by_room($id_room)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/by_room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_mapel_kelas($id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/mapel/".$id_mapel."/kelas/".$id_kelas;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function by_mapel_kelas_jenis($id_mapel, $id_kelas, $jenis)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/mapel/".$id_mapel."/kelas/".$id_kelas."/jenis/".$jenis;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_nama_jenis($nama)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/by_nama_jenis/" . $nama;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/profile/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensis/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function import($id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/file/excel-example/mapel/".$id_mapel."/kelas/".$id_kelas;
        // dd($url);
        return $url;
    }

    public function upload_excel($body, $id_mapel, $id_kelas)
    {
        $url = $this->urlApi . "api/data/learning/kompetensi/file/excel-import/mapel/".$id_mapel."/kelas/".$id_kelas;
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }
}
