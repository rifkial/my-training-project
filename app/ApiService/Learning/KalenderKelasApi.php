<?php

namespace App\ApiService\Learning;

use App\Helpers\ApiService;

class KalenderKelasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_acara($id_room)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/room/".$id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }


    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelass/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room)
    {
        $url = $this->urlApi . "api/data/learning/kalender_kelas/kalender/kelas/room/".$id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
