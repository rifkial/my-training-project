<?php

namespace App\ApiService\PenjaminMutu;

use App\Helpers\ApiService;

class DokumenApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/mutu/dokumen";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/mutu/dokumen/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_my_docs($id){
        $url = $this->urlApi . "api/data/mutu/dokumen/sekolah/user/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_all_unpublish(){
        $url = $this->urlApi . "api/data/mutu/dokumen/trash/sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id($id_docs){
        $url = $this->urlApi . "api/data/mutu/dokumen/". $id_docs;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/mutu/dokumen";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/mutu/dokumen";
        //dd($body);
        return ApiService::request_image($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/mutu/dokumen" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/mutu/dokumen/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function publish($id)
    {
        $url = $this->urlApi . "api/data/mutu/dokumen/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }

    public function unpublish($id)
    {
        $url = $this->urlApi . "api/data/mutu/dokumen/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>