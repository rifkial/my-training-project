<?php

namespace App\ApiService\PenjaminMutu\ProfileUser;

use App\Helpers\ApiService;

class UserMutuApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/mutu/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/mutu/user/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/user/siswa";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/mutu/user";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/mutu/user/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/mutu/user/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    
    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/user/siswas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_profile($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/auth/update-profile";
        return ApiService::request($url, "POST", $body);
    } 

    public function update_profile_user($body){
        $url = $this->urlApi . "api/data/mutu/user/info";
        return ApiService::request($url, "POST", $body);
    }  

    public function update_profile_image($body)
    {
        $url = $this->urlApi . "api/auth/update-profile";
        //dd($body);
        return ApiService::request_image($url, "POST", $body);
    }

    
}
