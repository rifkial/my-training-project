<?php

namespace App\ApiService\Absensi;

use App\Helpers\ApiService;

class LiburApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur";
        return ApiService::request($url, "POST", $body);
    }    
   
    public function update($body)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/info";
        return ApiService::request($url, "PUT", $body);
    }    
    
    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }
    
    public function get_sekolah($tahun)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/sekolah/".session('id_sekolah')."/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_bulan($id_sekolah, $bulan, $tahun)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/sekolah/".$id_sekolah."/bulan/".$bulan."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function syncrone_data($tahun)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/syncrone/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function getLibur($bulan, $tahun)
    {
        $url = env("API_LIBUR") . "api?month=".$bulan."&year=".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function getLiburYear($tahun)
    {
        $url = env("API_LIBUR") . "api?year=".$tahun;
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/absensi/setting_libur/update/status";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

}
