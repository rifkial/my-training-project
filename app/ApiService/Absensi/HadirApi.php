<?php

namespace App\ApiService\Absensi;

use App\Helpers\ApiService;

class HadirApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all($page, $search)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran?page=$page&search=$search";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran";
        return ApiService::request($url, "POST", $body);
    }

    public function create_login($body)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/post";
        return ApiService::request($url, "POST", $body);
    }
   
    public function form_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/form/".$id_sekolah;
        return ApiService::request($url, "GET", null);
    }
    
    public function my_izin($id_siswa, $date)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/siswa/".$id_siswa."/tanggal/".$date;
        return ApiService::request($url, "GET", null);
    }
   
    public function my_izin_guru($id_guru, $date)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/guru/".$id_guru."/profil/tanggal/".$date;
        return ApiService::request($url, "GET", null);
    }
   
    public function izin_guru($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/data/guru/ta_sm/".$id_tahun_ajar;
        return ApiService::request($url, "GET", null);
    }
   
    public function hadir_siswa_semester($id_rombel, $tahun, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/data/rombel/".$id_rombel."/tahun/".$tahun."/ta_sm/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function rekap_siswa($id_rombel, $tahun, $bulan, $tahun_absen)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/data/rombel/".$id_rombel."/tahun/".$tahun."/bulan/".$bulan."/tahun_absen/".$tahun_absen;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function rekap_guru($bulan, $tahun_absen)
    {
        $url = $this->urlApi . "api/data/absensi/kehadiran/data/guru/bulan/".$bulan."/tahun_absen/".$tahun_absen;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

   

}
