<?php

namespace App\ApiService\Absensi;

use App\Helpers\ApiService;

class IzinApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/absensi/izin";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/absensi/izin";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/absensi/izin";
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/absensi/izin/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/absensi/izin/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/absensi/izin/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_sekolah($tahun)
    {
        $url = $this->urlApi . "api/data/absensi/izin/sekolah/".session('id_sekolah')."/tahun/" . $tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_filter_mont_year($id_rombel, $bulan, $tahun_absen)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/rombel/".$id_rombel."/bulan/".$bulan."/tahun/".session('tahun')."/tahun_absen/".$tahun_absen;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_filter_guru($bulan, $tahun_absen)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/guru/bulan/".$bulan."/tahun_absen/".$tahun_absen;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function riwayat_siswa_date($id_siswa, $date)
    {
        $url = $this->urlApi . "api/data/absensi/izin/riwayat/siswa/".$id_siswa."/hari/".$date;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function riwayat_guru_date($id_guru, $date)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/riwayat/guru/".$id_guru."/tanggal/".$date;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function admin_all_siswa_date($tahun, $date)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/siswa/tahun/".$tahun."/tanggal/".$date;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function admin_all_guru_date($date)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/guru/tanggal/".$date;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   
    public function statistic_siswa_bulan($tahun, $bulan, $tahun_absen)
    {
        $url = $this->urlApi . "api/data/absensi/izin/data/siswa/tahun/".$tahun."/bulan/".$bulan."/tahun_absen/".$tahun_absen;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function update_status($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/absensi/izin/konfirmasi";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

}
