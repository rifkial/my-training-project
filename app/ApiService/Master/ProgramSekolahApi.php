<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class ProgramSekolahApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/program_sekolah";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id_program_sekolah($id, $idprogram)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/template/" . $id . "/" . $idprogram;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/profile/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/info";
        return ApiService::request($url, "PUT", $body);
    }
    
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/update_program";
        return ApiService::request($url, "POST", $body);
    }
   
    public function update_template($body)
    {
        $url = $this->urlApi . "api/data/master/program_sekolah/update_template";
        return ApiService::request($url, "POST", $body);
    }
}
