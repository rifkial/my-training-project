<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class KelasApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_active()
    {
        $url = $this->urlApi . "api/data/kelas/active";
        return ApiService::request($url, "GET", null);
    }

    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/kelas";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/kelas";
        // dd(ApiService::request($url, "POST", $body));
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/kelas/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/kelas/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jurusan($id)
    {
        $url = $this->urlApi . "api/data/master/kelas/by_jurusan/". $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_jurusan_null()
    {
        $url = $this->urlApi . "api/data/master/kelas/jurusan/data/empty/null";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/kelas/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/kelas/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/kelas/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/kelas/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/kelas/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/kelas/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/kelas/file/excel-example/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/kelas/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
