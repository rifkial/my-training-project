<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class GuruPelajaranApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran";
        return ApiService::request($url, "GET", null);
    }

    public function group($id_tahun)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/group/mapel/kelas/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/guru_pelajaran";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_detail_all($id_guru, $id_mapel, $id_kelas, $id_tahun)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/guru/".$id_guru."/mapel/".$id_mapel."/kelas/".$id_kelas."/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kelas($id_kelas)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/kelas/" . $id_kelas;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_mapel($id_mapel)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/mapel/" . $id_mapel;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/trash/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_banyak_data($guru, $mapel, $rombel)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/banyak_data/" . $guru . "/" . $mapel . "/" . $rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/profile/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function group_by_mapel_kelas($id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/group/mapel/kelas/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_guru($id_tahun)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/mapel-raport/guru/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/wali_kelas/rombel/".$id_rombel;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajarans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_rombel($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/wali_kelas/rombel/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajarans/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/file/excel-example/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/file/excel-import/".$id_ta_sm;
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function get_by_collection_rombel()
    {
        $url = $this->urlApi . "api/data/master/guru_pelajaran/sekolah/rombel/pelajaran/all";
        return ApiService::request($url, "GET", null);
    }
}
