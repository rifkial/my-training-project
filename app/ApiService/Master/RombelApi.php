<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class RombelApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/rombel";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/rombel";
        // dd(ApiService::request($url, "POST", $body));
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/rombel/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/rombel/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/rombel/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/rombel/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/rombels/details/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/rombels/info";
        return ApiService::request($url, "PUT", $body);
    }

    
    
    public function get_by_kelas($id_kelas)
    {
        $url = $this->urlApi . "api/data/master/rombel/kelas/" . $id_kelas;
        return ApiService::request($url, "GET", null);
    }
    

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/rombel/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/rombel/restore/".$id;
        return ApiService::request($url, "PATCH", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/rombel/file/excel-example/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/rombel/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

    public function dashboard_gender($tahun)
    {
        $url = $this->urlApi . "api/data/master/rombel/dashboard/siswa/tahun/".$tahun;
        return ApiService::request($url, "GET", null);
    }
}
