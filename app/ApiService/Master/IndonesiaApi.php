<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class IndonesiaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_provinsi()
    {
        $url = $this->urlApi . "api/data/master/indonesia/provinsi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_kabupaten()
    {
        $url = $this->urlApi . "api/data/master/indonesia/kabupaten";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_kabupaten_by_provinsi($id_provinsi)
    {
        $url = $this->urlApi . "api/data/master/indonesia/kabupaten/".$id_provinsi;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_kecamatan_by_kabupaten($id_kabupaten)
    {
        $url = $this->urlApi . "api/data/master/indonesia/kecamatan/".$id_kabupaten;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_desa_by_kecamatan($id_kecamatan)
    {
        $url = $this->urlApi . "api/data/master/indonesia/desa/".$id_kecamatan;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail_desa($id_desa)
    {
        $url = $this->urlApi . "api/data/master/indonesia/get/desa/".$id_desa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
