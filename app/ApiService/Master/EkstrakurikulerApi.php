<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class EkstrakurikulerApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler";
        return ApiService::request($url, "POST", $body);
    }
    
    public function update_file($body)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/update/file";
        return ApiService::request_image($url, "POST", $body);
    }
   
    public function delete_file($body)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/delete/file";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/sekolah/ekstra-profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_kategori($id_kategori)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/kategori/data/".$id_kategori;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikulers/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function import($id_sekolah)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/file/excel/".$id_sekolah;
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/ekstrakulikuler/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }

}
