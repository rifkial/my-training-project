<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class NotifikasiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/notifikasi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/notifikasi";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/notifikasi/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/jurusans/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/jurusans/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function baca($id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/baca/".$id;
        return ApiService::request($url, "PUT", null);
    }
   
    public function read_all($body, $id)
    {
        $url = $this->urlApi . "api/data/master/notifikasi/read/all/".$id;
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/notifikasi/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_siswa()
    {
        $url = $this->urlApi . "api/data/master/notifikasi/siswa/notif";
        return ApiService::request($url, "GET", null);
    }

    public function get_guru()
    {
        $url = $this->urlApi . "api/data/master/notifikasi/guru/notif";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
