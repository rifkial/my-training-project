<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class AnggotaEkskulApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        //dd($body);
        $url = $this->urlApi . "api/data/master/anggota-ekstra";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/" . $id;
        return ApiService::request($url, "GET", null);
    }
   
    public function get_by_ekskul($id, $tahun)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/get/find/ekstra/".$id."/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/delete/" . $id;
        //dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah($tahun)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/sekolah/profile/tahun/".$tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstras/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function menu()
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/menu/anggota-ekstra/data";
        return ApiService::request($url, "GET", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/file/excel-example";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/anggota-ekstra/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
