<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class SettingApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function create($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/config";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/config";
        return ApiService::request_image($url, "POST", $body);
    }

    public function create_file1($body)
    {
        $url = $this->urlApi . "api/data/master/config";
        return ApiService::request_image1($url, "POST", $body);
    }
   
    public function create_files($body)
    {
        $url = $this->urlApi . "api/data/master/config";
        return ApiService::request_images($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/config/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah($id)
    {
        $url = $this->urlApi . "api/data/master/config/sekolah/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/config/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/config/info";
        return ApiService::request($url, "PUT", $body);
    }

}
