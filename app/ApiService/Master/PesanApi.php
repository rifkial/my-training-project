<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class PesanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/pesan";
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/pesan";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/pesan";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function create_file_bk($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/pesan/post/banyak";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function create_bk($body)
    {
        // dd($body);
        $url = $this->urlApi . "api/data/master/pesan/post/banyak";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/show/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/permanent/delete/".$id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/pesan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/pesan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function update_fcm($body)
    {
        $url = $this->urlApi . "api/data/master/pesan/auth/fcm";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/pesan/trash/profile/sekolah";
        return ApiService::request($url, "GET", null);
    }

    public function terkirim()
    {
        $url = $this->urlApi . "api/data/master/pesan/terkirim";
        return ApiService::request($url, "GET", null);
    }

    public function terima()
    {
        $url = $this->urlApi . "api/data/master/pesan/show/terima/masuk";
        return ApiService::request($url, "GET", null);
    }

    public function belum()
    {
        $url = $this->urlApi . "api/data/master/pesan/show/terima/belum";
        return ApiService::request($url, "POST", null);
    }

    public function tampil_chat()
    {
        $url = $this->urlApi . "api/data/master/pesan/thumbnail";
        return ApiService::request($url, "GET", null);
    }

    public function tampil_email($email)
    {
        $url = $this->urlApi . "api/data/master/pesan/search/get/data_all_user/".$email;
        return ApiService::request($url, "GET", null);
    }

    public function data_user()
    {
        $url = $this->urlApi . "api/data/master/pesan/search/get/data_all_user";
        return ApiService::request($url, "GET", null);
    }

    public function tes()
    {
        $url = $this->urlApi . "api/data/master/pesan/f/c/m";
        return ApiService::request($url, "GET", null);
    }


    public function data_chat($email)
    {
        $url = $this->urlApi . "api/data/master/pesan/show/pengirim/penerima/".$email;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function all_email()
    {
        $url = $this->urlApi . "api/data/master/pesan/all/email";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_status_baca($id)
    {
        $url = $this->urlApi . "api/data/master/pesan/dibaca/".$id;
        // dd($url);
        return ApiService::request($url, "POST", null);
    }

}
