<?php

namespace App\ApiService\Master;

use App\Helpers\ApiService;

class MapelApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/master/mapel";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/master/mapel";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/master/mapel/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/master/mapel/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }    

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/master/mapel/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/master/mapel/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/master/mapel/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/master/mapel/sekolah/trash";
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/master/mapel/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/master/mapel/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function import()
    {
        $url = $this->urlApi . "api/data/master/mapel/file/excel";
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/master/mapel/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function update_status($body)
    {
        $url = $this->urlApi . "api/data/master/mapel/update/status";
        return ApiService::request($url, "POST", $body);
    }
}
