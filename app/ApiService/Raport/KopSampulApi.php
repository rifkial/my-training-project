<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class KopSampulApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/" . $id;
        return ApiService::request($url, "GET", null);
    }
    
    

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/restore/" . $id;
        // dd($url);
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/info";
        return ApiService::request_image($url, "POST", $body);
    }


    public function update($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul/trash/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul";
        return ApiService::request_images($url, "POST", $body);
    }

    public function createones_file($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul";
        return ApiService::request_image_sampul($url, "POST", $body);
    }

    public function createone_file($body)
    {
        $url = $this->urlApi . "api/data/raport/kop_sampul";
        return ApiService::request_image($url, "POST", $body);
    }

}
