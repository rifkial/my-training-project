<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class ConfigApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/config";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/config";
        return ApiService::request($url, "POST", $body);
    }
   
    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/raport/config";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/config/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/config/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/config/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/config/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/config/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function by_tahun()
    {
        $url = $this->urlApi . "api/data/raport/config/tahun/profile/sekolah/all";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function by_all_tahun($id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/config/tahun-ajaran/tahun/ajaran/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/config/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/config/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/config/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/config/trash/admin";
        return ApiService::request($url, "GET", null);
    }
    
    public function by_tahun_aktif($id_tahun_aktif)
    {
        $url = $this->urlApi . "api/data/raport/config/tahun-ajaran/sm/".$id_tahun_aktif;
        return ApiService::request($url, "GET", null);
    }

}
