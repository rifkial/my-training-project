<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiKDKetrampilanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_guru, $id_mapel, $id_kelas, $id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/guru/".$id_guru."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_kd($id_kd, $id_guru, $id_mapel, $id_kelas, $id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/kd/".$id_kd."/guru/".$id_guru."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_gabungan($id_kd, $id_guru, $id_mapel, $id_kelas, $id_rombel, $tahun, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/kd/".$id_kd."/guru/".$id_guru."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/tahun/".$tahun."/sm/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/details/" . $id;
        return ApiService::request($url, "GET", null);
    }
    

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function numrows($id_kd, $id_mapel, $id_kelas, $id_rombel, $id_guru)
    {
        $url = $this->urlApi . "api/data/raport/nilai_kd_keterampilan/kd/".$id_kd."/mapel/".$id_mapel."/kelas/".$id_kelas."/rombel/".$id_rombel."/guru/".$id_guru;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}
