<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class TranskipApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/transkrip";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/transkrip";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/raport/transkrip";
        return ApiService::request_image($url, "POST", $body);
    }

    public function data_siswa($page, $tahun, $jurusan, $keyword)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/data/siswa/tahun/jurusan/nis?page=$page&tahun=$tahun&jurusan=$jurusan&keyword=$keyword";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kelas_siswa($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/data/kelas-siswa/" . $id_kelas_siswa;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/transkrip/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/transkrip/info";
        return ApiService::request($url, "POST", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/transkrip/trash/admin";
        return ApiService::request($url, "GET", null);
    }
}
