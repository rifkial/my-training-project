<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiPrestasiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/prestasi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/prestasi";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/" . $id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_siswa($id_kelas_siswa, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/kelas/siswa/".$id_kelas_siswa."/tahun/" . $id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_wali_kelas($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/wali-kelas/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/prestasi/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/prestasis/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/prestasi/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/prestasi/trash/admin";
        return ApiService::request($url, "GET", null);
    }

}
