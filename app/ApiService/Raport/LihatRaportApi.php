<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class LihatRaportApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function kd_auth_siswa($id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/siswa/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function kd_kelas_siswa($id_kelas_siswa, $id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/kelas_siswa/".$id_kelas_siswa."/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function manual_auth_siswa($id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/siswa/manual/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function k16_auth_siswa($id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/siswa/k16/tahun/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function manual_kelas_siswa($id_kelas_siswa, $id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/kelas_siswa/".$id_kelas_siswa."/manual/tahun/".$id_tahun;
        return ApiService::request($url, "GET", null);
    }

    public function k16_kelas_siswa($id_kelas_siswa, $id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/kelas_siswa/".$id_kelas_siswa."/k16/tahun/".$id_tahun;
        return ApiService::request($url, "GET", null);
    }

    public function print_leger_kd($id_tahun_ajar, $id_rombel)
    {
        $url = $this->urlApi . "api/data/raport/lihat_raport/leger/ta_sm/".$id_tahun_ajar."/rombel/".$id_rombel;
        return ApiService::request($url, "GET", null);
    }




}
