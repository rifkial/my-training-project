<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }


    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rombel($id_rombel, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai/rombel/".$id_rombel."/tahun/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_gabungan($tahun, $id_guru_pelajaran, $id_tahun_ajar)
    {
        $url = $this->urlApi . "api/data/raport/nilai/siswa/tahun/".$tahun."/guru_pelajaran/".$id_guru_pelajaran."/sm/".$id_tahun_ajar;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai/details/" . $id;
        return ApiService::request($url, "GET", null);
    }
    

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai/trash/admin";
        return ApiService::request($url, "GET", null);
    }

}
