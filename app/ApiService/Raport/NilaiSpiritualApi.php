<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiSpiritualApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/room/" . $id_room. "/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/absen-siswa/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_wali_kelas($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/wali-kelas/tahun/".$id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_gabungan($tahun, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/nilai/tahun/".$tahun."/sm/".$id_ta_sm;
        return ApiService::request($url, "GET", null);
    }

    public function check_insert($id_wali_kelas, $id_kelas_siswa, $id_tahun)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/wali_kelas/".$id_wali_kelas."/kelas_siswa/".$id_kelas_siswa."/tahun/".$id_tahun;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spirituals/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/raport/nilai_spiritual/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
