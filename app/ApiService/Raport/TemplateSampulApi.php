<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class TemplateSampulApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/s1";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
   

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/s1";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/s1/" . $id;
        return ApiService::request($url, "GET", null);
    }


    
    public function get_S1()
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s1/siswa";
        return ApiService::request($url, "GET", null);
    }
    public function get_S2()
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s2/siswa";
        return ApiService::request($url, "GET", null);
    }
    public function get_S4()
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s4/siswa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_S1($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s1/kelas_siswa/".$id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_S2($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s2/kelas_siswa/".$id_kelas_siswa;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_S4($id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/cetak_sampul/s4/kelas_siswa/".$id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/s1/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/s1/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/s1/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/s1/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/s1/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/s1/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/s1/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/s1/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function createone_file($body)
    {
        $url = $this->urlApi . "api/data/raport/s1";
        return ApiService::request_image($url, "POST", $body);
    }
    
    public function createones_file($body)
    {
        $url = $this->urlApi . "api/data/raport/s1";
        return ApiService::request_image_sampul($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/raport/s1";
        return ApiService::request_images($url, "POST", $body);
    }

}
