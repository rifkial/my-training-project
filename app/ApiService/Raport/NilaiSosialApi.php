<?php

namespace App\ApiService\Raport;

use App\Helpers\ApiService;

class NilaiSosialApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_room($id_room, $id_kelas_siswa)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/room/" . $id_room. "/kelas_siswa/" . $id_kelas_siswa;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/absen-siswa/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function rooms($id_room)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/room/" . $id_room;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_wali_kelas($id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/wali-kelas/tahun/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_gabungan($tahun, $id_ta_sm)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/wali-kelas/tahun/".$tahun."/sm/".$id_ta_sm;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/by_jenis/" . $id_jenis;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosials/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_template()
    {
        $url = $this->urlApi . "api/data/raport/nilai_sosial/file/excel-example";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
}
