<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class IndustriApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/industri";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolahWithoutLogin($id_sekolah)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/sekolah/profile/without_login/".$id_sekolah;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/industri/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function search_by_industri($body, $id_sekolah)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/search/industri/nama/".$id_sekolah;
        // dd($body);
        return ApiService::request($url, "POST", $body);
    }
    
    public function download_excel()
    {
        $url = $this->urlApi . "api/data/prakerin/industri/file/excel-example";
        // dd($body);
        return $url;
    }

    public function upload_excel($body)
    {
        $url = $this->urlApi . "api/data/prakerin/industri/file/excel-import";
        return ApiService::request_image($url, "POST", $body);
    }
}
