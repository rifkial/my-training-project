<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class NilaiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/nilai";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

   
    public function sekolah($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/trash/admin";
        return ApiService::request($url, "GET", null);
    }
   
    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_jurusan($id_jurusan)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/sekolah/profile/jurusan/".$id_jurusan;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_jenis($id_jenis)
    {
        $url = $this->urlApi . "api/data/prakerin/nilai/sekolah/profile/berdasar/jenis/".$id_jenis;
        return ApiService::request($url, "GET", null);
    }
}
