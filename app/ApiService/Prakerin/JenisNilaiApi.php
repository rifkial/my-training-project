<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class JenisNilaiApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai";
        return ApiService::request($url, "POST", $body);
    }


    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

   
    public function sekolah_tanpa_login($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/sekolah/get/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/trash/admin";
        return ApiService::request($url, "GET", null);
    }
   
    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }
    
    public function get_by_jurusan($id)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/sekolah/jurusan/".$id;
        return ApiService::request($url, "GET", null);
    }
   
    public function get_by_group()
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/sekolah/jenis_nilai/group";
        return ApiService::request($url, "GET", null);
    }
    
    public function search_by_name($body)
    {
        $url = $this->urlApi . "api/data/prakerin/jenis_nilai/search/name/jenis";
        return ApiService::request($url, "POST", $body);
    }
}
