<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class PedomanApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman";
        return ApiService::request($url, "POST", $body);
    }
   
    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah_tanpa_login($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/trash/admin";
        return ApiService::request($url, "GET", null);
    }
    
    public function pengunjung_menu($id)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/sekolah/profile/tampil/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_title($body)
    {
        $url = $this->urlApi . "api/data/prakerin/pedoman/get/title";
        return ApiService::request($url, "POST", $body);
    }

}
