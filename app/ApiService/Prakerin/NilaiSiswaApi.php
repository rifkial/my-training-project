<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class NilaiSiswaApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function by_jenis($id_jenis, $id_siswa)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/jenis/".$id_jenis."/siswa/".$id_siswa;
        return ApiService::request($url, "GET", null);
    }
   
    public function cetak_nilai($id_prakerin_siswa, $id_jurusan)
    {
        $url = $this->urlApi . "api/data/prakerin/siswa_nilai/cetak/nilai/siswa/".$id_prakerin_siswa."/jurusan/".$id_jurusan;
        return ApiService::request($url, "GET", null);
    }
}
