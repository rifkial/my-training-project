<?php

namespace App\ApiService\Prakerin;

use App\Helpers\ApiService;

class KelompokApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok";
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok";
        return ApiService::request_image($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/hard_delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }


    public function sekolah_tanpa_login($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/sekolah/profile/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function by_all_jurusan($id_tahun)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/jurusan/ta_sm/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }
    
    public function by_jurusan($id_jurusan, $id_tahun)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/jurusan/".$id_jurusan."/ta_sm/".$id_tahun;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/details/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function restore($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/restore/" . $id;
        return ApiService::request($url, "PATCH", null);
    }


    public function update_info($body)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/info";
        return ApiService::request($url, "PUT", $body);
    }

    public function all_trash()
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/trash/admin";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/profile/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_pembimbing_industri()
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/sekolah/profile/industri/pemb_industri";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_industri($id)
    {
        $url = $this->urlApi . "api/data/prakerin/kelompok/sekolah/profile/industri/by/industri/".$id;
        return ApiService::request($url, "GET", null);
    }
}
