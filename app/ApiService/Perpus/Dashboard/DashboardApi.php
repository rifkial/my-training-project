<?php

namespace App\ApiService\Perpus\Dashboard;

use App\Helpers\ApiService;

class DashboardApi{

	public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_peminjaman(){
        $url = $this->urlApi . "api/data/perpus/dashboard/peminjaman";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_pengadaan(){
        $url = $this->urlApi . "api/data/perpus/dashboard/pengadaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_koleksi(){
        $url = $this->urlApi . "api/data/perpus/dashboard/koleksi";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_user(){
        $url = $this->urlApi . "api/data/perpus/dashboard/user";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

}