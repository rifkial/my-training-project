<?php

namespace App\ApiService\Perpus\Denda;

use App\Helpers\ApiService;

class DendaApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/denda";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/denda/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_buku($id){
        $url = $this->urlApi . "api/data/perpus/denda/buku/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_user($id){
        $url = $this->urlApi . "api/data/perpus/denda/user/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function riwayat_denda($id){
        $url = $this->urlApi . "api/data/perpus/denda/riwayat/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/denda/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/denda";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_many($body){
        $url = $this->urlApi . "api/data/perpus/denda/store/many";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($id,$body){
        $url = $this->urlApi . "api/data/perpus/denda/info/". $id;
        // dd($url);
        return ApiService::request($url, "PUT", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/denda/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/denda/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>