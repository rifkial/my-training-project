<?php

namespace App\ApiService\Perpus\Buku;

use App\Helpers\ApiService;

class BukuApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/buku";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_home(){
        $url = $this->urlApi . "api/data/perpus/buku/home/page";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_home_all($body){
        $url = $this->urlApi . "api/data/perpus/buku/home/all";
        // dd($url);
        return ApiService::request($url, "GET",$body);
    }

    public function get_favorite(){
        $url = $this->urlApi . "api/data/perpus/buku/favorite/all";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_favorite_home(){
        $url = $this->urlApi . "api/data/perpus/buku/favorite/home";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/buku/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/buku/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/buku";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_file($body){
        $url = $this->urlApi . "api/data/perpus/buku";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/buku/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update_file($body){
        $url = $this->urlApi . "api/data/perpus/buku/update";
        // dd($url);
        return ApiService::request_image($url, "POST", $body);
    }

    public function search_judul($body){
        $url = $this->urlApi . "api/data/perpus/buku/search_default";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

     public function search_advance($body){
        $url = $this->urlApi . "api/data/perpus/buku/search_advance";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/buku/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/buku/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>
