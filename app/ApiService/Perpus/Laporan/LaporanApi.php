<?php

namespace App\ApiService\Perpus\Laporan;

use App\Helpers\ApiService;

class LaporanApi{

	public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_pustaka(){
        $url = $this->urlApi . "api/data/perpus/laporan/pustaka";
        return ApiService::request($url, "GET", null);
    }

    public function get_pustaka_by_rak(){
        $url = $this->urlApi . "api/data/perpus/laporan/pustaka/rak";
        return ApiService::request($url, "GET", null);
    }

    public function get_pustaka_by_jenis(){
        $url = $this->urlApi . "api/data/perpus/laporan/pustaka/jenis";
        return ApiService::request($url, "GET", null);
    }

    public function get_pustaka_digital(){
        $url = $this->urlApi . "api/data/perpus/laporan/pusdig";
        return ApiService::request($url, "GET", null);
    }

    public function get_pustaka_digital_by_jenis(){
        $url = $this->urlApi . "api/data/perpus/laporan/pusdig/jenis";
        return ApiService::request($url, "GET", null);
    }

    public function get_user($id){
        $url = $this->urlApi . "api/data/perpus/laporan/user/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_admin(){
        $url = $this->urlApi . "api/data/perpus/laporan/admin";
        return ApiService::request($url, "GET", null);
    }

    public function pengadaan_by_mount($m){
        $url = $this->urlApi . "api/data/perpus/laporan/pengadaan/bulan/" . $m;
        return ApiService::request($url, "GET", null);
    }

    public function pengadaan_diterima($m){
        $url = $this->urlApi . "api/data/perpus/laporan/pengadaan/diterima/" . $m;
        return ApiService::request($url, "GET", null);
    }

    public function pengadaan_ditolak($m){
        $url = $this->urlApi . "api/data/perpus/laporan/pengadaan/ditolak/" . $m;
        return ApiService::request($url, "GET", null);
    }

    public function pengadaan_berjalan($m){
        $url = $this->urlApi . "api/data/perpus/laporan/pengadaan/berjalan";
        return ApiService::request($url, "GET", null);
    }

    public function peminjaman_by_mount($m){
        $url = $this->urlApi . "api/data/perpus/laporan/peminjaman/bulan/" . $m;
        return ApiService::request($url, "GET", null);
    }

    public function peminjaman_berjalan(){
        $url = $this->urlApi . "api/data/perpus/laporan/peminjaman/berjalan";
        return ApiService::request($url, "GET", null);
    }

    public function peminjaman_terlambat(){
        $url = $this->urlApi . "api/data/perpus/laporan/peminjaman/terlambat";
        return ApiService::request($url, "GET", null);
    }

    public function peminjaman_kembali(){
        $url = $this->urlApi . "api/data/perpus/laporan/peminjaman/kembali";
        return ApiService::request($url, "GET", null);
    }

    public function denda_by_mount($m){
        $url = $this->urlApi . "api/data/perpus/laporan/denda/bulan/" . $m;
        return ApiService::request($url, "GET", null);
    }

    public function koleksi($id){
        $url = $this->urlApi . "api/data/perpus/laporan/koleksi/". $id;
        return ApiService::request($url, "GET", null);
    }

    public function gmd($id){
        $url = $this->urlApi . "api/data/perpus/laporan/gmd/". $id;
        return ApiService::request($url, "GET", null);
    }

    public function jenis($id){
        $url = $this->urlApi . "api/data/perpus/laporan/jenis/". $id;
        return ApiService::request($url, "GET", null);
    }





}
