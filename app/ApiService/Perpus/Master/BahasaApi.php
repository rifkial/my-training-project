<?php

namespace App\ApiService\Perpus\Master;

use App\Helpers\ApiService;

class BahasaApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/bahasa";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/bahasa/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/bahasa/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/bahasa";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function search_judul($body){
        $url = $this->urlApi . "api/data/perpus/bahasa/search_default";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/bahasa/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/bahasa/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/bahasa/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>