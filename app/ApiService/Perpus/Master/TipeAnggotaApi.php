<?php

namespace App\ApiService\Perpus\Master;

use App\Helpers\ApiService;

class TipeAnggotaApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function search_judul($body){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/search_default";
        //dd($body);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/tipe_anggota/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>