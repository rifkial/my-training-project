<?php

namespace App\ApiService\Perpus\pengadaan;

use App\Helpers\ApiService;

class pengadaanApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/pengadaan";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_active(){
        $url = $this->urlApi . "api/data/perpus/pengadaan/active";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_accepted(){
        $url = $this->urlApi . "api/data/perpus/pengadaan/approved";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_rejected(){
        $url = $this->urlApi . "api/data/perpus/pengadaan/rejected";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/pengadaan";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function action($id,$act)
    {
        $url = $this->urlApi . "api/data/perpus/pengadaan/action/".$id."/".$act;
        // dd($url);
        return ApiService::request($url, "POST", null);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/pengadaan/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/pengadaan/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    } 

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/pengadaan/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/pengadaan/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/pengadaan/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }



}

?>