<?php

namespace App\ApiService\Perpus\Item;

use App\Helpers\ApiService;

class ItemApi{

    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }

    public function get_all(){
        $url = $this->urlApi . "api/data/perpus/item";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function findMany($body){
        $url = $this->urlApi . "api/data/perpus/item/find";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function get_detail($id){
        $url = $this->urlApi . "api/data/perpus/item/".$id;
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function get_by_buku($id){
        $url = $this->urlApi . "api/data/perpus/item/buku/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_kode($body){
        $url = $this->urlApi . "api/data/perpus/item/getKode";
        return ApiService::request($url, "POST", $body);
    }

    public function history($id){
        $url = $this->urlApi . "api/data/perpus/item/riwayat/".$id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah(){
        $url = $this->urlApi . "api/data/perpus/item/sekolah/profile";
        // dd($url);
        return ApiService::request($url, "GET", null);
    }

    public function create($body){
        $url = $this->urlApi . "api/data/perpus/item";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function create_many($body){
        $url = $this->urlApi . "api/data/perpus/item/store/many";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function update($body){
        $url = $this->urlApi . "api/data/perpus/item/update";
        // dd($url);
        return ApiService::request($url, "POST", $body);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/item/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/item/permanent/delete/" . $id;
        // dd($url);
        return ApiService::request($url, "DELETE", null);
    }

}

?>
