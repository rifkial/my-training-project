<?php

namespace App\ApiService\Perpus\User;

use App\Helpers\ApiService;

class UserPerpusApi
{
    public $urlApi;

    public function __construct()
    {
        $this->urlApi = env("API_URL");
    }


    public function get_all()
    {
        $url = $this->urlApi . "api/data/perpus/user";
        return ApiService::request($url, "GET", null);
    }

    public function get_by_sekolah()
    {
        $url = $this->urlApi . "api/data/perpus/user/sekolah/profile";
        return ApiService::request($url, "GET", null);
    }

    public function create($body)
    {
        $url = $this->urlApi . "api/data/perpus/user";
        return ApiService::request($url, "POST", $body);
    }

    public function get_by_id($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_induk($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/get_induk/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_by_nama($body)
    {
        $url = $this->urlApi . "api/data/perpus/user/get/nama";
        return ApiService::request($url, "POST", $body);
    }

    public function get_many($body)
    {
        $url = $this->urlApi . "api/data/perpus/user/find/user";
        return ApiService::request($url, "POST", $body);
    }

    public function get_peminjaman($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/peminjaman/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_denda($id)
    {
        $url = $this->urlApi . "api/data/perpus/denda/user/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_denda_berjalan($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/denda_berjalan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function get_keanggotaan($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/keanggotaan/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function extend($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/extend/" . $id;
        return ApiService::request($url, "GET", null);
    }

    public function soft_delete($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

    public function change_password($body)
    {
        $url = $this->urlApi . "api/auth/change-password";
        return ApiService::request($url, "POST", $body);
    }

    public function update($body)
    {
        $url = $this->urlApi . "api/data/perpus/user/update";
        return ApiService::request($url, "POST", $body);
    }

    public function delete($id)
    {
        $url = $this->urlApi . "api/data/user/perpus/permanent/delete/" . $id;
        return ApiService::request($url, "DELETE", null);
    }

     public function restore($id)
    {
        $url = $this->urlApi . "api/data/perpus/user/restore/" . $id;
        return ApiService::request($url, "POST", $body);
    }

}
