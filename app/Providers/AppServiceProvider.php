<?php

namespace App\Providers;

use App\ApiService\Alumni\KategoriSurveyApi;
use App\ApiService\BKK\ConfigApi;
use App\ApiService\Learning\DataSiswaApi;
use App\ApiService\Learning\PertemuanApi;
use App\ApiService\Learning\RoomApi;
use App\ApiService\Master\EnvironmentApi;
use App\ApiService\Master\JurusanApi;
use App\ApiService\Master\NotifikasiApi;
use App\ApiService\Master\PesanApi;
use App\ApiService\Master\SekolahApi;
use App\ApiService\Master\TahunAjaranApi;
use App\ApiService\Master\TemplateApi;
use App\ApiService\Perpus\Master;
use App\ApiService\Prakerin\ConfigApi as PrakerinConfigApi;
use App\ApiService\Prakerin\InformasiApi;
use App\ApiService\Prakerin\PedomanApi;
use App\ApiService\User\BKApi;
use App\ApiService\User\OrangTuaApi;
use App\ApiService\User\ProfileApi;
use Illuminate\Support\ServiceProvider;
use Hashids\Hashids;
//file migration database
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    private $jurusanApi;
    private $notifApi;
    private $templateApi;
    private $profileApi;
    private $pertemuanApi;
    private $sekolahApi;
    private $dataSiswaApi;
    private $roomApi;
    private $ortuApi;
    private $bkApi;
    private $alumniKategoriApi;
    private $informasiApi;
    private $pedomanApi;
    private $bkkConfigApi;
    private $prekerinConfigApi;
    private $envApi;
    private $tahunApi;
    private $hashids;

    public function __construct()
    {
        $this->templateApi = new TemplateApi();
        $this->jurusanApi = new JurusanApi();
        $this->notifApi = new NotifikasiApi();
        $this->roomApi = new RoomApi();
        $this->profileApi = new ProfileApi();
        $this->pertemuanApi = new PertemuanApi();
        $this->dataSiswaApi = new DataSiswaApi();
        $this->pesanApi = new PesanApi();
        $this->ortuApi = new OrangTuaApi();
        $this->sekolahApi = new SekolahApi();
        $this->alumniKategoriApi = new KategoriSurveyApi();
        $this->bkApi = new BKApi();
        $this->informasiApi = new InformasiApi();
        $this->pedomanApi = new PedomanApi();
        $this->bkkConfigApi = new ConfigApi();
        $this->prekerinConfigApi = new PrakerinConfigApi();
        $this->envApi = new EnvironmentApi();
        $this->tahunApi = new TahunAjaranApi();
        $this->hashids = new Hashids();
    }
    public function register()
    {
        //
        Schema::defaultStringLength(191);
    }

    public function boot()
    {
        View()->composer('includes.program-e_learning.menu.menu-admin', function ($view) {
            $template = $this->templateApi->get_all();
            $template = $template['body']['data'];
            $view->with('tema', $template);
        });

        View()->composer('includes.program-raport.menu.menu-admin', function ($view) {
            $template = $this->templateApi->get_all();
            $template = $template['body']['data'];
            $view->with('tema', $template);
        });

        View()->composer(['includes.profil_header', 'template.template_mobile.app'], function ($view) {
            $profile = $this->profileApi->get_profile();
            // dd($profile);
            $profile = $profile['body']['data'];
            //dd($profile);
            if (session('role') == 'guru') {
                $notifi = $this->notifApi->get_guru();
            } else {
                $notifi = $this->notifApi->get_siswa();
            }
            if (session('role') == 'guru' || session('role') == 'siswa') {
                $notifi = $notifi['body']['data'];
                $notif = [];
                foreach ($notifi as $nt) {
                    $notif[] = array(
                        'url' => $nt['url'],
                        'id' => $nt['id'],
                        'isi' => $nt['isi'],
                        'id_code' => $this->hashids->encode($nt['id']),
                        'id_code_room' => $this->hashids->encode($nt['id_room']),
                    );
                }
                $view->with(['profile' => $profile, 'notif' => $notif]);
            }

            $view->with(['profile' => $profile]);
        });

        View()->composer('content.master.pesan.list_message', function ($view) {
            $email = $this->profileApi->get_profile();
            $email = $email['body']['data']['email'];
            session()->put('email_pengirim', $email);
            $user = $this->pesanApi->tampil_chat();
            $user = $user['body']['data'];
            $data = [];
            foreach ($user as $us) {
                $user = $this->pesanApi->tampil_email($us['email']);
                // dd($user);
                $gambar = $user['body']['data']['file'];
                $data[] = array(
                    "nama" => $user['body']['data']['nama'],
                    "kode" => $user['body']['data']['kode'],
                    "email" => $us['email'],
                    "role" => $user['body']['data']['role'],
                    "pesan_terakhir" => $us['pesan_terakhir'],
                    "waktu" => $us['waktu'],
                    "file" => $gambar
                );
            }
            $view->with(['user' => $data, 'my_email' => $email]);
        });

        View()->composer(['includes.program-e_learning.room.menu-room', 'content.e_learning.room.v_data_room'], function ($view) {
            $id = request()->segment(4);
            $id_room = $this->hashids->decode($id)[0];
            $pertemuan = $this->pertemuanApi->get_all();
            $pertemuan = $pertemuan['body']['data'];
            $room = $this->roomApi->get_by_id($id_room);
            // dd($room);
            $room = $room['body']['data'];
            if (session("role") == "siswa") {
                $status = $this->dataSiswaApi->get_by_room_last_kelas($id_room, session('id_kelas_siswa'));
                // dd($status);
                $status = $status['body']['data'];
                $view->with(['room' => $room, 'pertemuan' => $pertemuan, 'status' => $status]);
            } else {
                $view->with(['room' => $room, 'pertemuan' => $pertemuan]);
            }
        });

        View()->composer([
            'content.bkk.dashboard.menu.menu-bkk-perusahaan',
            'content.bkk.dashboard.menu.menu-bkk-pelamar',
            'includes.program-bursa_kerja.menu.menu-bkk-admin',
            'content.bkk.dashboard.bkk-pelamar.profile.v_profile',
            'content.profile.layout.sidebar',
            'includes.program-e_learning.menu.menu-siswa_mobile',
            'includes.program-e_learning.menu.menu-learning-admin'
        ], function ($view) {
            $profile = $this->profileApi->get_profile();
            $profile = $profile['body']['data'];
            $view->with(['profile' => $profile]);
        });

        View()->composer(['content.profile.layout.profile', 'content.profile.layout.profile_bkk'], function ($view) {
            $profile = $this->profileApi->get_profile();
            $profile = $profile['body']['data'];
            if (session('role') == 'admin' || session('role') == 'admin-absensi') {
                $template = 'default';
            } else {
                $template = session('template');
            }
            $view->with(['data' => $profile, 'template' => $template]);
        });


        View()->composer('includes.program-sistem_pkl.menu.menu-siswa', function ($view) {
            $profile = $this->profileApi->get_profile();
            $profile = $profile['body']['data'];
            // dd($this->hashids->encode(session('id_prakerin_siswa')));
            // dd($profile);
            $profile['id_code'] = $this->hashids->encode($profile['id']);
            $view->with(['id_code' =>  $this->hashids->encode(session('id_prakerin_siswa'))]);
        });

        View()->composer('components.spp.user-dropdown', function ($view) {
            $profilUser = $this->profileApi->get_profile();
            $view->with([
                'profil' => $profilUser['body']['data']
            ]);
        });

        View()->composer('content.master.sekolah.v_menu_sekolah', function ($view) {
            $sekolah = $this->sekolahApi->get_by_id($this->hashids->decode(request()->segment(4))[0]);
            $sekolah = $sekolah['body']['data'];
            $view->with(['sekolah' => $sekolah]);
        });

        View()->composer(['content.prakerin.kaprodi.v_cetak', 'content.prakerin.pengunjung.v_beranda', 'content.bkk.pengunjung.v_home', 'includes.head', 'includes.footer', 'auth.login-revisi', 'auth.login_admin','ppdb.components.public.v_welcome','spp.components.v_pembayaran'], function ($view) {
            $domain = request()->getHost();
            $domain = str_replace('www.', '', $domain);
            $sekolah = $this->sekolahApi->search_by_domain($domain);
            // dd($sekolah);
            $sekolah = $sekolah['body']['data'];
            if ($sekolah != null) {
                session()->put('id_sekolah', $sekolah['id']);
                $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
                // dd($sekolah);
                $sekolah = $sekolah['body']['data'];
                session()->put('logo_sekolah', $sekolah['file']);
            }
            $view->with(['sekolah' => $sekolah]);
        });

        View()->composer(['content.point.bimbingan_konseling.email.v_menu_mail', 'includes.program-point.menu.menu-ortu', 'includes.program-point.menu.menu-bk'], function ($view) {
            if (session("role") == 'bk') {
                $email = $this->ortuApi->get_id_sekolah();
            } else {
                $email = $this->bkApi->get_by_sekolah();
            }
            // dd($email);
            $email = $email['body']['data'];
            $terima = $this->pesanApi->belum();
            //dd($terima);
            $dibaca = count($terima['body']['data']);
            $jurusan = $this->jurusanApi->get_by_sekolah();
            $jurusan = $jurusan['body']['data'];
            $view->with(['email' => $email, 'dibaca' => $dibaca, 'jurusan' => $jurusan]);
        });
        View()->composer(['content.supervisor.learning.kelas.v_menu', 'content.supervisor.learning.v_kalender'], function ($view) {
            $menu = $this->jurusanApi->menu();
            // dd($menu);
            $menu = $menu['body']['data'];
            // dd($menu);
            $view->with(['menu' => $menu]);
        });

        View()->composer(['components.penjamin_mutu.header-profil'], function ($view) {
            $id_user = session('id');
            $profile = $this->profileApi->get_profile_mutu($id_user);
            $profile = $profile["body"]["data"];
            //dd($profile);
            $view->with(['profile' => $profile]);
        });

        View()->composer(['components.sarana.header-profil-sarana'], function ($view) {
            $id_user = session('id');

            if (session('role-sarana') == 'admin') {
                $profile = $this->profileApi->get_profile();
            } else {
                $profile = $this->profileApi->get_profile_sarana_user($id_user);
            }
            $profile = $profile["body"]["data"];
            //dd($profile);
            $view->with(['profile' => $profile]);
        });

        //PERPUSTAKAAN
        View()->composer(['includes.program-perpus.nav.nav_user'], function ($view) {
            $profile = $this->profileApi->get_profile();
            $profile = $profile["body"]["data"];
            //dd($profile);
            $view->with(['profile' => $profile]);
        });

        View()->composer(['components.perpus.search.search'], function ($view) {
            $bahasaApi = new Master\BahasaApi();
            $gmdApi = new Master\GmdApi();
            $jenisApi = new Master\JenisApi();
            $koleksiApi = new Master\KoleksiApi();

            $bahasa = $bahasaApi->get_all();
            $gmd = $gmdApi->get_all();
            $jenis = $jenisApi->get_all();
            $koleksi = $koleksiApi->get_all();

            $bahasa = $bahasa['body']['data'];
            $gmd = $gmd['body']['data'];
            $jenis = $jenis['body']['data'];
            $koleksi = $koleksi['body']['data'];

            $view->with([
                'bahasa' => $bahasa,
                'gmd' => $gmd,
                'jenis' => $jenis,
                'koleksi' => $koleksi
            ]);
        });

        View()->composer(['includes.program-perpus.nav.nav_admin'], function ($view) {
            $profile = $this->profileApi->get_profile();
            $profile = $profile["body"]["data"];
            //dd($profile);
            $view->with(['profile' => $profile]);
        });


        View()->composer('content.alumni.survey.admin.v_survey', function ($view) {
            $kategori = $this->alumniKategoriApi->get_id_sekolah(session('id_sekolah'));
            // dd($terima);
            $kategori = $kategori['body']['data'];
            $view->with(['kategori' => $kategori]);
        });

        View()->composer('content.prakerin.pengunjung.menu_prakerin', function ($view) {
            $domain = request()->getHost();
            $domain = str_replace('www.', '', $domain);
            $sekolah = $this->sekolahApi->search_by_domain($domain);
            // dd($sekolah);
            $sekolah = $sekolah['body']['data'];
            if ($sekolah != null) {
                session()->put('id_sekolah', $sekolah['id']);
                $sekolah = $this->sekolahApi->get_by_id(session('id_sekolah'));
                // dd($sekolah);
                $sekolah = $sekolah['body']['data'];
                session()->put('logo_sekolah', $sekolah['file']);
            }
            $submenu = $this->informasiApi->pengunjung_menu(session('id_sekolah'));
            $submenu = $submenu['body']['data'];
            $menu_pedoman = $this->pedomanApi->pengunjung_menu(session('id_sekolah'));
            $menu_pedoman = $menu_pedoman['body']['data'];
            $config = $this->prekerinConfigApi->get_by_sekolah(session('id_sekolah'));
            $config = $config['body']['data'];
            $view->with(['submenu' => $submenu, 'config' => $config, 'menu_pedoman' => $menu_pedoman, 'sekolah' => $sekolah]);
        });

        View()->composer('content.bkk.pengunjung.main', function ($view) {
            $config = $this->bkkConfigApi->get_by_sekolah(session('id_sekolah'));
            $config = $config['body']['data'];
            $view->with(['config' => $config]);
        });

        View()->composer('content.bkk.pengunjung.faq.v_diskusi', function ($view) {
            $forum = $this->envApi->by_jenis_sekolah('forum', session('id_sekolah'));
            $forum = $forum['body']['data'];
            $chat = $this->envApi->by_jenis_sekolah('lifechat', session('id_sekolah'));
            $chat = $chat['body']['data'];
            $view->with(['forum' => $forum, 'chat' => $chat]);
        });

        View()->composer(['includes.program-cbt.menu.menu-admin-cbt'], function ($view) {
            $tahun = $this->tahunApi->get_aktif(session('id_sekolah'));
            // dd($tahun);
            $tahun = $tahun['body']['data'];
            // dd($tahun);
            $view->with(['tahun' => $tahun]);
        });
    }
}
