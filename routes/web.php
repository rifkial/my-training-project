<?php

use App\Http\Controllers\Absensi\AbsenController;
use App\Http\Controllers\Absensi\GuruController as AbsensiGuruController;
use App\Http\Controllers\Absensi\HadirController;
use App\Http\Controllers\Absensi\IzinController;
use App\Http\Controllers\Absensi\jadwalController as AbsensiJadwalController;
use App\Http\Controllers\Absensi\LiburController;
use App\Http\Controllers\Absensi\MenuController;
use App\Http\Controllers\Absensi\ProfileController as AbsensiProfileController;
use App\Http\Controllers\Absensi\SiswaController as AbsensiSiswaController;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Help\DetailModalController;

// Master
use App\Http\Controllers\Master\JurusanController;
use App\Http\Controllers\Master\KelasController;
use App\Http\Controllers\Master\ProgramTemplateController;
use App\Http\Controllers\Master\KalenderController;
use App\Http\Controllers\Master\MapelController;
use App\Http\Controllers\Master\RombelController;
use App\Http\Controllers\Master\KelasSiswaController;
use App\Http\Controllers\Master\GuruPelajaranController;
use App\Http\Controllers\Master\EkstrakurikulerController;
use App\Http\Controllers\Master\JadwalPelajaranController;
use App\Http\Controllers\Master\KalenderRombelController;
use App\Http\Controllers\Master\TahunAjaranController;
use App\Http\Controllers\Master\DashboardController;
use App\Http\Controllers\Master\IndonesiaController;
use App\Http\Controllers\Master\JabatanSupervisorController;
use App\Http\Controllers\Master\PesanController;

// User
use App\Http\Controllers\User\GuruController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\SupervisorController;
use App\Http\Controllers\User\SiswaController;
use App\Http\Controllers\User\TataUsahaController;
use App\Http\Controllers\User\WaliKelasController;
use App\Http\Controllers\User\AlumniController;
use App\Http\Controllers\User\BKController;
use App\Http\Controllers\User\OrtuController;
use App\Http\Controllers\User\BKKController;
use App\Http\Controllers\User\AdminBKKController;

//Admin
use App\Http\Controllers\Admin\AdminSekolahController;
use App\Http\Controllers\Admin\DasarController;
use App\Http\Controllers\Admin\Learning\LearningController;
use App\Http\Controllers\Admin\Raport\ConfigController;
use App\Http\Controllers\Admin\Raport\KopSampulController;
use App\Http\Controllers\Admin\Raport\NilaiConfigController;
use App\Http\Controllers\Admin\Raport\PredikatController;
use App\Http\Controllers\Admin\Raport\SikapSosialController;
use App\Http\Controllers\Admin\Raport\SikapSpiritualController;
use App\Http\Controllers\Admin\Raport\TemplateCatatanController;
use App\Http\Controllers\Admin\Raport\TemplateSampulController;
use App\Http\Controllers\Admin\RoomController as AdminRoomController;
use App\Http\Controllers\Alumni\AdminController as AlumniAdminController;
use App\Http\Controllers\Alumni\AgendaController as AlumniAgendaController;
use App\Http\Controllers\Alumni\AlumniController as AlumniAlumniController;
use App\Http\Controllers\Alumni\BlogController;
use App\Http\Controllers\Alumni\FakultasController;
use App\Http\Controllers\Alumni\GaleriController;
use App\Http\Controllers\Alumni\JenisPekerjaanController;
use App\Http\Controllers\Alumni\JurusanController as AlumniJurusanController;
use App\Http\Controllers\Alumni\KategoriBlogController;
use App\Http\Controllers\Alumni\KategoriGaleriController;
use App\Http\Controllers\Alumni\KategoriSurveyController;
use App\Http\Controllers\Alumni\KomentarController;
use App\Http\Controllers\Alumni\OrganisasiController;
use App\Http\Controllers\Alumni\PekerjaanController;
use App\Http\Controllers\Alumni\PendidikanController as AlumniPendidikanController;
use App\Http\Controllers\Alumni\SosialController;
use App\Http\Controllers\Alumni\SurveyController;
//SuperMaster
use App\Http\Controllers\SuperMaster\KategoriProgramController;
use App\Http\Controllers\SuperMaster\ProgramController;
use App\Http\Controllers\SuperMaster\DashboardController as SuperMasterDashboardController;
use App\Http\Controllers\SuperMaster\TemplateSertifikatController;


// Learning
use App\Http\Controllers\Learning\KalenderKelasController;
use App\Http\Controllers\Learning\KompetensiDasarController;
use App\Http\Controllers\Learning\MapelKelasController;
use App\Http\Controllers\Learning\RoomController;
use App\Http\Controllers\Learning\DataSiswaController;
use App\Http\Controllers\Learning\JurnalController;
use App\Http\Controllers\Learning\MapelWaliKelasController;
use App\Http\Controllers\Learning\TugasKetrampilanController;
use App\Http\Controllers\Learning\TugasPengetahuanController;
use App\Http\Controllers\Learning\SettingController;
use App\Http\Controllers\Learning\AbsensiController;
use App\Http\Controllers\Learning\AbsensiWaliKelasController;
use App\Http\Controllers\Learning\AdminController;
use App\Http\Controllers\Learning\BahanAjarController;
use App\Http\Controllers\Learning\BahanAjarWaliKelasController;
use App\Http\Controllers\Learning\JawabanKetrampilanController;
use App\Http\Controllers\Learning\JawabanPengetahuanController;
use App\Http\Controllers\Learning\KalenderWaliKelasController;
use App\Http\Controllers\Learning\KompetensiIntiController;
use App\Http\Controllers\Learning\TugasWaliKelasController;



//Raport
use App\Http\Controllers\Raport\NilaiPengetahuanController;
use App\Http\Controllers\Raport\NilaiSikapSosialController;
use App\Http\Controllers\Raport\NilaiSikapSpiritualController;
use App\Http\Controllers\Raport\RiwayatMengajarController;
use App\Http\Controllers\Raport\AbsensiController as RaportAbsensiController;
use App\Http\Controllers\Raport\CatatanController;
use App\Http\Controllers\Raport\CetakLegerController;
use App\Http\Controllers\Raport\CetakRaportController;
use App\Http\Controllers\Raport\MapelController as RaportMapelController;
use App\Http\Controllers\Raport\NilaiEkstrakurikulerController;
use App\Http\Controllers\Raport\NilaiKetrampilanController;
use App\Http\Controllers\Raport\PrestasiController;

//Point
use App\Http\Controllers\Point\PesanController as PointPesanController;
use App\Http\Controllers\Point\WaliMuridController;
use App\Http\Controllers\Raport\NilaiController;
use App\Http\Controllers\Point\EmailController;

// Kesiswaan
use App\Http\Controllers\Kesiswaan\HistoriPelanggaranController;
use App\Http\Controllers\Kesiswaan\KategoriPelanggaranController;
use App\Http\Controllers\Kesiswaan\PelanggaranController;
use App\Http\Controllers\Kesiswaan\PelanggaranSiswaController;
use App\Http\Controllers\Kesiswaan\SanksiController;

//BKK
use App\Http\Controllers\BKK\Admin\ProfilBKKController;
use App\Http\Controllers\BKK\Admin\UserBKKController;
use App\Http\Controllers\BKK\AgendaController;
use App\Http\Controllers\BKK\BidangLokerController;
use App\Http\Controllers\BKK\DokumenController;
use App\Http\Controllers\BKK\FeedCommentController;
use App\Http\Controllers\BKK\FeedController;
use App\Http\Controllers\BKK\IndustriController;
use App\Http\Controllers\BKK\KetrampilanController;
use App\Http\Controllers\BKK\LokerController;
use App\Http\Controllers\BKK\PelamarController;
use App\Http\Controllers\BKK\PendidikanController;
use App\Http\Controllers\BKK\PengalamanController;
use App\Http\Controllers\Cbt\AdminController as CbtAdminController;
use App\Http\Controllers\Cbt\BankSoalController;
use App\Http\Controllers\Cbt\GuruController as CbtGuruController;
use App\Http\Controllers\Cbt\JadwalController as CbtJadwalController;
use App\Http\Controllers\Cbt\JenisController as CbtJenisController;
use App\Http\Controllers\Cbt\KelasRuangController;
use App\Http\Controllers\Cbt\MapelController as CbtMapelController;
use App\Http\Controllers\Cbt\RuangController;
use App\Http\Controllers\Cbt\SesiController;
use App\Http\Controllers\Cbt\SettingController as CbtSettingController;
use App\Http\Controllers\Cbt\SiswaController as CbtSiswaController;
use App\Http\Controllers\Cbt\SoalController;
use App\Http\Controllers\Cbt\TokenController;
use App\Http\Controllers\Learning\FeedsRombelController;
use App\Http\Controllers\Pengunjung\BKKController as PengunjungBKKController;
use App\Http\Controllers\Induk\AdminController as IndukAdminController;
use App\Http\Controllers\Induk\GuruController as IndukGuruController;
use App\Http\Controllers\Induk\SiswaController as IndukSiswaController;
use App\Http\Controllers\Induk\TataUsahaController as IndukTataUsahaController;
use App\Http\Controllers\Kesiswaan\AbsensiKesiswaanController;
use App\Http\Controllers\Kesiswaan\AdminKesiswaanController;
use App\Http\Controllers\Kesiswaan\AnggotaEkskulController;
use App\Http\Controllers\Kesiswaan\PrestasiGuruController;
use App\Http\Controllers\Kesiswaan\ProgramKerjaController;
use App\Http\Controllers\Kesiswaan\SiswaController as KesiswaanSiswaController;
use App\Http\Controllers\Master\ConfigController as MasterConfigController;
use App\Http\Controllers\Master\EnvironmentController;
use App\Http\Controllers\Master\KategoriEkstraController;
use App\Http\Controllers\Master\KategoriLombaController;
use App\Http\Controllers\Master\MutasiController;
use App\Http\Controllers\Master\PrestasiSiswaController;
use App\Http\Controllers\Master\SliderController;
use App\Http\Controllers\Pengunjung\AbsensiController as PengunjungAbsensiController;
//Prakerin
use App\Http\Controllers\Prakerin\AjaxController;
use App\Http\Controllers\Prakerin\IndustriController as PrakerinIndustriController;
use App\Http\Controllers\Prakerin\JenisNilaiController;
use App\Http\Controllers\Prakerin\KelompokController;
use App\Http\Controllers\Prakerin\NilaiController as PrakerinNilaiController;
use App\Http\Controllers\Prakerin\NilaiSiswaController;
use App\Http\Controllers\Prakerin\PembimbingIndustriController;
use App\Http\Controllers\Prakerin\SettingController as PrakerinSettingController;
use App\Http\Controllers\Prakerin\SiswaPesertaController;
use App\Http\Controllers\Prakerin\TugasController;
use App\Http\Controllers\Pengunjung\PrakerinController;
use App\Http\Controllers\Point\BeasiswaController;
use App\Http\Controllers\Point\JenisBeasiswaController;
use App\Http\Controllers\Point\KarirController;
use App\Http\Controllers\Point\SiswaController as PointSiswaController;
use App\Http\Controllers\Prakerin\AdminController as PrakerinAdminController;
use App\Http\Controllers\SuperMaster\BackupController;
use App\Http\Controllers\SuperMaster\SupermasterController;
use App\Http\Controllers\User\AdminLearningController;

// SPP
use App\Http\Controllers\SPP\KonfirmasiController;
use App\Http\Controllers\SPP\LaporanController;
use App\Http\Controllers\SPP\PosController;
use App\Http\Controllers\SPP\PosPemasukanController;
use App\Http\Controllers\SPP\SiswaController as SPPSiswaController;
use App\Http\Controllers\SPP\JurusanController as SPPJurusanController;
use App\Http\Controllers\SPP\SPPController;
use App\Http\Controllers\SPP\TagihanController;
use App\Http\Controllers\SPP\TagihanSettingController;
use App\Http\Controllers\SPP\TransaksiController;
use App\Http\Controllers\SuperMaster\AjaxController as SuperMasterAjaxController;
use App\Http\Controllers\User\KaprodiController;

//PenjaminMutu
use App\Http\Controllers\PenjaminMutu\DashboardController as PMDashboardController;
use App\Http\Controllers\PenjaminMutu\DokumenController as PMDokumenController;
use App\Http\Controllers\PenjaminMutu\PesanController as PMPesanController;
use App\Http\Controllers\PenjaminMutu\Master\KategoriController;
use App\Http\Controllers\PenjaminMutu\Master\SubKategoriController;

//user penjamin mutu
use App\Http\Controllers\PenjaminMutu\UserController as PMUserController;

//MasterSekolah
use App\Http\Controllers\Sekolah\FakultasController as PMFakultasController;
use App\Http\Controllers\Sekolah\JenjangController as PMJenjangController;
use App\Http\Controllers\Sekolah\KejuruanController as PMKejuruanController;


//Sarana Prasarana
use App\Http\Controllers\Sarana\DashboardController as SPDashboardController;
use App\Http\Controllers\Sarana\Peminjaman\PeminjamanController as PeminjamanController;
use App\Http\Controllers\Sarana\Pemusnahan\PemusnahanController as PemusnahanController;
use App\Http\Controllers\Sarana\Pengadaan\PengadaanController as PengadaanController;
use App\Http\Controllers\Sarana\Laporan\LaporanController as SPLaporanController;
//user
use App\Http\Controllers\Sarana\ProfileController as SPProfileController;
//master
use App\Http\Controllers\Sarana\Barang\BarangController;
use App\Http\Controllers\Sarana\Item\ItemController;
use App\Http\Controllers\Sarana\Master\KategoriBarangController;
use App\Http\Controllers\Sarana\Master\JenisBarangController;
use App\Http\Controllers\Sarana\Master\LokasiBarangController;
use App\Http\Controllers\Sarana\Master\SupllyerBarangController;
use App\Http\Controllers\Sarana\Master\SatuanBarangController;
//user
use App\Http\Controllers\Sarana\User\AdminBarangController;
use App\Http\Controllers\Sarana\User\UserBarangController;
use App\Http\Controllers\Sarana\Barcode\BarcodeController;


//ppdb
use App\Http\Controllers\Ppdb\PpdbController;
use App\Http\Controllers\Ppdb\Akun\AdminUserPpdb;
use App\Http\Controllers\Ppdb\Akun\PesertaUserPpdb;
use App\Http\Controllers\Ppdb\Download\BrosurController;
use App\Http\Controllers\Ppdb\Download\SampledocController;
use App\Http\Controllers\Ppdb\Informasi\BannerController;
use App\Http\Controllers\Ppdb\Informasi\FaqController;
use App\Http\Controllers\Ppdb\Informasi\JadwalController;
use App\Http\Controllers\Ppdb\Informasi\NoticeController;
use App\Http\Controllers\Ppdb\Informasi\PanduanController;
use App\Http\Controllers\Ppdb\Informasi\RulesController;
use App\Http\Controllers\Ppdb\Informasi\SyaratController;
use App\Http\Controllers\Ppdb\PaymentController;
use App\Http\Controllers\Ppdb\Pesan\PesanAdminPpdbContoller;
use App\Http\Controllers\Ppdb\Pesan\PesanUserPpdbContoller;
use App\Http\Controllers\Ppdb\Peserta\DokumenSupController;
use App\Http\Controllers\Ppdb\Peserta\PendaftarController;
use App\Http\Controllers\Ppdb\ProfilAdminController;
use App\Http\Controllers\Ppdb\ProfilUserController;
use App\Http\Controllers\Ppdb\PublicController;
use App\Http\Controllers\Ppdb\Setting\SettingPpdbController;
use App\Http\Controllers\Ppdb\UserController;
use App\Http\Controllers\Prakerin\ConfigController as PrakerinConfigController;
use App\Http\Controllers\Prakerin\InformasiController;
use App\Http\Controllers\Prakerin\PedomanController;
use App\Http\Controllers\Prakerin\PembimbingSertifikatController;
use App\Http\Controllers\Raport\AdminController as RaportAdminController;
use App\Http\Controllers\Raport\TranskipController;


//Perpustakaan
use App\Http\Controllers\Perpus\Home\HomePerpusController;
use App\Http\Controllers\Perpus\Dashboard\DashboardController as DashboardPerpusController;
use App\Http\Controllers\Perpus\Buku\BukuController;
use App\Http\Controllers\Perpus\PustakaDigital\PustakaDigitalController;
use App\Http\Controllers\Perpus\Eskemplar\EskemplarController;
use App\Http\Controllers\Perpus\Denda\DendaController;
use App\Http\Controllers\Perpus\Kartu\KartuController;
use App\Http\Controllers\Perpus\Label\LabelController;
use App\Http\Controllers\Perpus\Profile\UserPerpusController;


use App\Http\Controllers\SumbanganSpp\ReportController;
//spp template new
use App\Http\Controllers\SumbanganSpp\SPPSumbangan;
use App\Http\Controllers\SumbanganSpp\SettingTagihan;
use App\Http\Controllers\SumbanganSpp\TransaksiSpp;
use App\Http\Controllers\Perpus\Profile\AdminPerpusController;
use App\Http\Controllers\Perpus\Peminjaman\PeminjamanController as PeminjamanPerpusController;
use App\Http\Controllers\Perpus\Pengadaan\PengadaanController as PengadaanPerpusController;
use App\Http\Controllers\Perpus\Laporan\LaporanController as LaporanPerpusController;
use App\Http\Controllers\Perpus\Pengumuman\PengumumanController as PengumumanPerpusController;
use App\Http\Controllers\Perpus\Master\AgenController;
use App\Http\Controllers\Perpus\Master\BahasaController;
use App\Http\Controllers\Perpus\Master\JenisController;
use App\Http\Controllers\Perpus\Master\GmdController;
use App\Http\Controllers\Perpus\Master\PenerbitController;
use App\Http\Controllers\Perpus\Master\KoleksiController;
use App\Http\Controllers\Perpus\Master\PengarangController;
use App\Http\Controllers\Perpus\Master\TopikController;
use App\Http\Controllers\Perpus\Master\RakController;
// use App\Http\Controllers\Perpus\Master\AgenController;
use App\Http\Controllers\Raport\Nilaik16Controller;
use App\Http\Controllers\SumbanganSpp\AkuntansiController;
use App\Http\Controllers\SumbanganSpp\IntergrasiController;
//spp template new
use App\Http\Controllers\SumbanganSpp\Konfirmasi;

Route::prefix('auth')->name('auth.')->group(function () {
    Route::get('login', 'AuthController@login')->name('login');
    Route::get('login/adm', 'AuthController@login_adm')->name('login_admin');
    Route::get('su/adm', 'AuthController@superadmin')->name('login_superadmin');
    Route::get('register', 'AuthController@register')->name('register');
    Route::post('verifylogin', 'AuthController@verifylogin')->name('verifylogin');
    Route::post('login_demo', 'AuthController@login_demo')->name('login_demo');
    Route::get('reset_password', 'AuthController@reset_password')->name('resetPassword');
    Route::post('check_email', 'AuthController@checkEmail')->name('check_email');
    Route::post('verifyRegister', 'AuthController@verifyregister')->name('verifyRegister');
    Route::get('logout', 'AuthController@logout')->name('logout');
});

Route::get('/', 'Master\DashboardController@program')->name('home');
Route::get('bursa_kerja', [PengunjungBKKController::class, 'home'])->name('bursa_kerja-home');
Route::prefix('bursa_kerja')->group(function () {
    Route::get('profile', [PengunjungBKKController::class, 'profile'])->name('bursa_kerja-profile');
    Route::get('pekerjaan', [PengunjungBKKController::class, 'index'])->name('bursa_kerja-beranda');
    Route::post('side_detail', [LokerController::class, 'side_detail'])->name('bkk_loker-side_details');
    Route::get('daftar-perusahaan', [PengunjungBKKController::class, 'perusahaan'])->name('bursa_kerja-perusahaan');
    Route::get('detail_loker', [PengunjungBKKController::class, 'detail_loker'])->name('bursa_kerja-detail_lowongan');
    Route::get('detail-perusahaan', [PengunjungBKKController::class, 'detailPerusahaan'])->name('bursa_kerja-detail_perusahaan');
    Route::get('more-perusahaan', [PengunjungBKKController::class, 'perusahaan'])->name('bursa_kerja-more_perusahaan');
    Route::get('daftar-lulusan', [PengunjungBKKController::class, 'lulusan'])->name('bursa_kerja-lulusan');
    Route::post('store-alumni', [PengunjungBKKController::class, 'store_alumni'])->name('bursa_kerja-simpan_alumni');
    Route::get('get_all', [LokerController::class, 'get_all'])->name('bkk_loker-get_all');
    Route::get('daftar-perusahaan/filter', [PengunjungBKKController::class, 'search'])->name('bkk_industri-search_industri');
    Route::post('search', [LokerController::class, 'search'])->name('bkk_loker-search_pelamars');
    Route::prefix('faq')->group(function () {
        Route::get('diskusi', [PengunjungBKKController::class, 'diskusi'])->name('bkk_loker-diskusi');
        Route::get('question', [PengunjungBKKController::class, 'question'])->name('bkk_loker-question');
        Route::post('store_diskusi', [PengunjungBKKController::class, 'store_diskusi'])->name('bkk_loker-simpan_diskusi');
        Route::post('get_id', [FeedController::class, 'detail'])->name('bkk_loker-detail_feed');
        Route::post('store_comment', [FeedCommentController::class, 'store'])->name('bkk_loker-comment_feed_simpan');
        Route::post('update_status', [FeedController::class, 'update_status'])->name('bkk_loker-update_status_feed');
        Route::post('delete', [FeedController::class, 'soft_delete'])->name('bkk_loker-delete_feed');
    });
});

Route::prefix('absensi')->group(function () {
    Route::get('siswa', [PengunjungAbsensiController::class, 'index'])->name('halaman_absensi_siswa');
    Route::post('save_absensi', [PengunjungAbsensiController::class, 'store'])->name('absensi-save_absen');
    Route::get('download', [PengunjungAbsensiController::class, 'download_pdf'])->name('absensi-download_pdf');
});


Route::get('prakerind', [PrakerinController::class, 'index'])->name('prakerin-beranda');
Route::prefix('prakerind')->group(function () {
    Route::get('informasi', [PrakerinController::class, 'informasi'])->name('prakerin-informasi');
    Route::get('industri', [PrakerinController::class, 'industri'])->name('prakerin-industri');
    Route::get('pedoman', [PrakerinController::class, 'pedoman'])->name('prakerin-pedoman');
    Route::post('search', [PrakerinIndustriController::class, 'search'])->name('pkl_industri-search_name');
    Route::get('informasi/{slug}', [PrakerinController::class, 'detail_informasi'])->name('prakerin_informasi-detail');
    Route::get('pedoman/{slug}', [PrakerinController::class, 'detail_pedoman'])->name('prakerin_pedoman-detail');
});


Route::get('sarana', [SPDashboardController::class, 'index'])->name('sarana-beranda');

Route::prefix('perpus')->group(function () {
    Route::get('/public', function () {
        return view('content.perpus.home.home');
    })->name('semua-barang');

    Route::get('detail-buku', function () {
        return view('content.perpus.buku.detail_buku');
    });
});


Route::middleware(['check.api.token'])->group(function () {
    Route::post('session', [SessionController::class, 'program'])->name('session-program');
    Route::get('admin-sekolah', 'Admin\AdminSekolahController@dashboard')->name('dashboard_admin_sekolah');
    Route::get('edit', [ProfileController::class, 'edit'])->name('edit-profile_admin');
    Route::post('update', [ProfileController::class, 'update'])->name('update-profile_admin');
    Route::get('reset-pwd', [ProfileController::class, 'reset_passwords'])->name('reset_password');
    Route::get('reset-password', [ProfileController::class, 'reset_password'])->name('reset_password-profile_admin');
    Route::post('change-password', [ProfileController::class, 'change_password'])->name('change_password');
    Route::post('update-profile', [ProfileController::class, 'update'])->name('update-profile_tanpa_slug');
    Route::get('v_change-password', [ProfileController::class, 'v_change_password'])->name('view_change_password-admin');
    Route::post('read', [RoomController::class, 'read'])->name('room-read_notif');
    Route::get('read_all', [RoomController::class, 'read_all'])->name('room-read_all_notif');

    Route::prefix('detail_modal')->group(function () {
        Route::prefix('detail')->group(function () {
            Route::post('industri', [DetailModalController::class, 'detail_industri'])->name('pkl-detail_industri');
            Route::post('siswa', [DetailModalController::class, 'detail_siswa'])->name('detail_siswa');
            Route::post('guru', [DetailModalController::class, 'detail_guru'])->name('detail_guru');
        });
    });

    Route::prefix('admin')->group(function () {
        //menu tampilan admin
        Route::prefix('master')->group(function () {
            Route::get('get_statistic', [DashboardController::class, 'get_dashboard_admin'])->name('master-statistic');
            Route::get('stats_admin_alumni', [DashboardController::class, 'dashboard_alumni'])->name('admin_alumni-statistic');
            Route::get('dashboard', [MapelKelasController::class, 'index'])->name('dashboard-master');
            //admin tahun ajaran
            Route::get('tahun_ajaran', [TahunAjaranController::class, 'index'])->name('master-tahun_ajaran');
            Route::prefix('tahun_ajaran')->group(function () {
                Route::post('aktif', [TahunAjaranController::class, 'update_aktif'])->name('update-tahun_ajaran_aktif');
                Route::put('update', [TahunAjaranController::class, 'update'])->name('update-tahun_ajaran');
                Route::post('store', [TahunAjaranController::class, 'store'])->name('store-tahun_ajaran');
                Route::post('edit', [TahunAjaranController::class, 'edit'])->name('edit-tahun_ajaran');
                Route::post('trash', [TahunAjaranController::class, 'trash'])->name('delete-tahun_ajaran');
                Route::get('data-trash', [TahunAjaranController::class, 'data_trash'])->name('data_trash-tahun_ajaran');
                Route::delete('hard_delete/{id}', [TahunAjaranController::class, 'delete'])->name('hard_delete-tahun_ajaran');
                Route::patch('restore/{id}', [TahunAjaranController::class, 'restore'])->name('restore-tahun_ajaran');
            });
            //admin jurusan
            Route::get('jurusan', [JurusanController::class, 'index'])->name('e_learning-jurusan');
            Route::prefix('jurusan')->group(function () {
                Route::post('store', [JurusanController::class, 'store'])->name('store-jurusan');
                Route::post('edit', [JurusanController::class, 'edit'])->name('edit-jurusan');
                Route::put('update', [JurusanController::class, 'update'])->name('update-jurusan');
                Route::delete('trash/{id}', [JurusanController::class, 'trash'])->name('trash-jurusan');
                Route::get('data-trash', [JurusanController::class, 'data_trash'])->name('data_trash-jurusan');
                Route::delete('hard_delete/{id}', [JurusanController::class, 'delete'])->name('delete-jurusan');
                Route::patch('restore/{id}', [JurusanController::class, 'restore'])->name('restore-jurusan');
                Route::post('upload_data', [JurusanController::class, 'import'])->name('import-jurusan');
            });
            //admin kelas
            Route::get('kelas', [KelasController::class, 'index'])->name('e_learning-kelas');
            Route::prefix('kelas')->group(function () {
                Route::post('store', [KelasController::class, 'store'])->name('store-kelas');
                Route::post('edit', [KelasController::class, 'edit'])->name('edit-kelas');
                Route::put('update', [KelasController::class, 'update'])->name('update-kelas');
                // Route::get('export', [KelasController::class, 'export'])->name('export-kelas');
                Route::post('jurusan', [KelasController::class, 'get_by_jurusan'])->name('get_kelas-jurusan');
                Route::get('habis/semua/kosong', [KelasController::class, 'get_jurusan_kosong'])->name('get_kelas-jurusan_kosong');
                Route::post('get_jurusan', [KelasController::class, 'get_jurusan'])->name('get-jurusan');
                Route::post('load_jurusan', [KelasController::class, 'load_jurusan'])->name('edit_data-jurusan');
                Route::get('download', [KelasController::class, 'download'])->name('download-kelas');
                Route::delete('delete/{id}', [KelasController::class, 'delete'])->name('delete-kelas');
                Route::get('data-trash', [KelasController::class, 'data_trash'])->name('data_trash-kelas');
                Route::patch('restore/{id}', [KelasController::class, 'restore'])->name('restore-kelas');
                Route::delete('hard_delete/{id}', [KelasController::class, 'hard_delete'])->name('delete-kelas_permanent');
                Route::post('import', [KelasController::class, 'import'])->name('import-kelas');
            });
            //admin rombel
            Route::get('rombongan_belajar', [RombelController::class, 'index'])->name('e_learning-rombel');
            Route::prefix('rombongan_belajar')->group(function () {
                Route::post('store', [RombelController::class, 'store'])->name('store-rombel');
                Route::post('edit', [RombelController::class, 'edit'])->name('edit-rombel');
                Route::post('deletes', [RombelController::class, 'deletes'])->name('deletes-rombel');
                Route::post('get_kelas', [RombelController::class, 'get_kelas'])->name('get-rombel_kelas');
                Route::post('get_kelas_edit', [RombelController::class, 'get_kelas_edit'])->name('get-rombel_kelas_edit');
                Route::put('update', [RombelController::class, 'update'])->name('update-rombel');
                Route::post('edit_kelas_select', [RombelController::class, 'edit_kelas_select'])->name('load_rombel-kelas_select');
                Route::delete('trash/{id}', [RombelController::class, 'trash'])->name('trash-rombel');
                Route::get('data-trash', [RombelController::class, 'data_trash'])->name('data_trash-rombel');
                Route::delete('delete/{id}', [RombelController::class, 'delete'])->name('delete-rombel');
                Route::patch('restore/{id}', [RombelController::class, 'restore'])->name('restore-rombel');
                Route::post('upload_data', [RombelController::class, 'import'])->name('import-rombel');
                // Route::get('rombel-trash', [RombelController::class, 'data_trash']);
            });
            //admin mapel
            Route::get('mapel', [MapelController::class, 'index'])->name('e_learning-mapel');
            Route::prefix('mapel')->group(function () {
                Route::post('store', [MapelController::class, 'store'])->name('store-mapel');
                Route::post('edit', [MapelController::class, 'edit'])->name('edit-mapel');
                Route::put('update', [MapelController::class, 'update'])->name('update-mapel');
                Route::delete('trash/{id}', [MapelController::class, 'trash'])->name('trash-mapel');
                Route::get('data-trash', [MapelController::class, 'data_trash'])->name('data_trash-mapel');
                Route::delete('delete/{id}', [MapelController::class, 'delete'])->name('delete-mapel');
                Route::patch('restore/{id}', [MapelController::class, 'restore'])->name('restore-mapel');
                Route::post('import', [MapelController::class, 'import'])->name('import-mapel');
            });
            //admin kategori ekstrakurikuler
            Route::get('kategori_ekstra', [KategoriEkstraController::class, 'index'])->name('master-kategori_ekstrakurikuler');
            Route::prefix('kategori_ekstra')->group(function () {
                Route::post('store', [KategoriEkstraController::class, 'store'])->name('store-kategori_ekstrakurikuler');
                Route::post('edit', [KategoriEkstraController::class, 'edit'])->name('edit-kategori_ekstrakurikuler');
                Route::put('update', [KategoriEkstraController::class, 'update'])->name('update-kategori_ekstrakurikuler');
                Route::post('trash', [KategoriEkstraController::class, 'trash'])->name('trash-kategori_ekstrakurikuler');
                Route::post('import', [KategoriEkstraController::class, 'import'])->name('import-kategori_ekstra');
            });
            // admin ekstrakurikuler
            Route::get('ekstrakurikuler', [EkstrakurikulerController::class, 'index'])->name('e_learning-ekstrakurikuler');
            Route::prefix('ekstrakurikuler')->group(function () {
                Route::post('store', [EkstrakurikulerController::class, 'store'])->name('store-ekstrakurikuler');
                Route::post('edit', [EkstrakurikulerController::class, 'edit'])->name('edit-ekstrakurikuler');
                Route::put('update', [EkstrakurikulerController::class, 'update'])->name('update-ekstrakurikuler');
                Route::post('trash', [EkstrakurikulerController::class, 'trash'])->name('trash-ekstrakurikuler');
                Route::post('import', [EkstrakurikulerController::class, 'import'])->name('import-ekstra');
                Route::post('upload', [EkstrakurikulerController::class, 'upload'])->name('upload-file_ekstra');
                Route::post('delete-file', [EkstrakurikulerController::class, 'delete_file'])->name('delete-file_ekstra');
                Route::get('download/file/{based}/{id}', [EkstrakurikulerController::class, 'download_file'])->name('download-file');
            });

            //admin jabatan supervisor
            Route::get('jabatan', [JabatanSupervisorController::class, 'index'])->name('master-jabatan_supervisor');
            Route::prefix('jabatan')->group(function () {
                Route::post('store', [JabatanSupervisorController::class, 'store'])->name('store-jabatan_supervisor');
                Route::post('edit', [JabatanSupervisorController::class, 'edit'])->name('edit-jabatan_supervisor');
                Route::put('update', [JabatanSupervisorController::class, 'update'])->name('update-jabatan_supervisor');
                Route::delete('trash/{id}', [JabatanSupervisorController::class, 'delete'])->name('trash-jabatan_supervisor');
                Route::get('data-trash', [JabatanSupervisorController::class, 'data_trash'])->name('data_trash-jabatan_supervisor');
                Route::delete('hard_delete/{id}', [JabatanSupervisorController::class, 'hard_delete'])->name('delete-jabatan_supervisor');
                Route::patch('restore/{id}', [JabatanSupervisorController::class, 'restore'])->name('restore-jabatan_supervisor');
            });
            //Mutasi Siswa
            Route::get('mutasi_siswa', [MutasiController::class, 'index'])->name('master-mutasi_siswa');
            Route::prefix('mutasi_siswa')->group(function () {
                Route::post('data_siswa', [MutasiController::class, 'data_siswa'])->name('master_mutasi-data_siswa');
                Route::post('store', [MutasiController::class, 'store'])->name('master_mutasi-tambah');
                Route::post('create_in', [MutasiController::class, 'store_in'])->name('master_mutasi-tambah_masuk');
                Route::post('detail', [MutasiController::class, 'detail'])->name('master_mutasi-detail');
                Route::put('update', [MutasiController::class, 'update'])->name('master_mutasi-update');
                Route::post('trash', [MutasiController::class, 'delete'])->name('master_mutasi-soft_delete');
            });
            //admin kompetensi inti
            Route::get('kompetensi_inti', [KompetensiIntiController::class, 'index'])->name('e_learning-kompetensi_inti');
            Route::prefix('kompetensi_inti')->group(function () {
                Route::post('store', [KompetensiIntiController::class, 'store'])->name('store-kompetensi_inti');
                Route::put('update', [KompetensiIntiController::class, 'update'])->name('update-kompetensi_inti');
                Route::post('edit', [KompetensiIntiController::class, 'edit'])->name('edit-kompetensi_inti');
                Route::post('delete', [KompetensiIntiController::class, 'trash'])->name('delete-kompetensi_inti');
                Route::get('data-trash', [KompetensiIntiController::class, 'data_trash'])->name('data_trash-kompetensi_inti');
                Route::delete('hard_delete/{id}', [KompetensiIntiController::class, 'delete'])->name('delete-kompetensi_inti');
                Route::patch('restore/{id}', [KompetensiIntiController::class, 'restore'])->name('restore-kompetensi_inti');
                Route::post('template', [KompetensiIntiController::class, 'template'])->name('template-kompetensi_inti');
                Route::post('import', [KompetensiIntiController::class, 'import'])->name('import-kompetensi_inti');
            });
            //admin kompetensi dasar
            Route::post('kompetensi_dasar', [DasarController::class, 'edit_kd'])->name('master-edit_kompetensi_dasar');
            Route::prefix('kompetensi_dasar')->group(function () {
                Route::post('simpan', [DasarController::class, 'simpan'])->name('master-simpan_kompetensi_dasar');
                Route::post('simpan_ketrampilan', [DasarController::class, 'simpan_ketrampilan'])->name('master-simpan_kompetensi_dasar_ketrampilan');
                // Route::put('update', [KompetensiIntiController::class, 'update'])->name('update-kompetensi_inti');
                Route::post('ambil_siswa', [DasarController::class, 'ambil_siswa'])->name('raport-kompetensi_dasar_ambil_siswa');
                Route::post('ambil_siswa_ketrampilan', [DasarController::class, 'ambil_siswa_ketrampilan'])->name('raport-kompetensi_dasar_ambil_siswa_ketrampilan');
                Route::post('delete', [DasarController::class, 'delete'])->name('raport-delete_kompetensi_dasar');
            });
            //admin jadwal pelajaran
            Route::get('jadwal_pelajaran', [JadwalPelajaranController::class, 'index'])->name('master-jadwal');
            Route::prefix('jadwal_pelajaran')->group(function () {
                Route::post('store', [JadwalPelajaranController::class, 'store'])->name('store-jadwal');
                Route::put('update', [JadwalPelajaranController::class, 'update'])->name('update-jadwal');
                Route::post('edit', [JadwalPelajaranController::class, 'edit'])->name('edit-jadwal');
                Route::post('get_datatable', [JadwalPelajaranController::class, 'get_datatable'])->name('datatable-jadwal');
                Route::post('trash', [JadwalPelajaranController::class, 'trash'])->name('soft_delete-jadwal');
                Route::get('data-trash', [JadwalPelajaranController::class, 'data_trash'])->name('data_trash-jadwal');
                Route::delete('hard_delete/{id}', [JadwalPelajaranController::class, 'delete'])->name('delete-jadwal');
                Route::patch('restore/{id}', [JadwalPelajaranController::class, 'restore'])->name('restore-jadwal');
                Route::post('import', [JadwalPelajaranController::class, 'import'])->name('import-jadwal');
            });
            //admin kalender
            Route::get('kalendar', [KalenderController::class, 'index'])->name('master-kalender');
            Route::prefix('kalendar')->group(function () {
                Route::post('store', [KalenderController::class, 'store'])->name('store-kalendar');
                Route::get('edit/{id}', [KalenderController::class, 'edit'])->name('edit-kalendar');
                Route::put('update', [KalenderController::class, 'update'])->name('update-kalendar');
                Route::post('edit_klik', [KalenderController::class, 'edit_klik'])->name('edit_klik-kalendar');
                Route::post('trash', [KalenderController::class, 'trash'])->name('trash-kalendar');
                Route::get('data_kalender', [KalenderController::class, 'data_kalender'])->name('data_kalender-kalendar');
            });
            //admin kalender rombel?
            Route::get('kalender_rombel', [KalenderRombelController::class, 'index'])->name('master-kalender_rombel');
            Route::prefix('kalender_rombel')->group(function () {
                Route::post('store', [KalenderRombelController::class, 'store'])->name('store-kalender_rombel');
                Route::get('edit/{id}', [KalenderRombelController::class, 'edit'])->name('edit-kalender_rombel');
                Route::put('update', [KalenderRombelController::class, 'update'])->name('update-kalender_rombel');
                Route::post('trash', [KalenderRombelController::class, 'trash'])->name('trash-kalender_rombel');
            });

            Route::get('kelas_siswa', [KelasSiswaController::class, 'index'])->name('e_learning-kelas_siswa');
            Route::prefix('kelas_siswa')->group(function () {
                Route::post('update', [KelasSiswaController::class, 'update'])->name('update-kelas_siswa');
                Route::post('get_by_rombel', [KelasSiswaController::class, 'get_by_rombel'])->name('rombel-kelas_siswa');
                Route::post('load_siswa', [KelasSiswaController::class, 'load_siswa'])->name('kelas_siswa-load_siswa');
                Route::post('get_datatable', [KelasSiswaController::class, 'get_datatable'])->name('get_datatable_rombrl-kelas_siswa');
                Route::post('import', [KelasSiswaController::class, 'import'])->name('import-kelas_siswa');
                Route::post('soft_delete', [KelasSiswaController::class, 'soft_delete'])->name('kelas_siswa-soft_delete');
                Route::post('load_kelas_siswa', [KelasSiswaController::class, 'load_kelas_siswa'])->name('load_kelas_siswa-by_rombel');
            });

            //admin guru pelajaran
            Route::get('guru_pelajaran', [GuruPelajaranController::class, 'index'])->name('e_learning-guru_pelajaran');
            Route::prefix('guru_pelajaran')->group(function () {
                Route::post('store', [GuruPelajaranController::class, 'store'])->name('store-guru_pelajaran');
                Route::post('edit', [GuruPelajaranController::class, 'edit'])->name('edit-guru_pelajaran');
                Route::put('update', [GuruPelajaranController::class, 'update'])->name('update-guru_pelajaran');
                Route::post('trash', [GuruPelajaranController::class, 'trash'])->name('trash-guru_pelajaran');
                Route::post('get_by_mapel', [GuruPelajaranController::class, 'get_by_mapel'])->name('get_by_mapel-guru_pelajaran');
                Route::post('load_mapel_select', [GuruPelajaranController::class, 'load_mapel_select'])->name('get_by_mapel-guru_pelajaran_load_select');
                Route::post('import', [GuruPelajaranController::class, 'import'])->name('import-guru_pelajaran');
            });
            //admin program template
            Route::get('program_template', [ProgramTemplateController::class, 'index'])->name('e_learning-program_sekolah');
            Route::prefix('program_template')->group(function () {
                Route::post('update', [ProgramTemplateController::class, 'update'])->name('update-template');
                Route::put('update', [ProgramTemplateController::class, 'update_info'])->name('update-program_sekolah');
                Route::post('update_program', [ProgramTemplateController::class, 'update_program'])->name('update-program_template');
                Route::post('update_template', [ProgramTemplateController::class, 'update_template'])->name('update-template_ajax');
                Route::post('edit', [ProgramTemplateController::class, 'edit'])->name('edit-program_sekolah');
            });

            Route::get('program', [ProgramController::class, 'index'])->name('admin-program');
            Route::prefix('program')->group(function () {
                Route::post('store', [ProgramController::class, 'store'])->name('admin-store_program');
                Route::post('edit', [ProgramController::class, 'edit'])->name('admin-edit_program');
                Route::post('update', [ProgramController::class, 'update'])->name('admin-update_program');
                Route::delete('trash/{id}', [ProgramController::class, 'trash'])->name('admin-trash_program');
                Route::post('update_sort', [ProgramController::class, 'update_sort'])->name('admin-update_sort_program');
                Route::post('update_status', [ProgramController::class, 'update_status'])->name('admin-update_status_program');
            });

            //kategori lomba
            Route::get('kategori-lomba', [KategoriLombaController::class, 'index'])->name('kategori_lomba-beranda');
            Route::prefix('kategori-lomba')->group(function () {
                Route::post('store', [KategoriLombaController::class, 'store'])->name('kategori_lomba-simpan');
                Route::post('update', [KategoriLombaController::class, 'update'])->name('kategori_lomba-update');
                Route::post('detail', [KategoriLombaController::class, 'detail'])->name('kategori_lomba-detail');
                Route::post('trash', [KategoriLombaController::class, 'trash'])->name('kategori_lomba-delete');
                Route::post('upload_data', [KategoriLombaController::class, 'import'])->name('kategori_lomba-import');
                Route::post('import', [KategoriLombaController::class, 'import_prestasi'])->name('prestasi_siswa-import');
                Route::get('download', [KategoriLombaController::class, 'sample'])->name('kategori_lomba-download_sample');
            });
            //Prestasi siswa
            Route::prefix('prestasi-siswa')->group(function () {
                Route::post('store', [PrestasiSiswaController::class, 'store'])->name('prestasi_siswa-simpan');
                Route::post('update', [PrestasiSiswaController::class, 'update'])->name('prestasi_siswa-update');
                Route::post('detail', [PrestasiSiswaController::class, 'detail'])->name('prestasi_siswa-detail');
                Route::post('trash', [PrestasiSiswaController::class, 'trash'])->name('prestasi_siswa-delete');
                Route::get('download', [PrestasiSiswaController::class, 'sample'])->name('prestasi_siswa-download_sample');
            });
            //Prestasi Guru
            Route::get('prestasi-guru', [PrestasiGuruController::class, 'index'])->name('prestasi_guru-beranda');
            Route::prefix('prestasi-guru')->group(function () {
                Route::post('store', [PrestasiGuruController::class, 'store'])->name('prestasi_guru-simpan');
                Route::put('update', [PrestasiGuruController::class, 'update'])->name('prestasi_guru-update');
                Route::post('detail', [PrestasiGuruController::class, 'edit'])->name('prestasi_guru-detail');
                Route::post('trash', [PrestasiGuruController::class, 'soft_delete'])->name('prestasi_guru-delete');
                Route::post('import', [PrestasiGuruController::class, 'import'])->name('prestasi_guru-import');
            });


            // Settingan Master
            Route::get('config', [MasterConfigController::class, 'index'])->name('master_config-beranda');
            Route::prefix('config')->group(function () {
                Route::post('store', [MasterConfigController::class, 'store'])->name('master_config-simpan');
            });

            Route::get('slider', [SliderController::class, 'index'])->name('master_slider-beranda');
            Route::prefix('slider')->group(function () {
                Route::post('create', [SliderController::class, 'store'])->name('master_slider-create');
                Route::post('update', [SliderController::class, 'update'])->name('master_slider-update');
                Route::post('edit', [SliderController::class, 'edit'])->name('master_slider-edit');
                Route::post('delete', [SliderController::class, 'delete'])->name('master_slider-delete');
            });

            Route::get('environment', [EnvironmentController::class, 'index'])->name('master_env-beranda');
            Route::prefix('environment')->group(function () {
                // Route::post('create', [EnvironmentController::class, 'store'])->name('master_env-create');
                Route::post('update', [EnvironmentController::class, 'update'])->name('master_env-update');
                // Route::post('edit', [EnvironmentController::class, 'edit'])->name('master_env-edit');
                // Route::post('delete', [EnvironmentController::class, 'delete'])->name('master_env-delete');
            });
        });

        Route::prefix('user')->group(function () {
            Route::get('dashboard', [AdminController::class, 'index'])->name('e_learning-user');
            ///Supervisor
            Route::get('supervisor', [SupervisorController::class, 'index'])->name('user-supervisor');
            Route::prefix('supervisor')->group(function () {
                Route::post('store', [SupervisorController::class, 'store'])->name('store-supervisor');
                Route::post('edit', [SupervisorController::class, 'edit'])->name('edit-supervisor');
                Route::post('update', [SupervisorController::class, 'update'])->name('update-supervisor');
                Route::post('trash', [SupervisorController::class, 'trash'])->name('soft_delete-supervisor');
                Route::get('data-trash', [SupervisorController::class, 'data_trash'])->name('data_trash-supervisor');
                Route::delete('hard_delete/{id}', [SupervisorController::class, 'delete'])->name('delete-supervisor');
                Route::patch('restore/{id}', [SupervisorController::class, 'restore'])->name('restore-supervisor');
                Route::post('import', [SupervisorController::class, 'import'])->name('import-supervisor');
                Route::post('reset_password', [SupervisorController::class, 'reset_password'])->name('reset_password-supervisor');
            });
            //guru
            Route::get('guru', [GuruController::class, 'index'])->name('user-guru');
            Route::prefix('guru')->group(function () {
                Route::post('store', [GuruController::class, 'store'])->name('store-guru');
                Route::post('edit', [GuruController::class, 'edit'])->name('edit-guru');
                Route::post('update', [GuruController::class, 'update'])->name('update-guru');
                Route::post('delete', [GuruController::class, 'trash'])->name('delete-guru');
                Route::post('import', [GuruController::class, 'import'])->name('import-guru');
                Route::get('data-trash', [GuruController::class, 'data_trash'])->name('data_trash-guru');
                Route::delete('hard_delete/{id}', [GuruController::class, 'delete'])->name('hard_delete-guru');
                Route::patch('restore/{id}', [GuruController::class, 'restore'])->name('restore-guru');
                Route::post('reset_password', [GuruController::class, 'reset_password'])->name('reset_password-guru');
            });
            //bk
            Route::get('bk', [BKController::class, 'index'])->name('user-bk');
            Route::prefix('bk')->group(function () {
                Route::post('store', [BKController::class, 'store'])->name('store-bk');
                Route::post('edit', [BKController::class, 'edit'])->name('edit-bk');
                Route::post('update', [BKController::class, 'update'])->name('update-bk');
                Route::delete('delete/{id}', [BKController::class, 'trash'])->name('delete-bk');
                Route::post('import', [BKController::class, 'import'])->name('import-bk');
                Route::get('data-trash', [BKController::class, 'data_trash'])->name('data_trash-bk');
                Route::delete('hard_delete/{id}', [BKController::class, 'delete'])->name('hard_delete-bk');
                Route::patch('restore/{id}', [BKController::class, 'restore'])->name('restore-bk');
                Route::post('reset_password', [BKController::class, 'reset_password'])->name('reset_password-bk');
            });
            //siswa
            Route::get('siswa', [SiswaController::class, 'index'])->name('user-siswa');
            Route::prefix('siswa')->group(function () {
                Route::post('store', [SiswaController::class, 'store'])->name('store-siswa');
                Route::get('detail', [SiswaController::class, 'edit_detail'])->name('edit_detail-siswa');
                Route::post('edit', [SiswaController::class, 'edit'])->name('edit-siswa');
                Route::post('update', [SiswaController::class, 'update'])->name('update-siswa_user');
                Route::delete('delete/{id}', [SiswaController::class, 'trash'])->name('delete-siswa');
                Route::post('import', [SiswaController::class, 'import'])->name('import-siswa');
                Route::get('data-trash', [SiswaController::class, 'data_trash'])->name('data_trash-siswa');
                Route::delete('hard_delete/{id}', [SiswaController::class, 'delete'])->name('hard_delete-siswa');
                Route::patch('restore/{id}', [SiswaController::class, 'restore'])->name('restore-siswa');
                Route::post('reset_password', [SiswaController::class, 'reset_password'])->name('reset_password-siswa');
                Route::post('res_pass', [SiswaController::class, 'res_pass'])->name('reset_password_manual-siswa');
                Route::post('delete-profile', [SiswaController::class, 'delete_profile'])->name('delete_profile-siswa');
                Route::post('upload_profile', [SiswaController::class, 'upload_profile'])->name('upload_profile-siswa');
                Route::post('delete_multiple', [SiswaController::class, 'delete_multiple'])->name('delete_multiple-siswa');
            });
            //wali kelas
            Route::get('wali-kelas', [WaliKelasController::class, 'index'])->name('user-wali_kelas');
            Route::prefix('wali-kelas')->group(function () {
                Route::post('store', [WaliKelasController::class, 'store'])->name('store-wali_kelas');
                Route::post('edit', [WaliKelasController::class, 'edit'])->name('edit-wali_kelas');
                Route::put('update', [WaliKelasController::class, 'update'])->name('update-wali_kelas');
                Route::post('delete', [WaliKelasController::class, 'trash'])->name('delete-wali_kelas');
                Route::get('data-trash', [WaliKelasController::class, 'data_trash'])->name('data_trash-wali_kelas');
                Route::delete('hard_delete/{id}', [WaliKelasController::class, 'delete'])->name('hard_delete-wali_kelas');
                Route::patch('restore/{id}', [WaliKelasController::class, 'restore'])->name('restore-wali_kelas');
                Route::post('reset_password', [WaliKelasController::class, 'reset_password'])->name('reset_password-walikelas');
            });
            //tata usaha
            Route::get('tata-usaha', [TataUsahaController::class, 'index'])->name('user-tata_usaha');
            Route::prefix('tata-usaha')->group(function () {
                Route::post('store', [TataUsahaController::class, 'store'])->name('store-tata_usaha');
                Route::post('edit', [TataUsahaController::class, 'edit'])->name('edit-tata_usaha');
                Route::post('update', [TataUsahaController::class, 'update'])->name('update-tata_usaha');
                Route::post('delete', [TataUsahaController::class, 'trash'])->name('delete-tata_usaha');
                Route::get('data-trash', [TataUsahaController::class, 'data_trash'])->name('data_trash-tata_usaha');
                Route::delete('hard_delete/{id}', [TataUsahaController::class, 'delete'])->name('hard_delete-tata_usaha');
                Route::patch('restore/{id}', [TataUsahaController::class, 'restore'])->name('restore-tata_usaha');
                Route::post('import', [TataUsahaController::class, 'import'])->name('import-tata_usaha');
                Route::post('reset_password', [TataUsahaController::class, 'reset_password'])->name('reset_password-tata_usaha');
            });
            //ortu
            Route::get('ortu', [OrtuController::class, 'index'])->name('user-orang_tua');
            Route::prefix('ortu')->group(function () {
                Route::post('store', [OrtuController::class, 'store'])->name('store-orang_tua');
                Route::post('edit', [OrtuController::class, 'edit'])->name('edit-orang_tua');
                Route::post('update', [OrtuController::class, 'update'])->name('update-orang_tua');
                Route::post('delete', [OrtuController::class, 'trash'])->name('delete-orang_tua');
                Route::post('ortu_edit', [OrtuController::class, 'ortu_edit'])->name('edit_ortu-orang_tua');
                Route::get('data-trash', [OrtuController::class, 'data_trash'])->name('data_trash-orang_tua');
                Route::delete('hard_delete/{id}', [OrtuController::class, 'delete'])->name('hard_delete-orang_tua');
                Route::patch('restore/{id}', [OrtuController::class, 'restore'])->name('restore-orang_tua');
                Route::post('import', [OrtuController::class, 'import'])->name('import-ortu');
                Route::post('reset_password', [OrtuController::class, 'reset_password'])->name('reset_password-ortu');
            });
            //alumni
            Route::get('alumni', [AlumniController::class, 'index'])->name('user-alumni');
            Route::prefix('alumni')->group(function () {
                Route::post('store', [AlumniController::class, 'store'])->name('store-alumni');
                Route::post('edit', [AlumniController::class, 'edit'])->name('edit-alumni');
                Route::get('detail', [AlumniController::class, 'detail'])->name('detail-alumni');
                Route::post('update', [AlumniController::class, 'update'])->name('update-alumni');
                Route::post('delete', [AlumniController::class, 'trash'])->name('delete-alumni');
                Route::get('data-trash', [AlumniController::class, 'data_trash'])->name('data_trash-alumni');
                Route::delete('hard_delete/{id}', [AlumniController::class, 'delete'])->name('hard_delete-alumni');
                Route::patch('restore/{id}', [AlumniController::class, 'restore'])->name('restore-alumni');
                Route::post('template', [AlumniController::class, 'template'])->name('template-alumni');
                Route::post('import', [AlumniController::class, 'import'])->name('import-alumni');
                Route::post('res_pass', [AlumniController::class, 'res_pass'])->name('reset_password_manual-alumni');
                Route::post('delete-profile', [AlumniController::class, 'delete_profile'])->name('delete_profile-alumni');
                Route::post('upload_profile', [AlumniController::class, 'upload_profile'])->name('upload_profile-alumni');
                Route::post('delete_multiple', [AlumniController::class, 'delete_multiple'])->name('delete_multiple-alumni');
            });
            //bkk
            Route::get('bkk', [BKKController::class, 'index'])->name('user-bkk');
            Route::prefix('bkk')->group(function () {
                Route::post('store', [BKKController::class, 'store'])->name('store-bkk');
                Route::post('edit', [BKKController::class, 'edit'])->name('edit-bkk');
                Route::post('update', [BKKController::class, 'update'])->name('update-bkk');
                Route::post('delete/{id}', [BKKController::class, 'trash'])->name('delete-bkk');
                Route::get('data-trash', [BKKController::class, 'data_trash'])->name('data_trash-bkk');
                Route::delete('hard_delete/{id}', [BKKController::class, 'delete'])->name('hard_delete-bkk');
                Route::patch('restore/{id}', [BKKController::class, 'restore'])->name('restore-bkk');
                Route::post('import', [BKKController::class, 'import'])->name('import-bkk');
            });
            //admin sekolah
            Route::get('admin_sekolah', [AdminSekolahController::class, 'index'])->name('e_learning-sekolah');
            Route::prefix('admin_sekolah')->group(function () {
                Route::post('update', [AdminSekolahController::class, 'update'])->name('update-admin_sekolah');
                Route::post('update_status', [AdminSekolahController::class, 'update_status'])->name('update_status-admin_sekolah');
            });

            //Kaprodi User
            Route::get('kaprodi', [KaprodiController::class, 'index'])->name('user-kaprodi');
            Route::prefix('kaprodi')->group(function () {
                Route::post('store', [KaprodiController::class, 'store'])->name('store-user_kaprodi');
                Route::get('detail', [KaprodiController::class, 'detail'])->name('detail-user_kaprodi');
                Route::post('edit', [KaprodiController::class, 'edit'])->name('edit-user_kaprodi');
                Route::post('update', [KaprodiController::class, 'update'])->name('update-user_kaprodi');
                Route::post('delete', [KaprodiController::class, 'trash'])->name('delete-user_kaprodi');
            });
        });

        Route::prefix('program')->group(function () {
            //learning admin
            Route::prefix('learning')->group(function () {
                //admin learning room
                Route::get('room', [AdminRoomController::class, 'index'])->name('admin_learning-room');
                Route::prefix('room')->group(function () {
                    Route::post('store', [AdminRoomController::class, 'store'])->name('admin_learning-store_room');
                    Route::post('edit', [AdminRoomController::class, 'edit'])->name('admin_learning-edit_room');
                    Route::post('update', [AdminRoomController::class, 'update'])->name('admin_learning-update_room');
                    Route::post('delete', [AdminRoomController::class, 'trash'])->name('admin_learning-delete');
                    Route::post('updateNonaktif', [AdminRoomController::class, 'updateNonAktif'])->name('admin_room-update_nonAktif');
                    Route::post('get_room', [AdminRoomController::class, 'get_by_kelas'])->name('admin_room-get_by_kelas');
                    Route::post('update_status', [AdminRoomController::class, 'update_status'])->name('admin_room-update_status');
                });


                Route::get('admin_learning', [AdminLearningController::class, 'index'])->name('learning_admin-beranda');
                Route::prefix('admin_learning')->group(function () {
                    Route::get('detail', [AdminLearningController::class, 'detail'])->name('learning_admin-detail');
                    Route::post('edit', [AdminLearningController::class, 'edit'])->name('learning_admin-edit');
                    Route::post('update', [AdminLearningController::class, 'update'])->name('learning_admin-update');
                    Route::post('save', [AdminLearningController::class, 'store'])->name('learning_admin-save');
                    Route::post('soft_delete', [AdminLearningController::class, 'trash'])->name('learning_admin-soft_delete');
                    Route::post('reset_password', [AdminLearningController::class, 'reset_password'])->name('reset_password-admin_learning');
                });

                //admin learning setting
                Route::get('setting', [SettingController::class, 'index'])->name('admin_learning-setting');
                Route::prefix('setting')->group(function () {
                    Route::post('store', [SettingController::class, 'store'])->name('admin_learning-update_setting');
                });

                Route::get('login', [LearningController::class, 'index'])->name('admin_learning-login');
                Route::prefix('login')->group(function () {
                    Route::post('store', [SettingController::class, 'store'])->name('admin_learning-update_setting');
                });
            });
            //raport admin
            Route::prefix('raport')->group(function () {
                Route::get('sikap_sosial', [SikapSosialController::class, 'index'])->name('admin_raport-sikap_sosial');
                Route::prefix('sikap_sosial')->group(function () {
                    Route::post('store', [SikapSosialController::class, 'store'])->name('admin_raport-store_sikap_sosial');
                    Route::post('edit', [SikapSosialController::class, 'edit'])->name('admin_raport-edit_sikap_sosial');
                    Route::put('update', [SikapSosialController::class, 'update'])->name('admin_raport-update_sikap_sosial');
                    Route::delete('trash/{id}', [SikapSosialController::class, 'trash'])->name('admin_raport-delete_sikap_sosial');
                });

                Route::get('sikap_spiritual', [SikapSpiritualController::class, 'index'])->name('admin_raport-sikap_spiritual');
                Route::prefix('sikap_spiritual')->group(function () {
                    Route::post('store', [SikapSpiritualController::class, 'store'])->name('admin_raport-store_sikap_spiritual');
                    Route::post('edit', [SikapSpiritualController::class, 'edit'])->name('admin_raport-edit_sikap_spiritual');
                    Route::put('update', [SikapSpiritualController::class, 'update'])->name('admin_raport-update_sikap_spiritual');
                    Route::delete('trash/{id}', [SikapSpiritualController::class, 'trash'])->name('admin_raport-delete_sikap_spiritual');
                });

                Route::get('raport_predikat', [PredikatController::class, 'index'])->name('raport-predikat');
                Route::prefix('raport_predikat')->group(function () {
                    Route::post('create', [PredikatController::class, 'store'])->name('raport-store_predikat');
                    Route::put('update', [PredikatController::class, 'update'])->name('raport-update_predikat');
                    Route::post('edit', [PredikatController::class, 'edit'])->name('raport-edit_predikat');
                    Route::post('trash', [PredikatController::class, 'trash'])->name('raport-trash_predikat');
                    Route::post('import', [PredikatController::class, 'import'])->name('import-nilai_predikat');
                });

                Route::get('raport_set_nilai', [NilaiConfigController::class, 'index'])->name('raport-setting_nilai');
                Route::prefix('raport_set_nilai')->group(function () {
                    Route::post('create', [NilaiConfigController::class, 'store'])->name('raport-store_setting_nilai');
                    Route::put('update', [NilaiConfigController::class, 'update'])->name('raport-update_setting_nilai');
                    Route::post('edit', [NilaiConfigController::class, 'edit'])->name('raport-edit_setting_nilai');
                    Route::post('delete', [NilaiConfigController::class, 'trash'])->name('raport-trash_setting_nilai');
                });

                Route::get('template_catatan', [TemplateCatatanController::class, 'index'])->name('raport-template_catatan');
                Route::prefix('template_catatan')->group(function () {
                    Route::post('simpan', [TemplateCatatanController::class, 'store'])->name('raport-save_template_catatan');
                    Route::put('update', [TemplateCatatanController::class, 'update'])->name('raport-update_template_catatan');
                    Route::post('edit', [TemplateCatatanController::class, 'edit'])->name('raport-edit_template_catatan');
                    Route::delete('trash/{id}', [TemplateCatatanController::class, 'trash'])->name('raport-delete_template_catatan');
                });




                Route::get('config', [ConfigController::class, 'index'])->name('raport-config');
                Route::prefix('config')->group(function () {
                    Route::post('simpan', [ConfigController::class, 'store'])->name('raport-save_config');
                    Route::post('update', [ConfigController::class, 'update'])->name('raport-update_config');
                    Route::post('edit', [ConfigController::class, 'edit'])->name('raport-edit_config');
                    Route::post('trash', [ConfigController::class, 'trash'])->name('raport-delete_config');
                });

                Route::get('template_sampul', [TemplateSampulController::class, 'index'])->name('raport-template_sampul');
                Route::prefix('template_sampul')->group(function () {
                    Route::post('simpan', [TemplateSampulController::class, 'store'])->name('raport-save_template_sampul');
                    Route::post('update', [TemplateSampulController::class, 'update'])->name('raport-update_template_sampul');
                    Route::post('edit', [TemplateSampulController::class, 'edit'])->name('raport-edit_template_sampul');
                    Route::delete('trash/{id}', [TemplateSampulController::class, 'trash'])->name('raport-delete_template_sampul');
                });

                Route::get('kop_sampul', [KopSampulController::class, 'index'])->name('raport-kop_sampul');
                Route::prefix('kop_sampul')->group(function () {
                    Route::post('simpan', [KopSampulController::class, 'store'])->name('raport-save_kop_sampul');
                    Route::post('update', [KopSampulController::class, 'update'])->name('raport-update_kop_sampul');
                    Route::post('edit', [KopSampulController::class, 'edit'])->name('raport-edit_kop_sampul');
                    Route::delete('trash/{id}', [KopSampulController::class, 'trash'])->name('raport-delete_kop_sampul');
                    Route::get('data-trash', [KopSampulController::class, 'data_trash'])->name('data_trash-kop_sampul');
                    Route::delete('hard_delete/{id}', [KopSampulController::class, 'delete'])->name('delete-kop_sampul');
                    Route::patch('restore/{id}', [KopSampulController::class, 'restore'])->name('restore-kop_sampul');
                });

                Route::get('transkip', [TranskipController::class, 'index'])->name('raport-transkip');
                Route::prefix('transkip')->group(function () {
                    Route::post('siswa', [TranskipController::class, 'get_siswa'])->name('raport_transkip-get_siswa');
                    Route::post('simpan', [TranskipController::class, 'store'])->name('raport_transkip-save');
                    Route::post('update', [TranskipController::class, 'update'])->name('raport_transkip-update');
                    Route::post('edit', [TranskipController::class, 'edit'])->name('raport_transkip-detail');
                    Route::post('trash', [TranskipController::class, 'delete'])->name('raport_transkip-delete');
                    Route::get('download/{id}', [TranskipController::class, 'download'])->name('raport_transkip-download');
                    // Route::delete('hard_delete/{id}', [KopSampulController::class, 'delete'])->name('delete-kop_sampul');
                    // Route::patch('restore/{id}', [KopSampulController::class, 'restore'])->name('restore-kop_sampul');
                });


                Route::get('admin_raport', [RaportAdminController::class, 'index'])->name('admin_raport-beranda');
                Route::prefix('admin_raport')->group(function () {
                    Route::post('store', [RaportAdminController::class, 'store'])->name('admin_raport-store');
                    Route::post('edit', [RaportAdminController::class, 'edit'])->name('admin_raport-edit');
                    Route::post('update', [RaportAdminController::class, 'update'])->name('admin_raport-update');
                    Route::post('delete', [RaportAdminController::class, 'delete'])->name('admin_raport-delete');
                    Route::post('reset_password', [RaportAdminController::class, 'reset_password'])->name('reset_password-admin_raport');
                });
            });

            //Prakerin admin
            Route::prefix('prakerin')->group(function () {
                Route::get('admin_prakerin', [PrakerinAdminController::class, 'index'])->name('admin_prakerin-beranda');
                Route::prefix('admin_prakerin')->group(function () {
                    Route::post('store', [PrakerinAdminController::class, 'store'])->name('admin_prakerin-store');
                    Route::post('edit', [PrakerinAdminController::class, 'edit'])->name('admin_prakerin-edit');
                    Route::post('update', [PrakerinAdminController::class, 'update'])->name('admin_prakerin-update');
                    Route::delete('delete/{id}', [PrakerinAdminController::class, 'delete'])->name('admin_prakerin-delete');
                    Route::post('reset_password', [PrakerinAdminController::class, 'reset_password'])->name('reset_password-admin_prakerin');
                });
            });
        });

        //Prakerin Buku Induk
        Route::prefix('induk')->group(function () {
            Route::get('admin_induk', [IndukAdminController::class, 'index'])->name('admin_induk-beranda');
            Route::prefix('admin_induk')->group(function () {
                Route::post('store', [IndukAdminController::class, 'store'])->name('admin_induk-store');
                Route::post('edit', [IndukAdminController::class, 'edit'])->name('admin_induk-edit');
                Route::post('update', [IndukAdminController::class, 'update'])->name('admin_induk-update');
                Route::post('delete', [IndukAdminController::class, 'delete'])->name('admin_induk-delete');
                Route::post('reset_password', [IndukAdminController::class, 'reset_password'])->name('reset_password-admin_induk');
            });
            Route::get('siswa', [IndukSiswaController::class, 'index'])->name('siswa_induk-beranda');
            Route::get('guru', [IndukGuruController::class, 'index'])->name('guru_induk-beranda');
            Route::get('tata_usaha', [IndukTataUsahaController::class, 'index'])->name('tata_usaha_induk-beranda');
        });
        // Program Absensi
        Route::prefix('absensi')->group(function () {
            Route::get('izin', [IzinController::class, 'index'])->name('absensi-izin');
            Route::prefix('izin')->group(function () {
            });

            Route::get('menu', [MenuController::class, 'index'])->name('absensi-menu');
            Route::prefix('menu')->group(function () {
                Route::get('akses', [MenuController::class, 'index'])->name('absensi_menu-akses');
                Route::get('submenu', [MenuController::class, 'submenu'])->name('admin_menu-submenu');
            });

            Route::get('absen', [AbsenController::class, 'index'])->name('absensi_absen');
            Route::prefix('absen')->group(function () {
                Route::get('libur', [AbsenController::class, 'libur'])->name('absensi_absen-libur');
            });
        });
    });



    // //super admin
    Route::prefix('super_admin')->group(function () {
        Route::prefix('master')->group(function () {
            //         //kategori program
            //         Route::get('kategori_program', [KategoriProgramController::class, 'index'])->name('super_admin-kategori_program');
            //         Route::prefix('kategori_program')->group(function () {
            //             Route::post('store', [KategoriProgramController::class, 'store'])->name('super_admin-store_kategori_program');
            //             Route::post('edit', [KategoriProgramController::class, 'edit'])->name('super_admin-edit_kategori_program');
            //             Route::put('update', [KategoriProgramController::class, 'update'])->name('super_admin-update_kategori_program');
            //             Route::delete('trash/{id}', [KategoriProgramController::class, 'trash'])->name('super_admin-trash_kategori_program');
            //         });

            //         //program
            //         Route::get('program', [ProgramController::class, 'index'])->name('super_admin-program');
            //         Route::prefix('program')->group(function () {
            //             Route::post('store', [ProgramController::class, 'store'])->name('super_admin-store_program');
            //             Route::post('edit', [ProgramController::class, 'edit'])->name('super_admin-edit_program');
            //             Route::post('update', [ProgramController::class, 'update'])->name('super_admin-update_program');
            //             Route::delete('trash/{id}', [ProgramController::class, 'trash'])->name('super_admin-trash_program');
            //         });

            //         //template
            //         Route::get('template', [TemplateController::class, 'index'])->name('super_admin-template');
            //         Route::prefix('template')->group(function () {
            //             Route::post('edit', [TemplateController::class, 'edit'])->name('super_admin-edit_template');
            //             Route::post('update', [TemplateController::class, 'update'])->name('super_admin-update_template');
            //             Route::delete('trash/{id}', [TemplateController::class, 'trash'])->name('super_admin-trash_template');
            //         });

            //         //indonesia
            Route::prefix('indonesia')->group(function () {
                Route::post('kabupaten', [IndonesiaController::class, 'get_kabupaten'])->name('get_kabupaten-provinsi');
                Route::get('kecamatan/{id_kabuapten}', [IndonesiaController::class, 'get_kecamatan'])->name('get_kecamatan-kabupaten');
                Route::get('desa/{id_kecamatan}', [IndonesiaController::class, 'get_desa'])->name('get_desa-kecamatan');
                Route::get('detail/desa/{id_desa}', [IndonesiaController::class, 'get_detail_desa'])->name('get_detail_desa');
                Route::post('edit_kabupaten', [IndonesiaController::class, 'edit_kab'])->name('get-edit_kabupaten');
                Route::post('edit_kecamatan', [IndonesiaController::class, 'edit_kec'])->name('get-edit_kecamatan');
                Route::post('edit_desa', [IndonesiaController::class, 'edit_des'])->name('get-edit_desa');
            });

            //         //sekolah
            //         Route::get('sekolah', [SekolahController::class, 'index'])->name('supermaster_sekolah');
            //         Route::prefix('sekolah')->group(function () {
            //             Route::post('store', [SekolahController::class, 'store'])->name('store-sekolah');
            //             Route::post('filter', [SekolahController::class, 'filter'])->name('filter-sekolah');
            //             Route::get('edit/{id}', [SekolahController::class, 'edit'])->name('edit-sekolah');
            //             Route::post('update', [SekolahController::class, 'update'])->name('update-sekolah');
            //             Route::post('update_status', [SekolahController::class, 'update_status'])->name('update_status-sekolah');
            //             Route::delete('delete/{id}', [SekolahController::class, 'delete'])->name('delete-sekolah');
            //         });

            //         Route::get('sekolah/{id}', [SuperMasterDashboardController::class, 'index'])->name('supermaster-menu_sekolah');
            //         Route::prefix('sekolah/{id}')->group(function () {
            //             Route::get('admin', [AdminController::class, 'admin'])->name('superadmin-admin_sekolah');
            //             Route::prefix('admin')->group(function () {
            //                 Route::post('store', [AdminController::class, 'store'])->name('supermaster-store_admin_sekolah');
            //                 Route::post('edit', [AdminController::class, 'edit'])->name('supermaster-edit_admin_sekolah');
            //                 Route::post('update', [AdminController::class, 'update'])->name('supermaster-update_admin_sekolah');
            //                 Route::post('delete', [AdminController::class, 'delete'])->name('supermaster-delete_admin_sekolah');
            //                 Route::post('reset_password', [AdminController::class, 'reset_password'])->name('reset_password-admin');
            //             });
            //         });

            //         Route::get('template_sertifikat', [TemplateSertifikatController::class, 'index'])->name('supermaster-template_sertifikat');
            //         Route::prefix('template_sertifikat')->group(function () {
            //             Route::post('store', [TemplateSertifikatController::class, 'store'])->name('supermaster-store_template_sertifikat');
            //             Route::post('edit', [TemplateSertifikatController::class, 'edit'])->name('supermaster-edit_template_sertifikat');
            //             Route::post('update', [TemplateSertifikatController::class, 'update'])->name('supermaster-update_template_sertifikat');
            //             Route::get('export', [TemplateSertifikatController::class, 'export'])->name('supermaster-export_template_sertifikat');
            //             Route::get('download', [TemplateSertifikatController::class, 'download'])->name('supermaster-download_template_sertifikat');
            //             Route::delete('delete/{id}', [TemplateSertifikatController::class, 'delete'])->name('supermaster-delete_template_sertifikat');
            //         });

            //         Route::get('backup', [BackupController::class, 'index'])->name('supermaster-backup_database');
            //         Route::prefix('backup')->group(function () {
            //             Route::get('create', [BackupController::class, 'create'])->name('supermaster-create_backup_database');
            //             Route::delete('delete/{id}', [BackupController::class, 'delete'])->name('supermaster-delete_backup_database');
            //         });

            //         Route::get('superadmin', [SupermasterController::class, 'index'])->name('supermaster-superadmin');
            //         Route::prefix('superadmin')->group(function () {
            //             Route::post('create', [SupermasterController::class, 'store'])->name('supermaster-store_superadmin');
            //             Route::post('update', [SupermasterController::class, 'update'])->name('supermaster-update_superadmin');
            //             Route::post('edit', [SupermasterController::class, 'edit'])->name('supermaster-edit_superadmin');
            //             Route::delete('delete/{id}', [SupermasterController::class, 'delete'])->name('supermaster-delete_backup_database');
            //         });

            //         Route::prefix('ajax')->group(function () {
            //             Route::post('load_admin', [SuperMasterAjaxController::class, 'load_admin'])->name('supermaster-load_admin');
            //             Route::post('load_program', [SuperMasterAjaxController::class, 'load_program'])->name('supermaster-load_program');
            //         });
        });
    });


    Route::post('dashboard/jadwal', 'Master\DashboardController@get_jadwal')->name('dashboard-get_jadwal');
    Route::post('dashboard/jadwal/hariini', 'Master\DashboardController@get_jadwal_hari_ini')->name('dashboard-get_jadwal_hari_ini');
    Route::post('dashboard/jadwal/guru', 'Master\DashboardController@get_jadwal_guru')->name('dashboard-get_jadwal_guru');
    Route::post('dashboard/jadwal/guru/hariini', 'Master\DashboardController@get_jadwal_guru_hari_ini')->name('dashboard-get_jadwal_guru_hari_ini');
    Route::post('dashboard/rombel', 'Master\DashboardController@siswa_rombel')->name('dashboard-get_rombel');
    Route::get('dashboard/count/siswa', 'Master\DashboardController@count_siswa')->name('dashboard-count_siswa');
    Route::get('admin', 'Master\DashboardController@admin')->name('admin');

    //masuk program
    Route::get('program/{slug}', 'Master\DashboardController@index')->name('program');
    Route::prefix('program/{slug}')->group(function () {
        //profile
        Route::get('profile', [ProfileController::class, 'index'])->name('profile');
        Route::prefix('profile')->group(function () {
            Route::get('edit', [ProfileController::class, 'edit'])->name('edit-profile');
            Route::get('preview', [ProfileController::class, 'preview'])->name('profile-preview');
            Route::get('v_change-password', [ProfileController::class, 'v_change_password'])->name('view_change_password-user');
            Route::get('reset_password', [ProfileController::class, 'reset_password'])->name('reset_password-user');
            Route::get('upload_paraf', [ProfileController::class, 'upload_paraf'])->name('upload_paraf-user');
            Route::post('update_paraf', [ProfileController::class, 'update_paraf'])->name('update_paraf-user');
            Route::post('change-password', [ProfileController::class, 'change_password'])->name('change_password-profile');
            Route::post('update', [ProfileController::class, 'update'])->name('update-profile');
            Route::post('update_image', [ProfileController::class, 'update_image'])->name('update-profile_image');
            Route::get('update/pelamar', [ProfileController::class, 'edit_pelamar'])->name('update-profile_pelamar');
        });
        //pesan
        Route::get('pesan', [PesanController::class, 'index'])->name('pesan');
        Route::prefix('pesan')->group(function () {
            Route::post('get_create', [PesanController::class, 'get_create'])->name('create-pesan');
            Route::post('create_chat', [PesanController::class, 'createChat'])->name('create_chat-pesan');
            Route::get('sent', [PesanController::class, 'sent'])->name('send-pesan');
            Route::get('create/{email}', [PesanController::class, 'to_email'])->name('send-pesan');
            Route::post('save_token', [PesanController::class, 'save_fcm'])->name('save-token');
            Route::post('new_create', [PesanController::class, 'new_crate'])->name('new_create-message');
            Route::get('all/email', [PesanController::class, 'all_email'])->name('all_email-message');
        });
        //kalender
        Route::get('kalendar', [KalenderController::class, 'index'])->name('master-kalender_program');
    });

    Route::prefix('program')->group(function () {
        //learning
        Route::prefix('e_learning')->group(function () {
            Route::post('session_template', [DashboardController::class, 'session_template'])->name('learning-session_template');
            Route::get('rombel', [MapelKelasController::class, 'index'])->name('room-rombel');
            Route::get('rombel/{id}', [MapelKelasController::class, 'rombel'])->name('multiple_rombel');
            Route::post('get_pertemuan', [MapelKelasController::class, 'get_pertemuan_by_room'])->name('get_pertemuan_by_room');
            Route::get('detail/{id}', [RoomController::class, 'detail'])->name('room-detail');
            Route::post('update', [RoomController::class, 'update_room'])->name('room-update');
            Route::get('kalendar', [KalenderController::class, 'index'])->name('e-learning-kalender');
            Route::get('kalendar_rombel', [KalenderRombelController::class, 'index'])->name('e-learning-kalender_rombel');
            Route::post('kalrom_supervisor', [KalenderRombelController::class, 'kalender_supervisor'])->name('e-learning_supervisor-kalender_rombel');
            Route::get('room/{id_room}', [RoomController::class, 'index'])->name('room-dashboard');
            Route::post('update_room', [RoomController::class, 'update_virtual'])->name('room-update_virtual');
            Route::post('detail', [RoomController::class, 'detailRoom'])->name('room-get_by_id_room');
            Route::get('feeds_rombel', [FeedsRombelController::class, 'index'])->name('feed_rombel-beranda');
            Route::post('absensi', [DataSiswaController::class, 'update_status'])->name('update_absensi-data_siswa');
            Route::post('data_siswa', [DataSiswaController::class, 'data_table_detail'])->name('get_datatable_data_siswa');
            Route::prefix('feeds_rombel')->group(function () {
                Route::post('detail', [FeedsRombelController::class, 'detail'])->name('feed_rombel-detail');
                // Route::post('update', [RoomController::class, 'update'])->name('room-update_post');
                // Route::post('soft_delete', [RoomController::class, 'soft_delete'])->name('room-soft_delete');
                // Route::post('delete_comment', [RoomController::class, 'delete_comment'])->name('room-delete_comment');
                Route::post('store', [FeedsRombelController::class, 'store'])->name('feed_rombel-store');
                Route::post('store_comment', [FeedsRombelController::class, 'store_comment'])->name('feed_rombel-store_comment');
                // Route::post('store_comment', [RoomController::class, 'comment'])->name('room-store_comment');
                // Route::post('comment_update', [RoomController::class, 'comment_update'])->name('room-update_comment');
            });

            //learning room
            Route::prefix('room/{id_room}')->group(function () {
                Route::post('dashboard', [RoomController::class, 'guru'])->name('room-dashboard_guru');
                Route::post('absensi_siswa', [RoomController::class, 'absensi'])->name('room-absensi_siswa');
                Route::get('logout/{id}', [RoomController::class, 'logout_session'])->name('room-logout');
                Route::prefix('postingan')->group(function () {
                    Route::get('detail/{id}', [RoomController::class, 'detail_post'])->name('room-detail_post');
                    Route::post('update', [RoomController::class, 'update'])->name('room-update_post');
                    Route::post('soft_delete', [RoomController::class, 'soft_delete'])->name('room-soft_delete');
                    Route::post('delete_comment', [RoomController::class, 'delete_comment'])->name('room-delete_comment');
                    Route::post('store', [RoomController::class, 'store'])->name('room-store_post');
                    Route::post('store_comment', [RoomController::class, 'comment'])->name('room-store_comment');
                    Route::post('comment_update', [RoomController::class, 'comment_update'])->name('room-update_comment');
                });

                //learning standar kompetensi
                Route::get('standar_kompetensi', [KompetensiDasarController::class, 'index'])->name('room-standar_kompetensi');
                Route::prefix('standar_kompetensi')->group(function () {
                    Route::post('store', [KompetensiDasarController::class, 'store'])->name('store-standar_kompetensi');
                    Route::post('edit', [KompetensiDasarController::class, 'edit'])->name('edit-standar_kompetensi');
                    Route::post('edit_kd', [KompetensiDasarController::class, 'edit_kd'])->name('edit-kd');
                    Route::post('load', [KompetensiDasarController::class, 'load'])->name('edit-standar_kompetensi');
                    Route::post('kompetensi_dasar', [KompetensiDasarController::class, 'kompetensi_dasar'])->name('edit-kompetensi_dasar');
                    Route::put('update', [KompetensiDasarController::class, 'update'])->name('update-standar_kompetensi');
                    Route::post('trash', [KompetensiDasarController::class, 'trash'])->name('trash-standar_kompetensi');
                    Route::post('trash_kd', [KompetensiDasarController::class, 'trash_kd'])->name('trash-kd');
                    Route::get('data-trash', [KompetensiDasarController::class, 'data_trash'])->name('data_trash-standar_kompetensi');
                    Route::delete('hard_delete/{id}', [KompetensiDasarController::class, 'delete'])->name('delete-standar_kompetensi');
                    Route::patch('restore/{id}', [KompetensiDasarController::class, 'restore'])->name('restore-standar_kompetensi');
                    Route::post('get_inti', [KompetensiDasarController::class, 'get_inti_checkbox'])->name('restore-standar_kompetensi');
                });

                //learning bahan ajar
                Route::get('bahan_ajar', [BahanAjarController::class, 'index'])->name('room-bahan_ajar');
                Route::prefix('bahan_ajar')->group(function () {
                    Route::post('store', [BahanAjarController::class, 'store'])->name('store-bahan_ajar');
                    Route::post('edit', [BahanAjarController::class, 'edit'])->name('edit-bahan_ajar');
                    Route::post('update', [BahanAjarController::class, 'update'])->name('update-bahan_ajar');
                    Route::post('trash', [BahanAjarController::class, 'softdelete'])->name('soft_delete-bahan_ajar');
                    Route::get('download/{id}', [BahanAjarController::class, 'download'])->name('download-bahan_ajar');
                });

                //learning data siswa
                Route::get('data_siswa', [DataSiswaController::class, 'index'])->name('room-data_siswa');
                Route::prefix('data_siswa')->group(function () {
                    Route::post('store', [DataSiswaController::class, 'store'])->name('store-data_siswa');

                    Route::get('rombel', [DataSiswaController::class, 'rombel'])->name('data_siswa-rombel');
                    Route::get('detail/{id}', [DataSiswaController::class, 'detail'])->name('data_siswa-detail');
                    Route::get('data_siswa_rombel', [DataSiswaController::class, 'data_siswa_rombel'])->name('data_siswa_rombel');
                    Route::get('edit/{id}', [DataSiswaController::class, 'edit'])->name('edit-data_siswa');
                    Route::get('siswa/{id}', [DataSiswaController::class, 'get_by_id'])->name('show-data_siswa');
                    Route::put('update', [DataSiswaController::class, 'update'])->name('update-data_siswa');
                    Route::post('store_siswa', [DataSiswaController::class, 'store_siswa'])->name('store-data_siswa');
                });

                //learning absensi
                Route::get('absensi', [AbsensiController::class, 'index'])->name('room-absensi');
                Route::prefix('absensi')->group(function () {
                    Route::post('edit', [AbsensiController::class, 'edit'])->name('edit-absensi');
                    Route::put('update', [AbsensiController::class, 'update'])->name('update-absensi');
                    Route::post('filter_date', [AbsensiController::class, 'filter_date'])->name('filter_tanggal-siswa');
                });


                //learning jurnal guru
                Route::get('jurnal_guru', [JurnalController::class, 'index'])->name('room-jurnal_guru');
                Route::prefix('jurnal_guru')->group(function () {
                    Route::post('store', [JurnalController::class, 'store'])->name('store-jurnal');
                    Route::post('edit', [JurnalController::class, 'edit'])->name('edit-jurnal');
                    Route::put('update', [JurnalController::class, 'update'])->name('update-jurnal');
                    Route::post('kompetensi', [JurnalController::class, 'get_by_id'])->name('room-get_jurnal_by_kompetensi');
                    Route::post('trash', [JurnalController::class, 'trash'])->name('trash-jurnal');
                    Route::get('data-trash', [JurnalController::class, 'data_trash'])->name('data_trash-jurnal');
                    Route::delete('hard_delete/{id}', [JurnalController::class, 'delete'])->name('delete-jurnal');
                    Route::patch('restore/{id}', [JurnalController::class, 'restore'])->name('restore-jurnal');
                });


                //learning tugas pengetahuan
                Route::get('tugas_pengetahuan', [TugasPengetahuanController::class, 'index'])->name('room-tugas_pengetahuan');
                Route::prefix('tugas_pengetahuan')->group(function () {
                    Route::post('store', [TugasPengetahuanController::class, 'store'])->name('store-tugas_pengetahuan');
                    Route::post('edit', [TugasPengetahuanController::class, 'edit'])->name('edit-tugas_pengetahuan');
                    Route::post('update', [TugasPengetahuanController::class, 'update'])->name('update-tugas_pengetahuan');
                    Route::get('download/{id}', [TugasPengetahuanController::class, 'download'])->name('download-tugas_pengetahuan');
                    Route::post('hasil', [TugasPengetahuanController::class, 'hasil'])->name('hasil-tugas_pengetahuan');
                    Route::get('kompetensi/{id}', [JurnalController::class, 'get_by_id'])->name('room-get_jurnal_by_kompetensi');
                    Route::post('trash', [TugasPengetahuanController::class, 'trash'])->name('trash-tugas_pengetahuan');
                    Route::get('data-trash', [TugasPengetahuanController::class, 'data_trash'])->name('data_trash-tugas_pengetahuan');
                    Route::delete('hard_delete/{id}', [TugasPengetahuanController::class, 'delete'])->name('delete-tugas_pengetahuan');
                    Route::patch('restore/{id}', [TugasPengetahuanController::class, 'restore'])->name('restore-tugas_pengetahuan');
                });


                //learning jawaban pengetahuan
                Route::get('jawaban_pengetahuan', [JawabanPengetahuanController::class, 'index'])->name('room-jawaban_pengetahuan');
                Route::prefix('jawaban_pengetahuan')->group(function () {
                    Route::post('get_by_tugas', [JawabanPengetahuanController::class, 'get_by_tugas'])->name('get_by_tugas-jawaban_pengetahuan');
                    Route::post('store', [JawabanPengetahuanController::class, 'store'])->name('store-nilai_jawaban_pengetahuan');
                    Route::post('tugas_siswa', [JawabanPengetahuanController::class, 'get_by_jawaban_siswa'])->name('get_by_tugas_pengetahuan-jawaban_siswa');
                    // Route::post('tugas_siswa', [JawabanKetrampilanController::class, 'get_by_jawaban_siswa'])->name('get_by_tugas-jawaban_siswa');
                    Route::post('edit', [JawabanPengetahuanController::class, 'edit'])->name('edit-jawaban_pengetahuan');
                    Route::post('update', [JawabanPengetahuanController::class, 'update'])->name('update-nilai_jawaban_pengetahuan');
                    Route::post('trash', [JawabanPengetahuanController::class, 'trash'])->name('trash-nilai_jawaban_pengetahuan');
                    Route::get('data-trash', [JawabanPengetahuanController::class, 'data_trash'])->name('data_trash-nilai_jawaban_pengetahuan');
                    Route::delete('hard_delete/{id}', [JawabanPengetahuanController::class, 'delete'])->name('delete-nilai_jawaban_pengetahuan');
                    Route::get('download/{id}', [JawabanPengetahuanController::class, 'download'])->name('download-hasil_pengetahuan');
                    Route::post('save_nilai', [JawabanPengetahuanController::class, 'save_nilai'])->name('jawaban_simpan_nilai');
                });

                //learning tugas ketrampilan
                Route::get('tugas_ketrampilan', [TugasKetrampilanController::class, 'index'])->name('room-tugas_ketrampilan');
                Route::prefix('tugas_ketrampilan')->group(function () {
                    Route::post('store', [TugasKetrampilanController::class, 'store'])->name('store-tugas_ketrampilan');
                    Route::post('edit', [TugasKetrampilanController::class, 'edit'])->name('edit-tugas_ketrampilan');
                    Route::post('update', [TugasKetrampilanController::class, 'update'])->name('update-tugas_ketrampilan');
                    Route::get('kompetensi/{id}', [TugasKetrampilanController::class, 'get_by_id'])->name('room-get_jurnal_by_kompetensi');
                    Route::get('download/{id}', [TugasKetrampilanController::class, 'download'])->name('download-nilai_ketrampilan');
                    Route::post('trash', [TugasKetrampilanController::class, 'trash'])->name('trash-tugas_ketrampilan');
                    Route::get('data-trash', [TugasKetrampilanController::class, 'data_trash'])->name('data_trash-tugas_ketrampilan');
                    Route::delete('hard_delete/{id}', [TugasKetrampilanController::class, 'delete'])->name('delete-tugas_ketrampilan');
                    Route::patch('restore/{id}', [TugasKetrampilanController::class, 'restore'])->name('restore-tugas_ketrampilan');
                });

                //learning jawaban ketrampilan
                Route::get('jawaban_ketrampilan', [JawabanKetrampilanController::class, 'index'])->name('room-jawaban_ketrampilan');
                Route::prefix('jawaban_ketrampilan')->group(function () {
                    Route::get('download/{id}', [JawabanKetrampilanController::class, 'download'])->name('download-hasil_ketrampilan');
                    Route::post('get_by_tugas', [JawabanKetrampilanController::class, 'get_by_tugas'])->name('get_by_tugas-jawaban_ketrampilan');
                    Route::post('tugas_siswa', [JawabanKetrampilanController::class, 'get_by_jawaban_siswa'])->name('get_by_tugas-jawaban_siswa');
                    Route::post('store', [JawabanKetrampilanController::class, 'store'])->name('store-jawaban_ketrampilan');
                    Route::post('edit', [JawabanKetrampilanController::class, 'edit'])->name('edit-jawaban_ketrampilan');
                    Route::post('update', [JawabanKetrampilanController::class, 'update'])->name('update-jawaban_ketrampilan');
                    Route::post('trash', [JawabanKetrampilanController::class, 'trash'])->name('trash-jawaban_ketrampilan');
                    Route::post('save_nilai', [JawabanKetrampilanController::class, 'save_nilai'])->name('jawaban_ketrampilan_simpan_nilai');
                });

                //learning kalender kelas
                Route::get('kalendar_kelas', [KalenderKelasController::class, 'index'])->name('room-kalender_kelas');
                Route::prefix('kalendar_kelas')->group(function () {
                    Route::post('store', [KalenderKelasController::class, 'store'])->name('store-kalendar_kelas');
                    Route::post('edit', [KalenderKelasController::class, 'edit'])->name('edit-kalendar_kelas');
                    Route::put('update', [KalenderKelasController::class, 'update'])->name('update-kalendar_kelas');
                    Route::post('trash', [KalenderKelasController::class, 'trash'])->name('trash-kalendar_kelas');
                });

                Route::get('pengaturan', [SettingController::class, 'index'])->name('room-pengaturan');
            });

            //learning login wali kelas
            Route::prefix('wali_kelas')->group(function () {
                //feeds
                Route::prefix('feeds')->group(function () {
                    Route::post('store', [MapelKelasController::class, 'store'])->name('walikelas_store-feeds');
                    Route::get('detail/{id}', [MapelKelasController::class, 'detail_post'])->name('wali_kelas-detail_post');
                    Route::post('store_comment', [MapelKelasController::class, 'comment'])->name('wali_kelas-store_comment');
                });

                //guru mapel
                Route::get('guru_mapel', [MapelWaliKelasController::class, 'index'])->name('walikelas-guru_mapel');
                Route::prefix('guru_mapel')->group(function () {
                    Route::post('detail', [MapelWaliKelasController::class, 'detail'])->name('walikelas_detail-guru_mapel');
                    Route::post('jadwal', [MapelWaliKelasController::class, 'jadwal_datatable'])->name('walikelas-datatable_jadwal');
                });


                //mapel
                Route::get('mapel', [MapelWaliKelasController::class, 'mapel'])->name('walikelas-mapel');
                Route::prefix('mapel')->group(function () {
                    Route::post('store', [MapelWaliKelasController::class, 'store_mapel'])->name('wali_kelas-store_mapel');
                    Route::get('edit/{id}', [MapelWaliKelasController::class, 'mapel_edit'])->name('wali_kelas-edit_mapel_kelas');
                });

                //data siswa
                Route::prefix('data_siswa')->group(function () {
                    Route::get('data_siswa_rombel', [DataSiswaController::class, 'data_siswa_rombel'])->name('data_siswa_rombels');
                    Route::post('store_siswa', [DataSiswaController::class, 'store_siswa'])->name('store-siswa_by_wali_kelas');
                    Route::get('edit/{id}', [DataSiswaController::class, 'edit'])->name('edit_siswa');
                    Route::put('update', [DataSiswaController::class, 'update_by_wali_kelas'])->name('update-siswa_by_wali_kelas');
                    Route::get('siswa/{id}', [DataSiswaController::class, 'detail_siswa'])->name('show_siswa');
                    Route::delete('trash/{id}', [DataSiswaController::class, 'trash'])->name('trash-siswa_by_wali_kelas');
                });

                //absensi
                Route::get('absensi', [AbsensiWaliKelasController::class, 'index'])->name('walikelas-absensi');
                Route::prefix('absensi')->group(function () {
                    Route::get('detail/{id}', [AbsensiWaliKelasController::class, 'detail'])->name('walikelas-detail_absensi');
                    Route::get('detail_pertemuan/{id}', [AbsensiWaliKelasController::class, 'detail_pertemuan'])->name('walikelas-detail_pertemuan');
                    Route::put('update', [DataSiswaController::class, 'update'])->name('update-siswa_wali_kelas');
                    Route::post('store_siswa', [DataSiswaController::class, 'store_siswa'])->name('store-siswa_wali_kelas');
                    Route::post('datatable', [AbsensiWaliKelasController::class, 'get_datatable'])->name('datatable-absensi_siswa_wali_kelas');
                    Route::post('get_detail', [AbsensiWaliKelasController::class, 'detail_absensi'])->name('wali_kelas-absensi-detail_absensi');

                    // Route::get('detail/{id}', [MapelWaliKelasController::class, 'detail'])->name('detail-guru_mapel');
                });
                //tugas
                Route::prefix('tugas')->group(function () {
                    Route::get('pengetahuan', [TugasWaliKelasController::class, 'pengetahuan'])->name('walikelas-tugas_pengetahuan');
                    Route::get('ket_download/{id}', [TugasWaliKelasController::class, 'download_ket'])->name('download-walikelas_ketrampilan');
                    Route::get('peng_download/{id}', [TugasWaliKelasController::class, 'download_peng'])->name('download-walikelas_pengetahuan');
                    Route::get('ketrampilan', [TugasWaliKelasController::class, 'ketrampilan'])->name('walikelas-tugas_ketrampilan');
                    Route::post('ket_datatable', [TugasWaliKelasController::class, 'get_ketrampilan_datatable'])->name('walikelas-tugas_ketrampilan_datatable');
                    Route::post('peng_datatable', [TugasWaliKelasController::class, 'get_pengetahuan_datatable'])->name('walikelas-tugas_pengetahuan_datatable');
                    Route::post('get_by_tugas_ketrampilan', [TugasWaliKelasController::class, 'get_by_tugas'])->name('get_by_tugas-wali_kelas_jawaban_ketrampilan');
                    Route::post('get_by_tugas_pengetahuan', [TugasWaliKelasController::class, 'get_by_tugas_peng'])->name('get_by_tugas-wali_kelas_jawaban_pengetahuan');
                    Route::get('download/{id}', [JawabanPengetahuanController::class, 'download'])->name('download-wali_kelas_hasil_peng');
                });

                //kalender rombel
                Route::get('kalender_rombel', [KalenderWaliKelasController::class, 'index'])->name('walikelas-kalender');
                Route::prefix('kalender_rombel')->group(function () {
                    Route::post('store', [KalenderWaliKelasController::class, 'store'])->name('walikelas-store_kalender');
                    Route::get('edit/{id}', [KalenderWaliKelasController::class, 'edit'])->name('walikelas-edit_kalendar');
                    Route::put('update', [KalenderWaliKelasController::class, 'update'])->name('walikelas-update_kalendar');
                    Route::delete('trash', [KalenderWaliKelasController::class, 'trash'])->name('walikelas-trash_kalendar');
                });

                //bahan ajar
                Route::get('bahan_ajar', [BahanAjarWaliKelasController::class, 'index'])->name('walikelas-bahan_ajar');
                Route::prefix('bahan_ajar')->group(function () {
                    Route::get('detail/{id}', [BahanAjarWaliKelasController::class, 'detail'])->name('walikelas-bahan_ajar_detail');
                    Route::post('get_datatable', [BahanAjarWaliKelasController::class, 'get_datatable'])->name('walikelas-bahan_ajar_datatable');
                    Route::get('download/{id}', [BahanAjarWaliKelasController::class, 'download'])->name('walikelas-download_bahan_ajar');
                });
            });
        });
        //Program Raport
        Route::prefix('raport')->group(function () {
            Route::get('mapel', [RaportMapelController::class, 'index'])->name('raport-mapel');
            Route::prefix('mapel')->group(function () {
                Route::get('dashboard', [MapelKelasController::class, 'index'])->name('e_learning-master');
            });
            Route::get('nilai-pengetahuan/{id}', [NilaiPengetahuanController::class, 'index'])->name('raport-nilai_pengetahuan');
            Route::prefix('nilai-pengetahuan/{id}')->group(function () {
                Route::post('store', [NilaiPengetahuanController::class, 'store'])->name('save-raport_data_nilai_pengetahuan');
                Route::get('edit', [NilaiPengetahuanController::class, 'simpan'])->name('save-raport_nilai_pengetahuan');
                Route::post('list_kd', [NilaiPengetahuanController::class, 'list_kd'])->name('list_kd-raport_nilai_pengetahuan');
                Route::post('ambil_siswa', [NilaiPengetahuanController::class, 'ambil_siswa'])->name('ambil_siswa-raport_nilai_pengetahuan');
                Route::post('simpan_manual', [NilaiPengetahuanController::class, 'simpan_manual'])->name('simpan_manual-raport_nilai_pengetahuan');
            });
            Route::get('nilai-manual/{id}', [NilaiController::class, 'index'])->name('raport-nilai_manual');
            Route::prefix('nilai-manual/{id}')->group(function () {
                Route::post('simpan_manual', [NilaiController::class, 'simpan_manual'])->name('simpan_manual-raport_nilai_manual');
                Route::post('get_predikat', [NilaiController::class, 'get_range'])->name('get_predikat-raport_nilai_manual');
            });
            Route::get('nilai-k16/{id}', [Nilaik16Controller::class, 'index'])->name('raport-nilai_k16');
            Route::prefix('nilai-k16/{id}')->group(function () {
                Route::post('simpan', [Nilaik16Controller::class, 'simpan'])->name('simpan_manual-raport_nilai_k16');
                // Route::post('get_predikat', [NilaiController::class, 'get_range'])->name('get_predikat-raport_nilai_manual');
            });
            Route::get('nilai-ketrampilan/{id}', [NilaiKetrampilanController::class, 'index'])->name('raport-nilai_ketrampilan');
            Route::prefix('nilai-ketrampilan/{id}')->group(function () {
                Route::post('store', [NilaiKetrampilanController::class, 'store'])->name('save-raport_nilai_ketrampilan');
                Route::get('edit', [NilaiKetrampilanController::class, 'edit'])->name('save-raport_nilai_ketrampilan');
                Route::post('list_kd', [NilaiKetrampilanController::class, 'list_kd'])->name('list_kd-raport_ketrampilan');
            });

            Route::get('riwayat-mengajar', [RiwayatMengajarController::class, 'index'])->name('raport-riwayat_mengajar');
            Route::prefix('nilai-pengetahuan')->group(function () {
                Route::get('dashboard', [MapelKelasController::class, 'index'])->name('e_learning-master');
            });

            Route::get('nilai-sikap_spr', [NilaiSikapSpiritualController::class, 'index'])->name('raport-nilai_sikap_spiritual');
            Route::prefix('nilai-sikap_spr')->group(function () {
                Route::post('store', [NilaiSikapSpiritualController::class, 'store'])->name('raport-store_sikap_spiritual');
                Route::post('simpan_manual', [NilaiSikapSpiritualController::class, 'simpan_manual'])->name('raport-simpan_sikap_spiritual');
                Route::get('cetak', [NilaiSikapSpiritualController::class, 'cetak'])->name('raport-cetak_sikap_spiritual');
            });

            Route::get('nilai-sikap_sosial', [NilaiSikapSosialController::class, 'index'])->name('raport-nilai_sikap_sosial');
            Route::prefix('nilai-sikap_sosial')->group(function () {
                Route::post('store', [NilaiSikapSosialController::class, 'store_revisi'])->name('store-nilai_sikap_sosial');
                Route::get('cetak', [NilaiSikapSosialController::class, 'cetak'])->name('raport-cetak_nilai_sikap_sosial');
            });

            Route::get('nilai-ekstrakurikuler', [NilaiEkstrakurikulerController::class, 'index'])->name('raport-nilai_ekstrakurikuler');
            Route::prefix('nilai-ekstrakurikuler')->group(function () {
                Route::post('create', [NilaiEkstrakurikulerController::class, 'store_revisi'])->name('raport-store_nilai_ekstrakurikuler');
                Route::post('get_siswa', [NilaiEkstrakurikulerController::class, 'get_siswa_revisi'])->name('raport_get_siswa-nilai_ekstrakurikuler');
            });



            Route::get('absensi', [RaportAbsensiController::class, 'index'])->name('raport-absensi');
            Route::prefix('absensi')->group(function () {
                Route::post('simpan', [RaportAbsensiController::class, 'store'])->name('raport-simpan_absensi');
                Route::post('import', [RaportAbsensiController::class, 'import'])->name('raport-import_absensi');
            });

            Route::get('prestasi', [PrestasiController::class, 'index'])->name('raport-prestasi');
            Route::prefix('prestasi')->group(function () {
                Route::post('create', [PrestasiController::class, 'store'])->name('raport-store_prestasi');
                Route::put('update', [PrestasiController::class, 'update'])->name('raport-update_prestasi');
                Route::post('edit', [PrestasiController::class, 'edit'])->name('raport-edit_prestasi');
                Route::delete('trash/{id}', [PrestasiController::class, 'trash'])->name('raport-trash_prestasi');
            });

            Route::get('catatan', [CatatanController::class, 'index'])->name('raport-catatan');
            Route::prefix('catatan')->group(function () {
                Route::post('simpan', [CatatanController::class, 'store'])->name('raport-save_catatan');
            });

            Route::get('cetak-raport', [CetakRaportController::class, 'index'])->name('raport-cetak_raport');
            Route::prefix('cetak-raport')->group(function () {
                Route::get('cetak/{id_siswa}', [CetakRaportController::class, 'cetak_sampul1'])->name('raport-cetak_sampul_1');
                Route::get('cetak2/{id_siswa}', [CetakRaportController::class, 'cetak_sampul2'])->name('raport-cetak_sampul_2');
                Route::get('cetak3/{id_siswa}', [CetakRaportController::class, 'cetak_sampul3'])->name('raport-cetak_sampul_3');
                Route::get('cetak4/{id_siswa}', [CetakRaportController::class, 'cetak_sampul_raport'])->name('raport-cetak_sampul_raport');
                Route::get('prestasi/{id_siswa}', [CetakRaportController::class, 'cetak_prestasi'])->name('raport-cetak_prestasi_raport');
            });

            Route::get('cetak-leger', [CetakLegerController::class, 'index'])->name('raport-cetak_leger');
            Route::prefix('cetak-leger')->group(function () {
                Route::get('print', [CetakLegerController::class, 'print_leger'])->name('raport-cetak_leger');
            });
        });
        //Program Point
        Route::prefix('point')->group(function () {
            Route::get('pesan_point', [PointPesanController::class, 'index'])->name('point_pesan-beranda');
            Route::prefix('pesan_point')->group(function () {
                Route::post('get_chat', [PointPesanController::class, 'get_chat'])->name('point-get_chat');
                Route::post('save_chat', [PointPesanController::class, 'save_Chat'])->name('point_chat-save');
            });

            Route::get('siswa', [PointSiswaController::class, 'index'])->name('point-siswa');
            Route::prefix('siswa')->group(function () {
                Route::post('search_siswa', [PointSiswaController::class, 'get_siswa'])->name('point_siswa-get_data');
                Route::post('search', [PointSiswaController::class, 'get_siswa_pesan'])->name('point_siswa-get_data_pesan');
                Route::post('update', [PointSiswaController::class, 'update_siswa'])->name('point_siswa-update_data_siswa');
                Route::get('pelanggaran-sering', [PointSiswaController::class, 'sering_dilakukan'])->name('point_siswa-sering_dilakukan');
            });

            //jenis beasiswa
            Route::get('jenis-beasiswa', [JenisBeasiswaController::class, 'index'])->name('point_jenis_beasiswa-beranda');
            Route::prefix('jenis-beasiswa')->group(function () {
                Route::post('detail', [JenisBeasiswaController::class, 'detail'])->name('point_jenis_beasiswa-detail');
                Route::post('simpan', [JenisBeasiswaController::class, 'store'])->name('point_jenis_beasiswa-simpan');
                Route::post('update', [JenisBeasiswaController::class, 'update'])->name('point_jenis_beasiswa-update');
                Route::post('delete', [JenisBeasiswaController::class, 'delete'])->name('point_jenis_beasiswa-delete');
                Route::get('trash', [JenisBeasiswaController::class, 'data_trash'])->name('point_jenis_beasiswa-data_trash');
                Route::post('restore', [JenisBeasiswaController::class, 'restore'])->name('point_jenis_beasiswa-restore');
                Route::post('hard-delete', [JenisBeasiswaController::class, 'hard_delete'])->name('point_jenis_beasiswa-hard_delete');
                Route::post('import', [JenisBeasiswaController::class, 'import'])->name('point_jenis_beasiswa-import');
            });

            //beasiswa
            Route::get('beasiswa', [BeasiswaController::class, 'index'])->name('point_beasiswa-beranda');
            Route::prefix('beasiswa')->group(function () {
                Route::get('create', [BeasiswaController::class, 'create'])->name('point_beasiswa-create');
                Route::post('detail', [BeasiswaController::class, 'detail'])->name('point_beasiswa-detail');
                Route::post('simpan', [BeasiswaController::class, 'store'])->name('point_beasiswa-simpan');
                Route::post('update', [BeasiswaController::class, 'update'])->name('point_beasiswa-update');
                Route::post('delete', [BeasiswaController::class, 'delete'])->name('point_beasiswa-delete');
                Route::post('kelas_siswa', [BeasiswaController::class, 'get_siswa'])->name('point_beasiswa-data_siswa');
                Route::post('import', [BeasiswaController::class, 'import'])->name('point_beasiswa-import');
                Route::get('trash', [BeasiswaController::class, 'data_trash'])->name('point_beasiswa-data_trash');
                Route::post('restore', [BeasiswaController::class, 'restore'])->name('point_beasiswa-restore');
                Route::post('hard-delete', [BeasiswaController::class, 'hard_delete'])->name('point_beasiswa-hard_delete');
            });

            //karir
            Route::get('karir', [KarirController::class, 'index'])->name('point_karir-beranda');
            Route::prefix('karir')->group(function () {
                Route::post('siswa', [KarirController::class, 'get_siswa'])->name('point_karir-data_siswa');
                Route::post('detail', [KarirController::class, 'detail'])->name('point_karir-detail');
                Route::post('simpan', [KarirController::class, 'store'])->name('point_karir-simpan');
                Route::post('update', [KarirController::class, 'update'])->name('point_karir-update');
                Route::post('delete', [KarirController::class, 'delete'])->name('point_karir-delete');
                Route::post('update_status', [KarirController::class, 'update_status'])->name('point_karir-update_status');
            });




            Route::get('pesan/{slug}', [EmailController::class, 'pesan'])->name('point_pesan_email-slug');
            Route::get('pesan_email', [EmailController::class, 'index'])->name('point_pesan_email-beranda');
            Route::prefix('pesan_email')->group(function () {
                Route::post('save_chat', [EmailController::class, 'save_chat'])->name('point_pesan_email-save');
                Route::post('get_datatable', [EmailController::class, 'get_datatable'])->name('point_pesan_email-datatable');
                Route::post('delete', [EmailController::class, 'delete_array'])->name('point_pesan_email-delete');
                Route::post('detail', [EmailController::class, 'detail'])->name('point_pesan_email-detail');
                Route::get('detail_pesan/{id}', [EmailController::class, 'detail_pesan'])->name('point_pesan_email-detail_pesan');
                Route::post('trash', [EmailController::class, 'delete'])->name('point_pesan_email-delete_pesan');
                Route::post('reply', [EmailController::class, 'reply'])->name('point_pesan_email-reply');
            });

            Route::get('wali_murid', [WaliMuridController::class, 'index'])->name('point_wali_murid-beranda');
            Route::prefix('histori')->group(function () {
                Route::delete('delete/{id}', [WaliMuridController::class, 'delete'])->name('point_wali_murid-delete');
            });

            Route::get('pelanggaran_siswa', [PelanggaranSiswaController::class, 'index'])->name('point_pelanggaran_siswa');
            Route::get('histori', [HistoriPelanggaranController::class, 'index'])->name('point_histori-beranda');
        });
        //Program Kesiswaan
        Route::prefix('kesiswaan')->group(function () {
            Route::get('kategori_pelanggaran', [KategoriPelanggaranController::class, 'index'])->name('kesiswaan_ketegori_pelanggaran-beranda');
            Route::prefix('kategori_pelanggaran')->group(function () {
                Route::post('simpan', [KategoriPelanggaranController::class, 'store'])->name('kesiswaan_kategori_pelanggaran-simpan');
                Route::put('update', [KategoriPelanggaranController::class, 'update'])->name('kesiswaan_kategori_pelanggaran-update');
                Route::post('delete', [KategoriPelanggaranController::class, 'delete'])->name('kesiswaan_kategori_pelanggaran-delete');
                Route::post('edit', [KategoriPelanggaranController::class, 'edit'])->name('kesiswaan_kategori_pelanggaran-edit');
            });

            Route::get('siswa', [KesiswaanSiswaController::class, 'index'])->name('kesiswaan_siswa-beranda');
            Route::prefix('siswa')->group(function () {
                Route::post('get_siswa', [KesiswaanSiswaController::class, 'get_siswa'])->name('kesiswaan_siswa-get_siswa');
                Route::get('cetak_absensi/{id_kelas_siswa}/{id_mapel}/{id_rombel}', [KesiswaanSiswaController::class, 'cetak_absensi'])->name('kesiswaan_siswa-cetak_absensi');
            });


            Route::get('absensi', [AbsensiKesiswaanController::class, 'index'])->name('kesiswaan-absensi');
            Route::prefix('absensi')->group(function () {
                Route::post('mapel', [AbsensiKesiswaanController::class, 'get_mapel'])->name('kesiswaan_absensi-mapel');
                Route::post('get_siswa', [AbsensiKesiswaanController::class, 'get_siswa'])->name('kesiswaan_absensi-get_siswa');
            });

            Route::get('program_kerja', [ProgramKerjaController::class, 'index'])->name('kesiswaan-program_kerja');
            Route::prefix('program_kerja')->group(function () {
                Route::post('simpan', [ProgramKerjaController::class, 'store'])->name('kesiswaan_simpan-program_kerja');
                Route::post('update', [ProgramKerjaController::class, 'update'])->name('kesiswaan_update-program_kerja');
                Route::post('soft-delete', [ProgramKerjaController::class, 'soft_delete'])->name('kesiswaan_soft_delete-program_kerja');
                Route::post('detail', [ProgramKerjaController::class, 'detail'])->name('kesiswaan_detail-program_kerja');
                Route::get('download/{slug}/{id}', [ProgramKerjaController::class, 'download'])->name('kesiswaan_program_kerja-download');
            });

            Route::get('ekstrakurikuler', [AnggotaEkskulController::class, 'index'])->name('kesiswaan-anggota_ekskul');
            Route::prefix('ekstrakurikuler')->group(function () {
                Route::post('simpan', [AnggotaEkskulController::class, 'store'])->name('kesiswaan_simpan-anggota_ekskul');
                Route::put('update', [AnggotaEkskulController::class, 'update'])->name('kesiswaan_update-anggota_ekskul');
                Route::post('edit', [AnggotaEkskulController::class, 'detail'])->name('kesiswaan_detail-anggota_ekskul');
                Route::post('delete', [AnggotaEkskulController::class, 'delete'])->name('kesiswaan_delete-anggota_ekskul');
            });



            Route::get('pelanggaran_siswa', [PelanggaranSiswaController::class, 'index'])->name('point_pelanggaran_siswa-beranda');
            Route::prefix('pelanggaran_siswa')->group(function () {
                Route::post('simpan', [PelanggaranSiswaController::class, 'store'])->name('kesiswaan_pelanggaran_siswa-simpan');
                Route::put('update', [PelanggaranSiswaController::class, 'update'])->name('kesiswaan_pelanggaran_siswa-update');
                Route::delete('delete/{id}', [PelanggaranSiswaController::class, 'delete'])->name('kesiswaan_pelanggaran_siswa-delete');
                Route::post('edit', [PelanggaranSiswaController::class, 'edit'])->name('kesiswaan_pelanggaran_siswa-edit');
                Route::post('get_siswa', [PelanggaranSiswaController::class, 'get_siswa'])->name('kesiswaan_pelanggaran_siswa-data_siswa');
                Route::post('get_pelanggaran', [PelanggaranSiswaController::class, 'get_pelanggaran'])->name('kesiswaan_pelanggaran_siswa-get_pelanggaran_by_kategori');
                Route::post('next_input', [PelanggaranSiswaController::class, 'next_input'])->name('kesiswaan_pelanggaran_siswa-next_input');
                Route::post('finish_step', [PelanggaranSiswaController::class, 'finish_step'])->name('kesiswaan_pelanggaran_siswa-finish_step');
                Route::post('soft_delete', [PelanggaranSiswaController::class, 'soft_delete'])->name('kesiswaan_pelanggaran_siswa-soft_delete');
                Route::get('get_datatable', [PelanggaranSiswaController::class, 'getCustomFilterData'])->name('kesiswaan_pelanggaran_siswa-datatable');
                Route::get('get_detail', [PelanggaranSiswaController::class, 'getDetail'])->name('pelanggaran_siswa-detail_datatable');
                Route::get('filter_siswa', [PelanggaranSiswaController::class, 'get_filter_siswa'])->name('pelanggaran_siswa-datatable_siswa');
                Route::get('pelanggaran_terbanyak', [PelanggaranSiswaController::class, 'get_pelanggaran_terbanyak'])->name('pelanggaran_siswa-morris_donut');
                Route::get('dashboard_siswa', [PelanggaranSiswaController::class, 'get_pelanggaran_dashboard_siswa'])->name('pelanggaran_login_siswa-morris_donut');
                Route::post('update_sanksi', [PelanggaranSiswaController::class, 'update_sanksi'])->name('pelanggaran_siswa-update_sanksi');
            });
            Route::get('pelanggaran', [PelanggaranController::class, 'index'])->name('point_pelanggaran-beranda');
            Route::prefix('pelanggaran')->group(function () {
                Route::post('simpan', [PelanggaranController::class, 'store'])->name('point_pelanggaran-simpan');
                Route::put('update', [PelanggaranController::class, 'update'])->name('point_pelanggaran-update');
                Route::post('delete', [PelanggaranController::class, 'delete'])->name('point_pelanggaran-delete');
                Route::post('edit', [PelanggaranController::class, 'edit'])->name('point_pelanggaran-edit');
            });
            Route::get('sanksi', [SanksiController::class, 'index'])->name('kesiswaan_sanksi-beranda');
            Route::prefix('sanksi')->group(function () {
                Route::post('simpan', [SanksiController::class, 'store'])->name('kesiswaan_sanksi-simpan');
                Route::put('update', [SanksiController::class, 'update'])->name('kesiswaan_sanksi-update');
                Route::post('delete', [SanksiController::class, 'soft_delete'])->name('kesiswaan_sanksi-delete');
                Route::post('edit', [SanksiController::class, 'edit'])->name('kesiswaan_sanksi-edit');
                Route::get('data-trash', [SanksiController::class, 'data_trash'])->name('data_trash-kesiswaan_sanksi');
                Route::delete('hard_delete/{id}', [SanksiController::class, 'delete'])->name('delete-point_sanksi');
                Route::patch('restore/{id}', [SanksiController::class, 'restore'])->name('restore-kesiswaan_sanksi');
            });

            Route::get('histori', [HistoriPelanggaranController::class, 'index'])->name('kesiswaan_histori-beranda');
            Route::prefix('histori')->group(function () {
                Route::post('delete', [HistoriPelanggaranController::class, 'delete'])->name('kesiswaan_histori-delete');
            });

            Route::get('admin_kesiswaan', [AdminKesiswaanController::class, 'index'])->name('kesiswaan_admin-beranda');
            Route::prefix('admin_kesiswaan')->group(function () {
                Route::post('simpan', [AdminKesiswaanController::class, 'store'])->name('kesiswaan_admin-simpan');
                Route::post('update', [AdminKesiswaanController::class, 'update'])->name('kesiswaan_admin-update');
                Route::get('edit', [AdminKesiswaanController::class, 'edit'])->name('kesiswaan_admin-edit');
                Route::post('detail', [AdminKesiswaanController::class, 'detail'])->name('kesiswaan_admin-detail');
                Route::post('delete', [AdminKesiswaanController::class, 'delete'])->name('kesiswaan_admin-delete');
                Route::post('reset_password', [AdminKesiswaanController::class, 'reset_password'])->name('reset_password-admin_kesiswaan');

                Route::post('res_pass', [AdminKesiswaanController::class, 'res_pass'])->name('reset_password_manual-admin_kesiswaan');
                Route::post('delete-profile', [AdminKesiswaanController::class, 'delete_profile'])->name('delete_profile-admin_kesiswaan');
                Route::post('upload_profile', [AdminKesiswaanController::class, 'upload_profile'])->name('upload_profile-admin_kesiswaan');
            });
        });
        //Program Bursa Kerja
        Route::prefix('bursa_kerja')->group(function () {
            Route::get('pelamar', [PelamarController::class, 'index'])->name('bkk_pelamar-beranda');
            Route::prefix('pelamar')->group(function () {
                Route::get('create/{id}', [PelamarController::class, 'create'])->name('bkk_pelamar-create');
                Route::post('simpan', [PelamarController::class, 'store'])->name('bkk_pelamar-simpan');
                Route::post('edit', [PelamarController::class, 'edit'])->name('bkk_pelamar-edit');
                Route::post('get_datatable_pelamar', [PelamarController::class, 'get_datatable_pelamar'])->name('bkk_pelamar-datatable_pelamar');
                Route::post('get_data', [PelamarController::class, 'get_data'])->name('bkk_pelamar-get_data');
                Route::post('get_status', [PelamarController::class, 'data_status'])->name('bkk_pelamar-status_by_perusahaan');
                Route::post('update_status', [PelamarController::class, 'update_status'])->name('bkk_pelamar-update_status');
                Route::post('detail', [PelamarController::class, 'detailPelamar'])->name('bkk_pelamar-detail_pelamar');
                Route::post('admin_datatable', [PelamarController::class, 'admin_datatable'])->name('bkk_pelamar-loker_datatable');
                Route::post('riwayat', [PelamarController::class, 'riwayat_pelamar'])->name('bkk_pelamar-admin_riwayat');
                Route::post('filter_name', [PelamarController::class, 'filter_name'])->name('bkk_user_filter_name');
            });

            Route::get('industri', [IndustriController::class, 'index'])->name('bkk_industri-beranda');
            Route::prefix('industri')->group(function () {
                Route::get('detail', [IndustriController::class, 'detail'])->name('bkk_industi-detail');
                Route::post('edit', [IndustriController::class, 'edit'])->name('bkk_industi-edit');
                Route::get('change', [IndustriController::class, 'change'])->name('bkk_industi-change');
                Route::post('update', [IndustriController::class, 'update'])->name('bkk_industi-update');
                Route::post('save', [IndustriController::class, 'store'])->name('bkk_industi-save');
                Route::post('soft_delete', [IndustriController::class, 'soft_delete'])->name('bkk_industi-soft_delete');
                Route::post('search', [IndustriController::class, 'search_name'])->name('bkk_industi-filter_name');
                Route::post('delete-profile', [IndustriController::class, 'delete_profile'])->name('delete_profile-bkk_industri');
                Route::post('upload_profile', [IndustriController::class, 'upload_profile'])->name('upload_profile-bkk_industri');
            });

            Route::get('admin_bkk', [AdminBKKController::class, 'index'])->name('bkk_admin-beranda');
            Route::prefix('admin_bkk')->group(function () {
                Route::get('detail', [AdminBKKController::class, 'detail'])->name('bkk_admin-detail');
                Route::post('edit', [AdminBKKController::class, 'edit'])->name('bkk_admin-edit');
                Route::post('update', [AdminBKKController::class, 'update'])->name('bkk_admin-update');
                Route::post('save', [AdminBKKController::class, 'store'])->name('bkk_admin-save');
                Route::delete('soft_delete/{id}', [AdminBKKController::class, 'trash'])->name('bkk_admin-soft_delete');
                Route::post('reset_password', [AdminBKKController::class, 'reset_password'])->name('reset_password-admin_bkk');
            });

            Route::get('pengalaman', [PengalamanController::class, 'index'])->name('bkk_pengalaman-beranda');
            Route::prefix('pengalaman')->group(function () {
                Route::post('create', [PengalamanController::class, 'store'])->name('bkk_pengalaman-create');
                Route::put('update', [PengalamanController::class, 'update'])->name('bkk_pengalaman-update');
                Route::post('edit', [PengalamanController::class, 'edit'])->name('bkk_pengalaman-edit');
                Route::post('soft_delete', [PengalamanController::class, 'soft_delete'])->name('bkk_pengalaman-sof_delete');
            });

            Route::get('pendidikan', [PendidikanController::class, 'index'])->name('bkk_pendidikan-beranda');
            Route::prefix('pendidikan')->group(function () {
                Route::post('create', [PendidikanController::class, 'store'])->name('bkk_pendidikan-create');
                Route::put('update', [PendidikanController::class, 'update'])->name('bkk_pendidikan-update');
                Route::post('edit', [PendidikanController::class, 'edit'])->name('bkk_pendidikan-edit');
                Route::post('soft_delete', [PendidikanController::class, 'soft_delete'])->name('bkk_pendidikan-sof_delete');
            });

            Route::get('keterampilan', [KetrampilanController::class, 'index'])->name('bkk_keterampilan-beranda');
            Route::prefix('keterampilan')->group(function () {
                Route::post('create', [KetrampilanController::class, 'store'])->name('bkk_keterampilan-create');
                Route::put('update', [KetrampilanController::class, 'update'])->name('bkk_keterampilan-update');
                Route::post('edit', [KetrampilanController::class, 'edit'])->name('bkk_keterampilan-edit');
                Route::post('soft_delete', [KetrampilanController::class, 'soft_delete'])->name('bkk_keterampilan-sof_delete');
            });

            Route::get('lowongan', [LokerController::class, 'index'])->name('lowongan-beranda');
            Route::prefix('lowongan')->group(function () {
                Route::get('create', [LokerController::class, 'create'])->name('lowongan-create');
                Route::post('share', [LokerController::class, 'share'])->name('lowongan-share');
                Route::get('next/{id}', [LokerController::class, 'deskIndustri'])->name('lowongan-deksripsi_industri');
                Route::post('save', [LokerController::class, 'store'])->name('lowongan-save_create');
                Route::put('update_deskripsi', [LokerController::class, 'update_deskripsi'])->name('bkk_loker-update_deskripsi');
                Route::get('finish-step/{id}', [LokerController::class, 'finish'])->name('bkk_loker-finish_step');
                Route::get('get_by_sekolah', [LokerController::class, 'get_by_sekolah_datatable'])->name('bkk_loker-get_sekolah');
                Route::get('edit/{id}', [LokerController::class, 'edit'])->name('bkk_loker-edit');
                Route::put('update', [LokerController::class, 'update'])->name('bkk_loker-update');
                Route::post('search', [LokerController::class, 'search'])->name('bkk_loker-search_pelamar');
                Route::post('admin/search', [LokerController::class, 'admin_search'])->name('bkk_loker-admin_search');
                Route::get('detail', [LokerController::class, 'detail'])->name('bkk_loker-get_detail');
                Route::post('draft', [LokerController::class, 'draft'])->name('bkk_loker-draft');
                Route::post('get_data', [LokerController::class, 'data_status'])->name('bkk_loker-data_loker_status');
                Route::post('soft_delete', [LokerController::class, 'delete'])->name('bkk_loker-soft_delete');
                Route::post('side_detail', [LokerController::class, 'side_detail'])->name('bkk_loker-side_detail');
                Route::post('detail_by_pelamar', [LokerController::class, 'detail_by_pelamar'])->name('bkk_loker-detail_by_pelamar');
                Route::post('load_more', [LokerController::class, 'load_more'])->name('bkk_loker-load_more');
            });

            Route::get('dokumen', [DokumenController::class, 'index'])->name('bkk_dokumen-beranda');
            Route::prefix('dokumen')->group(function () {
                Route::post('save', [DokumenController::class, 'store'])->name('bkk_dokumen-save');
                Route::post('update', [DokumenController::class, 'update'])->name('bkk_dokumen-update');
                Route::get('view/{id}', [DokumenController::class, 'view_document'])->name('bkk_dokumen-view');
                Route::post('edit', [DokumenController::class, 'edit'])->name('bkk_dokumen-edit');
                Route::get('resume', [DokumenController::class, 'resume'])->name('bkk_dokumen-resume');
                Route::get('download/{id}', [DokumenController::class, 'download'])->name('bkk_dokumen-download');
                Route::post('hard_delete', [DokumenController::class, 'hard_delete'])->name('bkk_dokumen-hard_delete');
            });

            Route::get('agenda', [AgendaController::class, 'index'])->name('bkk_agenda-beranda');
            Route::prefix('agenda')->group(function () {
                Route::post('get_datatable', [AgendaController::class, 'get_datatable'])->name('bkk_agenda-datatable');
                Route::post('create', [AgendaController::class, 'store'])->name('bkk_agenda-simpan');
                Route::put('update', [AgendaController::class, 'update'])->name('bkk_agenda-update');
                Route::get('by_loker/{id}', [AgendaController::class, 'by_loker'])->name('bkk_agenda-by_loker');
                Route::get('by_perusahaan', [AgendaController::class, 'by_user_perusahaan'])->name('bkk_agenda-by_perusahaan');
                Route::post('edit', [AgendaController::class, 'edit'])->name('bkk_agenda-edit');
                Route::post('soft_delete', [AgendaController::class, 'soft_delete'])->name('bkk_agenda-soft_delete');
            });

            Route::get('bidang_loker', [BidangLokerController::class, 'index'])->name('bkk_bidang_loker-beranda');
            Route::prefix('bidang_loker')->group(function () {
                Route::post('simpan', [BidangLokerController::class, 'store'])->name('bkk_bidang_loker-simpan');
                Route::put('update', [BidangLokerController::class, 'update'])->name('bkk_bidang_loker-update');
                Route::post('delete', [BidangLokerController::class, 'delete'])->name('bkk_bidang_loker-delete');
                Route::post('edit', [BidangLokerController::class, 'edit'])->name('bkk_bidang_loker-edit');
                Route::get('data-trash', [BidangLokerController::class, 'data_trash'])->name('bkk_bidang_loker-data_trash');
                Route::delete('hard_delete/{id}', [BidangLokerController::class, 'hard_delete'])->name('bkk_bidang_loker-hard_delete');
                Route::patch('restore/{id}', [BidangLokerController::class, 'restore'])->name('bkk_bidang_loker-restore');
                Route::post('upload_data', [BidangLokerController::class, 'import'])->name('bkk_bidang_loker-import');
            });


            Route::get('profile-sekolah', [ProfilBKKController::class, 'index'])->name('bkk_profile-beranda');
            Route::prefix('profile-sekolah')->group(function () {
                Route::get('layout', [ProfilBKKController::class, 'layout'])->name('bkk_profile-layout');
                Route::post('create', [ProfilBKKController::class, 'store'])->name('bkk_profile-create');
                Route::post('update', [ProfilBKKController::class, 'update'])->name('bkk_profile-update');
                Route::post('edit', [ProfilBKKController::class, 'edit'])->name('bkk_profile-edit');
                Route::post('soft_delete', [ProfilBKKController::class, 'soft_delete'])->name('bkk_profile-sof_delete');
            });

            Route::get('user-bkk', [UserBKKController::class, 'index'])->name('user_bkk-beranda');
            Route::prefix('user-bkk')->group(function () {
                Route::get('perusahaan', [UserBKKController::class, 'user_perusahaan'])->name('user_bkk-perusahaan');
                Route::post('create', [UserBKKController::class, 'store'])->name('user_bkk-create');
                Route::post('update', [UserBKKController::class, 'update'])->name('user_bkk-update');
                Route::post('edit', [UserBKKController::class, 'edit'])->name('user_bkk-edit');
                Route::post('datatable', [UserBKKController::class, 'get_datatable'])->name('user_bkk-get_datatable');
                Route::post('soft_delete', [UserBKKController::class, 'soft_delete'])->name('user_bkk-sof_delete');
                Route::post('import', [UserBKKController::class, 'import'])->name('import-user_bkk');
            });
        });
        //Progam Prakerin
        Route::prefix('sistem_pkl')->group(function () {
            Route::get('dashboard_guru', [DashboardController::class, 'pkl_dashboard_guru'])->name('sistem_pkl-dashboard_guru');
            Route::get('industri', [PrakerinIndustriController::class, 'index'])->name('pkl_industri-beranda');
            Route::prefix('industri')->group(function () {
                Route::get('perusahaan', [PrakerinIndustriController::class, 'user_perusahaan'])->name('pkl_industri-perusahaan');
                Route::post('create', [PrakerinIndustriController::class, 'store'])->name('pkl_industri-create');
                Route::post('update', [PrakerinIndustriController::class, 'update'])->name('pkl_industri-update');
                Route::post('edit', [PrakerinIndustriController::class, 'edit'])->name('pkl_industri-edit');
                Route::post('load_industri', [PrakerinIndustriController::class, 'load_pembimbing_kelompok'])->name('load_kelompok_pemb_industri-by_industri');
                Route::post('delete', [PrakerinIndustriController::class, 'delete'])->name('pkl_industri-delete');
                Route::post('load_kabupaten', [PrakerinIndustriController::class, 'load_kabupaten'])->name('pkl_industri-load_kabupaten_by_industri');
                Route::post('delete-pembimbing', [PrakerinIndustriController::class, 'deletePembimbing'])->name('pkl_industri-delete_pembimbing');
                Route::post('simpan-pembimbing', [PrakerinIndustriController::class, 'simpanPembimbing'])->name('pkl_industri-simpan_pembimbing');
                Route::post('update-pembimbing', [PrakerinIndustriController::class, 'updatePembimbing'])->name('pkl_industri-update_pembimbing');
                Route::post('import', [PrakerinIndustriController::class, 'import'])->name('import-industri_prakerin');
            });



            Route::get('slider', [SliderController::class, 'index'])->name('pkl_slider-beranda');
            Route::prefix('slider')->group(function () {
                Route::post('create', [SliderController::class, 'store'])->name('pkl_slider-create');
                Route::post('update', [SliderController::class, 'update'])->name('pkl_slider-update');
                Route::post('edit', [SliderController::class, 'edit'])->name('pkl_slider-edit');
                Route::post('delete', [SliderController::class, 'delete'])->name('pkl_slider-delete');
            });

            Route::get('config', [PrakerinConfigController::class, 'index'])->name('pkl_config-beranda');
            Route::prefix('config')->group(function () {
                Route::post('create', [PrakerinConfigController::class, 'store'])->name('pkl_config-create');
            });


            Route::get('kelompok', [KelompokController::class, 'index'])->name('pkl_kelompok-beranda');
            Route::prefix('kelompok')->group(function () {
                Route::get('list', [KelompokController::class, 'list_data'])->name('pkl_kelompok-list_data');
                Route::post('create', [KelompokController::class, 'store'])->name('pkl_kelompok-create');
                Route::put('update', [KelompokController::class, 'update'])->name('pkl_kelompok-update');
                Route::post('edit', [KelompokController::class, 'edit'])->name('pkl_kelompok-edit');
                Route::post('delete', [KelompokController::class, 'delete'])->name('pkl_kelompok-delete');
                Route::post('list_siswa', [KelompokController::class, 'list_siswa'])->name('pkl_kelompok-list_siswa');
                Route::post('create-siswa', [KelompokController::class, 'create_siswa'])->name('pkl_kelompok-create_siswa');
                Route::post('delete-siswa', [KelompokController::class, 'delete_siswa'])->name('pkl_kelompok-delete_siswa');
                Route::post('edit_waktu_siswa', [KelompokController::class, 'edit_waktu_siswa'])->name('pkl_kelompok-edit_waktu_siswa');
                Route::get('cetak/{id}', [KelompokController::class, 'cetak'])->name('pkl_kelompok-cetak');
                Route::post('industri', [KelompokController::class, 'by_industri'])->name('pkl_kelompok-by_industri');
                Route::post('save-all', [KelompokController::class, 'save_multiple'])->name('pkl_kelompok-save_peserta');
            });

            Route::get('jenis_nilai', [JenisNilaiController::class, 'index'])->name('pkl_jenis_nilai-beranda');
            Route::prefix('jenis_nilai')->group(function () {
                Route::post('create', [JenisNilaiController::class, 'store'])->name('pkl_jenis_nilai-create');
                Route::put('update', [JenisNilaiController::class, 'update'])->name('pkl_jenis_nilai-update');
                Route::post('edit', [JenisNilaiController::class, 'edit'])->name('pkl_jenis_nilai-edit');
                Route::post('delete', [JenisNilaiController::class, 'delete'])->name('pkl_jenis_nilai-delete');
            });


            Route::get('nilai', [PrakerinNilaiController::class, 'index'])->name('pkl_nilai-beranda');
            Route::prefix('nilai')->group(function () {
                Route::post('create', [PrakerinNilaiController::class, 'store'])->name('pkl_nilai-create');
                Route::put('update', [PrakerinNilaiController::class, 'update'])->name('pkl_nilai-update');
                Route::post('edit', [PrakerinNilaiController::class, 'edit'])->name('pkl_nilai-edit');
                Route::post('delete', [PrakerinNilaiController::class, 'delete'])->name('pkl_nilai-delete');
                Route::post('get_jurusan', [PrakerinNilaiController::class, 'get_by_jurusan'])->name('jenis_nilai-searh_jurusan');
            });


            Route::get('peserta', [SiswaPesertaController::class, 'index'])->name('pkl_siswa-beranda');
            Route::prefix('peserta')->group(function () {
                Route::get('tambah', [SiswaPesertaController::class, 'create'])->name('pkl_siswa-tambah');
                Route::post('create', [SiswaPesertaController::class, 'store'])->name('pkl_siswa-create');
                Route::put('update', [SiswaPesertaController::class, 'update'])->name('pkl_siswa-update');
                Route::post('edit', [SiswaPesertaController::class, 'edit'])->name('pkl_siswa-edit');
                Route::post('delete', [SiswaPesertaController::class, 'delete'])->name('pkl_siswa-delete');
                Route::post('kelompok', [SiswaPesertaController::class, 'get_by_kelompok'])->name('pkl_siswa-get_kelompok');
                Route::post('search_siswa', [SiswaPesertaController::class, 'search_siswa'])->name('pkl_siswa-search_name');
                Route::post('search_by_pembimbing_sekolah', [SiswaPesertaController::class, 'search_pembimbing_sekolah'])->name('pkl_siswa-search_name-pembimbing_sekolah');
                Route::post('get_siswa', [SiswaPesertaController::class, 'get_siswa'])->name('pkl_siswa-get_kelas_siswa');
                Route::post('save-siswa', [SiswaPesertaController::class, 'save_siswa'])->name('pkl_siswa-save_siswa');
            });


            Route::get('pembimbing_industri', [PembimbingIndustriController::class, 'index'])->name('pkl_pembimbing_industri-beranda');
            Route::prefix('pembimbing_industri')->group(function () {
                Route::get('detail/edit', [PembimbingIndustriController::class, 'detail'])->name('pkl_pembimbing_industri-detail_edit');
                Route::post('create', [PembimbingIndustriController::class, 'store'])->name('pkl_pembimbing_industri-create');
                Route::post('update', [PembimbingIndustriController::class, 'update'])->name('pkl_pembimbing_industri-update');
                Route::post('res_pass', [PembimbingIndustriController::class, 'reset_password'])->name('pkl_pembimbing_industri-reset_password');
                Route::post('edit', [PembimbingIndustriController::class, 'edit'])->name('pkl_pembimbing_industri-edit');
                Route::post('delete', [PembimbingIndustriController::class, 'delete'])->name('pkl_pembimbing_industri-delete');
                Route::post('industri', [PembimbingIndustriController::class, 'by_industri'])->name('pkl_pembimbing_industri-by_industri');
                Route::post('set_template', [PembimbingIndustriController::class, 'set_default'])->name('pkl_pembimbing_industri-set_template');
                Route::post('del-profile', [PembimbingIndustriController::class, 'delete_profile'])->name('pkl_pembimbing_industri-del_profile');
                Route::post('upl-profile', [PembimbingIndustriController::class, 'upload_profile'])->name('pkl_pembimbing_industri-upload_profile');
            });


            Route::get('setting', [PrakerinSettingController::class, 'index'])->name('pkl_setting-beranda');
            Route::prefix('setting')->group(function () {
                Route::post('create', [PrakerinSettingController::class, 'store'])->name('pkl_setting-create');
                Route::post('update', [PrakerinSettingController::class, 'update'])->name('pkl_setting-update');
                Route::post('update_paraf', [PrakerinSettingController::class, 'update_profile'])->name('pkl_setting-update_profile');
                Route::post('edit', [PrakerinSettingController::class, 'edit'])->name('pkl_setting-edit');
                Route::delete('delete/{id}', [PrakerinSettingController::class, 'delete'])->name('pkl_setting-delete');
            });

            Route::get('tugas', [TugasController::class, 'index'])->name('pkl_tugas-beranda');
            Route::prefix('tugas')->group(function () {
                Route::post('create', [TugasController::class, 'store'])->name('pkl_tugas-create');
                Route::post('update', [TugasController::class, 'update'])->name('pkl_tugas-update');
                Route::post('edit', [TugasController::class, 'edit'])->name('pkl_tugas-edit');
                Route::post('trash', [TugasController::class, 'delete'])->name('pkl_tugas-delete');
                Route::get('siswa/{id}', [TugasController::class, 'tugas_siswa'])->name('pkl_tugas-delete');
                Route::post('siswa/detail', [TugasController::class, 'tugas_detail_siswa'])->name('pkl_tugas-detail_siswa');
                Route::post('kegiatan/save', [TugasController::class, 'save'])->name('pkl_tugas-save_kegiatan');
                Route::post('check', [TugasController::class, 'check'])->name('pkl_tugas-check');
                Route::get('cetak/{id}', [TugasController::class, 'cetak'])->name('pkl_tugas-cetak_print');
            });

            Route::get('nilai_siswa', [NilaiSiswaController::class, 'index'])->name('pkl_nilai_siswa-beranda');
            Route::prefix('nilai_siswa')->group(function () {
                Route::post('create', [NilaiSiswaController::class, 'store'])->name('pkl_nilai_siswa-create');
                Route::get('list', [NilaiSiswaController::class, 'list_siswa'])->name('pkl_nilai_siswa-list_siswa');
                Route::post('update', [NilaiSiswaController::class, 'update'])->name('pkl_nilai_siswa-update');
                Route::post('edit', [NilaiSiswaController::class, 'edit'])->name('pkl_nilai_siswa-edit');
                Route::post('get_nilai', [NilaiSiswaController::class, 'get_nilai'])->name('pkl_nilai_siswa-get_nilai');
                Route::delete('delete/{id}', [NilaiSiswaController::class, 'delete'])->name('pkl_nilai_siswa-delete');
                Route::get('jenis/{id}', [NilaiSiswaController::class, 'jenis'])->name('pkl_nilai_siswa-jenis');
                Route::get('cetak/{id}', [NilaiSiswaController::class, 'cetak'])->name('pkl_nilai_siswa-cetak_nilai');
                Route::get('sertifikat/{id}', [NilaiSiswaController::class, 'sertifikat'])->name('pkl_nilai_siswa-cetak_sertifikat');
                Route::get('cetak/kegiatan/{id}', [NilaiSiswaController::class, 'cetak_kegiatan'])->name('pkl_nilai_siswa-cetak_kegiatan');
                Route::post('nilai/jenis', [NilaiSiswaController::class, 'get_by_jenis'])->name('pkl_nilai_siswa-nilai_by_jenis');
                Route::post('get_by_siswa', [NilaiSiswaController::class, 'get_siswa'])->name('pkl_nilai_siswa-get_siswa');
                Route::get('sertifikat/show/{id_siswa}', [NilaiSiswaController::class, 'show_sertifikat'])->name('pkl_nilai_siswa-show_sertifikat');
            });

            Route::get('informasi', [InformasiController::class, 'index'])->name('pkl_informasi-beranda');
            Route::prefix('informasi')->group(function () {
                Route::post('create', [InformasiController::class, 'store'])->name('pkl_informasi-create');
                Route::put('update', [InformasiController::class, 'update'])->name('pkl_informasi-update');
                Route::post('edit', [InformasiController::class, 'edit'])->name('pkl_informasi-edit');
                Route::post('delete', [InformasiController::class, 'delete'])->name('pkl_informasi-delete');
            });

            Route::get('pedoman', [PedomanController::class, 'index'])->name('pkl_pedoman-beranda');
            Route::prefix('pedoman')->group(function () {
                Route::post('create', [PedomanController::class, 'store'])->name('pkl_pedoman-create');
                Route::post('update', [PedomanController::class, 'update'])->name('pkl_pedoman-update');
                Route::post('edit', [PedomanController::class, 'edit'])->name('pkl_pedoman-edit');
                Route::post('delete', [PedomanController::class, 'delete'])->name('pkl_pedoman-delete');
            });

            Route::get('template_sertifikat', [TemplateSertifikatController::class, 'index'])->name('prakerin-template_sertifikat');
            Route::prefix('template_sertifikat')->group(function () {
                Route::get('sample', [TemplateSertifikatController::class, 'data'])->name('prakerin_template-data_template');
                Route::post('edit', [TemplateSertifikatController::class, 'edit'])->name('supermaster-edit_template_sertifikat');
                Route::get('detail/{id}', [TemplateSertifikatController::class, 'detail'])->name('prakerin_template-detail');
                Route::post('update', [TemplateSertifikatController::class, 'update'])->name('supermaster-update_template_sertifikat');
                Route::get('export', [TemplateSertifikatController::class, 'export'])->name('supermaster-export_template_sertifikat');
                Route::get('download', [TemplateSertifikatController::class, 'download'])->name('supermaster-download_template_sertifikat');
                Route::delete('delete/{id}', [TemplateSertifikatController::class, 'delete'])->name('supermaster-delete_template_sertifikat');
            });

            Route::get('industri_sertifikat', [PembimbingSertifikatController::class, 'index'])->name('pembimbing_sertifikat-beranda');
            Route::prefix('industri_sertifikat')->group(function () {
                Route::post('set_default', [PembimbingSertifikatController::class, 'set_default'])->name('pembimbing_sertifikat-set_default');
                Route::post('create', [PembimbingSertifikatController::class, 'store'])->name('pembimbing_sertifikat-create');
                Route::put('update', [PembimbingSertifikatController::class, 'update'])->name('pembimbing_sertifikat-update');
                Route::post('edit', [PembimbingSertifikatController::class, 'edit'])->name('pembimbing_sertifikat-detail');
            });

            Route::prefix('ajax')->group(function () {
                Route::post('siswa/lihat_nilai', [AjaxController::class, 'siswa_lihat_nilai'])->name('pkl_nilai_siswa-lihat_nilai');
                Route::post('siswa/lihat_tugas', [AjaxController::class, 'siswa_lihat_tugas'])->name('pkl_nilai_siswa-lihat_tugas');
            });
        });

        Route::prefix('spp')->group(function () {
            Route::prefix('pengaturan')->group(function () {
                Route::prefix('pos')->group(function () {
                    Route::get('/', [PosController::class, 'index'])->name('spp-pos');
                    Route::post('/', [PosController::class, 'create'])->name('spp-pos_create');
                    Route::get('{id}', [PosController::class, 'single'])->name('spp-pos_single');
                    Route::put('{id}', [PosController::class, 'edit'])->name('spp-pos_edit');
                });
                Route::prefix('pos-pemasukan')->group(function () {
                    // FIXME: tahun ajaran pindah ke tempat yang lebih pas
                    Route::get('tahun_ajaran/{tahun}', [PosPemasukanController::class, 'show_tahun'])->name('spp-pos_pemasukan_tahun');
                    Route::get('/', [PosPemasukanController::class, 'index'])->name('spp-pos_pemasukan');
                    Route::post('/', [PosPemasukanController::class, 'create'])->name('spp-pos_pemasukan_create');
                    Route::put('{id}', [PosPemasukanController::class, 'edit'])->name('spp-pos_pemasukan_edit');
                });
                Route::prefix('tagihan')->group(function () {
                    Route::get('/', [TagihanSettingController::class, 'index'])->name('spp-setting_tagihan');
                    Route::post('/', [TagihanSettingController::class, 'create'])->name('spp-setting_create');
                    Route::put('{id}', [TagihanSettingController::class, 'edit'])->name('spp-setting_edit');
                });
            });
            Route::prefix('transaksi')->group(function () {
                Route::get('/', [TransaksiController::class, 'index'])->name('spp-transaksi');
                Route::post('/', [TransaksiController::class, 'create'])->name('spp-transaksi_create');
                Route::get('/search/siswa', [TransaksiController::class, 'searchSiswa'])->name('spp-transaksi_search_siswa');
                Route::get('{kode}/transaksi_detail', [TransaksiController::class, 'detail'])->name('spp-transaksi_detail');
            });
            Route::prefix('konfirmasi')->group(function () {
                Route::get('/', [KonfirmasiController::class, 'index'])->name('spp-konfirmasi');
                Route::post('/', [KonfirmasiController::class, 'create'])->name('spp-konfirmasi_create');
                Route::get('{id_konfirmasi}/detail', [KonfirmasiController::class, 'detail'])->name('spp-konfirmasi_detail');
            });
            Route::prefix('siswa')->group(function () {
                Route::get('/', [SPPSiswaController::class, 'index'])->name('spp-siswa');
                Route::get('/search', [SPPSiswaController::class, 'search'])->name('spp-siswa_search');
                Route::get('/create', [SPPSiswaController::class, 'create'])->name('spp-siswa_create');
                Route::post('/import', [SPPSiswaController::class, 'import'])->name('spp-siswa_import');
                Route::post('/store', [SPPSiswaController::class, 'store'])->name('spp-siswa_store');
                Route::get('{id_kelas_siswa}', [SPPSiswaController::class, 'siswa'])->name('spp-siswa_detail');
                Route::get('{id_kelas_siswa}/tagihan', [SPPSiswaController::class, 'getTagihanSiswa'])->name('spp-siswa_tagihan');
                Route::get('{id_kelas_siswa}/transaksi', [SPPSiswaController::class, 'tagihanBySiswa'])->name('spp-siswa_transaksi');
            });
            Route::prefix('laporan')->group(function () {
                Route::get('/', [LaporanController::class, 'index'])->name('spp-laporan');
                Route::get('penerimaan', [LaporanController::class, 'penerimaanSPP'])->name('spp-laporan_penerimaan');
            });

            Route::prefix('jurusan')->group(function () {
                Route::post('store', [SPPJurusanController::class, 'store'])->name('spp-store-jurusan');
                Route::delete('delete/{id}', [SPPJurusanController::class, 'delete'])->name('spp-delete-jurusan');
            });

            // FIXME: beresin ini ke bawah
            Route::prefix('tagihan')->group(function () {
                Route::get('/', [TagihanController::class, 'index'])->name('spp-tagihan');
                Route::get('{id}', [TagihanController::class, 'tagihan'])->name('spp-tagihan_detail');
            });
            Route::get('spp', [SPPController::class, 'index'])->name('spp-spp');
            Route::get('spp/{id}', [SPPController::class, 'spp'])->name('spp-spp_detail');
            Route::get('info', function () {
                return view('content.spp.info')->with(['currentPage' => 'pembayaran']);
            })->name('spp-info');
        });

        Route::prefix('mutu')->group(function () {
            Route::prefix('profile')->group(function () {
                Route::get('edit', [ProfileController::class, 'index'])->name('edit-profile-mutu');
                Route::post('edit', [ProfileController::class, 'update'])->name('update-profile-mutu');
            });


            Route::prefix('master')->group(function () {
                Route::prefix('fakultas')->group(function () {
                    Route::get('/', [PMFakultasController::class, 'index'])->name('main-Fakultas');
                    Route::get('detail', [PMFakultasController::class, 'detail'])->name('detail-Fakultas');
                    Route::post('create', [PMFakultasController::class, 'create'])->name('buat-Fakultas');
                    Route::post('buat', [PMFakultasController::class, 'store'])->name('Buats-Fakultas');
                    Route::get('edit', [PMFakultasController::class, 'edit'])->name('edit-Fakultas');
                    Route::post('delete/{id}', [PMFakultasController::class, 'delete'])->name('hapus-Fakultas');
                });

                Route::prefix('jenjang')->group(function () {
                    Route::get('/', [PMJenjangController::class, 'index'])->name('main-Jenjang');
                    Route::post('create', [PMJenjangController::class, 'create'])->name('buat-Jenjang');
                    Route::get('edit', [PMJenjangController::class, 'edit'])->name('edit-Jenjang');
                    Route::get('delete', [PMJenjangController::class, 'delete'])->name('hapus-Jenjang');
                });

                Route::prefix('kejuruan')->group(function () {
                    Route::get('/', [PMKejuruanController::class, 'index'])->name('main-Kejuruan');
                    Route::post('create', [PMKejuruanController::class, 'create'])->name('buat-Kejuruan');
                    Route::post('edit', [PMKejuruanController::class, 'update'])->name('edit-Kejuruan');
                    Route::get('delete', [PMKejuruanController::class, 'delete'])->name('hapus-Kejuruan');
                });

                Route::prefix('kategori')->group(function () {
                    Route::get('/', [KategoriController::class, 'index'])->name('main-kategori');
                    Route::post('create', [KategoriController::class, 'create'])->name('buat-kategori');
                    Route::post('edit', [KategoriController::class, 'edit'])->name('edit-kategori');
                    Route::get('delete', [KategoriController::class, 'delete'])->name('hapus-kategori');
                    Route::get('detail', [KategoriController::class, 'search_by_kategori'])->name('detail-kategori');
                });

                Route::prefix('sub-kategori')->group(function () {
                    Route::get('/', [SubKategoriController::class, 'index'])->name('main-subkategori');
                    Route::post('create', [SubKategoriController::class, 'create'])->name('buat-subkategori');
                    Route::post('edit', [SubKategoriController::class, 'edit'])->name('edit-subkategori');
                    Route::get('delete', [SubKategoriController::class, 'delete'])->name('hapus-subkategori');
                    Route::get('detail', [SubKategoriController::class, 'search_by_kategori'])->name('detail-subkategori');
                });
            });

            Route::prefix('user')->group(function () {
                Route::get('/', [PMUserController::class, 'index'])->name('user-mutu');
                Route::post('create', [PMUserController::class, 'create'])->name('user-create');
                Route::get('show', [PMUserController::class, 'show'])->name('user-detail');
                Route::post('update', [PMUserController::class, 'update'])->name('user-update');
            });

            Route::prefix('pesan')->group(function () {
                Route::get('inbox', [PMPesanController::class, 'inbox'])->name('inbox-pesan');
                Route::get('send', [PMPesanController::class, 'send'])->name('send-pesan');
                Route::post('create', [PMPesanController::class, 'create'])->name('buat-pesan');
                Route::get('edit', [PMPesanController::class, 'edit'])->name('edit-pesan');
                Route::get('detail', [PMPesanController::class, 'detail'])->name('detail-pesan');
                Route::post('delete', [PMPesanController::class, 'delete'])->name('hapus-pesan');
            });

            Route::prefix('dokumen')->group(function () {
                Route::get('/', [PMDokumenController::class, 'index'])->name('all-dokumen');
                Route::get('publish', [PMDokumenController::class, 'publish_docs'])->name('main-dokumen');
                Route::get('unpublish', [PMDokumenController::class, 'unpublish_docs'])->name('unpublish-dokumen');
                Route::post('create', [PMDokumenController::class, 'create'])->name('buat-Dokumen');
                Route::post('update', [PMDokumenController::class, 'update'])->name('update-Dokumen');
                Route::get('delete', [PMDokumenController::class, 'delete'])->name('hapus-Dokumen');
                Route::get('detail', [PMDokumenController::class, 'detail'])->name('detail-Dokumen');
                Route::post('publish', [PMDokumenController::class, 'publish'])->name('publish-Dokumen');
                Route::post('unpublish', [PMDokumenController::class, 'unpublish'])->name('unpublish-Dokumen');

                Route::get('my', [PMDokumenController::class, 'my_dokumen'])->name('my-Dokumen');

                Route::get('kategori', [PMDokumenController::class, 'search_by_kategori'])->name('dokumen-kategori');
                Route::get('sub-kategori', [PMDokumenController::class, 'search_by_sub'])->name('dokumen-sub');
                Route::get('jurusan', [PMDokumenController::class, 'search_by_jurusan'])->name('dokumen-jurusan');
                Route::get('jenjang', [PMDokumenController::class, 'search_by_jenjang'])->name('dokumen-jenjang');
            });
        });

        Route::prefix('sarpras')->group(function () {
            Route::get('semua', [SPBarangController::class, 'index'])->name('semua-barang');

            Route::prefix('profile')->group(function () {
                Route::get('edit', [ProfileController::class, 'edit'])->name('edit-profile-sarana');
                Route::get('update', [ProfileController::class, 'update'])->name('update-profile-sarana');
                Route::get('change-password', [SPProfileController::class, 'change_password'])->name('change_password_sarana');
            });

            Route::prefix('master')->group(function () {
                Route::prefix('kategori')->group(function () {
                    Route::get('/', [KategoriBarangController::class, 'index'])->name('main-kategori-barang');
                    Route::get('detail', [KategoriBarangController::class, 'detail'])->name('detail-kategori-barang');
                    Route::post('create', [KategoriBarangController::class, 'create'])->name('buat-kategori-barang');
                    Route::post('edit', [KategoriBarangController::class, 'edit'])->name('edit-kategori-barang');
                    Route::get('delete', [KategoriBarangController::class, 'delete'])->name('hapus-kategori-barang');
                });

                Route::prefix('jenis')->group(function () {
                    Route::get('/', [JenisBarangController::class, 'index'])->name('main-jenis-barang');
                    Route::post('create', [JenisBarangController::class, 'create'])->name('buat-jenis-barang');
                    Route::get('detail', [JenisBarangController::class, 'detail'])->name('detail-jenis-barang');
                    Route::post('edit', [JenisBarangController::class, 'edit'])->name('edit-jenis-barang');
                    Route::get('delete', [JenisBarangController::class, 'delete'])->name('hapus-jenis-barang');
                });

                Route::prefix('satuan')->group(function () {
                    Route::get('/', [SatuanBarangController::class, 'index'])->name('main-satuan-barang');
                    Route::post('create', [SatuanBarangController::class, 'create'])->name('buat-satuan-barang');
                    Route::get('detail', [SatuanBarangController::class, 'detail'])->name('detail-satuan-barang');
                    Route::post('edit', [SatuanBarangController::class, 'edit'])->name('edit-satuan-barang');
                    Route::get('delete', [SatuanBarangController::class, 'delete'])->name('hapus-satuan-barang');
                });


                Route::prefix('lokasi')->group(function () {
                    Route::get('/', [LokasiBarangController::class, 'index'])->name('main-lokasi-barang');
                    Route::post('create', [LokasiBarangController::class, 'create'])->name('buat-lokasi-barang');
                    Route::get('detail', [LokasiBarangController::class, 'detail'])->name('detail-lokasi-barang');
                    Route::post('edit', [LokasiBarangController::class, 'edit'])->name('edit-lokasi-barang');
                    Route::get('delete', [LokasiBarangController::class, 'delete'])->name('hapus-lokasi-barang');
                });

                Route::prefix('supllyer')->group(function () {
                    Route::get('/', [SupllyerBarangController::class, 'index'])->name('main-supllyer-barang');
                    Route::post('create', [SupllyerBarangController::class, 'create'])->name('buat-supllyer-barang');
                    Route::get('detail', [SupllyerBarangController::class, 'detail'])->name('detail-supllyer-barang');
                    Route::post('edit', [SupllyerBarangController::class, 'edit'])->name('edit-supllyer-barang');
                    Route::get('delete', [SupllyerBarangController::class, 'delete'])->name('hapus-supllyer-barang');
                });
            });

            Route::prefix('admin')->group(function () {
                Route::get('/', [AdminBarangController::class, 'index'])->name('main-admin-barang');
                Route::get('detail', [AdminBarangController::class, 'detail'])->name('detail-admin-barang');
                Route::post('create', [AdminBarangController::class, 'create'])->name('buat-admin-barang');
                Route::post('edit', [AdminBarangController::class, 'edit'])->name('edit-admin-barang');
                Route::get('delete', [AdminBarangController::class, 'delete'])->name('hapus-admin-barang');
            });

            Route::prefix('user')->group(function () {
                Route::get('/', [UserBarangController::class, 'index'])->name('main-user-barang');
                Route::get('detail', [UserBarangController::class, 'detail'])->name('detail-user-barang');
                Route::post('create', [UserBarangController::class, 'create'])->name('buat-user-barang');
                Route::post('edit', [UserBarangController::class, 'edit'])->name('edit-user-barang');
                Route::get('delete', [UserBarangController::class, 'delete'])->name('hapus-user-barang');
                Route::post('redirect', [UserBarangController::class, 'redirect'])->name('redirect-user-history');
                Route::get('riwayat/{id}', [UserBarangController::class, 'history'])->name('riwayat-user');
            });

            Route::prefix('barang')->group(function () {
                Route::get('/', [BarangController::class, 'index'])->name('daftar-barang');
                Route::post('create', [BarangController::class, 'create'])->name('buat-barang');
                Route::post('detail', [BarangController::class, 'detail'])->name('detail-barang');
                Route::get('/{id}', [BarangController::class, 'detail_item'])->name('detail-item-barang');
                Route::post('edit', [BarangController::class, 'edit'])->name('edit-barang');
                Route::post('redirect', [BarangController::class, 'redirect'])->name('redirect-barang');
                Route::get('delete', [BarangController::class, 'delete'])->name('hapus-barang');
            });

            Route::prefix('item')->group(function () {
                Route::get('/', [ItemController::class, 'index'])->name('daftar-item');
                Route::post('create', [ItemController::class, 'create'])->name('buat-item');
                Route::post('detail', [ItemController::class, 'detail'])->name('detail-item');
                Route::post('edit', [ItemController::class, 'edit'])->name('edit-item');
                Route::get('delete', [ItemController::class, 'delete'])->name('hapus-item');
                Route::post('redirect', [ItemController::class, 'redirect'])->name('redirect-item-history');
                Route::get('riwayat/{id}', [ItemController::class, 'history'])->name('riwayat-item');
            });

            Route::prefix('pengadaan')->group(function () {
                Route::get('/', [PengadaanController::class, 'index'])->name('daftar-pengadaan');
                Route::get('admin', [PengadaanController::class, 'admin_page'])->name('admin-page-pengadaan');
                Route::get('detail', [PengadaanController::class, 'detail'])->name('detail-pengadaan');
                Route::post('edit', [PengadaanController::class, 'edit'])->name('edit-pengadaan');
                Route::get('delete', [PengadaanController::class, 'delete'])->name('delete-pengadaan');
                Route::get('approved', [PengadaanController::class, 'approved'])->name('approved-pengadaan');
                Route::get('rejected', [PengadaanController::class, 'rejected'])->name('rejected-pengadaan');
                Route::get('my', [PengadaanController::class, 'my'])->name('user-pengadaan');
                Route::post('request', [PengadaanController::class, 'request'])->name('request-pengadaan');
                Route::post('approve', [PengadaanController::class, 'approve'])->name('approve-pengadaan');
                Route::post('reject', [PengadaanController::class, 'reject'])->name('reject-pengadaan');
                Route::get('riwayat', [PengadaanController::class, 'riwayat'])->name('riwayat-pengadaan');
                Route::post('aksi', [PengadaanController::class, 'aksi'])->name('aksi-pengadaan');
            });

            Route::prefix('peminjaman')->group(function () {
                Route::get('/', [PeminjamanController::class, 'index'])->name('daftar-peminjaman');
                Route::get('user', [PeminjamanController::class, 'user_page'])->name('user-page-peminjaman');
                Route::get('saya', [PeminjamanController::class, 'my'])->name('peminjaman-saya');
                Route::get('history/saya', [PeminjamanController::class, 'history_saya'])->name('riwayat-peminjaman-saya');
                Route::get('admin', [PeminjamanController::class, 'admin_page'])->name('admin-page-peminjaman');
                Route::get('detail', [PeminjamanController::class, 'detail'])->name('detail-peminjaman');
                Route::get('item', [PeminjamanController::class, 'get_per_item'])->name('peminjaman-per-item');
                Route::post('edit', [PeminjamanController::class, 'edit'])->name('edit-peminjaman');
                Route::get('delete', [PeminjamanController::class, 'delete'])->name('delete-peminjaman');
                Route::post('create', [PeminjamanController::class, 'create'])->name('request-peminjaman');
                Route::post('approve', [PeminjamanController::class, 'approve'])->name('approve-peminjaman');
                Route::post('reject', [PeminjamanController::class, 'reject'])->name('reject-peminjaman');
                Route::get('riwayat', [PeminjamanController::class, 'riwayat'])->name('riwayat-peminjaman');
                Route::get('expired', [PeminjamanController::class, 'expired'])->name('expired-peminjaman');
                Route::post('kembali', [PeminjamanController::class, 'kembali'])->name('kembali-peminjaman');
            });

            Route::prefix('pemusnahan')->group(function () {
                Route::get('/', [PemusnahanController::class, 'index'])->name('daftar-pemusnahan');
                Route::get('admin', [PemusnahanController::class, 'admin_page'])->name('admin-page-pemusnahan');
                Route::get('detail', [PemusnahanController::class, 'detail'])->name('detail-pemusnahan');
                Route::post('edit', [PemusnahanController::class, 'edit'])->name('edit-pemusnahan');
                Route::get('delete', [PemusnahanController::class, 'delete'])->name('delete-pemusnahan');
                Route::get('approved', [PemusnahanController::class, 'approved'])->name('approved-pemusnahan');
                Route::get('rejected', [PemusnahanController::class, 'rejected'])->name('rejected-pemusnahan');
                Route::get('my', [PemusnahanController::class, 'my'])->name('user-pemusnahan');
                Route::post('request', [PemusnahanController::class, 'request'])->name('request-pemusnahan');
                Route::post('approve', [PemusnahanController::class, 'approve'])->name('approve-pemusnahan');
                Route::post('reject', [PemusnahanController::class, 'reject'])->name('reject-pemusnahan');
                Route::get('riwayat', [PemusnahanController::class, 'riwayat'])->name('riwayat-pemusnahan');
                Route::post('aksi', [PemusnahanController::class, 'aksi'])->name('aksi-pemusnahan');
            });

            Route::prefix('laporan')->group(function () {
                Route::get('barang', [SPLaporanController::class, 'barang'])->name('daftar-laporan-barang');
                Route::get('item', [SPLaporanController::class, 'item'])->name('daftar-laporan-item');
                Route::get('user', [SPLaporanController::class, 'user'])->name('daftar-laporan-user');
                Route::get('item_by_lokasi', [SPLaporanController::class, 'item_by_lokasi'])->name('daftar-laporan-item-by-lokasi');
                Route::get('print_item_by_lokasi/{id}', [SPLaporanController::class, 'print_item_by_lokasi'])->name('print-daftar-laporan-item-by-lokasi');
                Route::get('item_by_kondisi', [SPLaporanController::class, 'item_by_kondisi'])->name('daftar-laporan-item-by-kondisi');
                Route::get('print_item_by_kondisi/{kondisi}', [SPLaporanController::class, 'print_item_by_kondisi'])->name('print-daftar-laporan-item-by-kondisi');
                Route::get('pengadaan', [SPLaporanController::class, 'pengadaan'])->name('daftar-laporan-pengadaan');
                Route::get('pengadaan_diterima', [SPLaporanController::class, 'pengadaan_diterima'])->name('daftar-laporan-pengadaan-diterima');
                Route::get('pemusnahan', [SPLaporanController::class, 'pemusnahan'])->name('daftar-laporan-pemusnahan');
                Route::get('pemusnahan_diterima', [SPLaporanController::class, 'pemusnahan_diterima'])->name('daftar-laporan-pemusnahan-diterima');
                Route::get('peminjaman', [SPLaporanController::class, 'peminjaman'])->name('daftar-laporan-peminjaman');
                Route::post('peminjaman-by-time', [SPLaporanController::class, 'peminjaman_with_time'])->name('daftar-laporan-peminjaman-by-time');
                Route::get('peminjaman_dipinjam', [SPLaporanController::class, 'peminjaman_dipinjam'])->name('daftar-laporan-peminjaman-dipinjam');
                Route::get('user', [SPLaporanController::class, 'user'])->name('daftar-laporan-user');
            });

            Route::prefix('barcode')->group(function () {
                Route::get('/', [BarcodeController::class, 'index'])->name('daftar-barang-cetak');
                Route::post('redirect', [BarcodeController::class, 'redirect'])->name('redirect-cetak-barang');
                Route::get('/{id}', [BarcodeController::class, 'cetak_item'])->name('cetak-item-barang');
            });
        });

        Route::prefix('alumni')->group(function () {
            Route::get('fakultas', [FakultasController::class, 'index'])->name('alumni-fakultas');
            Route::prefix('fakultas')->group(function () {
                Route::post('create', [FakultasController::class, 'store'])->name('alumni-fakultas_create');
                Route::put('update', [FakultasController::class, 'update'])->name('alumni-fakultas_update');
                Route::post('edit', [FakultasController::class, 'edit'])->name('alumni-fakultas_edit');
                Route::post('trash', [FakultasController::class, 'soft_delete'])->name('alumni-fakultas_delete');
                Route::post('restore', [FakultasController::class, 'restore'])->name('alumni-fakultas_restore');
                Route::post('delete', [FakultasController::class, 'delete'])->name('alumni-fakultas_hard_delete');
                Route::post('update_status', [FakultasController::class, 'update_status'])->name('update_status-fakultas_alumni');
                Route::get('sampah', [FakultasController::class, 'data_trash'])->name('alumni-fakultas_data_trash');
            });
            Route::get('jurusan', [AlumniJurusanController::class, 'index'])->name('alumni-jurusan');
            Route::prefix('jurusan')->group(function () {
                Route::post('create', [AlumniJurusanController::class, 'store'])->name('alumni-jurusan_create');
                Route::put('update', [AlumniJurusanController::class, 'update'])->name('alumni-jurusan_update');
                Route::post('edit', [AlumniJurusanController::class, 'edit'])->name('alumni-jurusan_edit');
                Route::post('trash', [AlumniJurusanController::class, 'soft_delete'])->name('alumni-jurusan_delete');
                Route::post('restore', [AlumniJurusanController::class, 'restore'])->name('alumni-jurusan_restore');
                Route::post('delete', [AlumniJurusanController::class, 'delete'])->name('alumni-jurusan_hard_delete');
                Route::get('sampah', [AlumniJurusanController::class, 'data_trash'])->name('alumni-jurusan_data_trash');
                Route::post('update_status', [AlumniJurusanController::class, 'update_status'])->name('update_status-jurusan_alumni');
                Route::post('get_fakultas', [AlumniJurusanController::class, 'by_fakultas'])->name('alumni-jurusan_by_fakultas');
            });
            Route::get('jenis_pekerjaan', [JenisPekerjaanController::class, 'index'])->name('alumni-jenis_pekerjaan');
            Route::prefix('jenis_pekerjaan')->group(function () {
                Route::post('create', [JenisPekerjaanController::class, 'store'])->name('alumni-jenis_pekerjaan_create');
                Route::put('update', [JenisPekerjaanController::class, 'update'])->name('alumni-jenis_pekerjaan_update');
                Route::post('edit', [JenisPekerjaanController::class, 'edit'])->name('alumni-jenis_pekerjaan_edit');
                Route::post('trash', [JenisPekerjaanController::class, 'soft_delete'])->name('alumni-jenis_pekerjaan_delete');
                Route::post('restore', [JenisPekerjaanController::class, 'restore'])->name('alumni-jenis_pekerjaan_restore');
                Route::post('delete', [JenisPekerjaanController::class, 'delete'])->name('alumni-jenis_pekerjaan_hard_delete');
                Route::get('sampah', [JenisPekerjaanController::class, 'data_trash'])->name('alumni-jenis_pekerjaan_data_trash');
                Route::post('update_status', [JenisPekerjaanController::class, 'update_status'])->name('update_status-jenis_pekerjaan_alumni');
            });
            Route::get('alumni-profile', [AlumniAlumniController::class, 'index'])->name('alumni-alumni');
            Route::prefix('alumni-profile')->group(function () {
                Route::post('create', [AlumniAlumniController::class, 'store'])->name('alumni-alumni_create');
                Route::post('update', [AlumniAlumniController::class, 'update'])->name('alumni-alumni_update');
                Route::get('edit/{id}', [AlumniAlumniController::class, 'edit'])->name('alumni-alumni_edit');
                Route::post('reset_pass', [AlumniAlumniController::class, 'resetPassword'])->name('alumni-alumni_reset_password');
                Route::post('update_status', [AlumniAlumniController::class, 'update_status'])->name('update_status-user_alumni');
                Route::get('detail/{id}', [AlumniAlumniController::class, 'detail'])->name('detail-alumni_alumni');
                Route::get('my_profile', [AlumniAlumniController::class, 'my_profile'])->name('detail-profile_alumni');
                Route::post('profile', [AlumniAlumniController::class, 'profile'])->name('detail-data_alumni_profile');
                Route::get('pending', [AlumniAlumniController::class, 'pending'])->name('alumni-status_pending');
                Route::post('detail', [AlumniAlumniController::class, 'detail_profile'])->name('alumni-detail_profile');
                Route::post('delete', [AlumniAlumniController::class, 'soft_delete'])->name('alumni-soft_delete');
                Route::get('view-table', [AlumniAlumniController::class, 'tabel'])->name('alumni-tampilan_tabel');
            });

            Route::get('admin', [AlumniAdminController::class, 'index'])->name('alumni-admin');
            Route::prefix('admin')->group(function () {
                Route::post('create', [AlumniAdminController::class, 'store'])->name('alumni-admin_create');
                Route::post('update', [AlumniAdminController::class, 'update'])->name('alumni-admin_update');
                Route::post('edit', [AlumniAdminController::class, 'edit'])->name('alumni-admin_edit');
                Route::post('trash', [AlumniAdminController::class, 'soft_delete'])->name('alumni-admin_delete');
                Route::post('update_status', [AlumniAdminController::class, 'update_status'])->name('update_status-admin_alumni');
            });
            Route::get('pendidikan', [AlumniPendidikanController::class, 'index'])->name('alumni-pendidikan');
            Route::prefix('pendidikan')->group(function () {
                Route::post('create', [AlumniPendidikanController::class, 'store'])->name('alumni-pendidikan_create');
                Route::put('update', [AlumniPendidikanController::class, 'update'])->name('alumni-pendidikan_update');
                Route::post('edit', [AlumniPendidikanController::class, 'edit'])->name('alumni-pendidikan_edit');
                Route::post('trash', [AlumniPendidikanController::class, 'soft_delete'])->name('alumni-pendidikan_delete');
                Route::post('restore', [AlumniPendidikanController::class, 'restore'])->name('alumni-pendidikan_restore');
                Route::post('delete', [AlumniPendidikanController::class, 'delete'])->name('alumni-pendidikan_hard_delete');
                Route::get('sampah', [AlumniPendidikanController::class, 'data_trash'])->name('alumni-pendidikan_data_trash');
                Route::post('by-alumni', [AlumniPendidikanController::class, 'get_data'])->name('alumni-pendidikan_by_alumni');
                Route::get('detail-alumni', [AlumniPendidikanController::class, 'detail_alumni'])->name('alumni-pendidikan_detail_by_alumni');
            });
            // Route::get('pekerjaan', [PekerjaanController::class, 'index'])->name('alumni-perkerjaan');
            Route::prefix('pekerjaan')->group(function () {
                Route::post('create', [PekerjaanController::class, 'store'])->name('alumni-pekerjaan_create');
                Route::put('update', [PekerjaanController::class, 'update'])->name('alumni-pekerjaan_update');
                Route::post('edit', [PekerjaanController::class, 'edit'])->name('alumni-pekerjaan_edit');
                Route::post('trash', [PekerjaanController::class, 'soft_delete'])->name('alumni-pekerjaan_delete');
                Route::post('detail-alumni', [PekerjaanController::class, 'detail_alumni'])->name('alumni-pekerjaan_detail_by_alumni');
            });
            Route::get('organisasi', [OrganisasiController::class, 'index'])->name('alumni-organisasi');
            Route::prefix('organisasi')->group(function () {
                Route::post('create', [OrganisasiController::class, 'store'])->name('alumni-organisasi_create');
                Route::put('update', [OrganisasiController::class, 'update'])->name('alumni-organisasi_update');
                Route::post('edit', [OrganisasiController::class, 'edit'])->name('alumni-organisasi_edit');
                Route::post('detail-alumni', [OrganisasiController::class, 'detail_by_alumni'])->name('alumni-organisasi_detail');
                Route::post('delete', [OrganisasiController::class, 'soft_delete'])->name('alumni-organisasi_delete');
            });

            // Route::get('sosial', [SosialController::class, 'index'])->name('alumni-sosial');
            Route::prefix('sosial')->group(function () {
                Route::post('create', [SosialController::class, 'store'])->name('alumni-sosial_create');
                Route::put('update', [SosialController::class, 'update'])->name('alumni-sosial_update');
                Route::post('detail', [SosialController::class, 'detail'])->name('alumni-sosial_detail');
                Route::post('edit', [SosialController::class, 'edit'])->name('alumni-sosial_edit');
                Route::post('delete', [SosialController::class, 'soft_delete'])->name('alumni-sosial_delete');
            });

            Route::get('kategori-blog', [KategoriBlogController::class, 'index'])->name('alumni-kategori_blog');
            Route::prefix('kategori-blog')->group(function () {
                Route::post('create', [KategoriBlogController::class, 'store'])->name('alumni-kat_blog_create');
                Route::put('update', [KategoriBlogController::class, 'update'])->name('alumni-kat_blog_update');
                Route::post('edit', [KategoriBlogController::class, 'edit'])->name('alumni-kategori_blog_edit');
                Route::post('trash', [KategoriBlogController::class, 'soft_delete'])->name('alumni-kategori_blog_delete');
                // Route::post('restore', [AlumniPendidikanController::class, 'restore'])->name('alumni-pendidikan_restore');
                // Route::post('delete', [AlumniPendidikanController::class, 'delete'])->name('alumni-pendidikan_hard_delete');
                // Route::get('sampah', [AlumniPendidikanController::class, 'data_trash'])->name('alumni-pendidikan_data_trash');
                Route::post('update_status', [KategoriBlogController::class, 'update_status'])->name('update_status-kategori_blog_alumni');
            });

            Route::get('kategori-galeri', [KategoriGaleriController::class, 'index'])->name('alumni-kategori_galeri');
            Route::prefix('kategori-galeri')->group(function () {
                Route::post('create', [KategoriGaleriController::class, 'store'])->name('alumni-kat_galeri_create');
                Route::put('update', [KategoriGaleriController::class, 'update'])->name('alumni-kat_galeri_update');
                Route::post('edit', [KategoriGaleriController::class, 'edit'])->name('alumni-kategori_galeri_edit');
                Route::post('trash', [KategoriGaleriController::class, 'soft_delete'])->name('alumni-kategori_galeri_delete');
                // Route::post('restore', [AlumniPendidikanController::class, 'restore'])->name('alumni-pendidikan_restore');
                // Route::post('delete', [AlumniPendidikanController::class, 'delete'])->name('alumni-pendidikan_hard_delete');
                // Route::get('sampah', [AlumniPendidikanController::class, 'data_trash'])->name('alumni-pendidikan_data_trash');
                Route::post('update_status', [KategoriGaleriController::class, 'update_status'])->name('update_status-kategori_galeri_alumni');
            });

            Route::get('galeri', [GaleriController::class, 'index'])->name('alumni-galeri');
            Route::prefix('galeri')->group(function () {
                Route::post('create', [GaleriController::class, 'store'])->name('alumni-galeri_create');
                Route::post('update', [GaleriController::class, 'update'])->name('alumni-galeri_update');
                Route::post('trash', [GaleriController::class, 'soft_delete'])->name('alumni-galeri_delete');
                Route::get('approve', [GaleriController::class, 'data_approve'])->name('alumni-galeri_data_approve');
                Route::post('change_status', [GaleriController::class, 'update_status'])->name('alumni-galeri_change_status');
            });

            Route::get('blog', [BlogController::class, 'index'])->name('alumni-blog');
            Route::prefix('blog')->group(function () {
                Route::get('create/new', [BlogController::class, 'create'])->name('alumni-blog_new_create');
                Route::post('create', [BlogController::class, 'store'])->name('alumni-blog_create');
                Route::get('by-aprove', [BlogController::class, 'blog_aprrove'])->name('alumni-blog_approve');
                Route::post('update', [BlogController::class, 'update'])->name('alumni-blog_update');
                Route::get('detail', [BlogController::class, 'detail'])->name('alumni-blog_detail');
                Route::post('edit', [BlogController::class, 'edit'])->name('alumni-blog_edit');
                Route::post('blog-detail', [BlogController::class, 'blog'])->name('alumni-data_blog_detail');
                Route::post('soft-delete', [BlogController::class, 'soft_delete'])->name('alumni-data_blog_soft_delete');
                Route::get('delete/{id}', [BlogController::class, 'soft_del'])->name('alumni-data_blog_delete');
                Route::post('search', [BlogController::class, 'search_blog'])->name('alumni-search_blog');
                Route::get('search', [BlogController::class, 'search'])->name('alumni-blog_search');
                Route::get('update-status/{id}', [BlogController::class, 'update_status'])->name('alumni-blog_update_status');
                Route::post('change_status', [BlogController::class, 'change_status'])->name('alumni-blog_change_status');
            });

            Route::get('blog-komentar', [KomentarController::class, 'index'])->name('alumni-blog_komentar');
            Route::prefix('blog-komentar')->group(function () {
                Route::post('create', [KomentarController::class, 'store'])->name('alumni-blog_komentar_create');
                // Route::get('detail/{id}', [BlogController::class, 'detail'])->name('alumni-blog_detail');
            });

            // Route::get('kategori-survey', [KategoriSurveyController::class, 'index'])->name('alumni-kategori_survey');
            Route::prefix('kategori-survey')->group(function () {
                Route::post('create', [KategoriSurveyController::class, 'store'])->name('alumni-kategori_survey_create');
                Route::put('update', [KategoriSurveyController::class, 'update'])->name('alumni-kategori_survey_update');
                Route::post('delete', [KategoriSurveyController::class, 'soft_delete'])->name('alumni-kategori_survey_soft-delete');
                Route::post('detail', [KategoriSurveyController::class, 'detail'])->name('alumni-kategori_survey_detail');
            });

            Route::prefix('select')->group(function () {
                Route::post('kategori', [SurveyController::class, 'select_kategori_survey'])->name('alumni-select_kategori');
                Route::get('kategori/{id}', [SurveyController::class, 'start_kategori'])->name('alumni-survey_by_kategori');
                Route::get('statistik/{id}', [SurveyController::class, 'statistik'])->name('alumni-statistik_survey');
            });

            Route::get('agenda', [AlumniAgendaController::class, 'index'])->name('alumni-agenda');
            Route::prefix('agenda')->group(function () {
                Route::get('detail-agenda', [AlumniAgendaController::class, 'get_detail'])->name('alumni-detail_lengkap');
                Route::post('get-agenda', [AlumniAgendaController::class, 'get_agenda'])->name('alumni-get_agenda');
                Route::post('create', [AlumniAgendaController::class, 'store'])->name('alumni-agenda_create');
                Route::post('update', [AlumniAgendaController::class, 'update'])->name('alumni-agenda_update');
                Route::post('soft-delete', [AlumniAgendaController::class, 'soft_delete'])->name('alumni-agenda_soft_delete');
                Route::get('pending', [AlumniAgendaController::class, 'pending_agenda'])->name('alumni-agenda_pending');
                Route::get('update-status/{id}', [AlumniAgendaController::class, 'update_status'])->name('alumni-agenda_update_status');
                Route::get('delete/{id}', [AlumniAgendaController::class, 'soft_del'])->name('alumni-agenda_delete');
                Route::post('edit', [AlumniAgendaController::class, 'get_by_id'])->name('alumni-agenda_get_by_id');
            });

            Route::get('survey', [SurveyController::class, 'index'])->name('alumni-survey');
            Route::prefix('survey')->group(function () {
                Route::post('simpan', [SurveyController::class, 'store'])->name('alumni-simpan_survey');
                Route::post('save-survey', [SurveyController::class, 'store_soal'])->name('alumni-simpan_soal_survey');
                Route::put('update-survey', [SurveyController::class, 'update_soal'])->name('alumni-update_soal_survey');
                Route::get('soft_delete/{id}', [SurveyController::class, 'soft_delete'])->name('alumni-delete_survey');
                Route::post('detail', [SurveyController::class, 'detail'])->name('alumni-detail_survey');
            });

            Route::prefix('ajax')->group(function () {
                Route::post('siswa/lihat_nilai', [AjaxController::class, 'siswa_lihat_nilai'])->name('pkl_nilai_siswa-lihat_nilai');
                Route::post('siswa/lihat_tugas', [AjaxController::class, 'siswa_lihat_tugas'])->name('pkl_nilai_siswa-lihat_tugas');
            });
        });


        //Program Absensi
        Route::prefix('absensi')->group(function () {
            Route::get('detail', [AbsensiProfileController::class, 'index'])->name('absensi-profile');
            Route::prefix('detail')->group(function () {
                Route::get('izin', [AbsensiProfileController::class, 'izin'])->name('absensi-profile_izin');
            });

            Route::get('siswa', [AbsensiSiswaController::class, 'index'])->name('absensi-siswa');
            Route::prefix('siswa')->group(function () {
                Route::post('create', [AbsensiSiswaController::class, 'store'])->name('absensi-siswa_create');
                Route::post('filter', [AbsensiSiswaController::class, 'get_siswa'])->name('absensi-filter_siswa');
            });
            Route::get('guru', [AbsensiGuruController::class, 'index'])->name('absensi-guru');
            Route::prefix('guru')->group(function () {
                Route::post('update_status', [AbsensiGuruController::class, 'update_status'])->name('absensi-guru_update_status');
            });







            Route::get('libur', [LiburController::class, 'index'])->name('absensi-libur');
            Route::prefix('libur')->group(function () {
                Route::post('create', [LiburController::class, 'store'])->name('absensi-libur_create');
                Route::put('update', [LiburController::class, 'update'])->name('absensi-libur_update');
                Route::post('edit', [LiburController::class, 'edit'])->name('absensi-libur_edit');
                Route::post('delete', [LiburController::class, 'soft_delete'])->name('absensi-libur_delete');
                Route::post('filter', [LiburController::class, 'filterBulan'])->name('absensi-libur_filter');
                Route::post('syncrone', [LiburController::class, 'syncrone_data'])->name('absensi-libur_syncrone');
                Route::post('update_status', [LiburController::class, 'update_status'])->name('absensi-libur_update_status');
            });

            Route::get('jadwal', [AbsensiJadwalController::class, 'index'])->name('absensi-jadwal');
            Route::prefix('jadwal')->group(function () {
                Route::post('create', [AbsensiJadwalController::class, 'store'])->name('absensi-jadwal_create');
                Route::put('update', [AbsensiJadwalController::class, 'update'])->name('absensi-jadwal_update');
                Route::post('edit', [AbsensiJadwalController::class, 'edit'])->name('absensi-jadwal_edit');
                Route::post('delete', [AbsensiJadwalController::class, 'soft_delete'])->name('absensi-jadwal_delete');
                Route::post('update_status', [AbsensiJadwalController::class, 'update_status'])->name('absensi-jadwal_update_status');
            });

            Route::get('izin', [IzinController::class, 'index'])->name('absensi-izin');
            Route::prefix('izin')->group(function () {
                Route::get('guru', [IzinController::class, 'guru'])->name('absensi-izin_guru');
                Route::post('create', [IzinController::class, 'store'])->name('absensi-izin_create');
                Route::put('update', [IzinController::class, 'update'])->name('absensi-izin_update');
                Route::post('filter', [IzinController::class, 'rekap_filter'])->name('absensi-izin_rekap_filter');
                Route::post('detail', [IzinController::class, 'edit'])->name('absensi-izin_detail');
                Route::post('delete', [IzinController::class, 'soft_delete'])->name('absensi-izin_delete');
                Route::post('show_siswa', [IzinController::class, 'izin_hari_ini'])->name('absensi-izin_hari_ini');
                Route::post('update_status', [IzinController::class, 'update_status'])->name('absensi-izin_update_status');
                Route::get('statisctic', [IzinController::class, 'statistic_izin'])->name('absensi-izin_sstatistic_siswa');
                Route::post('update_izin', [IzinController::class, 'update_izin'])->name('absensi-izin_update');
            });

            Route::get('hadir', [HadirController::class, 'index'])->name('absensi-hadir');
            Route::prefix('hadir')->group(function () {
                Route::post('create', [HadirController::class, 'store'])->name('absensi-hadir_create');
                Route::put('update', [HadirController::class, 'update'])->name('absensi-hadir_update');
                Route::post('filter_siswa', [HadirController::class, 'filter_siswa'])->name('absensi-get_filter_siswa');
                Route::get('siswa', [HadirController::class, 'siswa'])->name('absensi-hadir_siswa');
                Route::get('guru', [HadirController::class, 'guru'])->name('absensi-hadir_guru');
                Route::post('fil-siswa', [HadirController::class, 'fil_siswa'])->name('absensi-hadir_fil_siswa');
                Route::get('cetak-rekap', [HadirController::class, 'cetak_rakap'])->name('absensi-hadir_cetak_rekap_pdf');
                Route::get('cetak', [HadirController::class, 'cetak'])->name('absensi-hadir_print_pdf');
            });
        });

        //Program CBT
        Route::prefix('cbt')->group(function () {
            Route::get('siswa', [CbtSiswaController::class, 'index'])->name('cbt-siswa');
            Route::prefix('siswa')->group(function () {
                Route::post('simpan', [CbtSiswaController::class, 'store'])->name('cbt-siswa_create');
                Route::post('update', [CbtSiswaController::class, 'update'])->name('cbt-siswa_update');
                Route::post('filter', [CbtSiswaController::class, 'get_siswa'])->name('cbt-filter_siswa');
                Route::post('update_status', [CbtSiswaController::class, 'update_status'])->name('cbt-update_status_siswa');
            });
            //Data Guru CBT
            Route::get('guru', [CbtGuruController::class, 'index'])->name('cbt-guru');
            Route::prefix('guru')->group(function () {
                Route::post('create', [CbtGuruController::class, 'store'])->name('cbt-siswa_create');
                Route::post('filter', [CbtGuruController::class, 'get_siswa'])->name('cbt-filter_siswa');
                Route::post('delete', [CbtGuruController::class, 'delete'])->name('cbt-guru_delete');
                Route::post('update_status', [CbtGuruController::class, 'update_status'])->name('cbt-guru_update_status');
                Route::post('filter', [CbtGuruController::class, 'filter_name'])->name('cbt-guru_filter_name');
            });
            //Data mapel CBT
            Route::get('mapel', [CbtMapelController::class, 'index'])->name('cbt-mapel');
            Route::prefix('mapel')->group(function () {
                Route::post('create', [CbtMapelController::class, 'store'])->name('cbt-mapel_create');
                Route::put('update', [CbtMapelController::class, 'update'])->name('cbt-mapel_update');
                Route::post('delete', [CbtMapelController::class, 'delete'])->name('cbt-mapel_delete');
                Route::post('update_status', [CbtMapelController::class, 'update_status'])->name('cbt-mapel_update_status');
            });


            //Setting CBT
            Route::get('setting', [CbtSettingController::class, 'index'])->name('cbt-setting');
            Route::prefix('setting')->group(function () {
                Route::post('create', [CbtSettingController::class, 'store'])->name('cbt-setting_create');
                Route::put('update', [CbtSettingController::class, 'update'])->name('cbt-setting_update');
                Route::post('delete', [CbtSettingController::class, 'delete'])->name('cbt-setting_delete');
                Route::post('update_status', [CbtSettingController::class, 'update_status'])->name('cbt-setting_update_status');
            });
            // Admin CBT
            Route::get('admin', [CbtAdminController::class, 'index'])->name('cbt-admin');
            Route::prefix('admin')->group(function () {
                Route::post('create', [CbtAdminController::class, 'store'])->name('cbt-admin_create');
                Route::post('update', [CbtAdminController::class, 'update'])->name('cbt-admin_update');
                Route::post('detail', [CbtAdminController::class, 'detail'])->name('cbt-admin_detail');
                Route::post('delete', [CbtAdminController::class, 'delete'])->name('cbt-admin_delete');
                Route::post('update_status', [CbtAdminController::class, 'update_status'])->name('cbt-admin_update_status');
                Route::post('res_pass', [CbtAdminController::class, 'res_pass'])->name('reset_password_manual-admin_cbt');
            });

            Route::get('jenis', [CbtJenisController::class, 'index'])->name('cbt-jenis');
            Route::prefix('jenis')->group(function () {
                Route::post('create', [CbtJenisController::class, 'store'])->name('cbt-jenis_create');
                Route::put('update', [CbtJenisController::class, 'update'])->name('cbt-jenis_update');
                Route::post('detail', [CbtJenisController::class, 'detail'])->name('cbt-jenis_detail');
                Route::post('delete', [CbtJenisController::class, 'delete'])->name('cbt-jenis_delete');
            });

            Route::get('sesi', [SesiController::class, 'index'])->name('cbt-sesi');
            Route::prefix('sesi')->group(function () {
                Route::post('create', [SesiController::class, 'store'])->name('cbt-sesi_create');
                Route::put('update', [SesiController::class, 'update'])->name('cbt-sesi_update');
                Route::post('detail', [SesiController::class, 'detail'])->name('cbt-sesi_detail');
                Route::post('delete', [SesiController::class, 'delete'])->name('cbt-sesi_delete');
            });

            Route::get('ruang', [RuangController::class, 'index'])->name('cbt-ruang');
            Route::prefix('ruang')->group(function () {
                Route::post('create', [RuangController::class, 'store'])->name('cbt-ruang_create');
                Route::put('update', [RuangController::class, 'update'])->name('cbt-ruang_update');
                Route::post('detail', [RuangController::class, 'detail'])->name('cbt-ruang_detail');
                Route::post('delete', [RuangController::class, 'delete'])->name('cbt-ruang_delete');
            });

            Route::get('bank_soal', [BankSoalController::class, 'index'])->name('cbt-bank_soal');
            Route::prefix('bank_soal')->group(function () {
                Route::get('add', [BankSoalController::class, 'create'])->name('cbt-add_bank');
                Route::post('create', [BankSoalController::class, 'store'])->name('cbt-store_bank');
                Route::get('detail', [BankSoalController::class, 'detail'])->name('cbt-detail_bank');
                Route::post('by_guru', [BankSoalController::class, 'by_guru'])->name('cbt-bank_get_by_guru');
                // Route::post('detail', [BankSoalController::class, 'detail'])->name('cbt-ruang_detail');
                // Route::post('delete', [BankSoalController::class, 'delete'])->name('cbt-ruang_delete');
            });

            //Soal
            Route::get('soal', [SoalController::class, 'index'])->name('cbt-soal');
            Route::prefix('soal')->group(function () {
                Route::get('new', [SoalController::class, 'add'])->name('cbt-soal_new');
                Route::post('create', [SoalController::class, 'store'])->name('cbt-soal_create');
                Route::post('load_soal', [SoalController::class, 'tampil_soal'])->name('cbt-load_soal');
                Route::post('show', [SoalController::class, 'update_tampil'])->name('cbt-update_tampil');
            });

            Route::get('ruang_kelas', [KelasRuangController::class, 'index'])->name('cbt-kelas_ruang');
            Route::prefix('ruang_kelas')->group(function () {
                Route::post('create', [KelasRuangController::class, 'update'])->name('cbt-kelas_ruang_save');
                // Route::post('delete', [BankSoalController::class, 'delete'])->name('cbt-ruang_delete');
            });

            Route::get('jadwal', [CbtJadwalController::class, 'index'])->name('cbt-jadwal');
            Route::prefix('jadwal')->group(function () {
                Route::get('edit', [CbtJadwalController::class, 'edit'])->name('cbt-jadwal_edit');
                Route::get('add', [CbtJadwalController::class, 'add'])->name('cbt-jadwal_create');
                Route::post('create', [CbtJadwalController::class, 'store'])->name('cbt-jadwal_store');
            });

            Route::get('token', [TokenController::class, 'index'])->name('cbt-token');
            Route::prefix('token')->group(function () {
                Route::get('generate', [TokenController::class, 'generate'])->name('cbt-token_generate');
            });
        });



        Route::prefix('perpustakaan')->group(function () {
            //public
            Route::get('/', [HomePerpusController::class, 'index'])->name('home-perpus');

            Route::prefix('user')->group(function () {
                Route::get('profile', [UserPerpusController::class, 'index'])->name('profil-user-perpus');
                Route::get('peminjaman', [UserPerpusController::class, 'peminjaman'])->name('peminjaman-user-perpus');
                Route::get('denda', [UserPerpusController::class, 'denda'])->name('denda-user-perpus');
                Route::post('updated', [UserPerpusController::class, 'update_profile'])->name('update-perpus-user');
                Route::post('change-password', [UserPerpusController::class, 'update_password'])->name('change-perpus-password');
                //user
                Route::get('all', [UserPerpusController::class, 'getAll'])->name('user-perpus-all');
                Route::post('store', [UserPerpusController::class, 'store'])->name('user-perpus-store');
                Route::get('detail', [UserPerpusController::class, 'getOne'])->name('user-perpus-detail');
                Route::post('delete', [UserPerpusController::class, 'delete'])->name('user-perpus-delete');

                Route::get('details/{id}', [UserPerpusController::class, 'detail'])->name('user-perpus-details');
                Route::post('get_nama', [UserPerpusController::class, 'get_by_nama'])->name('user-perpus-by-nama');
                Route::post('get_many', [UserPerpusController::class, 'findMany'])->name('user-perpus-find-many');
                Route::post('print_many', [UserPerpusController::class, 'printMany'])->name('user-perpus-print-many');

                Route::get('extends/user', [UserPerpusController::class, 'extends'])->name('user-perpus-extends');
            });

            Route::prefix('admin')->group(function () {
                Route::get('profile', [AdminPerpusController::class, 'index'])->name('profil-admin-perpus');
                Route::get('peminjaman', [AdminPerpusController::class, 'peminjaman'])->name('peminjaman-admin-perpus');
                Route::get('denda', [AdminPerpusController::class, 'denda'])->name('denda-admin-perpus');
                Route::post('updated', [AdminPerpusController::class, 'update_profile'])->name('update-perpus-admin');
                Route::post('change-password', [AdminPerpusController::class, 'update_password'])->name('change-perpus-password');

                //admin
                Route::get('all', [AdminPerpusController::class, 'getAll'])->name('admin-perpus-all');
                Route::post('store', [AdminPerpusController::class, 'store'])->name('admin-perpus-store');
                Route::get('detail', [AdminPerpusController::class, 'getOne'])->name('admin-perpus-detail');
                Route::post('delete', [AdminPerpusController::class, 'delete'])->name('admin-perpus-delete');
            });

            Route::prefix('sirkulasi')->group(function () {
                Route::get('/', [PeminjamanPerpusController::class, 'sirkulasi'])->name('sirkulasi-main');
            });

            Route::prefix('peminjaman')->group(function () {
                Route::get('pengembalian', [PeminjamanPerpusController::class, 'pengembalian_page'])->name('pengembalian-page');

                Route::get('user', [PeminjamanPerpusController::class, 'get_peminjaman_user'])->name('peminjaman-user');

                Route::get('buku', [PeminjamanPerpusController::class, 'get_peminjaman_buku'])->name('peminjaman-buku');

                Route::post('pengembalian', [PeminjamanPerpusController::class, 'pengembalian'])->name('peminjaman-kembali');

                Route::get('/', [PeminjamanPerpusController::class, 'get_active'])->name('peminjaman-active');
                Route::post('/', [PeminjamanPerpusController::class, 'store'])->name('peminjaman-add');

                Route::get('baru', [PeminjamanPerpusController::class, 'add_new'])->name('peminjaman-baru');
                Route::get('return', [PeminjamanPerpusController::class, 'get_return'])->name('peminjaman-return');
                Route::get('extends', [PeminjamanPerpusController::class, 'extends'])->name('peminjaman-extends');
                // Route::post('user',[PeminjamanPerpusController::class,'cari_user_pinjam'])->name('peminjaman-add-user');
            });

            Route::prefix('pengadaan')->group(function () {
                Route::get('/', [PengadaanPerpusController::class, 'get_active'])->name('pengadaan-active');
                Route::get('/accepted', [PengadaanPerpusController::class, 'get_accepted'])->name('pengadaan-accepted');
                Route::get('/rejected', [PengadaanPerpusController::class, 'get_rejected'])->name('pengadaan-rejected');

                Route::post('/', [PengadaanPerpusController::class, 'store'])->name('pengadaan-add');
                Route::post('detail', [PengadaanPerpusController::class, 'get_one'])->name('pengadaan-detail');
                Route::post('store', [PengadaanPerpusController::class, 'store'])->name('pengadaan-store');
                Route::post('action', [PengadaanPerpusController::class, 'action'])->name('pengadaan-action');
                Route::post('delete', [PengadaanPerpusController::class, 'delete'])->name('pengadaan-delete');
            });

            Route::prefix('kartu')->group(function () {
                Route::get('/', [KartuController::class, 'index'])->name('kartu-index');
                Route::post('/store', [KartuController::class, 'store'])->name('kartu-store');
                Route::get('/detail', [KartuController::class, 'get_one'])->name('kartu-get-one');
            });

            Route::prefix('label')->group(function () {
                Route::get('/', [LabelController::class, 'index'])->name('label-index');
                Route::post('/store', [LabelController::class, 'store'])->name('label-store');
                Route::get('/detail', [LabelController::class, 'get_one'])->name('label-get-one');
            });

            Route::prefix('denda')->group(function () {
                Route::get('/', [DendaController::class, 'get_all'])->name('perpus-denda');
                Route::post('detail', [DendaController::class, 'get_one'])->name('denda-detail-perpus');
                Route::get('user', [DendaController::class, 'get_denda_user'])->name('denda-user');
            });

            Route::prefix('item')->group(function () {
                Route::post('store', [EskemplarController::class, 'store'])->name('item-store');

                Route::get('/book/{id}', [EskemplarController::class, 'get_by_book'])->name('item-by-book');

                Route::get('show', [EskemplarController::class, 'get_one'])->name('item-by-id');

                Route::post('find', [EskemplarController::class, 'find_many'])->name('item-find');

                Route::post('print', [EskemplarController::class, 'print_many'])->name('item-print');

                Route::post('print/items', [EskemplarController::class, 'print_items'])->name('item-prints');

                Route::post('/kode', [EskemplarController::class, 'get_by_kode'])->name('item-by-kode');

                Route::post('delete', [EskemplarController::class, 'delete'])->name('item-delete');
            });

            Route::prefix('master')->group(function () {
                Route::prefix('bahasa')->group(function () {
                    Route::get('/', [BahasaController::class, 'get_all'])->name('perpus-bahasa');
                    Route::post('/', [BahasaController::class, 'store'])->name('perpus-bahasa-store');
                    Route::post('delete', [BahasaController::class, 'delete'])->name('perpus-bahasa-delete');
                    Route::post('update', [BahasaController::class, 'update'])->name('perpus-bahasa-update');
                });

                Route::prefix('jenis')->group(function () {
                    Route::get('/', [JenisController::class, 'get_all'])->name('perpus-jenis');
                    Route::post('/', [JenisController::class, 'store'])->name('perpus-jenis-store');
                    Route::post('delete', [JenisController::class, 'delete'])->name('perpus-jenis-delete');
                    Route::post('update', [JenisController::class, 'update'])->name('perpus-jenis-update');
                });

                Route::prefix('gmd')->group(function () {
                    Route::get('/', [GmdController::class, 'get_all'])->name('perpus-gmd');
                    Route::post('/', [GmdController::class, 'store'])->name('perpus-gmd-store');
                    Route::post('delete', [GmdController::class, 'delete'])->name('perpus-gmd-delete');
                    Route::post('update', [GmdController::class, 'update'])->name('perpus-gmd-update');
                });

                Route::prefix('penerbit')->group(function () {
                    Route::get('/', [PenerbitController::class, 'get_all'])->name('perpus-penerbit');
                    Route::post('/', [PenerbitController::class, 'store'])->name('perpus-penerbit-store');
                    Route::post('delete', [PenerbitController::class, 'delete'])->name('perpus-penerbit-delete');
                    Route::post('update', [PenerbitController::class, 'update'])->name('perpus-penerbit-update');
                });

                Route::prefix('koleksi')->group(function () {
                    Route::get('/', [KoleksiController::class, 'get_all'])->name('perpus-koleksi');
                    Route::post('/', [KoleksiController::class, 'store'])->name('perpus-koleksi-store');
                    Route::post('delete', [KoleksiController::class, 'delete'])->name('perpus-koleksi-delete');
                    Route::post('update', [KoleksiController::class, 'update'])->name('perpus-koleksi-update');
                });

                Route::prefix('pengarang')->group(function () {
                    Route::get('/', [PengarangController::class, 'get_all'])->name('perpus-pengarang');
                    Route::post('/', [PengarangController::class, 'store'])->name('perpus-pengarang-store');
                    Route::post('delete', [PengarangController::class, 'delete'])->name('perpus-pengarang-delete');
                    Route::post('update', [PengarangController::class, 'update'])->name('perpus-pengarang-update');
                });

                Route::prefix('topik')->group(function () {
                    Route::get('/', [TopikController::class, 'get_all'])->name('perpus-topik');
                    Route::post('/', [TopikController::class, 'store'])->name('perpus-topik-store');
                    Route::post('delete', [TopikController::class, 'delete'])->name('perpus-topik-delete');
                    Route::post('update', [TopikController::class, 'update'])->name('perpus-topik-update');
                });

                Route::prefix('rak')->group(function () {
                    Route::get('/', [RakController::class, 'get_all'])->name('perpus-rak');
                    Route::post('/', [RakController::class, 'store'])->name('perpus-rak-store');
                    Route::post('delete', [RakController::class, 'delete'])->name('perpus-rak-delete');
                    Route::post('update', [RakController::class, 'update'])->name('perpus-rak-update');
                });

                Route::prefix('agen')->group(function () {
                    Route::get('/', [AgenController::class, 'get_all'])->name('perpus-agen');
                    Route::post('/', [AgenController::class, 'store'])->name('perpus-agen-store');
                    Route::post('delete', [AgenController::class, 'delete'])->name('perpus-agen-delete');
                    Route::post('update', [AgenController::class, 'update'])->name('perpus-agen-update');
                });
            });

            Route::prefix('laporan')->group(function () {
                Route::get('index', [LaporanPerpusController::class, 'index'])->name('laporan-index');
                Route::get('pustaka', [LaporanPerpusController::class, 'pustaka'])->name('laporan-pustaka');
                Route::get('pustaka/rak', [LaporanPerpusController::class, 'pustakaByRak'])->name('laporan-pustaka-rak');
                Route::get('pustaka/jenis', [LaporanPerpusController::class, 'pustakaByJenis'])->name('laporan-pustaka-jenis');
                Route::get('pusdig', [LaporanPerpusController::class, 'pusdig'])->name('laporan-pusdig');
                Route::get('pusdig/jenis', [LaporanPerpusController::class, 'pusdigByJenis'])->name('laporan-pusdig-jenis');
                Route::get('user', [LaporanPerpusController::class, 'user'])->name('laporan-user');
                Route::get('peminjaman', [LaporanPerpusController::class, 'peminjaman'])->name('laporan-peminjaman');
                Route::get('peminjaman/terlambat', [LaporanPerpusController::class, 'peminjamanTerlambat'])->name('laporan-peminjaman-terlambat');
                Route::get('pengadaan', [LaporanPerpusController::class, 'pengadaan'])->name('laporan-pengadaan');
                Route::get('denda', [LaporanPerpusController::class, 'denda'])->name('laporan-denda');
            });

            Route::prefix('dashboard')->group(function () {
                Route::get('peminjaman', [DashboardPerpusController::class, 'peminjaman'])->name('dashboard-peminjaman');
                Route::get('pengadaan', [DashboardPerpusController::class, 'pengadaan'])->name('dashboard-pengadaan');
                Route::get('user', [DashboardPerpusController::class, 'user'])->name('dashboard-user');
                Route::get('koleksi', [DashboardPerpusController::class, 'koleksi'])->name('dashboard-koleksi');
            });

            Route::prefix('pengumuman')->group(function () {
                Route::get('/', [PengumumanPerpusController::class, 'index'])->name('pengumuman-index');
                Route::get('detail', [PengumumanPerpusController::class, 'show'])->name('pengumuman-detail');
                Route::post('/', [PengumumanPerpusController::class, 'create'])->name('pengumuman-create');
                Route::post('delete', [PengumumanPerpusController::class, 'destroy'])->name('pengumuman-delete');
            });
        });
    });
});


//perpustakaan public
Route::prefix('perpustakaan')->group(function () {

    Route::prefix('public')->group(function () {
        Route::get('/', [HomePerpusController::class, 'public_area'])->name('perpus-public');
        Route::get('membership', [HomePerpusController::class, 'membershipInfo'])->name('perpus-member');
        Route::get('tentang', [HomePerpusController::class, 'tentang'])->name('perpus-about');
        Route::get('buku/latest', [HomePerpusController::class, 'buku_latest'])->name('perpus-public-latest');
        Route::get('buku/fav', [HomePerpusController::class, 'buku_fav'])->name('perpus-public-fav');
        Route::get('pusdig', [HomePerpusController::class, 'pusdig_all'])->name('perpus-public-pusdig');
    });


    Route::prefix('buku')->group(function () {
        Route::post('store', [BukuController::class, 'store'])->name('buku-store');
        Route::get('detail/{id}', [BukuController::class, 'detail_buku'])->name('detail-buku');
        Route::post('ajax', [BukuController::class, 'ajax_detail'])->name('ajax-detail-buku');
        Route::get('admin', [BukuController::class, 'admin_get_buku'])->name('buku-admin');

        //public
        Route::get('search/judul', [BukuController::class, 'search_judul'])->name('search-judul-buku');
        Route::get('search', [BukuController::class, 'search_advance'])->name('search-advance-buku');
    });

    Route::prefix('pusdig')->group(function () {
        Route::post('store', [PustakaDigitalController::class, 'store'])->name('pusdig-store');
        Route::get('detail/{id}', [PustakaDigitalController::class, 'detail_pusdig'])->name('detail-pusdig');
        Route::post('ajax', [PustakaDigitalController::class, 'ajax_detail'])->name('ajax-detail-pusdig');
        Route::get('search/judul', [PustakaDigitalController::class, 'search_judul'])->name('search-judul-pusdig');
        Route::get('admin', [PustakaDigitalController::class, 'admin_get_pusdig'])->name('pusdig-admin');
        Route::get('read/{id}', [PustakaDigitalController::class, 'read_pdf'])->name('read-pusdig');
    });


    //public
    Route::get('pengarang/detail/{id}', [PengarangController::class, 'get_detail'])->name('perpus-pengarang-detail');
    Route::get('topik/detail/{id}', [TopikController::class, 'get_detail'])->name('perpus-topik-detail');
});



//ppdb
Route::prefix('ppdb')->group(function () {

    //route ppdb
    Route::get('/', function () {
        return redirect()->route('ppdb-public');
    });

    // auth login redirect

    Route::get('access', [PpdbController::class, 'access'])->name('access');

    //protect page with middleware

    Route::middleware(['check.api.token'])->group(function () {
        //group halaman admin ppdb
        Route::prefix('admin')->group(function () {
            //page dashboard
            Route::get('/', [PpdbController::class, 'index'])->name('dashboard-ppdb');
            Route::get('dashboard', [PpdbController::class, 'index'])->name('dashboard-ppdb');
            Route::get('profil', [PpdbController::class, 'get_profil'])->name('profil');
            //page akun
            Route::prefix('akun')->group(function () {
                Route::get('pages_admin', [AdminUserPpdb::class, 'index'])->name('administrator');
                Route::get('datasource', [AdminUserPpdb::class, 'ajax_data'])->name('source_ajax');
                Route::get('create-admin', [AdminUserPpdb::class, 'create'])->name('create-admin');
                Route::get('detail-admin/{id}', [AdminUserPpdb::class, 'show'])->name('detail-admin');
                Route::get('edit-admin/{id}', [AdminUserPpdb::class, 'edit'])->name('edit-admin');
                Route::post('save-admin', [AdminUserPpdb::class, 'store'])->name('save-admin');
                Route::post('update-admin/{id}', [AdminUserPpdb::class, 'update'])->name('update-admin');
                Route::delete('delete-admin/{id}', [AdminUserPpdb::class, 'destroy'])->name('delete-admin');
                Route::get('pages_user', [PesertaUserPpdb::class, 'index'])->name('peserta');
            });
            //page tata tertib
            Route::prefix('rule')->group(function () {
            });
            //page download
            Route::prefix('download')->group(function () {
                Route::prefix('brosur')->group(function () {
                    Route::get('/', [BrosurController::class, 'index'])->name('brosur-ppdb');
                    Route::get('tambah-brosur', [BrosurController::class, 'create'])->name('brosur-add');
                    Route::post('simpan-brosur', [BrosurController::class, 'store'])->name('brosur-simpan');
                    Route::post('update-brosur/{id}', [BrosurController::class, 'update'])->name('brosur-update');
                    Route::get('datasource', [BrosurController::class, 'ajax_data'])->name('data-brosur');
                    Route::get('edit-brosur/{id}', [BrosurController::class, 'edit'])->name('brosur-edit');
                    Route::get('detail-brosur/{id}', [BrosurController::class, 'show'])->name('info-brosur');
                    Route::delete('remove-brosur/{id}', [BrosurController::class, 'destroy'])->name('delete-brosur');
                    Route::delete('remove-force/{id}', [BrosurController::class, 'deleteForce'])->name('remove-permanent-brosur');
                    Route::get('trash-brosur', [BrosurController::class, 'ajaxtrash'])->name('data-trash-brosur');
                    Route::patch('restore/{id}', [BrosurController::class, 'restore'])->name('restore-brosur');
                });
                Route::prefix('sample')->group(function () {
                    Route::get('/', [SampledocController::class, 'index'])->name('sample-doc');
                    Route::get('tambah-sample', [SampledocController::class, 'create'])->name('sample-add');
                    Route::get('datasource', [SampledocController::class, 'ajax_data'])->name('data-sample');
                    Route::post('sample-simpan', [SampledocController::class, 'store'])->name('sample-simpan');
                    Route::get('sample-doc/{id}', [SampledocController::class, 'show'])->name('info-sample');
                    Route::get('edit-doc/{id}', [SampledocController::class, 'show'])->name('sample-edit');
                    Route::post('sample-update/{id}', [SampledocController::class, 'update'])->name('sample-update');
                    Route::delete('remove-sample/{id}', [SampledocController::class, 'destroy'])->name('delete-sample');
                    Route::delete('remove-force/{id}', [SampledocController::class, 'deleteForce'])->name('remove-permanent-sample');
                    Route::get('trash-sample', [SampledocController::class, 'ajaxtrash'])->name('data-trash-sample');
                    Route::patch('restore/{id}', [SampledocController::class, 'restore'])->name('restore-sample');
                });
            });

            //page peserta
            Route::prefix('peserta')->group(function () {
                Route::prefix('dokumen')->group(function () {
                    Route::get('/', [DokumenSupController::class, 'index'])->name('doc-support');
                    Route::get('datasource', [DokumenSupController::class, 'ajax_data'])->name('data-doc');
                    Route::get('datasource2', [DokumenSupController::class, 'ajax_data_peserta'])->name('data-peserta');
                    Route::get('add-doc', [DokumenSupController::class, 'create'])->name('doc-add');
                    Route::post('save-doc', [DokumenSupController::class, 'store'])->name('doc-save');
                    Route::delete('delete-dokumen/{id}', [DokumenSupController::class, 'destroy'])->name('delete-dokumen');
                    Route::delete('delete-force-dokumen/{id}', [DokumenSupController::class, 'deleteForce'])->name('delete-permanent-dokumen');
                    Route::get('detail-dokumen/{id}', [DokumenSupController::class, 'show'])->name('detail-dokumen');
                    Route::get('edit-dokumen/{id}', [DokumenSupController::class, 'edit'])->name('edit-dokumen');
                    Route::post('update-doc/{id}', [DokumenSupController::class, 'update'])->name('doc-update');
                });
                Route::prefix('pendaftar')->group(function () {
                    Route::get('/', [PendaftarController::class, 'index'])->name('akun-pendaftar');
                    Route::get('/register-ulang', [PendaftarController::class, 'register_ulang_peserta'])->name('register-ulang');
                    Route::get('peserta_diterima', [PendaftarController::class, 'peserta_diterima'])->name('peserta_diterima');
                    Route::get('peserta_ditolak', [PendaftarController::class, 'peserta_ditolak'])->name('peserta_ditolak');
                    Route::get('data_source_ul', [PendaftarController::class, 'ajax_data_register_ul'])->name('data-register-peserta');
                    Route::get('datasource', [PendaftarController::class, 'ajax_data'])->name('data-pesertav');
                    Route::get('datasource_terima', [PendaftarController::class, 'ajax_data_peserta_diterima'])->name('data-peserta_diterima');
                    Route::get('datasource_tolak', [PendaftarController::class, 'ajax_data_peserta_ditolak'])->name('data-peserta_ditolak');
                    Route::get('add', [PendaftarController::class, 'create'])->name('add-form');
                    Route::post('save', [PendaftarController::class, 'store'])->name('add-save');
                    Route::get('info/{id}', [PendaftarController::class, 'show'])->name('detail-pendaftar');
                    Route::get('edit/{id}', [PendaftarController::class, 'edit'])->name('edit-pendaftar');
                    Route::post('ubah/{id}', [PendaftarController::class, 'update'])->name('update-pendaftar');
                    Route::post('keputusan/{id}', [PendaftarController::class, 'update_status'])->name('update-pendaftar-status');
                    Route::delete('delete-pendaftar/{id}', [PendaftarController::class, 'destroy'])->name('delete-pendaftar');
                    Route::delete('delete-force-pendaftar/{id}', [PendaftarController::class, 'deleteForce'])->name('delete-permanet-pendaftaran');
                    Route::patch('restore/{id}', [PendaftarController::class, 'restore'])->name('restore-pendaftaran');
                    Route::get('file_peserta/{id}', [PendaftarController::class, 'getfile_peserta'])->name('file-peserta');
                    Route::get('force_downloadx/{id}/{user_id}/{namefile}', [PendaftarController::class, 'getdownload'])->where('id', '(.*)')->name('force_download_peserta');
                    Route::get('update-tolak-peserta/{id}', [PendaftarController::class, 'update_status_tolak'])->name('tolak-peserta');
                    Route::get('trash/sekolah', [PendaftarController::class, 'ajaxtrash'])->name('trash-peserta');
                    Route::get('info-detail-register/{id}', [PendaftarController::class, 'showdetail'])->name('detail-pendaftar-regiter');
                    Route::get('add-register-peserta', [PendaftarController::class, 'form_register'])->name('register-peserta');
                    Route::post('save-register2', [PendaftarController::class, 'store_register'])->name('save-register2');
                    Route::get('getfile-peserta/{id}', [PendaftarController::class, 'filepeserta'])->name('getfile-peserta');
                });
            });

            //page informasi
            Route::prefix('informasi')->group(function () {
                Route::prefix('halaman')->group(function () {
                    Route::get('banner', [BannerController::class, 'index'])->name('informasi-banner');
                    Route::post('simpan-banner', [BannerController::class, 'store'])->name('banner-simpan');
                    Route::get('alur', [RulesController::class, 'index'])->name('informasi-alur');
                    Route::post('simpan-alur', [RulesController::class, 'store'])->name('alur-simpan');
                    Route::get('syarat', [SyaratController::class, 'index'])->name('informasi-syarat');
                    Route::post('simpan-syarat', [SyaratController::class, 'store'])->name('syarat-simpan');
                    Route::get('panduan', [PanduanController::class, 'index'])->name('informasi-panduan');
                    Route::post('simpan-panduan', [PanduanController::class, 'store'])->name('panduan-simpan');
                    Route::get('faq', [FaqController::class, 'index'])->name('informasi-faq');
                    Route::post('simpan-faq', [FaqController::class, 'store'])->name('faq-simpan');
                });

                Route::prefix('jadwal-kegiatan')->group(function () {
                    Route::get('/', [JadwalController::class, 'index'])->name('informasi-jadwal');
                    Route::get('all-status', [JadwalController::class, 'ajax_status'])->name('jadwal-all-status');
                    Route::get('status-tidak-aktif', [JadwalController::class, 'ajax_status_hidden'])->name('status-hidden');
                    Route::get('tambah-jadwal', [JadwalController::class, 'create'])->name('jadwal-tambah');
                    Route::post('simpan-jadwal', [JadwalController::class, 'store'])->name('jadwal-simpan');
                    Route::get('datasource', [JadwalController::class, 'ajax_data'])->name('data-ajax');
                    Route::get('detail-info/{id}', [JadwalController::class, 'show'])->name('info-detail');
                    Route::get('edit-info/{id}', [JadwalController::class, 'edit'])->name('edit-detail');
                    Route::put('update-info/{id}', [JadwalController::class, 'update'])->name('update-detail');
                    Route::delete('remove-info/{id}', [JadwalController::class, 'destroy'])->name('remove-detail');
                    Route::get('info-trash', [JadwalController::class, 'ajaxtrash'])->name('trash-info');
                    Route::patch('restore/{id}', [JadwalController::class, 'restore'])->name('restore-info');
                    Route::delete('delete-permanent/{id}', [JadwalController::class, 'deleteForce'])->name('delete-permanen-info');
                });

                Route::prefix('pengumuman')->group(function () {
                    Route::get('/', [NoticeController::class, 'index'])->name('informasi-pengumuman');
                    Route::get('tambah', [NoticeController::class, 'create'])->name('tambah-pengumuman');
                    Route::post('simpan', [NoticeController::class, 'store'])->name('pengumuman-simpan');
                    Route::get('datasource', [NoticeController::class, 'ajax_data'])->name('feeds');
                    Route::get('detail/{id}', [NoticeController::class, 'show'])->name('detail-feed');
                    Route::get('edit/{id}', [NoticeController::class, 'edit'])->name('edit-feed');
                    Route::post('update/{id}', [NoticeController::class, 'update'])->name('update-feed');
                    Route::delete('remove-info-feed/{id}', [NoticeController::class, 'destroy'])->name('remove-feed');
                    Route::get('info-trash', [NoticeController::class, 'ajaxtrash'])->name('trash-info-feed');
                    Route::delete('delete-permanent/{id}', [NoticeController::class, 'deleteForce'])->name('delete-permanen-info-feed');
                    Route::patch('restore/{id}', [NoticeController::class, 'restore'])->name('restore-feed');
                });
            });

            //page profil
            Route::prefix('profil')->group(function () {
                Route::get('ubah_profil', [ProfilAdminController::class, 'index'])->name('ubah_profil_admin');
                Route::post('simpan_profil', [ProfilAdminController::class, 'store'])->name('profil_simpan_admin');
                Route::get('ubah_password', [ProfilAdminController::class, 'form_password'])->name('ubah_password_admin');
                Route::post('simpan_password', [ProfilAdminController::class, 'update_password'])->name('password_simpan_admin');
            });

            //page setting
            Route::prefix('setting')->group(function () {
                Route::prefix('ppdb')->group(function () {
                    Route::get('/', [SettingPpdbController::class, 'index'])->name('setting-ppdb');
                    Route::post('simpan-ppdb', [SettingPpdbController::class, 'store'])->name('simpan-ppdb');
                    Route::get('sambutan', [SettingPpdbController::class, 'sambutan'])->name('setting-sambutan');
                    Route::post('simpan-sambutan', [SettingPpdbController::class, 'store_sambutan'])->name('simpan-pengaturan-sambutan');
                });
                Route::prefix('pembayaran')->group(function () {
                    Route::get('/', [SettingPpdbController::class, 'pembayaran'])->name('setting-pembayaran');
                    Route::post('simpan-pembayaran', [SettingPpdbController::class, 'store_pembayaran'])->name('simpan-pengaturan-pembayaran');
                });
                Route::prefix('form')->group(function () {
                    Route::get('/', [SettingPpdbController::class, 'form'])->name('setting-form');
                    Route::get('setting_kartu', [SettingPpdbController::class, 'form_card_'])->name('setting-form-kartu');
                    Route::get('preview-form', [SettingPpdbController::class, 'show_form_setting'])->name('preview-setting-form');
                    Route::post('store_form', [SettingPpdbController::class, 'store_form'])->name('save-setting-form');
                    Route::get('custom_form', [SettingPpdbController::class, 'customform'])->name('custom-form');
                    Route::get('type-form', [SettingPpdbController::class, 'jenis_form'])->name('type-form');
                    Route::get('datasource_custom_form', [SettingPpdbController::class, 'ajax_data_custom_form'])->name('ajax-form-custom');
                    Route::get('datasource_type_form', [SettingPpdbController::class, 'ajax_data_type_form'])->name('ajax-form-type');
                    Route::get('form-new', [SettingPpdbController::class, 'Create_customform'])->name('custom-new-form');
                    Route::post('save-custom', [SettingPpdbController::class, 'store_create_form'])->name('add-save-custom');
                    Route::get('detail-info-form/{id}', [SettingPpdbController::class, 'showform'])->name('detail-form-custom');
                    Route::get('edit-info-form/{id}', [SettingPpdbController::class, 'showformEdit'])->name('edit-form-custom');
                    Route::put('update-custom/{id}', [SettingPpdbController::class, 'update_customform'])->name('update-custom-form');
                    Route::get('create-new-type-form', [SettingPpdbController::class, 'Create_customformtype'])->name('create-type-form');
                    Route::post('save-type-form', [SettingPpdbController::class, 'store_create_formtype'])->name('save-type-form');
                    Route::get('detail-info-type-form/{id}', [SettingPpdbController::class, 'showformtype'])->name('detail-info-typeform');
                    Route::get('edit-type-form/{id}', [SettingPpdbController::class, 'showformtypeEdit'])->name('edit-type-form');
                    Route::put('update-type-form/{id}', [SettingPpdbController::class, 'update_create_formtype'])->name('update-form-type');
                    Route::get('form-aktif', [SettingPpdbController::class, 'form_aktif'])->name('setting-form-aktif');
                    Route::post('update-urutan-form', [SettingPpdbController::class, 'update_urutanform'])->name('update_urutanform');
                    Route::get('kartu-sekolah', [SettingPpdbController::class, 'get_setting_kartu'])->name('kartu-setting');
                    Route::post('update-status-form', [SettingPpdbController::class, 'update_statusform'])->name('update_statusform');
                });

                Route::prefix('template')->group(function () {
                    Route::prefix('kartu')->group(function () {
                        Route::get('/', [SettingPpdbController::class, 'template_kartu'])->name('template-kartu');
                        Route::post('update-template-kartu', [SettingPpdbController::class, 'update_template'])->name('update-template-kartu');
                        Route::get('preview-card', [SettingPpdbController::class, 'preview_cardx'])->name('preview-kartu');
                    });
                    Route::prefix('surat')->group(function () {
                        Route::get('/', [SettingPpdbController::class, 'template_surat'])->name('template-surat');
                        Route::post('update-template-surat', [SettingPpdbController::class, 'update_template'])->name('update-template-surat');
                        Route::get('preview-form', [SettingPpdbController::class, 'preview_form'])->name('preview-form');
                        Route::get('preview-surat', [SettingPpdbController::class, 'preview_pengumuman'])->name('preview_pengumuman');
                    });
                });
            });

            //page pesan
            Route::prefix('pesan')->group(function () {
                Route::get('/', [PesanAdminPpdbContoller::class, 'index'])->name('pesan-admin');
                Route::get('datasource', [PesanAdminPpdbContoller::class, 'ajax_data'])->name('source_ajax_pesan_admin');
                Route::get('create-message-admin', [PesanAdminPpdbContoller::class, 'create'])->name('create-message-admin');
                Route::post('send-message-peserta', [PesanAdminPpdbContoller::class, 'store'])->name('send-message-peserta');
                Route::get('info-pesan-detail/{id}', [PesanAdminPpdbContoller::class, 'show'])->name('detail-pesan-admin');
                Route::get('edit-pesan-admin/{id}', [PesanAdminPpdbContoller::class, 'edit'])->name('edit-pesan-admin');
                Route::post('update/{id}', [PesanAdminPpdbContoller::class, 'update'])->name('update-pesan-admin');
                Route::delete('delete-pesan/{id}', [PesanAdminPpdbContoller::class, 'destroy'])->name('delete-pesan-admin');
                Route::post('balas-pesan/{id}', [PesanAdminPpdbContoller::class, 'balas_pesan'])->name('balas-pesan-admin');
                Route::get('trash-pesan-admin', [PesanAdminPpdbContoller::class, 'ajaxtrash'])->name('trash-pesan-admin');
                Route::patch('restore-pesan-admin/{id}', [PesanAdminPpdbContoller::class, 'restore'])->name('restore-pesan-admin');
                Route::delete('delete-permanent-pesan-admin/{id}', [PesanAdminPpdbContoller::class, 'forceDelete'])->name('delete-pesan-permanent-admin');
                Route::post('tutup-pesan/{id}', [PesanAdminPpdbContoller::class, 'tutup_pesan'])->name('tutup-pesan-admin');
            });

            //page pembayaran
            Route::prefix('pembayaran')->group(function () {
                Route::prefix('pending')->group(function () {
                    Route::get('/', [PaymentController::class, 'index'])->name('payment-pending');
                    Route::get('confirm', [PaymentController::class, 'confirm'])->name('payment-confirm');
                    Route::get('paid', [PaymentController::class, 'paid'])->name('payment-paid');
                    Route::get('reject', [PaymentController::class, 'reject'])->name('payment-reject');
                    Route::get('data-source-pending', [PaymentController::class, 'ajax_data_payment_pending'])->name('datasource-pending');
                    Route::get('data-source-confirm', [PaymentController::class, 'ajax_data_payment_confirm'])->name('datasource-confirm');
                    Route::get('data-source-paid', [PaymentController::class, 'ajax_data_payment_paid'])->name('datasource-paid');
                    Route::get('data-source-reject', [PaymentController::class, 'ajax_data_payment_reject'])->name('datasource-reject');
                    Route::get('detail-payment/{id}', [PaymentController::class, 'show'])->name('detail-payment');
                    Route::get('post-download/{id}', [PaymentController::class, 'getdownload'])->where('id', '(.*)')->name('force_download_bukti');
                    Route::get('edit-status-payment/{id}', [PaymentController::class, 'edit'])->name('edit-payment');
                    Route::put('update-pembayaran/{id}', [PaymentController::class, 'update'])->name('update-pembayaran');
                });
            });

            //page statistik
            Route::prefix('analytic')->group(function () {
                Route::get('datasoure-payment', [PpdbController::class, 'ajax_data_payment_pending'])->name('payment-status');
                Route::get('data-pesan', [PpdbController::class, 'ajax_data_pesan'])->name('data-pesan');
                Route::get('data-analystic', [PpdbController::class, 'ajax_analytic'])->name('data-analystic');
                Route::get('detail-pembayaran/{id}', [PpdbController::class, 'detail_pembayaran'])->name('detail_pembayaran');
                Route::get('datasource-pembayaran/{id}', [PpdbController::class, 'show_pembayaran'])->name('show_pembayaran');
            });
        });

        //group halaman peserta ppdb
        Route::prefix('user')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('user-ppdb');
            Route::get('dashboard', [UserController::class, 'index'])->name('user-ppdb');
            //page profil
            Route::prefix('profil')->group(function () {
                Route::get('ubah_profil', [ProfilUserController::class, 'index'])->name('ubah_profil_user');
                Route::post('simpan_profil', [ProfilUserController::class, 'store'])->name('profil_simpan_user');
                Route::get('ubah_password', [ProfilUserController::class, 'form_password'])->name('ubah_password_user');
                Route::post('simpan_password', [ProfilUserController::class, 'update_password'])->name('password_simpan_user');
            });

            Route::prefix('pengumuman')->group(function () {
                Route::get('/', [UserController::class, 'pengumuman'])->name('user-pengumuman');
                Route::put('views/{id}', [UserController::class, 'update_view'])->name('view-pengumuman-user');
                Route::get('feed/{id}/{slug}', [UserController::class, 'detail_feed'])->name('detail-info-feed-user');
            });

            Route::prefix('informasi')->group(function () {
                Route::get('alur-pendaftaran', [UserController::class, 'alur'])->name('alur-pendaftaran-user');
                Route::get('syarat-pendaftaran', [UserController::class, 'syarat'])->name('syarat-pendaftaran-user');
                Route::get('panduan-pendaftaran', [UserController::class, 'panduan'])->name('panduan-pendaftaran-user');
                Route::get('jadwal-pendaftaran', [UserController::class, 'jadwal'])->name('jadwal-pendaftaran-user');
                Route::get('ajax-jadwal', [UserController::class, 'ajax_data_jadwal'])->name('jadwal-kegiatan-user');
                Route::get('faq', [UserController::class, 'faq'])->name('faq-user');
            });

            Route::prefix('download')->group(function () {
                Route::get('file', [UserController::class, 'DownloadFile'])->name('user-download-file');
                Route::get('brosur', [UserController::class, 'DownloadBrosur'])->name('user-download-brosur');
                Route::get('ajax_file', [UserController::class, 'ajax_file_download'])->name('file-download-user');
                Route::get('ajax_brosur', [UserController::class, 'ajax_brosur_download'])->name('brosur-download-user');
                Route::get('post-download/{id}', [UserController::class, 'getdownload'])->where('id', '(.*)')->name('force_download-user');
            });

            Route::prefix('pesan')->group(function () {
                Route::get('/', [PesanUserPpdbContoller::class, 'index'])->name('pesan-user');
                Route::get('datasource', [PesanUserPpdbContoller::class, 'ajax_data'])->name('source_ajax_pesan_user');
                Route::get('info-pesan-detail/{id}', [PesanUserPpdbContoller::class, 'show'])->name('detail-pesan-peserta');
                Route::get('create-message-peserta', [PesanUserPpdbContoller::class, 'create'])->name('create-message-peserta');
                Route::post('send-message-peserta', [PesanUserPpdbContoller::class, 'store'])->name('send-message-admin');
                Route::get('edit-pesan-peserta/{id}', [PesanUserPpdbContoller::class, 'edit'])->name('edit-pesan-peserta');
                Route::post('update/{id}', [PesanUserPpdbContoller::class, 'update'])->name('update-pesan-peserta');
                Route::delete('delete-pesan/{id}', [PesanUserPpdbContoller::class, 'destroy'])->name('delete-pesan-peserta');
                Route::post('balas-pesan/{id}', [PesanUserPpdbContoller::class, 'balas_pesan'])->name('balas-pesan-peserta');
                Route::get('trash-pesan-peserta', [PesanUserPpdbContoller::class, 'ajaxtrash'])->name('trash-pesan-peserta');
                Route::patch('restore-pesan-peserta/{id}', [PesanUserPpdbContoller::class, 'restore'])->name('restore-pesan-peserta');
                Route::delete('delete-permanent-pesan-peserta/{id}', [PesanUserPpdbContoller::class, 'forceDelete'])->name('delete-pesan-permanent-peserta');
                Route::get('response-pesan-detail/{id}', [PesanUserPpdbContoller::class, 'from_message_admin'])->name('response-balasan-admin');
                Route::post('tutup-pesan/{id}', [PesanUserPpdbContoller::class, 'tutup_pesan'])->name('tutup-pesan-user');
            });

            //page konfirmasi pembayaran
            Route::prefix('konfirmasi-pembayaran')->group(function () {
                Route::get('/', [UserController::class, 'konfirmasi_pembayaran'])->name('konfirmasi-pembayaran');
                Route::post('post-konfirm-pembayaran', [UserController::class, 'store'])->name('send-konfirmasi');
            });
            // page history pembayaran
            Route::prefix('history-pembayaran')->group(function () {
                Route::get('/', [UserController::class, 'history_payment'])->name('history-pembayaran');
                Route::get('download-invoice', [UserController::class, 'invoice_pembayaran'])->name('invoice_pembayaran');
            });

            // page form pendaftaran
            Route::prefix('formulir-pendaftaran')->group(function () {
                Route::get('/', [UserController::class, 'form_register'])->name('formulir-pendaftaran');
                Route::post('save-register', [UserController::class, 'store_register'])->name('save-register');
            });

            //page print preview
            Route::prefix('print-preview')->group(function () {
                Route::get('print-card', [UserController::class, 'print_card_printview'])->name('print-card-user');
                Route::get('print-form', [UserController::class, 'print_form_printview'])->name('print-form-user');
                Route::get('print-pengumuman', [UserController::class, 'print_pengumuman'])->name('print_pengumuman-user');
            });

            Route::prefix('print')->group(function () {
                Route::get('print-form', [UserController::class, 'print_form_p'])->name('printform');
                Route::get('print-kartu-peserta', [UserController::class, 'print_kartu_peserta'])->name('print_kartu_peserta');
                Route::get('print-pengumuman', [UserController::class, 'print_pengumuman_cetak'])->name('print_pengumuman_cetak');
            });

            Route::prefix('upload-document')->group(function () {
                Route::get('/', [UserController::class, 'uploadfile'])->name('upload-file');
                Route::get('datasource', [UserController::class, 'ajax_data_filedoc'])->name('ajax-file-user');
                Route::get('doc-add-user', [UserController::class, 'create_file_upload'])->name('doc-add-user');
                Route::post('doc-save-user', [UserController::class, 'storeUploadfile'])->name('doc-save-user');
                Route::get('detail-dokumen-user/{id}', [UserController::class, 'showfile'])->name('detail-dokumen-user');
                Route::get('edit-dokumen-user/{id}', [UserController::class, 'editfile'])->name('edit-dokumen-user');
                Route::post('doc-update-user/{id}', [UserController::class, 'updatefile'])->name('doc-update-user');
                Route::delete('delete-dokumen-user/{id}', [UserController::class, 'destroyfile'])->name('delete-dokumen-user');
            });

            Route::prefix('notif')->group(function () {
                Route::get('pembayaran', [UserController::class, 'notif_pembayaran'])->name('notif_pembayaran');
            });

            Route::prefix('export')->group(function () {
                Route::get('export-file-excel', [PendaftarController::class, 'export_file'])->name('export_file');
            });
        });
    });

    //Route public ppdb
    Route::prefix('public')->group(function () {
        Route::get('/', [PublicController::class, 'index'])->name('ppdb-public');
        Route::get('faq', [PublicController::class, 'faq'])->name('ppdb-faq');
        Route::get('rules_ppdb', [PublicController::class, 'rule_ppdb'])->name('ppdb-rule-ppdb');
        Route::get('syarat_ppdb', [PublicController::class, 'syarat_ppdb'])->name('ppdb-syarat-ppdb');
        Route::get('panduan_ppdb', [PublicController::class, 'panduan_ppdb'])->name('ppdb-panduan-ppdb');
        Route::get('kegiatan_ppdb', [PublicController::class, 'kegiatan_ppdb'])->name('ppdb-kegiatan-ppdb');
        Route::get('form_ppdb', [PublicController::class, 'form_ppdb'])->name('form-ppdb');
        Route::post('register', [PublicController::class, 'store'])->name('daftar');
        Route::get('ajax-jadwal', [PublicController::class, 'ajax_data_jadwal'])->name('jadwal-kegiatan');

        Route::prefix('pengumuman')->group(function () {
            Route::get('/', [PublicController::class, 'pengumuman'])->name('ppdb-pengumuman');
            Route::put('views/{id}', [PublicController::class, 'update_view'])->name('view-pengumuman');
            Route::get('feed/{id}/{slug}', [PublicController::class, 'detail_feed'])->name('detail-info-feed');
        });

        Route::prefix('download')->group(function () {
            Route::get('/', [PublicController::class, 'download'])->name('ppdb-download');
            Route::get('ajax_file', [PublicController::class, 'ajax_file_download'])->name('file-download');
            Route::get('ajax_brosur', [PublicController::class, 'ajax_brosur_download'])->name('brosur-download');
            Route::get('post-download/{id}', [PublicController::class, 'getdownload'])->where('id', '(.*)')->name('force_download');
        });

        Route::prefix('forget_pass')->group(function () {
            Route::get('/', [UserController::class, 'forget_password'])->name('forget_password_peserta');
            Route::post('reset-password', [UserController::class, 'store_forget_pass'])->name('store_forget_pass');
            Route::get('reset-passworsd-new/{id}', [UserController::class, 'token_passwordx'])->name('token-passs');
            Route::post('change-password', [UserController::class, 'store_reset_pass'])->name('store_reset_pass');
        });
    });
});

//spp-online
//redirect view new template spp
/*Route::prefix('sumbangan-spp')->group(function () {
    Route::get('/', [SPPSumbangan::class, 'index'])->name('sumbangan-spp');
    Route::get('data-siswa', [TransaksiSpp::class, 'ajax_data_siswa'])->name('ajax_data_siswa');
    Route::prefix('setting')->group(function () {
        Route::prefix('tagihan')->group(function () {
            Route::get('/', [SettingTagihan::class, 'index'])->name('setting-tagihan');
            Route::get('datasource', [SettingTagihan::class, 'ajax_data'])->name('source_ajax_data');
            Route::get('create-tagihan', [SettingTagihan::class, 'create'])->name('create-spp');
            Route::post('save-tagihan', [SettingTagihan::class, 'store_tagihan'])->name('store_tagihan');
            Route::get('ajax-pemasukan-post/{id}', [SettingTagihan::class, 'ajax_pemasukan'])->name('ajax_pemasukan');
            Route::get('show-tagihan/{id}', [SettingTagihan::class, 'show'])->name('show-tagihan');
            Route::get('edit-tagihan/{id}', [SettingTagihan::class, 'edit'])->name('edit-tagihan');
            Route::put('update-tagihan/{id}', [SettingTagihan::class, 'update'])->name('update-tagihan');
            Route::delete('delete-tagihan/{id}', [SettingTagihan::class, 'destroy'])->name('delete-tagihan');
        });
        Route::prefix('pos')->group(function () {
            Route::get('/', [SettingTagihan::class, 'pos_index'])->name('setting-pos');
            Route::get('create-pos', [SettingTagihan::class, 'create_pos'])->name('create_pos');
            Route::get('datasource2', [SettingTagihan::class, 'ajax_data_pos'])->name('ajax_data_pos');
            Route::get('show-pos/{id}', [SettingTagihan::class, 'show_pos'])->name('show-pos');
            Route::get('edit-pos/{id}', [SettingTagihan::class, 'edit_pos'])->name('edit-pos');
            Route::post('save-pos', [SettingTagihan::class, 'store_pos'])->name('store_pos');
            Route::put('update-pos/{id}', [SettingTagihan::class, 'store_pos_update'])->name('store_pos_update');
            Route::delete('delete-pos/{id}', [SettingTagihan::class, 'destroy_pos'])->name('destroy-pos');
            Route::prefix('target')->group(function () {
                Route::get('/', [SettingTagihan::class, 'target_pos'])->name('target_pos');
                Route::get('datasourcex', [SettingTagihan::class, 'ajax_data_target'])->name('ajax_data_target');
                Route::get('show-target/{id}', [SettingTagihan::class, 'show_target'])->name('show-target');
                Route::get('edit-target/{id}', [SettingTagihan::class, 'edit_target'])->name('edit-target');
                Route::put('update-target/{id}', [SettingTagihan::class, 'store_target_update'])->name('store_target_update');
                Route::delete('delete-target/{id}', [SettingTagihan::class, 'destroy_target'])->name('destroy_target');
                Route::get('create-target', [SettingTagihan::class, 'create_target'])->name('create_target');
                Route::post('save-target', [SettingTagihan::class, 'store_target_post'])->name("store_target_post");
            });
        });
        Route::prefix('template')->group(function () {
            Route::get('/', [SettingTagihan::class, 'template'])->name('setting-template-tagihan');
            Route::put('update-spp/{id}', [SettingTagihan::class, 'store_setting_spp'])->name('store_setting_spp');
        });
        Route::prefix('pesan')->group(function () {
            Route::get('/', [SettingTagihan::class, 'setting_pesan'])->name('setting_pesan');
            Route::get('dataxsource', [SettingTagihan::class, 'ajax_data_settingpesan'])->name('datax-setting-pesan');
            Route::get('show-setting-pesan/{id}', [SettingTagihan::class, 'show_settingpesan'])->name('show_settingpesan');
            Route::delete('delete-setting-pesan/{id}', [SettingTagihan::class, 'destroy_settingpesan'])->name('destroy_settingpesan');
            Route::post('save-setting-pesan', [SettingTagihan::class, 'store_setting_pesan'])->name('store_setting_pesan');
        });
        Route::prefix('transaksi')->group(function () {
            Route::get('pemasukan', [TransaksiSpp::class, 'index'])->name('pemasukan-tagihan');
            Route::get('siswa', [TransaksiSpp::class, 'ajax_data'])->name('ajax-siswa');
            Route::get('siswa/{id}', [TransaksiSpp::class, 'ajax_siswa'])->name('ajax_siswa_id');
            Route::get('tagihan-kelas/{id}', [TransaksiSpp::class, 'ajax_tagihan_kelas'])->name('ajax_tagihan_kelas');
            Route::get('siswa/search/{id}', [TransaksiSpp::class, 'ajax_siswa_search'])->name('ajax_siswa_search');
            Route::get('siswa/search/tahun/{id}', [TransaksiSpp::class, 'ajax_siswa_search_by_tahun'])->name('ajax_siswa_search_by_tahun');
            Route::get('siswa/search/kelas/tahun/{id}/{tahun}', [TransaksiSpp::class, 'ajax_siswa_search_by_kelas'])->name('ajax_siswa_search_by_kelas');
            Route::get('show-tagihan/{id}', [TransaksiSpp::class, 'show_tagihan'])->name('get-tagihan-id');
            Route::get('show-tagihan/bulan/siswa/{id}', [TransaksiSpp::class, 'show_tagihan_bulan'])->name('show_tagihan_bulan');
            Route::post('save-transaksi', [TransaksiSpp::class, 'store'])->name('store-transaksi');
            Route::post('update-transaksi/{id}', [TransaksiSpp::class, 'update'])->name('update-transaksi');
            Route::delete('remove-transaksi/{id}', [TransaksiSpp::class, 'destroy'])->name('remove-transaksi');

            Route::prefix('history-transaksi')->group(function () {
                Route::get('pemasukan', [TransaksiSpp::class, 'history_pemasukan'])->name('history-transaksi-pemasukan');
                Route::get('ajax-history-pemasukan', [TransaksiSpp::class, 'ajax_data_history_transaksi'])->name('ajax-history-pemasukan');
                Route::get('detail-transaksi/{id}', [TransaksiSpp::class, 'show'])->name('detail-transaksi');
                Route::get('last-transaksi/{id}', [TransaksiSpp::class, 'show_last_transaksi'])->name('last-transaksi');
            });
            Route::prefix('keringanan-spp')->group(function () {
                Route::get('/', [TransaksiSpp::class, 'keringananspp'])->name('potongan-spp');
                Route::get('dt-ajax-keringanan', [TransaksiSpp::class, 'ajax_data_keringanan_spp'])->name('dt-keringanan-spp');
                Route::post('simpan-keringan', [TransaksiSpp::class, 'store_keringanan'])->name('store_keringanan');
                Route::get('show-detail/{id}', [TransaksiSpp::class, 'show_diskon_spp'])->name('show_diskon_spp');
                Route::get('edit-detail/{id}', [TransaksiSpp::class, 'edit_diskon_spp'])->name('edit_diskon_spp');
                Route::put('update-keringan/{id}', [TransaksiSpp::class, 'update_keringanan'])->name('update_keringanan');
            });
            Route::prefix('konfirmasi-spp')->group(function () {
                Route::get('/', [TransaksiSpp::class, 'konfirmasi_spp'])->name('konfirmasi-transaksi');
                Route::get('dt-konfirm', [TransaksiSpp::class, 'ajax_data_konfirmasi_spp'])->name('ajax-konfirm-transaksi');
            });
            Route::prefix('print-nota')->group(function () {
                Route::get('{id}', [TransaksiSpp::class, 'print_nota'])->name('print_nota');
            });
            Route::prefix('report')->group(function () {
                Route::get('/report-tagihan', [ReportController::class, 'index'])->name('report-tagihan');
                Route::post('/report-tagihan', [ReportController::class, 'index'])->name('report-tagihan-post');
                Route::get('print-report/{id}/{tahun}/{name_kelas}/{name_tahun}', [ReportController::class, 'print_report_tagihan'])->where(['name_tahun' => "[\w\/]+"])->name('print_report_tagihan');
            });
        });
    });
});
*/

//SPP new template
Route::prefix('sumbangan-spp')->group(function () {
    Route::middleware('check.api.token')->group(function () {
        Route::get('/', [SPPSumbangan::class, 'index'])->name('sumbangan-spp');
        Route::get('data-siswa', [TransaksiSpp::class, 'ajax_data_siswa'])->name('ajax_data_siswa');
        Route::prefix('setting')->group(function () {
            Route::prefix('tagihan')->group(function () {
                Route::get('/', [SettingTagihan::class, 'index'])->name('setting-tagihan');
                Route::get('datasource', [SettingTagihan::class, 'ajax_data'])->name('source_ajax_data');
                Route::get('create-tagihan', [SettingTagihan::class, 'create'])->name('create-spp');
                Route::post('save-tagihan', [SettingTagihan::class, 'store_tagihan'])->name('store_tagihan');
                Route::get('ajax-pemasukan-post/{id}', [SettingTagihan::class, 'ajax_pemasukan'])->name('ajax_pemasukan');
                Route::get('show-tagihan/{id}', [SettingTagihan::class, 'show'])->name('show-tagihan');
                Route::get('edit-tagihan/{id}', [SettingTagihan::class, 'edit'])->name('edit-tagihan');
                Route::put('update-tagihan/{id}', [SettingTagihan::class, 'update'])->name('update-tagihan');
                Route::delete('delete-tagihan/{id}', [SettingTagihan::class, 'destroy'])->name('delete-tagihan');
            });
            Route::prefix('pos')->group(function () {
                Route::get('/', [SettingTagihan::class, 'pos_index'])->name('setting-pos');
                Route::get('create-pos', [SettingTagihan::class, 'create_pos'])->name('create_pos');
                Route::get('datasource2', [SettingTagihan::class, 'ajax_data_pos'])->name('ajax_data_pos');
                Route::get('show-pos/{id}', [SettingTagihan::class, 'show_pos'])->name('show-pos');
                Route::get('edit-pos/{id}', [SettingTagihan::class, 'edit_pos'])->name('edit-pos');
                Route::post('save-pos', [SettingTagihan::class, 'store_pos'])->name('store_pos');
                Route::put('update-pos/{id}', [SettingTagihan::class, 'store_pos_update'])->name('store_pos_update');
                Route::delete('delete-pos/{id}', [SettingTagihan::class, 'destroy_pos'])->name('destroy-pos');
                Route::prefix('target')->group(function () {
                    Route::get('/', [SettingTagihan::class, 'target_pos'])->name('target_pos');
                    Route::get('datasourcex', [SettingTagihan::class, 'ajax_data_target'])->name('ajax_data_target');
                    Route::get('show-target/{id}', [SettingTagihan::class, 'show_target'])->name('show-target');
                    Route::get('edit-target/{id}', [SettingTagihan::class, 'edit_target'])->name('edit-target');
                    Route::put('update-target/{id}', [SettingTagihan::class, 'store_target_update'])->name('store_target_update');
                    Route::delete('delete-target/{id}', [SettingTagihan::class, 'destroy_target'])->name('destroy_target');
                    Route::get('create-target', [SettingTagihan::class, 'create_target'])->name('create_target');
                    Route::post('save-target', [SettingTagihan::class, 'store_target_post'])->name("store_target_post");
                    Route::put('update-realisasi/{id}', [SettingTagihan::class, 'store_target_realisasi'])->name('store_target_realisasi');
                });
            });
            Route::prefix('template')->group(function () {
                Route::get('/', [SettingTagihan::class, 'template'])->name('setting-template-tagihan');
                Route::put('update-spp/{id}', [SettingTagihan::class, 'store_setting_spp'])->name('store_setting_spp');
            });
            Route::prefix('pesan')->group(function () {
                Route::get('/', [SettingTagihan::class, 'setting_pesan'])->name('setting_pesan');
                Route::get('dataxsource', [SettingTagihan::class, 'ajax_data_settingpesan'])->name('datax-setting-pesan');
                Route::get('show-setting-pesan/{id}', [SettingTagihan::class, 'show_settingpesan'])->name('show_settingpesan');
                Route::delete('delete-setting-pesan/{id}', [SettingTagihan::class, 'destroy_settingpesan'])->name('destroy_settingpesan');
                Route::post('save-setting-pesan', [SettingTagihan::class, 'store_setting_pesan'])->name('store_setting_pesan');
            });
            Route::prefix('transaksi')->group(function () {
                Route::get('pemasukan', [TransaksiSpp::class, 'index'])->name('pemasukan-tagihan');
                Route::get('siswa', [TransaksiSpp::class, 'ajax_data'])->name('ajax-siswa');
                Route::get('siswa/{id}', [TransaksiSpp::class, 'ajax_siswa'])->name('ajax_siswa_id');
                Route::get('tagihan-kelas/{id}', [TransaksiSpp::class, 'ajax_tagihan_kelas'])->name('ajax_tagihan_kelas');
                Route::get('siswa/search/{id}', [TransaksiSpp::class, 'ajax_siswa_search'])->name('ajax_siswa_search');
                Route::get('siswa/search/tahun/{id}', [TransaksiSpp::class, 'ajax_siswa_search_by_tahun'])->name('ajax_siswa_search_by_tahun');
                Route::get('siswa/search/kelas/tahun/{id}/{tahun}', [TransaksiSpp::class, 'ajax_siswa_search_by_kelas'])->name('ajax_siswa_search_by_kelas');
                Route::get('show-tagihan/{id}', [TransaksiSpp::class, 'show_tagihan'])->name('get-tagihan-id');
                Route::get('show-tagihan/bulan/siswa/{id}', [TransaksiSpp::class, 'show_tagihan_bulan'])->name('show_tagihan_bulan');
                Route::post('save-transaksi', [TransaksiSpp::class, 'store'])->name('store-transaksi');
                Route::post('update-transaksi/{id}', [TransaksiSpp::class, 'update'])->name('update-transaksi');
                Route::delete('remove-transaksi/{id}', [TransaksiSpp::class, 'destroy'])->name('remove-transaksi');
                Route::get('ajax-transaksi-siswa/{id}', [TransaksiSpp::class, 'get_riwayat_transaksi'])->name('get_riwayat_transaksi');
                Route::get('print-tagihan/{id}', [TransaksiSpp::class, 'print_tagihan_siswa'])->name('print_tagihan_siswax');

                Route::prefix('history-transaksi')->group(function () {
                    Route::get('pemasukan', [TransaksiSpp::class, 'history_pemasukan'])->name('history-transaksi-pemasukan');
                    Route::get('ajax-history-pemasukan', [TransaksiSpp::class, 'ajax_data_history_transaksi'])->name('ajax-history-pemasukan');
                    Route::get('detail-transaksi/{id}', [TransaksiSpp::class, 'show'])->name('detail-transaksi');
                    Route::get('last-transaksi/{id}', [TransaksiSpp::class, 'show_last_transaksi'])->name('last-transaksi');
                });
                Route::prefix('keringanan-spp')->group(function () {
                    Route::get('/', [TransaksiSpp::class, 'keringananspp'])->name('potongan-spp');
                    Route::get('dt-ajax-keringanan', [TransaksiSpp::class, 'ajax_data_keringanan_spp'])->name('dt-keringanan-spp');
                    Route::post('simpan-keringan', [TransaksiSpp::class, 'store_keringanan'])->name('store_keringanan');
                    Route::get('show-detail/{id}', [TransaksiSpp::class, 'show_diskon_spp'])->name('show_diskon_spp');
                    Route::get('edit-detail/{id}', [TransaksiSpp::class, 'edit_diskon_spp'])->name('edit_diskon_spp');
                    Route::put('update-keringan/{id}', [TransaksiSpp::class, 'update_keringanan'])->name('update_keringanan');
                });
                Route::prefix('konfirmasi-spp')->group(function () {
                    Route::get('/', [TransaksiSpp::class, 'konfirmasi_spp'])->name('konfirmasi-transaksi');
                    Route::get('dt-konfirm', [TransaksiSpp::class, 'ajax_data_konfirmasi_spp'])->name('ajax-konfirm-transaksi');
                    Route::post('create-konfirm-message', [TransaksiSpp::class, 'store_konfirm_pesan'])->name('store_konfirm_pesan');
                    Route::get('paid-konfirmasi', [TransaksiSpp::class, 'ajax_paid_konfirmasi_spp'])->name('ajax-paid-konfirm');
                    Route::get('show-konfirmasi/{id}', [TransaksiSpp::class, 'show_transaction_konfirmasi'])->name('detail-konfirmasi-trans');
                    Route::delete('terima-konfirmasi/{id}/{id_siswa}', [Konfirmasi::class, 'approve_transaction'])->name('approve_transaction');
                    Route::get('post-download/{id}', [TransaksiSpp::class, 'getdownload'])->where('id', '(.*)')->name('force_downloadx');
                    Route::delete('tolak-konfirmasi/{id}/{id_siswa}', [Konfirmasi::class, 'reject_transaction'])->name('reject_transaction');
                });
                Route::prefix('print-nota')->group(function () {
                    Route::get('{id}', [TransaksiSpp::class, 'print_nota'])->name('print_nota');
                });
                Route::prefix('report')->group(function () {
                    Route::get('/report-tagihan', [ReportController::class, 'index'])->name('report-tagihan');
                    Route::post('/report-tagihan', [ReportController::class, 'index'])->name('report-tagihan-post');
                    Route::get('print-report/{id}/{tahun}/{name_kelas}/{name_tahun}', [ReportController::class, 'print_report_tagihan'])->where(['name_tahun' => "[\w\/]+"])->name('print_report_tagihan');
                    Route::get('realisasi-filter/{id}/{bulan}', [ReportController::class, 'report_realiasi_filter'])->name('report_realiasi_filter');
                    Route::get('realisasi',[ReportController::class,'report_realiasi'])->name('report_realiasi');
                    Route::get('realisasi-filter/{id}/{bulan}',[ReportController::class,'report_realiasi_filter'])->name('report_realiasi_filter');
                    Route::get('realisasi/{tahun}',[ReportController::class,'report_realiasiTahun'])->name('report_realiasiTahun');
                    Route::prefix('print')->group(function(){
                        Route::get('realisasi/{tahun}',[ReportController::class,'report_realiasiTahunprint'])->name('report_realiasiTahunprint');
                        Route::get('realisasi-filter/{id}/{bulan}',[ReportController::class,'report_realiasi_filterprint'])->name('report_realiasi_filterprint');
                    });
                });

                //notifikasi transaction
                Route::prefix('pesan')->group(function(){
                    Route::get('ortu/{id}',[TransaksiSpp::class,'notifikasi_trans'])->name('notifikasi_trans');
                    Route::get('admin',[TransaksiSpp::class,'pesan_admin'])->name('pesan_admin');
                    Route::get('admin/{id}',[TransaksiSpp::class,'notifikasiadmin_trans'])->name('notifikasiadmin_trans');
                    Route::get('admin/datasource/{id}',[TransaksiSpp::class,'ajax_data_pesanadmin'])->name('ajax_data_pesanadmin');
                    Route::put('admin/read/{id}',[TransaksiSpp::class,'read_notifikasi_transadmin'])->name('read_notifikasi_transadmin');
                });

            });
        });

        //Akuntansi
        Route::prefix('akuntansi')->group(function(){
            Route::prefix('akun_kategori')->group(function(){
                Route::get('/',[AkuntansiController::class,'index'])->name('akun_kategori_spp');
                Route::get('datasource',[AkuntansiController::class,'ajax_data_kategori'])->name('ajax_data_kategorispp');
                Route::get('/{id}',[AkuntansiController::class,'show_kategori'])->name('show_kategori');
                Route::put('update-kategori/{id}',[AkuntansiController::class,'update_kategori'])->name('update_kategorispp');
                Route::post('save-kategori',[AkuntansiController::class,'store_kategori'])->name('tambah_kategorispp');
            });
            Route::prefix('akun_subkategori')->group(function(){
                Route::get('/',[AkuntansiController::class,'indexsub'])->name('akun_subkategori_spp');
                Route::get('datasource',[AkuntansiController::class,'ajax_data_subkategori'])->name('ajax_data_subkategorispp');
                Route::get('/{id}',[AkuntansiController::class,'show_subkategori'])->name('show_subkategori');
                Route::put('update-subkategori/{id}',[AkuntansiController::class,'update_subkategori'])->name('update_subkategorispp');
                Route::post('save-subkategori',[AkuntansiController::class,'store_subkategori'])->name('tambah_subkategorispp');
            });
        });

        //intergrasi Pembayaran & notifikasi
        Route::prefix('intergrasi')->group(function(){
            Route::get('/',[IntergrasiController::class,'index'])->name('intergrasi-pembayaran');
            Route::post('/',[IntergrasiController::class,'store'])->name('update_storex');
        });

        // halaman sisswa
        Route::prefix('siswa')->group(function () {
            Route::get('history-pembayaran-spp', [ReportController::class, 'history_transaksi'])->name('history-pembayaran-spp');
            Route::get('datasource', [ReportController::class, 'ajax_data_history_transaksi'])->name('ajax-history-siswa');
            Route::get('tagihan-bulan-ini', [ReportController::class, 'tagihan_siswa_bulan'])->name('tagihan-siswa');
            Route::get('print-tagihan', [ReportController::class, 'print_tagihan_siswa'])->name('print_tagihan_siswa');
            Route::prefix('konfirmasi-pembayaran')->group(function () {
                Route::get('upload-bukti', [Konfirmasi::class, 'create_upload'])->name('create_upload');
                Route::post('simpan-bukti', [Konfirmasi::class, 'store_upload'])->name('store_upload');
                Route::get('ajax-tagihan-siswa', [Konfirmasi::class, 'get_tagihan_siswa'])->name('get_tagihan_siswa');
                Route::post('post-epayment', [Konfirmasi::class, 'store_epayment'])->name('store_epayment');
            });
        });

        //halaman ortu
        Route::prefix('ortu')->group(function () {
            Route::get('datasource', [ReportController::class, 'ajax_data_history_transaksiortu'])->name('ajax-history-ortu');
            Route::get('history-pembayaran', [ReportController::class, 'history_transaksiortu'])->name('history-pembayaran-ortu');
            Route::get('tagihan-bulan-ini', [ReportController::class, 'tagihan_ortu_bulan'])->name('tagihan-ortu');
            Route::get('print-tagihan', [ReportController::class, 'print_tagihan_ortu'])->name('print_tagihan_ortu');
            Route::prefix('pemberitahuan')->group(function(){
                Route::get('/',[TransaksiSpp::class,'pesan_ortu'])->name('pesan_ortu');
                Route::get('datasource/{id}',[TransaksiSpp::class,'ajax_data_pesanortu'])->name('ajax_data_pesanortu');
                Route::put('read/{id}/{id_kelas_siswa}',[TransaksiSpp::class,'read_notifikasi_trans'])->name('read_notifikasi_trans');
            });
        });

        //profil
        Route::prefix('profil')->group(function () {
            Route::get('/', [SPPSumbangan::class, 'profilUser'])->name('profilUser');
            Route::post('simpan-profil', [SPPSumbangan::class, 'store'])->name('update_profil');
            Route::get('reset-password', [SPPSumbangan::class, 'form_password'])->name('reset-passwordx');
            Route::post('update-password', [SPPSumbangan::class, 'update_password'])->name('update_passwordx');
        });
    });

    Route::prefix('public')->group(function () {
        Route::prefix('pembayaran')->group(function () {
            Route::get('{id}', [Konfirmasi::class, 'tagihan_by_code'])->name('pembayaran-konfirmasi');
            Route::post('change-metode', [Konfirmasi::class, 'change_metode_bayar'])->name('change_metode_bayar');
            Route::get('transfer_manual/{id}', [Konfirmasi::class, 'transfer_by_code'])->name('transfer_by_code');
            Route::get('pembayaran-success/{id}/{total}/{type}/{message}', [Konfirmasi::class, 'transaksi_success'])->name('transaksi_success');
            Route::get('cek-pembayaran/{id}/{role}', [Konfirmasi::class, 'cek_statusOrder'])->name('cek-order');
        });
        Route::prefix('forget_pass')->group(function () {
            Route::get('/', [SPPSumbangan::class, 'forget_password'])->name('forget_passwordx');
            Route::post('reset-password', [SPPSumbangan::class, 'store_forget_pass'])->name('store_forget_passx');
            Route::get('reset-passworsd-new/{id}', [SPPSumbangan::class, 'token_passwordx'])->name('token-passsx');
            Route::post('change-password', [SPPSumbangan::class, 'store_reset_pass'])->name('store_reset_passx');
        });
    });
});
