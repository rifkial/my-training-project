<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    <style>
        html,
        body {
            min-height: 100%;
            min-width: 100%;
        }

        body {
            background-image: linear-gradient(to right top, #f5576c, #df4a83, #bf4895, #964ba0, #654ea3);
            font-family: 'Quicksand', sans-serif;
            font-weight: 400;
        }

        .login {
            background: rgba(0, 0, 0, 0.2);
            color: #ffffff;
            border-radius: 150px 0px 150px 0px;
        }

        .form-group .form-control {
            background-color: transparent;
            padding: 25px 15px;
            color: #ffffff;
            border-color: rgba(0, 0, 0, 0.3);
        }

        .form-control:focus {
            background-color: rgba(245, 87, 108, 0.2);
            border-color: rgba(245, 87, 108, 0.7);
        }

        .btn-btc {
            color: #fff;
            background-color: rgba(245, 87, 108, 0.7);
            padding: 10px 25px;
            font-size: 15px;
        }

        .btn-btc:hover {
            color: #ffffff;
            background-color: rgba(245, 87, 108, 0.9);
        }

        input::-webkit-input-placeholder {
            color: white !important;
        }

        input:-moz-placeholder {
            /* Firefox 18- */
            color: white !important;
        }

        input: :-moz-placeholder {
            /* Firefox 19+ */
            color: white !important;
        }

        input:-ms-input-placeholder {
            color: white !important;
        }

    </style>

    <div class="container">
        <section class="form-block py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-7 col-sm-12 mx-auto">
                        <div class="login shadow py-5 px-4 my-5">
                            <div class="title text-center py-3">
                                <img src="{{ $sekolah['file'] }}" alt="" style="width: 200px">
                                <p>{{ strtoupper($sekolah['nama']) }}</p>
                            </div>
                            <form class="py-4" action="{{ route('auth.verifylogin') }}" method="post" id="formLogin" onsubmit="return loader()">
                                @csrf
                                <div class="form-group mb-4">
                                    <input type="hidden" name="role" value="admin">
                                    <input type="text" name="username"  class="form-control rounded-pill shadow-none"
                                        id="exampleInputEmail" placeholder="Enter Username" required>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="password" name="password" class="form-control rounded-pill shadow-none"
                                        id="exampleInputPassword" placeholder="Password" required>
                                </div>
                                <button type="submit" class="btn btn-btc font-weight-bold shadow-none" id="btnLogin">Submit</button>
                                <span style="float:right; text-align:left;  display: inline-block;">
                                    <a class="small-text" href="{{ url('auth/reset_password') }}">Forgot password?</a>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        function loader() {
            $("#btnLogin").attr("disabled", true);
            $("#btnLogin").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
        }


        // $('#formLogin').on('submit', function(event) {
        //     event.preventDefault();
        //     $("#btnLogin").html(
        //         '<i class="fa fa-spin fa-spinner"></i> Menunggu');
        //     $("#btnLogin").attr("disabled", true);
        //     $.ajax({
        //         url: "{{ route('auth.verifylogin') }}",
        //         method: 'POST',
        //         data: $(this).serialize(),
        //         dataType: "json",
        //         success: function(data) {
        //             if (data.status == 'berhasil') {
        //                 window.location.href = data.redirect;
        //             } else {
        //                 $("#btnLogin").html(
        //                     'Masuk');
        //                 $("#btnLogin").attr("disabled", false);
        //             }
        //             swa(data.status + "!", data.message, data.icon);

        //         },
        //         error: function(data) {
        //             console.log('Error:', data);
        //             $('#btnLogin').html(
        //                 'Masuk');
        //         }
        //     });
        // });

        function swa(status, message, icon) {
            swal(
                status,
                message,
                icon
            );
            return true;
        }

        function lupaPassword() {
            var role = $('#role').val();
            if (role == 'admin') {
                window.location.href = "{{ url('auth/reset_password') }}";
            } else {
                alert('harap hubungi admin untuk mereset password')
            }
        }
    </script>
</body>

</html>
