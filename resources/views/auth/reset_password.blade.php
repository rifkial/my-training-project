<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body class="body-bg-full profile-page"
    style="background: radial-gradient(ellipse at center, #0264d6 1%, #1c2b5a 100%);">
    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center login-center-mini mx-auto"
            style="background-color: rgba(0,0,0,0.5) !important;">
            <div class="form-gap"></div>
            <div class="container">
                <div class="row" style="display: block;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                                <h3><i class="fa fa-lock fa-4x" style="color: #fff"></i></h3>
                                <h2 class="text-center" style="color: #fff">Forgot Password?</h2>
                                <p style="color: #fff">You can reset your password here.</p>
                                <div class="panel-body">
                                    <form method="POST" autocomplete="off"
                                        action="{{ route('auth.check_email') }}" onsubmit="return loader()">
                                        @csrf
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                                <input id="email" name="email" placeholder="email address"
                                                    class="form-control" type="email" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-lg btn-info btn-block" type="submit"
                                                id="btnReset">Reset
                                                Password</button>
                                        </div>
                                    </form>
                                    <p style="color: #fff">Back to <a href="{{ url('auth/login/adm') }}">Login</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function loader() {
            $("#btnReset").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btnReset").attr("disabled", true);
        }
    </script>
    @include('includes.foot')
</body>

</html>
