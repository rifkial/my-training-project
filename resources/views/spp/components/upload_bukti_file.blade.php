@extends('spp.apps')
@section('spp.components')
    <style>
        .clonable.add {
            display: none;
        }

        .clonable:last-child .add {
            display: inline-block !important;
        }

        .clonable:first .add {
            display: none !important;
        }

        .clonable.dx:first .add-clone-form {
            display: none !important;
        }

        .clonable.dx:last-child .add-clone-form {
            display: inline-block !important;
        }

        .clonable:only-child .remove {
            display: none !important;
        }

    </style>
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Konfirmasi Pembayaran</h5>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0"> Tipe Pembayaran </label>
                            <div class="col-md-4">
                                <select class="form-control" id="tipe_pembayaran" name="tipe_pembayaran">
                                    <option disabled="disabled" selected="true" value="">Pilih Tipe Pembayaran</option>
                                    <option value="transfer_bank"> Transfer Bank </option>
                                    <option value="e-payment"> E-Payment </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="transfer_bank" style="display: none;">
                                <form id="DocForm" name="DocForm" method="POST"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kode Transaksi <span
                                                class="text-red">(*)</span></label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input class="form-control" id="kode" name="kode"
                                                    placeholder="Kode Transaksi" type="text"
                                                    value="{{ old('kode') ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Bank<span
                                                class="text-red">(*)</span></label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input class="form-control" id="nama" name="nama" placeholder="Nama Bank"
                                                    type="text" value="{{ old('nama') ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l1">Tanggal
                                        </label>
                                        <div class="col-md-4">
                                            <div class="input-group input-has-value">
                                                <input type="text" id="tanggal_bayar" name="tanggal_bayar"
                                                    value="{{ old('tanggal_bayar') }}" readonly="readonly"
                                                    class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                    data-plugin-options='{"autoclose": true}'>
                                                <span class="input-group-addon"><i
                                                        class="list-icon material-icons">date_range</i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran </label>
                                        <div class="col-md-4">
                                            <select class="form-control" id="tahun_ajaran" name="tahun_ajaran">
                                                <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                                </option>
                                                @foreach ($tahun_ajaran as $key => $value)
                                                    <option value="{{ $value['tahun_ajaran'] }}">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l16">Bukti Transfer
                                            <span class="text-red">(*)</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input id="image" type="file" name="image" accept="image/*,doc,docx,pdf">
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 btn-list">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="material-icons list-icon">save</i>
                                                        Simpan
                                                    </button>
                                                </div>
                                                <!-- /.col-sm-12 -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12" id="e-payment" style="display: none;">
                                <form id="DocFormx" name="DocFormx" class="form-horizontal" enctype="multipart/form-data"
                                    novalidate>
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row mr-b-2">
                                                <input type="hidden" class="form-control is-valid" id="id_siswa"
                                                    name="id_siswa">
                                                <div class="col-md-6 mb-3 input-has-value">
                                                    <label>NIS</label>
                                                    <input type="text" class="form-control is-valid" id="nisn" name="nisn"
                                                        placeholder="Nisn" readonly="">
                                                </div>
                                                <div class="col-md-6 mb-3 input-has-value">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control is-valid" id="nama" name="nama"
                                                        placeholder="Nama" readonly="">
                                                </div>
                                                <input type="hidden" class="form-control is-valid" id="id_kelas"
                                                    name="id_kelas">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row mr-b-50">
                                                <div class="col-md-6 mb-3 input-has-value">
                                                    <label>Jurusan</label>
                                                    <input type="text" class="form-control is-valid" id="jurusan"
                                                        name="jurusan" placeholder="Jurusan" readonly="">
                                                </div>
                                                <div class="col-md-6 mb-3 input-has-value">
                                                    <label>Rombongan Belajar </label>
                                                    <input type="text" class="form-control is-valid" id="rombel"
                                                        name="rombel" placeholder="Rombel" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: -50px;">
                                        <div class="col-md-4">
                                            <label>Tahun Ajaran</label>
                                            <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                                <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran</option>
                                                @foreach ($tahun_ajaran as $key => $value)
                                                    <option value="{{ $value['tahun_ajaran'] }}">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tanggal Bayar</label>
                                            <div class="input-group input-has-value">
                                                <input type="text" class="form-control datepicker" name="tgl_bayar"
                                                    data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'
                                                    value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"> <span
                                                    class="input-group-addon"><i class="list-icon material-icons">date_range</i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-append"></div>
                                    <br>
                                    <button type="submit" class="btn btn-info update pos_transaction">
                                        <i class="fa fa-send list-icon"></i>
                                        Konfirmasi
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            const monthNames = ["Januari", "Februari", "Maret",
                "April", "Mei", "Juni",
                "Juli", "Agustus", "September", "Oktober",
                "November", "Desember"
            ];
            const d = new Date();
            $(document).ready(function() {

                //select type pembayaran
                $('body').on('change', 'select#tipe_pembayaran', function() {
                    var x = $(this).val();
                    if (x == 'transfer_bank') {
                        $('#transfer_bank').show();
                        $('#e-payment').hide();
                    } else if (x == 'e-payment') {
                        let formappend = "";
                        let list_tagihan = "";
                        let printdata = "";
                        let list_bulan = '';
                        $.ajax({
                            type: "GET",
                            url: '{{ route('get_tagihan_siswa') }}',
                            beforeSend: function() {},
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {

                                        $('#transfer_bank').hide();
                                        $('#e-payment').show();

                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));

                                        $('input[name="id_siswa"]').val(rows.id);

                                        $('input[name="kelas"]').val(rows.kelas);
                                        $('input[name="jurusan"]').val(rows.jurusan);
                                        $('input[name="rombel"]').val(rows.rombel);
                                        $('input[name="nis"]').val(rows.nis);
                                        $('input[name="nisn"]').val(rows.nisn);
                                        $('input[name="nama"]').val(rows.nama);

                                        list_bulan = ['-',
                                            'Juli', 'Agustus', 'September',
                                            'Oktober', 'November', 'Desember',
                                            'Januari', 'Februari', 'Maret',
                                            'April', 'Mei', 'Juni'
                                        ];

                                        var total_parent = parseInt(rows.tagihan.length) +
                                            1;

                                        var nominal_parsex = 0;

                                        var check_pembayaran = [];

                                        rows.tagihan.forEach((element, index) => {
                                            var x_tagihan = element['id_tagihan'];
                                            var x_bulan = element['id_bulan'];
                                            var x_nama = element['nama'];

                                            list_tagihan = list_tagihan +
                                                `<option value="` +
                                                x_tagihan + `| `+x_nama+` ">` + x_nama +
                                                `</option>`;

                                            // show tagihan bulan ini saja yang ditampilkan
                                            if (list_bulan[element['id_bulan']] ==
                                                monthNames[d.getMonth()]) {

                                                check_pembayaran.push(element);

                                                printdata = printdata +
                                                    `<tr class="clonable dx " parentId="` +
                                                    index + `">
                                                <td>
                                                    <input type="hidden" class="form-control" id="nama_tagihan" name="nama_tagihan[]"
                                                            placeholder="nama_tagihan" value="` + element['nama'] +
                                                    `">
                                                    <input type="hidden" class="form-control" id="periode" name="periode[]"
                                                            placeholder="periode" value="` + element['priode'] +
                                                    `">
                                                   <input type="hidden" class="form-control" id="id_tagihan" name="id_tagihan[]"
                                                            placeholder="id_tagihan" value="` + element['id_tagihan'] +
                                                    `">  ` + element['nama'] + `
                                                 </td>
                                                 <td>` + element['priode'] + `</td>
                                                 <td>
                                                   <input type="hidden" class="form-control" id="bulan" name="bulan[]"
                                                            placeholder="bulan" value="` + element['id_bulan'] +
                                                    `">  ` + list_bulan[element[
                                                        'id_bulan']] +
                                                    `
                                                 </td>
                                                 <td><input name="nominal[]" type="text" id="nominal" class="form-control" required="" onkeyup="currencyFormat(this)" value="` +
                                                    parseInt(element['nominal'])
                                                    .format() + `"></td>
                                                 <td class="btn-group text-center" role="group" width="100%">
                                                    <button type="button" name="remove" id="removex" class="btn btn-danger remove-clone remove"><i class="fa fa-remove"></i></button> &nbsp;
                                                    <button type="button" name="add" id="addx_form" class="btn btn-info add-clone-form add " style="display:none;"><i class="fa fa-plus"></i></button>
                                                </td>
                                                `;
                                                nominal_parsex += parseInt(element[
                                                    'nominal']);

                                            }

                                        });

                                        setTimeout(() => {
                                            var clonexD = $('.dx');
                                            clonexD.each(function(el, index) {
                                                const lastItem = clonexD[
                                                    clonexD.length - 1];
                                                var lastKEy = $(lastItem)
                                                    .attr('parentId');

                                                $('tr[parentId="' +
                                                    lastKEy +
                                                    '"] button.add-clone-form'
                                                ).show();
                                            });
                                        }, 100);


                                        formappend = `<br><table class="table table-hover">
                                        <legend class="table-info ml-2 ">List Tagihan </legend>
                                        <thead>
                                            <tr>
                                                <td>Tagihan</td>
                                                <td>Periode</td>
                                                <td>Bulan</td>
                                                <td>Nominal</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ` + printdata + `
                                            <tr class="clonable form-clone " parentId="` + total_parent + `" style="display:none;">
                                            <td>
                                                <select class="form-control id_tagihan" name="id_tagihan[]" id="id_tagihan" required>
                                                    <option disabled="disabled" selected="true" value="">Pilih Tagihan</option>
                                                    ` + list_tagihan + `
                                                </select>
                                            </td>
                                            <td width="20%"><input name="periode[]" type="text" id="periode" class="form-control" readonly=""></td>
                                            <td>
                                                <select class="form-control bulan" name="bulan[]" id="bulan" required="required">
                                                    <option disabled="disabled" selected="true" value="">Pilih Bulan</option>
                                                    <option value="1">Juli</option>
                                                    <option value="2">Agustus</option>
                                                    <option value="3">September</option>
                                                    <option value="4">Oktober</option>
                                                    <option value="5">November</option>
                                                    <option value="6">Desember</option>
                                                    <option value="7">Januari</option>
                                                    <option value="8">Februari</option>
                                                    <option value="9">Maret</option>
                                                    <option value="10">April</option>
                                                    <option value="11">Mei</option>
                                                    <option value="12">Juni</option>
                                                </select>
                                            </td>
                                            <td><input name="nominal[]" type="text" id="nominal" class="form-control" required="" onkeyup="currencyFormat(this)"></td>
                                            <td class="btn-group text-center" role="group" width="100%">
                                                <button type="button" name="remove" id="removex" class="btn btn-danger remove-clone remove"><i class="fa fa-remove"></i></button> &nbsp;
                                                <button type="button" name="add" id="addx" class="btn btn-info add-clone add"><i class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                        `;

                                        formappend = formappend + `<tfoot>
                                            <td colspan="5" align="right">&nbsp;</td>
                                        </tfoot>
                                        </table>`;

                                        formappend = formappend +
                                            '<div class="table-info"> Total Tagihan : <span class="total_tagihan" data-nominal="0">0</span></div>'

                                        $('.form-append').html(formappend);

                                        //set bulan ini
                                        $("select[name='bulan[]'] option").filter(
                                            function() {
                                                return $(this).text() == monthNames[d
                                                    .getMonth()];
                                            }).prop("selected", true);

                                        if (check_pembayaran.length == 1) {
                                            $('button.add').attr('disabled', 'disabled');
                                            $('button.remove').attr('disabled', 'disabled');
                                            window.notif('warning',
                                                'Maaf Tidak bisa menambahkan form karena hanya satu tagihan saja!'
                                            );
                                        } else if (check_pembayaran.length == 0) {
                                            window.notif('info',
                                                'Maaf tagihan bulan ini belum ada, silahkan pilih tagihan yang lain!'
                                            );
                                        } else {
                                            $('button.add').removeAttr('disabled');
                                            $('button.remove').removeAttr('disabled');
                                        }

                                        //calc
                                        $('span.total_tagihan').html("Rp." + nominal_parsex
                                            .format());
                                        $('span.total_tagihan').attr('data-nominal',
                                            nominal_parsex);

                                    } else if (result['info'] == 'error') {

                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        //console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                }

                            }
                        });
                    }
                });

                //show clone form
                $('body').on('click', 'button.add-clone-form', function() {
                    $(this).hide();
                    $('.clonable.form-clone').show();
                    var clonned = $(this).parents('.clonable.form-clone:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    $(this).hide();

                    $('.clonable').parents('tbody').append(clonned);
                    $("select[name='bulan[]'] option").filter(
                        function() {
                            return $(this).text() == monthNames[d.getMonth()];
                        }).prop("selected", true);
                });

                //clone form
                $('body').on('click', 'button.add-clone', function() {
                    var clonned = $(this).parents('.clonable:last-child').clone();
                    var parentId = clonned.attr('parentId');
                    clonned.attr('parentId', parseInt(parentId) + 1);
                    clonned.find('input[type="text"]').each(function() {
                        return $(this).val('');
                    });

                    $(this).hide();

                    $('.clonable').parents('tbody').append(clonned);

                    $("select[name='bulan[]'] option").filter(
                        function() {
                            return $(this).text() == monthNames[d.getMonth()];
                        }).prop("selected", true);
                });

                //remove clone
                $('body').on('click', 'button.remove-clone', function() {
                    $(this).parents('.clonable').remove();
                    var nominal_parse = 0;
                    $('input[name="nominal[]"]').each(function() {
                        var valx = $(this).val().toString();
                        if (valx != '') {
                            nominal_parse += parseFloat(valx.replace(/\$|,/g, ''));
                        }
                    });

                    if (!isNaN(nominal_parse)) {
                        $('span.total_tagihan').html("Rp." + nominal_parse.format());
                        $('span.total_tagihan').attr('data-nominal', nominal_parse);
                    }

                    $('button.update.pos_transaction').prop("disabled", true);

                    var indexClone = $('input[name="nominal[]"]').length;
                    if (indexClone == 1) {
                        $('.clonable.form-clone').show();
                    }
                });

                //calc bayar atau kembalian
                $('body').on('keyup', 'input[name="bayar"]', function() {
                    var xv = $(this).val().split(',').join('');
                    var nominal = $('span.total_tagihan').attr('data-nominal');
                    var kembalian = parseInt(xv) - parseInt(nominal);
                    $('input[name="kembalian"]').val(kembalian.format());
                    var $check_list_tagihan = $('input[name="nominal[]"]').val();
                    if ($check_list_tagihan == '') {
                        $('.note').html('List Tagihan pilih dahulu !');
                    } else {
                        if (!isNaN(parseInt(xv))) {
                            if (kembalian > 0) {
                                $('.note').html('');
                                $('button.update.pos_transaction').prop("disabled", false);
                            } else if (kembalian == 0) {
                                $('.note').html('');
                                $('button.update.pos_transaction').prop("disabled", false);
                            } else {
                                $('.note').html('Nominal Bayar Kurang !');
                                $('button.update.pos_transaction').prop("disabled", true);
                            }
                        } else {
                            $('input[name="kembalian"]').val('');
                        }
                    }
                });

                //keyup nominal manual calc
                $("body").on('keyup', 'input[name="nominal[]"]', function() {
                    var parentId = $(this).parents('.clonable').attr('parentId');
                    var xv = parseInt($(this).val().split(',').join(''));
                    // calc total
                    if (isNaN(xv) || xv == 0) {
                        $('span.total_tagihan').html("Rp. 0");
                        $('span.total_tagihan').attr('data-nominal', '0');
                        window.notif('error', 'Nominal Harus di isi ');
                    } else {
                        $('span.total_tagihan').html("Rp." + xv
                            .format());
                        $('span.total_tagihan').attr('data-nominal',
                            xv);
                    }
                });

                //select tagihan
                $('body').on('change', 'select.id_tagihan', function() {
                    var x = $(this).val();
                    var parentId = $(this).parents('.clonable').attr('parentId');
                    var url = '{{ route('get-tagihan-id', ':id') }}';
                    url = url.replace(':id', x);
                    if (x) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {},
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        var rows = JSON.parse(JSON
                                            .stringify(
                                                result['data']));
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="nominal[]"]').val(rows
                                            .nominal.format());
                                        $('tr[parentId="' + parentId +
                                            '"] input[name="periode[]"]').val(rows
                                            .periode);

                                        // calc total
                                        var nominal_parse = 0;
                                        $('input[name="nominal[]"]').each(function() {
                                            var valx = $(this).val();
                                            nominal_parse += parseInt(valx.split(
                                                ',').join(''));
                                        });
                                        $('span.total_tagihan').html("Rp." + nominal_parse
                                            .format());
                                        $('span.total_tagihan').attr('data-nominal',
                                            nominal_parse);
                                    }
                                }
                            }
                        });
                    } else {
                        $('tr[parentId="' + parentId + '"] input[name="nominal[]"]').val('');
                        $('tr[parentId="' + parentId + '"] input[name="periode[]"]').val('');
                    }
                });

                //post e-payment
                $('body').on('submit', '#DocFormx', function(e) {
                    e.preventDefault();
                    var urlx = '{{ route('store_epayment') }}';
                    var formData = new FormData(this);
                    const loader = $('button.update');
                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    loader.html('<i class="fa fa-send"></i>')
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                     if(typeof rows.token !='underfined'){
                                        snap.pay(rows.token, {
                                        onSuccess: function(result){console.log('success');console.log(result);},
                                        onPending: function(result){console.log('pending');console.log(result);},
                                        onError: function(result){console.log('error');console.log(result);},
                                        onClose: function(){console.log('customer closed the popup without finishing the payment');}
                                        });
                                     }
                                } else if (result['info'] == 'error') {
                                    window.notif(result['info'], result['message']);
                                    loader.html('<i class="fa fa-send"></i>')
                                }
                            }
                        },
                        error: function(data) {
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    //console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);

                            }

                        }
                    });
                });


                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

            });
        })(jQuery, window);
    </script>
@endsection
