@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pengaturan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Tambah </li>
                <li class="breadcrumb-item "><a href="{{ route('setting-tagihan') }}">Tagihan</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('store_tagihan') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l11">POS <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-4">
                                    <select class="form-control" id="id_pemasukan" name="id_pemasukan">
                                        <option disabled="disabled" selected="true"  value="">Pilih POS</option>
                                        @foreach ($pemasukan as $key => $value)
                                            <option value="{{ $value['id'] }}"> {{ $value['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="pemasukan" style="display: none;">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Kode Pemasukan <span
                                            class="text-red">(*)</span></label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control" id="kode" name="kode" placeholder="Kode Tagihan" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Target Pemasukan <span
                                            class="text-red">(*)</span></label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="target" name="target" placeholder="Target Tagihan" type="text"
                                            readonly="readonly">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l11">Kelas <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select class="form-control" id="id_kelas" name="id_kelas">
                                            <option disabled="disabled" selected="true" value="" >Pilih Kelas</option>
                                            @foreach ($kelas as $key => $value)
                                                <option value="{{ $value['id'] }}"> Kelas - {{ $value['nama'] }} -
                                                    {{ $value['jurusan'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Nama <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-9">
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Tagihan" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Periode <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="periode" name="periode">
                                        <option disabled="disabled" selected="true" value="">Pilih Periode</option>
                                        <option value="sekali">Sekali</option>
                                        <option value="tahunan">Tahunan</option>
                                        <option value="bulanan">Bulanan</option>
                                        <option value="semester">Semester</option>
                                        <option value="bebas">Bebas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Bulan <span
                                        class="text-red">(*)</span> </label>
                                <div class="col-md-9">
                                    <select class="form-control py-3" id="bulan" name="bulan[]" multiple="multiple" required>
                                        <option value="1">Juli</option>
                                        <option value="2">Agustus</option>
                                        <option value="3">September</option>
                                        <option value="4">Oktober</option>
                                        <option value="5">November</option>
                                        <option value="6">Desember</option>
                                        <option value="7">Januari</option>
                                        <option value="8">Februari</option>
                                        <option value="9">Maret</option>
                                        <option value="10">April</option>
                                        <option value="11">Mei</option>
                                        <option value="12">Juni</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="tahun_ajaran" name="tahun_ajaran">
                                        <option disabled="disabled" selected="true" value="" >Pilih Tahun Ajaran</option>
                                        @foreach ($tahun_ajaran as $key => $value)
                                            <option value="{{ $value['tahun_ajaran'] }}">{{ $value['tahun_ajaran'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Nominal <span
                                        class="text-red">(*)</span></label>
                                <div class="col-md-9">
                                    <input class="form-control" id="nominal" name="nominal" placeholder="Nominal Tagihan" type="text" onkeyup="currencyFormat(this)">
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('setting-tagihan') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('body').on('change', 'select[name="id_pemasukan"]', function() {
                    var valx = $(this).val();
                    var url = '{{ route('ajax_pemasukan', ':id') }}';
                    url = url.replace(':id', valx);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {

                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('.pemasukan').show();
                                    $('input[name="kode"]').val(rows.kode);
                                    $('input[name="target"]').val(rows.target.format());
                                }
                            }

                        }
                    });

                });

                    //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

            });
        })(jQuery, window);
    </script>
@endsection
