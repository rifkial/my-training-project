<!DOCTYPE html>
<html>

<head>
    <title>{{ session('title') }}</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            padding: 30px 10px;
            margin-right: 150px;
            margin-left: 20px;
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px;
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .grid-item {
            font-size: 30px;
            text-align: center;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }

            @page {
                size: portrait;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
       PrintWindow();
    </script>
</head>
<body>
    <center>
        <h1><b>{{ session('title') }}</b></h1>
        <hr>
    </center>
    <table class="table  table-striped table-hover">
        <table class="table table-striped table-hover">
            <thead class="table-info">
                <tr>
                    <td rowspan='2'
                        style="text-align: left; vertical-align: middle;">No</td>
                    <td rowspan='2'
                        style="text-align: left; vertical-align: middle;">PENERIMAAN</td>
                    <td rowspan='2'
                        style="text-align: left; vertical-align: middle;">Nominal</td>
                </tr>
            </thead>
            <tbody>
                @php
                    $total_nominal = 0;
                @endphp
                @foreach ($realisasi as $key => $val)
                  @php
                      $total_nominal += intval($val['nominal']);
                  @endphp
                 <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['nama'] }}</td>
                    <td>{{ number_format($val['nominal'],0) }}</td>
                 </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr class="table-info">
                    <td colspan='2' class="text-left"
                    style="text-align: left; vertical-align: middle;">Total</td>
                    <td>{{ number_format($total_nominal,0) }}</td>
                </tr>
            </tfoot>
    </table>
</body>
</html>
