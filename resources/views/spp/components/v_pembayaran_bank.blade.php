<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('spp.includes.head')
</head>

<body class="sidebar-horizontal header-centered">
    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            @if (count($tagihan) > 0)

                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-heading clearfix text-center">
                                <p class="text-info">Pembayaran Tagihan Metode Transfer Bank / Upload Bukti Transfer
                                </p>
                                <hr>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger border-info mt-1" role="alert">
                                        <button aria-label="Close" class="close" data-dismiss="alert"
                                            type="button"><span aria-hidden="true">×</span>
                                        </button>
                                        <div class="widget-list">
                                            <div class="col-md-12 widget-holder">
                                                <div class="widget-body clearfix">
                                                    <div class="row">
                                                        <i class="material-icons list-icon md-48">warning</i>
                                                        <ul class="mr-t-10">
                                                            @foreach ($errors->all() as $error)
                                                                <li class="text-red">{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- /.widget-body -->
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if ($message = Session::get('error'))
                                        <div class="alert alert-error border-error" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <div class="widget-list">
                                                <div class="col-md-12 widget-holder">
                                                    <div class="widget-body clearfix">
                                                        <strong>{{ $message }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success border-info alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <div class="widget-list">
                                                <div class="col-md-12 widget-holder">
                                                    <div class="widget-body clearfix text-center">
                                                        <i class="material-icons list-icon">check_circle</i>
                                                        <strong>{{ $message }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                            </div>
                            <!-- /.widget-heading -->
                            <div class="widget-body clearfix">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5 class="text-info">Informasi Data Siswa </h5>
                                        <table class="table">
                                            <tbody>
                                                <tr width="100%;">
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['nama'] }}</td>
                                                </tr>
                                                <tr width="100%;">
                                                    <td>NIS</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['nis'] }}</td>
                                                </tr>
                                                <tr width="100%;">
                                                    <td>Kelas</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['kelas'] }}</td>
                                                </tr>
                                                <tr width="100%;">
                                                    <td>Rombel</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['rombel'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jurusan</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['jurusan'] }}</td>
                                                </tr>
                                                <tr width="100%;">
                                                    <td>Sekolah</td>
                                                    <td>:</td>
                                                    <td>{{ $tagihan['sekolah'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-8">
                                        <h5 class="text-info"> Form Upload </h5>
                                        <form id="DocForm" name="DocForm" method="POST"
                                            action="{{ route('store_upload') }}" class="form-horizontal"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l0">Kode Transaksi <span
                                                        class="text-red"></span></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input class="form-control" id="kode" name="kode"
                                                            placeholder="Kode Transaksi" type="text" readonly="readonly"
                                                            value="{{ old('kode') ?? ($tagihan['kode'] ?? '') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l0">Nama Bank Tujuan<span
                                                        class="text-red"></span></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input class="form-control" id="nama" name="nama"
                                                            placeholder="Nama Bank" type="text" readonly
                                                            value="{{ old('nama') ?? ($rekening['nama_bank'] ?? '') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l0">No Bank Tujuan<span
                                                        class="text-red"></span></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input class="form-control" id="no_rekening"
                                                            name="no_rekening" placeholder="Rekening tujuan "
                                                            type="text" readonly
                                                            value="{{ old('no_rekening') ?? ($rekening['rekening_bank'] ?? '') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l0">Atas Nama (Pemilik
                                                    Rekening Bank Tujuan)<span class="text-red"></span></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input class="form-control" id="atas_nama" name="atas_nama"
                                                            placeholder="Atas Nama " type="text" readonly
                                                            value="{{ old('atas_nama') ?? ($rekening['atas_nama'] ?? '') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l1">Tanggal
                                                    <span class="text-red">(*)</span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="input-group input-has-value">
                                                        <input type="text" id="tanggal_bayar" name="tanggal_bayar"
                                                            value="{{ old('tanggal_bayar') }}" readonly="readonly"
                                                            class="form-control datepicker"
                                                            data-date-format="yyyy-mm-dd"
                                                            data-plugin-options='{"autoclose": true}'>
                                                        <span class="input-group-addon"><i
                                                                class="list-icon material-icons">date_range</i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l0">Tahun Ajaran </label>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="tahun_ajaran" name="tahun_ajaran"
                                                        placeholder="Tahun Ajaran" type="text" readonly
                                                        value="{{ old('tahun_ajaran') ?? ($tagihan['tahun_ajaran'] ?? '') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l16">Bukti Transfer
                                                    <span class="text-red">(*)</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input id="image" type="file" name="image"
                                                        accept="image/*,doc,docx,pdf">
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12 btn-list">
                                                            <button type="submit" class="btn btn-primary">
                                                                <i class="material-icons list-icon">save</i>
                                                                Simpan
                                                            </button>
                                                            @if ($message = Session::get('success'))
                                                            <a href="{{ route('sumbangan-spp') }}"
                                                                class="btn btn-info">Kembali</a>
                                                            @else
                                                            <a href="{{ route('pembayaran-konfirmasi', ['id' => $tagihan['kode']]) }}"
                                                                class="btn btn-info">Metode Lain</a>
                                                            @endif
                                                        </div>
                                                        <!-- /.col-sm-12 -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->

            @else
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="page-title">
                            <h1>Data Tidak ditemukan</h1>
                        </div>
                        <p class="mr-t-10 mr-b-20">Kode Transaksi tidak valid!</p><a href="javascript: history.back();"
                            class="btn btn-info btn-lg btn-rounded mr-b-20 ripple">Go Back</a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    @include('spp.includes.foot')
</body>

</html>
