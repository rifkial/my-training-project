@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Laporan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Tagihan</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <div class="modal fade bs-modal-lg" tabindex="-1" role="dialog" id="detail-info" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Info</h5>
                </div>
                <div class="modal-body">
                    <div class="row list-detail"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading">
                        <form name="filter_report" id="filter_report" method="POST" enctype="multipart/form-data"
                            action="{{ route('report-tagihan-post') }}">
                            @csrf
                            <div class="headersx">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Tahun Ajaran</label>
                                        <select class="form-control" id="tahun_ajaran" name="tahun_ajaran" required="">
                                            @if (empty($request_tahun))
                                                <option disabled="disabled" selected="true" value="">Pilih Tahun Ajaran
                                                </option>
                                            @endif

                                            @foreach ($tahun_ajaran as $key => $value)
                                                @php
                                                    $tahun = explode('/', $value['tahun_ajaran']);
                                                @endphp
                                                @if ($request_tahun == $tahun[0])
                                                    <option value="{{ $value['tahun_ajaran'] }}" selected="true">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @else
                                                    <option value="{{ $value['tahun_ajaran'] }}">
                                                        {{ $value['tahun_ajaran'] }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Rombongan Belajar</label>
                                        <select class="form-control" id="id_rombel" name="id_rombel" required="">
                                            @if (empty($request_rombel))
                                                <option disabled="disabled" selected="true" value="">Pilih Rombel</option>
                                            @endif

                                            @foreach ($rombelx as $key => $value)
                                                @if ($value['id'] == $request_rombel)
                                                    <option value="{{ $value['id'] }}" selected="true">
                                                        {{ $value['nama'] }} - {{ $value['jurusan'] }}
                                                    </option>
                                                @else
                                                    <option value="{{ $value['id'] }}">
                                                        {{ $value['nama'] }} - {{ $value['jurusan'] }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-lg btn-outline-default mt-4"><i
                                                class="fa fa-filter"></i></button>
                                        <button type="button" class="btn btn-lg btn-outline-default mt-4 printNota"><i
                                                class="fa fa-print"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table id="table_doc" class="table table-striped" data-toggle="table" width="100%;" border="1">
                                <thead class="table-info">
                                    <tr>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">No</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">NIS</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">Nama</td>

                                        <td colspan='{{ count($pos) }}' class="text-center"
                                            style="text-align: center; vertical-align: middle;">Tagihan</td>
                                        <td rowspan='2' class="text-center"
                                            style="text-align: center; vertical-align: middle;">Total</td>
                                    </tr>
                                    <tr>
                                        @php

                                            $listColumn = [];
                                            foreach ($report_tagihan as $t) {
                                                foreach ($t['tagihan'] as $m) {
                                                    array_push($listColumn, $m['nama']);
                                                }
                                            }
                                            $filteredList = array_unique($listColumn);
                                        @endphp
                                        @foreach ($filteredList as $v)
                                            <td class="text-center" style="text-align: center; vertical-align: middle;">
                                                {{ $v }}</td>
                                        @endforeach
                                    <tr>
                                </thead>
                                <tbody class="text-center">
                                    @if (count($report_tagihan) > 0)

                                        @foreach ($report_tagihan as $key => $valx)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $valx['nis'] }}</td>
                                                <td>{{ ucfirst($valx['nama']) }}</td>

                                                @php
                                                    $total_tagihan = 0;
                                                @endphp

                                                @foreach ($valx['tagihan'] as $x => $d)

                                                    @if (intval($d['ditagih']) == 0)
                                                        <td class="text-info">{{ number_format($d['ditagih'], 0) }}
                                                        </td>
                                                    @else
                                                        <td class="text-red">{{ number_format($d['ditagih'], 0) }}
                                                        </td>
                                                    @endif

                                                    @php
                                                        $total_tagihan += intval($d['ditagih']);
                                                    @endphp

                                                @endforeach

                                                <td colspan='{{ count($pos) }}' >{{ number_format($total_tagihan,0) }}</td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-red" colspan="4">Tagihan tidak ditemukan</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <script type="text/javascript">
        (function($, global) {
            $(document).ready(function(){
                //print nota
                $('body').on('click', '.printNota', function() {
                    var id = $('select[name="id_rombel"] option:selected').val();
                    var tahun = $('select[name="tahun_ajaran"] option:selected').val().split('/');
                    var name_tahun  = $('select[name="tahun_ajaran"] option:selected').text();
                    var name_kelas  = $('select[name="id_rombel"] option:selected').text();
                    var url = "{{ route('print_report_tagihan',['id'=>':id','tahun'=>':tahun','name_kelas'=>':name_kelas','name_tahun'=>':name_tahun']) }}";
                    url = url.replace(':id',id);
                    url = url.replace(':tahun',tahun[0]);
                    url = url.replace(':name_kelas',name_kelas);
                    url = url.replace(':name_tahun',name_tahun);
                    var str=url.toString();
                    str=str.split("%20").join("").replace(/\s+/g, "");
                    var a = document.createElement('a');
                    a.target = "_blank";
                    a.href = str;
                    a.click();
                });

                 //reset modal
                 $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

            });
        })(jQuery, window);
    </script>
@endsection
