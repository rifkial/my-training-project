@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Konfirmasi Pembayaran</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Konfirmasi</li>
                <li class="breadcrumb-item "><a href="{{ route('potongan-spp') }}">Keringanan</a></li>
                <li class="breadcrumb-item "><a href="{{ route('pemasukan-tagihan') }}">Tagihan</a></li>
                <li class="breadcrumb-item "><a href="{{ route('history-transaksi-pemasukan') }}">Histori</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- input -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ModalInput" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Form Konfirmasi Pembayaran</h5>
                </div>
                <form id="DocForm" name="DocForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l8">NIS (Siswa)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input class="form-control" name="nis_search" id="l8" placeholder="Search..."
                                        type="text">
                                    <span class="input-group-btn"><a class="btn btn-success" href="javascript: void(0);"
                                            id="search_nis"> <i class="fa fa-search"></i>
                                        </a></span>
                                </div>
                            </div>
                        </div>
                        <div class="result_filter"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update pos_konfirm" style="display: none;">
                            <i class="material-icons list-icon">save</i>
                            Simpan
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ModalShowTrans" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Detail Transaksi Konfirmasi</h5>
                </div>
                <div class="modal-body">
                    <div class="row print-data"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-rounded ripple text-left cekstatusorder" data-dismiss="modal">
                        Cek Status Pembayaran </button>
                    <button type="button" class="btn btn-warning btn-rounded ripple text-left reject" data-dismiss="modal">
                            Tolak Konfirmasi (Reject)</button>
                    <button type="button" class="btn btn-info btn-rounded ripple text-left approve" data-dismiss="modal">
                        Terima Konfirmasi (Approve)</button>
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-heading text-right">
                        <div class="headersx"></div>
                    </div>
                    <div class="widget-body clearfix">
                        <h5>Pembayaran</h5>
                        <table id="table_docx" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Total</th>
                                    <th>Metode</th>
                                    <th>Tanggal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Total</th>
                                    <th>Metode</th>
                                    <th>Tanggal</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script type="text/javascript">
        (function($, global) {
            "use-strict"
            var table_setting, table_settingx;
            let config_table, config_table2;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config_table2 = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah',
                                id: 'CreateKon'
                            },
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-paid-konfirm') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'kode',
                            name: 'kode'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'nis',
                            name: 'nis'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'bentuk',
                            name: 'bentuk'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_settingx = $('#table_docx').dataTable(config_table2);

                $('body').on('click', '#CreateKon', function() {
                    $('#ModalInput').modal('show');
                    $('input[name="nis_search"]').val('');
                    $('.result_filter').html('');
                });

                //filter by nis siswa
                $('body').on('click', 'a#search_nis', function() {
                    let printdata = '';
                    let list_bulan = '';
                    var search_nis = $('input[name="nis_search"]').val();
                    const loader = $('.result_filter');
                    if (search_nis == null || search_nis == '') {
                        window.notif('error', 'Form Search NIS Harus di isi!');
                    } else {
                        var url = '{{ route('ajax_siswa_search', ':id') }}';
                        url = url.replace(':id', search_nis);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            beforeSend: function() {
                                loader.html('<i class="fa fa-spin fa-spinner"><i>');
                            },
                            success: function(result) {
                                if (typeof result['data'] !==
                                    'underfined' &&
                                    typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        if (result['data'].length == 0) {
                                            loader.html('');
                                            window.notif('error', ' Data ' + search_nis +
                                                ' tidak  ditemukan ');
                                        } else {
                                            var rows = JSON.parse(JSON
                                                .stringify(
                                                    result['data']));
                                            var rows_profil = JSON.parse(JSON
                                                .stringify(
                                                    result['profil'][0]));

                                            var id_siswa = rows_profil.id;
                                            printdata = printdata +
                                                `<input type="hidden" name="id_siswa" value="` +
                                                id_siswa + `">`;

                                            printdata = printdata + `
                                            <div class="col-md-12">
                                                    <div class="row">
                                                      <div class="col-md-6">
                                                        <h5 class="box-title table-info"> <span class="ml-2">Informasi Siswa</span>
                                                        </h5>
                                                        <table class="table">
                                                            <tbody>
                                                                <tr width="100%;">
                                                                    <td>Nama</td>
                                                                    <td>:</td>
                                                                    <td>` + rows.nama + `</td>
                                                                </tr>
                                                                <tr width="100%;">
                                                                    <td>Kelas</td>
                                                                    <td>:</td>
                                                                    <td>` + rows.kelas + `</td>
                                                                </tr>
                                                                <tr width="100%;">
                                                                    <td>Rombongan Belajar</td>
                                                                    <td>:</td>
                                                                    <td>` + rows.rombel + `</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jurusan</td>
                                                                    <td>:</td>
                                                                    <td>` + rows.jurusan + `</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                      </div>
                                                      <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="l13">Bulan</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" id="bulan" name="bulan" required="">
                                                                    <option disabled selected value=""> Pilih Bulan </option>
                                                                    <option value="1">Juli</option>
                                                                    <option value="2">Agustus</option>
                                                                    <option value="3">September</option>
                                                                    <option value="4">Oktober</option>
                                                                    <option value="5">November</option>
                                                                    <option value="6">Desember</option>
                                                                    <option value="7">Januari</option>
                                                                    <option value="8">Februari</option>
                                                                    <option value="9">Maret</option>
                                                                    <option value="10">April</option>
                                                                    <option value="11">Mei</option>
                                                                    <option value="12">Juni</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label" for="l13">Pesan</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" id="pesan" name="pesan" required="">
                                                                    <option disabled selected value=""> Pilih Pesan </option>
                                                                     @foreach ($pesan as $key => $val)
                                                                         <option value="{{ $val['id'] }}">{{ $val['kategori'] }}</option>
                                                                     @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>`;

                                            printdata = printdata +
                                                `<div class="col-md-12">`;
                                            printdata = printdata + `
                                                    <h5 class="box-title table-info"><span class="ml-2"> Informasi Pembayaran</span> </h5>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <td>#</td>
                                                                <td>Nama Tagihan</td>
                                                                <td>Periode</td>
                                                                <td>Tahun Ajaran</td>
                                                                <td>Terakhir Pembayaran</td>
                                                                <td>Nominal</td>
                                                                <td>Total</td>
                                                            </tr>
                                                        </thead>
                                                    `;

                                            rows.tagihan.forEach(element => {
                                                var xnominal = '';
                                                var xtotal = '';
                                                if (isNaN(parseInt(parseBlankValue(
                                                        element['nominal'])))) {
                                                    xnominal = 0;
                                                } else {
                                                    xnominal = element['nominal']
                                                        .format();
                                                }

                                                if (isNaN(parseInt(parseBlankValue(
                                                        element[
                                                            'total_dibayar']
                                                    )))) {
                                                    xtotal = 0;
                                                } else {
                                                    xtotal = element[
                                                            'total_dibayar']
                                                        .format();
                                                }

                                                printdata = printdata + `
                                                            <tr>
                                                                <td>#</td>
                                                                <td>` + parseBlankValue(element['nama']) + `</td>
                                                                <td>` + parseBlankValue(element['priode']) + `</td>
                                                                <td>` + parseBlankValue(element['tahun_ajaran']) + `</td>
                                                                <td>` + parseBlankValue(element['terakhir_bayar']) + `</td>
                                                                <td>` + xnominal + `</td>
                                                                <td>` + xtotal + `</td>
                                                            </tr>
                                                        `;
                                            });

                                            printdata = printdata + `</table>`;

                                            $('.result_filter').html(printdata);

                                            $('button.pos_konfirm').show();

                                        }
                                    } else if (result['info'] == 'error') {
                                        loader.html('');
                                        window.notif('error', ' Data ' + search_nis +
                                            ' tidak  ditemukan ');
                                    }
                                }
                            }
                        });
                    }
                });

                //submit konfirmasi
                $('body').on('submit', '#DocForm', function(e) {
                    e.preventDefault();
                    var bulan = $('select[name="bulan"] option:selected').val();
                    var id_pesan = $('select[name="pesan"] option:selected').val();

                    if (bulan == '' && id_pesan == '') {
                        window.notif('error', 'Form Bulan dan Pesan Harus di isi!');
                    } else {
                        var urlx = '{{ route('store_konfirm_pesan') }}';
                        var formData = new FormData(this);
                        const loader = $('button.update');

                        $.ajax({
                            type: "POST",
                            url: urlx,
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(result) {
                                if (typeof result['data'] !== 'underfined' && typeof result[
                                        'info'] !== 'underfined') {
                                    if (result['info'] == 'success') {
                                        $('#transaksiModal').modal('hide');
                                        table_settingx.fnDraw(false);
                                        $(loader).html('<i class="fa fa-save"></i> Simpan');
                                        window.notif(result['info'], result['message']);

                                        $('#ModalInput').modal('hide');

                                    } else if (result['info'] == 'error') {
                                        $(loader).html('<i class="fa fa-save"></i> Simpan');
                                        window.notif(result['info'], result['message']);
                                    }
                                }
                            },
                            error: function(data) {
                                let log = "";
                                if (typeof data !== 'underfined') {
                                    let item = data['responseJSON']['errors'];
                                    for (var key in item) {
                                        //console.log(item[key])
                                        log = log + item[key];
                                    }
                                    window.notif('error', log);
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                }

                            }
                        });
                    }
                });

                $('body').on('click', 'button.show.konfirmasi', function() {
                    var id = $(this).data('id');
                    var tanggal_bayar = $(this).data('tanggal');
                    var url = '{{ route('detail-konfirmasi-trans', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let printdata = '';
                    let list_bulan = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            loader.html('<i class="fa fa-spin fa-spinner"><i>');
                        },
                        success: function(result) {
                            if (typeof result['data'] !==
                                'underfined' &&
                                typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    if (result['data'].length == 0) {
                                        loader.html('');
                                        window.notif('error', ' Data ' + search_nis +
                                            ' tidak  ditemukan ');
                                        loader.html('<i class="fa fa-eye"><i>');
                                    } else {
                                        $('#ModalShowTrans').modal('show');
                                        $('.print-data').html('');
                                        loader.html('<i class="fa fa-eye"><i>');
                                        var rows = JSON.parse(JSON
                                            .stringify(
                                                result['data']));

                                        window.appendDataDetail(rows, printdata,tanggal_bayar);

                                    }
                                } else if (result['info'] == 'error') {
                                    window.notif('error', ' Data ' + search_nis +
                                        ' tidak  ditemukan ');
                                    loader.html('<i class="fa fa-eye"><i>');
                                }
                            }
                        }
                    });
                });

                //apend data detail
                window.appendDataDetail = function detax(rows, printdata,tanggal_bayar) {
                    printdata = printdata + `
                                       <div class="col-md-5">
                                            <h5 class="box-title table-info"><span class="ml-2"> Informasi Siswa </span></h5>
                                            <table>
                                                <tbody>
                                                    <tr width="100%;">
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>` + ucwordx(rows.nama) + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>` + rows.kelas + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Rombel</td>
                                                        <td>:</td>
                                                        <td>` + rows.rombel + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan</td>
                                                        <td>:</td>
                                                        <td>` + rows.jurusan + `</td>
                                                    </tr>
                                                    <tr width="100%;">
                                                        <td>Sekolah</td>
                                                        <td>:</td>
                                                        <td>` + rows.sekolah + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                    printdata = printdata + `
                                       <div class="col-md-6">
                                            <h5 class="box-title table-info"><span class="ml-2">Informasi Transaksi</span> </h5>
                                            <table width="100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>Kode</td>
                                                        <td>:</td>
                                                        <td><small><b>` + rows.kode + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Metode Pembayaran </td>
                                                        <td>:</td>
                                                        <td><small><b>` + ucwordx(rows.bentuk).replace('_', ' ') + `</b></small></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Ajaran</td>
                                                        <td>:</td>
                                                        <td>` + rows.tahun_ajaran + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah Tagihan Dibayar</td>
                                                        <td>:</td>
                                                        <td>` + rows.jml_tagihan_dibayar + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td>:</td>
                                                        <td>` + parseInt(rows.nominal).format() + `</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Bayar</td>
                                                        <td>:</td>
                                                        <td>` + parseBlankValue(tanggal_bayar) + `</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>`;
                    printdata = printdata + `<div class="col-md-12">`;
                    printdata = printdata + `
                        <h5 class="box-title table-info">Informasi Pembayaran </h5>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nama Tagihan</td>
                                    <td>Nominal</td>
                                </tr>
                            </thead>
                        `;

                    rows.tagihan.forEach(element => {
                        printdata = printdata + `
                                                <tr>
                                                    <td>#</td>
                                                    <td>` + element['nama_tagihan'] + `</td>
                                                    <td>` + element['nominal'].format() + `</td>
                                                </tr>
                                            `;
                    });
                    printdata = printdata + `</table>`;

                    if (rows.file !== null) {
                        printdata = printdata +
                            ` <h5 class="box-title table-info"><span class="ml-2"> Informasi Bukti Transafer </span>  <a href="javascript:void(0)"  class="float-right mr-3 download file" data-src="` +
                            rows.file + `"><i class="fa fa-download"></i></a> </h5> <div class="notex"></div>`;
                        printdata = printdata + `<img src="` + rows.file +
                            `" class="image" width="200px" height="200px"> `;
                    }


                    printdata = printdata + `</div>`;
                    $('.print-data').html(printdata);
                    $('button.approve').attr('data-id', rows.id);
                    $('button.approve').attr('data-id_siswa', rows.id_kelas_siswa);
                    $('button.reject').attr('data-id', rows.id);
                    $('button.reject').attr('data-id_siswa', rows.id_kelas_siswa);

                    if(rows.token_vtweb == null){
                        $('button.cekstatusorder').hide();
                        $('button.reject').show();
                        $('.notex').html('<p class="text-red text-center"> Catatan : Apabila Bukti transfer tidak sesuai, Abaikan saja transaksi ini (Ditolak / Reject) !</p>');
                    }else{
                        $('button.cekstatusorder').show();
                        $('button.reject').hide();
                        $('button.cekstatusorder').attr('data-kodemitrans',rows.kode_midtrans);
                    }

                }

                 //reset modal
                 $('body').on('hidden.bs.modal', '.modal', function() {
                    $(this).removeData('bs.modal');
                });

                //status order
                $('body').on('click','button.cekstatusorder',function(){
                    var kode_midtrans = $(this).data('kodemitrans');
                    var urlx = '{{ route('cek-order', ['id'=>':id','role'=>':role']) }}';
                    urlx = urlx.replace(':id', kode_midtrans);
                    urlx = urlx.replace(':role', 'administrator');
                    var anchor = document.createElement('a');
                    anchor.href = urlx;
                    anchor.target ='_blank';
                    anchor.click();
                });

                //approve transaksi konfirmasi
                $('body').on('click', 'button.approve', function() {
                    var id = $(this).data('id');
                    var id_siswa = $(this).data('id_siswa');
                    var url = '{{ route('approve_transaction', ['id'=>':id','id_siswa'=>':id_siswa']) }}';
                    url = url.replace(':id', id);
                    url = url.replace(':id_siswa', id_siswa);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menerima transaksi  data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Terima Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //reject transaksi konfirmasi
                $('body').on('click', 'button.reject', function() {
                    var id = $(this).data('id');
                    var id_siswa = $(this).data('id_siswa');
                    var url = '{{ route('reject_transaction', ['id'=>':id','id_siswa'=>':id_siswa']) }}';
                    url = url.replace(':id', id);
                    url = url.replace(':id_siswa', id_siswa);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menolak transaksi  data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Tolak Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.reject(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //force donwload file bukti transfer
                $('body').on('click', 'a.download.file', function() {
                    var id = $(this).data('src');
                    var urlx = '{{ route('force_downloadx', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var anchor = document.createElement('a');
                    anchor.href = urlx;
                    anchor.click();

                });

                //reject
                window.reject = function reject(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_settingx.fnDraw(false);
                                    window.notif(result['info'], result['message']);
                                } else if (result['info'] == 'error') {
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_settingx.fnDraw(false);
                                    window.notif(result['info'], result['message']);
                                } else if (result['info'] == 'error') {
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                //parse blank
                window.parseBlankValue = function returnblank(item) {
                    if (item == null) {
                        return "-";
                    } else {
                        return item;
                    }
                }

                //window notif
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                String.prototype.reverse = function() {
                    return this.split("").reverse().join("");
                }

                window.currencyFormat = function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, ""); // Strip out all commas
                    x = x.reverse();
                    x = x.replace(/.../g, function(e) {
                        return e + ",";
                    }); // Insert new commas
                    x = x.reverse();
                    x = x.replace(/^,/, ""); // Remove leading comma
                    input.value = x;
                }

                //currensy
                Number.prototype.format = function(n, x) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                };

                //str upword
                window.ucwordx = function ucwords(str) {
                    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
                        return $1.toUpperCase();
                    });
                }

            });
        })(jQuery, window);
    </script>
@endsection
