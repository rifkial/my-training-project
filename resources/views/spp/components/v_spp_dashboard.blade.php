@extends('spp.apps')
@section('spp.components')
    <!-- Page Title Area -->
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    <!-- /.page-title -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        @if (session('role') == 'siswa' || session('role') == 'ortu')
                            @if (count($tagihan['tagihan']) > 0)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-outline-info">
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3">Data Siswa </h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody align="left">
                                                            <tr width="100%;">
                                                                <td>Nama</td>
                                                                <td>:</td>
                                                                <td>{{ ucwords($tagihan['nama']) }}</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Kelas</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['kelas'] }}</td>
                                                            </tr>
                                                            <tr width="100%;">
                                                                <td>Rombel</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['rombel'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jurusan</td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['jurusan'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sekolah</td>
                                                                <td>:</td>
                                                                <td>{{ session('sekolah') }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tahun Ajaran </td>
                                                                <td>:</td>
                                                                <td>{{ $tagihan['tahun_ajaran'] }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="col-md-12 mt-2">
                                                        <a href="{{ route('history-pembayaran') }}"
                                                            class="form-control"><i class="fa fa-eye"></i> Histori
                                                            Transaksi Pembayaran </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card card-outline-info">
                                            <div class="card-header">
                                                <h5 class="card-title mt-0 mb-3"> Detail Tagihan </h5>
                                            </div>
                                            <div class="card-body">
                                                @php
                                                    $total_tagihan = 0;
                                                @endphp
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr class="table-warning">
                                                                <td>No</td>
                                                                <td>Nama</td>
                                                                <td>Periode</td>
                                                                <td>Bulan</td>
                                                                <td>Nominal</td>
                                                                <td>Status</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($tagihan['tagihan'] as $key => $val)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{ $val['nama'] }}</td>
                                                                    <td>{{ $val['priode'] }}</td>
                                                                    <td>{{ $val['bulan'] }}</td>
                                                                    <td>{{ number_format($val['ditagih'], 0) }}</td>
                                                                    <td>
                                                                        @if (intval($val['ditagih']) == 0)
                                                                            <span class="text-success"> Lunas </span>
                                                                        @else
                                                                            <span class="text-danger">Pending</span>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @php
                                                                    $total_tagihan += intval($val['ditagih']);
                                                                @endphp
                                                            @endforeach
                                                        <tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td>Total</td>
                                                                <td>:</td>
                                                                <td>{{ number_format($total_tagihan, 0) }}</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert aler-info"> Bulan {{ $bulan }} tidak ada tagihan!.</div>
                            @endif
                        @elseif (session('role') =='tata-usaha')
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg">
                                    <div class="widget-body clearfix">
                                        <h5 class="box-title text-center"> GRAFIK REALISASI PENERIMAAN ANGGARAN <br> Periode
                                            Tahun {{ $tahun }}</h5>
                                        <canvas id="chartJsBar" height="150"></canvas>
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <div class="col-md-12 widget-holder">
                                <div class="widget-bg" id="divData">
                                    <div class="widget-body clearfix">
                                        <div class="text-center">
                                            <b>TABEL REALISASI PENERIMAAN ANGGARAN</b>
                                            <br>
                                            @if (!empty(session('sekolah')))
                                                <b>{{ session('sekolah') }}</b>
                                                <br>
                                            @endif
                                            <b>PERIODE Tahun {{ $tahun }} </b>
                                        </div>
                                        <hr>
                                        <div class="table-responsive">
                                            <table class="table  table-striped table-hover" id="tablex">
                                                <thead class="table-info">
                                                    <tr>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">No</td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">PENERIMAAN
                                                        </td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">TARGET</td>
                                                        <td colspan='{{ count($list_bulan) }}' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">REALISASI
                                                            PENERIMAAN</td>
                                                        <td rowspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">Total</td>
                                                    </tr>
                                                    <tr>
                                                        @foreach ($list_bulan as $bulan)
                                                            <td class="text-center"
                                                                style="text-align: center; vertical-align: middle;">
                                                                {{ $bulan }}
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $total_target = 0;
                                                        $sumArray_bulan = [];
                                                        $total_realisasi = 0;
                                                    @endphp
                                                    @if (count($realisasi) > 0)

                                                        @foreach ($realisasi as $key => $val)
                                                            @php
                                                                $total_target += intval($val['target']);
                                                                $total_realisasi += intval($val['realisasi']);
                                                            @endphp
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $val['nama'] }}</td>
                                                                <td>{{ number_format($val['target'], 0) }}</td>
                                                                @foreach ($val['bulan'] as $x => $k)
                                                                    @php
                                                                        if (!isset($sumArray_bulan[$x])) {
                                                                            $sumArray_bulan[$x] = 0;
                                                                        }
                                                                        $sumArray_bulan[$x] += $k['nominal'];
                                                                    @endphp
                                                                    <td>{{ number_format($k['nominal'], 0) }}</td>
                                                                @endforeach
                                                                <td>{{ number_format($val['realisasi']) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            <td>-</td>
                                                            @foreach ($list_bulan as $x => $bulan)
                                                                @php
                                                                    $sumArray_bulan[$x] = 0;
                                                                @endphp
                                                                <td>-</td>
                                                            @endforeach
                                                            <td>-</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr class="table-info">
                                                        <td colspan='2' class="text-center"
                                                            style="text-align: center; vertical-align: middle;">Total</td>
                                                        <td>{{ number_format($total_target, 0) }}</td>
                                                        @foreach ($sumArray_bulan as $p)
                                                            <td>{{ number_format($p, 0) }}</td>
                                                        @endforeach
                                                        <td>{{ number_format($total_realisasi, 0) }}</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.widget-list -->
    @if (session('role') == 'tata-usaha')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <script>
            (function($, global) {
                "use-strict"
                $(document).ready(function() {

                    var ctx = document.getElementById("chartJsBar");
                    if (ctx === null) return;

                    var ctx2 = document.getElementById("chartJsBar").getContext("2d");
                    var data2 = {
                        labels: @json($list_bulan),
                        datasets: [{
                            label: "Nominal",
                            backgroundColor: "#5867c3",
                            strokeColor: "#5867c3",
                            data: @json($data_chart)
                        }, ]
                    };

                    var chartJsBar = new Chart(ctx2, {
                        type: "bar",
                        data: data2,
                        options: {
                            legend: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                titleFontColor: "#000",
                                titleMarginBottom: 10,
                                backgroundColor: "rgba(255,255,255,.9)",
                                bodyFontColor: "#000",
                                borderColor: "#e9e9e9",
                                bodySpacing: 10,
                                borderWidth: 3,
                                xPadding: 10,
                                yPadding: 10,
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        return "Rp. " + Number(tooltipItem.yLabel).toFixed(0).replace(
                                            /./g,
                                            function(c, i, a) {
                                                return i > 0 && c !== "." && (a.length - i) % 3 ===
                                                    0 ? "," + c : c;
                                            });
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }],
                                yAxes: [{
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        callback: function(value, index, values) {
                                            if (parseInt(value) > 999) {
                                                return 'Rp.' + value.toString().replace(
                                                    /\B(?=(\d{3})+(?!\d))/g, ",");
                                            } else if (parseInt(value) < -999) {
                                                return '-Rp.' + Math.abs(value).toString()
                                                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                            } else {
                                                return 'Rp.' + value;
                                            }
                                        }
                                    }
                                }]
                            }
                        },
                        responsive: true
                    });


                });
            })(jQuery, window);
        </script>
    @endif
@endsection
