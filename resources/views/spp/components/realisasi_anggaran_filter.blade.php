@extends('spp.apps')
@section('spp.components')
<!-- Page Title Area -->
<div class="row page-title clearfix">
    <div class="page-title-left">
        <h5 class="mr-0 mr-r-5">Laporan</h5>
    </div>
    <!-- /.page-title-left -->
    <div class="page-title-right d-inline-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Realisasi Penerimaan Anggaran </li>
        </ol>
    </div>
    <!-- /.page-title-right -->
</div>
 <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-header text-center">
                        <b>LAPORAN REALISASI PENERIMAAN ANGGARAN</b>
                        <br>
                        @if (!empty(session('sekolah')))
                        <b>{{ session('sekolah') }}</b>
                        <br>
                        @endif
                        <b>Periode Bulan  {{ $nama_bulan }}  {{ $request_tahun }} </b>
                    </div>
                    <hr>
                    <div class="widget-body clearfix">
                        <div class="table-responsive">
                            <table class="table  table-striped table-hover">
                                <thead class="table-info">
                                    <tr>
                                        <td rowspan='2'
                                            style="text-align: left; vertical-align: middle;">No</td>
                                        <td rowspan='2'
                                            style="text-align: left; vertical-align: middle;">PENERIMAAN</td>
                                        <td rowspan='2'
                                            style="text-align: left; vertical-align: middle;">Nominal</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_nominal = 0;
                                    @endphp
                                    @foreach ($realisasi as $key => $val)
                                      @php
                                          $total_nominal += intval($val['nominal']);
                                      @endphp
                                     <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $val['nama'] }}</td>
                                        <td>{{ number_format($val['nominal'],0) }}</td>
                                     </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="table-info">
                                        <td colspan='2' class="text-left"
                                        style="text-align: left; vertical-align: middle;">Total</td>
                                        <td>{{ number_format($total_nominal,0) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
