<!-- User Details -->
<div class="side-user">
    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
        <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
            <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt="">
        </figure>
        <div class="media-body hide-menu">
            <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4>
        </div>
    </a>
    <div class="clearfix"></div>
    <ul class="nav in side-menu">
        @if (session('role') == 'siswa' || session('role') == 'ortu')
            <li><a href="{{ route('profilUser') }}"><i class="list-icon material-icons">account_box</i> My Profile</a>
            </li>
        @endif
        <li><a href="{{ route('auth.logout') }}"><i class="list-icon material-icons">settings_power</i> Logout</a>
        </li>
    </ul>
</div>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        @switch(session('role'))
            @case('tata-usaha')
                <li><a href="{{ route('sumbangan-spp') }}" class="ripple"><i
                            class="list-icon material-icons">dashboard</i> <span class="hide-menu">Dashboard</span></a>
                </li>
                <li><a href="{{ route('pesan_admin') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">mail_outline</i> <span
                            class="hide-menu">Pemberitahuan<span
                                class="badge badge-border badge-border-inverted bg-primary pull-right mr-3"><span
                                    class="notif-count-pesan admin text-info">0</span></span></span></a>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                            class="list-icon material-icons">widgets</i> <span class="hide-menu">Transaksi</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children"><a href="javascript:void(0);" aria-expanded="false">Pembayaran
                                SPP</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="height: 0px;">
                                <li><a href="{{ route('pemasukan-tagihan') }}">Tagihan </a></li>
                                <li><a href="{{ route('potongan-spp') }}">Keringanan</a></li>
                                <li><a href="{{ route('konfirmasi-transaksi') }}">Konfirmasi</a></li>
                                <li><a href="{{ route('history-transaksi-pemasukan') }}">Histori Pembayaran </a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                                aria-expanded="false"> Transaksi Lainnya </a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="#">Transaski Jurnal</a>
                                </li>
                                <li><a href="#">Transaski Hutang </a>
                                </li>
                                <li><a href="#">Transaski Piutang </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                            class="list-icon material-icons">book</i> <span class="hide-menu">Laporan</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="{{ route('report-tagihan') }}">Tagihan </a>
                        </li>
                        <li><a href="{{ route('report_realiasi') }}">Realisasi Penerimaan </a>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                                aria-expanded="false"> Akuntansi </a>
                            <ul class="list-unstyled sub-menu">
                                <li><a href="#">Jurnal Umum</a>
                                </li>
                                <li><a href="#">Buku Besar</a>
                                </li>
                                <li><a href="#">Cash Flow</a>
                                </li>
                                <li><a href="#">Laba/Rugi</a>
                                </li>
                                <li><a href="#">Neraca</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                            class="list-icon material-icons">widgets</i> <span class="hide-menu">Pengaturan</span></a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children"><a href="javascript:void(0);" aria-expanded="false">SPP</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="height: 0px;">
                                <li><a href="{{ route('setting-pos') }}">Pos</a>
                                </li>
                                <li><a href="{{ route('target_pos') }}">Pos Pemasukan </a>
                                </li>
                                <li><a href="{{ route('setting-tagihan') }}">Tagihan</a>
                                </li>
                                <li><a href="{{ route('setting-template-tagihan') }}">Bank</a>
                                </li>
                                <li><a href="{{ route('intergrasi-pembayaran') }}">Intergrasi Pembayaran</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('setting_pesan') }}">Pesan</a>
                        </li>
                        <li class="menu-item-has-children"><a href="javascript:void(0);" aria-expanded="false">Akuntansi</a>
                            <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="height: 0px;">
                                <li><a href="{{ route('akun_kategori_spp') }}">Akun Kategori </a></li>
                                <li><a href="{{ route('akun_subkategori_spp') }}">Akun Sub Kategori </a></li>
                                <li><a href="#">Rekening</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            @break
            @case('siswa')
                <li><a href="{{ route('tagihan-siswa') }}" class="ripple"><i
                            class="list-icon material-icons">report</i> <span class="hide-menu">Tagihan</span></a>
                </li>
                <li><a href="{{ route('history-pembayaran-spp') }}" class="ripple"><i
                            class="list-icon material-icons">book</i> <span class="hide-menu">Histori Pembayaran
                        </span></a>
                </li>
            @break
            @case('ortu')
                <li><a href="{{ route('pesan_ortu') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">mail_outline</i> <span
                            class="hide-menu">Pemberitahuan<span
                                class="badge badge-border badge-border-inverted bg-primary pull-right mr-3"><span
                                    class="notif-count-pesan text-info">0</span></span></span></a>
                </li>
                <li><a href="{{ route('tagihan-ortu') }}" class="ripple"><i
                            class="list-icon material-icons">report</i> <span class="hide-menu">Tagihan</span></a>
                </li>
                <li><a href="{{ route('history-pembayaran-ortu') }}" class="ripple"><i
                            class="list-icon material-icons">book</i> <span class="hide-menu">Histori Pembayaran
                        </span></a>
                </li>
            @break
        @endswitch
    </ul>
</nav>
<script type="text/javascript">
    (function($, global) {
        "use-strict"
        var urlb;
        $(document).ready(function() {

            //count pesan
            @switch(session('role'))
                @case('ortu')
                urlb = "{{ route('notifikasi_trans', ['id' => session('id_kelas_siswa')]) }}";
                @break
                @case('tata-usaha')
                urlb = "{{ route('notifikasiadmin_trans', ['id' => session('id')]) }}";
                @break
            @endswitch

            if (urlb != null) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: urlb,
                    beforeSend: function() {},
                    success: function(result) {
                        if (typeof result['data'] !== 'underfined' && typeof result[
                                'info'] !== 'underfined') {
                            if (result['info'] == 'success') {
                                $('span.notif-count-pesan').html(result['data']);
                            }
                        }
                    }
                });
            }
        });
    })(jQuery, window);
</script>
