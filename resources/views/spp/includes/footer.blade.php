@if (session('role') != 'superadmin')
    <footer class="footer text-center clearfix">
        {{ strtoupper(session('footer')) ?? strtoupper(session('sekolah')) ?? '2021 © ' }}
    </footer>
@else
    <footer class="footer text-center clearfix">2021 © Tim Dev Bumitekno</footer>
@endif

