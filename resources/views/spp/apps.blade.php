<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('spp.includes.head')
</head>

<body class="sidebar-light sidebar-expand ">
    <div id="wrapper" class="wrapper">
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header">
                <a href="{{ route('sumbangan-spp') }}" class="navbar-brand text-center">
                    <img class="logo-expand" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                        style="max-width: 25%;">
                    <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                        style="max-width: 80%;">
                    <span class="text-white logo-expand"> SPP Online </span>
                    <span class="text-white logo-collapse">&nbsp;SPP &nbsp;</span>
                </a>
            </div>
            <!-- /.navbar-header -->
            <!-- Left Menu & Sidebar Toggle -->
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i
                            class="material-icons list-icon">menu</i></a>
                </li>
            </ul>
            <!-- /.navbar-left -->
            <div class="spacer"></div>
            <ul class="nav navbar-nav d-none d-lg-block">
                @if(!empty($PUSHER_APP_KEY))
                <li class="dropdown dropdown-notifications"><a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false"><i class="material-icons list-icon">mail_outline</i> <span class="badge badge-border bg-primary" data-count="0" ><span class="notif-count">0</span></span></a>
                    <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-dark animated flipInY">
                        <div class="card">
                            <header class="card-header">New messages <span class="mr-l-10 badge badge-border badge-border-inverted bg-primary"><span class="notif-count">0</span></span>
                            </header>
                            <ul class="list-unstyled dropdown-list-group ps ps--theme_default ps--active-y list-message">
                             </ul>
                            <!-- /.dropdown-list-group -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.dropdown-menu -->
                </li>
                @endif
            </ul>
            <!-- User Image with Dropdown -->
            <ul class="nav navbar-nav">
                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle ripple"
                        data-toggle="dropdown"><span class="avatar thumb-sm">
                            @php
                                $role = str_replace('-',' ',ucfirst(session('role')));
                            @endphp
                            {{ $role }}
                            <img src="{{ session('avatar') }}" class="rounded-circle" alt=""> <i
                                class="material-icons list-icon">expand_more</i></span></a>
                    <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-wide">
                        <div class="card">
                            <header class="card-heading-extra">
                                <div class="row">
                                    <div class="col-8">
                                        <h3 class="mr-b-10 sub-heading-font-family fw-300">{{ session('username') }}
                                        </h3>
                                        <span class="user--online">{{ session('role') }}</span>
                                        @if (session('role') == 'siswa' || session('role') == 'ortu')
                                            <a href="{{ route('profilUser') }}"><i
                                                    class="list-icon material-icons">account_box</i> My
                                                Profile</a>
                                        @endif

                                    </div>
                                    <div class="col-4 d-flex justify-content-end">
                                        <a href="{{ route('auth.logout') }}" class="mr-t-10"><i
                                                class="material-icons list-icon">power_settings_new</i> Logout</a>
                                    </div>
                                    <!-- /.col-4 -->
                                </div>
                                <!-- /.row -->
                            </header>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- /.navbar-right -->
        </nav>
        <div class="content-wrapper">
            <aside class="site-sidebar scrollbar-enabled clearfix">
                @include('spp.includes.navbar')
            </aside>
            <main class="main-wrapper clearfix">
                @yield('spp.components')
            </main>
        </div>
        @if (session('role') != 'superadmin')
            <footer class="footer text-center clearfix">
                {{ strtoupper(session('footer')) ?? (strtoupper(session('sekolah')) ?? '2021 © ') }}
            </footer>
        @else
            <footer class="footer text-center clearfix">2021 © Tim Dev Bumitekno</footer>
        @endif
    </div>
    @include('spp.includes.foot')
</body>
