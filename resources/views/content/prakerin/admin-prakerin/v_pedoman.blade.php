@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == "university")
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="button mb-3">
                    <button class="btn btn-info" id="tambah_menu">Tambah Menu</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover widget-status-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Sub Menu Pedoman</th>
                                <th>File</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="data-pedoman">
                            @if (empty($pedoman))
                                <tr>
                                    <td colspan="5" style="text-align: center">Data saat ini kosong</td>
                                </tr>
                            @else
                                @php
                                    $nomer = 1;
                                @endphp
                                @foreach ($pedoman as $pd)
                                    <tr>
                                        <td>{{ $nomer++ }}</td>
                                        <td>{{ $pd['nama'] }}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm" data-id=""="{{ $pd['file'] }}"
                                                onclick="viewPDF({{ $pd['id'] }})">View PDF</button>
                                        </td>
                                        <td><span
                                                class="badge badge-{{ $pd['status'] == 1 ? 'success' : 'danger' }} text-inverse">{{ $pd['status'] == 1 ? 'Tampil' : 'Disembunyikan' }}</span>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" data-id="{{ $pd['id'] }}"
                                                class="edit"><i
                                                    class="material-icons list-icon md-18 text-info">edit</i></a>
                                            <a href="javascript:void(0)" onclick="deletePedoman({{ $pd['id'] }})"><i
                                                    class="material-icons list-icon md-18 text-danger">delete</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formMenu" name="formMenu" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_pedoman">
                                <div class="form-group">
                                    <label class="" for="l1">Nama Sub Menu Pedoman</label>
                                    <div class="input-group">
                                        <input type="text" name="nama" id="nama" autocomplete="off" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="" for="l1">File</label>
                                    <div class="input-group">
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="application/pdf" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="" for="l1">Status</label>
                                    <div class="input-group">
                                        <select name="status" id="status" class="form-control">
                                            <option value="0">Sembunyikan</option>
                                            <option value="1" selected>Tampilkan</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPdf" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingPreview"></h5>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="text-danger">*Harap matikan Internet Download Manager bila ingin melihat terlebih dahulu</span>
                            <div id="viewPdf">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#tambah_menu').click(function() {
                $('#formMenu').trigger("reset");
                $('#modelHeading').html("Tambah Menu Pedoman");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#formMenu', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_pedoman-create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_pedoman-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formMenu').trigger("reset");
                            $('#ajaxModel').modal('hide');
                        }
                        $('#data-pedoman').html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                const id = $(this).data('id');
                $('#form_result').html('');
                const loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_pedoman-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#modelHeading').html("Edit Menu Pedoman");
                        $('#id_pedoman').val(data.id);
                        $('#nama').val(data.nama);
                        $('#status').val(data.status).trigger('change');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        function viewPDF(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('pkl_pedoman-edit') }}",
                data: {
                    id
                },
                success: function(data) {
                    $("#HeadingPreview").html('Preview PDF');
                    var object =
                        '<object data="'+data.file+'" type="application/pdf" width="100%" height="500px">';
                    object +=
                        'If you are unable to view file, you can download from <a href ="'+data.file+'">here</a>';
                    object +=
                        ' or download <a target ="_blank" href ="http://get.adobe.com/reader/">Adobe PDF Reader</a> to view the file.';
                    object += '</object>';
                    $("#viewPdf").html(object);                    
                    $('#modalPdf').modal('show');
                }
            });
            
        }

        function deletePedoman(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('pkl_pedoman-delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    // beforeSend: function() {
                    //     $(".delete-" + id).html(
                    //         '<i class="fa fa-spin fa-spinner"></i> Loading');
                    // },
                    success: function(data) {
                        console.log(data);
                        $('#data-pedoman').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        // function restoreData(id) {
        //     swal(
        //         'Pulihkan?',
        //         'data anda akan kembali pulih',
        //         'question'
        //     ).then((lanjut) => {
        //         if (lanjut) {
        //             $.ajax({
        //                 url: "bidang_loker/restore/" + id,
        //                 type: "POST",
        //                 data: {
        //                     '_method': 'PATCH'
        //                 },
        //                 beforeSend: function() {
        //                     $(".restore-" + id).html(
        //                         '<i class="fa fa-spin fa-spinner"></i> Loading');
        //                 },
        //                 success: function(data) {
        //                     if (data.status == 'berhasil') {
        //                         swa("Berhasil!", data.message, data.success);
        //                         $('#data-tabel').dataTable().fnDraw(false);
        //                         $('#data-trash').dataTable().fnDraw(false);
        //                     } else {
        //                         swa("Gagal!", data.message, data.success);
        //                     }
        //                 }
        //             });
        //         } else {
        //             swal("Proses dibatalkan!");
        //         }
        //     });
        // }

        // function forceDelete(id) {
        //     swal({
        //         title: 'Apa anda yakin?',
        //         text: "Data anda nantinya tidak dapat dipulihkan lagi!",
        //         type: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!',
        //         cancelButtonText: 'No, cancel!',
        //         confirmButtonClass: 'btn btn-success',
        //         cancelButtonClass: 'btn btn-danger',
        //         buttonsStyling: false
        //     }).then(function() {
        //         $.ajax({
        //             url: "bidang_loker/hard_delete/" + id,
        //             type: "POST",
        //             data: {
        //                 '_method': 'DELETE'
        //             },
        //             beforeSend: function() {
        //                 $(".hardDelete-" + id).html(
        //                     '<i class="fa fa-spin fa-spinner"></i> Loading');
        //             },
        //             success: function(data) {
        //                 if (data.status == 'berhasil') {
        //                     swa("Berhasil!", data.message, data.success);
        //                     $('#data-tabel').dataTable().fnDraw(false);
        //                     $('#data-trash').dataTable().fnDraw(false);
        //                 } else {
        //                     swa("Gagal!", data.message, data.success);
        //                 }
        //             }
        //         })
        //     }, function(dismiss) {
        //         if (dismiss === 'cancel') {
        //             swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
        //         }
        //     })
        // }
    </script>
@endsection
