@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ Session::get('title') }}</h5>
                    @if (session('role') == 'admin-prakerin')
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info" id="addData"><i class="fas fa-plus-circle"></i> Tambah
                                    Data</button>
                                <button class="btn btn-purple importData"><i class="fas fa-file-upload"></i> Import File</button>
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-sm-9 col-md-9"></div>
                        <div class="col-sm-3 col-md-3">
                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    {{-- @php
                                        $serc = str_replace('-', ' ', $search);
                                    @endphp --}}
                                    <input type="text" value="" id="search" name="search" {{-- <input type="text" value="{{ $serc }}" id="search" name="search" --}}
                                        class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <a href="javascript:void(0)" id="fil" {{-- <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')" --}}
                                            class="btn btn-info"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div style="width: 100%;">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Pimpinan</th>
                                                <th>Telepon</th>
                                                <th>Email</th>
                                                <th>Alamat</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data_industri">
                                            @if (empty($industri))
                                                <tr>
                                                    <td colspan="7" class="text-center">Data saat ini belum tersedia</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($industri as $id)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $id['nama'] }}</td>
                                                        <td>{{ $id['pimpinan'] }}</td>
                                                        <td>{{ $id['telepon'] }}</td>
                                                        <td>{{ $id['email'] }}</td>
                                                        <td>{{ $id['alamat'] }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" data-toggle="collapse"
                                                                data-target="#demo{{ $id['id'] }}"
                                                                class="btn btn-info btn-sm accordion-toggle"><i
                                                                    class="fas fa-users-cog"></i></a>
                                                            @if (session('role') == 'admin-prakerin')
                                                                <a href="javascript:void(0)"
                                                                    class="btn btn-purple btn-sm edit"
                                                                    data-id="{{ $id['id'] }}"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="javascript:void(0)" data-id="{{ $id['id'] }}"
                                                                    class=" delete btn btn-danger btn-sm"><i
                                                                        class="fas fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="hiddenRow">
                                                            <div class="accordian-body collapse"
                                                                id="demo{{ $id['id'] }}">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <h5 class="box-title my-3">Pembimbing Industri</h5>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        @if (session('role') == 'admin-prakerin')
                                                                            <button
                                                                                class="btn btn-info btn-sm my-3 pull-right"
                                                                                onclick="tambahPembimbing({{ $id['id'] }})"><i
                                                                                    class="fas fa-plus-circle"></i>
                                                                                Tambah Pembimbing</button>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr class="bg-info">
                                                                            <th class="text-center">No</th>
                                                                            <th class="text-center">Nama</th>
                                                                            <th class="text-center">Telepon</th>
                                                                            <th class="text-center">Email</th>
                                                                            <th class="text-center">Alamat</th>
                                                                            @if (session('role') == 'admin-prakerin')
                                                                                <th class="text-center">Opsi</th>
                                                                            @endif
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="data_pembimbing{{ $id['id'] }}">
                                                                        @if (empty($id['pembimbing']))
                                                                            <tr>
                                                                                <td colspan="6" class="text-center">Data
                                                                                    pembimbing saat ini tidak tersedia</td>
                                                                            </tr>
                                                                        @else
                                                                            @php
                                                                                $nomer = 1;
                                                                            @endphp
                                                                            @foreach ($id['pembimbing'] as $pb)
                                                                                <tr>
                                                                                    <td>{{ $nomer++ }}</td>
                                                                                    <td>{{ Str::upper($pb['nama']) }}
                                                                                    </td>
                                                                                    <td>{{ $pb['telepon'] }}</td>
                                                                                    <td>{{ $pb['email'] }}</td>
                                                                                    <td>{{ $pb['alamat'] }}</td>
                                                                                    @if (session('role') == 'admin-prakerin')
                                                                                        <td>
                                                                                            <a href="javascript:void(0)"
                                                                                                class="btn btn-purple btn-sm editPembimbing"
                                                                                                data-id="{{ $pb['id'] }}"
                                                                                                data-industri="{{ $id['id'] }}"><i
                                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                                            <a href="javascript:void(0)"
                                                                                                data-id="{{ $pb['id'] }}"
                                                                                                data-industri="{{ $id['id'] }}"
                                                                                                class="deletePembimbing btn btn-danger btn-sm"><i
                                                                                                    class="fas fa-trash"></i></a>
                                                                                        </td>
                                                                                    @endif
                                                                                </tr>
                                                                            @endforeach
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_industri">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" autocomplete="off" id="telepon"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" autocomplete="off" id="email"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pimpinan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" name="pimpinan" id="pimpinan"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Provinsi</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="provinsi" id="provinsi" class="form-control">
                                                <option value="">PILIH PROVINSI...</option>
                                                @foreach ($provinsi as $pro)
                                                    <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Kabupaten</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="kabupaten" id="kabupaten" class="form-control" disabled>
                                                <option value="">Pilih Provinsi dulu..</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPembimbing" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingPembimbing"></h5>
                </div>
                <form id="formPembimbing" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="pm_id_pembimbing">
                                <input type="hidden" name="id_industri" id="pm_id_industri">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="pm_nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="pm_telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <img id="modal-previews" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_fotos" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURLS(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="pm_email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="pm_jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="pm_tempat_lahir"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="pm_tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="pm_alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionPembimbing" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"
                            id="btnPembimbing">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Pelamar</h5>
                </div>
                <form id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" style="cursor: pointer" class="text-success">
                                            <i class="material-icons text-success">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('click', '.importData', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Perusahaan');
            });

            $('select[name="provinsi"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $('select[name="kabupaten"]').attr('disabled', 'disabled')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('get_kabupaten-provinsi') }}",
                        dataType: "json",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('select[name="kabupaten"]').html(
                                '<option value="">MEMPROSES KABUPATEN...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">PILIH KABUPATEN..</option>';
                            $.each(data, function(k, v) {
                                s += '<option value="' + v.id + '">' + v.name +
                                    '</option>';
                            })
                            $('select[name="kabupaten"]').removeAttr('disabled');
                            $('select[name="kabupaten"]').html(s)
                        }
                    });
                }
            })

            $('#addData').click(function() {
                $("#kabupaten").attr("disabled", true);
                $('#kabupaten').html("<option>Pilih Provinsi dulu..</option>");
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Industri");

                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_industri-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_industri-update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data_industri').html(data.industri);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#formPembimbing', function(e) {
                e.preventDefault();
                $('#btnPembimbing').html('Sending..');
                let id_industri = $('#pm_id_industri').val();
                var action_url = '';

                if ($('#actionPembimbing').val() == 'Add') {
                    action_url = "{{ route('pkl_industri-simpan_pembimbing') }}";
                }

                if ($('#actionPembimbing').val() == 'Edit') {
                    action_url = "{{ route('pkl_industri-update_pembimbing') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formPembimbing').trigger("reset");
                            $('#modalPembimbing').modal('hide');
                            $('#data_pembimbing' + id_industri).html(data.pembimbing);
                        }
                        noti(data.icon, data.message);
                        $('#btnPembimbing').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



            $(document).on('click', '.editPembimbing', function() {
                let id = $(this).data('id');
                let id_industri = $(this).data('industri');
                const loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_pembimbing_industri-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        console.log(data);
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#headingPembimbing').html("Edit Pembimbing");
                        $('#pm_id_pembimbing').val(data.id);
                        $('#pm_id_industri').val(data.id_industri).trigger('change');
                        $('#pm_nama').val(data.nama);
                        $('#pm_telepon').val(data.telepon);
                        $('#pm_email').val(data.email);
                        $('#pm_tempat_lahir').val(data.tempat_lahir);
                        $('#pm_alamat').val(data.alamat);
                        $('#pm_tanggal_lahir').val(data.tgl_lahir);
                        $('#pm_jenkel').val(data.jenkel).trigger('change');
                        $('#delete_fotos').html('');
                        if (data.file) {
                            $('#modal-previews').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#actionPembimbing').val('Edit');
                        $('#modalPembimbing').modal('show');
                    }
                });
            });
            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                const loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_industri-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        console.log(data);
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Industri");
                        $('#id_industri').val(data.id);
                        $('#nama').val(data.nama);
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#provinsi').val(data.id_provinsi).trigger('change');
                        $('#alamat').val(data.alamat);
                        $('#pimpinan').val(data.pimpinan);
                        edit_kabupaten(data.id_provinsi, data.id_kabupaten);
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'factory.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_industri-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_industri').html(data.industri);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
            $(document).on('click', '.deletePembimbing', function() {
                var id = $(this).data('id');
                var id_industri = $(this).data('industri');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_industri-delete_pembimbing') }}",
                        type: "POST",
                        data: {
                            id,
                            id_industri
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pembimbing' + id_industri).html(data
                                    .pembimbing);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importFile"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-industri_prakerin') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        swa(data.status+"!", data.message, data.success);
                    }
                });
            });
        });

        function edit_kabupaten(id_provinsi, id_kabupaten) {
            $("#kabupaten").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_industri-load_kabupaten_by_industri') }}",
                type: "POST",
                data: {
                    id_provinsi,
                    id_kabupaten
                },
                beforeSend: function() {
                    $('#kabupaten').html('<option value="">Memproses Kabupaten..</option>');
                },
                success: function(fb) {
                    $('#kabupaten').html('');
                    $('#kabupaten').html(fb);
                    $("#kabupaten").attr("disabled", false);
                }
            });
            return false;
        }

        function tambahPembimbing(id_industri) {
            $('#formPembimbing').trigger("reset");
            $('#headingPembimbing').html("Tambah Data Pembimbing");
            $('#pm_id_industri').val(id_industri);
            $('#modalPembimbing').modal('show');
            $('#actionPembimbing').val('Add');
        }
    </script>
@endsection
