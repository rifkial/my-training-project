@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-md-6 col-12 mb-2">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        </div>
                        <div class="col-md-12">
                            <div class="addMutasi">
                                <a href="{{ route('pkl_siswa-tambah') }}" class="btn btn-info"><i
                                        class="fas fa-plus-circle"></i> Tambah Peserta</a>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9"></div>
                        <div class="col-sm-3 col-md-3">
                            <form class="navbar-form" action="javascript:void(0)" role="search">
                                <div class="input-group">
                                    @php
                                        $serc = str_replace('-', ' ', $search);
                                    @endphp
                                    <input type="text" value="{{ $serc }}" id="search" name="search"
                                        class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button id="fil" onclick="filter('{{ $routes }}')" type="submit"
                                            class="btn btn-info"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>Rombel</th>
                                        <th>Jurusan</th>
                                        <th>Industri</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_siswa">
                                    @if (!empty($siswa))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $sw)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $sw['nama_siswa'] }}</td>
                                                <td>{{ $sw['nis'] }}</td>
                                                <td>{{ $sw['nama_rombel'] }}</td>
                                                <td>{{ $sw['nama_jurusan'] }}</td>
                                                <td>{{ $sw['industri'] }}</td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)"
                                                        class="btn btn-info btn-sm accordion-toggle" data-toggle="collapse"
                                                        data-target="#demo{{ $sw['id'] }}"><i
                                                            class="fas fa-info-circle"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $sw['id'] }}"
                                                        class="btn btn-success btn-sm edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $sw['id'] }}"
                                                        class="btn btn-danger btn-sm delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                                        <table class="table">
                                                            <tr>
                                                                <th>Nama</th>
                                                                <td>{{ $sw['nama_siswa'] }}</td>
                                                                <td></td>
                                                                <th>Email</th>
                                                                <td>{{ $sw['email_siswa'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>NIS</th>
                                                                <td>{{ $sw['nis'] }}</td>
                                                                <td></td>
                                                                <th>Jurusan</th>
                                                                <td>{{ $sw['nama_jurusan'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Industri</th>
                                                                <td>{{ $sw['industri'] }}</td>
                                                                <td></td>
                                                                <th>Pembimbing Industri</th>
                                                                <td>{{ $sw['nama_pembimbing_industri'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Pembimbing Sekolah</th>
                                                                <td>{{ $sw['nama_pembimbing_sekolah'] }}</td>
                                                                <td></td>
                                                                <th>Kelompok Prakerin</th>
                                                                <td>{{ $sw['nama_kelompok'] }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Mulai Prakerin</th>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_mulai']) }}
                                                                </td>
                                                                <td></td>
                                                                <th>Selesai Prakerin</th>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($sw['tgl_selesai']) }}
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="7">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {!! $pagination !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalSiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit Peserta Prakerin</h5>
                </div>
                <form id="formSiswa" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_siswa">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Rombel</label>
                                    <div class="col-sm-12">
                                        <select id="rombel" class="form-control">
                                            <option value="">Pilih Rombel..</option>
                                            @foreach ($rombel as $rb)
                                                <option value="{{ $rb['id'] }}">{{ $rb['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Siswa</label>
                                    <div class="col-sm-12">
                                        <select name="id_kelas_siswa" id="id_kelas_siswa" class="form-control" disabled>
                                            <option value="" selected>Pilih Rombel dahulu...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Industri</label>
                                    <div class="col-sm-12">
                                        <select name="industri" id="industri" class="form-control">
                                            <option value="">Pilih Industri...</option>
                                            @foreach ($industri as $ind)
                                                <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pembimbing Industri</label>
                                    <div class="col-sm-12">
                                        <select name="pembimbing_industri" id="pembimbing_industri" class="form-control"
                                            disabled>
                                            <option value="">Pilih Industri dahulu...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kelompok Prakerin</label>
                                    <div class="col-sm-12">
                                        <select name="kelompok" id="kelompok" class="form-control" disabled>
                                            <option value="" selected>Pilih Industri dahulu...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Guru Pembimbing</label>
                                    <div class="col-sm-12">
                                        <select name="guru" id="guru" class="form-control">
                                            <option value="">Pilih Guru...</option>
                                            @foreach ($guru as $gr)
                                                <option value="{{ $gr['id'] }}">
                                                    {{ $gr['nama'] . ' NIP ' . $gr['nip'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Mulai</label>
                                    <div class="col-sm-12">
                                        <input type="date" name="tgl_mulai" id="tgl_mulai" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Selesai</label>
                                    <div class="col-sm-12">
                                        <input type="date" name="tgl_selesai" id="tgl_selesai" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="tahun_ajar" id="tahun_ajar" class="form-control">
                                            <option value="">Pilih tahun ajaran...</option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['id'] }}">
                                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formSiswa').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pkl_siswa-update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formSiswa').trigger("reset");
                            $('#modalSiswa').modal('hide');
                            location.reload()
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_siswa-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#id_siswa').val(data.id);
                        $('#tahun_ajar').val(data.id_ta_sm).trigger('change');
                        $('#guru').val(data.id_pemb_sekolah).trigger('change');
                        $('#tgl_mulai').val(data.tgl_mulai);
                        $('#tgl_selesai').val(data.tgl_selesai);
                        $('#industri').val(data.id_industri).trigger('change');
                        $('#rombel').val(data.id_rombel).trigger('change');
                        load_pembimbing_kelompok(data.id_industri, data.id_pemb_industri, data
                            .id_prakerin_kelompok);
                        load_kelas_siswa(data.id_rombel, data.id_kelas_siswa);
                        $('#modalSiswa').modal('show');

                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_siswa-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload()
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('#rombel').on('change', function() {
                let id = $(this).val();
                if (id) {
                    $('#id_kelas_siswa').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('rombel-kelas_siswa') }}",
                        type: "POST",
                        data: {
                            id_rombel: id
                        },
                        beforeSend: function() {
                            $('#id_kelas_siswa').html(
                                '<option value="">Memproses data Siswa...</option>');
                        },
                        success: function(data) {
                            let s = '<option value="">--Pilih Siswa--</option>';
                            // data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    ' NISN ' + val.nisn + '</option>';
                            })
                            $('#id_kelas_siswa').removeAttr('disabled')
                            $('#id_kelas_siswa').html(s);
                        }
                    });
                }
            })

            $('#industri').on('change', function() {
                // console.log("pilih industri");
                var id = $(this).val();
                if (id) {
                    $('#pembimbing_industri').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('pkl_pembimbing_industri-by_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#pembimbing_industri').html(
                                '<option value="">Memproses data Industri...</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Pembimbing--</option>';
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#pembimbing_industri').removeAttr('disabled')
                            $('#pembimbing_industri').html(s);

                        }
                    });
                    $.ajax({
                        url: "{{ route('pkl_kelompok-by_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#kelompok').html(
                                '<option value="">Memproses data...</option>'
                            );
                        },
                        success: function(response) {
                            if (!$.trim(response)) {
                                $('#kelompok').html(
                                    '<option value="">--- No Kelompok ---</option>'
                                );
                            } else {
                                var input =
                                    '<option value="">Pilih Kelompok</option>';
                                // data = JSON.parse(data);
                                response.forEach(function(rows) {
                                    input += '<option value="' +
                                        rows.id + '">' + rows
                                        .nama +
                                        '</option>';

                                })
                                $('#kelompok').removeAttr(
                                    'disabled');
                            }
                            $('#kelompok').html(input);
                        }
                    });
                }
            })


        });

        function load_kelas_siswa(id_rombel, id_kelas_siswa) {
            $('select[name="id_kelas_siswa"]').attr("disabled", true);
            $.ajax({
                url: "{{ route('load_kelas_siswa-by_rombel') }}",
                type: "POST",
                data: {
                    id_rombel,
                    id_kelas_siswa
                },
                beforeSend: function() {
                    $('select[name="id_kelas_siswa"]').html('<option value="">Memproses data...</option>');
                },
                success: function(fb) {
                    $('select[name="id_kelas_siswa"]').html('');
                    $('select[name="id_kelas_siswa"]').html(fb);
                    $('select[name="id_kelas_siswa"]').attr("disabled", false);
                }
            });
            return false;
        }




        function load_pembimbing_kelompok(id_industri, id_pemb_industri, id_kelompok) {
            $('select[name="pembimbing_industri"]').attr("disabled", true);
            $('select[name="kelompok"]').attr("disabled", true);
            $.ajax({
                url: "{{ route('load_kelompok_pemb_industri-by_industri') }}",
                type: "POST",
                data: {
                    id_industri,
                    id_pemb_industri,
                    id_kelompok
                },
                beforeSend: function() {
                    $('select[name="pembimbing_industri"]').html('<option value="">Memproses data...</option>');
                    $('select[name="kelompok"]').html('<option value="">Memproses data...</option>');
                },
                success: function(data) {
                    $('select[name="pembimbing_industri"]').html(data.pembimbing);
                    $('select[name="kelompok"]').html(data.kelompok);
                    $('select[name="pembimbing_industri"]').attr("disabled", false);
                    $('select[name="kelompok"]').attr("disabled", false);
                }
            });
            return false;
        }


        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
