@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == "university")
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-success refresh pull-right"><i class="fa fa-sync"></i> Refresh
                                Page</button>
                            <a class="btn btn-danger pull-right mx-2" href="{{ route('pkl_nilai-beranda') }}"><i
                                    class="fa fa-arrow-circle-left"></i> Nilai</a>
                        </div>
                    </div>
                    @if (session('role') != 'supervisor')
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <hr>
                                <button class="btn btn-info addJenis"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped widget-status-table mr-b-0 hover">
                            <thead>
                                <tr class="">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Jurusan yang terhubung</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_jenis">
                                @if (!empty($jenis))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($jenis as $jn)
                                        <tr>
                                            <td class="text-center">{{ $no }}</td>
                                            <td class="text-center">{{ $jn['nama_jenis'] }}</td>
                                            <td class="text-center"><small
                                                    class="text-{{ $jn['jumlah'] > 0 ? 'info' : 'danger' }}">{{ $jn['jumlah'] }}
                                                    Jurusan yang terhubung</small></td>
                                            <td class="text-center">
                                                <a href="javascript:void(0)" class="btn-sm accordion-toggle text-info"
                                                    data-toggle="collapse" data-target="#demo{{ $no }}"><i
                                                        class="fas fa-info-circle"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="hiddenRow">
                                                <div class="accordian-body collapse" id="demo{{ $no }}">
                                                    <table class="table">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>Nomer</th>
                                                                <th>Nama Jurusan</th>
                                                                @if (session('role') != 'supervisor')
                                                                    <th>Opsi</th>
                                                                @endif
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tabel_{{ str_replace(' ', '_', $jn['nama_jenis']) }}">
                                                            @if (!empty($jn['jenis_nilai']))
                                                                @php
                                                                    $nomer = 1;
                                                                @endphp
                                                                @foreach ($jn['jenis_nilai'] as $jenis)
                                                                    <tr>
                                                                        <td>{{ $nomer++ }}</td>
                                                                        <td>{{ $jenis['jurusan'] }}</td>
                                                                        @if (session('role') != 'supervisor')
                                                                            <td>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $jenis['id'] }}"
                                                                                    data-jenis="{{ str_replace(' ', '_', $jn['nama_jenis']) }}"
                                                                                    class="edit text-info">
                                                                                    <i class="fas fa-pencil-alt"></i></a>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $jenis['id'] }}"
                                                                                    data-jenis="{{ str_replace(' ', '_', $jn['nama_jenis']) }}"
                                                                                    class="delete text-danger">
                                                                                    <i class="fas fa-trash"></i></a>
                                                                            </td>
                                                                        @endif
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td class="text-center" colspan="3">Data saat ini
                                                                        tidak
                                                                        tersedia</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="updateModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Jenis</h5>
                </div>
                <form id="formUpdate" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_jenis">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jurusan</label>
                                    <div class="col-sm-12">
                                        <select name="jurusan" id="jurusan" class="form-control">
                                            <option value="">--Pilih jurusan--</option>
                                            @foreach ($jurusan as $jr)
                                                <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Jenis Nilai</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="addModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Tambah Jenis</h5>
                </div>
                <form id="formAddJenis" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jurusan</label>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            @if (!empty($jurusan))
                                                @foreach ($jurusan as $jr)
                                                    <div class="col-md-4">
                                                        <input class="form-check-input mx-0" type="checkbox"
                                                            name="id_jurusan[]" value="{{ $jr['id'] }}"
                                                            id="flexCheckDefault">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            {{ $jr['nama'] }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="col-md-12">
                                                    <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show"
                                                        role="alert"><i class="material-icons list-icon">not_interested</i>
                                                        <strong>Oh Maaf!</strong> Anda belum mempunyai jurusan, silahkan
                                                        tambahkan jurusan terlebih dulu untuk menggunakan fitur ini.
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Jenis Nilai</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="nama[]" value="" required>
                                    </div>
                                </div>
                                <div class="fomAddJenis"></div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="addBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.addJenis').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Jenis Nilai");
                $('.tambahBaris').show('');
                $('#addModal').modal('show');
                $('#action').val('Add');
            });
            $('.refresh').click(function() {
                $('.refresh').html(
                    '<i class="fa fa-spin fa-spinner"></i> Merefresh Halaman...');
                location.reload();
            });

            $('#formUpdate').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();

                $.ajax({
                    url: "{{ route('pkl_jenis_nilai-update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formUpdate').trigger("reset");
                            $('#updateModal').modal('hide');
                            $('#data_jenis').html(data.jenis);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
            $('#formAddJenis').on('submit', function(event) {
                $('#addBtn').html('Sending..');
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_jenis_nilai-create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formAddJenis').trigger("reset");
                            $('#addModal').modal('hide');
                            $('#data_jenis').html(data.jenis);
                        }
                        noti(data.icon, data.message);
                        $('#addBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_jenis_nilai-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#id_jenis').val(data.id);
                        $('#nama').val(data.nama);
                        $('#jurusan').val(data.id_jurusan).trigger('change');
                        $('#updateModal').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let jenis = $(this).data('jenis');
                let loader = $(this);
                // alert(jenis);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_jenis_nilai-delete') }}",
                        type: "POST",
                        data: {
                            id,
                            jenis
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            // console.log(data);
                            if (data.status == 'berhasil') {
                                $('#tabel_' + jenis).html(data.jenis);
                            }
                            $(loader).html('<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })

            });
        });

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddJenis').append(
                ' <div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama Jensi Nilai</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
