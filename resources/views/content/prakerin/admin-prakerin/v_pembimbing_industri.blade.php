@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == "university")
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ Session::get('title') }}</h5>
                    @if (session('role') == 'admin-prakerin')
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info" id="addData"><i class="fas fa-plus-circle"></i> Tambah
                                    Data</button>
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div style="width: 100%;">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Industri</th>
                                                <th>Telepon</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data_pembimbing">
                                            @if (empty($pembimbing))
                                                <tr>
                                                    <td colspan="7" class="text-center">Data saat ini belum tersedia</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($pembimbing as $pb)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $pb['nama'] }}</td>
                                                        <td>{{ $pb['industri'] }}</td>
                                                        <td>{{ $pb['telepon'] }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" data-toggle="collapse"
                                                                data-target="#demo{{ $pb['id'] }}"
                                                                class="btn btn-info btn-sm accordion-toggle"><i
                                                                    class="fas fa-award"></i></a>
                                                            @if (session('role') == 'admin-prakerin')
                                                                <a href="{{ route('pkl_pembimbing_industri-detail_edit', ['k' => (new \App\Helpers\Help())->encode($pb['id']), 'name' => str_slug($pb['nama'])]) }}"
                                                                    class="btn btn-purple btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="javascript:void(0)" data-id="{{ $pb['id'] }}"
                                                                    class=" delete btn btn-danger btn-sm"><i
                                                                        class="fas fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="hiddenRow">
                                                            <div class="accordian-body collapse"
                                                                id="demo{{ $pb['id'] }}">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <h5 class="box-title my-3">Template Sertifikat</h5>
                                                                    </div>
                                                                </div>
                                                                <table class="table table-striped">
                                                                    <thead id="head_tabel{{ $pb['id'] }}">
                                                                        <tr
                                                                            class="{{ empty($pb['template_sertifikat']) ? 'bg-danger' : 'bg-info' }}">
                                                                            <th class="text-center">Template</th>
                                                                            <th class="text-center">Opsi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="data_sertifikat{{ $pb['id'] }}">
                                                                        @if (empty($pb['template_sertifikat']))
                                                                            <tr>
                                                                                <td colspan="2" class="text-center">
                                                                                    Template saat ini tidak tersedia. <br>
                                                                                    <button data-id="{{ $pb['id'] }}"
                                                                                        class="btn btn-success btn-sm set_default"><i
                                                                                            class="fas fa-wrench"></i> Set
                                                                                        Default</button>
                                                                                    <small class="text-danger">*Untuk
                                                                                        mengeset sertifikat sekarang</small>
                                                                                </td>
                                                                            </tr>
                                                                        @else
                                                                            @foreach ($pb['template_sertifikat'] as $template)
                                                                                <tr>
                                                                                    <td class="text-center">
                                                                                        {{ $template['template'] }}</td>
                                                                                    <td class="text-center">
                                                                                        <a href="{{ route('prakerin_template-detail', $template['id_template']) }}"
                                                                                            target="_blank"
                                                                                            class="btn btn-purple btn-sm editPembimbing"
                                                                                            data-id="{{ $pb['id'] }}"
                                                                                            data-industri="{{ $pb['id'] }}"><i
                                                                                                class="fas fa-info-circle"></i></a>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>






    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_pembimbing">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Industri</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="id_industri" id="id_industri" class="form-control">
                                                <option value="">--Pilih Industri--</option>
                                                @foreach ($industri as $ind)
                                                    <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.set_default', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_pembimbing_industri-set_template') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-bolt"></i> Set Default');
                        $('#data_sertifikat' + id).html(data.html.template);
                        $('#head_tabel' + id).html(data.html.head);
                        noti(data.icon, data.message);
                    }
                });
            });



            $('#addData').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Pembimbing Industri");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });


            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pkl_pembimbing_industri-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pkl_pembimbing_industri-update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data_pembimbing').html(data.pembimbing);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_pembimbing_industri-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {

                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Pembimbing Industri");
                        $('#id_pembimbing').val(data.id);
                        $('#id_industri').val(data.id_industri).trigger('change');
                        $('#nama').val(data.nama);
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#alamat').val(data.alamat);
                        $('#tanggal_lahir').val(data.tgl_lahir);
                        $('#jenkel').val(data.jenkel).trigger('change');
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('pkl_pembimbing_industri-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pembimbing').html(data.pembimbing);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

    </script>
@endsection
