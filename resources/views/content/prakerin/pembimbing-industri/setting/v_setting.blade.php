@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        div.clear {
            clear: both;
        }

        div.product-chooser {}

        div.product-chooser.disabled div.product-chooser-item {
            zoom: 1;
            filter: alpha(opacity=60);
            opacity: 0.6;
            cursor: default;
        }

        div.product-chooser div.product-chooser-item {
            padding: 11px;
            border-radius: 6px;
            cursor: pointer;
            position: relative;
            border: 1px solid #efefef;
            margin-bottom: 10px;
            margin-left: 10px;
            margin-right: 10x;
        }

        div.product-chooser div.product-chooser-item.selected {
            border: 4px solid #428bca;
            background: #efefef;
            padding: 8px;
            filter: alpha(opacity=100);
            opacity: 1;
        }

        div.product-chooser div.product-chooser-item img {
            padding: 0;
        }

        div.product-chooser div.product-chooser-item span.title {
            display: block;
            margin: 10px 0 5px 0;
            font-weight: bold;
            font-size: 12px;
        }

        div.product-chooser div.product-chooser-item span.description {
            font-size: 12px;
        }

        div.product-chooser div.product-chooser-item input {
            position: absolute;
            left: 0;
            top: 0;
            visibility: hidden;
        }




        ul.nav-news-feed {
            padding-left: 20px;
            padding-right: 20px;
            margin: 0 0 40px 0;
            background: #fff;
            padding-top: 20px;
        }

        ul.nav-news-feed li {
            list-style: none;
            display: block;
            padding: 15px 0;
        }

        ul.nav-news-feed li div {
            position: relative;
            margin-left: 30px;
        }

        ul.nav-news-feed li div::after {
            content: "";
            width: 100%;
            height: 1px;
            border-top: 1px solid #f1f2f2;
            position: absolute;
            bottom: -15px;
            left: 0;
        }

        ul.nav-news-feed li a {
            color: #6d6e71;
        }

        ul.nav-news-feed li i {
            font-size: 18px;
            margin-right: 15px;
            float: left;
        }

        ul.nav-news-feed i.icon1 {
            color: #8dc63f;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-md-4 static">
<<<<<<< HEAD
            {{-- <div class="profile-card">
                <img src=" alt="user" class="profile-photo">
                <h5><a href="#" class="text-white">akhmad</a></h5>
                <a href="#" class="text-white"><i class="fa fa-user"></i> NISN 544645645</a>
            </div> --}}
=======
>>>>>>> 72af603e191047956d5072b075d04fd10a22a54b
            <ul class="nav-news-feed">

                <li onclick="" class="">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="#">Template Sertifikat</a>
                    </div>
                </li>
                <li onclick="" class="">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="#">Sertifikat</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-8">
            <form action="javascript:void(0)" name="formAddNilai" id="formAddNilai" method="post">
                <div class="wadah_nilai">
                    <div class="container">
                        <h2>Normal</h2>
                        <div class="row form-group product-chooser">
                            @foreach ($tempser as $ts)
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" onclick="update({{ $ts['id'] }})">
                                    <div class="product-chooser-item">
                                        <img src="{{ $ts['file'] }}"
                                            class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12"
                                            alt="Mobile and Desktop">
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <span class="title">{{ ucwords($ts['nama']) }}</span>
                                            <input type="radio" name="product" value="mobile_desktop" checked="checked">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            @endforeach
                            {{-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="product-chooser-item selected">
                                    <img src="http://renswijnmalen.nl/bootstrap/desktop.png"
                                        class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Desktop">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title">Desktop</span>
                                        <span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et
                                            magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                                            ultricies nec, pellentesque eu, pretium quis, sem.</span>
                                        <input type="radio" name="product" value="desktop">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="product-chooser-item">
                                    <img src="http://renswijnmalen.nl/bootstrap/mobile.png"
                                        class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title">Mobile</span>
                                        <span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et
                                            magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                                            ultricies nec, pellentesque eu, pretium quis, sem.</span>
                                        <input type="radio" name="product" value="mobile">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div> --}}

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function() {
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function update(id) {
            $.ajax({
                type: 'POST',
                url: "setting/update",
                data: {
                    id
                },
                // beforeSend: function() {
                //     $(".editData-" + id).html(
                //         '<i class="fa fa-spin fa-spinner"></i> Loading');
                // },
                success: function(data) {
                    console.log(data);
                        noti(data.success, data.message);
                        // $('#saveBtn').html('Simpan');
                        // $("#saveBtn").attr("disabled", false);
                }
            });
            console.log(id);
        }
    </script>


@endsection
