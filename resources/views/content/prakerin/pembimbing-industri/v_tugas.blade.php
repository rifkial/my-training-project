@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <style>
        .pace {
            display: none;
        }


        .form-control:focus {
            box-shadow: none;
            border-color: #BA68C8
        }

        .profile-button {
            background: rgb(99, 39, 120);
            box-shadow: none;
            border: none
        }

        .profile-button:hover {
            background: #682773
        }

        .profile-button:focus {
            background: #682773;
            box-shadow: none
        }

        .profile-button:active {
            background: #682773;
            box-shadow: none
        }

        .back:hover {
            color: #682773;
            cursor: pointer
        }

        .labels {
            font-size: 11px
        }

        #custom-search-form {
            margin: 0;
            margin-top: 5px;
            padding: 0;
        }

        #custom-search-form .search-query {
            padding-right: 3px;
            padding-right: 4px \9;
            padding-left: 3px;
            padding-left: 4px \9;
            border-color: #03a9f3;
            margin-bottom: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -webkit-transition: width 0.2s ease-in-out;
            -moz-transition: width 0.2s ease-in-out;
            -o-transition: width 0.2s ease-in-out;
            transition: width 0.2s ease-in-out;
        }

        #custom-search-form button {
            border: 0;
            background: none;
            /** belows styles are working good */
            padding: 2px 5px;
            margin-top: 2px;
            position: relative;
            left: -28px;
            /* IE7-8 doesn't have border-radius, so don't indent the padding */
            margin-bottom: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .search-query:focus+button {
            z-index: 3;
        }

        .search-query:focus {
            width: 260px;
        }


        #loading {
            /* text-align: center; */
            background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
            /* position: absolute; */
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            text-align: center;
            top: 66px;
        }

        .profile-user-box {
            position: relative;
            border-radius: 5px
        }

        .bg-custom {
            background-color: #2471d2 !important;
        }

        .profile-user-box {
            position: relative;
            border-radius: 5px;
        }

        .card-box {
            padding: 20px;
            border-radius: 3px;
            margin-bottom: 30px;
            background-color: #fff;
        }


        .bg-white {
            background-color: #fff;
        }

        .friend-list {
            list-style: none;
            margin-left: -40px;
        }

        .friend-list li {
            border-bottom: 1px solid #eee;
        }

        .friend-list li a img {
            float: left;
            width: 45px;
            height: 45px;
            margin-right: 10px;
        }

        .friend-list li a {
            position: relative;
            display: block;
            padding: 10px;
            transition: all .2s ease;
            -webkit-transition: all .2s ease;
            -moz-transition: all .2s ease;
            -ms-transition: all .2s ease;
            -o-transition: all .2s ease;
        }

        .friend-list li.active a {
            background-color: #f1f5fc;
        }

        .friend-list li a .friend-name,
        .friend-list li a .friend-name:hover {
            color: #777;
        }

        .friend-list li a .last-message {
            width: 65%;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .friend-list li a .time {
            position: absolute;
            top: 10px;
            right: 8px;
        }

        small,
        .small {
            font-size: 85%;
        }

        .friend-list li a .chat-alert {
            position: absolute;
            right: 8px;
            top: 27px;
            font-size: 10px;
            padding: 3px 5px;
        }

        .chat-message {
            padding: 60px 20px 115px;
        }

        .chat {
            list-style: none;
            margin: 0;
        }

        .chat-message {
            background: #f9f9f9;
        }

        .chat li img {
            width: 45px;
            height: 45px;
            border-radius: 50em;
            -moz-border-radius: 50em;
            -webkit-border-radius: 50em;
        }

        img {
            max-width: 100%;
        }

        .chat-body {
            padding-bottom: 20px;
        }

        .chat li.left .chat-body {
            margin-left: 70px;
            background-color: #fff;
        }

        .chat li .chat-body {
            position: relative;
            font-size: 11px;
            padding: 10px;
            border: 1px solid #f1f5fc;
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        .chat li .chat-body .header {
            padding-bottom: 5px;
            border-bottom: 1px solid #f1f5fc;
        }

        .chat li .chat-body p {
            margin: 0;
        }

        .chat li.left .chat-body:before {
            position: absolute;
            top: 10px;
            left: -8px;
            display: inline-block;
            background: #fff;
            width: 16px;
            height: 16px;
            border-top: 1px solid #f1f5fc;
            border-left: 1px solid #f1f5fc;
            content: '';
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
        }

        .chat li.right .chat-body:before {
            position: absolute;
            top: 10px;
            right: -8px;
            display: inline-block;
            background: #fff;
            width: 16px;
            height: 16px;
            border-top: 1px solid #f1f5fc;
            border-right: 1px solid #f1f5fc;
            content: '';
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -o-transform: rotate(45deg);
        }

        .chat li {
            margin: 15px 0;
        }

        .chat li.right .chat-body {
            margin-right: 70px;
            background-color: #fff;
        }

        .chat-box {
            padding: 15px;
            border-top: 1px solid #eee;
            transition: all .5s ease;
            -webkit-transition: all .5s ease;
            -moz-transition: all .5s ease;
            -ms-transition: all .5s ease;
            -o-transition: all .5s ease;
        }

        .primary-font {
            color: #3c8dbc;
        }

        a:hover,
        a:active,
        a:focus {
            text-decoration: none;
            outline: 0;
        }

    </style>
    <div class="row">
        <div class="col-md-4 bg-white ">
            <div class="row border-bottom padding-sm"
                style="height: 63px; display: flex; align-items: center; justify-content: center;">
                <form id="custom-search-form" class="form-search form-horizontal pull-right">
                    <div class="input-append span12">
                        <input type="text" name="siswa" class="search-query mac-style" placeholder="Masukan nama siswa">
                        <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div>
            <ul class="friend-list">
                @foreach ($siswa as $sw)
                    <li class="bounceInDown">
                        <div class="row" style="display: flex; align-items: center; justify-content: center;">
                            <div class="col-7">
                                <a href="javascript:void(0)" class="clearfix">
                                    <img src="{{ $sw['file'] }}" alt="" class="img-circle">
                                    <div class="friend-name">
                                        <strong>{{ ucwords($sw['nama']) }}</strong>
                                    </div>
                                    <div class="last-message text-muted load_{{ $sw['id'] }}">{{ $sw['nisn'] }}</div>
                                </a>
                            </div>
                            <div class="col-5">
                                <div class="float-right">
                                    <a href="javascript:void(0)" onclick="detailTugas({{ $sw['id'] }}, 'paraf')"
                                        class="btn btn-xs btn-info mr-2 mt-1" style="float: left; padding: 0 7px 0 7px;"><i
                                            class="fa fa-star"></i> Paraf</a>
                                    <a href="javascript:void(0)" class="btn btn-xs btn-success mr-2 mt-1"
                                        onclick="detailTugas({{ $sw['id'] }}, 'kegiatan')"
                                        style="float: right; padding: 0 7px 0 7px;"><i class="fa fa-calendar"></i>
                                        Kegiatan</a>
                                    <a href="javascript:void(0)" onclick="detailTugas({{ $sw['id'] }}, 'siswa')"
                                        class="btn btn-xs btn-primary mr-2 mt-1"
                                        style="float: left; padding: 0 7px 0 7px;"><i class="fa fa-eye"></i> Detail</a>
                                    <a href="javascript:void(0)" class="btn btn-xs btn-warning mr-2 mt-1"
                                        onclick="detailTugas({{ $sw['id'] }}, 'guru')"
                                        style="float: right; padding: 0 7px 0 7px;"><i class="fa fa-user"></i> Guru</a>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-md-8 widget-bg">
            <div class="profile-user-box card-box bg-custom" style="display:none">
            </div>
            <form name="save_kegiatan" id="save_kegiatan" style="width: 100%">
                <div class="chat-message" style="display: flex; align-items: center; justify-content: center;">
                    <div class="alert alert-info border-info" style="width: 100%" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p><strong>Harap mengklik dulu tombol disamping, untuk membuka halaman ini:</strong>
                        </p>
                        <ul class="mr-t-10">
                            <li>Paraf => Memberikan paraf &amp; mencetak(print) kegiatan siswa</li>
                            <li>Detail => Melihat detail Siswa Prakerin</li>
                            <li>Guru => Melihat pembimbing sekolah dari siswa prakerin</li>
                            <li>Kegiatan => Melihat agenda siswa dan catatan dari dari pembimbing sekolah</li>
                        </ul>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="javascript:void(0)" id="formUpdateKalender">
                        <div class="row">
                            <div class="col-md-12" id="informasi">
                                <p class="mb-1">Detail Kegiatan.</p>
                                <p><small>Anda hanya bisa melihat detail dari kegiatan siswa</small></p>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="form-label mb-0">Kegiatan Siswa Hari ini</label>
                            </div>
                            <div class="col-md-12">
                                <select name="title[]" id="title" class="select2" multiple required disabled></select>
                            </div>
                            <div class="col-md-12 mt-3">
                                <label for="" class="form-label mb-0">Catatan dari pembimbing sekolah</label>
                            </div>
                            <div class="col-md-12">
                                <textarea id="catatan" rows="4" disabled class="form-control"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function detailTugas(id, params) {
            const loader = $(this);
            $('.load_' + id).html('sedang mengeload tugas....');
            $.ajax({
                url: "tugas/siswa/detail",
                type: "POST",
                data: {
                    id,
                    params
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $('.profile-user-box').show();
                    $('.profile-user-box').html(data.html);
                    $('.load_' + id).html(data.nisn);
                    if (params == 'kegiatan') {
                        $(".chat-message").html('<div id="calendar_' + id + '"></div>');
                        kalender(id);
                    } else {
                        $(".chat-message").html(data.output);
                        $('.chc').bootstrapToggle();
                    }
                }

            });

        }

        function kalender(id) {
            var calendar = $('#calendar_' + id).fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "tugas/siswa" + '/' + id,
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['waktu'],
                                    title: value['nama'],
                                    catatan: value['catatan'],
                                    ulang: value['loop'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.catatan != null) {
                        element.children().last().append(
                            "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Catatan : " +
                            event
                            .catatan + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                eventClick: function(event, jsEvent, view) {
                    console.log(event);
                    $('#catatan').val(event.catatan);
                    var s2 = $('#title');
                    var vals = event.ulang;
                    console.log(vals);
                    vals.forEach(function(key) {
                        if (!s2.find('option:contains(' + key + ')').length)
                            s2.append($('<option>').text(key));
                    });
                    s2.val(vals).trigger("change");
                    $('#ajaxModel').modal('show');
                },
            });
            $('#id_siswa').val(id);
        }

        $('#custom-search-form').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ route('pkl_siswa-search_name') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    $('.friend-list').html(data);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $('#save_kegiatan').on('submit', function(event) {
            event.preventDefault();
            var paraf = "{{ $paraf }}";
            if (paraf === "") {
                var confirmdelete = confirm(
                    "Anda belum mengupload paraf!, apa anda ingin mengupload sekarang?");
                if (confirmdelete == true) {
                    window.location = "/program/sistem_pkl/profile/edit";
                }
            }
            $("#saveCetak").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveCetak").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_tugas-save_kegiatan') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    noti(data.success, data.message);
                    $('#saveCetak').html('<i class="fa fa-floppy-o"></i> Simpan');
                    $("#saveCetak").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });

        });

        $('.Switch').click(function() {
            $(this).toggleClass('Off').toggleClass('On');
        });
    </script>
@endsection
