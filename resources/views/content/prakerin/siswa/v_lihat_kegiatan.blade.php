@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <div class="row widget-bg">
        <div class="row" style="margin-bottom: 23px; width: 100%">

            <div class="col-md-6" style="float: none; margin: 0 auto;">
                <table style="width: 100%" class="mb-10">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ ucwords($siswa['nama']) }}</td>
                    </tr>
                    <tr>
                        <td>NIS</td>
                        <td>:</td>
                        <td>{{ $siswa['nis'] }}</td>
                    </tr>
                    <tr>
                        <td>Perusahaan Tempat Bekerja</td>
                        <td>:</td>
                        <td>{{ $siswa['industri'] }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $siswa['alamat_industri'] }}</td>
                    </tr>
                    <tr>
                        <td>Pembimbing Lapangan</td>
                        <td>:</td>
                        <td>{{ ucwords($siswa['pemb_industri']) }}</td>
                    </tr>
                    <tr>
                        <td>Pembimbing Jurusan</td>
                        <td>:</td>
                        <td>{{ ucwords($siswa['pemb_sekolah']) }}</td>
                    </tr>
                </table>
            </div>

        </div>
        <div class="row" style="margin-bottom: 23px; width: 100%">
            <div class="col-md-10" style="float: none; margin: 0 auto;">
                <form action="{{ route('pkl_tugas-save_kegiatan') }}" method="POST" style="width: 100%">
                    @csrf
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Hari, Tanggal</th>
                                <th>Kegiatan</th>
                                <th>Paraf</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($agenda as $ag)
                                @if ($ag['nama'] == null)
                                    <tr style="background: #d23030">
                                        <td style="color: #fff">{{ $no++ }}</td>
                                        <td style="color: #fff">{{ (new \App\Helpers\Help())->getDay($ag['waktu']) }}</td>
                                        <td style="color: #fff">Tidak ada kegiatan siswa yang diinput</td>
                                        <td>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ (new \App\Helpers\Help())->getDay($ag['waktu']) }}</td>
                                        <td>{{ $ag['nama'] }}</td>
                                        <td>
                                            <input type="checkbox" name="checked[{{ $ag['id'] }}]" {{ $ag['status'] == 4 ? "checked" : "" }} data-toggle="toggle">
                                        </td>
                                    <tr>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="3"></td>
                                <td>
                                    <button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Simpan &
                                        Print</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('.Switch').click(function() {
            $(this).toggleClass('Off').toggleClass('On');
        });
    </script>
@endsection
