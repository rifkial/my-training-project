@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <link rel="stylesheet" href="https://daneden.github.io/animate.css/animate.min.css">
    <style>
        .height {
            height: 100px
        }

        .search {
            position: relative;
            box-shadow: 0 0 40px rgba(51, 51, 51, .1)
        }

        .search input {
            height: 60px;
            text-indent: 25px;
            border: 2px solid #d6d4d4
        }

        .search input:focus {
            box-shadow: none;
            border: 2px solid blue
        }

        .search .fa-search {
            position: absolute;
            top: 20px;
            left: 16px
        }

        .search button {
            position: absolute;
            top: 5px;
            right: 5px;
            height: 50px;
            width: 110px;
            background: blue
        }

        .catatanGuru {
            white-space: normal;
        }

        .pace {
            display: none;
        }


        #profile-image1 {
            cursor: pointer;
            width: 100px;
            height: 100px;
            /* border: 2px solid #03b1ce; */
        }

        .img-circle {
            border-radius: 50%;
        }

        .profile_view {
            margin-bottom: 20px;
            display: inline-block;
            width: 100%;
            -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.3);
        }

        .profile_view:hover {
            -webkit-box-shadow: 0 14px 12px 0 rgba(0, 0, 0, 0.17), 0 20px 40px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 14px 12px 0 rgba(0, 0, 0, 0.17), 0 20px 40px 0 rgba(0, 0, 0, 0.3);
        }

        .well.profile_view {
            padding: 10px 0 0;
        }

        .well.profile_view .divider {
            border-top: 1px solid #e5e5e5;
            padding-top: 5px;
            margin-top: 5px;
        }

        .well.profile_view .ratings {
            margin-bottom: 0;
        }

        .pagination.pagination-split li {
            display: inline-block;
            margin-right: 3px;
        }

        .pagination.pagination-split li a {
            border-radius: 4px;
            color: #768399;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
        }

        .well.profile_view {
            background: #fff;
        }

        .well.profile_view .bottom {
            margin-top: -20px;
            background: #F2F5F7;
            padding: 9px 0;
            border-top: 1px solid #E6E9ED;
        }

        .well.profile_view .left {
            margin-top: 20px;
        }

        .well.profile_view .left p {
            margin-bottom: 3px;
        }

        .well.profile_view .right {
            margin-top: 0px;
            padding: 10px;
        }

        .well.profile_view .img-circle {
            border: 1px solid #E6E9ED;
            padding: 2px;
        }

        .well.profile_view h2 {
            margin: 5px 0;
            font-size: 14px;
            font-weight: bold;
        }

        .well.profile_view .ratings {
            text-align: left;
            font-size: 16px;
        }

        .well.profile_view .brief {
            margin: 0;
            font-weight: 300;
        }

        .profile_left {
            background: white;
        }

    </style>
    <div class="widget-bg">
        <div class="col-md-12 bootstrap snippets bootdeys">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row height d-flex justify-content-center align-items-center animated fadeInDown">
                        <div class="col-md-8">
                            <form action="javascript:void(0)" id="search_siswa" name="search_siswa">
                                <div class="search"> <i class="fa fa-search"></i> <input type="text" name="nama" class="form-control"
                                        placeholder="nama siswa, NISN siswa" required> <button type="submit"
                                        class="btn btn-primary">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row" id="data_siswa">
                        @foreach ($siswa as $sw)
                            <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
                                <div class="well profile_view">
                                    <div class="col-sm-12">
                                        <h4 class="brief"><i>{{ ucwords($sw['nama']) }}</i></h4>
                                        <div class="row">
                                            <div class="left col-md-7">
                                                <h2>{{ ucwords($sw['industri']) }}</h2>
                                                <p><strong>NISN: </strong> {{ $sw['nisn'] }}. </p>
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-phone"></i> Alamat Perusahaan :
                                                        {{ $sw['alamat_industri'] }}</li>
                                                </ul>
                                            </div>
                                            <div class="right col-md-5 text-center" style="padding: 25px;">
                                                <img src="{{ $sw['file'] }}" alt="" class="img-circle img-responsive">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 bottom text-center">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 emphasis">
                                                <div class="mr-3" style="float: right">
                                                    <button type="button" class="btn btn-primary btn-xs"
                                                        onclick="detailProfile('{{ $sw['id_siswa'] }}','siswa')">
                                                        <i class="fa fa-user"></i> View Profile
                                                    </button>
                                                    <a href="javascript:void(0)" onclick="showAgenda({{ $sw['id'] }})"
                                                    class="btn btn-success btn-xs"><i class="fa fa-calendar "></i>
                                                        Kegiatan
                                                    </a>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-md-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="sizeModal modal-dialog">
            <div class="modal-content widget-bg">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle" style="color: #00b1b1 !important">Info Detail</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-body">
                                    <div class="box box-info">
                                        <div class="box-body" id="profileDetail"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="javascript:void(0)" id="formUpdateKalender">
                        <div class="row">
                            <div class="col-md-12" id="informasi">
                                <p class="mb-1">Anda bisa menambahkan data kegiatan di halaman ini.</p>
                                <p><small>Klik tambah untuk menambahkan data</small></p>
                            </div>
                            <div class="col-md-9">
                                <select name="title[]" id="title" class="select2" multiple required disabled></select>
                            </div>
                            <div class="col-md-12">
                                <label for="" class="form-label mb-0">Tambahkan catatan Untuk kegiatan</label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" id="action" value="add">
                                <input type="hidden" name="id" id="id_tugas">
                                <input type="hidden" name="start" id="start">
                                <input type="hidden" name="id_siswa" id="id_siswa">

                                <textarea name="catatan" id="catatan" rows="3" class="form-control"></textarea>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success btn-update">Tambah Catatan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function detailProfile(id, params) {
            var action_url = '';
            if (params == 'industri') {
                action_url = "{{ route('pkl-detail_industri') }}";
            } else if (params == 'siswa') {
                action_url = "{{ route('detail_siswa') }}";
            } else {
                action_url = "{{ route('detail_guru') }}";
            }
            $.ajax({
                type: 'POST',
                url: action_url,
                data: {
                    id
                },
                success: function(data) {
                    $("#modelTitle").html('&nbsp;&nbsp;&nbsp;');
                    $(".sizeModal").addClass('modal-md');
                    $(".sizeModal").removeClass('modal-lg');
                    $('#profileDetail').html(data);
                    $('#modalDetail').modal('show');

                }
            });
        }

        function showAgenda(id) {
            $("#modelTitle").html('&nbsp;&nbsp;&nbsp;');
            $(".sizeModal").removeClass('modal-md');
            $(".sizeModal").addClass('modal-lg');
            $("#profileDetail").html('<div id="calendar_' + id + '"></div>');
            kalender(id);
        }

        function kalender(id) {
            var calendar = $('#calendar_' + id).fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "tugas/siswa" + '/' + id,
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['waktu'],
                                    title: value['nama'],
                                    catatan: value['catatan'],
                                    ulang: value['loop'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.catatan != null) {
                        element.children().last().append(
                            "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Catatan : " +
                            event
                            .catatan + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: "{{ route('bkk_agenda-update') }}",
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id + '&id_loker=' + event.id_loker + '&alamat=' + event
                            .alamat,
                        type: "PUT",
                        success: function(data) {
                            noti(data.success, data.message)
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    $('#informasi').html('');
                    $('.btn-update').html('Tambah');
                    $('#deleteKalender').show();
                    $('#catatan').val(event.catatan);
                    var s2 = $('#title');
                    var vals = event.ulang;
                    vals.forEach(function(key) {
                        if (!s2.find('option:contains(' + key + ')').length)
                            s2.append($('<option>').text(key));
                    });
                    s2.val(vals).trigger("change");
                    $('#informasi').html(
                        '<p class="mb-1">Anda bisa memberi catatan kepada tugas siswa.</p><p><small>klik tambah untuk menambahkan catatan</small></p>'
                    );
                    $('#id_tugas').val(event.id);
                    $('#start').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#ajaxModel').modal('show');
                },
            });
            $('#modalDetail').modal('show');
            $('#modalDetail').on('shown.bs.modal', function() {
                $("#calendar_" + id).fullCalendar('render');
            });
            $('#id_siswa').val(id);
        }

        $('#formUpdateKalender').on('submit', function(event) {
            event.preventDefault();
            const id_kalender = $('#id_siswa').val();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".btn-update").attr("disabled", true);
            $.ajax({
                url: "{{ route('pkl_tugas-update') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#formUpdateKalender').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#calendar_' + id_kalender).fullCalendar('refetchEvents');
                    }
                    noti(data.icon, data.success);
                    $('.btn-update').html('Update');
                    $(".btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $('#search_siswa').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ route('pkl_siswa-search_name-pembimbing_sekolah') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    $('#data_siswa').html(data);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>
@endsection
