@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')

@section('content')
    <style>
        .profile-card {
            background: linear-gradient(to bottom, rgba(39, 170, 225, .8), rgba(28, 117, 188, .8));
            background-size: cover;
            width: 100%;
            min-height: 90px;
            border-radius: 4px;
            padding: 10px 20px;
            color: #fff;
            margin-bottom: 40px;
        }

        .active {
            background: #f2f4f8;
        }

        .profile-card img.profile-photo {
            border: 7px solid #fff;
            float: left;
            margin-right: 20px;
            position: relative;
            top: -30px;
            height: 70px;
            width: 70px;
            border-radius: 50%;
        }

        .profile-card h5 a {
            font-size: 15px;
        }

        .profile-card a {
            font-size: 12px;
        }

        /*Newsfeed Links CSS*/

        ul.nav-news-feed {
            padding-left: 20px;
            padding-right: 20px;
            margin: 0 0 40px 0;
            background: #fff;
            padding-top: 20px;
        }

        ul.nav-news-feed li {
            list-style: none;
            display: block;
            padding: 15px 0;
        }

        ul.nav-news-feed li div {
            position: relative;
            margin-left: 30px;
        }

        ul.nav-news-feed li div::after {
            content: "";
            width: 100%;
            height: 1px;
            border-top: 1px solid #f1f2f2;
            position: absolute;
            bottom: -15px;
            left: 0;
        }

        ul.nav-news-feed li a {
            color: #6d6e71;
        }

        ul.nav-news-feed li i {
            font-size: 18px;
            margin-right: 15px;
            float: left;
        }

        ul.nav-news-feed i.icon1 {
            color: #8dc63f;
        }

        ul.nav-news-feed i.icon2 {
            color: #662d91;
        }

        ul.nav-news-feed i.icon3 {
            color: #ee2a7b;
        }

        ul.nav-news-fee i.icon4 {
            color: #f7941e;
        }

        ul.nav-news-fee i.con5 {
            color: #1c75bc;
        }

        ul.nav-news-feed i.icon6 {
            color: #9e1f63;
        }

        /*Chat Block CSS*/

        #chat-block {
            margin: 0 0 40px 0;
            text-align: center;
            background: #fff;
            padding-top: 20px;
        }

        #chat-block .title {
            background: #8dc63f;
            padding: 2px 20px;
            width: 70%;
            height: 30px;
            border-radius: 15px;
            position: relative;
            margin: 0 auto 20px;
            color: #fff;
            font-size: 14px;
            font-weight: 600;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        ul.online-users li {
            list-style: none;
            position: relative;
            margin: 3px auto !important;
            padding-left: 2px;
            padding-right: 2px;
        }

        ul.online-users li span.online-dot {
            background: linear-gradient(to bottom, rgba(141, 198, 63, 1), rgba(0, 148, 68, 1));
            border: none;
            height: 12px;
            width: 12px;
            border-radius: 50%;
            position: absolute;
            bottom: -6px;
            border: 2px solid #fff;
            left: 0;
            right: 0;
            margin: auto;
        }

        img.profile-photo {
            height: 58px;
            width: 58px;
            border-radius: 50%;
        }

        ul.online-users {
            padding-left: 20px;
            padding-right: 20px;
            text-align: center;
            margin: 0;
        }

        .list-inline {
            padding-left: 0;
            margin-left: -5px;
            list-style: none;
        }

        .list-inline>li {
            display: inline-block;
            padding-right: 5px;
            padding-left: 5px;
        }

        .text-white {
            color: #fff;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-md-4 static">
            {{-- <div class="profile-card">
                <img src=" alt="user" class="profile-photo">
                <h5><a href="#" class="text-white">akhmad</a></h5>
                <a href="#" class="text-white"><i class="fa fa-user"></i> NISN 544645645</a>
            </div> --}}
            <ul class="nav-news-feed">
                
                <li onclick="" class="">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="#">Template Sertifikat</a>
                    </div>
                </li>
                <li onclick="" class="">
                    <i class="fa fa-list-alt icon1"></i>
                    <div>
                        <a href="#">Sertifikat</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-8">
            <form action="javascript:void(0)" name="formAddNilai" id="formAddNilai" method="post">
                <div class="wadah_nilai">

                </div>
            </form>
        </div>
    </div>


@endsection
