@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }

    </style>
    <div class="widget-bg">
        @if (session('role') == 'alumni-alumni' || session('role') == 'admin')
            <div class="row">
                <div class="col-md-12">
                    <h2 class="box-title">{{ Session::get('title') }}</h2>
                </div>
            </div>
            <hr>
            <div class="row mb-3">
                <div class="col-md-12">

                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            
        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped- table-bordered table-hover" id="table-data">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($industri))
                            <tr>
                                <td colspan="4" style="text-align: center">Data saat ini kosong</td>
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($industri as $ind)
                                <tr data-toggle="collapse" data-target="#demo{{ $ind['id'] }}" class="accordion-toggle">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $ind['nama'] }}</td>
                                    <td>{{ $ind['telepon'] }}</td>
                                    <td>{{ $ind['alamat'] }}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="hiddenRow">
                                        <div class="accordian-body collapse" id="demo{{ $ind['id'] }}">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr class="bg-info">
                                                        <th class="text-center">No</th>
                                                        <th class="text-center">Nama</th>
                                                        <th class="text-center">Gender</th>
                                                        <th class="text-center">Telepon</th>
                                                        <th class="text-center">Email</th>
                                                        <th class="text-center">Sertifikat</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="data_pembimbing{{ $ind['id'] }}">
                                                    @if (!empty($ind['pembimbing']))
                                                        @php
                                                            $nomer = 1;
                                                        @endphp
                                                        @foreach ($ind['pembimbing'] as $pemb)
                                                            <tr
                                                                class="{{ empty($pemb['template_sertifikat']) ? 'bg-danger' : '' }}">
                                                                <td class="align-middle">{{ $nomer++ }}</td>
                                                                <td class="align-middle">{{ $pemb['nama'] }}</td>
                                                                <td class="align-middle">{{ $pemb['jenkel'] }}</td>
                                                                <td class="align-middle">{{ $pemb['telepon'] }}</td>
                                                                <td class="align-middle">{{ $pemb['email'] }}</td>
                                                                <td class="text-center align-middle">
                                                                    @if (empty($pemb['template_sertifikat']))
                                                                        <small class="text-light">*Sertifikat belum di
                                                                            set</small>
                                                                        <br>
                                                                        <a href="javascript:void(0)"
                                                                            data-id="{{ $pemb['id'] }}"
                                                                            data-industri="{{ $ind['id'] }}"
                                                                            class="btn btn-success btn-sm set_default"><i
                                                                                class="fas fa-bolt"></i> Set Default</a>
                                                                        <a href="javascript:void(0)"
                                                                            class="btn btn-info btn-sm"
                                                                            onclick="pilihTemplate({{ $pemb['id'] }}, {{ $ind['id'] }})"><i
                                                                                class="fas fa-broom"></i> Set Custom</a>
                                                                    @else
                                                                        <button class="btn btn-info btn-sm editTemplate" data-id="{{ $pemb['template_sertifikat'][0]['id'] }}"><i
                                                                                class="fas fa-pencil"></i> Edit
                                                                        </button>
                                                                        <a href="{{ route('prakerin_template-detail', $pemb['template_sertifikat'][0]['id_template']) }}"
                                                                            target="_blank"
                                                                            class="btn btn-success btn-sm"><i
                                                                                class="fas fa-eye"></i> Lihat</a>

                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6" class="text-center">Pembimbing Industri belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTemplate" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formTemplate" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_template_industri">
                                <input type="hidden" name="id_pembimbing" id="id_pembimbing">
                                <input type="hidden" name="id_industri" id="id_industri">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Template</label>
                                    <div class="col-sm-12">
                                        <select name="id_template" id="id_template" class="form-control">
                                            @foreach ($theme as $tm)
                                                <option value="{{ $tm['id'] }}">{{ $tm['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.set_default', function() {
                let id = $(this).data('id');
                let id_industri = $(this).data('industri');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pembimbing_sertifikat-set_default') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-bolt"></i> Set Default');
                        $('#data_pembimbing' + id_industri).html(data.html);
                        noti(data.icon, data.message);
                    }
                });
            });
            
            $(document).on('click', '.editTemplate', function() {
                // alert("edit template");
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pembimbing_sertifikat-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        // console.log(data);
                        $(loader).html('<i class="fas fa-pencil"></i> Edit');
                        $('#modelHeading').html("Edit Template Sertifikat");
                        $('#id_template_industri').val(data.id);
                        $('#id_template').val(data.id_template).trigger("change");
                        $('#id_pembimbing').val(data.id_pemb_industri);
                        $('#id_industri').val(data.id_industri);
                        $('#action').val('Edit');
                        $('#modalTemplate').modal('show');
                    }
                });
            });

            $('#formTemplate').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                let id_industri = $('#id_industri').val();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('pembimbing_sertifikat-create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('pembimbing_sertifikat-update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formTemplate').trigger("reset");
                            $('#modalTemplate').modal('hide');
                        }
                        $('#data_pembimbing' + id_industri).html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        });

        function pilihTemplate(id, id_industri) {
            $('#formTemplate').trigger("reset");
            $('#id_pembimbing').val(id);
            $('#id_industri').val(id_industri);
            $('#modelHeading').html("Set Template Sertifikat");
            $('#modalTemplate').modal('show');
            $('#action').val('Add');
        }
    </script>
@endsection
