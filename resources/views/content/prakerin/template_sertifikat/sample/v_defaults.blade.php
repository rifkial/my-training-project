<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    {{-- <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script> --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Saira+Condensed:700');

        /* @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        } */

        hr {
            background-color: #be2d24;
            height: 3px;
            margin: 5px;
        }

        div#cert-footer {
            position: absolute;
            width: 92%;
            top: 466px;
            text-align: center;
        }

        div#cert-issued-by,
        div#cert-ceo-design {
            display: inline-block;
            float: right;
        }

        div#cert-ceo-design {
            margin-leftcert-ceo-design: 10%;
        }

        h1 {
            font-family: 'Saira Condensed', sans-serif;
            margin: 5px 0px;
        }

        p {
            font-family: 'Arial', sans-serif;
            font-size: 18px;
            margin: 5px 0px;
        }

        html {
            display: inline-block;
            width: 1024px;
            height: 723px;
            background: #eee url("{{ asset('asset/img/sertifikat/default.jpg') }}") no-repeat;
            background-size: 100%;
            position: relative;
        }

        h1#cert-holder {
            font-size: 50px;
            color: #be2d24;
        }

        p.smaller {
            font-size: 17px !important;
        }

        div#cert-verify {
            opacity: 1;
            position: absolute;
            top: 680px;
            left: 60%;
            font-size: 12px;
            color: #c9c6c6;
        }

    </style>
    <img id="cert-stamp" src="{{ asset('asset/img/sma.png') }}"
        style="width: 109px; height: 109px; position: absolute; right: 88px; top: 36px;">
    <br><br><br><br>
    <br><br><br><br>
    <h1 id="cert-title" style="position: absolute; width: 99%; text-align: center; color: #2887b3">
        Diberikan Kepada
    </h1>
    <br><br><br>
    <h1 id="cert-holder" style="text-align: center;  text-decoration:underline;
    text-decoration-style: dotted;">
        <b>NAMA SISWA</b>
    </h1>

    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Telah melaksanakan Praktek Kerja Industri yang bertempat di
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
       <b>NAMA INDUSTRI</b>
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Terhitung mulai dari tanggal,<b>TANGGAL MULAI</b> sampai dengan <b>TANGGAL SELESAI</b>
    </p>
    <p class="smaller" id='cert-completed-line' style="text-align: center">
        Dengan hasil <b>"Baik / Sangat Baik"</b>
    </p>

    <div id="cert-footer">
        <div id="cert-ceo-design">
            <p class="smaller" id='cert-issued' style="text-align: left">
               <b>KABUPATEN INDUSTRI</b>, <b>TANGGAL SAAT INI</b> <br>
                Pembimbing Industri <br>
                <b>NAMA INDUSTRI</b>
            </p>
            <br><br><br><br>
            <hr>
            <p style="text-align: left"><b>NAMA PEMBIMBING INDUSTRI</b></p>
        </div>
    </div>

    <div id="cert-verify">
        Verify at companywebsite.ai/verify/XYZ12ER56129F. <br>
        Company has confirmed the participation of this individual in the course.
    </div>



</body>

</html>
