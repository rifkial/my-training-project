@extends('content/prakerin/pengunjung/main')
@section('content_prakerin')
    <style>
        h1,
        h2,
        h3,
        h4,
        h5 {
            font-family: 'Montserrat', sans-serif;
        }

        a,
        a:hover,
        .btn {
            outline: none !important;
        }

        .btn-search {
            background: #FD3A13;
            border-color: #FD3A13;
            color: #fff;
            padding: 7px 10px
        }

        .btn-search:hover {
            background: #ca1b1b;
            border-color: #ca1b1b
        }

        section {
            padding: 30px 0;
            float: left;
            width: 100%
        }

        .card {
            float: left;
            width: 100%
        }

        .navbar {
            border: medium none;
            float: left;
            margin-bottom: 0px;
            width: 100%;
            border-radius: 0
        }

        .title-large {
            font-size: 20px;
            margin: 10px 0 5px;
            line-height: 27px;
            color: #141517;
        }

        .title-small {
            color: #141517;
            font-size: 16px;
            font-weight: 400;
            line-height: 23px;
            margin: 6px 0 0;
        }

        .title-x-small {
            font-size: 18px;
            margin: 0px;
        }

        .title-large a,
        .title-small a,
        .title-x-small a {
            color: inherit
        }

        .banner-sec {
            float: left;
            width: 100%;
            background: #EBEBEB
        }

        .card-block {
            padding: 0 10px 10px;
        }

        .card-text {
            margin: 0
        }

        .text-time {
            color: #ff0000;
            font-weight: 600;
        }

        .banner-sec .card-img-overlay {
            padding: 0;
            top: 3px;
            left: 7px;
            height: 20%
        }

        header {
            float: left;
            width: 100%
        }

        .small-top {
            border-bottom: 1px solid #2b2b2b;
            float: left;
            width: 100%;
            background: #000
        }

        .small-top .social-icon {
            float: right;
        }

        .small-top .social-icon a {
            border-left: 1px solid #2b2b2b;
            color: #ca1b1b;
            float: left;
            padding: 6px 13px;
        }

        .small-top .social-icon a:last-child {
            border-right: 1px solid #2b2b2b;
        }

        .small-top .social-icon a:hover {
            color: #FD3A13;
            text-decoration: none;
        }

        .small-top .date-sec {
            font-size: 13px;
            font-weight: 600;
            float: left;
            margin-top: 4px;
            color: #898989
        }

        .top-head {
            background: #141517;
            width: 100%;
            float: left;
            height: 100px;
        }

        .top-head h1 {
            color: #fff;
            font-size: 36px;
            font-weight: 600;
            margin: 18px 0 0;
        }

        .top-head small {
            float: left;
            width: 100%;
            font-size: 14px;
            color: #c0c0c0;
            margin-top: 5px;
            margin-left: 5px;
        }

        .top-head .admin-bar {
            text-align: right;
            margin-top: 22px;
        }

        .top-head .admin-bar a {
            color: #fff;
            line-height: 49px;
            position: relative;
            padding: 0 7px;
        }

        .top-head .admin-bar a:hover {
            color: #ff0000
        }

        .top-head .admin-bar a i {
            margin-right: 6px;
        }

        .top-head .admin-bar .ping {
            background: #ff0000;
            border: 3px solid #141517;
            border-radius: 50%;
            height: 14px;
            position: absolute;
            right: 3px;
            top: 13px;
            width: 14px;
            z-index: 1;
        }

        .top-head .admin-bar img {
            float: right;
            height: 50px;
            width: 50px;
            margin-left: 18px;
        }

        .top-nav {
            background: #fff;
            padding: 0;
            border-bottom: 1px solid #dbdbdb
        }

        .top-nav .nav-link {
            padding-bottom: 0.7rem;
            padding-top: 0.7rem;
        }

        .top-nav .navbar-nav .nav-item+.nav-item {
            margin-left: 0
        }

        .top-nav li a {
            color: #141517;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: 700;
            padding: 0 10px;
            border-bottom: 2px solid #fff
        }

        .top-nav li a:hover,
        .top-nav li a:focus,
        .top-nav li.active a {
            color: #141517;
            border-bottom: 2px solid #FD3A13
        }

        .top-nav .form-control {
            border-color: #fff
        }

        .navbar-toggle {
            background: #fff;
        }

        .navbar-toggle .icon-bar {
            background: #0A2E61;
        }

        .navbar-brand {
            display: none;
        }

        .top-slider .carousel-indicators {
            bottom: 0
        }

        .top-slider .carousel-indicators li {
            border: 1px solid #000;
        }

        .top-slider .carousel-indicators .active {
            background: #000;
        }


        .side-bar .nav-tabs {
            border-bottom: none;
        }

        .side-bar .nav-tabs .nav-link {
            color: #aeaeae;
            text-transform: uppercase;
            border: none;
        }

        .side-bar .nav-tabs .nav-link.active,
        .side-bar .nav-tabs .nav-link:hover {
            border-bottom: 2px solid #ff0000;
            text-transform: uppercase;
            color: #222
        }

        .sidebar-tabing .media {
            margin-top: 20px
        }

        .sidebar-tabing img {
            width: 120px;
            height: 100px;
        }

        .sidebar-tabing .title-small {
            line-height: 23px;
            margin-top: 5px;
            font-size: 18px
        }

        #search {
            float: right;
            margin-top: 9px;
            width: 250px;
        }

        .search {
            padding: 5px 0;
            width: 230px;
            height: 30px;
            position: relative;
            left: 10px;
            float: left;
            line-height: 22px;
        }

        .search input {
            background: #d0d0d0;
            border: medium none;
            border-radius: 3px 0 0 3px;
            float: left;
            height: 36px;
            line-height: 18px;
            margin-left: 210px;
            padding: 0 9px;
            position: absolute;
            top: -3px;
            width: 0;
            -webkit-transition: all 0.7s ease-in-out;
            -moz-transition: all 0.7s ease-in-out;
            -o-transition: all 0.7s ease-in-out;
            transition: all 0.7s ease-in-out;
        }

        .search:hover input,
        .search input:focus {
            width: 200px;
            margin-left: 0px;
            background: #d0d0d0;
        }

        .top-nav .btn {
            position: absolute;
            right: 0;
            top: -3px;
            border-radius: 3px;
        }

        .banner-sec {
            float: left;
            width: 100%;
        }

        .banner-sec .news-block {
            margin-bottom: 20px
        }

        .banner-sec .news-block:last-child {
            margin-bottom: 0px
        }

        .banner-sec .news-des {
            margin-bottom: 5px;
        }

        .banner-sec .title-large {
            margin: 18px 0 0
        }

        .banner-sec .time {
            margin-top: 0px;
            font-size: 13px;
        }

        .banner-sec .carousel-control.left,
        .banner-sec .carousel-control.right {
            background: none;
        }

        .banner-sec .card {
            margin-bottom: 20px;
        }

        .section-01 {
            float: left;
            width: 100%;
            border-top: 1px solid #d5d5d5;
            border-bottom: 1px solid #d5d5d5
        }

        .section-01 .heading-large {
            border-bottom: 2px solid #222;
            color: #222;
            float: left;
            width: 100%;
            padding: 0 0 6px;
            margin: 0 0 18px;
            text-align: left;
        }

        .section-01 .heading-large::before,
        .section-01 .heading-large::after {
            background: transparent;
        }

        .section-01 .heading-small {
            border-bottom: 2px solid #222;
            color: #222;
            float: left;
            margin: 7px 0 0;
            width: 100%;
            padding-bottom: 10px;
            font-size: 18px
        }

        .section-01 .title-small {
            margin-bottom: 5px;
            font-size: 17px
        }

        .section-01 .news-block {
            border-bottom: 1px dashed #000;
            padding-bottom: 30px;
            border: none;
        }

        .section-01 aside>.news-block {
            border-bottom: 1px dashed #000;
            padding-bottom: 19px;
        }

        .section-01 aside>.news-block:last-child {
            border-bottom: none;
            margin-bottom: 20px
        }

        .section-01 .card {
            border: none;
        }

        .section-01 .card-block {
            padding: 10px 0;
        }

        .section-01 .video-sec {
            float: left;
            margin-top: 30px;
            width: 100%;
        }

        .section-01 .video-block {
            float: left;
            margin-top: 20px;
            width: 100%;
        }

        .action-sec {
            width: 100%;
            float: left;
            background: #222
        }

        .action-box {
            float: left;
            width: 100%;
            text-align: center;
        }

        .action-box h2 {
            color: #fff;
            font-size: 20px;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <section class="banner-sec">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @foreach ($data1 as $dt1)
                                <div class="card">
                                    @if ($dt1['urlToImage'] != null)
                                        <img class="img-fluid" src="{{ $dt1['urlToImage'] }}" alt=""
                                            style="height: 158px;">
                                    @endif
                                    <div class="card-img-overlay"> <span class="badge badge-pill badge-danger">News</span>
                                    </div>
                                    <div class="card-body" style="min-height: 206px;">
                                        <div class="news-title">
                                            <h2 class=" title-small"><a href="{{ $dt1['url'] }}">{{ $dt1['title'] }}</a>
                                            </h2>
                                        </div>
                                        <p class="card-text"><small class="text-time"><em>{{ (new \App\Helpers\Help())->timeHuman($dt1['publishedAt']) }}</em></small></p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-3">
                            @foreach ($data2 as $dt2)
                                <div class="card">
                                    @if ($dt2['urlToImage'] != null)
                                        <img class="img-fluid" src="{{ $dt2['urlToImage'] }}" alt=""
                                            style="height: 158px;">
                                    @endif
                                    <div class="card-img-overlay"> <span class="badge badge-pill badge-danger">news</span>
                                    </div>
                                    <div class="card-body" style="min-height: 206px;">
                                        <div class="news-title">
                                            <h2 class=" title-small"><a href="{{ $dt2['url'] }}">{{ $dt2['title'] }}</a>
                                            </h2>
                                        </div>
                                        <p class="card-text"><small class="text-time"><em>{{ (new \App\Helpers\Help())->timeHuman($dt2['publishedAt']) }}</em></small></p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-6 top-slider">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>


                                <div class="carousel-inner" role="listbox">
                                    @foreach ($slider as $sld)
                                        <div
                                            class="carousel-item {{ reset($slider)['title'] == $sld['title'] ? 'active' : '' }}">
                                            <div class="news-block">
                                                <div class="news-media"><img class="img-fluid"
                                                        src="{{ $sld['urlToImage'] }}" alt=""></div>
                                                <div class="news-title">
                                                    <h2 class=" title-large"><a
                                                            href="{{ $sld['url'] }}">{{ $sld['title'] }}</a></h2>
                                                </div>
                                                <div class="news-des">{{ $sld['description'] }}.</div>
                                                <div class="time-text"><strong>{{ (new \App\Helpers\Help())->timeHuman($sld['publishedAt']) }}</strong></div>
                                                <div></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section-01">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-12">
                            <h3 class="heading-large">Sports</h3>
                            <div class="">
                                @foreach ($sport as $sp)
                                    <div class="col-lg-6 card p-3" style="min-height: 395px; background-color: #fff8f800;">
                                        <div class="card"> <img class="img-fluid" src="{{ $sp['urlToImage'] }}" alt=""
                                                style="height: 188px;">
                                            <div class="card-body" style="min-height: 253px;">
                                                <div class="news-title"><a href="{{ $sp['url'] }}">
                                                        <h2 class=" title-small">{{ $sp['title'] }}
                                                        </h2>
                                                    </a></div>
                                                <p class="card-text">{{ $sp['description'] }}.</p>
                                                <p class="card-text"><small class="text-time"><em>{{ (new \App\Helpers\Help())->timeHuman($sp['publishedAt']) }}</em></small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <aside class="col-lg-4 side-bar col-md-12">
                            <ul class="nav nav-tabs" role="tablist" id="myTab">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home"
                                        role="tab">Latest</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile"
                                        role="tab">Top</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages"
                                        role="tab">Featured</a> </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content sidebar-tabing" id="nav-tabContent">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Key Republicans sign letter warning
                                                        against candidate</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Obamacare Appears to Be Making People
                                                        Healthier</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Key Republicans sign letter warning
                                                        against candidate</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile" role="tabpanel">
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Key Republicans sign letter warning
                                                        against candidate</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">‘S.N.L.’ to Lose Two Longtime Cast
                                                        Members</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Obamacare Appears to Be Making People
                                                        Healthier</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages" role="tabpanel">
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Key Republicans sign letter warning
                                                        against candidate</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="https://cdn-2.tstatic.net/jogja/foto/bank/images/ukk-smk_20180219_173500.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">‘S.N.L.’ to Lose Two Longtime Cast
                                                        Members</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                    <div class="media"> <a href="#"> <img class="d-flex mr-3"
                                                src="http://grafreez.com/wp-content/temp_demos/river/img/sport1.jpg"
                                                alt="Generic placeholder image"></a>
                                        <div class="media-body">
                                            <div class="news-title">
                                                <h2 class="title-small"><a href="#">Key Republicans sign letter warning
                                                        against candidate</a></h2>
                                            </div>
                                            <div class="news-auther"><span class="time">1h ago</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="video-sec">
                                <h4 class="heading-small">Featured Video</h4>
                                <div class="video-block">
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe class="embed-responsive-item"
                                            src="//www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

    </script>
@endsection
