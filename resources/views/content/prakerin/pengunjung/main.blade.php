<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        @include('content.prakerin.pengunjung.menu_prakerin')
        <div class="content-wrapper">
            {{-- <main class="main-wrapper clearfix"> --}}
                @yield('content_prakerin')
            {{-- </main> --}}
        </div>
        @include('includes.footer')
    </div>
    @include('includes.foot')
</body>

</html>
