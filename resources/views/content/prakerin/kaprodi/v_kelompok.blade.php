@php
$ext = '';
@endphp
@if (session('template') == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif(session('template') == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif(session('template') == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif(session('template') == "university")
    @php
        $ext = '_university';
    @endphp
@elseif(session('template') == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-outline-info" id="create"><i class="fas fa-plus"></i>
                                    Kelompok</button>
                            </div>
                        </div>
                        {{-- <div class="alert alert-danger border-danger my-2" role="alert">
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <p class="my-1"><strong>Informasi:</strong>
                            </p>
                            <ul class="mr-t-10">
                                <li>Pastikan siswa yang ingin dijadikan kelompok sudah terdaftar di peserta prakerin</li>
                                <li>Tambahkan Kelompok</li>
                                <li>Pilih siswa yang ingin di jadikan anggota</li>
                            </ul>
                        </div> --}}
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Kelompok</th>
                                        <th>Industri</th>
                                        <th>Tahun Ajaran</th>
                                        <th>Anggota</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody id="data-kelompok">
                                    @if (!empty($kelompok['kelompok']))
                                        @php
                                            $nomer = 1;
                                        @endphp
                                        @foreach ($kelompok['kelompok'] as $kl)
                                            <tr>
                                                <td>{{ $nomer++ }}</td>
                                                <td>{{ $kl['nama'] }}</td>
                                                <td>{{ $kl['industri'] }}</td>
                                                <td>{{ $kl['tahun_ajaran'] . ' ' . $kl['semester'] }}</td>
                                                <td>{{ $kl['tahun_ajaran'] . ' ' . $kl['semester'] }}</td>
                                                <td>
                                                    <button class="btn btn-success btn-sm" data-toggle="collapse"
                                                        data-target="#demo{{ $kl['id'] }}" class="accordion-toggle"><i
                                                            class="fas fa-caret-square-down"></i> Lihat anggota</button>
                                                    <a href="javascript:void(0)" class="btn btn-info btn-sm editKelompok"
                                                        data-id="{{ $kl['id'] }}"><i class="fas fa-edit"></i>
                                                        Edit</a>
                                                    <button class="btn btn-danger btn-sm"
                                                        onclick="deleteKelompok({{ $kl['id'] }})"><i
                                                            class="fas fa-trash-alt"></i>
                                                        Hapus</button>
                                                    <a href="{{ route('pkl_kelompok-cetak', (new \App\Helpers\Help())->encode($kl['id'])) }}"
                                                        target="_blank" class="btn btn-purple btn-sm"><i
                                                            class="fas fa-print"></i>
                                                        Cetak</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="demo{{ $kl['id'] }}">
                                                        <div class="tambah">
                                                            <button class="btn btn-info btn-sm pull-right mb-2 addAnggota"
                                                                data-id="{{ $kl['id'] }}"
                                                                data-industri="{{ $kl['id_industri'] }}"><i
                                                                    class="fas fa-user-plus"></i> Tambah
                                                                Anggota</button>
                                                        </div>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="bg-info">
                                                                    <th class="text-center">Nomer</th>
                                                                    <th>Profil</th>
                                                                    <th class="text-center">Kelas</th>
                                                                    <th class="text-center">Tanggal Prakerin</th>
                                                                    <th class="text-center">Pembimbing</th>
                                                                    <th class="text-center">Aksi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if (!empty($kl['anggota']))
                                                                    @php
                                                                        $no = 1;
                                                                    @endphp
                                                                    @foreach ($kl['anggota'] as $ag)
                                                                        <tr>
                                                                            <td class="text-center">{{ $no++ }}
                                                                            </td>
                                                                            <td><b>{{ ucwords($ag['nama']) }}</b> <p class="m-0">NIS/NISN. {{ $ag['nis'] ." / ". $ag['nisn'] }}</p>
                                                                                <small>Telepon . {{ $ag['telepon'] }}</small>
                                                                            </td>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                {{ $ag['rombel'] }}</td>
                                                                            <td class="text-center">
                                                                                {{ $ag['tgl_mulai'] . ' sampai ' . $ag['tgl_selesai'] }}
                                                                                <a href="javascript:void(0)"
                                                                                    class="sw{{ $ag['id'] }}"
                                                                                    onclick="editSiswa({{ $ag['id'] }})">
                                                                                    <i
                                                                                        class="material-icons list-icon md-18 text-info">edit</i>
                                                                                </a>
                                                                            </td>
                                                                            <td class="text-center">{{ ucwords($ag['pemb_sekolah']) }}</td>
                                                                            <td class="text-center">
                                                                                <a href="javascript:void(0)"
                                                                                    onclick="deleteSiswa({{ $ag['id'] }})">
                                                                                    <i
                                                                                        class="material-icons list-icon md-18 text-danger">delete</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @else
                                                                    <tr>
                                                                        <td colspan="7" class="text-center">Data siswa
                                                                            untuk saat
                                                                            ini tidak tersedia</td>
                                                                    </tr>
                                                                @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">Data Kelompok, saat ini tidak tersedia
                                            </td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalCreateKelompok" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div id="smartwizard">
                        <ul class="nav">
                            <li>
                                <a class="nav-link" href="#step-1">
                                    Data Kelompok
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="#step-2">
                                    Peserta Prakerin
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="#step-3">
                                    Finished
                                </a>
                            </li>
                        </ul>
                        <form id="form_kelompok">

                            <div class="tab-content h-100">
                                <div id="step-1" class="tab-pane" role="tabpanel">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Industri</label>
                                            <div class="col-sm-12">
                                                <select name="id_industri" id="id_industri" class="form-control">
                                                    <option value="">--Pilih Industri--</option>
                                                    @foreach ($industri as $ind)
                                                        <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                            <div class="col-sm-12">
                                                <select name="tahun_ajaran" id="tahun_ajaran" class="form-control">
                                                    <option value="">--Pilih Tahun Ajaran--</option>
                                                    @foreach ($tahun as $th)
                                                        <option value="{{ $th['id'] }}"
                                                            {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                            {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Nama Kelompok</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="nama" name="nama" value=""
                                                    required>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="step-2" class="tab-pane" role="tabpanel">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Pembimbing dari
                                                Sekolah</label>
                                            <div class="col-sm-12">
                                                <select name="guru" id="guru" class="form-control">
                                                    <option value="">--Pilih Guru--</option>
                                                    @foreach ($guru as $gr)
                                                        <option value="{{ $gr['id'] }}">{{ $gr['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Pembimbing dari
                                                Perusahaan</label>
                                            <div class="row mx-0">
                                                <div class="col-md-12">
                                                    <span>Pilih Pembimbing</span>
                                                    <select name="pembimbing" id="pembimbing" class="form-control"
                                                        disabled>
                                                        <option value="">-- Pilih Pembimbing --</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-12 control-label">Tanggal Mulai</label>
                                                    <div class="col-sm-12">
                                                        <input type="text" class="form-control datepicker"
                                                            value="{{ date('d-m-Y') }}"
                                                            data-plugin-options="{'autoclose': true}" id="tgl_mulai"
                                                            name="tgl_mulai" readonly required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name" class="col-sm-12 control-label">Tanggal
                                                        Selesai</label>
                                                    <div class="col-sm-12">
                                                        <input type="text" class="form-control datepicker"
                                                            value="{{ date('d-m-Y') }}"
                                                            data-plugin-options="{'autoclose': true}" id="tgl_selesai"
                                                            name="tgl_selesai" readonly required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Filter Siswa berdasarkan
                                                Kelas</label>
                                            <div class="row mx-0">

                                                <div class="col-md-6">
                                                    <span>Pilih Kelas</span>
                                                    <select name="id_kelas" id="id_kelas" class="form-control">
                                                        <option value="">--Pilih kelas--</option>
                                                        @foreach ($kelas as $kls)
                                                            <option value="{{ $kls['id'] }}">
                                                                {{ $kls['nama_romawi'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <span>Pilih Rombel</span>
                                                    <select name="id_rombel" id="id_rombel" class="form-control"
                                                        disabled>
                                                        <option value="">--Pilih tombel--</option>

                                                    </select>
                                                </div>
                                                <div class="col-md-12 my-3">
                                                    <a href="javascript:void(0)" class="btn btn-info btn-block"
                                                        id="mulai_cari">Mulai cari...</a>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="name" class="col-sm-12 control-label pl-0">Daftar
                                                        Siswa</label>
                                                    <div class="alert alert-info border-info" role="alert">
                                                        <p class="m-0"><strong>Informasi:</strong>
                                                        </p>
                                                        <p class="m-0">Pastikan siswa yang dipilih sudah
                                                            terdaftar di peserta prakerin</p>
                                                    </div>
                                                    <table class="table table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Nomer</th>
                                                                <th>Nama</th>
                                                                <th>NISN</th>
                                                                <th>NIS</th>
                                                                <th>Rombel</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="data_siswa">
                                                            <tr>
                                                                <td colspan="6" class="text-center">Silahkan lakukan
                                                                    pencarian
                                                                    berdasarkan rombel terlebih dulu</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3" class="tab-pane" role="tabpanel">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <h5 class="card-title text-danger"><i class="fas fa-info-circle"></i>
                                                Informasi</h5>
                                            <p class="card-text m-0">Proses Selesai.</p>
                                            <small>Apa anda yakin ingin menyimpan proses ini?</small> <br>
                                            <button type="submit" class="btn btn-success" id="btnFinish"
                                                data-href="{{ route('pkl_kelompok-list_data') }}"><i
                                                    class="fas fa-save"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAnggota" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Tambahkan Siswa</h5>
                </div>
                <form id="formAnggota" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_siswa">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="tahun_ajar" id="tahun_ajar" class="form-control">
                                            <option value="">--Pilih Tahun Ajaran--</option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['id'] }}"
                                                    {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="kelompok" id="kelompok_siswa">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pembimbing dari Sekolah</label>
                                    <div class="col-sm-12">
                                        <select name="guru" id="guru" class="form-control">
                                            <option value="">--Pilih Guru--</option>
                                            @foreach ($guru as $gr)
                                                <option value="{{ $gr['id'] }}">{{ $gr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pembimbing dari Perusahaan</label>
                                    <div class="row mx-0">
                                        <div class="col-md-12">
                                            <span>Pilih Pembimbing</span>
                                            <select name="pembimbing" id="pembimbing" class="form-control" disabled>
                                                <option value="">-- Pilih Pembimbing --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Tanggal Mulai</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control datepicker"
                                                    value="{{ date('d-m-Y') }}" data-plugin-options="{'autoclose': true}"
                                                    id="tgl_mulai" name="tgl_mulai" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-12 control-label">Tanggal Selesai</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control datepicker"
                                                    value="{{ date('d-m-Y') }}" data-plugin-options="{'autoclose': true}"
                                                    id="tgl_selesai" name="tgl_selesai" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Filter Siswa berdasarkan
                                        Kelas</label>
                                    <div class="row mx-0">
                                        <div class="col-md-6">
                                            <span>Pilih Kelas</span>
                                            <select name="id_kelas" id="add_kelas" class="form-control">
                                                <option value="">--Pilih kelas--</option>
                                                @foreach ($kelas as $kls)
                                                    <option value="{{ $kls['id'] }}">{{ $kls['nama'] }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <span>Pilih Rombel</span>
                                            <select name="id_rombel" id="add_rombel" class="form-control" disabled>
                                                <option value="">--Pilih tombel--</option>

                                            </select>
                                        </div>
                                        <div class="col-md-12 my-3">
                                            <a href="javascript:void(0)" class="btn btn-info btn-block"
                                                id="btnCariSiswa">Mulai cari...</a>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="name" class="col-sm-12 control-label pl-0">Daftar Siswa</label>
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nomer</th>
                                                        <th>Nama</th>
                                                        <th>NISN</th>
                                                        <th>NIS</th>
                                                        <th>Rombel</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="data_siswa_cari">
                                                    <tr>
                                                        <td colspan="6" class="text-center">Silahkan lakukan pencarian
                                                            berdasarkan rombel terlebih dulu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"
                            id="btnSaveAnggota">Tambahkan Siswa</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalWaktu" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Edit Waktu Prakerin</h5>
                </div>
                <form id="formWaktu" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <input type="hidden" name="id" id="id_edit_siswa">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label">Waktu Prakerin</label>
                                    <div class="input-daterange input-group datepicker">
                                        <input type="text" class="form-control" name="tgl_mulai" id="waktu_tgl_mulai"
                                            readonly> <span class="input-group-addon bg-info text-inverse">sampai</span>
                                        <input type="text" class="form-control" name="tgl_selesai" id="waktu_tgl_selesai"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnUpdateWaktu"
                            value="create">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEK" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Edit Kelompok</h5>
                </div>
                <form id="formEK">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="col-sm-12">
                            <input type="hidden" name="id" id="id_kelompok">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Industri</label>
                                <div class="col-sm-12">
                                    <select name="industri" id="edit_industri" class="form-control">
                                        <option value="">--pilih industri--</option>
                                        @foreach ($industri as $ind)
                                            <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                <div class="col-sm-12">
                                    <select name="tahun_ajaran" id="edit_tahun" class="form-control">
                                        <option value="">--Pilih Tahun Ajaran--</option>
                                        @foreach ($tahun as $th)
                                            <option value="{{ $th['id'] }}"
                                                {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Nama Kelompok</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="edit_kelompok" name="nama[]" value=""
                                        required>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnEK">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"
        rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet"
        type="text/css" />

    <!-- JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(function() {

            $('#smartwizard').smartWizard({
                selected: 0, // Initial selected step, 0 = first step
                theme: 'arrows', // theme for the wizard, related css need to include for other than default theme
                justified: true, // Nav menu justification. true/false
                darkMode: false, // Enable/disable Dark Mode if the theme supports. true/false
                autoAdjustHeight: true, // Automatically adjust content height
                cycleSteps: false, // Allows to cycle the navigation of steps
                backButtonSupport: true, // Enable the back button support
                enableURLhash: true, // Enable selection of the step based on url hash
                transition: {
                    animation: 'fade', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                    speed: '400', // Transion animation speed
                    easing: '' // Transition animation easing. Not supported without a jQuery easing plugin
                },
                toolbarSettings: {
                    toolbarPosition: 'bottom', // none, top, bottom, both
                    toolbarButtonPosition: 'right', // left, right, center
                    showNextButton: true, // show/hide a Next button
                    showPreviousButton: true, // show/hide a Previous button
                    toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements
                },
                anchorSettings: {
                    anchorClickable: true, // Enable/Disable anchor navigation
                    enableAllAnchors: false, // Activates all anchors clickable all times
                    markDoneStep: true, // Add done state on navigation
                    markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
                    enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                },
                keyboardSettings: {
                    keyNavigation: true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                    keyLeft: [37], // Left key code
                    keyRight: [39] // Right key code
                },
                lang: { // Language variables for button
                    next: 'Next',
                    previous: 'Previous'
                },
                disabledSteps: [], // Array Steps disabled
                errorSteps: [], // Highlight step with errors
                hiddenSteps: [] // Hidden steps
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#create').click(function() {
                $('#form_kelompok').trigger("reset");
                $('#modelHeading').html("Tambah Data Kelompok");
                $('#modalCreateKelompok').modal('show');
                $('#action').val('Add');
            });

            $('#formAnggota').on('submit', function(event) {
                $('#btnSaveAnggota').html('Sending..');
                $("#btnSaveAnggota").attr("disabled", true);
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_kelompok-create_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formAnggota').trigger("reset");
                            $('#modalAnggota').modal('hide');
                        }
                        $('#data-kelompok').html(data.html);
                        noti(data.icon, data.message);
                        $('#btnSaveAnggota').html('Tambahkan Siswa');
                        $("#btnSaveAnggota").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



            $('#formWaktu').on('submit', function(event) {
                $('#btnUpdateWaktu').html('Mengupdate..');
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_kelompok-edit_waktu_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formWaktu').trigger("reset");
                            $('#modalWaktu').modal('hide');
                        }
                        $('#data-kelompok').html(data.html);
                        noti(data.icon, data.message);
                        $('#btnUpdateWaktu').html('Update');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpdateWaktu').html('Update');
                    }
                });
            });

            $('#formEK').on('submit', function(event) {
                $('#btnEK').html('Sending..');
                $("#btnEK").attr("disabled", true);
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_kelompok-update') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            window.location.href = "{{ route('pkl_kelompok-list_data') }}";

                        } else {
                            $('#btnEK').html('Simpan');
                            $("#btnEK").attr("disabled", false);
                        }
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#form_kelompok').on('submit', function(event) {
                $('#btnFinish').html('<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                $("#btnFinish").attr("disabled", true);
                event.preventDefault();
                $.ajax({
                    url: "{{ route('pkl_kelompok-save_peserta') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#form_kelompok').trigger("reset");
                            window.location.href = "{{ route('pkl_kelompok-list_data') }}";

                        } else {
                            $('#btnFinish').html('<i class="fas fa-save"></i> Simpan');
                            $("#btnFinish").attr("disabled", false);
                        }
                        noti(data.icon, data.message);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.editKelompok', function() {
                const id = $(this).data('id');
                const loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pkl_kelompok-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-edit"></i> Edit');
                        $('#id_kelompok').val(data.id);
                        $('#edit_kelompok').val(data.nama);
                        $('#edit_tahun').val(data.id_tahun_ajaran).trigger('change');
                        $('#edit_industri').val(data.id_industri).trigger('change');
                        $('#modalEK').modal('show');
                    }
                });
            });

            $(document).on('click', '#mulai_cari', function() {
                let id_rombel = $('#id_rombel').val();
                let id_kelas = $('#id_kelas').val();
                let id_jurusan = $('#id_jurusan').val();
                if (id_rombel) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('pkl_kelompok-list_siswa') }}",
                        data: {
                            id_rombel,
                            id_kelas,
                            id_jurusan
                        },
                        beforeSend: function() {
                            $('#mulai_cari').html(
                                '<i class="fa fa-spin fa-spinner"></i> Sedang mencari...');
                        },
                        success: function(data) {
                            $('#mulai_cari').html('Mulai cari...');
                            $('#data_siswa').html(data);
                        }
                    });
                } else {
                    alert('Harap pilih rombel terlebih dulu');
                }

            });

            $(document).on('click', '#btnCariSiswa', function() {
                let id_rombel = $('#add_rombel').val();
                let id_kelas = $('#add_kelas').val();
                // let id_jurusan = $('#id_jurusan').val();
                if (id_rombel) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('pkl_kelompok-list_siswa') }}",
                        data: {
                            id_rombel,
                            id_kelas,
                            // id_jurusan
                        },
                        beforeSend: function() {
                            $('#btnCariSiswa').html(
                                '<i class="fa fa-spin fa-spinner"></i> Sedang mencari...');
                        },
                        success: function(data) {
                            $('#btnCariSiswa').html('Mulai cari...');
                            $('#data_siswa_cari').html(data);
                        }
                    });
                } else {
                    alert('Harap pilih rombel terlebih dulu');
                }

            });

            $('select[name="id_industri"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('pkl_pembimbing_industri-by_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="pembimbing"]').html(
                                    '<option value="">--- No Pembimbing Found ---</option>');
                            } else {
                                var s = '<option value="">--Pilih Pembimbing--</option>';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama +
                                        '</option>';

                                })
                                $('select[name="pembimbing"]').removeAttr('disabled');
                            }
                            $('select[name="pembimbing"]').html(s)
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('select[name="id_rombel"]').html(
                                '<option value="">Memproses Rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih Rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('select[name="id_rombel"]').removeAttr('disabled')
                            $('select[name="id_rombel"]').html(s);
                        }
                    });
                }
            })

            $(document).on('click', '.addAnggota', function() {
                // console.log("kentang");
                let id = $(this).data('id');
                // console.log(id);
                let id_industri = $(this).data('industri');
                let loader = $(this);
                if (id_industri) {
                    $.ajax({
                        url: "{{ route('pkl_pembimbing_industri-by_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Sedang mencari...');
                        },
                        success: function(data) {
                            // console.log("proses kirim ajax");
                            $(loader).html(
                                '<i class="fas fa-user-plus"></i> Tambah Anggota');
                            if (!$.trim(data)) {
                                $('select[name="pembimbing"]').html(
                                    '<option value="">--- No Pembimbing Found ---</option>');
                            } else {
                                var s = '<option value="">--Pilih Pembimbing--</option>';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama +
                                        '</option>';

                                })
                                $('select[name="pembimbing"]').removeAttr('disabled');
                            }
                            $('select[name="pembimbing"]').html(s)
                            $('#formAnggota').trigger("reset");
                            $('#modelHeading').html("Tambah Data Kelompok");
                            $('#kelompok_siswa').val(id);
                            $('#modalAnggota').modal('show');
                            $('#actionAnggota').val('Add');
                        }
                    });
                }
            });

        });

        // function addAnggota(id) {
        //     $('#formAnggota').trigger("reset");
        //     $('#modelHeading').html("Tambah Data Kelompok");
        //     $('#kelompok_siswa').val(id);
        //     $('#modalAnggota').modal('show');
        //     $('#actionAnggota').val('Add');
        // }

        function editSiswa(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('pkl_siswa-edit') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".sw" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".sw" + id).html('<i class="material-icons list-icon md-18 text-info">edit</i>');
                    $('#id_edit_siswa').val(data.id);
                    $('#waktu_tgl_mulai').val(data.tgl_mulai);
                    $('#waktu_tgl_selesai').val(data.tgl_selesai);
                    $('#modalWaktu').modal('show');
                }
            });
        }

        function deleteKelompok(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('pkl_kelompok-delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $("#data-kelompok").html(
                            '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memperbarui data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-kelompok').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function deleteSiswa(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('pkl_kelompok-delete_siswa') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $("#data-kelompok").html(
                            '<tr><td colspan="6" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memperbarui data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-kelompok').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        // var i = 1;

        // function tambahData() {
        //     i++;
        //     $('.fomAddKelompok').append(
        //         ' <div class="form-group mb-1 mt-3" id="row' + i +
        //         '"><label for="name" class="col-sm-12 control-label">Nama Kelompok</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
        //         i + '">Hapus Baris</a></div></div>'
        //     );
        // }

        // $(document).on('click', '.btn-remove', function() {
        //     var button_id = $(this).attr("id");
        //     $('#row' + button_id + '').remove();
        // });
    </script>
@endsection
