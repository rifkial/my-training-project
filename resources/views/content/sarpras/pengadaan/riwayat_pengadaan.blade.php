@extends('template.template_default.app')
@section('content')
<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center bg-white">
        <h3>Riwayat Penambahan Barang</h3>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCreatePengadaan">Request</button>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-riwayat-pengadaan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Barang</th>
                    <th>Prioritas</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>


@include('content.sarpras.layout_sarpras')
@include('content.sarpras.pengadaan.modal_pengadaan')

<script>

    let tabel = $('#data-riwayat-pengadaan');

    $(function(){ 
        let url = "{{ route('riwayat-pengadaan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "20%"
                },
                {
                    data: 'kode',
                    name: 'kode',
                    width: "5%"
                },
                {
                    data: 'barang',
                    name: 'barang',
                    width: "20%"
                },
                
                {
                    data: 'prioritas',
                    name: 'prioritas',
                    width: "10%"
                },
                {
                    data: 'status_diterima',
                    name: 'status_diterima',
                    width: "10%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                },
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,5]
                    },
                    title: 'Riwayat pengadaan barang'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,5]
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,5]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);

    })




</script>

@endsection