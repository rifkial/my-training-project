@extends('template.template_default.app')
@section('content')

<style type="text/css">
    .pace{
        display: none;
    }
    p {
        margin-bottom: 5px !important;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Usulan pengadaan diterima</h3>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pengadaan-approved">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Tanggal diterima</th>
                    <th>Total harga</th>
                    <th>Prioritas</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</section>


@include('content.sarpras.layout_sarpras')
@include('content.sarpras.pengadaan.modal_pengadaan')


<script>

    let tabel = $('#data-pengadaan-approved');

    $(function(){
        let url = "{{ route('approved-pengadaan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "30%"
                },
                {
                    data: 'barang',
                    name: 'barang',
                    width: "15%"
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    width: "5%"
                },
                {
                    data: 'tgl_diterima',
                    name: 'tgl_diterima',
                    width: "20%"
                },
                {
                    data: 'total_harga',
                    name: 'total_harga',
                    width: "15%"
                },
                {
                    data: 'prioritas',
                    name: 'prioritas',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                },
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    title : 'Usulan pengadaan diterima'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    title : 'Usulan pengadaan diterima'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5,6]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Usulan pengadaan diterima'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];


        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column,
            buttons: buttons,
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var totalHarga = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                }, 0 );

                $( api.column( 4 ).footer() ).html('Total harga : ');
                $( api.column( 5 ).footer() ).html(totalHarga.toLocaleString());
            },
            columnDefs:
                [
                    {
                        targets: 5,
                        render: $.fn.dataTable.render.number(',', '.', 0, '')
                    },
            ]
            
        });

    })

</script>

@endsection