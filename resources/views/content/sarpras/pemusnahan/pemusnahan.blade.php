@extends('template.template_default.app')
@section('content')
<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</head>

<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
    .filter-option-inner-inner{
        color:black;
    }
</style>

<section class="container bg-white">
    @if (Session::has('message'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>    
      </div>
    @endif
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Usulan pemusnahan barang</h3>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCreatepemusnahan">Request</button>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pemusnahan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Tanggal pemusnahan</th>
                    <th>Keterangan</th>
                    <th>Status</th>                  
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modalCreatepemusnahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Request pemusnahan barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('request-pemusnahan')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group ">
                          <label for="inputEmail4">Barang</label>
                          <select name="id_item" class="form-control selectpicker text-dark" data-live-search="true">
                            @foreach($items as $item)
                                <option value="{{$item['id']}}">{{$item['nama']}} {{$item['kode']}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Bukti foto</label>
                        <input type="file" name="image" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tanggal pemusnahan</label>
                        <input type="date" name="tgl_pemusnahan" class="form-control" >   
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" >   
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Request</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- End Modal Create -->

<!-- modal edit-->
  <div class="modal fade" id="modal-edit-pemusnahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <!-- <input type="text" name="routes" id="routes-edit" class="form-control" > -->
              <form action="{{ route('edit-pemusnahan') }}" method="POST" id="edit-modal" enctype="multipart/form-data">
                @csrf

                <div class="form-group ">
                      <label for="inputEmail4">Barang</label>
                      <select name="id_item" class="form-control selectpicker text-dark" id="input-barang" data-live-search="true">
                        @foreach($items as $item)
                            <option value="{{$item['id']}}">{{$item['nama']}} {{$item['kode']}}</option>
                        @endforeach
                      </select>
                </div>
                <div id="modal-body-edit">
                 
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

              </form>

          </div>

        </div>
  </div>
<!-- end modal -->

<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-pemusnahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail request pemusnahan barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-pemusnahan');
    
    $(function(){
        let url = "{{ route('daftar-pemusnahan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'kode',
                    name: 'kode',
                    width: "15%"
                },
                {
                    data: 'tgl_pemusnahan',
                    name: 'tgl_pemusnahan',
                    width: "20%"
                },
                {
                    data: 'keterangan',
                    name: 'keterangan',
                    width: "50%"
                },
                {
                    data: 'status_pemusnahan',
                    name: 'status_pemusnahan',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    title : 'Usulan pemusnahan barang'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    title : 'Usulan pemusnahan barang'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Usulan pemusnahan barang'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);

    });

        function detailPemusnahan(id){
            let btn_return = "";
             $.ajax({
                type : "get",
                url  : "{{route('detail-pemusnahan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    let gambar = "";
                    if(data.data.file != null){
                        gambar = `<img src="${data.data.file}" class="img-radius" alt="Bukti-Image" width="300px"> `;
                    }else{
                        gambar = "";
                    }

                    $('#modal-detail-pemusnahan').modal('show');
                    $('#modal-detail').html(`
                        <div class="container">
                            <div class="row ">
                                <div class="col-6">
                                    <h4>${data.data.barang}</h4>
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="my-0">Kode</p>
                                            <p class="my-0">Tanggal pemusnahan</p>
                                            <p class="my-0">Status</p>
                                        </div>
                                        <div class="col-6">
                                            <p class="my-0">: ${data.data.kode}</p>
                                            <p class="my-0">: ${data.data.tgl_pemusnahan}</p>
                                            <p class="my-0">: ${data.data.status_pemusnahan}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    ${gambar}
                                    <h5>Keterangan : </h5>
                                    <h5 class="card-text">${data.data.keterangan}</h5>
                                </div> 
                            </div>

                            <div class="row mt-2 ml-1">
                                <button onclick="reject(${data.data.id})" class="btn btn-success btn-sm mr-1">Pertahankan</button>
                                <button onclick="approve(${data.data.id})" class="btn btn-danger btn-sm">Hancurkan</button>
                            </div>
                        <div>
                    `);
                    
                },
                error : function(data){
                    
                }
            });
        }

        function deletePemusnahan(id){
            $.ajax({
                type : "get",
                url  : "{{route('delete-pemusnahan')}}",
                data: {
                    'id' : id
                },
                success : (data) =>{
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

        function modalEditPemusnahan(id){
          $('#modal-edit-pemusnahan').modal('show');


          $.ajax({
            type : "GET",
            url : "{{route('detail-pemusnahan')}}",
            data : {
              'id' : id
            },
            success: function(data) {
              console.log(data);
              data = data.data;
              $('#modal-body-edit').html(`
                    <div class="form-group"> 
                        <input type="hidden" name="id" class="form-control" value="${data.id}">    
                    </div>

                     <div class="form-group">
                        <label for="recipient-name" class="col-form-label">File</label>
                        <input type="file" name="image" class="form-control" value="${data.file}">   
                    </div>

                     <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" value="${data.keterangan}">   
                    </div>

                    <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Tanggal pemusnahan</label>
                            <input type="date" name="tgl_pemusnahan" value="${data.tgl_pemusnahan}" class="form-control" >   
                    </div>
              `);
              $('#input-barang').val(data.id_item)
            }

          });
        }

        function approve(id){

            $.ajax({
                type : "post",
                url  : "{{route('approve-pemusnahan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $('#modal-detail-pemusnahan').modal('hide');
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }

        function reject(id){
            $.ajax({
                type : "post",
                url  : "{{route('reject-pemusnahan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                     $('#modal-detail-pemusnahan').modal('hide');
                },
                error : function(data){
                    console.log(data);
                    tabel.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                }
            });
        }
    
</script>

@endsection