@extends('template.template_default.app')
@section('content')
<style type="text/css">
    .pace{
        display: none;
    }
    p {
        margin-bottom: 5px !important;
    }
</style>

<section class="container bg-white">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Pemusnahan barang disetujui</h3>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pemusnahan-approved">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Tanggal pemusnahan</th>
                    <th>Keterangan</th>
                    <th>Status</th>                  
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>

<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-pemusnahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail request pemusnahan barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Detail -->


@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-pemusnahan-approved');

    $(function(){
        let url = "{{ route('approved-pemusnahan') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'kode',
                    name: 'kode',
                    width: "15%"
                },
                {
                    data: 'tgl_pemusnahan',
                    name: 'tgl_pemusnahan',
                    width: "20%"
                },
                {
                    data: 'keterangan',
                    name: 'keterangan',
                    width: "50%"
                },
                {
                    data: 'status_pemusnahan',
                    name: 'status_pemusnahan',
                    width: "15%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    title : 'Usulan pemusnahan barang diterima'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    title : 'Usulan pemusnahan barang diterima'
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    title : 'Usulan pemusnahan barang diterima'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);
    });

        function detailPemusnahan(id){
            let btn_return = "";
             $.ajax({
                type : "get",
                url  : "{{route('detail-pemusnahan')}}",
                data : {
                  'id' : id
                },
                success : function(data){
                    let gambar = "";
                    if(data.data.file != null){
                        gambar = `<img src="${data.data.file}" class="img-radius" alt="Bukti-Image" width="300px"> `;
                    }else{
                        gambar = "";
                    }

                    $('#modal-detail-pemusnahan').modal('show');
                    $('#modal-detail').html(`
                        <div class="container">
                            <div class="row ">
                                <div class="col-6">
                                    <h4>${data.data.barang}</h4>
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="my-0">Kode</p>
                                            <p class="my-0">Tanggal pemusnahan</p>
                                            <p class="my-0">Status</p>
                                        </div>
                                        <div class="col-6">
                                            <p class="my-0">: ${data.data.kode}</p>
                                            <p class="my-0">: ${data.data.tgl_pemusnahan}</p>
                                            <p class="my-0">: ${data.data.status_pemusnahan}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    ${gambar}
                                    <h5>Keterangan : </h5>
                                    <h5 class="card-text">${data.data.keterangan}</h5>
                                </div>
                                
                                
                            </div>
                        <div>
                    `);
                },
                error : function(data){
                    
                }
            });
        }

    



</script>

@endsection