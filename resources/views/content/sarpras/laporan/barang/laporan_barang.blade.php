@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
	  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
	.tg .tg-0lax{text-align:left;vertical-align:top}
</style>

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan daftar barang</h4>
		<span>
			<a href="{{route('daftar-laporan-barang',['download'=>'pdf'])}}" class="btn btn-primary btn-sm">
				<i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
			Download Pdf
			</a>
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
	<div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">DAFTAR BARANG</h4>
	</div>

	<div class="container">
        @foreach($barangs as $barang)
            @if(!empty($barang["barang"]))
            <div class="container my-5">
                <h5>{{$barang["jenis"]}}</h5>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Kategori</th>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Jumlah</th>
                      <th scope="col">Supllyer</th>
                      <!-- <th scope="col">Keterangan</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($barang["barang"] as $item)
                        <tr>
                          <th scope="row">{{$no}}</th>
                          <td>{{$item["kategori"]}}</td>
                          <td>{{$item["kode"]}}</td>
                          <td>{{$item["nama"]}}</td>
                          <td>{{$item["jumlah_item"]}}</td>
                          <td>{{$item["suplayer"]}}</td>
                          <!-- <td>{{$item["keterangan"]}}</td> -->
                        </tr>
                        @php
                            ++$no;
                        @endphp
                    @endforeach
                  </tbody>
                </table>
            </div>
            @endif
        @endforeach
	</div>

</section>


@include('content.sarpras.layout_sarpras')

@endsection