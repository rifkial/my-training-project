@extends('content.sarpras.laporan.template_export')
@section('content')
	<style type="text/css">
		.jenis{
			margin-bottom: 5px;
		}
	</style>

	


	<div class="container">
        @foreach($barangs as $barang)
            @if(!empty($barang["barang"]))
            <div class="container my-5">
                <h4 class="jenis">Jenis {{$barang["jenis"]}}</h4>
                <table  class="tg" width="100%">
                  <thead>
                    <tr>
                      <th class="tg-0lax">No</th>
                      <th class="tg-0lax">Kategori</th>
                      <th class="tg-0lax">Kode</th>
                      <th class="tg-0lax">Nama</th>
                      <th class="tg-0lax">Jumlah</th>
                      <th class="tg-0lax">Supllyer</th>
                      <!-- <th class="tg-0lax">Keterangan</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($barang["barang"] as $item)
                        <tr>
                          <td class="tg-0lax">{{$no}}</th>
                          <td class="tg-0lax">{{$item["kategori"]}}</td>
                          <td class="tg-0lax">{{$item["kode"]}}</td>
                          <td class="tg-0lax">{{$item["nama"]}}</td>
                          <td class="tg-0lax">{{$item["jumlah_item"]}}</td>
                          <td class="tg-0lax">{{$item["suplayer"]}}</td>
                          <!-- <td class="tg-0lax">{{$item["keterangan"]}}</td> -->
                        </tr>
                        @php
                            ++$no;
                        @endphp
                    @endforeach
                  </tbody>
                </table>
            </div>
            @endif
        @endforeach
    </div>

@endsection

