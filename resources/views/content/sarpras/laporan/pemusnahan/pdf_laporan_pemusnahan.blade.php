@extends('content.sarpras.laporan.template_export')
@section('content')

	<table class="tg" width="100%">
			<thead>
			  <tr>
			    <th class="tg-0lax">No</th>
			    <th class="tg-0lax">Nama Barang</th>
			    <th class="tg-0lax">Kode</th>
			    <th class="tg-0lax">Jenis</th>
			    <th class="tg-0lax">Tanggal pemusnahan</th>
			    <th class="tg-0lax">Keterangan</th>
			    <th class="tg-0lax">Status</th>
			  </tr>
			</thead>
			<tbody>
			@php
			    $no = 1;
			@endphp
			@foreach($pemusnahans as $pemusnahan)
			  <tr>
			    <td class="tg-0lax">{{$no}}</td>
			    <td class="tg-0lax">{{ $pemusnahan['barang'] }}</td>
			    <td class="tg-0lax">{{ $pemusnahan['kode'] }}</td>
			    <td class="tg-0lax">{{ $pemusnahan['jenis'] }}</td>
			    <td class="tg-0lax">{{ $pemusnahan['tgl_pemusnahan'] }}</td>
			    <td class="tg-0lax">{{ $pemusnahan['keterangan'] }}</td>
			    <td class="tg-0lax">{{ $pemusnahan['status_pemusnahan'] }}</td>
			  </tr>
			  	@php
			    	++$no;
				@endphp
			@endforeach
			</tbody>
		</table>

@endsection

