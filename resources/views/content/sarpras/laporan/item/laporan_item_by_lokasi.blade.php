@extends('template.template_default.app')
@section('content')

<style type="text/css">
    .pace{
        display: none;
    }
</style>

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan pengadaan item berdasarkan lokasi</h4>
		<span>
            <div class="dropdown">
              <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download pdf
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item">-------- Lokasi --------</a>
                @foreach($lokasis as $lokasi)
                <a class="dropdown-item" href="{{route("print-daftar-laporan-item-by-lokasi", $lokasi['id'])}}">
                    <i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>{{$lokasi['nama']}}
                </a>
                @endforeach
              </div>
            </div>	
		</span>
		
	</div>
</section>


<section class="container bg-white py-3">
    <div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
        <h4 class="mb-0">DATA ITEM</h4>
        <h4 class="mt-1">{{$sekolah['nama']}}</h4>
    </div>

    <div class="container">
        @foreach($datas as $data)
        <div class="container">
            @if(!empty($data["kategori"]))
                @foreach($data["kategori"] as $kat)
                @if(!empty($kat["items"]))
                <h5><strong>{{$data["jenis"]}}</strong></h5>   
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Kategori</th>
                      <th scope="col">Kode</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Kondisi</th>
                      <th scope="col">Lokasi</th>
                    </tr>
                  </thead>
                    @break
                @endif
                @endforeach
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($data["kategori"] as $kategoris)
                        @foreach($kategoris["items"] as $items)
                            <tr>
                              <td>{{$no++}}</td>
                              <td>{{$items["kategori"]}}</td>
                              <td>{{$items["kode"]}}</td>
                              <td>{{$items["nama"]}}</td>
                              <td>{{$items["kondisi"]}}</td>
                              <td>{{$items["lokasi"]}}</td>
                            </tr>
            
                        @endforeach
                    @endforeach
                  </tbody>
                </table>
                
            @endif
        </div>
        @endforeach
    </div>

</section>


@include('content.sarpras.layout_sarpras')

<script type="text/javascript">


    let tabel = $('#data-laporan-item');
    
    $(function(){
        let url = "{{ route('daftar-laporan-item') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "30%"
                },
                {
                    data: 'kode',
                    name: 'kode',
                    width: "15%"
                },
                {
                    data: 'lokasi',
                    name: 'lokasi',
                    width: "15%"
                },
                {
                    data: 'kategori',
                    name: 'kategori',
                    width: "15%"
                },
                {
                    data: 'kondisi',
                    name: 'kondisi',
                    width: "20%"
                }
            ]
        ;

        tabel.DataTable({
            ...konfigUmum,
            dom : 'rt<"float-right"p>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    })


        function export_lokasi(id){
            $.ajax({
                type : "get",
                url  : "{{route('daftar-laporan-item-by-lokasi')}}",
                data : {
                    'id' : id,
                    'download' : 'pdf'
                },
                success : (data) =>{
                    console.log(data);
                },
                error: function(data) {
                    
                } 
            });
        }

        $('#filter_lokasi').change(function( ){
            tabel.DataTable().columns(3)
            .search($(this).val()).draw();
        }); 

    

</script>

@endsection