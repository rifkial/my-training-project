@extends('template.template_default.app')
@section('content')

<section class="container">
	<div class="row justify-content-between">
		<h4>Laporan daftar user</h4>
		<span>
			<a href="{{route('daftar-laporan-user',['download'=>'pdf'])}}" class="btn btn-danger btn-sm">
				<i class="fa fa-file-pdf-o mr-1" aria-hidden="true"></i>
			Download Pdf
			</a>
		</span>
		
	</div>
</section>


<section class="container bg-white">
	<div class="row flex-column justify-content-center row flex-column justify-content-center align-items-center my-3">
		<h4 class="mb-0">DATA USER SARANA DAN PRASARANA</h4>
		<h4 class="mt-1">{{$sekolah['nama']}}</h4>
	</div>

	<div class="container py-2">
        
		<table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">Role</th>
              <th scope="col">Telepon</th>
            </tr>
          </thead>
          <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($users as $user)
            <tr>
              <td scope="row">{{$no}}</td>
              <td>{{$user["nama"]}}</td>
              <td>{{$user["username"]}}</td>
              <td>{{$user["email"]}}</td>
              <td>{{$user["role"]}}</td>
              <td>{{$user["telepon"]}}</td>
            </tr>
            @php
                ++$no;
            @endphp
            @endforeach
          </tbody>
        </table>
        
	</div>

</section>


@include('content.sarpras.layout_sarpras')

<script type="text/javascript">
	
    let tabel = $('#data-laporan-user');
    
    $(function(){
  	  
      let url = "{{ route('daftar-laporan-user') }}";
      let column = 
          [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "5%"
              },
              {
                  data: 'nama',
                  name: 'nama',
                  width: "30%"
              },
              {
                  data: 'username',
                  name: 'username',
                  width: "15%"
              },
              {
                  data: 'email',
                  name: 'email',
                  width: "15%"
              },
              {
                  data: 'role',
                  name: 'role',
                  width: "15%"
              },
              {
                  data: 'telepon',
                  name: 'telepon',
                  width: "20%"
              }
          ]
      ;

      tabel.DataTable({
          ...konfigUmum,
          dom : 'rt<"float-right"p>',
          ajax: {
              "url": url,
              "method": "GET"
          },
          columns: column
      });
      
    })

</script>

@endsection