@extends('content.sarpras.laporan.template_export')
@section('content')

	<div class="container py-2">
        
		<table class="tg" width="100%">
          <thead>
            <tr>
              <th class="tg-0lax">#</th>
              <th class="tg-0lax">Nama</th>
              <th class="tg-0lax">Username</th>
              <th class="tg-0lax">Email</th>
              <th class="tg-0lax">Role</th>
              <th class="tg-0lax">Telepon</th>
            </tr>
          </thead>
          <tbody>
            @php
                $no = 1;
            @endphp
            @foreach($users as $user)
            <tr>
              <td class="tg-0lax">{{$no}}</td>
              <td class="tg-0lax">{{$user["nama"]}}</td>
              <td class="tg-0lax">{{$user["username"]}}</td>
              <td class="tg-0lax">{{$user["email"]}}</td>
              <td class="tg-0lax">{{$user["role"]}}</td>
              <td class="tg-0lax">{{$user["telepon"]}}</td>
            </tr>
            @php
                ++$no;
            @endphp
            @endforeach
          </tbody>
        </table>
        
	</div>

@endsection

