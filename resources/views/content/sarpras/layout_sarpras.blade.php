<script>


// (function($,global){
//     "use strict"
    
    $(function(){
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })

        
    	const konfigUmum = {
                responsive: true,
                serverSide: true,
                processing: true,
                ordering: true,
                paging: true,
                searching: true,
            };

        function noti(tipe, value) {
            $.toast({
                icon: tipe,
                text: value,
                hideAfter: 5000,
                showConfirmButton: true,
                position: 'top-right',
            });
            return true;
        };

        function MakeTable(tabel,url,column){
            tabel.DataTable({
                ...konfigUmum,
                dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                ajax: {
                    "url": url,
                    "method": "GET"
                },
                columns: column
            });
        }

        function MakeFullTable(tabel,url,column,buttons){
            tabel.DataTable({
                ...konfigUmum,
                dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                ajax: {
                    "url": url,
                    "method": "GET"
                },
                columns: column,
                buttons: buttons
            });
        }

        // CREATE

        function modalCreate(route){
            $('#modal-create').modal('show');
            $('#routes-create').val(route);

            
            $('#modal-create-body').html(`
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>
            `);   
        }

        $('#create-modal').on('submit', function(event) {
            let route_create;
            let tabel_create;
            let data;

            let nama_rute = $('#routes-create').val();
            data = $(this).serialize();

            if(nama_rute == "kategori"){
                route_create = "{{route('buat-kategori-barang')}}";
                tabel_create = $('#data-kategori');
            }else if(nama_rute == "jenis"){
                route_create = "{{route('buat-jenis-barang')}}";
                tabel_create = $('#data-jenis');
            }else if(nama_rute == "lokasi"){
                route_create = "{{route('buat-lokasi-barang')}}";
                tabel_create = $('#data-lokasi');
            }else if(nama_rute == "satuan"){
                route_create = "{{route('buat-satuan-barang')}}";
                tabel_create = $('#data-satuan');
            }else {
                route_create = "{{route('buat-supllyer-barang')}}";
                tabel_create = $('#data-supllyer');
                let file = $('#images').val();
                let files = $('#images').prop('files');
                file = file.split(/(\\|\/)/g).pop();
                console.log(files);
                data = data.concat("$images=",file);
            }
            
            console.log(data);
           
            event.preventDefault();
            $.ajax({
                type : "post",
                url  : route_create,
                data : data,
                success : (data) =>{
                    console.log(data);
                    tabel_create.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                    $("#modal-create").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        });

        $('#create-modal').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

        //EDIT ( belum jadi,binggung isi formnya)

        $('#edit-modal').on('submit',function(event){
            let route_create;
            let tabel_create;
            let nama_rute = $('#routes-edit').val();

            if(nama_rute == "kategori"){
                route_edit = "{{route('edit-kategori-barang')}}";
                tabel = $('#data-kategori');
            }else if(nama_rute == "jenis"){
                route_edit = "{{route('edit-jenis-barang')}}";
                tabel = $('#data-jenis');
            }else if(nama_rute == "lokasi"){
                route_edit = "{{route('edit-lokasi-barang')}}";
                tabel = $('#data-lokasi');
            }else if(nama_rute == "satuan"){
                route_edit = "{{route('edit-satuan-barang')}}";
                tabel = $('#data-satuan');
            }
            else {
                route_edit = "{{route('edit-supllyer-barang')}}";
                tabel = $('#data-supllyer');
            }

            console.log($(this).serialize());

            $.ajax({
                type : "post",
                url  : route_edit,
                data: $(this).serialize(),
                success : (data) =>{
                    noti(data.icon, data.message); 
                    tabel.DataTable().ajax.reload(); 
                    $("#modal-edit").modal('hide');
                },
                error: function(data) {
                    noti(data.icon, data.message);
                    $("#modal-edit").modal('hide');   
                } 
            }); 

        });

        $('#edit-modal').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });

        //DETAIL
        $('#detail-modal').on('hidden.bs.modal', function() {
          $(this).find('form').trigger('reset');
        });


        //DELETE !supplyer

        function deleteItem(route,id){
            let route_delete;
            let tabel_delete;
            if(route == "kategori"){
                route_delete = "{{route('hapus-kategori-barang')}}";
                tabel_delete = $('#data-kategori');
            }else if(route == "jenis"){
                route_delete = "{{route('hapus-jenis-barang')}}";
                tabel_delete = $('#data-jenis');
            }else if(route == "lokasi"){
                route_delete = "{{route('hapus-lokasi-barang')}}";
                tabel_delete = $('#data-lokasi');
            }else if(route == "satuan"){
                route_delete = "{{route('hapus-satuan-barang')}}";
                tabel_delete = $('#data-satuan');
            }else {
                route_delete = "{{route('hapus-supllyer-barang')}}";
                tabel_delete = $('#data-supllyer');
            }

            $.ajax({
                type : "get",
                url  : route_delete,
                data: {
                    'id' : id
                },
                success : (data) =>{
                    console.log(data);
                    tabel_delete.DataTable().ajax.reload();
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    noti(data.icon, data.message);   
                } 
            }); 
        }

        function modalCreateUser(role){
            if(role == "admin"){
                $('#modal-create-user').modal('show');  
            }else{
                $('#modal-create-users').modal('show');
            }
            
        }

        //USER
        function modalDetailUser(role,id){
            console.log(role);
            let route;
            let roles = "";
            let history = "";
            if(role == 1){
                route = "{{ route('detail-admin-barang') }}";
            }else{
                route = "{{ route('detail-user-barang') }}";
            }

            $.ajax({
                type: "GET",
                url: route,
                data : {
                    'id' : id
                },
                success: function(data){
                    console.log(data);
                    data = data.data;
                    $('#modal-detail-user').modal('show');
                    if(role == 2){
                        roles = `
                            <div class="col-sm-6">
                                <p class="m-b-10 f-w-600 mb-0">Role</p>
                                <h6 class="text-muted f-w-400 mt-0">${data.role}</h6>
                            </div>
                        `;

                        history = `
                            
                                <span>
                                    <a onclick="riwayatUser(${data.id})" class="btn btn-warning text-white btn-sm">
                                    Riwayat peminjaman
                                    </a>
                                </span>
                            
                        `;
                    }else{
                        roles = "";
                    }


                    $('#detailUser').html(`
                        <div class="row container d-flex justify-content-center p-0">
                            <div class="col-12">
                                <div class="card user-card-full">
                                    <div class="row m-l-0 m-r-0">
                                        <div class="col-sm-4 bg-c-lite-green user-profile">
                                            <div class="card-block card-text text-center text-white">
                                                <div class="m-b-25"> <img src="${data.file}" class="img-radius" alt="User-Profile-Image"> </div>
                                                <h4 class="f-w-600 text-white">${data.nama}</h4>
                                                <p>${data.jenkel}</p>  
                                                
                                                <i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="card-block">
                                                <span class="d-flex justify-content-between align-items-center">
                                                    <h5 class="m-b-20 p-b-5 b-b-default f-w-600">Informasi</h5>

                                                    ${history}
                                                    
                                                </span>
                                                

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Email</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.email}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Phone</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.telepon}</h6>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Agama</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.agama}</h6>
                                                    </div>
                                                    
                                                ${roles}
                                                    
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Tempat,tanggal lahir</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.tempat_lahir},${data.tgl_lahir}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Alamat</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.alamat}</h6>
                                                    </div>
                                                    
                                                </div>

                                                <h5 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600 mb-2">Data Akses</h5>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">IP</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.ip_terakhir}</h6>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Login Terakhir</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.terakhir_login}</h6>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p class="m-b-10 f-w-600 mb-0">Password Reset</p>
                                                        <h6 class="text-muted f-w-400 mt-0">${data.first_password}</h6>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `);

                }
            });
        }

        function resetPassUser(){
            
        }

        function resetPassUser(role,id){
            let url = "";
            if(role == 1){
                url = "{{env("API_URL")}}" + "api/password/reset-password/admin-sarpras/"+ id;
            }else{
                url = "{{env("API_URL")}}" + "api/password/reset-password/sarpras/"+ id;
            }
            swal({
                title: "Apa kamu yakin?",
                text: "ingin me-reset password user ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    type: 'PUT',
                    url: url,
                    success: function(data) {
                        swa("Sukses !", 'Password baru user : '+ data.data , 'success');
                        // noti(data.icon, data.message);  
                        $("#modal-detail").modal('hide'); 
                    }
                });
            },
            function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Password user tidak berubah', 'error');
                }
            })
        }
    

// })(jQuery,window)

</script>

