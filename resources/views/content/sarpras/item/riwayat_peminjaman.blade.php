@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
  <h4 class="mt-3 title-content">Riwayat {{$item['nama']}}</h4>
  <h5 class="my-1 title-content">{{$item['kode']}}</h5>

  <div class="row mb-3">
    <div class="col-4">
      <h6 class="my-1">Lokasi</h6>
      <h6 class="my-1">Kategori</h6>
      <h6 class="my-1">Jenis</h6>
    </div>
    <div class="col-5">
      <h6 class="my-1">: {{$item['lokasi']}}</h6>
      <h6 class="my-1">: {{$item['kategori']}}</h6>
      <h6 class="my-1">: {{$item['jenis']}}</h6>
    </div>
  </div>

	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-riwayat-items">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Penyewa</th>
                    <th>Role</th>
                    <th>Tanggal sewa</th>
                    <th>Tanggal Kembali</th>
                    <th>Keterangan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-riwayat-items');

    $(function(){   
        let urls = document.URL;
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'penyewa',
                    name: 'penyewa',
                    width: "20%",
                    orderable :true
                },
                {
                    data: 'role',
                    name: 'role'
                },
                {
                    data: 'tgl_sewa',
                    name: 'tgl_sewa'
                },
                {
                    data: 'tgl_kembali',
                    name: 'tgl_kembali'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'status_kembali',
                    name: 'status_kembali',
                    width: "10%"
                }
            ];
        let buttons = [
                {
                    text: '<i class="fa fa-upload"></i>',
                    className: 'btn btn-sm',
                    attr: {
                        title: 'Import Data',
                        id: 'import'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns: [ 0, 1, 2 , 3, 4 ,5 ,6]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];


        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-left"f><"float-right"B>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": urls,
                "method": "GET"
            },
            columns: column,
            buttons: buttons
        });
    })


</script>

@endsection

