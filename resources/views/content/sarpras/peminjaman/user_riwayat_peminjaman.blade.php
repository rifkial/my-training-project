@extends('template.template_default.app')
@section('content')
<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</head>

<style type="text/css">
    .pace{
        display: none;
    }

    p {
        margin-bottom: 5px !important;
    }
    .filter-option-inner-inner{
        color:black;
    }
</style>

<section class="container">
    <section class="container d-flex justify-content-between align-items-center">
        <h3>Riwayat Peminjaman Barang Anda</h3>
    </section>
    <div class="container">
        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-riwayat-peminjaman">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>Tanggal sewa</th>
                    <th>Tanggal kembali</th>         
                    <th>Status</th>                  
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>  
                        
			</tbody>
        </table>
    </div>
</section>


<!-- Modal Detail-->
<div class="modal fade bd-example-modal-lg" id="modal-detail-peminjaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Request barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-detail">
                
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Detail -->

@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-riwayat-peminjaman');

    $(function(){
        let url = "{{ route('riwayat-peminjaman-saya') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "5%"
                },
                {
                    data: 'nama_barang',
                    name: 'nama_barang',
                    width: "15%"
                },
                {
                    data: 'kode_item',
                    name: 'kode_item',
                    width: "10%"
                },
                {
                    data: 'tgl_sewa',
                    name: 'tgl_sewa',
                    width: "10%"
                },
                {
                    data: 'tgl_kembali',
                    name: 'tgl_kembali',
                    width: "10%"
                },
                {
                    data: 'status_kembali',
                    name: 'status_kembali',
                    width: "10%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        let buttons = [
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    messageTop: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    messageBottom: null,
                    exportOptions: {
                        columns : [ 0, 1, 2 ,3 ,4,5]
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }
            ];

        MakeFullTable(tabel,url,column,buttons);

    });

        function detailPeminjaman(id){
        let btn_return = "";
         $.ajax({
            type : "get",
            url  : "{{route('detail-peminjaman')}}",
            data : {
              'id' : id
            },
            success : function(data){
              console.log(data.data);

                $('#modal-detail-peminjaman').modal('show');
                $('#modal-detail').html(`
                    <div class="container">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">${data.data.nama_barang}</h4>
                                <p class="card-text">Kode : ${data.data.kode}</p>
                                <h6 class="card-subtitle mb-2 text-muted">Keterangan : ${data.data.keterangan}</h6>
                                
                            </div>

                            <div class="col-4">
                                <h5 class="card-text mt-0">${data.data.penyewa}</h5>
                                <p class="card-text">${data.data.role}</p>
                            </div>
                        </div>
                        

                        <div class="container px-0 mt-2">

                            <div class="row">
                                <div class="col-4">
                                    <p class="card-text mb-0">Tanggal sewa</p>
                                    <p class="card-text">Tanggal kembali</p>
                                </div>
                                <div class="col-6">
                                    <p class="card-text mb-0">${data.data.tgl_sewa}</p>
                                    <p class="card-text">${data.data.tgl_kembali}</p>
                                </div>
                            </div>

                        <div>
                    <div>
                `);
            },
            error : function(data){
                
            }
        });
        }
    




</script>

@endsection