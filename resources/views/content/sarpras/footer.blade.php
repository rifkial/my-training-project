<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

	const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: false,
        };

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    };

    function MakeTable(tabel,url,data,column){

        tabel.DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    }

    let datas =1 ;

</script>