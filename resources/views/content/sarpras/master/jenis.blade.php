@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

    .input-group a{
        color: #fff !important;
    }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Daftar Jenis Barang</h4>
		<button type="button" class="btn btn-info " onclick="modalCreate('jenis')">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-jenis">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


@include('content.sarpras.master.modal')
@include('content.sarpras.layout_sarpras')

<script>

    let tabel = $('#data-jenis');
    
    $(function(){
        let url = "{{ route('main-jenis-barang') }}";
        let column = 
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "60%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
        ;

        MakeTable(tabel,url,column);
    })

        modalEditJenis = function (id){
            $.ajax({
                type: "GET",
                url:"{{ route('detail-jenis-barang') }}",
                data : {
                    'id' : id
                },
                success: function(data) {
                  console.log(data);
                  $('#nama-edit').val(data.data.nama);
                  $('#id-edit').val(id);
                }
            });
            $('#routes-edit').val("jenis");
            $("#modal-edit").modal("show");
        }

        deleteJenis = function (id){
            deleteItem("jenis",id);
        }

        //modalEditJenis();

        $('#modal-create').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        })
    

</script>

@endsection

