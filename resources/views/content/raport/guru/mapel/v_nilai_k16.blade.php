@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">

        <div class="col-md-12">
            <div class="alert alert-warning">
                <b>Petunjuk : </b><br>
                <ul>
                    <li>Menu ini digunakan untuk menginput nilai pengetahuan pada mata pelajaran
                        <b><i><?php
                        echo $guru['mapel'] . ', kelas ' . $guru['rombel']; ?>.</i></b>
                    </li>
                    <li>Nilai yang diinput akan otomatis di kalkulasikan dengan settingan config nilai yang di setting di
                        halaman admin per semesternya. </li>
                    <li>Untuk saat ini rumus <br> <i>Nilai Harian Rata - rata adalah <b class="text-info"><u> "nilai
                                    yang diinput x
                                    {{ $config['nilai_uh_rata'] }} : 100"</i></b></u>, <br>
                        <i>Nilai UTS adalah <b class="text-info"><u> "nilai yang diinput x
                                    {{ $config['nilai_uts'] }} : 100"</i></u></b>,
                        <br> <i>Nilai UAS adalah <b class="text-info"><u>"nilai yang diinput x
                                    {{ $config['nilai_uas'] }} : 100"</i></u></b>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="m-2"><b>Nilai K16 &raquo; <?php echo $guru['mapel'] . ' - ' . $guru['rombel']; ?> </b>
                    </h5>
                </div>
                <div class="card-body">
                    <form method="post" id="nilai_kd">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tr class="bg-info">
                                    <th width="11%" class="text-center">No</th>
                                    <th width="23%">Profile</th>
                                    <th width="23%">Nilai</th>
                                    <th width="17%">Nilai Akhir</th>
                                    <th width="18%">Deskripsi</th>
                                    <th width="8%">Aksi</th>
                                </tr>
                                <tbody>
                                    <input type="hidden" name="jumlah" value="{{ count($siswa) }}">
                                    <input type="hidden" name="id_guru_pelajaran" value="{{ $guru['id'] }}">
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($siswa as $sw)
                                        <input type="hidden" name="id_siswa_{{ $no }}"
                                            value="{{ $sw['id_kelas_siswa'] }}">
                                        <tr>
                                            <td class="vertical-middle text-center">
                                                <h2>{{ $no }}</h2>
                                            </td>
                                            <td class="vertical-middle"><b>{{ ucwords($sw['nama']) }}</b>
                                                <p class="m-0">NISN. {{ $sw['nisn'] }}</p>
                                            </td>
                                            <td class="text-center">
                                                <div class="form-group mb-3">
                                                    <label for="exampleFormControlInput1">Ulangan Harian</label>
                                                    <input type="number" min="0" max="100"
                                                        name="harian_{{ $no }}"
                                                        id="harian_{{ $sw['id_kelas_siswa'] }}" class="form-control"
                                                        value="{{ $sw['nilai_uh_rata'] }}">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="exampleFormControlInput1">UTS</label>
                                                    <input type="number" min="0" max="100" name="uts_{{ $no }}"
                                                        id="uts_{{ $sw['id_kelas_siswa'] }}" class="form-control"
                                                        value="{{ $sw['nilai_uts'] }}">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="exampleFormControlInput1">UAS</label>
                                                    <input type="number" min="0" max="100" name="uas_{{ $no }}"
                                                        id="uas_{{ $sw['id_kelas_siswa'] }}" class="form-control"
                                                        value="{{ $sw['nilai_uas'] }}">
                                                </div>
                                            </td>


                                            <td class="vertical-middle">
                                                <input type="text" name="akhir_{{ $no }}"
                                                    id="akhir_{{ $sw['id_kelas_siswa'] }}" class="form-control"
                                                    value="{{ $sw['nilai_akhir'] }}">
                                            </td>
                                            <td class="vertical-middle">
                                                <textarea name="deskripsi_{{ $no }}"
                                                    id="deskripsi_{{ $no }}" class="form-control"
                                                    rows="8">{{ $sw['capaian_hasil'] }}</textarea>
                                            </td>
                                            <td class="vertical-middle text-center">
                                                <a href="javascript:void(0)"
                                                    class="btn my-2 btn-info hitung{{ $sw['id_kelas_siswa'] }}"
                                                    onclick="hasil({{ $sw['id_kelas_siswa'] }})"><i
                                                        class="fas fa-angle-double-right"></i> Hitung</a>
                                                <div class="alert alert-info border-info" role="alert">
                                                    <button aria-label="Close" class="close" data-dismiss="alert"
                                                        type="button"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <p>Rumus settingan raport</p>
                                                </div>

                                            </td>

                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td colspan="7"><button type="submit" class="btn btn-success pull-right"
                                                id="btnSave"><i class="fas fa-save"></i> Simpan</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
    <script type="text/javascript">
        hari = "{{ $config['nilai_uh_rata'] }}";
        ts = "{{ $config['nilai_uts'] }}";
        us = "{{ $config['nilai_uas'] }}";
        id_rombel = "{{ $guru['id_rombel'] }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#nilai_kd").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "{{ url('program/raport/nilai-k16/Request::segment(4)/simpan') }}",
                beforeSend: function() {
                    $("#btnSave").attr("disabled", true);
                    $("#btnSave").html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
                },
                success: function(r) {
                    // $("#tbsimpan").attr("disabled", false);
                    $("#btnSave").attr("disabled", false);
                    $("#btnSave").html('<i class="fa fa-save"></i> Simapn');
                    noti(r.icon, r.message);
                }
            });
            return false;
        });

        function hasil(id) {
            let harian = parseFloat($("#harian_" + id).val()) * parseInt(hari) / 100;
            console.log("harian " + harian);
            let uts = parseFloat($("#uts_" + id).val()) * parseInt(ts) / 100;
            console.log("uts " + uts);
            let uas = parseFloat($("#uas_" + id).val()) * parseInt(us) / 100;
            console.log("uas " + uas);
            let hasil_juml = harian + uts + uas;
            console.log("jumlah " + hasil_juml)
            let hasil = parseInt(hasil_juml);
            $('#akhir_' + id).val(hasil);

        }
    </script>


@endsection
