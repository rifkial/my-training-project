@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
            box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
        }

        a.btn.btn-xs.btn-success {
            margin-right: 3px;
        }

    </style>
    <div class="row">

        <div class="col-md-12">
            <div class="alert alert-warning" style="color: #000">
                <b>Petunjuk : </b><br>
                <ul>
                    <li>Menu ini digunakan untuk menginput nilai pengetahuan pada mata pelajaran <b><i><?php
                                echo $guru['mapel'] . ', kelas ' . $guru['rombel']; ?>.</i></b> </li>
                    <li>Jika kompetensi dasar belum ada, silakan klik tombol <b><i>Tambah KD</i></b>. Untuk mengubah atau
                        menghapus nama KD, silakan klik tombol "<i class="fa fa-pencil"></i>" atau "<i
                            class="fa fa-remove"></i>". </li>
                    <li>Untuk mengisikan nilai pengetahuan pada masing-masing KD, silakan klik nama KD, dan akan muncul
                        daftar siswa serta isian nilai. Nilai dalam <b><i>skala 1-100</i></b>. Jangan lupa klik tombol
                        <b><i>Simpan</i></b> di sebelah bawah.
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{-- <a href="<?php echo base_url(); ?>view_mapel" class="btn btn-info"><i class="fa fa-arrow-left"></i> Kembali</a>
                    <a href="<?php echo base_url(); ?>n_pengetahuan/cetak/<?php echo $id_guru_mapel; ?>" class="btn btn-warning" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                    <a href="<?php echo base_url(); ?>n_pengetahuan/import/<?php echo $detil_mp['id_mapel'] . '-' . $detil_mp['id_kelas']; ?>" class="btn btn-danger"><i class="fa fa-download"></i> Download File Excel</a>
                    <a href="<?php echo base_url(); ?>n_pengetahuan/upload/<?php echo $detil_mp['id_mapel'] . '-' . $detil_mp['id_kelas']; ?>" class="btn btn-success"><i class="fa fa-upload"></i> Upload File Excel</a> --}}
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <h5 class="card-header" style="margin-top: 0">
                    Nilai Ketrampilan &raquo; <?php echo $guru['mapel'] . ' - ' . $guru['rombel']; ?>
                </h5>
                <p style="margin: 5px !important">
                    <a href="javascript:void(0)" onclick="return edit(0);" class="btn btn-sm btn-info"><i
                            class="fa fa-plus-circle"></i>
                        Tambah KD
                    </a>
                </p>
                <ul class="list-group list-group-flush" id="list_kd">
                    <div id="list_kd_2" style="margin-bottom: 10px"></div>
                </ul>
            </div>

        </div>
        <div class="col-md-7">
            <div class="card">
                <h5 class="card-header" style="margin-top: 0">Input Nilai</h5>
                <div class="card-body">
                    <form name="f_input_nilai" action="javascript:void(0)" id="f_input_nilai">
                        @csrf
                        <input type="hidden" name="id_guru_mapel" id="id_guru" value="{{ $guru['id'] }}">
                        <input type="hidden" name="id_mapel_kd" id="id_mapel_kd" value="">
                        <input type="hidden" name="id_kelas" id="id_kelas" value="{{ $guru['id_kelas'] }}">
                        <input type="hidden" name="id_rombel" id="id_rombel" value="{{ $guru['id_rombel'] }}">
                        <input type="hidden" name="id_mapel" id="id_mapel" value="{{ $guru['id_mapel'] }}">
                        <input type="hidden" name="jenis" id="jenis" value="">
                        <div id="load_nilai">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md modal-side modal-bottom-left">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Set KD</h5>
                </div>
                <form class="form-horizontal" method="post" id="f_setmapel" name="f_setmapel"
                    onsubmit="return simpan_kd();">
                    <input type="hidden" name="_id" id="_id" value="">
                    <input type="hidden" name="_mode" id="_mode" value="">
                    <input type="hidden" name="id_guru" id="id_guru" value="{{ $guru['id_guru'] }}">
                    <input type="hidden" name="id_mapel" id="id_mapel" value="{{ $guru['id_mapel'] }}">
                    <input type="hidden" name="id_kelas" id="id_kelas" value="{{ $guru['id_kelas'] }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="col-sm-12 control-label">Pilih Kompetensi Inti</label>
                            <div class="col-sm-12">
                                <select name="inti" id="inti" class="form-control">
                                    @foreach ($inti as $it)
                                        <option value="{{ $it['id'] }}">{{ $it['kompetensi_inti'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama" class="col-sm-3 control-label">Kode</label>
                            <div class="col-sm-12">
                                <input type="text" name="kode" class="form-control" autofocus="true" id="kode" required>
                                <!--<p style="color: red; font-size: 10pt">DILARANG memakai spasi, koma, strip. Contoh : K01</p>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama" class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-12">
                                <textarea name="nama" class="form-control" id="nama" required
                                    style="height: 100px"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="tbSimpanKd">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        id_mapel = "{{ $guru['id_mapel'] }}";
        id_kelas = "{{ $guru['id_kelas'] }}";
        id_guru = "{{ $guru['id_guru'] }}";
        id_rombel = "{{ $guru['id_rombel'] }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        view_kd(0, 0);
        list_kd();
        $(document).on("ready", function() {
            $('#list_kd li').on('click', function() {
                $('li.active').removeClass('active');
                $(this).addClass('active');
            });


        });

        $("#f_input_nilai").on("submit", function() {
            // console.log("save data");
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "{{ url('program/raport/nilai-ketrampilan/Request::segment(4)/store') }}",
                beforeSend: function() {
                    $("#tbSimpan").attr("disabled", true);
                },
                success: function(data) {
                    console.log(data);
                    $("#tbSimpan").attr("disabled", false);
                    if (data.status == "gagal") {
                        noti(data.icon, data.message);
                    } else {
                        $("#ajaxModel").modal('hide');
                        noti(data.icon, data.message);
                    }
                }
            });
            return false;
        });

        function edit(id) {
            if (id == 0) {
                $("#_mode").val('add');
            } else {
                $("#_mode").val('edit');
            }
            $("#kode").prop("readonly", true);
            $("#nama").prop("readonly", true);
            $("#inti").prop("disabled", true);
            // $("#tbSimpan").prop("disabled", true);
            $("#kode").val('');
            $("#nama").val('');
            $("#ajaxModel").modal('show')
            $.ajax({
                type: "POST",
                url: "{{ url('admin/master/kompetensi_dasar') }}",
                data: {
                    id_kompetensi_dasar: id
                },
                success: function(data) {
                    $("#_id").val(data.id);
                    $("#inti").val(data.id_kompetensi_inti).trigger("change");
                    $("#kode").val(data.kode_kompetensi);
                    $("#nama").val(data.nama_kompetensi);
                    $("#kode").prop("readonly", false);
                    $("#inti").prop("disabled", false);
                    $("#nama").prop("readonly", false);
                    $("#tbSimpan").prop("disabled", false);
                }
            });
            return false;
        }

        function view_kd(id, kelas) {

            if (id == 0 && kelas == 0) {
                $("#load_nilai").html('<div class="alert alert-warning">Silakan pilih KD di samping</div>');
            } else {
                $("#id_mapel_kd").val(id);
                $("#jenis").val(jenis);

                $("#load_nilai").html("Loading...");
                $.ajax({
                    type: "POST",
                    url: "{{ url('admin/master/kompetensi_dasar/ambil_siswa_ketrampilan') }}",
                    data: {
                        id_kompetensi_dasar: id,
                        id_rombel: id_rombel,
                        id_mapel: id_mapel,
                        id_guru: id_guru,
                        id_kelas: id_kelas
                    },
                    beforeSend: function() {
                        $("#tbSimpanKd").attr("disabled", true);
                    },
                    success: function(data) {
                        // console.log(data);
                        $("#load_nilai").show('slow');
                        html =
                            '<table class="table table-condensed table-bordered table-hover"><thead><tr><th width="10%">No</th><th width="35%">Nama</th><th width="30%">NISN</th><th width="25%">Nilai</th></tr></thead><tbody>';
                        var i = 1;
                        $.each(data.data, function(k, v) {
                            html += '<tr><td>' + i + '</td><td>' + v.nama +
                                '</td><td>' + v.nisn +
                                '</td><td><input name="id_siswa[]" type="hidden" value="' + v.id_kelas_siswa +
                                '"><input name="nilai[]" type="number" min="0" max="100" class="form-control input-sm" value="' +
                                v.nilai + '"></td></tr>';
                            i++;
                        });
                        html +=
                            '</tbody></table><p><button type="submit" class="btn btn-success" id="tbSimpan"><i class="fa fa-check"></i> Simpan</button> &nbsp; <a href="#" class="btn btn-warning" onclick="return view_kd(0, 0);"><i class="fa fa-minus-circle"></i> Batal</a></p>';
                        $("#load_nilai").html(html);
                    }
                });

            }
            return false;
        }



        function hapus(id) {
            if (id == 0) {
                noti("danger", "Silakan pilih datanya..!");
            } else {
                if (confirm('Anda yakin...?')) {

                    $.ajax({
                        type: "POST",
                        url: "{{ url('admin/master/kompetensi_dasar/delete') }}",
                        data: {
                            id_kompetensi_dasar: id
                        },
                        success: function(data) {
                            noti(data.icon, data.message);
                            list_kd();
                        }
                    });
                }
            }
            return false;
        }

        function simpan_kd() {
            var data = $("#f_setmapel").serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: "{{ url('admin/master/kompetensi_dasar/simpan_ketrampilan') }}",
                beforeSend: function() {
                    $("#tbSimpanKd").attr("disabled", true);
                },
                success: function(r) {
                    console.log(r);
                    $("#tbSimpanKd").attr("disabled", false);
                    if (r.status == "gagal") {
                        noti(r.success, r.message);
                    } else {
                        $("#ajaxModel").modal('hide');
                        noti(r.success, r.message);
                        list_kd();
                    }
                }
            });
            return false;
        }

        function list_kd() {
            $.ajax({
                type: "POST",
                url: "{{ url('program/raport/nilai-pengetahuan/' . Request::segment(4) . '/list_kd') }}",
                data: {
                    id_mapel: id_mapel,
                    id_kelas: id_kelas,
                    jenis: "keterampilan"
                },
                beforeSend: function() {
                    $("#list_kd_2").html(
                        '<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
                },
                success: function(data) {
                    var h = '';
                    if (data.length > 0) {
                        $.each(data, function(i, v) {
                            h += '<li class="list-group-item"><a href="#" onclick="return view_kd(' + v
                                .id + ', {{ $guru['id_kelas'] }});">' + v.nama +
                                '</a><div class="pull-right"><a href="#" onclick="return edit(' + v.id +
                                ');" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a><a href="#" onclick="return hapus(' +
                                v.id +
                                ');" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> </a></div></li>';
                        });
                    } else {
                        h += '<div class="alert alert-info">KD Belum satupun diinputkan</div>';
                    }

                    $("#list_kd_2").html(h);
                }
            });
        }

    </script>


@endsection
