@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <b>Petunjuk : </b><br>
                            Menu ini digunakan untuk menginput nilai pada setiap masing-masing mata pelajaran diampu.
                            Silakan klik menu <b><i>Nilai Pengetahuan</i></b> untuk menginput nilai pengetahuan, dan
                            <b><i>Nilai Keterampilan</i></b> untuk menginput nilai keterampilan.
                        </div>
                    </div>
                    @if (!empty($mapel))
                        @foreach ($mapel as $item)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h5 class="m-0 text-white">
                                            {{ $item['mapel'] . ' - ' . $item['rombel'] }}</h5>
                                        <small class="text-white">{{ $item['jurusan'] }}</small>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <a href="{{ route('raport-nilai_pengetahuan', (new \App\Helpers\Help())->encode($item['id'])) }}"
                                                class="text-info">
                                                &raquo; Nilai Pengetahuan
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="{{ route('raport-nilai_ketrampilan', (new \App\Helpers\Help())->encode($item['id'])) }}"
                                                class="text-info">
                                                &raquo; Nilai Ketrampilan
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="{{ route('raport-nilai_manual', (new \App\Helpers\Help())->encode($item['id'])) }}"
                                                class="text-info">
                                                &raquo; Nilai Manual
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="{{ route('raport-nilai_k16', (new \App\Helpers\Help())->encode($item['id'])) }}"
                                                class="text-info">
                                                &raquo; Nilai K16
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-info">Belum ada mapel yang diampu..</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
