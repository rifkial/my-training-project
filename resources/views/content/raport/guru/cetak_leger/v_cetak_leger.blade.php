@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .card {
            box-shadow: 0 1.5em 2.5em -0.5em rgb(0 0 0 / 10%);
        }

        .card:hover {
            background: #753BBD;
            transform: scale(1.02);
        }

        h3.text-center:hover {
            color: #FFFFFF;
            border-bottom-color: #A754C4;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h5 class="box-title">{{ session('title') }}</h5>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xl-6 col-sm-6 col-12">
                            <a href="{{ route('raport-cetak_leger') }}">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="media d-flex">
                                                <div class="align-self-center">
                                                    <i class="fas fa-print fa-5x float-left text-info"></i>
                                                </div>
                                                <div class="media-body text-right">
                                                    <h3 class="text-center">Nilai Pengetahuan dan Ketrampilan</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-6 col-sm-6 col-12">
                            <a href="#">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="media d-flex">
                                                <div class="align-self-center">
                                                    <i class="fas fa-print fa-5x float-left text-info"></i>
                                                </div>
                                                <div class="media-body text-right">
                                                    <h3 class="text-center">Nilai Ekstra dan Absensi</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
