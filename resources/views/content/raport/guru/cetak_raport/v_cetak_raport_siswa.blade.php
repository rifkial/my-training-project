@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <hr>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="bg-info">
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($tahun as $th)
                                        <tr>
                                            <td class="vertical-middle">{{ $no++ }}</td>
                                            <td><b>Tahun Ajaran {{ $th['tahun_ajaran'] }}</b>
                                                <p class="m-0">Semester {{ $th['semester'] }}</p>
                                            </td>
                                            <td class="vertical-middle">
                                                <a href="{{ route('raport-cetak_sampul_1', (new \App\Helpers\Help())->encode($th['id'])) }}"
                                                    target="_blank" class="btn btn-pinterest"><i
                                                        class="fas fa-file-pdf"></i> Lihat/Cetak</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
