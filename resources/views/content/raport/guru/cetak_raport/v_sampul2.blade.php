<!DOCTYPE html>
<html>

<head>
    <title>Cetak Raport</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <center>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <b>LAPORAN</b><br><br>
        HASIL PENCAPAIAN KOMPETENSI PESERTA DIDIK<br>
        {{ strtoupper($siswa['sekolah']) }}
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <table style="margin-left:10%; width: 70%">
            <tr>
                <td width="20%">Nama Sekolah</td>
                <td width="2%">:</td>
                <td width="50%">{{ strtoupper($siswa['sekolah']) }}</td>
            </tr>
            <tr>
                <td>NPSN</td>
                <td>:</td>
                <td>{{ $sekolah['npsn'] != null ? $sekolah['npsn'] : '-' }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $sekolah['alamat'] != null ? $sekolah['alamat'] : '-' }}, Kode Pos :
                    {{ $sekolah['kode_pos'] != null ? $sekolah['kode_pos'] : '-' }}</td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>:</td>
                <td>{{ $sekolah['telepon'] != null ? $sekolah['telepon'] : '-' }}</td>
            </tr>
            <tr>
                <td>Kelurahan</td>
                <td>:</td>
                <td>{{ $sekolah['kelurahan'] != null ? $sekolah['kelurahan'] : '-' }}</td>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td>{{ $sekolah['kecamatan'] != null ? $sekolah['kecamatan'] : '-' }}</td>
            </tr>
            <tr>
                <td>Kabupaten/Kota</td>
                <td>:</td>
                <td>{{ $sekolah['kabupaten'] != null ? $sekolah['kabupaten'] : '-' }}</td>
            </tr>
            <tr>
                <td>Propinsi</td>
                <td>:</td>
                <td>{{ $sekolah['provinsi'] != null ? $sekolah['provinsi'] : '-' }}</td>
            </tr>
            <tr>
                <td>Website</td>
                <td>:</td>
                <td>{{ $sekolah['website'] != null ? $sekolah['website'] : '-' }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td>{{ $sekolah['email'] != null ? $sekolah['email'] : '-' }}</td>
            </tr>
        </table>
    </center>
</body>

</html>
