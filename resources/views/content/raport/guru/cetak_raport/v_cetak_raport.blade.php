@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row">
                    @if (session('role') == 'walikelas')
                        <div class="col-md-12">
                            <div class="alert alert-info border-info" role="alert">
                                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                        aria-hidden="true">×</span>
                                </button>
                                <p><strong>Informasi Settingan Config:</strong>
                                </p>
                                <ul class="mr-t-10">
                                    <li><strong>Tahun Ajaran &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                        </strong>{{ $config['tahun_ajaran'] }}</li>
                                    <li><strong>Semester
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                                        </strong>{{ $config['semester'] }}</li>
                                    <li><strong>Tanggal Raport &nbsp;&nbsp;&nbsp;:
                                        </strong>{{ (new \App\Helpers\Help())->getTanggal($config['tgl_raport']) }}</li>
                                    <li><strong>Jenis
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            : </strong>{{ $config['jenis'] == 'kd' ? 'Kompetensi Dasar' : 'Manual' }}</li>
                                </ul>
                            </div>
                        </div>
                    @endif


                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h5 class="box-title mr-b-0 text-white">Cetak Raport</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover" id="table-data">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="40%">Profil</th>
                                                <th width="50%">Cetak</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($siswa as $ssw)

                                                <tr>
                                                    <td class="vertical-middle">{{ $no }}</td>
                                                    <td><b>{{ $ssw['nama'] }}</b>
                                                        <p class="m-0">NIS/NISN.
                                                            {{ $ssw['nis'] . '/' . $ssw['nisn'] }}</p>
                                                    </td>
                                                    <td class="vertical-middle">
                                                        <a href="{{ route('raport-cetak_sampul_1', (new \App\Helpers\Help())->encode($ssw['id'])) }}"
                                                            target="_blank" class="btn btn-success btn-sm"><i
                                                                class="fa fa-eye"></i> Lihat</a>
                                                    </td>
                                                </tr>
                                                @php
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        {{-- <div class="card">
                            <div class="header">
                                <h4 class="title">Cetak Raport </h4>
                            </div>
                            <div class="content">

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="40%">Profil</th>
                                            <th width="50%">Cetak</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($siswa as $ssw)

                                            <tr>
                                                <td class="vertical-middle">{{ $no }}</td>
                                                <td><b>{{ $ssw['nama'] }}</b>
                                                    <p class="m-0">NIS/NISN.
                                                        {{ $ssw['nis'] . '/' . $ssw['nisn'] }}</p>
                                                </td>
                                                <td class="vertical-middle">
                                                    <a href="{{ route('raport-cetak_sampul_1', (new \App\Helpers\Help())->encode($ssw['id'])) }}"
                                                        target="_blank" class="btn btn-success btn-sm"><i
                                                            class="fa fa-eye"></i> Lihat</a>
                                                </td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        function cetak5(id) {
            $("#print5" + id).printPage({
                url: "cetak-raport/cetak1/" + id,
                attr: "href",
                message: "Your document is being created"
            });
            console.log(id);
        }

        function cetak2(id) {
            $("#print2" + id).printPage({
                url: "cetak-raport/cetak2/" + id,
                attr: "href",
                message: "Your document is being created"
            });
            console.log(id);
        }
    </script>

@endsection
