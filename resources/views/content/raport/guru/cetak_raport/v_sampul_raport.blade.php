<!DOCTYPE html>
<html>

<head>
    <title>Cetak Raport</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 11pt;
            width: 8.5in
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #000;
            padding: 3px;
        }

        .table tr th {
            font-weight: bold;
            text-align: center
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        .tbl {
            font-weight: bold
        }

        table tr td {
            vertical-align: top
        }

        .font_kecil {
            font-size: 12px
        }

        .center {
            text-align: center;
            vertical-align: middle;
        }

        /* 
        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        } */

    </style>
    {{-- <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script> --}}
</head>

<body>
    <table>
        <thead>
            <!-- biar bisa ganti lembar otomatis -->
        </thead>

        <tbody>
            <tr>
                <td colspan="6" style="text-align: center; font-weight: bold">
                    <p>
                    <h3>HASIL PENCAPAIAN KOMPETENSI PESERTA DIDIK</h3>
                    </p>
                </td>
            </tr>
            <tr>
                <td width="20%">Nama Sekolah</td>
                <td width="1%">:</td>
                <td width="39%" class="tbl">{{ $siswa['sekolah'] }}</td>
                <td width="20%">Kelas</td>
                <td width="1%">:</td>
                <td width="19%" class="tbl">{{ $siswa['rombel'] }}</td>
            </tr>
            <tr>
                <td>Alamat Sekolah</td>
                <td>:</td>
                <td class="tbl">{{ $siswa['alamat_sekolah'] }}</td>
                <td>Semester</td>
                <td>:</td>
                <td class="tbl">{{ $siswa['semester'] }}</td>
            </tr>
            <tr>
                <td>Nama Siswa</td>
                <td>:</td>
                <td class="tbl">{{ $siswa['nama'] }}</td>
                <td>Tahun Pelajaran</td>
                <td>:</td>
                <td class="tbl">{{ $siswa['tahun_ajaran'] }}</td>
            </tr>
            <tr>
                <td>NIS / NISN</td>
                <td>:</td>
                <td class="tbl">{{ $siswa['nis'] . ' / ' . $siswa['nisn'] }}</td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td colspan="6"><br><br></td>
            </tr>
            <tr>
                <td colspan="6"><b>A. Sikap</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="margin-left: 15px">
                        <tr>
                            <td width="3%"><b>1.</b></td>
                            <td width="97%"><b>Sikap Spiritual</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="border: solid 1px #000; padding: 10px">Deskripsi : {{ $spiritual }}</td>
                        </tr>
                        <tr>
                            <td width="3%"><b>2.</b></td>
                            <td width="97%"><b>Sikap Sosial</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="border: solid 1px #000; padding: 10px">{{ $sosial }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6"><br><b>B. Pengetahuan dan Keterampilan</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2" rowspan="2" width="30%">Mata Pelajaran</th>
                                <th colspan="3">Pengetahuan</th>
                                <th colspan="3">Keterampilan</th>
                            </tr>
                            <tr>
                                <th width="5%">Angka</th>
                                <th width="5%">Predikat</th>
                                <th width="25%">Deskripsi</th>
                                <th width="5%">Angka</th>
                                <th width="5%">Predikat</th>
                                <th width="25%">Deskripsi</th>
                            </tr>

                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($nilai_utama as $nu)
                                @php
                                    $deskripsi = '-';
                                    if ($nu['deskripsi_pengetahuan'] != null) {
                                        $deskripsi = str_replace('#', '. ', $nu['deskripsi_pengetahuan']);
                                    } else {
                                        $desksripsi = '-';
                                    }
                                @endphp
                                <tr>
                                    <td class="center">{{ $no++ }}</td>
                                    <td class="center">{{ $nu['mapel'] != null ? $nu['mapel'] : '-' }}</td>
                                    <td class="center">
                                        {{ $nu['nilai_akhir_pengetahuan'] != null ? $nu['nilai_akhir_pengetahuan'] : '-' }}
                                    </td>
                                    <td class="center">
                                        {{ $nu['nilai_predikat_pengetahuan'] != null ? $nu['nilai_predikat_pengetahuan'] : '-' }}
                                    </td>
                                    <td>{{ $deskripsi }}</td>
                                    <td class="center">
                                        {{ $nu['nilai_akhir_kerampilan'] != null ? $nu['nilai_akhir_kerampilan'] : '-' }}
                                    </td>
                                    <td class="center">
                                        {{ $nu['predikat_kerampilan'] != null ? $nu['predikat_kerampilan'] : '-' }}
                                    </td>
                                    <td class="center">
                                        {{ $nu['deskripsi_keterampilan'] != null ? $nu['deskripsi_keterampilan'] : '-' }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6"><br><b>C. Ekstrakurikuler</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="30%">Nama Kegiatan</th>
                                <th width="10%">Nilai</th>
                                <th width="55%">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($ekstra))
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($ekstra as $ne)
                                    @php
                                        $desk = '';
                                        if ($ne['nilai'] == 'A') {
                                            $desk = 'Memuaskan, aktif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                        } elseif ($ne['nilai'] == 'B') {
                                            $desk = 'Cukup Memuaskan, aktif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                        } elseif ($ne['nilai'] == 'C') {
                                            $desk = 'Kurang Memuaskan, pasif mengikuti kegiatan ' . $ne['nama'] . ' mingguan';
                                        } else {
                                            $desk = '-';
                                        }
                                    @endphp
                                    <tr>
                                        <td class="ctr"><?php echo $no; ?></td>
                                        <td><?php echo $ne['nama']; ?></td>
                                        <td class="ctr"><?php echo $ne['nilai']; ?></td>
                                        <td><?php echo $desk; ?></td>
                                    </tr>
                                @endforeach
                                @php
                                    $no++;
                                @endphp
                            @else
                                <tr>
                                    <td colspan="4">-</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6"><br><b>D. Ketidakhadiran</b></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table width="100%">
                        <tr>
                            <td width="40%">
                                <table class="table" width="100%">
                                    <tr>
                                        <td width="60%">Sakit</td>
                                        <td width="40%" class="ctr">{{ $absensi['sakit'] }} &nbsp; hari</td>
                                    </tr>
                                    <tr>
                                        <td>Izin</td>
                                        <td class="ctr">{{ $absensi['izin'] }} &nbsp; hari</td>
                                    </tr>
                                    <tr>
                                        <td>Tanpa Keterangan</td>
                                        <td class="ctr">{{ $absensi['alpha'] }} &nbsp; hari</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="60%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @if ($semester == 2)
                <tr>
                    <td colspan="6">
                        @php
                            $naik_kelas = $siswa['kelas'] + 1;
                            $kelas_now = $siswa['kelas'];
                        @endphp

                        @if ($kelas_now != 9 && $kelas_now != 6 && $kelas_now != 12)
                            <div style="border: solid 1px; padding: 10px; margin-top: 40px">
                                <b>Keputusan : </b>
                                <p>Berdasarkan pencapaian kompetensi pada semester ke-1 dan ke-2, peserta didik
                                    ditetapkan
                                    *)
                                    :<br>
                                <div style="display: block">
                                    <div style="diplay: inline; float: left; width: 200px">naik ke kelas </div>
                                    <div style="diplay: inline; float: left; font-weight: bold">{{ $naik_kelas }}
                                    </div>
                                </div><br>
                                <div style="display: block">
                                    <div style="diplay: inline; float: left; width: 200px"><strike>tinggal di
                                            kelas</strike>
                                    </div>
                                    <div style="diplay: inline; float: left; font-weight: bold">
                                        <strike>{{ $kelas_now }}</strike>
                                    </div>
                                </div>
                                <br><br>
                                *) Coret yang tidak perlu
                            </div>
                        @else
                            <div style="border: solid 1px; padding: 10px; margin-top: 40px">
                                <b>Keputusan : </b>
                                <p>Berdasarkan pencapaian kompetensi pada kelas 7, 8 dan 9, maka, peserta didik
                                    dinyatakan :
                                    *)
                                    :<br>
                                <div style="display: block; font-weight: bold">
                                    LULUS / <strike>TIDAK LULUS</strike>
                                </div><br><br>
                                *) Coret yang tidak perlu
                            </div>
                        @endif
                    </td>
                </tr>
            @endif
            <tr>
                <td colspan="6">
                    <br><br>
                    <table width="100%" style="border: 1px solid">
                        <tr>
                            <td width="10%"></td>
                            <td width="20%">
                                Mengetahui : <br>
                                Orang Tua/Wali, <br>
                                <br><br><br><br>
                                <u>..........................</u>
                            </td>
                            <td width="8%"></td>
                            <td width="25%">
                                <br>
                                Wali Kelas <br>
                                <br><br><br><br>
                                <u><b>{{ $wali != null ? ucwords($wali['nama']) : '-' }}</b></u>
                                <br>
                                NIP. {{ $wali != null ? $wali['nip'] : '-' }}
                            </td>
                            <td width="5%"></td>
                            <td width="32%">
                                {{-- @if ($siswa['kelas'] != 9 && $siswa['kelas'] != 6 && $siswa['kelas'] != 12) --}}
                                {{-- {{ dd($config) }} --}}
                                {{ $config['kabupaten'] != null ? $config['kabupaten'] : '-' }},
                                {{ $config['tgl_raport'] != null ? $config['tgl_raport'] : '-' }}
                                {{-- @endif --}}
                                <br>
                                Kepala {{ $config['sekolah'] }}
                                <br>
                                <br><br><br><br>
                                <u><b><?php echo $config['kepsek']; ?></b></u><br>
                                NIP. <?php echo $config['nip_kepsek']; ?>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

        </tbody>
    </table>
</body>

</html>
