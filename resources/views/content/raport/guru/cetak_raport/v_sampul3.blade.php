<!DOCTYPE html>
<html>

<head>
    <title>Cetak Raport</title>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 12pt;
            width: 8.5in;
            height: 12.5in;
            /* border: solid 2px #000; */
            padding: 30px 10px
        }

        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

    </style>
    <script type="text/javascript">
        function PrintWindow() {
            window.print();
            CheckWindowState();
        }

        function CheckWindowState() {
            if (document.readyState == "complete") {
                window.close();
            } else {
                setTimeout("CheckWindowState()", 1000)
            }
        }
        PrintWindow();
    </script>
</head>

<body>
    <table>
        <tr>
            <td width="10%">
                @if (!empty($kop['file']))
                    <img src="{{ $kop['file'] }}" style="margin-top: 10px; width: 90px;">
                @endif

            </td>
            <td style="text-align: center"><b>{{ $kop['title'] == null ? '' : $kop['title'] }}<br>
                    {{ $kop['sub_title'] == null ? '' : $kop['sub_title'] }} <br>
                    {{-- {{ $sekolah['kabupaten'] != null ? strtoupper($sekolah['kabupaten']) : '-' }}<br> --}}
                    {{ strtoupper($kop['sekolah']) }}<br></b>
                <span style="font-size: 10pt">Alamat : {{ strtoupper($kop['alamat']) }}. <br>
                    Telp : {{ $kop['telp'] != null ? strtoupper($kop['telp']) : '-' }} | Web :
                    {{ $kop['website'] != null ? $kop['website'] : '-' }}</span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border: solid 2px #000">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; font-weight: bold; font-size: 14pt">KETERANGAN DIRI TENTANG
                PESERTA DIDIK</td>
        </tr>
        <tr>
            <td colspan="2">

                <table width="100%">
                    <tr>
                        <td width="3%">1.</td>
                        <td width="40%">Nama Peserta Didik (Lengkap)</td>
                        <td width="2%">:</td>
                        <td width="55%">{{ $siswa['nama'] != null ? $siswa['nama'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Nomor Induk</td>
                        <td>:</td>
                        <td>{{ $siswa['nis'] != null ? $siswa['nis'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>NISN</td>
                        <td>:</td>
                        <td>{{ $siswa['nisn'] != null ? $siswa['nisn'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>:</td>
                        <td>{{ $siswa['tmp_lahir'] . ', ' . \Carbon\Carbon::parse($siswa['tgl_lahir'])->translatedFormat('j F, Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $siswa['jenkel'] == 'l' ? 'Laki-laki' : 'Perempuan' }}</td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Agama</td>
                        <td>:</td>
                        <td>{{ $siswa['agama'] != null ? $siswa['agama'] : '-' }}</td>
                    </tr>
                    {{-- <tr>
                        <td>7.</td>
                        <td>Anak Ke</td>
                        <td>:</td>
                        <td>{{ $siswa['anak_ke'] != null ? $siswa['anak_ke'] : '-' }}</td>
                    </tr> --}}
                    <tr>
                        <td>8.</td>
                        <td>Status dalam Keluarga</td>
                        <td>:</td>
                        <td>{{ $siswa['status_keluarga'] != null ? $siswa['status_keluarga'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Alamat Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $siswa['alamat'] != null ? $siswa['alamat'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        @php
                            $saat_ini = '';
                            if ($sekolah['jenjang'] == 'dasar') {
                                $saat_ini = 'SD / MI';
                            } elseif ($sekolah['jenjang'] == 'menengah') {
                                $saat_ini = 'SMP';
                            } elseif ($sekolah['jenjang'] == 'atas') {
                                $saat_ini = 'SMA';
                            } else {
                                $saat_ini = 'SMK';
                            }
                        @endphp
                        <td>Diterima di
                            @if ($sekolah['jenjang'] == 'dasar')
                                {{ $saat_ini }} ini
                            @elseif($sekolah['jenjang'] == "menengah")
                                {{ $saat_ini }} ini
                            @elseif($sekolah['jenjang'] == "atas")
                                {{ $saat_ini }} ini
                            @else
                                {{ $saat_ini }} ini
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Di Kelas</td>
                        <td>:</td>
                        <td>{{ $siswa['kelas_diterima'] != null ? $siswa['kelas_diterima'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Pada Tanggal</td>
                        <td>:</td>
                        <td>{{ \Carbon\Carbon::parse($siswa['tgl_diterima'])->translatedFormat('j F, Y') }}</td>
                    </tr>
                    {{-- <tr>
                        <td></td>
                        <td>c. Tahun Semester</td>
                        <td>:</td>
                        <td>{{ $siswa['tahun_angkatan'] != null ? $siswa['tahun_angkatan'] : '-' }}</td>
                    </tr> --}}
                    <tr>
                        <td>12.</td>
                        @php
                            $sebelum = '';
                            if ($sekolah['jenjang'] == 'dasar') {
                                $sebelum = 'TK';
                            } elseif ($sekolah['jenjang'] == 'menengah') {
                                $sebelum = 'SD / MI / Paket A';
                            } elseif ($sekolah['jenjang'] == 'atas') {
                                $sebelum = 'SMP / Madrash / Paket B';
                            } else {
                                $sebelum = 'SMP / Madrash / Paket B';
                            }
                        @endphp
                        <td>Ijazah {{ $sebelum }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Nomor</td>
                        <td>:</td>
                        <td>{{ $siswa['no_ijazah'] != null ? $siswa['no_ijazah'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Tahun</td>
                        <td>:</td>
                        <td>{{ $siswa['th_ijazah'] != null ? $siswa['th_ijazah'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td colspan="3">Surat Keterangan Hasil Ujian Akhir Sekolah Berstandar Nasional (UASBN)
                            {{ $sebelum }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Nomor</td>
                        <td>:</td>
                        <td>{{ $siswa['no_skhun'] != null ? $siswa['no_skhun'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Tahun</td>
                        <td>:</td>
                        <td>{{ $siswa['th_skhun'] != null ? $siswa['th_skhun'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Nama Ayah</td>
                        <td>:</td>
                        <td>{{ $siswa['nama_ayah'] != null ? $siswa['nama_ayah'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Nama Ibu</td>
                        <td>:</td>
                        <td>{{ $siswa['nama_ibu'] != null ? $siswa['nama_ibu'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Pekerjaan Orang Tua</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>{{ $siswa['pekerjaan_ayah'] != null ? $siswa['pekerjaan_ayah'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>{{ $siswa['pekerjaan_ibu'] != null ? $siswa['pekerjaan_ibu'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Wali</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Nama Wali</td>
                        <td>:</td>
                        <td>{{ $siswa['nama_wali'] != null ? $siswa['nama_wali'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Alamat Wali</td>
                        <td>:</td>
                        <td>{{ $siswa['alamat_wali'] != null ? $siswa['alamat_wali'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>c. Telepon/HP</td>
                        <td>:</td>
                        <td>{{ $siswa['telp_wali'] != null ? $siswa['telp_wali'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>d. Pekerjaan Wali</td>
                        <td>:</td>
                        <td>{{ $siswa['pekerjaan_wali'] != null ? $siswa['pekerjaan_wali'] : '-' }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding: 24px"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div
                                style="display: inline; float: right; width: 3cm; height: 3.7cm; border: solid 1px #000">
                        </td>
                        <td colspan="3">
                            <div style="margin-left: 120px; display: inline; float: right;">
                                {{ $config['kabupaten'] != null ? $config['kabupaten'] : '-' }},
                                {{ $config['tgl_raport'] != null ? $config['tgl_raport'] : '-' }}
                                <br>
                                Kepala {{ $saat_ini }}<br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <b><u>{{ $config['kepsek'] != null ? $config['kepsek'] : '-' }}</u></b><br>
                                NIP. {{ $config['nip_kepsek'] != null ? $config['nip_kepsek'] : '-' }}
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
