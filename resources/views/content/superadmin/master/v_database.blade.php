@extends('template.template_horizontal_nav_icons.app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .modal-confirm .close {
            top: -13px !important;
            right: -15px !important;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        .modal-confirm {
            color: #636363;
            width: 400px;
        }

        .modal-confirm .modal-content {
            padding: 20px;
            border-radius: 5px;
            border: none;
            text-align: center;
            font-size: 14px;
        }

        .modal-confirm .modal-header {
            border-bottom: none;
            position: relative;
        }

        .modal-confirm h4 {
            text-align: center;
            font-size: 26px;
            margin: 30px 0 -10px;
        }

        .modal-confirm .close {
            position: absolute;
            top: -5px;
            right: -2px;
        }

        .modal-confirm .modal-body {
            color: #999;
        }

        .modal-confirm .modal-footer {
            border: none;
            text-align: center;
            border-radius: 5px;
            font-size: 13px;
            padding: 10px 15px 25px;
        }

        .modal-confirm .modal-footer a {
            color: #999;
        }

        .modal-confirm .icon-box {
            width: 80px;
            height: 80px;
            margin: 0 auto;
            border-radius: 50%;
            z-index: 9;
            text-align: center;
            border: 3px solid #03a9f3;
        }

        .modal-confirm .icon-box i {
            color: #03a9f3;
            font-size: 46px;
            display: inline-block;
            margin-top: 13px;
        }

        .modal-confirm .btn,
        .modal-confirm .btn:active {
            color: #fff;
            border-radius: 4px;
            background: #03a9f3;
            text-decoration: none;
            transition: all 0.4s;
            line-height: normal;
            min-width: 120px;
            border: none;
            min-height: 40px;
            border-radius: 3px;
            margin: 0 5px;
        }

        .modal-confirm .btn-secondary {
            background: #f30303;
        }

        .modal-confirm .btn-secondary:hover,
        .modal-confirm .btn-secondary:focus {
            background: #f30303;
        }

        .modal-confirm .btn-danger {
            background: #03a9f3;
        }

        .modal-confirm .btn-danger:hover,
        .modal-confirm .btn-danger:focus {
            background: #03a9f3;
        }

        .trigger-btn {
            display: inline-block;
            margin: 100px auto;
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PATH</th>
                        <th>Created At</th>
                        <th>Ukuran</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header flex-column">
                        <div class="icon-box">
                            <i class="fa fa-database"></i>
                        </div>
                        <h4 class="modal-title w-100">Apa kamu yakin?</h4>
                    </div>
                    <div class="modal-body p-0">
                        <p>Proses backup akan membutuhkan waktu tunggu, pastikan sinyal internet tidak terputus.</p>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="submit" class="btn btn-info">Ya, Backup sekarang</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    text: 'Create Backup',
                    className: 'btn btn-sm btn-facebook',
                    attr: {
                        title: 'Tambah Data',
                        id: 'createNewCustomer'
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('.tambahBaris').show('');
                $('#modelHeading').html("Tambah Data Jurusan");
                $('#myModal').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "GET",
                    url: "{{ route('supermaster-create_backup_database') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        // if (data.status == 'berhasil') {
                        //     $('#CustomerForm').trigger("reset");
                        //     $('#ajaxModel').modal('hide');
                        //     $("#list_sekolah").load(" #list_sekolah");
                        // }
                        // noti(data.icon, data.success);
                        // $('#saveBtn').html('Simpan');
                        // $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "kategori_program/edit",
                    data: {
                        id_kategori: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Kategori Program");
                        $('#saveBtn').val("edit-user");
                        $('#id_kategori').val(data.id);
                        $('#nama').val(data.nama);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "kategori_program/trash/" + id,
                            type: "POST",
                            data: {
                                '_method': 'DELETE'
                            },
                            beforeSend: function() {
                                $(".delete-" + id).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    data.message,
                                    'success'
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    'Proses Penghapusan Gagal :)',
                                    'error'
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }
    </script>
@endsection
