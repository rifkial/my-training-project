@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title"> {{ Session::get('title') }}</h5>
                    <hr>
                    <div style="width: 100%;">
                        <div class="row mb-2">
                            <div class="col-sm-9 col-md-9"></div>
                            <div class="col-sm-3 col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover widget-status-table">
                                <thead>
                                    <tr class="bg-info">
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th rowspan="2">Nama Siswa</th>
                                        <th rowspan="2">NIS</th>
                                        <th rowspan="2">Rombel</th>
                                        <th class="text-center" colspan="3">Pelanggaran</th>
                                        <th class="text-center" rowspan="2">Wali Kelas</th>
                                        @if (session('role') == 'admin-kesiswaan')
                                            <th rowspan="2">Aksi</th>
                                        @endif
                                    </tr>
                                    <tr class="bg-info">
                                        <th class="text-center">Nama Pelanggaran</th>
                                        <th class="text-center">Point</th>
                                        <th class="text-center">Sanksi</th>
                                    </tr>
                                </thead>

                                <tbody id="data-siswa">
                                    @if (!empty($pelanggaran))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($pelanggaran as $pl)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ (new \App\Helpers\Help())->getTanggal($pl['tgl_pelanggaran']) }}
                                                </td>
                                                <td>{{ ucwords($pl['nama']) }}</td>
                                                <td>{{ $pl['nis'] }}</td>
                                                <td>{{ $pl['rombel'] }}</td>
                                                <td>{{ $pl['pelanggaran'] }}</td>
                                                <td>{{ $pl['point'] }}</td>
                                                <td class="text-center">
                                                    {{ $pl['sanksi_pelanggaran'] != null ? $pl['sanksi_pelanggaran'] : '-' }}
                                                    @if (session('role') != 'supervisor' && session('role') != 'bk')
                                                        <a href="javascript:void(0)" class="text-info editSanksi"
                                                            data-id="{{ $pl['id'] }}"
                                                            data-idSanksi="{{ $pl['id_sanksi_pelanggaran'] != null ? $pl['id_sanksi_pelanggaran'] : '' }}"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                    @endif
                                                </td>
                                                <td>{{ $pl['wali_kelas'] }}</td>
                                                @if (session('role') == 'admin-kesiswaan')
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" class="delete"
                                                            data-id="{{ $pl['id'] }}"><i
                                                                class="material-icons list-icon md-18 text-danger">delete</i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" class="text-center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                            {!! $pagination !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalSanksi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading">Edit Sanksi Pelanggaran</h5>
                </div>
                <form id="formSanksi" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_pelanggaran">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Sanksi Pelanggaran</label>
                                    <div class="col-sm-12">
                                        <select name="sanksi" id="sanksi" class="form-control">
                                            <option value="">Pilih sanksi pelanggaran..</option>
                                            @foreach ($sanksi as $ss)
                                                <option value="{{ $ss['id'] }}">
                                                    {{ $ss['sanksi'] . ' - ' . $ss['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label mb-0">Apakah anda ingin mereset Point
                                        ini?</label>
                                    <small class="pl-3 text-danger">*beri tanda entang jika ingin mereset</small>
                                    <div class="col-sm-12">
                                        <div class="form-check">
                                            <input class="form-check-input mx-0" type="checkbox" name="reset"
                                                id="check_reset" value="1">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Reset Point
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kesiswaan_histori-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            }
                            $(loader).html(
                                '<i class="material-icons list-icon md-18 text-danger">delete</i>'
                            );
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.editSanksi', function() {
                let id = $(this).data('id');
                let id_sanksi = $(this).data('idsanksi');
                $('#formSanksi').trigger("reset");
                $('#id_pelanggaran').val(id);
                $('#sanksi').val(id_sanksi).trigger('change');
                $("#check_reset").prop("checked", false);
                $('#modalSanksi').modal('show');
            });

            $('#formSanksi').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('pelanggaran_siswa-update_sanksi') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalSanksi').modal('hide');
                            $('#formSanksi').trigger("reset");
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        swa(data.status + "!", data.message, data.icon);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


        })

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>


@endsection
