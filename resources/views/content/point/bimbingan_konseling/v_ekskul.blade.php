@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget-bg">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                <hr>
                <div class="table-responsive">
                    <p></p>
                    <table class="table table-striped table-bordered table-hover" id="table-data">
                        <thead>
                            <tr class="bg-info">
                                <th rowspan="2" class="text-center">#</th>
                                <th rowspan="2" class="text-center">Nama</th>
                                <th colspan="2" class="text-center">Jumlah</th>
                                @if (session('role') != 'supervisor')
                                    <th rowspan="2" class="text-center">Aksi</th>
                                @endif
                            </tr>
                            <tr class="bg-info">
                                <th class="text-center">Laki - laki</th>
                                <th class="text-center">Perempuan</th>
                            </tr>
                        </thead>
                        <tbody id="result_data">
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($anggota as $item)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ $item['nama'] }}</td>
                                    <td class="text-center">{{ $item['l'] != null ? $item['l'] : '0' }}</td>
                                    <td class="text-center">{{ $item['p'] != null ? $item['p'] : '0' }}</td>
                                    @if (session('role') != 'supervisor')
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="edit{{ $item['id'] }}"
                                                onclick="editEkskul({{ $item['id'] }})">
                                                <i class="material-icons list-icon md-18 text-info">edit</i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_anggota">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Ekstrakurikuler</label>
                                    <div class="col-sm-12">
                                        <select name="ekskul" id="ekskul" class="form-control">
                                            <option value="">--Pilih ekskul--</option>
                                            @foreach ($ekskul as $eks)
                                                <option value="{{ $eks['id'] }}">{{ $eks['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jumlah</label>
                                    <div class="row mx-0">
                                        <div class="col-sm-6">
                                            <input type="number" name="l" id="l" class="form-control">
                                            <small class="text-danger">*laki-laki</small>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="number" name="p" id="p" class="form-control">
                                            <small class="text-danger">*perempuan</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tahun_ajaran" id="tahun_ajaran"
                                            value="{{ session('tahun') }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#addData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kesiswaan_simpan-anggota_ekskul') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kesiswaan_update-anggota_ekskul') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#result_data').html(
                            '<tr><td colspan="5" class="text-center"><i class="fa fa-spin fa-spinner"></i>Sedang Mengupdate Data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#CustomerForm').trigger("reset");
                        }
                        $('#result_data').html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        })

        function editEkskul(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('kesiswaan_detail-anggota_ekskul') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $('.edit' + id).html('<i class="fa fa-spin fa-spinner text-info"></i>');
                },
                success: function(data) {
                    $('.edit' + id).html('<i class="material-icons list-icon md-18 text-info">edit</i>');
                    $('#modelHeading').html("Edit Data");
                    $('#saveBtn').val("edit-user");
                    $('#id_anggota').val(data.id);
                    $('#ekskul').val(data.id_ekstra).trigger('change');
                    $('#l').val(data.l);
                    $('#p').val(data.p);
                    $('#tahun_ajaran').val(data.tahun);
                    $('#ajaxModel').modal('show');
                    $('#action').val('Edit')
                }
            });
        }

        function deleteEkskul(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('kesiswaan_delete-anggota_ekskul') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $('#result_data').html(
                            '<tr><td colspan="5" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang Mengupdate Data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#result_data').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>

@endsection
