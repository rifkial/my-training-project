@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <h2 class="box-title mt-1">{{ session('title') }}</h2>
                <form action="javascript:void(0)" method="post" id="formSearchSiswa">
                    <div class="row mx-0 my-3">
                        <div class="col-lg-4 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Jurusan:</label>
                            <select id="id_jurusan" name="id_jurusan" class="form-control">
                                <option value="">--Pilih Jurusan--</option>
                                @foreach ($jurusan as $jr)
                                    <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">{{ $jr['nama'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Kelas:</label>
                            <select id="id_kelas" name="id_kelas" class="form-control" disabled>
                                <option value="">Pilih Kelas..</option>
                            </select>
                        </div>
                        <div class="col-lg-3 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Rombel:</label>
                            <select id="id_rombel" name="id_rombel" class="form-control" disabled>
                                <option value="">Pilih Rombel..</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">&nbsp;</label>
                            <input type="submit" value="Mulai Cari" class="btn btn-outline-info btn-block">
                        </div>
                    </div>
                </form>

                <div id="collapse21" class="card-collapse collapse show" role="tabpanel" aria-labelledby="heading4">
                    <div class="card-body">
                        <div style="width: 100%;">
                            <div id="wali_kelas">
                                <div class="alert alert-danger border-danger" role="alert">
                                    <p class="mb-1"><strong>Biodata Wali Kelas:</strong>
                                    </p>
                                    <p class="mb-1">Data wali kelas akan tampil bila sudah melakukan pencarian
                                        kelas</p>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-info">
                                            <th>&nbsp;</th>
                                            <th>Nama</th>
                                            <th>NIS</th>
                                            <th>NISN</th>
                                            <th>Kelas</th>
                                            <th>Jurusan</th>
                                            <th>Status</th>
                                            @if (session('role') != 'supervisor')
                                                <th class="text-center">Opsi</th>
                                            @endif
                                        </tr>
                                    </thead>

                                    <tbody id="data-siswa">
                                        <tr>
                                            <td colspan="7" class="text-center">Silahkan masukan filter diatas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item"><a class="nav-link active" href="#profile" data-toggle="tab"
                                        aria-expanded="false">Profil</a>
                                </li>
                                <li class="nav-item ayah" aria-expanded="false"><a class="nav-link"
                                        href="#profile-ayah" data-toggle="tab" aria-expanded="false">Ayah</a>
                                </li>
                                <li class="nav-item ibu"><a class="nav-link" href="#profile-ibu" data-toggle="tab"
                                        aria-expanded="false">Ibu</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#profile-wali" data-toggle="tab"
                                        aria-expanded="true">Wali</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" aria-expanded="false">
                                    <div class="row">
                                        <input type="hidden" name="id" id="id_siswa">
                                        <div class="col-md-6">
                                            <label for="kode" class="control-label">Status Orang Tua</label>
                                            <select name="status_ortu" id="status_ortu" class="form-control">
                                                <option value="normal">Normal</option>
                                                <option value="yatim">Yatim</option>
                                                <option value="piatu">Piatu</option>
                                                <option value="yatim_piatu">Yatim Piatu</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">NIK</label>
                                                <input type="text" name="nik" class="form-control" id="nik">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">NIS</label>
                                                <input type="text" name="nis" id="nis" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">NISN</label>
                                                <input type="text" name="nisn" class="form-control" id="nisn" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tahun Angkatan</label>
                                                <input type="text" name="tahun_angkatan" id="tahun_angkatan"
                                                    class="form-control" placeholder="2021" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Nama</label>
                                                <input type="text" name="nama" class="form-control" id="nama" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Email</label>
                                                <input type="email" name="email" class="form-control" id="email">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Jenis Kelamin</label>
                                                <select name="jenkel" id="jenkel" class="form-control">
                                                    <option value="l">Laki - laki</option>
                                                    <option value="p">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Agama</label>
                                                <select name="agama" id="agama" class="form-control">
                                                    <option value="islam">Islam</option>
                                                    <option value="kristen">Kristen</option>
                                                    <option value="katholik">Katholik</option>
                                                    <option value="hindu">Hindu</option>
                                                    <option value="budha">Budha</option>
                                                    <option value="lainnya">Lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Telepon</label>
                                                <input type="text" name="telepon" class="form-control" id="telepon">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tempat Lahir</label>
                                                <input type="text" name="tempat_lahir" class="form-control"
                                                    id="tempat_lahir" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tanggal Lahir</label>
                                                <input type="text" name="tgl_lahir" class="form-control datepicker"
                                                    id="tgl_lahir" value="{{ date('d-m-Y') }}" readonly
                                                    data-plugin-options='{"autoclose": true}'>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Alamat</label>
                                                <input type="text" name="alamat" class="form-control" id="alamat">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Lintang</label>
                                                <input type="text" name="lintang" class="form-control" id="lintang">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Bujur</label>
                                                <input type="text" name="bujur" class="form-control" id="bujur">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Anak ke</label>
                                                <input type="text" name="anak_ke" class="form-control" id="anak_ke">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Status Keluarga</label>
                                                <input type="text" name="status_keluarga" class="form-control"
                                                    id="status_keluarga">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tanggal Diterima</label>
                                                <input type="text" name="tgl_diterima" class="form-control datepicker"
                                                    id="tgl_diterima" value="{{ date('d-m-Y') }}" readonly
                                                    data-plugin-options='{"autoclose": true}' required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">No Ijazah</label>
                                                <input type="text" name="no_ijazah" class="form-control" id="no_ijazah">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tahun Ijazah</label>
                                                <input type="text" name="tahun_ijazah" class="form-control"
                                                    id="tahun_ijazah">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">No SKHUN</label>
                                                <input type="text" name="no_skhun" class="form-control" id="no_skhun">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Tahun SKHUN</label>
                                                <input type="text" name="th_skhun" class="form-control" id="th_skhun">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Kelas Diterima</label>
                                                <select name="kls_diterima" id="kls_diterima" class="form-control">
                                                    <option value="">Pilih Rombel..</option>
                                                    @foreach ($rombel as $rm)
                                                        <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Gambar</label>
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                        <div class="col-md-2">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group" width="100%" style="margin-top: 10px">

                                        </div>
                                        <div class="col-md-4">
                                            <div id="delete_foto"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-ayah" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Nama Ayah</label>
                                                <input type="text" name="nama_ayah" class="form-control" id="nama_ayah">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Pekerjaan Ayah</label>
                                                <input type="text" name="pekerjaan_ayah" class="form-control"
                                                    id="pekerjaan_ayah">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-ibu" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Nama Ibu</label>
                                                <input type="text" name="nama_ibu" class="form-control" id="nama_ibu">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Pekerjaan Ibu</label>
                                                <input type="text" name="pekerjaan_ibu" class="form-control"
                                                    id="pekerjaan_ibu">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-wali" aria-expanded="true">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Nama Wali</label>
                                                <input type="text" name="nama_wali" class="form-control" id="nama_wali">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Telepon Wali</label>
                                                <input type="text" name="telepon_wali" class="form-control"
                                                    id="telepon_wali">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Alamat Wali</label>
                                                <input type="text" name="alamat_wali" class="form-control"
                                                    id="alamat_wali">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="kode" class="control-label">Pekerjaan Wali</label>
                                                <input type="text" name="pekerjaan_wali" class="form-control"
                                                    id="pekerjaan_wali">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#status_ortu').on('change', function() {
                let ortu = this.value;
                if (ortu == 'yatim') {
                    $('.ayah').hide();
                    $('.ibu').show();
                } else if (ortu == 'piatu') {
                    $('.ibu').hide();
                    $('.ayah').show();
                } else if (ortu == 'yatim_piatu') {
                    $('.ibu').hide();
                    $('.ayah').hide();
                } else {
                    $('.ibu').show();
                    $('.ayah').show();
                }
            });

            $('select[name="id_jurusan"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('select[name="id_kelas"]').html(
                                '<option value="">Load Kelas..</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                            $("#id_rombel").attr("disabled", true);
                            $("#id_rombel").html('<option value="">Pilih Rombel..</option>');
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('#id_rombel').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Load Rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih Rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('point_siswa-get_data') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="8" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data.data_siswa);
                        $('#wali_kelas').html(data.data_walikelas);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-siswa') }}",
                    data: {
                        id_siswa: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-user-edit"></i>');
                        $('#modelHeading').html("Edit Data Siswa");
                        $('#id_siswa').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nik').val(data.nik);
                        $('#nis').val(data.nis);
                        $('#nisn').val(data.nisn);
                        $('#agama').val(data.agama).trigger("change");
                        $('#jenkel').val(data.gender).trigger("change");
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#status_ortu').val(data.status_ortu);
                        if (data.status_ortu == 'yatim') {
                            $('.ayah').hide();
                            $('.ibu').show();
                        } else if (data.status_ortu == 'piatu') {
                            $('.ibu').hide();
                            $('.ayah').show();
                        } else if (data.status_ortu == 'yatim_piatu') {
                            $('.ibu').hide();
                            $('.ayah').hide();
                        } else {
                            $('.ibu').show();
                            $('.ayah').show();
                        }
                        $('#tahun_angkatan').val(data.tahun_angkatan);
                        $('#alamat').val(data.alamat);
                        $('#old_image').val(data.old_image);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tanggal_lahir').val(data.tgl_lahir);
                        $('#lintang').val(data.lintang);
                        $('#bujur').val(data.bujur);
                        $('#anak_ke').val(data.anak_ke);
                        $('#status_keluarga').val(data.status_keluarga);
                        $('#tgl_diterima').val(data.tgl_diterima);
                        $('#kls_diterima').val(data.kls_diterima);
                        $('#no_ijazah').val(data.no_ijazah);
                        $('#tahun_ijazah').val(data.th_ijazah);
                        $('#no_skhun').val(data.no_skhun);
                        $('#th_skhun').val(data.th_skhun);
                        $('#lintang').val(data.lintang);
                        $('#bujur').val(data.bujur);
                        $('#nama_ayah').val(data.nama_ayah);
                        $('#pekerjaan_ayah').val(data.pekerjaan_ayah);
                        $('#nama_ibu').val(data.nama_ibu);
                        $('#pekerjaan_ibu').val(data.pekerjaan_ibu);
                        $('#nama_wali').val(data.nama_wali);
                        $('#alamat_wali').val(data.alamat_wali);
                        $('#telepon_wali').val(data.telp_wali);
                        $('#pekerjaan_wali').val(data.pekerjaan_wali);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('point_siswa-update_data_siswa') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                        }
                        $('#data-siswa').html(data.siswa.data_siswa);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        })
    </script>

@endsection
