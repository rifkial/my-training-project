@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="widget-bg">
                <div class="row mb-2">
                    <div class="col-md-6 col-12">
                        <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                    </div>
                    @if (session('role') != 'supervisor')
                        <div class="col-md-6 col-12">
                            <div class="pull-right">
                                <button id="addData" class="btn btn-outline-info mt-1"><i class="fas fa-plus-circle"></i>
                                    Tambah</button>
                                <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                                    Import</button>
                                <button id="data_trash" class="btn btn-outline-danger mt-1"><i
                                        class="fas fa-trash-restore"></i>
                                    Trash</button>
                            </div>
                        </div>
                    @endif
                </div>
                <hr>
                <div class="table-responsive">
                    <p></p>
                    <table class="table table-striped table-bordered table-hover" id="table-data">
                        <thead>
                            <tr class="bg-info">
                                <th class="text-center">#</th>
                                <th class="text-center">Judul</th>
                                <th class="text-center">Tahun Ajaran</th>
                                <th class="text-center">Program Kerja</th>
                                <th class="text-center">Pelaksanaan</th>
                                @if (session('role') != 'supervisor')
                                    <th class="text-center">Opsi</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody id="data_kerja">
                            @if (!empty($kerja))
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($kerja as $kj)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $kj['judul'] }}</td>
                                        <td class="text-center">{{ $kj['tahun_ajaran'] }}</td>
                                        <td class="text-center">
                                            @if ($kj['file'] != null)
                                                <a href="{{ route('kesiswaan_program_kerja-download', ['file', (new \App\Helpers\Help())->encode($kj['id'])]) }}" target="_blank"
                                                    class="btn btn-success btn-sm"><i class="fas fa-download"></i></a>
                                            @else
                                                <a href="javascript:void(0)" onclick="alert('Program Kerja tidak mempunyai file yang dapat diunduh')" class="btn btn-dark btn-sm"><i
                                                        class="fas fa-times-circle"></i></a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($kj['pelaksanaan'] != null)
                                                <a href="{{ route('kesiswaan_program_kerja-download', ['pelaksanaan', (new \App\Helpers\Help())->encode($kj['id'])]) }}" target="_blank"
                                                    class="btn btn-info btn-sm"><i class="fas fa-download"></i></a>
                                            @else
                                                <a href="javascript:void(0)" onclick="alert('Pelaksanaan tidak mempunyai file yang dapat diunduh')" class="btn btn-dark btn-sm"><i
                                                        class="fas fa-times-circle"></i></a>
                                            @endif
                                        </td>
                                        @if (session('role') != 'supervisor')
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-info edit" href="javascript:void(0)"
                                                    data-id="{{ $kj['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                                <a class="btn btn-sm btn-danger delete" href="javascript:void(0)"
                                                    data-id="{{ $kj['id'] }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formProgram" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kerja">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="id_tahun_ajar" id="id_tahun_ajar" class="form-control">
                                            <option value="">Pilih tahun ajaran...</option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['id'] }}"
                                                    {{ $th['id'] == session('id_tahun_ajar') ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama/Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="judul" id="judul" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">File Program kerja</label>
                                    <div class="col-sm-12">
                                        <input type="file" class="form-control" type="file" name="image" accept="*" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">File Laporan Pelaksanaan</label>
                                    <div class="col-sm-12">
                                        <input type="file" class="form-control" type="file" name="image1" accept="*" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
        integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // tinymce.init({
            //     selector: "textarea",
            //     height: "480",
            //     plugins: [
            //         "advlist autolink lists link image charmap print preview anchor",
            //         "searchreplace visualblocks code fullscreen",
            //         "insertdatetime media table contextmenu paste "
            //     ],
            //     toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter      alignright alignjustify | bullist numlist outdent indent | link image"
            // });

            $('#addData').click(function() {
                $('#formProgram').trigger("reset");
                $('#modelHeading').html("Tambah Data");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#formProgram', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kesiswaan_simpan-program_kerja') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kesiswaan_update-program_kerja') }}";
                }
                let formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#result_data').html(
                            '<tr><td colspan="5" class="text-center"><i class="fa fa-spin fa-spinner"></i>Sedang Mengupdate Data...</td></tr>'
                        );
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#formProgram').trigger("reset");
                            $('#data_kerja').html(data.kerja)
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kesiswaan_detail-program_kerja') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data");
                        $('#id_kerja').val(data.id);
                        $('#id_tahun_ajar').val(data.id_tahun_ajar).trigger('change');
                        $('#judul').val(data.judul);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('kesiswaan_soft_delete-program_kerja') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_kerja').html(data.kerja)
                            }
                            $(loader).html('<i class="fas fa-trash-alt"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })
    </script>

@endsection
