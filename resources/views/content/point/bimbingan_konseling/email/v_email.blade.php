@extends('content.point.bimbingan_konseling.email.v_main_mail')
@section('content_pesan')
    <div class="row pl-3 pr-3 pt-4">
        <h2 class="col-sm-6 col-md-7 mr-t-0 fw-300 h1 sub-heading-font-family head_title">Inbox</h2>
    </div>
    <div class="table-responsive" style="padding: 15px;">
        <table class="table table-striped" id="data-tabel" style="width: 100%">
            <thead>
                <tr>
                    <th><input type="checkbox" class='checkall' id='checkall'></th>
                    <th></th>
                    <th>Nama</th>
                    <th>Subject</th>
                    <th>Pesan</th>
                    <th>Waktu</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection
