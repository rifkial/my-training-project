<div class="m-3">
    <a href="javascript:void(0)" class="btn btn-lg btn-info btn-block text-uppercase createEmail">Compose
    </a>
</div>
<div class="inbox-categories">
    <ul class="list-unstyled">
        <li>
            <a href="{{ url('/program/point/pesan/inbox') }}"
                class="{{ Request::segment(4) === 'inbox' ? 'active' : '' }}">
                <i class="material-icons list-icon float-left mr-2">inbox</i>
                <span
                    class="badge fs-12 {{ $dibaca != 0 ? 'bg-danger' : 'bg-info' }} float-right">{{ $dibaca }}</span>
                Inbox
            </a>
        </li>
        <li>
            <a href="{{ url('/program/point/pesan/send') }}"
                class="{{ Request::segment(4) === 'send' ? 'active' : '' }}">
                <i class="material-icons list-icon float-left mr-2">send</i>
                Sent
                Mails
            </a>
        </li>
        <li role="separator" class="divider"></li>
        <li>
            <a href="{{ url('/program/point/pesan/trash') }}"
                class="{{ Request::segment(4) === 'trash' ? 'active' : '' }}">
                <i class="material-icons list-icon float-left mr-2">delete</i>
                Trash
            </a>
        </li>
    </ul>
</div>
<div class="modal fade" id="create_message" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="modelTitle"> <i class="fa fa-envelope"></i> Buat Pesan Baru</h5>
            </div>
            <div class="col-lg-12 col-md-8 col-12 mail-inbox mail-single">

                <div class="mail-single-content border-0 m-4 p-0">
                    <div class="mail-single-message mail-single-compose">
                        @if (session('role') == 'bk')
                            <form action="javascript:void(0)" id="formSearchSiswa" method="post">
                                <div class="row bg-info p-3 rounded">
                                    <div class="col-md-12">
                                        <div class="row p-3 rounded">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Jurusan</label>
                                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                                        <option value="">--Pilih Jurusan--</option>
                                                        @foreach ($jurusan as $jr)
                                                            {{-- <option value="{{ $jr['id'] }}">{{ $jr['nama'] }} --}}
                                                            <option
                                                                value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">
                                                                {{ $jr['nama'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Kelas</label>
                                                    <select name="id_kelas" id="id_kelas" class="form-control"
                                                        disabled>
                                                        <option value="">Pilih Kelas..</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Rombel</label>
                                                    <select name="id_rombel" id="id_rombel" class="form-control"
                                                        disabled>
                                                        <option value="">Pilih Rombel..</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Dikirim ke</label>
                                                    <select name="asal" id="asal" class="form-control">
                                                        <option value="siswa">Siswa</option>
                                                        <option value="ortu">Orang Tua</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <input type="submit"
                                                        class="btn btn-success btn-block border border-light"
                                                        value="Mulai Cari..">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                        <form name="form_send_message" id="form_send_message" class="form-horizontal">
                            <div class="row my-4">
                                <div class="col-md-12">
                                    @if (session('role') == 'bk')
                                        <div class="alert_info" style="display: none">
                                            <div class="alert alert-info border-info" role="alert">
                                                <button aria-label="Close" class="close" data-dismiss="alert"
                                                    type="button"><span aria-hidden="true">×</span>
                                                </button>
                                                <p><strong>Pemberitahuan:</strong>
                                                </p>
                                                <ul class="mr-t-10">
                                                    <li id="teks">Connections</li>
                                                    <li>Silahkan pilih Siswa dibawah ini yang ingin anda kirimkan pesan
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <table class="table table-condensed table-hover">
                                            <tr class="bg-info">
                                                <th>#</th>
                                                <th>NIS</th>
                                                <th>Nama</th>
                                                <th>Rombel</th>
                                                <th>Aksi</th>
                                            </tr>
                                            <tbody id="data-siswa">
                                                <tr>
                                                    <td colspan="5" class="text-center">Silahkan pilih rombel
                                                        terlebih dahulu</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <input type="hidden" name="asal" id="untuk">
                                    @else
                                        <div class="form-group">
                                            <label for="">Pilih Guru BK</label>
                                            <select name="id_kelas_siswa[]" id="id_kelas_siswa" class="form-control">
                                                <option value="">--Pilih Penerima--</option>
                                                @foreach ($email as $em)
                                                    <option value="{{ $em['id'] }}">{{ $em['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="hidden" name="asal" value="bk">
                                    @endif

                                    <div class="form-group">
                                        <input name="subject" type="text" class="form-control"
                                            placeholder="Subject Pesan">
                                    </div>
                                    <div class="form-group">
                                        <textarea id="default" name="pesan"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="image" accept="image/*">
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info text-left" id="saveBtn" value="create"><i
                            class="fa fa-envelope"></i> Send Message</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script> --}}
<script>
    // showBc(0);
    $(function() {
        $('select[name="asal"]').on('change', function() {
            var asal = $(this).val();
            if (asal) {
                $('#id_rombel_search').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('get-rombel_kelas') }}",
                    type: "POST",
                    data: {
                        id_kelas: id_kelas
                    },
                    beforeSend: function() {
                        $('#id_rombel_search').html(
                            '<option value="">--Load data Rombel--</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">--Pilih Rombel--</option>';
                        data = JSON.parse(data);
                        data.forEach(function(val) {
                            s += '<option value="' + val.id + '">' + val.nama +
                                '</option>';
                        })
                        $('#id_rombel_search').removeAttr('disabled')
                        $('#id_rombel_search').html(s);
                    }
                });
            }
        })

        $('select[name="id_jurusan"]').on('change', function() {
            var id = $(this).val();
            if (id) {
                $.ajax({
                    url: "{{ route('get_kelas-jurusan') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $('select[name="id_kelas"]').html(
                            '<option value="">Load data Kelas..</option>');
                    },
                    success: function(data) {
                        if (!$.trim(data)) {
                            $('select[name="id_kelas"]').html(
                                '<option value="">No Kelas Found..</option>');
                        } else {
                            var s = '<option value="">Pilih Kelas..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row
                                    .nama_romawi +
                                    '</option>';

                            })
                            $('select[name="id_kelas"]').removeAttr('disabled');
                        }
                        $('select[name="id_kelas"]').html(s)
                        $("#id_rombel").attr("disabled", true);
                        $("#id_rombel").html('<option value="">Pilih Rombel..</option>');
                    }
                });
            }
        })

        $('select[name="id_kelas"]').on('change', function() {
            var id_kelas = $(this).val();
            if (id_kelas) {
                $('select[name="id_rombel"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('get-rombel_kelas') }}",
                    type: "POST",
                    data: {
                        id_kelas: id_kelas
                    },
                    beforeSend: function() {
                        $('#id_rombel').html(
                            '<option value="">Load Rombel..</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">Pilih Rombel..</option>';
                        data = JSON.parse(data);
                        data.forEach(function(val) {
                            s += '<option value="' + val.id + '">' + val.nama +
                                '</option>';
                        })
                        $('#id_rombel').removeAttr('disabled')
                        $('#id_rombel').html(s);
                    }
                });
            }
        })

        $('#formSearchSiswa').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ route('point_siswa-get_data_pesan') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $('#data-siswa').html(
                        '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                    );
                },
                success: function(data) {
                    $('#data-siswa').html(data.siswa);
                    $('#teks').html(data.text);
                    $('#untuk').val(data.asal);
                    $(".alert_info").show();
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    })
</script>
