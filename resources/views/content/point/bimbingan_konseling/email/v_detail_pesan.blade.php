@extends('content.point.bimbingan_konseling.email.v_main_mail')
@section('content_pesan')
    <style>
        .panel-footer {
            padding: 6px;
        }

        .senden-img {
            width: 50px;
            height: 50px;
        }

        .img-circle {
            border-radius: 50%;
        }

        .panel-teal .panel-heading {
            background-color: #37BC9B;
            border: 1px solid #36b898;
            color: white;
        }

        .panel .panel-heading {
            padding: 5px;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom: 1px solid #DDD;
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
        }

        .panel .panel-heading .panel-title {
            padding: 10px;
            font-size: 17px;
        }

        form .form-group {
            position: relative;
            margin-left: 0px !important;
            margin-right: 0px !important;
        }

        .inner-all {
            padding: 10px;
        }

        .nav-email>li:first-child+li:active {
            margin-top: 0px;
        }

        .nav-email>li+li {
            margin-top: 1px;
        }

        .nav-email li {
            background-color: white;
        }

        .nav-email li.active {
            background-color: transparent;
        }

        .nav-email li.active .label {
            background-color: white;
            color: black;
        }

        .nav-email li a {
            color: black;
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
        }

        .nav-email li a:hover {
            background-color: #EEEEEE;
        }

        .nav-email li a i {
            margin-right: 5px;
        }

        .nav-email li a .label {
            margin-top: -1px;
        }

        .table-email tr:first-child td {
            border-top: none;
        }

        .table-email tr td {
            vertical-align: top !important;
        }

        .table-email tr td:first-child,
        .table-email tr td:nth-child(2) {
            text-align: center;
            width: 35px;
        }

        .table-email tr.unread,
        .table-email tr.selected {
            background-color: #EEEEEE;
        }

        .table-email .media {
            margin: 0px;
            padding: 0px;
            position: relative;
        }

        .table-email .media h4 {
            margin: 0px;
            font-size: 14px;
            line-height: normal;
        }

        .table-email .media-object {
            width: 35px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
        }

        .table-email .media-meta,
        .table-email .media-attach {
            font-size: 11px;
            color: #999;
            position: absolute;
            right: 10px;
        }

        .table-email .media-meta {
            top: 0px;
        }

        .table-email .media-attach {
            bottom: 0px;
        }

        .table-email .media-attach i {
            margin-right: 10px;
        }

        .table-email .media-attach i:last-child {
            margin-right: 0px;
        }

        .table-email .email-summary {
            margin: 0px 110px 0px 0px;
        }

        .table-email .email-summary strong {
            color: #333;
        }

        .table-email .email-summary span {
            line-height: 1;
        }

        .table-email .email-summary span.label {
            padding: 1px 5px 2px;
        }

        .table-email .ckbox {
            line-height: 0px;
            margin-left: 8px;
        }

        .table-email .star {
            margin-left: 6px;
        }

        .table-email .star.star-checked i {
            color: goldenrod;
        }

        .nav-email-subtitle {
            font-size: 15px;
            text-transform: uppercase;
            color: #333;
            margin-bottom: 15px;
            margin-top: 30px;
        }

        .compose-mail {
            position: relative;
            padding: 15px;
        }

        .compose-mail textarea {
            width: 100%;
            padding: 10px;
            border: 1px solid #DDD;
        }

        .view-mail {
            padding: 10px;
            font-weight: 300;
        }

        .attachment-mail {
            padding: 10px;
            width: 100%;
            display: inline-block;
            margin: 20px 0px;
            border-top: 1px solid #EFF2F7;
        }

        .attachment-mail p {
            margin-bottom: 0px;
        }

        .attachment-mail a {
            color: #32323A;
        }

        .attachment-mail ul {
            padding: 0px;
        }

        .attachment-mail ul li {
            float: left;
            width: 200px;
            margin-right: 15px;
            margin-top: 15px;
            list-style: none;
        }

        .attachment-mail ul li a.atch-thumb img {
            width: 200px;
            margin-bottom: 10px;
        }

        .attachment-mail ul li a.name span {
            float: right;
            color: #767676;
        }

        @media (max-width: 640px) {
            .compose-mail-wrapper .compose-mail {
                padding: 0px;
            }
        }

        @media (max-width: 360px) {
            .mail-wrapper .panel-sub-heading {
                text-align: center;
            }

            .mail-wrapper .panel-sub-heading .pull-left,
            .mail-wrapper .panel-sub-heading .pull-right {
                float: none !important;
                display: block;
            }

            .mail-wrapper .panel-sub-heading .pull-right {
                margin-top: 10px;
            }

            .mail-wrapper .panel-sub-heading img {
                display: block;
                margin-left: auto;
                margin-right: auto;
                margin-bottom: 10px;
            }

            .mail-wrapper .panel-footer {
                text-align: center;
            }

            .mail-wrapper .panel-footer .pull-right {
                float: none !important;
                margin-left: auto;
                margin-right: auto;
            }

            .mail-wrapper .attachment-mail ul {
                padding: 0px;
            }

            .mail-wrapper .attachment-mail ul li {
                width: 100%;
            }

            .mail-wrapper .attachment-mail ul li a.atch-thumb img {
                width: 100% !important;
            }

            .mail-wrapper .attachment-mail ul li .links {
                margin-bottom: 20px;
            }
        }

    </style>
    <form class="form-horizontal">
        <div class="panel mail-wrapper rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">View Mail</h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-sub-heading inner-all">
                <div class="pull-left">
                    <h3 class="lead no-margin">{{ ucwords($pesan['subject']) }}</h3>
                </div>
                <div class="pull-right">
                    <a class="btn btn-info btn-sm tooltips" data-container="body" data-original-title="Print" href="#"
                        data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-print"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger btn-sm"
                        onclick="deletePesan({{ $pesan['id'] }})"><i class="fa fa-trash-o"></i></a>
                    @if ($pesan['data_dari'] != 'send')
                        <a href="javascript:void(0)" onclick="balas('{{ $pesan['pengirim'] }}')"
                            class="btn btn-success btn-sm"><i class="fa fa-reply"></i> Reply</a>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div><!-- /.panel-sub-heading -->
            <div class="panel-sub-heading inner-all">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-7">
                        <img src="{{ $pesan['user_profil'] }}" alt="..." class="img-circle senden-img">
                        @if ($pesan['data_dari'] == 'send')
                            <span>saya</span>
                            to
                            <strong>{{ $pesan['penerima'] }}</strong>
                        @else
                            <span>{{ $pesan['pengirim'] }}</span>
                            to
                            <strong>me</strong>
                        @endif

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-5">
                        <p class="pull-right"> {{ $pesan['jam'] . ' ' . $pesan['tanggal'] }}</p>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="view-mail">
                    {{ strip_tags($pesan['pesan']) }}
                </div>
                @if ($pesan['file'] != null)
                    <div class="attachment-mail">
                        <p>
                            <span><i class="fa fa-paperclip"></i> attachments — </span>
                            <a href="#">View all images</a>
                        </p>
                        <ul>
                            <li>
                                <a class="atch-thumb" href="#" data-toggle="modal"
                                    data-target=".bs-example-modal-photo1">
                                    <img src="{{ $pesan['file'] }}" alt="...">
                                </a>

                                <div class="links">
                                    <a href="#" data-toggle="modal" data-target=".bs-example-modal-photo1">View</a> -
                                    <a href="#">Download</a>
                                </div>

                            </li>
                        </ul>
                    </div>
                @else
                    <div class="attachment-mail">
                        <p>
                            <span><i class="fa fa-paperclip"></i> Empty Attachments</span>
                        </p>
                        <ul>
                            <li style="width: 100% !important">
                                <a class="atch-thumb" href="#" data-toggle="modal"
                                    data-target=".bs-example-modal-photo1">
                                    <p>Pesan tidak disertai file attachment</p>
                                </a>

                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </form>
    <div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title"> <i class="fa fa-reply"></i> Balas Pesan</h5>
                </div>
                <div class="col-lg-12 col-md-8 col-12 mail-inbox mail-single">
                    <form id="formReply" action="javascript:void(0)" class="form-horizontal">
                        <div class="mail-single-content border-0 m-4 p-0">
                            <div class="mail-single-message mail-single-compose">
                                <div class="row my-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="sosial_penerima"
                                                value="{{ $pesan['sosial_pengirim'] }}">
                                            <input type="hidden" name="sosial_pengirim"
                                                value="{{ $pesan['sosial_penerima'] }}">
                                            <input type="hidden" name="role_pengirim"
                                                value="{{ $pesan['role_penerima'] }}">
                                            <input type="hidden" name="role_penerima"
                                                value="{{ $pesan['role_pengirim'] }}">
                                            <label for="">Penerima</label>
                                            <input type="text" id="penerima" disabled value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input name="subject" type="text" class="form-control"
                                                placeholder="Subject Pesan">
                                        </div>
                                        <div class="form-group">
                                            <textarea id="pesan" name="pesan"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="file" name="image" accept="image/*">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info text-left" id="btnReply"><i
                                    class="fa fa-envelope"></i>
                                Send Message</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // tinymce.get(".view-mail").getContent({
            //     format: "text"
            // });

            tinymce.init({
                selector: 'textarea#pesan',
                height: 300,
                menubar: false,
                forced_root_block: "",
                force_br_newlines: true,
                force_p_newlines: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'removeformat | help',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });



            $('body').on('submit', '#formReply', function(e) {
                e.preventDefault();
                $("#btnReply").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnReply").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('point_pesan_email-reply') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formReply').trigger("reset");
                            $('#replyModal').modal('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnReply').html('Send Message');
                        $("#btnReply").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
        })

        function deletePesan(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                // console.log(data);
                $.ajax({
                    url: "{{ route('point_pesan_email-delete_pesan') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            window.location.href = '/program/point/pesan/inbox'
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }



        function balas(params) {
            $('#penerima').val(params);
            $('#replyModal').modal('show');
        }
    </script>
@endsection
