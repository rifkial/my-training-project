@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-6 col-12">
                <div class="pull-right">
                    <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                        Import</button>
                    <button id="btn-trash" class="btn btn-outline-danger mt-1"><i class="fas fa-trash-restore"></i>
                        Trash</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Keterangan</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                    </thead>
                    <tbody id="data_jenis">
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($jenis as $jn)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $jn['nama'] }}</td>
                                <td>{{ $jn['kategori'] }}</td>
                                <td>{{ $jn['keterangan'] }}</td>
                                <td class="text-center">
                                    <button data-toggle="collapse" data-target="#demo{{ $jn['id'] }}"
                                        class="btn btn-sm btn-success accordion-toggle"><i
                                            class="fas fa-info-circle"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="hiddenRow">
                                    <div class="accordian-body collapse" id="demo{{ $jn['id'] }}">
                                        <button class="btn btn-info btn-sm my-3 pull-right addBeasiswa"
                                            data-id="{{ $jn['id'] }}"><i class="fas fa-plus-circle"></i>
                                            Tambah Beasiswa</button>
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-info">
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Nama Siswa</th>
                                                    <th class="text-center">NISN</th>
                                                    <th class="text-center">Rombel</th>
                                                    <th class="text-center">Tahun Ajaran</th>
                                                    <th class="text-center">Sumber</th>
                                                    <th class="text-center">Nominal</th>
                                                    <th class="text-center">Opsi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="dataBeasiswa{{ $jn['id'] }}">
                                                @if (!empty($jn['beasiswa']))
                                                    @php
                                                        $nomer = 1;
                                                    @endphp
                                                    @foreach ($jn['beasiswa'] as $bs)
                                                        <tr>
                                                            <td class="text-center">{{ $nomer++ }}</td>
                                                            <td class="text-center">{{ $bs['nama_siswa'] }}</td>
                                                            <td class="text-center">{{ $bs['nisn'] }}</td>
                                                            <td class="text-center">{{ $bs['rombel'] }}</td>
                                                            <td class="text-center">{{ $bs['tahun_ajaran'] }}</td>
                                                            <td class="text-center">{{ $bs['sumber'] }}</td>
                                                            <td class="text-center">{{ $bs['nominal'] }}</td>
                                                            <td class="text-center">
                                                                <a href="javascript:void(0)" class="info"
                                                                    data-id="{{ $bs['id'] }}">
                                                                    <i
                                                                        class="material-icons list-icon md-18 text-success">info</i>
                                                                </a>
                                                                <a href="javascript:void(0)" class="edit"
                                                                    data-id="{{ $bs['id'] }}">
                                                                    <i
                                                                        class="material-icons list-icon md-18 text-info">edit</i>
                                                                </a>
                                                                <a href="javascript:void(0)" class="delete"
                                                                    data-id="{{ $bs['id'] }}"
                                                                    data-jenis="{{ $jn['id'] }}">
                                                                    <i
                                                                        class="material-icons list-icon md-18 text-danger">delete</i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="8" class="text-center">Beasiswa saat ini belum
                                                            tersedia</td>
                                                    </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalBeasiswa" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="formSearchSiswa" method="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Jurusan</label>
                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                        <option value="">Pilih Jurusan..</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">{{ $jr['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Kelas</label>
                                    <select name="id_kelas" id="id_kelas" disabled class="form-control">
                                        <option value="">Pilih Kelas..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Pilih Rombel</label>
                                    <select name="id_rombel" id="id_rombel" disabled class="form-control">
                                        <option value="">Pilih Rombel..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">&nbsp;</label>
                                    <input type="submit" class="btn btn-success form-control" value="Proses cari">
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="formBeasiswa" method="post">
                        <div class="table-responsive">
                            <p></p>
                            <input type="hidden" name="id_jenis_beasiswa" id="id_jenis_beasiswa">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>NISN</th>
                                        <th>Rombel</th>
                                        <th>Jurusan</th>
                                        <th style="width: 150px" class="text-center">
                                            <input class="form-check-input ml-0" type="checkbox" id="checkAll">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Pilih Semua
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="data-siswa">
                                    <tr>
                                        <td colspan="7" class="text-center">Harap pilih rombel dahulu</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEdit" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit Beasiswa</h5>
                </div>
                <form id="formEdit" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="edit_id_beasiswa">
                        <input type="hidden" name="id_siswa" id="edit_id_siswa">
                        <input type="hidden" name="id_rombel" id="edit_id_rombel">
                        <input type="hidden" name="id_jenis_beasiswa" id="edit_id_jenis">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="tahun_ajaran" id="edit_tahun_ajaran" class="form-control">
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['tahun_ajaran'] }}">
                                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Sumber</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="edit_sumber" name="sumber"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nominal</label>
                                    <div class="col-sm-12">
                                        <input type="number" class="form-control" id="edit_nominal" name="nominal"
                                            autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="edit_keterangan" class="form-control"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnEdit">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Beasiswa</h5>
                </div>
                <form action="{{ route('point_beasiswa-import') }}" method="POST" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" class="text-info p-1 rounded"> <i
                                                class="material-icons text-info">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" style="color:#03a9f3">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title"><i class="fas fa-trash-restore"></i> Data Trash</h5>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="height: 400px; overflow-y: auto;">
                        <p></p>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Siswa</th>
                                    <th>NISN</th>
                                    <th>Jenis Beasiswa</th>
                                    <th>Sumber</th>
                                    <th>Nominal</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_trash">
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark btn-rounded ripple text-left" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const actualBtn = document.getElementById('actual-btn');
            const fileChosen = document.getElementById('file-chosen');
            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            });

            $(document).on('click', '#btn-trash', function() {
                $.ajax({
                    type: 'GET',
                    url: "{{ route('point_beasiswa-data_trash') }}",
                    beforeSend: function() {
                        $('#data_trash').html(
                            '<tr><td colspan="5" class="text-center">sedang memproses data trash...</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data_trash').html(data);
                        $('#modalTrash').modal('show');
                    }
                });
            });

            $(document).on('click', '.addBeasiswa', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#modelHeading').html("Tambah Beasiswa");
                $('#modalBeasiswa').modal('show');
                $('#data-siswa').html(
                    '<tr><td colspan="7" class="text-center">Harap pilih rombel dahulu</td></tr>');
                $('#id_jenis_beasiswa').val(id);
                $('#action').val('Add');
            });

            $('select[name="id_jurusan"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Load Kelas...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">--- No Kelas Found ---</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                            $("#id_rombel").attr("disabled", true);
                            $("#id_rombel").html('<option value="">Pilih Rombel..</option>');
                        }
                    });
                }
            })

            $('select[name="id_kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">Load Rombel..</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">Pilih Rombel..</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#formSearchSiswa').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('point_beasiswa-data_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#data-siswa').html(
                            '<tr><td colspan="7" class="text-center"><i class="fa fa-spin fa-spinner"></i> Sedang memproses data</td></tr>'
                        );
                    },
                    success: function(data) {
                        $('#data-siswa').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formBeasiswa').on('submit', function(event) {
                event.preventDefault();
                let id_jenis = $('#id_jenis_beasiswa').val();
                $('#btnSimpanBeasiswa').html('Proses Menyimpan...');
                $("#btnSimpanBeasiswa").attr("disabled", true);
                $.ajax({
                    url: "{{ route('point_beasiswa-simpan') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == "berhasil") {
                            $('#modalBeasiswa').modal('hide');
                            $('#formBeasiswa').trigger("reset");
                            $('#formSearchSiswa').trigger("reset");
                        }
                        $('#dataBeasiswa' + id_jenis).html(data.html);
                        noti(data.icon, data.message);
                        $('#btnSimpanBeasiswa').html('Simpan');
                        $("#btnSimpanBeasiswa").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                let id_jenis = $('#edit_id_jenis').val();
                $('#btnEdit').html('Mengedit...');
                $("#btnEdit").attr("disabled", true);
                $.ajax({
                    url: "{{ route('point_beasiswa-update') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == "berhasil") {
                            $('#modalEdit').modal('hide');
                            $('#formEdit').trigger("reset");
                        }
                        $('#dataBeasiswa' + id_jenis).html(data.html);
                        noti(data.icon, data.message);
                        $('#btnEdit').html('Simpan');
                        $("#btnEdit").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('point_beasiswa-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="material-icons list-icon md-18 text-info">edit</i>');
                        $('#edit_id_beasiswa').val(data.id);
                        $('#edit_id_siswa').val(data.id_siswa);
                        $('#edit_id_rombel').val(data.id_rombel);
                        $('#edit_id_jenis').val(data.id_jenis_beasiswa);
                        $('#edit_tahun_ajaran').val(data.tahun_ajaran).trigger('change');
                        $('#edit_sumber').val(data.sumber);
                        $('#edit_nominal').val(data.nominal);
                        $('#edit_keterangan').val(data.keterangan);
                        $('#modalEdit').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let id_jenis = $(this).data('jenis');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_beasiswa-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#dataBeasiswa' + id_jenis).html(data.html);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '#import', function() {
                $('#importModal').modal('show');
            });

            $(document).on('click', '.restore', function() {
                let id = $(this).data('id');
                let loader = $(this);
                let konfirmasi = confirm("Apa kamu yakin ingin mengembalikan data ini?");
                if (konfirmasi == true) {
                    $.ajax({
                        url: "{{ route('point_beasiswa-restore') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_trash').html(data.trash);
                            $('#data_jenis').html(data.jenis);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                } else {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            });

            $(document).on('click', '.hardDelete ', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('point_beasiswa-hard_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            $('#data_trash').html(data.trash);
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>


@endsection
