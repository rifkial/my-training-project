@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .alert.alert-warning {
            text-align: center;
        }

        @media (max-width: 960px) {
            #cari {
                margin-top: 0px !important;
            }
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row" id="step-first">
                    <div class="col-md-12">
                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #9768a0;">
                                <div class="card-header" role="tab" id="heading4" style="background: #9768a0;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            Cari Siswa
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <form action="javascript:void(0)">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="kode" class="control-label">Pilih Kelas</label>
                                                        <select name="rombel" id="rombel" class="form-control">
                                                            <option value="">Pilih Rombel</option>
                                                            @foreach ($rombel as $rb)
                                                                <option value="{{ $rb['id'] }}">{{ $rb['nama'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="kode" class="control-label">NISN</label>
                                                        <select name="nisn" id="nisn" disabled="disabled"
                                                            class="form-control">
                                                            <option value="0">Pilih NISN</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block" id="cari"
                                                            onclick="return view_siswa()"
                                                            style="margin-top: 30px; background-color: #9768a0; border-color: #9768a0; color: #fff"><i
                                                                class="fa fa-search"></i> Cari siswa...
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <form name="next_input" action="javascript:void(0)" id="next_input">
                                            @csrf
                                            <div class="row">
                                                <div id="load_siswa" style="width: 100%">

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row next_proses" style="display: none">
                    <div class="col-md-12">
                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #605ca8;">
                                <div class="card-header" role="tab" id="heading4" style="background: #605ca8;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            Siswa yang melanggar
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <form name="next_step" action="javascript:void(0)" id="next_step">
                                            @csrf
                                            <div id="load_siswa_input">

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 widget-holder">
                                                    <div class="widget-bg">
                                                        <div class="widget-body clearfix">
                                                            <h5 class="box-title">Pilih Pelanggaran yang dilakukan oleh
                                                                siswa tersebut</h5>
                                                            <div class="tabs tabs-bordered">
                                                                <ul class="nav nav-tabs">
                                                                    @php
                                                                        $no = 1;
                                                                        $jml = count($kategori);
                                                                    @endphp
                                                                    @foreach ($kategori as $kt)
                                                                        <li class="nav-item"><a class="nav-link"
                                                                                href="#home-tab-bordered-{{ $no }}"
                                                                                data-toggle="tab" aria-expanded="true"
                                                                                onclick="return load_kategori({{ $kt['id'] . ',' . $no }})">{{ $kt['nama'] }}</a>
                                                                        </li>
                                                                        @php
                                                                            $no++;
                                                                        @endphp
                                                                    @endforeach
                                                                </ul>
                                                                <div class="tab-content">
                                                                    @for ($i = 1; $i <= $jml; $i++)
                                                                        <div class="tab-pane show_kategori"
                                                                            id="home-tab-bordered-{{ $i }}">
                                                                            <small class="text-warning">*Silahkan pilih
                                                                                kategori terlebih dahulu</small>
                                                                        </div>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id_rmbl = 0;
        var nisn = 0
        view_siswa();

        function view_siswa() {
            id_rmbl = $('#rombel').val();
            nisn = $('#nisn').val();
            if (id_rmbl == 0 && nisn == 0) {
                $("#load_siswa").html('<div class="alert alert-warning">Silakan pilih Rombel diatas terlebih dahulu</div>');
            } else {
                $("#load_siswa").html("Loading...");
                $.ajax({
                    type: "POST",
                    url: "{{ route('kesiswaan_pelanggaran_siswa-data_siswa') }}",
                    data: {
                        id_rombel: id_rmbl,
                        nisn: nisn
                    },
                    beforeSend: function() {
                        $("#tbSimpanKd").attr("disabled", true);
                    },
                    success: function(data) {
                        $("#load_siswa").show('slow');
                        html =
                            '<table class="table table-condensed table-bordered table-hover" style="width:100%"><thead><tr><th width="10%">No</th><th width="30%">Nama</th><th width="20%">NISN</th><th width="20%">Rombel</th><th width="20%">Opsi</th></tr></thead><tbody>';
                        var i = 1;
                        $.each(data, function(k, v) {
                            if (!$.trim(v)) {
                                html += '<tr><td colspan="5">Mohon maaf data kosong</td></tr>';

                            } else {
                                html += '<tr><td>' + i + '</td><td>' + v.nama + '</td><td>' + v.nisn +
                                    '</td><td>' + v.rombel +
                                    '</td><td><input name="id_siswa[]" type="hidden" value="' + v.id +
                                    '"><input type="checkbox" name="id_kelas_siswa[]" value="' + v.id +
                                    '"> <label for="vehicle1"> Pilih Siswa</label></td></tr>';
                                i++;
                            }

                        });
                        html +=
                            '</tbody></table><p><button type="submit" class="btn btn-success" id="tbSimpan"><i class="fa fa-check"></i> Lanjutkan >></button></p>';
                        $("#load_siswa").html(html);
                    }
                });

            }
            return false;
        }

        $('select[name="rombel"]').on('change', function() {
            var id_rombel = $(this).val();
            if (id_rombel) {
                $.ajax({
                    url: "{{ route('rombel-kelas_siswa') }}",
                    type: "POST",
                    data: {
                        id_rombel
                    },
                    beforeSend: function() {
                        $('select[name="nisn"]').html(
                            '<option value="">Memproses Siswa...</option>');
                    },
                    success: function(data) {
                        if (!$.trim(data)) {
                            $('select[name="nisn"]').html(
                                '<option value="">Siswa tidak ditemukan</option>');
                            $('select[name="nisn"]').attr("disabled", true);
                        } else {
                            var s = '<option value="">Tampilkan satu rombel</option>';
                            // data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.nisn + '">' + row.nisn +
                                    ' - ' + row.nama + '</option>';

                            })
                            $('select[name="nisn"]').removeAttr('disabled');
                        }
                        $('select[name="nisn"]').html(s)
                    }
                });
            }
        })

        $("#next_input").on("submit", function() {
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "{{ route('kesiswaan_pelanggaran_siswa-next_input') }}",
                beforeSend: function() {
                    $("#tbSimpan").attr("disabled", true);
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#step-first').css("display", "none");
                        $('.next_proses').css("display", "block");

                        html =
                            '<table class="table table-condensed table-bordered table-hover" style="width:100%"><thead><tr><th width="15%"></th><th width="30%">NISN</th><th width="30%">Nama</th><th width="25%">Rombel</th></tr></thead><tbody>';
                        var i = 1;
                        $.each(data.data, function(k, v) {
                            if (!$.trim(v)) {
                                html += '<tr><td colspan="5">Mohon maaf data kosong</td></tr>';

                            } else {
                                html +=
                                    '<tr><td><input type="checkbox" name="id_kelas_siswa[]" value="' +
                                    v
                                    .id +
                                    '" checked></td><td>' + v.nisn +
                                    '</td><td>' + v.nama +
                                    '</td><td>' + v.rombel +
                                    '</td></tr>';
                                i++;
                            }

                        });
                        $("#load_siswa_input").html(html);
                    } else {
                        swa("Maaf!", data.data, "error");
                        $("#tbSimpan").attr("disabled", false);
                    }

                }
            });
            return false;
        });

        $("#next_step").on("submit", function() {
            console.log('finish');
            var data = $(this).serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: "{{ route('kesiswaan_pelanggaran_siswa-finish_step') }}",
                beforeSend: function() {
                    $("#tbSave").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#tbSave").attr("disabled", true);
                },
                success: function(data) {
                    // console.log(data);
                    $("#tbSave").attr("disabled", false);
                    $("#tbSave").html(
                        '<i class="fa fa-check"></i> Simpan');
                    if (data.status == 'berhasil') {
                        swa("Berhasil!", data.message, data.icon);
                    } else {
                        swa("Gagal!", data.message, data.icon);
                    }
                }
            });
            return false;
        });

        function load_kategori(id, no) {
            $.ajax({
                type: "POST",
                url: "{{ route('kesiswaan_pelanggaran_siswa-get_pelanggaran_by_kategori') }}",
                data: {
                    id_kategori: id,
                },
                beforeSend: function() {
                    $("#home-tab-bordered-" + no).html('loading...');
                },
                success: function(data) {
                    $("#home-tab-bordered-" + no).html('');
                    $("#home-tab-bordered-" + no).show('slow');
                    html =
                        '<table class="table table-condensed table-bordered table-hover" style="width:100%"><thead><tr><th width="60%">Bentuk Pelanggaran</th><th width="20%">Bobot</th><th width="20%">Opsi</th></tr></thead><tbody>';
                    var i = 1;
                    $.each(data.pelanggaran, function(k, v) {
                        if (!$.trim(v)) {
                            html += '<tr><td colspan="5">Mohon maaf data kosong</td></tr>';

                        } else {
                            html += '<tr><td>' + v.nama + '</td><td>' + v.point +
                                '</td><td><input type="checkbox" name="id_pelanggaran[]" value="' + v
                                .id +
                                '"> <label for="vehicle1"> Melanggar</label></td></tr>';
                            i++;
                        }
                    });
                    html +=
                        '</tbody></table><p><button type="submit" class="btn btn-success" id="tbSave"><i class="fa fa-check"></i> Simpan</button></p>';
                    $("#home-tab-bordered-" + no).html(html);
                }
            });
        }
    </script>


@endsection
