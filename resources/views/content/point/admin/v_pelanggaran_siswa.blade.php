@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
            margin-right: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        .col-lg-3,
        .col-lg-2 {
            padding-right: 0;
            padding-left: 4px;
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <hr>
            <div class="card" style="margin-bottom: 12px;">
                <div class="card-header">
                    <h2 class="box-title">{{ Session::get('title') }}</h2>
                </div>
                <form action="javascript:void(0)" id="search-form">
                    <div class="card-body" style="padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Siswa" type="text">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <select name="rombel" id="rombel" class="form-control">
                                        <option value="">Pilih Kelas</option>
                                        @foreach ($rombel as $rb)
                                            <option value="{{ $rb['id'] }}">{{ $rb['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="l37">&nbsp;&nbsp;&nbsp;</label>
                                    <button class="btn btn-info" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Siswa</th>
                                <th>NISN</th>
                                <th>Kelas</th>
                                <th>Point</th>
                                <th>Jumlah Pelanggaran</th>
                                <th>Telepon Orang Tua</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Detail Pelanggaran Siswa</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-detail" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Kelas</th>
                                    <th>Tanggal Pelanggaran</th>
                                    <th>Pelanggaran</th>
                                    <th>Point</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            fill_datatable();
            fill_detail(0);

            function fill_datatable(filter_name = '', filter_rombel = '') {
                var table = $('#data-tabel').DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-clipboard"></i>',
                            exportOptions: {
                                columns: [0, ':visible']
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },

                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            text: '<i class="fa fa-undo"></i>',
                            action: function(e, dt, button, config) {
                                window.location = 'rombel-trash';
                            },
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: {
                        url: 'pelanggaran_siswa/get_datatable',
                        data: {
                            nama: filter_name,
                            rombel: filter_rombel
                        }
                    },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'total_point',
                            name: 'total_point'
                        },
                        {
                            data: 'jumlah_pelanggaran',
                            name: 'jumlah_pelanggaran'
                        },
                        {
                            data: 'telp_ortu',
                            name: 'telp_ortu'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });
            }



            $('#search-form').on('submit', function(e) {
                var filter_name = $('#nama').val();
                var filter_rombel = $('#rombel').val();

                if (filter_name != '' && filter_rombel != '') {
                    $('#data-tabel').DataTable().destroy();
                    fill_datatable(filter_name, filter_rombel);
                } else {
                    alert('Select Both filter option');
                }
            });

        });

        function fill_detail(kelas_siswa = '') {
            var table = $('#data-detail').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: 'pelanggaran_siswa/get_detail',
                    data: {
                        kelas_siswa
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nis',
                        name: 'nis'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'tgl_pelanggaran',
                        name: 'tgl_pelanggaran'
                    },
                    {
                        data: 'pelanggaran',
                        name: 'pelanggaran'
                    },
                    {
                        data: 'point',
                        name: 'point'
                    },
                ]
            });
        }

        function detail(id) {
            if (id != '') {
                $('#data-detail').DataTable().destroy();
                fill_detail(id);
                $('#modalDetail').modal('show');
            }
        }

    </script>
@endsection
