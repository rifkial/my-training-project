@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .alert.alert-warning {
            text-align: center;
        }

        @media (max-width: 960px) {
            #cari {
                margin-top: 0px !important;
            }
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row" id="step-first">
                    <div class="col-md-4">
                        <div class="accordion" id="accordion-2" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #605ca8;">
                                <div class="card-header" role="tab" id="heading4" style="background: #605ca8;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-2" href="#collapse2"
                                            aria-expanded="true" aria-controls="collapse2">
                                            Wali Murid dari siswa
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse2" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img class="img-circle" src="{{ $siswa['file'] }}" alt="User Avatar">
                                            </div>
                                            <div class="col-md-8">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item">{{ ucwords($siswa['nama']) }}</li>
                                                    <li class="list-group-item">{{ $siswa['rombel'] }}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">Total Pelanggaran {{ $siswa['jml_pelanggaran'] }}</li>
                                            <li class="list-group-item">Poin Pelanggaran {{ $siswa['total_point'] }}</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="accordion" id="accordion-3" role="tablist" aria-multiselectable="true">
                            <div class="card card-outline-danger" style="border-color: #605ca8;">
                                <div class="card-header" role="tab" id="heading4" style="background: #605ca8;">
                                    <h5 class="m-0">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-3"
                                            href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                            Pelanggaran {{ ucwords($siswa['nama']) }}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse21" class="card-collapse collapse show" role="tabpanel"
                                    aria-labelledby="heading4">
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            {{-- <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Title</th>
                                                    <th>Progress</th>
                                                    <th>Deadline</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead> --}}
                                            <tbody>
                                                @foreach ($pelanggaran as $pel)
                                                    <tr>
                                                        <td>{{ $pel['pelanggaran'] }}</td>
                                                        <td>{{ (new \App\Helpers\Help)->getTanggal($pel['tgl_pelanggaran']) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
