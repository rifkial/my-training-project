@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-12  widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="chart-container">
                        <div class="bar-chart-container">
                            <canvas id="pie-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12  widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="chart-container">
                        <div class="pie-chart-container">
                            <canvas id="bar-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <script>
        $(function() {
            var cData = JSON.parse(`<?php echo $list_master ?>`);
            var ctx = $("#pie-chart");
            var data = {
                labels: cData.label,
                datasets: [{
                    label: "Users Count",
                    data: cData.data,
                    backgroundColor: [
                        "#DEB887",
                        "#A9A9A9",
                        "#DC143C",
                        "#F4A460",
                        "#2E8B57",
                    ],
                    borderColor: [
                        "#CDA776",
                        "#989898",
                        "#CB252B",
                        "#E39371",
                        "#1D7A46",
                    ],
                    borderWidth: [1, 1, 1, 1, 1, 1, 1]
                }]
            };

            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "DATA MASTER",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                }
            };
            var chart1 = new Chart(ctx, {
                type: "pie",
                data: data,
                options: options
            });
        });

        $(function() {
            var cData = JSON.parse(`<?php echo $list_user ?>`);
            var ctx = $("#bar-chart");
            var data = {
                labels: cData.label,
                datasets: [{
                    label: "Jumlah Data",
                    data: cData.data,
                    backgroundColor: [
                        "#51d2b7",
                        "#03a9f3",
                        "#e6614f",
                        "#6f42c1",
                        "#38d57a",
                    ],
                    borderColor: [
                        "#CDA776",
                        "#989898",
                        "#CB252B",
                        "#E39371",
                        "#1D7A46",
                    ],
                    borderWidth: [1, 1, 1, 1, 1, 1, 1]
                }]
            };

            var options = {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: "DATA USER",
                    fontSize: 18,
                    fontColor: "#111"
                },

            };
            var chart1 = new Chart(ctx, {
                type: "bar",
                data: data,
                options: options
            });
        });
    </script>
@endsection
