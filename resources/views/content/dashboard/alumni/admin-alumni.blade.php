@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .canvasjs-chart-credit {
            display: none !important;
        }

    </style>
    <div class="row">
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-info">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Total Keseluruhan</h4>
                        <div class="media d-flex">
                            <div class="align-self-center">
                                <i class="fas fa-users fa-6x float-left"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="text-white">{{ $top['total_alumni'] }}</h3>
                                <span>Jumlah Alumni</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-success">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Berdasarkan Gender</h4>
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="success text-white">{{ $top['gender']['l'] }}</h3>
                                <i class="fas fa-male"></i><span> Pria</span>
                            </div>
                            <div class="media-body text-center">
                                <h3 class="success text-white">{{ $top['gender']['p'] }}</h3>
                                <i class="fas fa-female"></i><span> Wanita</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-primary">
                        <h4 class="text-center mt-0 text-white font-weight-bold">Tracking</h4>
                        <div class="media d-flex">
                            <div class="align-self-center">
                                <i class="fas fa-poll fa-6x float-left"></i>
                            </div>
                            <div class="media-body text-right">
                                <h3 class="text-white">{{ $top['survei'] }}</h3>
                                <span>Jumlah Survey</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body bg-purple">
                        <h4 class="text-center mt-0 font-weight-bold text-white">Tracking</h4>
                        <div class="media d-flex">

                            <div class="media-body">
                                <h3 class="text-white">{{ $top['partisipant'] }}</h3>
                                <span class="text-white">Partisipasi Survey</span>
                            </div>
                            <div class="align-self-center">
                                <h1 class="font-weight-bold text-white">{{ $top['partisipant_percent'] }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    {{-- <h5 class="box-title text-center">Pekerjaan</h5> --}}
                    <center>
                        <div class="padded-reverse" id="chart" style="height: 370px; width: 100%;">
                            {{-- {!! $alumniChart->container() !!} --}}
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Angkatan</h5>
                    <div class="padded-reverse">
                        <table class="table table-striped widget-status-table mr-b-0">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Angkatan</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tahun_lulus as $tl)
                                    <tr>
                                        <th class="pd-l-20"><a href="#">{{ $tl['lulusan'] }}</a>
                                        </th>
                                        <td>{{ $tl['total'] }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Jurusan</h5>
                    <div class="padded-reverse">
                        <table class="table table-striped widget-status-table mr-b-0">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Jurusan</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jurusan as $jr)
                                    <tr>
                                        <th class="pd-l-20"><a href="#">{{ $jr['nama'] }}</a>
                                        </th>
                                        <td>{{ $jr['total'] }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Survey</h5>
                    <hr>
                    <div class="padded-reverse">
                        <table class="table table-striped table-bordered mr-b-0">
                            <thead>
                                <tr class="bg-info">
                                    <th class="pd-l-20">Nama Survey</th>
                                    <th>Partisipan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($survey as $sr)
                                    <tr>
                                        <th class="pd-l-20 text-info">{{ $sr['nama'] }}
                                        </th>
                                        <td>{{ $sr['partisipant'] }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Aktifitas Alumni</h5>
                    <hr>
                    <div class="padded-reverse">
                        <table class="table table-striped table-bordered mr-b-0">
                            <thead>
                                <tr class="bg-info">
                                    <th class="pd-l-20">Aktifitas</th>
                                    <th>Jumlah Kegiatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stats_data as $sd => $val)
                                    <tr>
                                        <th class="pd-l-20 text-info">{{ $sd }}</th>
                                        <td>{{ $val }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>


    <script>
        var dataPoints = [];
        var chart = new CanvasJS.Chart("chart", {
            animationEnabled: true,
            exportEnabled: true,
            title: {
                text: "Pekerjaan Alumni"
            },
            // subtitles: [{
            //     text: "Currency Used: Thai Baht (฿)"
            // }],
            data: [{
                type: "pie",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - #percent%",
                yValueFormatString: "##0",
                dataPoints: dataPoints
            }]
        });
        chart.render();

        $.getJSON("{{ route('admin_alumni-statistic') }}", addData);

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y
                });
            }
            chart.render();
        }

        // })
    </script>
@endsection
