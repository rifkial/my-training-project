@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        video {
            width: 100% !important;
        }

    </style>
    <div class="row">
        <div class="col-xl-12 col-xxl-12">
            <div class="row">
                <div class="col-md-12 my-3">
                    @if ($informasi['status'] == true)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <h2 class="box-title m-0 text-white">Status Absen Hari ini
                                            {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h2>
                                    </div>
                                    <div class="card-body">
                                        <table class="table widget-status-table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Masuk</th>
                                                    <th class="text-center">Keluar</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="data_hadir">
                                                @if (!empty($detail))
                                                    <tr data-toggle="collapse" data-target="#detail_hadir"
                                                        style="cursor: pointer">
                                                        <td class="text-center">
                                                            <b>{{ $detail['jam_masuk'] != null ? (new \App\Helpers\Help())->getTime($detail['jam_masuk']) : '-' }}</b>
                                                        </td>
                                                        <td class="text-center">
                                                            <b>{{ $detail['jam_keluar'] != null ? (new \App\Helpers\Help())->getTime($detail['jam_keluar']) : '-' }}</b>
                                                        </td>
                                                        <td class="text-center"><span
                                                                class="badge badge-{{ $detail['status_kehadiran'] == 'hadir' ? 'success' : 'danger' }} text-inverse">{{ $detail['status_kehadiran'] }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="hiddenRow">
                                                            <div class="accordian-body collapse" id="detail_hadir">
                                                                <table class="table table-bordered">
                                                                    <tr>
                                                                        <th class="vertical-middle">Tanggal</th>
                                                                        <td class="text-center">
                                                                            {{ (new \App\Helpers\Help())->getTanggal($detail['tgl_kehadiran']) }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Bukti Masuk</th>
                                                                        <td class="text-center"> <img
                                                                                src="{{ $detail['bukti_masuk'] }}" alt=""
                                                                                height="150"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Bukti Keluar</th>
                                                                        <td class="text-center"> <img
                                                                                src="{{ $detail['bukti_keluar'] }}" alt=""
                                                                                height="150"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="vertical-middle">Keterangan</th>
                                                                        <td class="text-center">
                                                                            <p>{{ $detail['keterangan'] }}</p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="3" class="text-center">Belum ada Absensi untuk hari
                                                            ini
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <img src="{{ asset('images/weekend.png') }}" alt="">
                                </center>
                            </div>
                        </div>

                    @endif

                </div>
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header bg-info">
                            <h2 class="box-title mt-1 text-white text-center"><strong><i class="fas fa-user-clock"></i>
                                    Absensi</strong></h2>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        @if ($informasi['status'] == true)
                                                            <div class="col-md-7">
                                                                <div id="preview" class="w-100"></div>
                                                            </div>
                                                            <div class="col-md-5 d-flex align-items-center">
                                                                <button class="btn btn-info" onclick="take_snapshot()"><i
                                                                        class="fas fa-camera fa-3x"></i> <br>Take a
                                                                    Pitcure</button>
                                                            </div>
                                                        @else
                                                            <div class="col-md-12">

                                                                <img src="{{ asset('images/camera.jpg') }}" alt="">
                                                            </div>
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12 my-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div id="map" style="width: 100%; height:250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id="date-and-clock" class="alert-success p-3 my-2">
                                                <center>
                                                    <i class="fas fa-user-clock fa-5x"></i>
                                                    <h5 id="time" class="box-title mr-b-0"></h5>
                                                    <p id="datenow">
                                                        {{ (new \App\Helpers\Help())->getDay(date('Y-m-d')) }}</h3>
                                                    </p>
                                                </center>

                                            </div>
                                            <div class="alert alert-{{ $informasi['status'] == false ? 'danger' : 'info' }} border-{{ $informasi['status'] == false ? 'danger' : 'info' }} my-2 text-center"
                                                role="alert">
                                                <p class="m-0"><strong>{{ $informasi['message'] }}</strong>
                                                </p>
                                            </div>
                                            <form id="formAbsensi">
                                                <div class="row">
                                                    @if ($informasi['status'] == true)
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="l30">Bukti Gambar</label>
                                                                <div id="results" class="d-flex justify-content-center">
                                                                    <img src="{{ asset('images/profile.png') }}" alt=""
                                                                        width="200">
                                                                </div>
                                                                <input type="hidden" name="image" id="image">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="l32">Longtitude</label>
                                                                        <div class="form-input-icon"><i
                                                                                class="material-icons list-icon">place</i>
                                                                            <input class="form-control"
                                                                                placeholder="Longtitude" name="long"
                                                                                type="text" id="long" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="l32">Lattitude</label>
                                                                        <div class="form-input-icon"><i
                                                                                class="material-icons list-icon">place</i>
                                                                            <input class="form-control"
                                                                                placeholder="Latitude" type="text"
                                                                                name="lat" id="lat" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-actions btn-list">
                                                                        <button class="btn btn-info" type="submit"
                                                                            id="btnAbsensi">Absen</button>
                                                                        <button class="btn btn-outline-default"
                                                                            type="button">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="col-lg-12 d-flex justify-content-center">
                                                            <div class="closed text-center text-danger">
                                                                <i class="fas fa-times-circle fa-7x"></i>
                                                                <p>Saat ini absen tidak tersedia</p>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/webcamjs@1.0.26/webcam.min.js" defer></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            const secondHand = document.querySelector('.second-hand');
            const minsHand = document.querySelector('.min-hand');
            const hourHand = document.querySelector('.hour-hand');





            var map = L.map('map', {
                zoomControl: false
            }).fitWorld();
            var lokasi = "{{ $lokasi['token'] }}";
            if (lokasi) {
                L.tileLayer(
                    'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=' +
                    lokasi, {
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                        maxZoom: 18
                    }).addTo(map);
            } else {
                alert('anda belum set configurasi maps di settingan admin');
            }

            map.locate({
                setView: true,
                watch: true,
                maxZoom: 17
            });
            var marker;
            var MIcon = L.icon({
                iconSize: [25, 41],
                iconAnchor: [10, 41],
                popupAnchor: [2, -40],
                iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
                shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
            });

            map.on('locationfound', function(ev) {
                // console.log(ev);
                if (!marker) {
                    $('#lat').val(ev.latitude);
                    $('#long').val(ev.longitude);
                    marker = L.marker([ev.latitude, ev.longitude, 12], {
                        icon: MIcon
                    }).bindPopup('Lokasi Anda').addTo(map);
                } else {
                    marker.setLatLng(ev.latlng);
                }
            }).on("locationerror", error => {
                if (marker) {
                    map.removeLayer(marker);
                    marker = undefined;
                }
            });


            $('body').on('submit', '#formAbsensi', function(e) {
                e.preventDefault();
                $("#btnAbsensi").html(
                    '<i class="fa fa-spin fa-spinner"></i> Memproses..');
                $("#btnAbsensi").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    url: "{{ route('absensi-hadir_create') }}",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#gambar_bukti').attr('src',
                                '{{ asset('images/profile.png') }}');
                            $('#data_hadir').html(data.hadir);
                        }
                        noti(data.icon, data.message);
                        $('#btnAbsensi').html('Absen');
                        $("#btnAbsensi").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });
            let informasi = "{{ $informasi['status'] }}";
            if (informasi == true) {
                Webcam.set({
                    width: 320,
                    height: 240,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach('#preview');

            }

        })

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function() {
                startTime()
            }, 500);
        }
        startTime();

        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap(function(data_uri) {
                console.log(data_uri);
                // display results in page
                document.getElementById('results').innerHTML =
                    '<img src="' + data_uri + '"/>';
                $('#image').val(data_uri);
            });
        }
    </script>
@endsection
