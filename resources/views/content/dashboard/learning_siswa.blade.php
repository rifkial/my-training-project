@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <style>
        .left {
            display: none !important;
        }

        .welcome-app{
                width: 90%;
                height: 180px;
                border-radius: 10px;
                background: rgb(95,158,160);
                background: linear-gradient(90deg, rgba(95,158,160,1) 1%, rgba(111,208,205,1) 63%, rgba(171,246,243,1) 100%);
                margin: 0 20px;
                padding: 20px;
                color: white;
                font-size: 22px;
            }

        .main-card{
                width: 8rem; 
                height: 8rem; 
                border-radius: 10px;
                background: rgb(254,231,115);
                background: linear-gradient(332deg, rgba(254,231,115,1) 0%, rgba(250,222,84,1) 49%, rgba(250,215,44,1) 100%);
            }

        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            h5 {
                font-size: 21px !important;
                font-weight: 600 !important;
                padding: 4px;
                margin-top: 10px !important;
            }

            .left {
                display: none !important;
            }
        }

        .dataTables_wrapper .dataTables_paginate {
            margin-top: 1.14286em;
            padding: 0;
            border: 0.0625rem solid #eee;
        }

        .col-md-6 {
            margin-top: 12px;
        }

        .dataTables_wrapper {
            width: 100%;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: #51d2b7;
            border: 0;
            border-radius: 0;
            color: #fff !important;
        }

        .dataTables_wrapper table.no-footer {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

        .table{
            width:100%;
        }

    </style>
    @if (session('media_template') == 'mobile')
        <style>
            .welcome-app{
                width: 90%;
                height: 250px;
                border-radius: 10px;
                background: rgb(95,158,160);
                background: linear-gradient(90deg, rgba(95,158,160,1) 1%, rgba(111,208,205,1) 63%, rgba(171,246,243,1) 100%);
                margin: 0 20px;
                padding: 20px;
                color: white;
                font-size: 22px;
            }

            .welcome-app h5 {
                font-size: 38px !important;
                font-weight: 600 !important;
                padding: 4px 0 0 20px;
                margin-top: 10px !important;
            }

            .welcome-app h4{
                font-size: 28px !important;
                font-weight: 600 !important;
                padding: 4px 0 0 20px;
                margin-top: 10px !important;
            }

            .card-today{
                padding-left:50px;
            }

            .card-body h5{
                font-size: 18px;
                font-weight: 600;
            }

            h5.box-title {
                margin-top: 12px;
                padding: 4px;
                font-size: 19px;
            }

            .padded-reverse{
                border: #a8abab 1px solid;
                border-radius: 5px;
            }

            .table{
                width:100% !important;
                border: #bec3cc 1px solid;
                border-radius: 5px;
            }

            .main-card{
                width: 10rem; 
                height: 12rem; 
                border-radius: 10px;
                background: rgb(254,231,115);
                background: linear-gradient(332deg, rgba(254,231,115,1) 0%, rgba(250,222,84,1) 49%, rgba(250,215,44,1) 100%);
            }

            @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
                .welcome-app{
                    height: 150px;
                    /* background: white; */
                }

                .welcome-app h5 {
                    font-size: 18px !important;
                    font-weight: 600 !important;
                    padding: 4px;
                    margin-top: 10px !important;
                }

                .welcome-app h4 {
                    font-size: 15px !important;
                    font-weight: 600 !important;
                    padding: 4px;
                    margin-top: 10px !important;
                }

                .card-today{
                    padding-left:0px;
                }
            }

            @media only screen and (max-device-width : 800px) {
                .card-today{
                    padding-left:0px;
                }
            }

        </style>
    @endif

    @if (session('media_template') == 'mobile')
    <div class="row justify-content-center mb-3">
        <div class="welcome-app mx-1 mt-1 mt-md-3">
                <h5 class="ml-1 text-white">Selamat Datang</h5>
                <h5 class="ml-1 text-white">{{ Str::upper($profile['nama']) }}</h5>
                <h4 class="ml-1 text-white">NISN : {{ $profile['nisn'] }}</h4>
        </div>
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-11 col-sm-11 col-md-11 col-lg-12 widget-holder">
            <div class="widget-bg bg-transparent">
                <div class="widget-body clearfix">
                    <h5 class="box-title text-left text-md-center">Jadwal Hari Ini</h5>
                    <!-- mobile -->
                    @if (session('media_template') == 'mobile')
                        <div class="card-today d-flex flex-row flex-nowrap overflow-auto justify-content-start">
                            @foreach($kelas as $kls)
                                
                                    <div class="main-card {{ session('template') == 'mobile' ? 'col-3' : 'col-5 col-md-2' }} mx-2" style="">
                                        <!-- <a href=""> -->
                                        <div class="card-body" style="padding: 5px;">
                                            <h5 class="card-title mt-2 mb-0 pl-1" style="color: #47494e;">{{$kls['mapel']}}</h5>
                                            <p class="card-text mb-0 pl-1">Bpk/Ibu {{$kls['guru']}}</p>
                                            <p class="card-text pl-1">{{$kls['jam']}}</p>
                                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                        </div>
                                        <!-- </a> -->
                                    </div>
                            @endforeach
                        </div>
                    
                    <!-- end mobile -->
                    @else
                    <!-- desktop -->
                    <div class="padded-reverse">
                        <table class="table table-striped widget-status-table mr-b-0 hover bg-white" id="tabel-jadwal-hari-ini">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Jam</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Guru</th>
                                    <th class="pd-r-20">Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- end desktop -->
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row {{ session('media_template') != 'mobile' ? 'flex-row' : 'flex-column' }} align-items-center justify-content-center mt-3 mb-5">
        <div class="{{ session('media_template') != 'mobile' ? 'col-7' : 'col-11' }} rounded widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Jadwal Kelas</h5>
                    <div class="padded-reverse">
                        <table class="table table-striped  widget-status-table mr-b-0 hover" id="tabel-jadwal-kelas">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">Hari</th>
                                    <th>Jam</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Guru</th>
                                    <th class="pd-r-20">Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="{{ session('media_template') != 'mobile' ? 'col-5' : 'col-11 mt-3' }} widget-holder ">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Daftar Siswa</h5>
                    <div class="padded-reverse">
                        <table class="table table-striped widget-status-table mr-b-0 hover" id="data-rombel">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">#</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th class="pd-r-20">Jenis kelamin</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var templ = "{{ Session::get('media_template') }}";
        if (templ != "mobile") {
            setTimeout(function() {
                const mobileTemplate = confirm(
                    "Apa anda ingin beralih menggunakan template khusus mobile?");
                var template = '';
                if (mobileTemplate == true) {
                    $.ajax({
                        type: 'post',
                        url: "{{ route('learning-session_template') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "template": "mobile"
                        },
                        success: function(data) {
                            swa(data.status + "!", data.message, data.success);
                            location.reload(true);
                        },
                        error: function() {
                            console.log(data);
                        }
                    });
                    template = "mobile";
                }

            }, 3000);
        }


        const rombelId = "{{ session('id_rombel') }}";


        const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: false,
        };

        const konfigButtonExIm = {
            buttons: [{
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                },
                'colvis',
            ],
        };

        const konfigDOMTabelFull = {
            dom: `<"pd-l-20 float-left"B><"pd-r-20 float-right"f>rt<"row pd-l-20 pd-r-20"<"col-sm-4"l><"col-sm-8"p>>`,
        };

        $('#tabel-jadwal-hari-ini').DataTable({
            ...konfigUmum,
            paging: false,
            searching: false,
            dom: `rt`,
            ajax: {
                "url": "{{ route('dashboard-get_jadwal_hari_ini') }}",
                "method": "POST",
                "data": function(data) {
                    data.id_rombel = rombelId;
                },
            },
            columns: [{
                    data: 'jam',
                    name: 'jam'
                },
                {
                    data: 'mapel',
                    name: 'mapel'
                },
                {
                    data: 'guru',
                    name: 'guru'
                },
                {
                    data: 'ruang',
                    name: 'ruang'
                }
            ],
        });

        

        $('#tabel-jadwal-kelas').DataTable({
            ...konfigUmum,
            paging: false,
            searching: false,
            dom: `rt<"row pd-l-20 pd-r-20"<"col-sm-4"p>>`,
            ajax: {
                "url": "{{ route('dashboard-get_jadwal') }}",
                "method": "POST",
                "data": function(data) {
                    data.id_rombel = rombelId;
                },
            },
            columns: [{
                    data: 'hari',
                    name: 'hari'
                },
                {
                    data: 'jam',
                    name: 'jam'
                },
                {
                    data: 'mapel',
                    name: 'mapel'
                },
                {
                    data: 'guru',
                    name: 'guru'
                },
                {
                    data: 'ruang',
                    name: 'ruang'
                }
            ],
        });

        $('#data-rombel').DataTable({
            ...konfigUmum,
            paging: false,
            searching: false,
            dom: `rt`,
            ajax: {
                "url": "{{ route('dashboard-get_rombel') }}",
                "method": "POST",
                "data": function(data) {
                    data.id_rombel = rombelId;
                },
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nisn',
                    name: 'nisn'
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'jenkel',
                    name: 'jenkel'
                },
            ]
        });
    </script>
@endsection
