@extends('template/template_horizontal_nav_icons/app')
@section('content')
    <style>
        #sample-donut svg {
            left: -85.984375px !important;
        }

        .panel-body .btn:not(.btn-block) {
            width: 120px;
            margin-bottom: 10px;
        }

        .card-box {
            position: relative;
            color: #fff;
            padding: 20px 10px 40px;
            margin: 20px 0px;
        }

        .card-box:hover {
            text-decoration: none;
            color: #f1f1f1;
        }

        .card-box:hover .icon i {
            font-size: 100px;
            transition: 1s;
            -webkit-transition: 1s;
        }

        .card-box .inner {
            padding: 5px 10px 0 10px;
        }

        .card-box h3 {
            font-size: 27px;
            font-weight: bold;
            margin: 0 0 8px 0;
            white-space: nowrap;
            padding: 0;
            text-align: left;
        }

        .card-box p {
            font-size: 15px;
        }

        .card-box .icon {
            position: absolute;
            top: auto;
            bottom: 5px;
            right: 5px;
            z-index: 0;
            font-size: 72px;
            color: rgba(0, 0, 0, 0.15);
        }

        .card-box .card-box-footer {
            position: absolute;
            left: 0px;
            bottom: 0px;
            text-align: center;
            padding: 3px 0;
            color: rgba(255, 255, 255, 0.8);
            background: rgba(0, 0, 0, 0.1);
            width: 100%;
            text-decoration: none;
        }

        .card-box:hover .card-box-footer {
            background: rgba(0, 0, 0, 0.3);
        }

        .bg-blue {
            background-color: #00c0ef !important;
        }

        .bg-green {
            background-color: #00a65a !important;
        }

        .bg-orange {
            background-color: #f39c12 !important;
        }

        .bg-red {
            background-color: #d9534f !important;
        }

        /* i.icon-2x {
                                font-size: 30px;
                            }

                            .color-light {
                                color: #FFFFFF;
                            }
                            .servive-block {
                                padding: 20px 30px;
                                text-align: center;
                                margin-bottom: 20px;
                            }

                            .servive-block p,
                            .servive-block h2 {
                                color: #fff;
                            }

                            .servive-block h2 a:hover {
                                text-decoration: none;
                            }

                            .servive-block-light,
                            .servive-block-default {
                                background: #fafafa;
                                border: solid 1px #eee;
                            }

                            .servive-block-default:hover {
                                box-shadow: 0 0 8px #eee;
                            }

                            .servive-block-light p,
                            .servive-block-light h2,
                            .servive-block-default p,
                            .servive-block-default h2 {
                                color: #555;
                            }

                            .servive-block-u {
                                background: #72c02c;
                            }

                            .servive-block-blue {
                                background: #3498db;
                            }

                            .servive-block-red {
                                background: #e74c3c;
                            }

                            .servive-block-sea {
                                background: #1abc9c;
                            }

                            .servive-block-grey {
                                background: #95a5a6;
                            }

                            .servive-block-yellow {
                                background: #f1c40f;
                            }

                            .servive-block-orange {
                                background: #e67e22;
                            }

                            .servive-block-green {
                                background: #2ecc71;
                            }

                            .servive-block-purple {
                                background: #9b6bcc;
                            }

                            .servive-block-aqua {
                                background: #27d7e7;
                            }

                            .servive-block-brown {
                                background: #9c8061;
                            }

                            .servive-block-dark-blue {
                                background: #4765a0;
                            }

                            .servive-block-light-green {
                                background: #79d5b3;
                            }

                            .servive-block-dark {
                                background: #555;
                            }

                            .servive-block-light {
                                background: #ecf0f1;
                            } */

    </style>
    <div class="row">
        <div class="col-md-12">
            <h5 class="box-title mr-b-0">Selamat Datang </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="card-box bg-blue">
                <div class="inner">
                    <h3> 13436 </h3>
                    <p> Student Strength </p>
                </div>
                <div class="icon">
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                </div>
                <a href="#" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="card-box bg-green">
                <div class="inner">
                    <h3> ₹185358 </h3>
                    <p> Today’s Collection </p>
                </div>
                <div class="icon">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <a href="#" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="card-box bg-orange">
                <div class="inner">
                    <h3> 5464 </h3>
                    <p> New Admissions </p>
                </div>
                <div class="icon">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                </div>
                <a href="#" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="card-box bg-red">
                <div class="inner">
                    <h3> 723 </h3>
                    <p> Faculty Strength </p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="#" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <canvas id="myChart" width="400" height="400"></canvas>
    </div>

    <div class="panel-body mt-5">
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <a href="#" class="btn btn-danger btn-lg" role="button"><span class="fa fa-list"></span>
                    <br />Apps</a>
                <a href="#" class="btn btn-warning btn-lg" role="button"><span class="fa fa-bookmark"></span>
                    <br />Bookmarks</a>
                <a href="#" class="btn btn-primary btn-lg" role="button"><span class="fa fa-signal"></span>
                    <br />Reports</a>
                <a href="#" class="btn btn-primary btn-lg" role="button"><span class="fa fa-comment"></span>
                    <br />Comments</a>
            </div>
            <div class="col-xs-6 col-md-6">
                <a href="#" class="btn btn-success btn-lg" role="button"><span class="fa fa-user"></span>
                    <br />Users</a>
                <a href="#" class="btn btn-info btn-lg" role="button"><span class="fa fa-file"></span>
                    <br />Notes</a>
                <a href="#" class="btn btn-primary btn-lg" role="button"><span class="fa fa-picture-o"></span>
                    <br />Photos</a>
                <a href="#" class="btn btn-primary btn-lg" role="button"><span class="fa fa-tag"></span>
                    <br />Tags</a>
            </div>
        </div>
        <a href="http://www.jquery2dotnet.com/" class="btn btn-success btn-lg btn-block" role="button"><span
                class="fa fa-globe"></span> Website</a>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [8, 9, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endsection
