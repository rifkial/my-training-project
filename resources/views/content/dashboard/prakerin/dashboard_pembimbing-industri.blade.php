@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-top: 1.14286em;
            padding: 0;
            border: 0.0625rem solid #eee;
        }

        .col-md-6 {
            margin-top: 12px;
        }

        .dataTables_wrapper {
            width: 100%;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: #51d2b7;
            border: 0;
            border-radius: 0;
            color: #fff !important;
        }

        .dataTables_wrapper table.no-footer {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

    </style>

    <div class="row">
        <div class="col-sm-6 col-md-6 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Grafik Absensi</h5>
                    <div>
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">Data Perusahaan</h5>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-stripedmr-b-0 hover" id="tabel-dashboard">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Perusahaan</th>
                                        <th>Alamat</th>
                                        <th>Nama Pemimpin</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: "{{ route('sistem_pkl-dashboard_guru') }}",
            success: function(resp) {
                var ctx = document.getElementById('myChart').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: resp.label,
                        datasets: [{
                            label: 'Jumlah Absensi Siswa',
                            backgroundColor: 'rgb(255, 99, 132)',
                            borderColor: 'rgb(255, 99, 132)',
                            data: resp.value
                        }]
                    },
                    options: {}
                });
            },
            error: function() {
                console.log(resp);
            }
        });

        $('#tabel-dashboard').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                "url": "{{ route('pkl_industri-beranda') }}",
                "method": "GET",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'alamat',
                    name: 'alamat'
                },
                {
                    data: 'pimpinan',
                    name: 'pimpinan'
                },
            ]
        });
    </script>
@endsection
