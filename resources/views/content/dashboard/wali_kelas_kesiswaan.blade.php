@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        h6.media-heading.mr-b-5.text-uppercase.text-info {
            color: #fff !important;
        }

        span.user-type.fs-12.roles.text-info {
            color: #fff !important;
        }

        .pagination>li {
            display: inline;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
            float: right;
        }

        .pagination>li>a,
        .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

    </style>

    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title">REKAPAN ABSENSI</h5>
                        </div>
                        <div class="col-md-6">
                            <form id="formSearchSiswa">
                                <div class="form-row pull-right">
                                    <div class="col-auto">
                                        <label class="sr-only" for="inlineFormInput">Mata Pelajaran</label>
                                        <input type="hidden" name="id_rombel" value="{{ session('id_rombel') }}">
                                        <select name="id_mapel" class="form-control" id="id_mapel">
                                            <option value="">Pilih Pelajaran..</option>
                                            @foreach ($mapel as $mp)
                                                <option value="{{ $mp['id'] }}">{{ $mp['mapel'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-info mb-2" id="btnCari">Proses
                                            pencarian</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover widget-status-table">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Jurusan</th>
                                    <th>Kelas</th>
                                    <th>Rombel</th>
                                    <th>Absensi</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>

                            <tbody id="data-siswa">
                                <tr>
                                    <td colspan="8" class="text-center">Silahkan masukan filter
                                        diatas</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">REKAPAN PELANGGARAN</h5>
                    <div class="row mb-2">
                        <div class="col-sm-9 col-md-9"></div>
                        <div class="col-sm-3 col-md-3">
                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    @php
                                        $serc = str_replace('-', ' ', $search);
                                    @endphp
                                    <input type="text" value="{{ $serc }}" id="search" name="search"
                                        class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                            class="btn btn-success"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover widget-status-table">
                                    <thead>
                                        <tr class="bg-success">
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Tanggal</th>
                                            <th rowspan="2">Nama Siswa</th>
                                            <th rowspan="2">NIS</th>
                                            <th rowspan="2">Rombel</th>
                                            <th class="text-center" colspan="2">Pelanggaran</th>
                                            <th class="text-center" rowspan="2">Wali Kelas</th>
                                            @if (session('role') == 'admin-kesiswaan')
                                                <th rowspan="2">Aksi</th>
                                            @endif
                                        </tr>
                                        <tr class="bg-success">
                                            <th class="text-center">Nama Pelanggaran</th>
                                            <th class="text-center">Point</th>
                                        </tr>
                                    </thead>

                                    @if (!empty($pelanggaran))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($pelanggaran as $pl)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ (new \App\Helpers\Help())->getTanggal($pl['tgl_pelanggaran']) }}
                                                </td>
                                                <td>{{ ucwords($pl['nama_siswa']) }}</td>
                                                <td>{{ $pl['nis'] }}</td>
                                                <td>{{ $pl['rombel'] }}</td>
                                                <td>{{ $pl['pelanggaran'] }}</td>
                                                <td>{{ $pl['point'] }}</td>
                                                <td>{{ $pl['wali_kelas'] }}</td>
                                                @if (session('role') == 'admin-kesiswaan')
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" class="delete"
                                                            data-id="{{ $pl['id'] }}"><i
                                                                class="material-icons list-icon md-18 text-danger">delete</i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" class="text-center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                            {!! $pagination !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetail" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">DETAIL SISWA</h5>
                </div>
                <div style="width: 100%;">
                    <div class="team-single">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="font-size38 sm-font-size32 xs-font-size30 text-capitalize text-info"
                                    id="dt_nama_atas">
                                    Buckle Giarza</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 xs-margin-30px-bottom"
                                style="display: flex; align-items: center; justify-content: center;">
                                <div class="team-single-img my-2">
                                    <img id="gambar_siswa" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt=""
                                        width="200">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="team-single-text padding-50px-left sm-no-padding-left">

                                    <div class="bg-info p-1">
                                        <h5 class="font-size24 text-white m-1 sm-font-size22 xs-font-size20">Detail
                                            Informasi</h5>
                                    </div>
                                    <div class="contact-info-section margin-40px-tb border border-info p-2">
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Nama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIK:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nik">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIS:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nis">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NISN:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nisn">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Agama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_agama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Alamat.:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_alamat">4 Year in Education</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Email:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_email">Design Category</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Jenis Kelamin:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_jenkel">Regina ST, London, SK.</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong
                                                    class="margin-10px-left xs-margin-four-left text-info">Telephone:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_telepon">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Tempat,
                                                    Tanggal Lahir:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="ttl"><a href="javascript:void(0)">addyour@emailhere</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 mt-3">
                                <div class="bg-info p-1">
                                    <h5 class="font-size24 text-white m-1 sm-font-size22 xs-font-size20">Informasi
                                        Lainnya
                                    </h5>
                                </div>
                                <div class="border border-info p-3 mb-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Kelas Diterima:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_kelas_diterima">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Tahun Angkatan:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_tahun_angkatan">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Nomor Ijazah:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_no_ijazah">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Tahun Ijazah:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_tahun_ijazah">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Nomor SKHUN:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_nomor_skhun">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Tahun SKHUN:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_tahun_skhun">Master's Degrees</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Anak ke:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_anak_ke">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Status
                                                        Keluarga:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_status_keluarga">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Nama Wali:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_nama_wali">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Alamat Wali:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_alamat_wali">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Pekerjaan Wali:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_pekerjaan_wali">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Telepon Wali:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_telepon_wali">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Nama Ayah:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_nama_ayah">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Pekerjaan Ayah:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_pekerjaan_ayah">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Nama Ibu:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_nama_ibu">Master's Degrees</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 col-5">
                                                    <strong class="margin-10px-left text-info">Pekerjaan Ibu:</strong>
                                                </div>
                                                <div class="col-md-7 col-7">
                                                    <p id="dt_pekerjaan_ibu">Master's Degrees</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formSearchSiswa').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                $.ajax({
                    url: "{{ route('kesiswaan_siswa-get_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $("#data-siswa").html(
                            '<tr><td colspan="8" class="text-center">Sedang memproses data...</td></tr>'
                        );
                        $("#btnCari").html(
                            'Sedang mencari...'
                        );
                    },
                    success: function(data) {
                        $("#btnCari").html('Mulai Cari');
                        if (data.status) {
                            noti(data.icon, data.message);
                            $('#data-siswa').html(
                                '<tr><td colspan="4" class="text-center">Filter yang anda masukan bermasalah</td></tr>'
                            );
                        } else {
                            $('#data-siswa').html(data);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.detail', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-siswa') }}",
                    data: {
                        id_siswa: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-info-circle"></i>')
                        $('#dt_nama').html(data.nama == null ? '-' : data.nama);
                        $('#dt_nik').html(data.nik == null ? '-' : data.nik);
                        $('#dt_nis').html(data.nis == null ? '-' : data.nis);
                        $('#dt_nisn').html(data.nisn == null ? '-' : data.nisn);
                        $('#dt_nama_atas').html(data.nama == null ? '-' : data.nama);
                        $('#dt_agama').html(data.agama == null ? '-' : data.agama);
                        $('#dt_alamat').html(data.alamat == null ? '-' : data.alamat);
                        $('#dt_email').html(data.email == null ? '-' : data.email);
                        $('#dt_jenkel').html(data.jenkel == null ? '-' : data.jenkel);
                        $('#dt_telepon').html(data.telepon == null ? '-' : data.telepon);
                        $('#ttl').html(data.tempat_lahir + ", " + data.tgl_lahir);
                        $('#dt_kelas_diterima').html(data.kls_diterima == null ? '-' : data
                            .kls_diterima);
                        $('#dt_tahun_angkatan').html(data.tahun_angkatan == null ? '-' : data
                            .tahun_angkatan);
                        $('#dt_no_ijazah').html(data.no_ijazah == null ? '-' : data.no_ijazah);
                        $('#dt_tahun_ijazah').html(data.th_ijazah == null ? '-' : data
                            .th_ijazah);
                        $('#dt_nomor_skhun').html(data.no_skhun == null ? '-' : data.no_skhun);
                        $('#dt_tahun_skhun').html(data.th_skhun == null ? '-' : data.th_skhun);
                        $('#dt_anak_ke').html(data.anak_ke == null ? '-' : data.anak_ke);
                        $('#dt_status_keluarga').html(data.status_keluarga == null ? '-' : data
                            .status_keluarga);
                        $('#dt_nama_wali').html(data.nama_wali == null ? '-' : data.nama_wali);
                        $('#dt_alamat_wali').html(data.alamat_wali == null ? '-' : data
                            .alamat_wali);
                        $('#dt_pekerjaan_wali').html(data.pekerjaan_wali == null ? '-' : data
                            .pekerjaan_wali);
                        $('#dt_telepon_wali').html(data.telp_wali == null ? '-' : data
                            .telp_wali);
                        $('#dt_nama_ayah').html(data.nama_ayah == null ? '-' : data.nama_ayah);
                        $('#dt_pekerjaan_ayah').html(data.pekerjaan_ayah == null ? '-' : data
                            .pekerjaan_ayah);
                        $('#dt_nama_ibu').html(data.nama_ibu == null ? '-' : data.nama_ibu);
                        $('#dt_pekerjaan_ibu').html(data.pekerjaan_ibu == null ? '-' : data
                            .pekerjaan_ibu);
                        $('#gambar_siswa').attr('src', data.file);
                        $('#modalDetail').modal('show');
                    }
                });
            });
        })

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
