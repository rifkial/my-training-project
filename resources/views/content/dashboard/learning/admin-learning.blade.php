@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        p.mt-3.mb-0.text-muted.text-sm {
            text-align: center;
        }

        h2,
        h5,
        .h2,
        .h5 {
            font-family: inherit;
            font-weight: 600;
            line-height: 1.5;
            margin-bottom: .5rem;
            color: #32325d;
        }

        h5,
        .h5 {
            font-size: .8125rem;
        }

        @media (min-width: 992px) {

            .col-lg-6 {
                max-width: 50%;
                flex: 0 0 50%;
            }
        }

        @media (min-width: 1200px) {

            .col-xl-3 {
                max-width: 25%;
                flex: 0 0 25%;
            }

            .col-xl-6 {
                max-width: 50%;
                flex: 0 0 50%;
            }
        }


        .bg-danger {
            background-color: #f5365c !important;
        }



        @media (min-width: 1200px) {

            .justify-content-xl-between {
                justify-content: space-between !important;
            }
        }


        .pt-5 {
            padding-top: 3rem !important;
        }

        .pb-8 {
            padding-bottom: 8rem !important;
        }

        @media (min-width: 768px) {

            .pt-md-8 {
                padding-top: 8rem !important;
            }
        }

        @media (min-width: 1200px) {

            .mb-xl-0 {
                margin-bottom: 0 !important;
            }
        }




        .font-weight-bold {
            font-weight: 600 !important;
        }


        a.text-success:hover,
        a.text-success:focus {
            color: #24a46d !important;
        }

        .text-warning {
            color: #fb6340 !important;
        }

        a.text-warning:hover,
        a.text-warning:focus {
            color: #fa3a0e !important;
        }

        .text-danger {
            color: #f5365c !important;
        }

        a.text-danger:hover,
        a.text-danger:focus {
            color: #ec0c38 !important;
        }

        .text-white {
            color: #fff !important;
        }

        a.text-white:hover,
        a.text-white:focus {
            color: #e6e6e6 !important;
        }

        .text-muted {
            color: #8898aa !important;
        }

        @media print {

            *,
            *::before,
            *::after {
                box-shadow: none !important;
                text-shadow: none !important;
            }

            a:not(.btn) {
                text-decoration: underline;
            }

            p,
            h2 {
                orphans: 3;
                widows: 3;
            }

            h2 {
                page-break-after: avoid;
            }

            @ page {
                size: a3;
            }

            body {
                min-width: 992px !important;
            }
        }

        figcaption,
        main {
            display: block;
        }

        main {
            overflow: hidden;
        }

        .bg-yellow {
            background-color: #ffd600 !important;
        }

        .icon {
            width: 3rem;
            height: 3rem;
        }

        .icon i {
            /* font-size: 44px; */
            color: #8898aa;
        }

        .icon-shape {
            display: inline-flex;
            padding: 12px;
            text-align: center;
            border-radius: 50%;
            align-items: center;
            justify-content: center;
        }

        .col {
            text-align: center;
        }

        .btn-primary:active,
        .btn-primary.active,
        .show>.btn-primary.dropdown-toggle {
            background-color: #387ade;
            background-image: none;
            border-color: #3675d6;
            color: #fff !important;
            -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
            box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        }

        #radioBtn .notActive {
            color: #3276b1;
            background-color: #fff;
            border: 1px solid #fff;
        }

        .card {
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">


    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    @if ($message = Session::get('success'))
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <p class="mb-0">{{ $message }}</p>
                            </div>
                        </div>
                    @endif
                    @if (!empty($room))
                        <h5 class="box-title">Daftar Room yang aktif</h5>
                        <hr>
                        @foreach ($room as $rm)
                            <div class="col-xl-3 col-lg-6">
                                <div class="card card-stats mb-4 mb-xl-0 rounded">
                                    <div class="card-body bg-success">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <div class="icon icon-shape text-white shadow">
                                                    <i class="fas fa-chalkboard-teacher fa-4x text-white"></i>
                                                </div>
                                                <h5 class="card-title text-uppercase text-white mb-0 mt-2">Rombel</h5>
                                                <span
                                                    class="h2 font-weight-bold text-white mb-0">{{ $rm['rombel'] }}</span><br>
                                                <span class="text-nowrap text-white">{{ $rm['guru'] }}</span><br>
                                                <span class="text-nowrap text-white">{{ $rm['mapel'] }}</span>
                                            </div>
                                        </div>
                                        <div class="col mt-2">
                                            <div style=" text-align: center">
                                                <center>
                                                    <div id="radioBtn" class="btn-group">
                                                        <a class="btn btn-primary btn-sm active" onclick="#"
                                                            data-toggle="pilihan_check" data-title="Y">Hidup</a>
                                                        <form action="{{ route('admin_room-update_nonAktif') }}"
                                                            method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $rm['id'] }}">
                                                            <button class="btn btn-primary btn-sm notActive"
                                                                data-toggle="pilihan_check"
                                                                onclick="return confirm('Apa kamu yakin ingin mematikan room ini?');">Matikan
                                                            </button>
                                                        </form>
                                                    </div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <h5 class="box-title text-danger">Saat ini tidak ada room yg aktif</h5>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
