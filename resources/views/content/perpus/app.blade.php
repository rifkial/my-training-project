<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.program-perpus.head')

    <style type="text/css">
    	body{
    		overflow-x: hidden;
            background-color: white;
    	}

        .pustaka_pinjam{
            position: relative;
            bottom: 0.5rem;
        }

        .gold{
            border: 1px solid #e1ef94 !important;
            background-color: #fff89a !important;
        }

        nav{
            position: static !important;
            height: 75px !important;
        }

        .collapse.show {
            display: block;
            z-index: 6;
        }


        .container{
            border-radius: 10px;
        }

    	#img-banner{
    		  background-image: url("https://images.unsplash.com/photo-1507842217343-583bb7270b66?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=990&q=80"); /* The image used */
			  height: 300px; /* You must set a specified height */
			  background-position: center; /* Center the image */
			  background-repeat: no-repeat; /* Do not repeat the image */
			  background-size: cover
    	}

    	#search-section{
            margin-top: -30px;
        }

        #search-book{
            width: 40%;
            height: 50px;
            background-color: white;
            z-index: 5;

        }

        #form-search{
            height: 100%;
            background-color: #fff;
            flex: 5;
        }

        #sname{
            width: 90%;
            background-color: #fff;
            height: 100%;
            border: none;
            padding: 5px 20px;
        }

        #search-book{
            box-shadow: 0px 5px 30px 2px rgba(128,149,156,0.75);
            -webkit-box-shadow: 0px 5px 30px 2px rgba(128,149,156,0.75);
            -moz-box-shadow: 0px 5px 30px 2px rgba(128,149,156,0.75);
        }

        #btn-search{
            background-color: #fff;
            border : none;
        }

        .items{
        display: flex;
        border-radius: 5px;
        background-color: #ffffff;
            border: 1px solid #dddddd;

        }

        .items:hover{
            box-shadow: -1px 3px 8px 0px rgba(199,191,191,0.75);
            -webkit-box-shadow: -1px 3px 8px 0px rgba(199,191,191,0.75);
            -moz-box-shadow: -1px 3px 8px 0px rgba(199,191,191,0.75);
        }

        .item{
            display: flex;
            border-radius: 5px;
            background-color: #ffffff;
            border: 1px solid #dddddd;
        }

        .navbar-brand{
            width: 20%;
            height: 75px;
        }

        .alert-dismissible .close {
            top: -0.8rem !important;
            right: -1rem !important;
        }

        p{
            color: black;
        }

        @media only screen and (max-width: 992px) {
            .navbar-brand{
                width: 80%;
            }
        }

        @media only screen and (max-width: 768px) {
            #search-book{
                width: 60%;
            }
        }

        @media only screen and (max-width: 528px) {
            #search-book{
                width: 80%;
            }

            #sname {
                width: 85%;
            }
        }




    </style>
</head>
<body>
    @if(session('role-perpus') == 'user')
        @include('includes.program-perpus.nav.nav_user')
    @else
        @include('includes.program-perpus.nav.nav_public')
    @endif
    <!-- navbar -->

    <!-- carousel -->
    <div class="carousel">
        @yield('carousel')

    </div>


	<main class="row mt-5">
		@yield('content')
	</main>


	@include('includes.program-perpus.foot')
</body>
</html>
