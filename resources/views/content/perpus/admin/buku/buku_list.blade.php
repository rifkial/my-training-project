@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 250px;
	}

	#frame{
		height: 100px !important;
	}

	.caret{
		display: none !important;
	}

    #printArea{
        display: none;
    }
</style>

<section class="container bg-white py-3 px-5">
	@if (Session::has('message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
            <p class="mb-1">{{ Session::get('message') }}</p>
      </div>
    @endif

	<h4 class="mb-1">Daftar Pustaka
		<br>
		<small class="subtitle-section">daftar semua pustaka</small>
	</h4>

	<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
     </button>

    <div class="row justify-content-between my-4 flex-column">
      <section class="col-10 d-flex flex-row g-1">
          <div class="col-md-3">
            <label for="inputEmail4">Koleksi</label>
            <select id="filter_koleksi" class="form-control">
              <option value="0">Semua</option>
              @foreach($koleksi as $koleksis)
                  <option value="{{$koleksis['id']}}">{{$koleksis['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Jenis</label>
            <select id="filter_jenis" class="form-control">
              <option value="0">Semua</option>
              @foreach($jenis as $jeniss)
                  <option value="{{$jeniss['id']}}">{{$jeniss['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Bahasa</label>
            <select id="filter_bahasa" class="form-control">
              <option value="0">Semua</option>
              @foreach($bahasa as $bahasas)
                  <option value="{{$bahasas['nama']}}">{{$bahasas['nama']}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-3">
            <label for="inputEmail4">Rak</label>
            <select id="filter_rak" class="form-control">
              <option value="0">Semua</option>
              @foreach($rak as $raks)
                  <option value="{{$raks['id']}}">{{$raks['nama']}}</option>
              @endforeach
            </select>
          </div>
      </section>
      <h5 class="title-filter subtitle-section mx-auto mt-4"></h5>

	</div>
    <form action="{{ route('item-print') }}" method="POST">
    <span>
        <button type="submit" class="btn btn-info btn-sm mb-1">Cetak</button>
        <a onclick="check_all()" class="btn btn-primary text-white btn-sm mb-1">Check all</a>
    </span>

	<table class="table table-striped table-responsive tabel-buku">
	    <thead>
	        <tr>
                <th></th>
	            <th>No</th>
	            <th>GMD</th>
	            <th>Judul</th>
	            <th>Kode</th>
	            <th>id_jenis</th>
	            <th>id_koleksi</th>
	            <th>id_bahasa</th>
	            <th>id_rak</th>
	            <th>Lampiran</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>

			@csrf
	    <tbody>

		</tbody>

	</table>


    <form>


    <div id="printArea">

    </div>

	@include('includes.program-perpus.notify')
	@include('components.perpus.modal.buku-detail')
	@include('components.perpus.modal.buku-edit')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.8.0/dist/JsBarcode.all.min.js"></script>

</section>

@include('includes.program-perpus.datatable')

<script type="text/javascript">


	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('.tabel-buku');
	let url = "{{ route('buku-admin') }}";
	let column = [
        {
			data: 'checkbox',
            name: 'checkbox',
            width: "5%"
		},
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
		{
            data: 'gmd',
            name: 'gmd',
            width: "15%",
            orderable :true
	    },
	    {
            data: 'judul',
            name: 'judul',
            width: "15%",
            orderable :true
        },
        {
            data: 'kode',
            name: 'kode',
            width: "15%",
            orderable :true
        },
        {
          data: 'id_jenis',
          name: 'id_jenis',
          width: "10%",
          visible: false,
          searchable: true
        },
        {
          data: 'id_koleksi',
          name: 'id_koleksi',
          width: "10%",
          visible: false,
          searchable: true
        },
        {
          data: 'id_bahasa',
          name: 'id_bahasa',
          width: "10%",
          visible: false,
          searchable: true
        },
        {
          data: 'id_rak',
          name: 'id_rak',
          width: "10%",
          visible: false,
          searchable: true
        },
        {
            data: 'lampiran',
            name: 'lampiran',
            width: "15%"
        },
        {
            data: 'aksi',
            name: 'aksi',
            width: "15%"
        },
	];

	MakeTable(tabel,url,column);

    function PrintWindow() {
        var divContents = document.getElementById("printArea").innerHTML;
        var a = window.open('', '', 'height=1000, width=1200');
            a.document.write('<html>');
            a.document.write('<body>');
            a.document.write('<div class="main">');
            a.document.write(divContents);
            a.document.write('</div>');
            a.document.write('</body></html>');
            a.document.close();
            a.print();
    }


    function check_all(){
        $('input[type="checkbox"]').prop("checked", true);
    }

	function modalDetailBuku(id){
		let page;
		let penulis,topik,topik_label;
		$.ajax({
			type : 'POST',
			url  : "{{ route('ajax-detail-buku') }}",
			data : {
				id : id
			},
			dataType: 'json',
			beforeSend : ()=>{
				$('.tabledit-detail-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : function(data){
				$('.tabledit-detail-button-'+id).html(
                    '<i class="fas fa-info-circle"></i>');

				penulis = '';
				topik = '';

				if(data.data.penulis.length != 0){
					for(let i = 0 ; i < data.data.penulis.length ; i++){
						penulis += `${data.data.penulis[i].nama} ,`;
					}
				}

				if(data.data.topik.length != 0){
					for(let i = 0 ; i < data.data.topik.length ; i++){
						topik += `${data.data.topik[i].nama}, `;
					}
				}

				page = `
					<div class="row">
						<div class="col-4">
							<img src="${data.data.file}">
							<h5>${data.data.judul} - <strong>${data.data.kode}</strong></h5>
							<a href="${data.data.lampiran}">Lampiran</a>
						</div>
						<div class="col-8">
							<h6>Deskripsi : ${data.data.desc_buku}</h6>
							<div class="row mt-2">
	                        	<div class="col-4">
	                        		<p>ISBN</p>
	                        		<p>Jumlah eskemplar</p>
	                                <p>Deskripsi fisik</p>
	                                <p>Koleksi</p>
	                                <p>Topik</p>
	                        		<p>Penerbit</p>
	                        		<p>Tahun terbit</p>
	                        		<p>Pengarang</p>
	                        		<p>Bahasa</p>
	                        		<p>Halaman</p>
	                        		<p>Jenis</p>
	                        		<p>Lokasi rak</p>
	                        		<p>Agen</p>
	                        	</div>
	                        	<div class="col-8">
	                        		<p>: ${data.data.ISBN}</p>
	                        		<p>: ${data.data.jumlah_eskemplar}</p>
	                                <p>: ${data.data.desc_fisik}</p>
	                                <p>: ${data.data.koleksi}</p>
	                                <p>: ${topik}</p>
	                        		<p>: ${data.data.penerbit}</p>
	                        		<p>: ${data.data.tahun_terbit}</p>
	                        		<p>: ${penulis}</p>
	                        		<p>: ${data.data.bahasa}</p>
	                        		<p>: ${data.data.halaman}</p>
	                        		<p>: ${data.data.jenis}</p>
	                        		<p>: ${data.data.rak}</p>
	                        		<p>: ${data.data.agen}</p>
	                        	</div>
	                        </div>
                        </div>

					</div>
				`;
				$('#body-detail-buku').html(page);
				$('#modal-detail-buku').modal('show');
			},
		});
	}

	function modalCreate(){
		$('#modal-edit-buku').modal('show');
	}

	function modalEditBuku(id){
		$.ajax({
			type : 'POST',
			url  : "{{ route('ajax-detail-buku') }}",
			data : {
				id : id
			},
			dataType: 'json',
			beforeSend : ()=>{
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : function(data){
				$('#ids').val(id);
				$('#judul').val(data.data.judul);
				$('#kode').val(data.data.kode);
				$('#desc_buku').val(data.data.desc_buku);
				$('#desc_fisik').val(data.data.desc_fisik);
				$('#id_penerbit').val(data.data.id_penerbit);
				$('#form-jumlah').hide();
				$('#id_bahasa').val(data.data.id_bahasa);
				$('#id_koleksi').val(data.data.id_koleksi);
				$('#id_jenis').val(data.data.id_jenis);
				$('#id_lokasi').val(data.data.id_rak);
				$('#id_agen').val(data.data.id_agen);
				$('#isbn').val(data.data.ISBN);
				$('#tahun_terbit').val(data.data.tahun_terbit);
				$('#halaman').val(data.data.halaman);
				$('#modal-edit-buku').modal('show');
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fas fa-pencil-alt"></i>');
			}
		});
	}

	$('#modal-edit-buku').on('hidden.bs.modal', function () {
	    $(this).find('form').trigger('reset');
	    $("#frame").attr("src",null);
	});

	function preview() {
	    frame.src=URL.createObjectURL(event.target.files[0]);
	}

    function cetak(){

        let labelMain = `
            <style type="text/css">
                * {
                    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
                    color-adjust: exact !important;                 /*Firefox*/
                }

                body{
                    display : inline-block;
                    background-color : lightblue;
                }

                .label-main{
                    margin : 0;
                }

                .label-body{
                    border: 1px solid black;
                    width: 350px;
                    background-color: white;
                }
                    .label-head{
                        border-bottom: 1px solid black;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

                .label-content{
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                }

                .label-barcode{
                    padding: 3px 5px;
                }

                .alt-code{
                    margin-bottom: 0;
                }

                .barcode{
                    display: flex;
                    justify-content: center;
                }

                img{
                    height: 25px;
                    margin: 0 10px;
                }

                .label-main{
                    display: flex;
                }

                h5,h6{
                    margin : 3px 0;
                }


                @media print{

                    #printArea{
                        display: block;
                    }

                    body{
                        margin : 0;
                        box-sizing : border-box;
                        display : inline-block;
                    }

                    .label-main{
                        display : inline-block;
                        margin : 1.3px 0;
                    }

                    .alt-code{
                        margin-top : 0;
                        margin-bottom: 0;
                    }

                    h6,h5,p{
                        text-align: center;
                        margin-bottom : 0;
                    }

                }


            </style>
        `;
        let labelHead = '';
        let labelBarcode = '';
        let datas = '';
        let codes ='';

        let kode = [];
        $("input:checkbox[class=check_print]:checked").each(function() {
            kode.push($(this).val());
        });

        function display(item){
            sessionStorage.setItem('kode', item.kode);

            let bar = '<p>{!! '+item.kode+' !!}</p>';
            labelMain += `
            <div class="label-main">
                <div class="label-body">
                    ${labelHead}
                    <div class="label-barcode">
                        <div class="barcode">
                            {!! DNS1D::getBarcodeHTML('${item.kode}','C128',2.0,25) !!}

                        </div>
                        <p class="alt-code">${item.kode}</p>
                        <p>{!! `+item.kode+` !!}</p>
                        ${bar}
                    </div>
                </div>
            </div>
            `;

        }

        $.ajax({
            type : "get",
            url  : "{{route('label-get-one')}}",
            success : (data) =>{
                labelHead = `
                    <div class="label-head">
						<img src="${data.data.logo}">
						<span>
							<h5 class="mb-0">${data.data.nama_label}</h5>
							<h6 class="mb-0 mt-0">${data.data.nama_instansi}</h6>
							<p class="mb-0">${data.data.alamat}</p>
						</span>
					</div>
                `;

                $.ajax({
                    type : "post",
                    url  : "{{route('item-find')}}",
                    data : {
                        'id' : kode
                    },
                    success : (data)=>{
                        console.log(data.data);
                        datas = data.data;
                        datas.forEach(display);
                        $('#printArea').html(labelMain);
                        PrintWindow();
                    }
                });

            }
        });
    }


	$('#filter_jenis').change(function( ){
		tabel.DataTable().columns(4)
          .search($(this).val()).draw();

		if($(this).val() == 0 ){
			$('.title-filter').html('');
		}else{
			$('.title-filter').html('Filter berdasarkan jenis');
		}

	  });

	  $('#filter_koleksi').change(function( ){
	      tabel.DataTable().columns(5)
	      .search($(this).val()).draw();

	      if($(this).val() == 0 ){
			$('.title-filter').html('');
		}else{
			$('.title-filter').html('Filter berdasarkan koleksi');
		}

	  });

	  $('#filter_rak').change(function( ){
	      tabel.DataTable().columns(7)
	      .search($(this).val()).draw();

	      if($(this).val() == 0 ){
			$('.title-filter').html('');
		}else{
			$('.title-filter').html('Filter berdasarkan lokasi rak');
		}
	  });

	  $('#filter_bahasa').change(function( ){
	      tabel.DataTable().columns(6)
	      .search($(this).val()).draw();

	      if($(this).val() == 0 ){
			$('.title-filter').html('');
		}else{
			$('.title-filter').html('Filter berdasarkan bahasa');
		}
	  });



</script>


@endsection
