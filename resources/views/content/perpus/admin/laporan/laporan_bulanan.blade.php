@extends('content.perpus.admin')
@section('content')

<style type="text/css">

	.table>:not(:first-child) {
	    border-top: 1px solid currentColor;
	}
	p{
		margin-bottom: 0;
	}

</style>

<section class="container-fluid">
	<div class="d-flex justify-content-end mb-2">
		<div class="mr-2">
		<button onclick="PrintWindow()" class="btn btn-primary btn-sm">Print</button>
		</div>
		<div class="dropdown">
		  <a class="btn btn-secondary dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
		    Bulan
		  </a>

		  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 1]) }}">Januari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 2]) }}">Februari</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 3]) }}">Maret</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 4]) }}">April</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 5]) }}">Mei</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 6]) }}">Juni</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 7]) }}">Juli</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 8]) }}">Agustus</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 9]) }}">September</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 10]) }}">Oktober</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 11]) }}">November</a></li>
		    <li><a class="dropdown-item" href="{{ route('laporan-index',['bulan'=> 12]) }}">Desember</a></li>

		  </ul>
		</div>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 80%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan {{$kartu['nama_sistem']}}</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
			<p class="text-center my-1">Bulan {{$bulan}} {{  date('Y') }}</p>

		</div>

		<div class="container">
			<ol>
				<!-- anggota -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Anggota</p>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th class="text-center" scope="col">Jenis</th>
					      <th class="text-center" scope="col">Jumlah</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach($userRole as $usr => $value)
					    <tr>
					      <td class="text-center">{{ $usr }} </td>
					      <td class="text-center">{{ count($value) }}</td>
					    </tr>
					   @endforeach
					  </tbody>
					</table>
				</li>

				<!-- gmd -->
				<li class="my-1">
					<p class="my-1 text-uppercase">GMD</p>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th scope="col" width="10%">No</th>
					      <th scope="col" width="50%">Nama</th>
					      <th scope="col" width="20%">Kode</th>
					      <th scope="col" width="10%">Jumlah pustaka</th>
					      <th scope="col" width="10%">Jumlah eskemplar</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@php
	                        $no = 1;
	                    @endphp
					  	@foreach($gmds as $gm )
                            @if($gm['jumlah_pustaka'] != 0)
                                <tr>
                                <td>{{ $no++ }} </td>
                                <td>{{ $gm['nama'] }} </td>
                                <td>{{ $gm['kode'] }}</td>
                                <td class="text-center">{{ $gm['jumlah_pustaka'] }}</td>
                                <td class="text-center">{{ $gm['jumlah_eskemplar'] }}</td>
                                </tr>
                            @endif
					   @endforeach
					  </tbody>
					</table>
				</li>

				<!-- koleksi -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Koleksi</p>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th scope="col" width="10%">No</th>
					      <th scope="col" width="50%">Nama</th>
                          <th scope="col" width="20%">Kode</th>
					      <th scope="col" width="10%">Jumlah pustaka</th>
					      <th scope="col" width="10%">Jumlah eskemplar</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@php
	                        $no = 1;
	                    @endphp
					  	@foreach($koleksis as $kl )
                          @if($kl['jumlah_pustaka'] != 0)
                            <tr>
                            <td>{{ $no++ }} </td>
                            <td>{{ $kl['nama'] }} </td>
                            <td>{{ $kl['kode'] }} </td>
                            <td class="text-center">{{ $kl['jumlah_pustaka'] }}</td>
                            <td class="text-center">{{ $kl['jumlah_eskemplar'] }}</td>
                            </tr>
                          @endif
					   @endforeach
					  </tbody>
					</table>
				</li>

				<!-- jenis -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Jenis</p>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th scope="col" width="10%">No</th>
					      <th scope="col">Nama</th>
					      <th scope="col" width="10%">Jumlah pustaka</th>
					      <th scope="col" width="10%">Jumlah eskemplar</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@php
	                        $no = 1;
	                    @endphp
					  	@foreach($jeniss as $js )
                          @if($js['jumlah_pustaka'] != 0)
                            <tr>
                            <td>{{ $no++ }} </td>
                            <td>{{ $js['nama'] }} </td>
                            <td class="text-center">{{ $js['jumlah_pustaka'] }}</td>
                            <td class="text-center">{{ $js['jumlah_eskemplar'] }}</td>
                            </tr>
                          @endif
					   @endforeach
					  </tbody>
					</table>
				</li>

				<!-- peminjaman -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Peminjaman</p>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th scope="col">Jumlah anggota peminjam</th>
					      <th scope="col">Jumlah total peminjaman</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>{{ $peminjamans['jumlah_peminjam'] }}</td>
					      <td>{{ $peminjamans['jumlah_peminjaman'] }}</td>
					    </tr>
					  </tbody>
					</table>
				</li>

				<!-- pengadaan -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Pengadaan</p>
					<div class="d-flex">
						<div class="col-4">
							<p class="mb-0">Pengadaan</p>
							<p class="mb-0">Pengadaan diterima</p>
							<p class="mb-1">Pengadaan ditolak</p>
						</div>
						<div class="col-4">
							<p class="mb-0">: {{ $pengadaans['jumlah_pengadaan'] }}</p>
							<p class="mb-0">: {{ $pengadaans['jumlah_pengadaan_diterima'] }}</p>
							<p class="mb-1">: {{ $pengadaans['jumlah_pengadaan_ditolak'] }}</p>
						</div>
					</div>
					<table class="table table-bordered" style="width: 90%">
					  <thead>
					    <tr>
					      <th scope="col">No</th>
					      <th scope="col">Pustaka</th>
					      <th scope="col">Jumlah</th>
					      <th scope="col">Total harga</th>
					      <th scope="col">Status</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@php
	                        $no = 1;
                            $jumlah = 0;
	                    @endphp
					  	@foreach($pengadaans['data'] as $pd )
					    <tr>
					      <td>{{ $no++ }} </td>
					      <td>{{ $pd['buku'] }} </td>
					      <td class="text-center">{{ $pd['jumlah'] }}</td>
					      <td><?php echo number_format($pd['total_harga']) ?></td>
					      <td>{{ $pd['diterima'] }}</td>
                            @php
	                            $jumlah += $pd['jumlah'];
	                        @endphp
					    </tr>
					   @endforeach
					  </tbody>
					  <tfoot>
					   	<td class="text-center" colspan="3">Total pengadaan disetujui</td>
					   	<td>Rp. <?php echo number_format($pengadaans['total']) ?></td>
					   	<td></td>
					  </tfoot>
					</table>
				</li>

				<!-- denda -->
				<li class="my-1">
					<p class="my-1 text-uppercase">Denda</p>
                    <table class="table table-bordered" style="width: 90%">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Hari terlewat</th>
                            <th scope="col">Denda</th>
                          </tr>
                        </thead>
                        <tbody>
                            @forelse($dendas['data'] as $dd )
                            <tr>
                                <td width="10%">{{ $loop->iteration }} </td>
                                <td width="50%">{{ $dd['nama'] }} </td>
                                <td width="10%">{{ $dd['hari_terlewat'] }}</td>
                                <td width="30%"><?php echo number_format($dd['denda']) ?></td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="4">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                             <td class="text-center" colspan="3">Total denda : </td>
                             <td>Rp. <?php echo number_format($dendas['jumlah_denda']) ?></td>
                        </tfoot>
                      </table>
				</li>


			</ol>

		</div>
	</div>
</section>

<script type="text/javascript">

	function back() {
        location.reload();
    }

	window.onafterprint = back;

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}

</script>

@endsection
