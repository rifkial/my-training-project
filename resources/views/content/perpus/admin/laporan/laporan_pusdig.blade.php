@extends('content.perpus.admin')
@section('content')

<section class="container-fluid">
	<div class="row">
		<h4>
			<button onclick="PrintWindow()" class="btn btn-secondary">Print</button>
		</h4>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 80%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan Daftar Pustaka Digital</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
      <p class="text-center my-1">Berdasarkan Klasifikasi</p>

		</div>

		<div class="container">
		@foreach($bukus as $buku)
            @if(!empty($buku["buku"]))
            <div class="container my-5">
                <h5>{{$buku["koleksi"]}}</h5>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">ISBN</th>
                      <th scope="col">Judul</th>
                      <th scope="col">Kode</th>
                      <th scope="col">Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($buku["buku"] as $item)
                        <tr>
                          <th scope="row">{{$no}}</th>
                          <td>{{$item["ISBN"]}}</td>
                          <td>{{$item["judul"]}}</td>
                          <td>{{$item["kode"]}}</td>
                          <td>{{$item["download"]}}</td>
                        </tr>
                        @php
                            ++$no;
                        @endphp
                    @endforeach
                  </tbody>
                </table>
            </div>
            @endif
        @endforeach
		</div>
	</div>

</section>


<script type="text/javascript">

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}

  function back() {
        location.reload();
    }

  window.onafterprint = back;

</script>


@endsection
