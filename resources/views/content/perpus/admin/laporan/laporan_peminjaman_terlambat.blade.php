@extends('content.perpus.admin')
@section('content')

<style type="text/css">

	.table>:not(:first-child) {
	    border-top: 1px solid currentColor;
	}
	p{
		margin-bottom: 0;
	}

</style>

<section class="container-fluid">
	<div class="d-flex justify-content-end mb-2">
		<button onclick="PrintWindow()" class="btn btn-primary btn-sm">Print</button>
	</div>

	<div class="container bg-white p-3" id="printArea" style="width: 90%">
		<div class="container">
			<h4 class="text-center text-uppercase mb-0">Laporan Peminjaman Terlambat <br>{{$kartu['nama_sistem']}}</h4>
			<h4 class="text-center text-uppercase mt-1">{{$kartu['nama_instansi']}}</h4>
		</div>

		<div class="container">
			<table class="table table-bordered" style="width: 100%">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">Peminjam</th>
			      <th scope="col">Pustaka</th>
			      <th scope="col">Tanggal Pinjam</th>
			      <th scope="col">Tanggal Kembali</th>
			      <th scope="col">Terlambat</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@php
                    $no = 1;
                @endphp
			  	@foreach($peminjaman as $pm )
			    <tr>
			      <td>{{ $no++ }} </td>
			      <td>{{ $pm['peminjam'] }} </td>
			      <td>{{ $pm['buku'][0]}} {{$pm['kode_item']}}</td>
			      <td>{{ $pm['tanggal_pinjam'] }}</td>
			      <td>{{ $pm['tanggal_kembali'] }}</td>
			      <td class="text-center">{{ $pm['terlambat'] }} hari</td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
		</div>
	</div>

</section>

<script type="text/javascript">

    function back() {
        location.reload();
    }

	window.onafterprint = back;

	function PrintWindow(){
        var printContents = document.getElementById("printArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
	}



</script>


@endsection
