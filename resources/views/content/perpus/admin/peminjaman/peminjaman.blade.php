@extends('content.perpus.admin')
@section('content')




<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}

	.bootstrap-select{
		width: 100% !important;
		
	}

	/*.bs-caret{
		background-color: white !important;
	}*/

	.caret{
		display: none !important;
	}
</style>

<section class="container-fluid">
	<div class="col-8">
		<h4 class="mb-4">Tambah
			<br>
			<small class="subtitle-section">peminjaman buku</small>
		</h4>

		<form id="form-add-peminjaman" action="javascript:void(0)" method="post">
		@csrf
		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Peminjam</label>
		    <div class="col-sm-10">
		      <select class="selectpicker" data-live-search="true" name="id_peminjam">
		      	@foreach($users as $user)
				  <option value="{{$user['id']}}">{{$user['no_induk']}}</option>
				@endforeach
		  	  </select>
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Kode item</label>
		    <div class="col-sm-10">
		      <input type="text" name="kode_item" class="form-control">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Keterangan</label>
		    <div class="col-sm-10">
		      <input type="text" name="keterangan" class="form-control">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal pinjam</label>
		    <div class="col-sm-10">
		      <input type="date" name="tgl_pinjam" class="form-control">
		    </div>
		  </div>

		  <button type="submit" id="btn-Submit" class="btn btn-primary">Simpan</button>

		</form>
	</div>

	<div class="col-4">
		
	</div>
	
		

</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>

<script type="text/javascript">

	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    }

    function swa(status, message, icon) {
        swal(
            status,
            message,
            icon
        );
        return true;
    }

	$('#form-add-peminjaman').on('submit',function(){
        let data = $(this).serialize();
        console.log(data);

        $.ajax({
            type : "post",
            url  : "{{route('peminjaman-add')}}",
            data : data,
            beforeSend: function() {
                $('#btn-Submit').html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#form-add-peminjaman').trigger("reset");
                $('#btn-Submit').html(
                    'Simpan');
            },
            error: function(data) {
                noti(data.icon, data.message);  
                $('#btn-Submit').html(
                    'Simpan'); 
            } 
        }); 
    });

</script>
		
@endsection