@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}
</style>

<section class="container-fluid">

	<div class="widget-bg mb-5">
		<!-- header -->
		<div class="row p-3">    
			<div class="col-4">
				<h4>Peminjaman</h4>	
			</div>   	
		</div>

		<!-- body tab -->
		<div class="widget-body clearfix px-3">
			<!-- tabs -->
			<div class="tabs tabs-bordered">
				<ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link" href="{{route('peminjaman-active')}}">Berjalan</a>
                    </li>
                    <li class="nav-item active"><a class="nav-link" href="#return-tab-bordered-1" data-toggle="tab" aria-expanded="true">Kembali</a>
                    </li>
                </ul>
			</div>

			<!-- content-tab -->
			<div class="tab-pane" id="return-tab-bordered-1">
				<div class="container">
					<table class="table table-striped table-responsive" data-toggle="datatables" id="tabel-peminjaman">
					    <thead>
					        <tr>
					            <th>No</th>
					            <th>Peminjam</th>
					            <th>Buku</th>
					            <th>Kode Buku</th>
					            <th>Tanggal pinjam</th>
					            <th>Tanggal kembali</th>
					            <th>Status</th>
					        </tr>
					    </thead>
					    <tbody>  
					                    
						</tbody>
					</table>
				</div>		
			</div>

		</div>

	</div>

	
		
</section>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

	const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: true,
        paging: true,
        searching: true,
   };

   let tabel = $('#tabel-peminjaman');
	let url = "{{ route('peminjaman-return') }}";
 	let column = 
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'peminjam',
	            name: 'peminjam',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "35%",
	            orderable :true
	        },
	        {
	            data: 'kode_item',
	            name: 'kode_item',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'tanggal_pinjam',
	            name: 'tanggal_pinjam',
	            width: "20%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_dikembalikan',
	            name: 'tanggal_dikembalikan',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'status_dipinjam',
	            name: 'status_dipinjam',
	            width: "10%",
	            searchable: true
	        }
	    ];

	function MakeTable(tabel,url,column){
        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    }

    MakeTable(tabel,url,column);



</script>


		
@endsection