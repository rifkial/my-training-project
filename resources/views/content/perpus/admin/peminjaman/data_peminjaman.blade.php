@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}
</style>

<section class="container-fluid">

	<div class="widget-bg mb-5">

		<!-- header -->
		<div class="row p-3">
			<div class="col-4">
				<h4>Peminjaman</h4>

			</div>
		</div>

		<!-- body tab -->
		<div class="widget-body clearfix px-3">
			<!-- tabs -->
			<div class="tabs tabs-bordered">
				<ul class="nav nav-tabs">
                    <li class="nav-item active"><a class="nav-link" href="#home-tab-bordered-1" data-toggle="tab" aria-expanded="true">Berjalan</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('peminjaman-return')}}">Kembali</a>
                    </li>
                </ul>
			</div>

			<!-- content-tab -->
			<div class="tab-pane" id="home-tab-bordered-1">
				<div class="container">
					<table class="table table-striped table-responsive" id="tabel-peminjaman">
					    <thead>
					        <tr>
					            <th>No</th>
					            <th>Peminjam</th>
					            <th>Buku</th>
					            <th>Kode Buku</th>
					            <th>Tanggal pinjam</th>
					            <th>Tanggal kembali</th>
					            <th>Status</th>
					            <th>Aksi</th>
					        </tr>
					    </thead>
					    <tbody>

						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>


</section>

@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });


    let tabel = $('#tabel-peminjaman');
	let url = "{{ route('peminjaman-active') }}";
 	let column =
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'peminjam',
	            name: 'peminjam',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "25%",
	            orderable :true
	        },
	        {
	            data: 'kode_item',
	            name: 'kode_item',
	            width: "5%",
	            orderable :true
	        },
	        {
	            data: 'tanggal_pinjam',
	            name: 'tanggal_pinjam',
	            width: "10%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_kembali',
	            name: 'tanggal_kembali',
	            width: "10%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'status_dipinjam',
	            name: 'status_dipinjam',
	            width: "10%",
	            searchable: true
	        },
	        {
	            data: 'aksi',
	            name: 'aksi',
	            width: "25%"
	        }
	    ];


    MakeTable(tabel,url,column);

    function perpanjang(id){
        $.ajax({
            type : "get",
            url  : "{{route('peminjaman-extends')}}",
            data : {
                'id' : id
            },
            beforeSend : ()=>{
            	$('#btnExtend-'+ id).html(`
            		'<i class="fa fa-spin fa-spinner"></i>'
            	`);
            },
            success : (data) =>{
                noti(data.icon, data.message);
                $('#tabel-peminjaman').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('#btnExtend-'+ id).html(`
            		'perpanjang'
            	`);
            }
        });
    }

    function returned(id){
    	$.ajax({
            type : "post",
            url  : "{{route('peminjaman-kembali')}}",
            data : {
            	'id' : id
            },
            beforeSend : ()=>{
            	$('#btnReturn-'+ id).html(`
            		'<i class="fa fa-spin fa-spinner"></i> Loading'
            	`);
            },
            success : (data) =>{
            	noti(data.icon, data.message);
            	$('#tabel-peminjaman').DataTable().ajax.reload();
            	$('#btnReturn-'+ id).html(`
            		Kembali
            	`);
            },
            error: function(data) {
            	noti(data.icon, data.message);
            }
        });
    }

</script>


@endsection
