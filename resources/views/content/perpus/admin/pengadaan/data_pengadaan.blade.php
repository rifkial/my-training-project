@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}

	body{
		background-color: white;
	}

	.spacer{
		height: 100px;
	}
</style>

<section class="container-fluid">

	<div class="widget-bg mb-5">

		<!-- header -->
		<div class="container p-3 align-items-center justify-content-between">    
			
				<h4>Pengadaan Berjalan</h4>
				<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
			        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
			    </button>
				
			 	
		</div>

		<!-- body tab -->
		<div class="widget-body clearfix px-3">
			<!-- tabs -->
			<div class="tabs tabs-bordered">
				<ul class="nav nav-tabs">
					<li class="nav-item active"><a class="nav-link" href="#active-tab-bordered-1" data-toggle="tab" aria-expanded="true">Berjalan</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('pengadaan-accepted')}}">Diterima</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('pengadaan-rejected')}}">Ditolak</a>
                    </li>
                </ul>
			</div>

			<!-- content-tab -->
			<div class="tab-pane" id="active-tab-bordered-1">
				<div class="container">

					

					<table class="table table-striped table-responsive tabel-pengadaan" id="">
					    <thead>
					        <tr>
					            <th>No</th>
					            <th>Nama</th>
					            <th>Buku</th>
					            <th>Keterangan</th>
					            <th>Jumlah</th>
					            <th>Prioritas</th>
					            <th>Tanggal pengajuan</th>
					            <th>Total harga</th>
					            <th>Aksi</th>
					        </tr>
					    </thead>
					    <tbody>  
					                    
						</tbody>
					</table>
				</div>	
			</div>

		</div>
	</div>

		 
</section>

<!-- modal create -->
<div class="modal fade bd-example-modal-lg" id="modal-store-master" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Edit</h5>
        </div>

        <div class="modal-body">
        <form id="form-add-master" action="javascript:void(0)" method="post">
        @csrf

        <input type="hidden" class="form-control" name="id" id="id" placeholder="id">

        <div class="form-group">
            <label for="inputEmail4">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="nama">
        </div>

        <div class="form-group">
            <label for="inputState">Buku</label>
		      <select id="id_buku" name="id_buku" class="form-control">
		      	@foreach($bukus as $bk)
		        <option value="{{$bk['id']}}">{{$bk['judul']}}</option>
		        @endforeach
		      </select>
        </div>

        <div class="form-group">
            <label for="inputEmail4">Keterangan</label>
              <input type="text" class="form-control" name="keterangan" id="keterangan" >
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">Jumlah</label>
              <input type="text" class="form-control" name="jumlah" id="jumlah" >
            </div>

            <div class="form-group col-md-4 ">
              <label for="inputPassword4">Harga</label>
              <input type="number" class="form-control" name="harga" id="harga">
            </div>
            <div class="form-group col-md-4 ">
            <label for="inputState">Prioritas</label>
		      <select id="prioritas" name="prioritas" class="form-control">
		        <option value="mendesak">Mendesak</option>
		        <option value="normal">Normal</option>
		      </select>
        </div>
        </div>

      
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-edit">Tambah</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- end modal create -->

@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });


   let tabel = $('.tabel-pengadaan');
	let url = "{{ route('pengadaan-active') }}";
 	let column = 
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
	        {
	            data: 'nama',
	            name: 'nama',
	            width: "15%",
	            orderable :true
	        },
	        {
	            data: 'buku',
	            name: 'buku',
	            width: "10%",
	            orderable :true
	        },
	        {
	            data: 'keterangan',
	            name: 'keterangan',
	            width: "20%",
	            orderable :true
	        },
	        {
	            data: 'jumlah',
	            name: 'jumlah',
	            width: "5%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'prioritas',
	            name: 'prioritas',
	            width: "15%",
	            orderable :true,
	            visible: true
	        },
	        {
	            data: 'tanggal_pengajuan',
	            name: 'tanggal_pengajuan',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'total_harga',
	            name: 'total_harga',
	            width: "20%",
	            orderable :true,
	            searchable: true
	        },
	        {
	            data: 'aksi',
	            name: 'aksi',
	            width: "10%",
	            searchable: true
	        }
	    ];


    MakeTable(tabel,url,column);

    function modalCreate(){
		$('#modal-store-master').modal('show');
	}

    function edit_button(id){
    	$.ajax({
    		type : "post",
    		data : {
    			'id' : id
    		},
    		url  : "{{ route('pengadaan-detail') }}",
    		beforeSend : ()=>{
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) => {
				console.log(data);
				$('#id').val(data.data.id);
				$('#nama').val(data.data.nama);
				$('#id_buku').val(data.data.id_buku);
				$('#prioritas').val(data.data.prioritas);
				$('#keterangan').val(data.data.keterangan);
				$('#harga').val(data.data.harga);
				$('#jumlah').val(data.data.jumlah);
				$('#modal-store-master').modal('show');
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fas fa-pencil-alt"></i>');
				$('.btn-edit').html('Update');
			}
    	});
    }

    $('#form-add-master').on('submit',function(){
    	let data = $(this).serialize();
    	$.ajax({
    		type : "post",
    		data : data,
    		url  : "{{ route('pengadaan-store') }}",
    		beforeSend : ()=>{
				$('.btn-edit').html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
    		success : (data)=>{
				noti(data.icon, data.message);
				$('#modal-store-master').modal('hide');
				$('.btn-edit').html(
                    'Simpan');
                tabel.DataTable().ajax.reload();
			}
		});
    });

    function submit_accept(id){
    	$.ajax({
    		type : "post",
    		data : {
    			'id' : id,
    			'act' : 2 ,
    		},
    		url  : "{{ route('pengadaan-action') }}",
    		beforeSend : ()=>{
				$('.tabledit-accept-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) =>{
				noti(data.icon, data.message);
				$('.tabledit-accept-button-'+id).html(
                    'terima');
				tabel.DataTable().ajax.reload();

			}
		});
    }

    function submit_reject(id){
    	$.ajax({
    		type : "post",
    		data : {
    			'id' : id,
    			'act' : 1 ,
    		},
    		url  : "{{ route('pengadaan-action') }}",
    		beforeSend : ()=>{
				$('.tabledit-reject-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) =>{
				noti(data.icon, data.message);
				$('.tabledit-reject-button-'+id).html(
                    'terima');
				tabel.DataTable().ajax.reload();
			}
		});
    }

    function action_button(id){
    	if($('.action-btn-'+ id).css("display") == "none"){
    		$('.action-btn-'+ id).css("display", "block");
    		$('.tabledit-edit-button-'+id).css("display", "none");
    		$('.tabledit-delete-button-'+id).css("display", "none");
    	}else{
    		$('.action-btn-'+ id).css("display", "none");
    		$('.tabledit-edit-button-'+id).css("display", "block");
    		$('.tabledit-delete-button-'+id).css("display", "block");
    	}
    }

    function submit_delete(id){
    	$.ajax({
    		type : "post",
    		data : {
    			'id' : id
    		},
    		url  : "{{ route('pengadaan-delete') }}",
    		beforeSend : ()=>{
				$('.tabledit-delete-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) =>{
				noti(data.icon, data.message);
				$('.tabledit-delete-button-'+id).html(
                    '<i class="fas fa-trash-alt"></i>');
				tabel.DataTable().ajax.reload();

			}
		});
    }

    $('#modal-store-master').on('hidden.bs.modal', function () {
	    $(this).find('form').trigger('reset');
	})

</script>

		
@endsection