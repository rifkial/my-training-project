@extends('content.perpus.admin')
@section('content')

<style type="text/css">

	.label-body{
		border: 1px solid black;
		width: 300px;
		background-color: white;
	}
		.label-head{
			border-bottom: 1px solid black;
			display: flex;
		    align-items: center;
		    justify-content: center;
		}

	.label-content{
		display: flex;
	    flex-direction: column;
	    justify-content: center;
	    align-items: center;
	}

        .label-barcode{
            padding: 3px 5px;
        }

    .alt-code{
        margin-bottom: 0;
    }

    .barcode{
        display: flex;
        justify-content: center;
    }

	img{
		height: 50px;
		margin: 0 10px;
	}

	.label-main{
		display: flex;
	}

	h5,h6{
		margin : 5px 0;
	}

	h6,h5,p{
		text-align: center;
	}




</style>

<div class="container">
	<div class="row">

		<div class="col-6">

			<div class="label-main">
				<div class="label-body">
					<div class="label-head">
						<img src="{{$label['logo']}}">
						<span>
							<h5 class="mb-0">{{$label['nama_label']}}</h5>
							<h6 class="mb-0 mt-0">{{$label['nama_instansi']}}</h6>
							<p class="mb-0">{{$label['alamat']}}</p>
						</span>
					</div>

					<div class="label-barcode">
                        <div class="barcode">
                            {!! DNS1D::getBarcodeHTML("8243368","C39+",1.7,25) !!}
                        </div>
                        <p class="alt-code">8243368</p>
                    </div>
				</div>
			</div>



		</div>

		<div class="col-6 bg-white p-4">
			<h4>Setting Label Perpustakaan</h4>

			@if (Session::has('message'))
		      <div class="alert alert-warning alert-dismissible fade show" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		            </button>
		            <p class="mb-1">{{ Session::get('message') }}</p>
		      </div>
		    @endif

			<form action="{{ route('label-store') }}" method="POST" enctype="multipart/form-data">
				@csrf
			  <div class="form-group">
			  	<input type="hidden" name="id" value="1" class="form-control" placeholder="Enter nama">
			    <label for="exampleInputEmail1">Nama </label>
			    <input type="nama" name="nama_label" value="{{$label['nama_label']}}" class="form-control" placeholder="Enter nama">
			  </div>

			  <div class="form-group">
			    <label for="exampleInputnama1">Nama instansi</label>
			    <input type="nama" name="nama_instansi" value="{{$label['nama_instansi']}}" class="form-control" placeholder="Enter nama">
			  </div>

			  <div class="form-group">
			    <label for="exampleInputnama1">Alamat</label>
			    <input type="nama" name="alamat" value="{{$label['alamat']}}" class="form-control" placeholder="Enter nama">
			  </div>

			  <div class="form-group">
			    <label for="exampleFormControlFile1">Logo</label>
				  <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
			  </div>


			  <button type="submit" class="btn btn-primary">Edit</button>
			</form>
		</div>
	</div>

</div>


@endsection
