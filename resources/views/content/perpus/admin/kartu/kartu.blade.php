@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 50px;
	}

	.kartu-body{
		display: flex;
		background-color: white;
		border-radius: 10px;
		height: 260px;
		width: 480px;
	    background-repeat:no-repeat;
	    background-size:cover;
	}

		.kartu-left{
			flex: 2;
		    padding: 30px 0 10px 0;
		    display: flex;
		    flex-direction: column;
		    justify-content: space-between;
		}

			.kartu-title{
				display: flex;
			    flex-direction: column;
			    align-items: center;
			}
				.kartu-sistem{
					margin-top: 10px;
				    font-size: 18px;
				    font-weight: 700;
				}

			.kartu-identify{
				display: flex;
			    flex-direction: column;
			    align-items: center;
			    margin-top: 20px;
			}

		.kartu-right{
			flex: 1;
    		padding: 30px 0;
    		display: flex;
    		flex-direction: column;
    		justify-content: space-between;
		}
			.kartu-data{
				align-items: center;
				display: flex;
			    flex-direction: column;
			    align-items: center;
			}

			.kartu-user{
				display: flex;
    			justify-content: center;
			}

	.kartu-body-back{
		margin-top: 20px;
		display: flex;
		flex-direction: column;
		border-radius: 10px;
		height: 260px;
		width: 480px;
	    background-repeat:no-repeat;
	    background-size:cover;
	}
		.kartu-head{
			display: flex;
		    justify-content: flex-start;
		    align-items: center;
		    padding: 10px 0;
		}

			.kartu-logo{
				margin: 0 20px 0 20px;
			}

			.kartu-logo-back{
				margin: 0 10px 0 20px;
			}

		.kartu-ket{
			padding: 0 25px;
		}

			ol{
				padding: 0 20px !important;
				margin: 0;
			}

		.kartu-foot{
			display: flex;
		    justify-content: end;
		    padding: 0 40px;
		    margin: 0 0 -5px 0;
		}

		.kartu-footer{
			display: flex;
		    justify-content: end;
		    padding: 0 10px;
		    margin: 5px;
		}

		.kartu-nama-ttd{		
		    display: flex;
		    flex-direction: column;
		    align-items: center;
		}

		.kartu-ttds{
			font-size: 11px;
			margin-bottom: -5px;
		}




</style>

<section class="container">
	<div class="row">
		<div class="col-6">
			<!-- depan -->
			<div class="kartu-body" style="background-image:url('{{$kartu['cover']}}');">
				<!-- left -->
				<section class="kartu-left">
					<div class="kartu-title">
						<div class="kartu-logo">
						<img src="{{$kartu['logo']}}">
						</div>
						<div class="kartu-sistem">{{$kartu['nama_sistem']}}</div>
						<div class="kartu-instansi">{{$kartu['nama_instansi']}}</div>
					</div>
					<div class="kartu-identify">
						<div>Feri Alfajri</div>
						<div class="kartu-barcode">
							BARCODE
						</div>
					</div>
					
				</section>

				<!-- right -->
				<section class="kartu-right">
					<div class="kartu-data">
						<h6 class="kartu-user-tipe">Mahasiwsa</h6>
						<h6 class="kartu-user-id">12390088</h6>
						<p>Masa berlaku</p>
					</div>
					<div class="kartu-user">
						<img src="http://localhost/smart_school/public/images/perpus/setting/kartu/perpus_photo.jpeg" style="height: 130px;">
					</div>
					
				</section>
			</div>

			<!-- belakang -->
			<div class="kartu-body-back" style="background-image:url('{{$kartu['cover']}}');">
				<div class="kartu-head">
					<img class="kartu-logo-back" src="{{$kartu['logo']}}">
					<h5>{{$kartu['nama_sistem']}}</h5>
				</div>
				<div class="kartu-ket">
					<h6 style="margin: 5px;">Peraturan</h6>
						{!! $kartu['peraturan'] !!}
				</div>
				<div class="kartu-foot">
					<div class="kartu-ttd" style="position: absolute;">
						<img class="kartu-logos" src="{{$kartu['ttd_jabatan']}}">
					</div>
					<div class="kartu-stempel">
						<img src="{{$kartu['stempel']}}">
					</div>
				</div>
				<div class="kartu-footer">
					<div class="kartu-nama-ttd">
						<div class="kartu-ttds">{{$kartu['nama_jabatan']}}</div>
						<div class="kartu-ttds">{{$kartu['jabatan']}}</div>
						<div class="kartu-ttds">NIP. {{$kartu['nip_jabatan']}}</div>
					</div>	
				</div>
			</div>

		</div>
		<div class="col-6 bg-white p-3">
			<h4>Setting Kartu Perpustakaan</h4>

			@if (Session::has('message'))
		      <div class="alert alert-warning alert-dismissible fade show" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		            </button>
		            <p class="mb-1">{{ Session::get('message') }}</p>    
		      </div>
		    @endif

			<form action="{{ route('kartu-store') }}" method="POST" enctype="multipart/form-data">
				@csrf
			  <div class="form-group">
			  	<input type="hidden" name="id" value="1" class="form-control" placeholder="Enter nama">
			    <label for="exampleInputEmail1">Nama kartu</label>
			    <input type="nama" name="nama_kartu" value="{{$kartu['nama_kartu']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Nama sistem</label>
			    <input type="nama" name="nama_sistem" value="{{$kartu['nama_sistem']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Nama instansi</label>
			    <input type="nama" name="nama_instansi" value="{{$kartu['nama_instansi']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Jabatan</label>
			    <input type="nama" name="jabatan" value="{{$kartu['jabatan']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Nama pejabat</label>
			    <input type="nama" name="nama_jabatan" value="{{$kartu['nama_jabatan']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Peraturan</label>
			    <textarea name="peraturan" class="form-control editor">
                    {{ old('peraturan') ?? $kartu['peraturan'] ?? '' }}
                </textarea>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputnama1">Nip pejabat</label>
			    <input type="nama" name="nip_jabatan" value="{{$kartu['nip_jabatan']}}" class="form-control" placeholder="Enter nama">
			  </div>
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="exampleFormControlFile1">Logo</label>
    			  <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="exampleFormControlFile1">Stempel</label>
    			  <input type="file" name="stempel" class="form-control-file" id="exampleFormControlFile1">
			    </div>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="exampleFormControlFile1">Cover</label>
    			  <input type="file" name="cover" class="form-control-file" id="exampleFormControlFile1">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="exampleFormControlFile1">Tanda tangan</label>
    			  <input type="file" name="ttd" class="form-control-file" id="exampleFormControlFile1">
			    </div>
			  </div>
			  
			  <button type="submit" class="btn btn-primary">Edit</button>
			</form>
		</div>
	</div>

</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">

	$(document).ready(function() {
		 tinymce.init({
	            selector: "textarea.editor",
	            branding: false,
	            width: "100%",
	            height: "350",
	            plugins: [
	                "advlist autolink lists charmap print preview anchor",
	                "searchreplace visualblocks code fullscreen",
	                "paste wordcount"
	            ],
	            toolbar: "undo redo | bold italic | bullist numlist outdent indent "
	      
	    });
	});
</script>

@endsection