@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 250px;
	}
</style>

<section class="container">

	<h4 class="mb-1">Daftar pengumuman
		<br>
		<small class="subtitle-section">daftar semua pengumuman</small>
	</h4>

	<button type="button" class="btn btn-info btn-sm" onclick="modalOpen()"></i>Tambah
     </button>

	<table class="table table-striped table-responsive" id="tabel-pengumuman" width="100%">
	    <thead>
	        <tr>
	            <th>No</th>
	            <th>Nama</th>
              	<th>Tanggal akhir</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>  
	                    
		</tbody>
	</table>

</section>

<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="form-add-master" action="javascript:void(0)" method="post">
			@csrf
			<input type="hidden" id="id" name="id"class="form-control">
    		<div class="form-group">
			    <label for="exampleInputnama1">Pengumuman</label>
			    <textarea name="nama" id="nama" class="form-control editor">
                </textarea>
			 </div>
			<div class="form-group">
			    <label for="exampleInputnama1">Tanggal selesai</label>
			    <input type="date" id="tgl" name="tgl_akhir" class="form-control">
			</div>
	    
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Tambah</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>

@include('includes.program-perpus.notify')
@include('includes.program-perpus.datatable')

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script type="text/javascript">

	$(document).ready(function() {
		 tinymce.init({
	            selector: "textarea.editor",
	            branding: false,
	            width: "100%",
	            height: "350",
	            plugins: [
	                "advlist autolink lists charmap print preview anchor",
	                "searchreplace visualblocks code fullscreen",
	                "paste wordcount"
	            ],
	            toolbar: "undo redo | bold italic | bullist numlist outdent indent "
	      
	    });
	});

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('#tabel-pengumuman');
	let url = "{{ route('pengumuman-index') }}";
	let column = [
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
	    {
	        data: 'nama',
	        name: 'nama',
	        width: "75%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'tgl_akhir',
	        name: 'tgl_akhir',
	        width: "10%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'aksi',
	        name: 'aksi',
	        width: "10%"
	    },
	];

	let buttons = [
      {
          extend: 'print',
          text: '<i class="fa fa-print"></i>',
          exportOptions: {
              columns: [ 0, 1, 2 ]
          },
          title : 'Daftar pengumuman'
      },
      {
          extend: 'excelHtml5',
          text: '<i class="fa fa-file-excel-o"></i>',
          messageTop: null,
          exportOptions: {
              columns: [ 0, 1, 2 ]
          },
          title : 'Daftar pengumuman'
      },

      {
          extend: 'pdfHtml5',
          text: '<i class="fa fa-file-pdf-o"></i>',
          messageBottom: null,
          exportOptions: {
              columns: [ 0, 1, 2 ]
          },
          customize: function(doc) {
              doc.content[1].table.widths =
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
          },
          title : 'Daftar pengumuman'
      },
      {
          text: '<i class="fa fa-refresh"></i>',
          action: function(e, dt, node, config) {
              dt.ajax.reload(null, false);
          }
      }
 	 ];

	MakeTable(tabel,url,column);

	function modalOpen(){
		$('#exampleModal').modal('show');
	}

	function edit_button(id){
		$.ajax({
        type : "get",
        url  : "{{route('pengumuman-detail')}}",
        data : {
        	'id' : id,
        },
        beforeSend: function() {
            $('.tabledit-edit-button-'+ id).html(
                '<i class="fa fa-spin fa-spinner"></i>');
        },
        success : (data) =>{
            //console.log(data);
            $('#exampleModal').modal('show');
            $('.tabledit-edit-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
            //$('#nama').html(data.data.nama);
            $('#tgl').val(data.data.tgl_akhir);
            $('#id').val(id);
            $(tinymce.get('nama').getBody()).html(data.data.nama);
        },
        error: function(data) {  
            $('.tabledit-edit-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
        } 
    	});
	}

	function submit_delete(id){
		$.ajax({
        type : "post",
        url  : "{{route('pengumuman-delete')}}",
        data : {
        	'id' : id,
        },
        beforeSend: function() {
            $('.tabledit-delete-button-'+ id).html(
                '<i class="fa fa-spin fa-spinner"></i>');
        },
        success : (data) =>{
        	noti(data.icon, data.message);
            $('#tabel-pengumuman').DataTable().ajax.reload();
        },
        error: function(data) {  
            $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
        } 
    	});
	}

	$('#form-add-master').on('submit',function(){
		tinyMCE.triggerSave();
		let data = $(this).serialize();
		let nama = $("#nama").val();
		console.log(data);
		console.log(data.id);
		console.log(data.tgl_akhir);
        
        $.ajax({
            type : "post",
            url  : "{{route('pengumuman-create')}}",
            data : {
            	'id' : $('#id').val(),
            	'tgl_akhir' : $('#tgl').val(),
            	'nama' : nama
            },
            success : (data) =>{
                noti(data.icon, data.message);
                $('#form-add-master').trigger("reset");
                $('#exampleModal').modal('hide');
                $('#tabel-pengumuman').DataTable().ajax.reload();
            },
            error: function(data) {
                noti(data.icon, data.message);  
                
            } 
        }); 

	});

</script>


@endsection