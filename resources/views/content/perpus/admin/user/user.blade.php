@extends('content.perpus.admin')
@section('content')

<style type="text/css">
	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

    #printArea{
        display: none;
    }

    .cetak-btn{
        margin-top: 5px;
        position: absolute;
        z-index: 5;
    }
</style>

<section class="container bg-white py-3 px-5">
	<h4 class="mb-1">Daftar User
	</h4>

    @if (Session::has('message'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                </button>
                <p class="mb-1">{{ Session::get('message') }}</p>
        </div>
    @endif

	<button type="button" class="btn btn-info btn-sm" onclick="modalCreate()">
        <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>

    <form action="{{ route('user-perpus-print-many') }}" method="POST">
        @csrf
    <div class="cetak-btn">
        <button type="submit" class="btn btn-info btn-sm mb-1">Cetak</button>
        {{-- <button onclick="cetak()" class="btn btn-primary btn-sm mb-1">Cetak</button> --}}
        <a onclick="check_all()" class="btn btn-primary text-white btn-sm mb-1">Check all</a>
    </div>


	<table class="table table-striped table-responsive" id="tabel-user" width="100%">
	    <thead>
	        <tr>
                <th>&nbsp</th>
	            <th>No</th>
	            <th>Nama</th>
	            <th>No induk</th>
	            <th>Anggota</th>
	            <th>Email</th>
	            <th>Aksi</th>
	        </tr>
	    </thead>
	    <tbody>

		</tbody>
	</table>
</form>
</section>

<div class="container" id="printArea">

</div>



@include('includes.program-perpus.datatable')
@include('includes.program-perpus.notify')
@include('components.perpus.modal.user-modal')

<script type="text/javascript">

	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	          'Authorization': `Bearer {{Session::get('token')}}`
	      }
	  });

	let tabel = $('#tabel-user');
	let url = "{{ route('user-perpus-all') }}";
	let column = [
        {
			data: 'checkbox',
            name: 'checkbox',
            width: "5%"
		},
		{
			data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "5%"
		},
	    {
	        data: 'nama',
	        name: 'nama',
	        width: "20%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'no_induk',
	        name: 'no_induk',
	        width: "20%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'tipe_anggota',
	        name: 'tipe_anggota',
	        width: "10%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'email',
	        name: 'email',
	        width: "10%",
	        orderable :true,
	        searchable: true
	    },
	    {
	        data: 'aksi',
	        name: 'aksi',
	        width: "10%"
	    },
	];

	let buttons = [
              {
                  extend: 'print',
                  text: '<i class="fa fa-print"></i>',
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar user'
              },
              {
                  extend: 'excelHtml5',
                  text: '<i class="fa fa-file-excel-o"></i>',
                  messageTop: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  title : 'Daftar user'
              },

              {
                  extend: 'pdfHtml5',
                  text: '<i class="fa fa-file-pdf-o"></i>',
                  messageBottom: null,
                  exportOptions: {
                      columns: [ 0, 1, 2 ]
                  },
                  customize: function(doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  },
                  title : 'Daftar user'
              },
              {
                  text: '<i class="fa fa-refresh"></i>',
                  action: function(e, dt, node, config) {
                      dt.ajax.reload(null, false);
                  }
              }
          ];

	MakeFullTable(tabel,url,column);


	function modalCreate(){
		$('#modal-store-master').modal('show');
	}

	$('#modal-store-master').on('hidden.bs.modal', function () {
	    $(this).find('form').trigger('reset');
	});

	function edit_button(id){
		$.ajax({
			tipe : "post",
			url  : "{{ route('user-perpus-detail') }}",
			data : {
				'id' : id
			},
			beforeSend : ()=>{
				$('.tabledit-edit-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) => {
				console.log(data);
				$('#password').hide();
				$('#id').val(data.data.id);
				$('#nama').val(data.data.nama);
				$('#username').val(data.data.username);
				$('#alamat').val(data.data.alamat);
				$('#email').val(data.data.email);
				$('#role').val(data.data.role);
				$('#telepon').val(data.data.telepon);
				$('#no_induk').val(data.data.no_induk);
				$('#no_anggota').val(data.data.no_anggota);
				$('#tempat_lahir').val(data.data.tempat_lahir);
				$('#tgl_lahir').val(data.data.tgl_lahir);
				$('#tipe_anggota').val(data.data.id_tipe_anggota);
				$('.btn-action').html('Update');
				$('#modal-store-master').modal('show');
				$('.tabledit-edit-button-'+ id).html('<i class="fas fa-pencil-alt"><i>');
			}


		});
	}

	function detail_modal(id){
		$.ajax({
			tipe : "post",
			url  : "{{ route('user-perpus-detail') }}",
			data : {
				'id' : id
			},
			beforeSend : ()=>{
				$('.tabledit-detail-button-'+id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
			},
			success : (data) => {
				console.log(data.data);
				$('#modal-detail-user').modal('show');
				$('#modal-detail-body').html(`
					<div class="contact-details-profile pd-lr-30">
		                <div class="row">
		                    <div class="col-md-4 mt-1">
		                        <img src="${data.data.foto}" class="m-1" width="200px">
		                    </div>
		                    <div class="col-md-6 mt-1">
		                        <h6 class="text-dark text-uppercase">${data.data.nama}</h6>
		                        <p class="mr-t-0">${data.data.no_induk}</p>
		                    </div>
		                </div>
		                <!-- /.row -->
		                <hr class="mt-1">
		                <div class="row">
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Email</p>
		                        <h6 class="text-dark text-uppercase">${data.data.email}</h6>
		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Telepon</p>
		                        <h6 class="text-dark text-uppercase">${data.data.telepon}</h6>

		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Alamat</p>
		                        <h6 class="text-dark text-uppercase">${data.data.alamat}</h6>

		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Username</p>
		                        <h6 class="text-dark text-uppercase">${data.data.username}</h6>
		                    </div>
		                </div>
		                <!-- /.row -->
		                <hr class="mt-1">
		                <div class="row">
		                	<div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Role</p>
		                        <h6 class="text-dark text-uppercase">${data.data.no_anggota}</h6>
		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Role</p>
		                        <h6 class="text-dark text-uppercase">${data.data.role}</h6>
		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Anggota</p>
		                        <h6 class="text-dark text-uppercase">${data.data.tipe_anggota}</h6>
		                    </div>
		                    <div class="col-md-6 mt-1">
		                    	<p class="mr-t-0">Jenis Kelamin</p>
		                        <h6 class="text-dark text-uppercase">${data.data.jenkel}</h6>
		                    </div>
		                </div>
		                <!-- /.row -->
		                <hr class="border-0 mr-tb-50">
		            </div>
				`);
				$('.tabledit-detail-button-'+ id).html('<i class="fas fa-info-circle"><i>');

			}
		});
	}

	function submit_delete(id){
		$.ajax({
            type : "post",
            url  : "{{route('user-perpus-delete')}}",
            data : {
            	'id' : id
            },
            beforeSend: function() {
                $('.tabledit-delete-button-'+ id).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success : (data) =>{
                console.log(data);
                noti(data.icon, data.message);
                $('#tabel-user').DataTable().ajax.reload();
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            },
            error: function(data) {
                noti(data.icon, data.message);
                $('.tabledit-delete-button-'+ id).html('<i class="fas fa-trash-alt"><i>');
            }
        });

	}

    function check_all(){
        $('input[type="checkbox"]').prop("checked", true);
    }

    function cetak(){

        let items =`
            <style type="text/css">
                * {
                    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
                    color-adjust: exact !important;                 /*Firefox*/
                }

                h6,p{
                    margin-top: 0.25rem !important;
                    margin-bottom: 0.25rem !important;
                }

                img{
                    height: 50px;
                }

                .kartu-body{
                    display: flex;
                    background-color: white;
                    border-radius: 10px;
                    height: 250px;
                    width: 480px;
                    background-repeat:no-repeat;
                    background-size:cover;
                }

                    .kartu-left{
                        flex: 2;
                        padding: 30px 0 10px 0;
                        display: flex;
                        flex-direction: column;
                        justify-content: space-between;
                    }

                        .kartu-title{
                            display: flex;
                            flex-direction: column;
                            align-items: center;
                        }
                            .kartu-sistem{
                                margin-top: 10px;
                                font-size: 17px;
                                font-weight: 700;
                            }

                            .kartu-instansi{
                                font-size: 17px;
                            }

                        .kartu-identify{
                            display: flex;
                            flex-direction: column;
                            align-items: center;
                            margin-top: 20px;
                        }

                    .kartu-right{
                        flex: 1;
                        padding: 30px 0;
                        display: flex;
                        flex-direction: column;
                        justify-content: space-between;
                    }
                        .kartu-data{
                            align-items: center;
                            display: flex;
                            flex-direction: column;
                            align-items: center;
                        }

                        .kartu-user{
                            display: flex;
                            justify-content: center;
                        }

                .kartu-body-back{
                    position : relative;
                    display: flex;
                    flex-direction: column;
                    border-radius: 10px;
                    height: 250px;
                    width: 480px;
                    background-repeat:no-repeat;
                    background-size:cover;
                }
                    .kartu-head{
                        display: flex;
                        justify-content: flex-start;
                        align-items: center;
                        padding: 0;
                    }


                        .kartu-logo{
                            margin: 0 20px 0 20px;
                        }

                        .kartu-logo-back{
                            margin: 0 10px 0 20px;
                            height : 35px;
                            width : 35px;
                        }

                    .kartu-ket{
                        padding: 0 25px;
                    }

                        ol{
                            margin : 0 ;
                        }

                        ul{
                            padding: 0 20px !important;
                            margin-bottom: 0;
                        }

                    .kartu-foot{
                        display: flex;
                        justify-content: end;
                        padding: 0 40px;
                        margin: 0 0 -5px 0;
                    }

                    .kartu-footer{
                        display: flex;
                        justify-content: end;
                        padding: 0 10px;
                        margin: 5px;
                    }

                    .kartu-nama-ttd{
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                    }

                    .kartu-ttds{
                        font-size: 11px;
                        margin-bottom: -5px;
                    }



                @media print{

                    body{
                        margin : 0;
                    }

                    #printArea{
                        display : block;
                    }

                    h4{
                        margin: 15px 0;
                    }

                    .kartu{
                        display: flex;
                        flex-direction : column;
                        margin: 0 2px;
                    }

                    .kartu-body,.kartu-body-back{
                        height: 205px;
                        width: 350px;
                    }

                    .kartu-body-back{
                        position : relative;
                    }



                    .kartu-head{
                        margin-bottom: 0;
                        padding-bottom: 0px;
                    }

                    #kartu-peraturan{
                        margin: 2px 0 0 0 !important;
                    }

                    .kartu-sistem{
                        margin-top: 10px;
                        font-size: 14px;
                        font-weight: 700;
                        text-align: center;
                    }

                    .kartu-instansi{
                        font-size: 14px;
                        text-align: center;
                    }

                    .kartu-user-tipe{
                        font-weight: bold;
                    }

                    .kartu-ket{
                        margin: 0 0 3px 0;
                    }

                    ol{
                        margin : 0 ;
                    }


                    ol li{
                        font-size: 11px;
                    }

                    .kartu-foot{
                        padding: 0 35px;

                    }

                    .kartu-ttds{
                        font-size: 8px;
                        margin-bottom: 0;
                    }

                    .kartu-bottom{
                        z-index : 10;
                        position : absolute;
                        bottom: 0;
                        right: 0;
                    }
                }
            </style>
        `;

        let label = '';
        let user = '';
        let kartu_back;

        let kode = [];
        $("input:checkbox[class=check_print]:checked").each(function() {
            kode.push($(this).val());
        });

        function PrintWindow() {
            var divContents = document.getElementById("printArea").innerHTML;
            var a = window.open('', '', 'height=1000, width=1200');
                a.document.write('<html>');
                a.document.write('<body>');
                a.document.write('<div class="main">');
                a.document.write(divContents);
                a.document.write('</div>');
                a.document.write('</body></html>');
                a.document.close();
                a.print();
        }

        function display(item){
            items += `
            <div style="height: 206px;width : 100%; display:flex; margin:3px 0">
                <!-- depan -->
                <div class="kartu">
                    <div class="kartu-body" style="background-image:url(${label.cover}) !important;">
                        <!-- left -->
                        <section class="kartu-left">
                            <div class="kartu-title">
                                <div class="kartu-logo">
                                <img src="${label.logo}">
                                </div>
                                <div class="kartu-sistem">${label.nama_sistem}</div>
                                <div class="kartu-instansi">${label.nama_instansi}</div>
                            </div>
                            <div class="kartu-identify">
                                <div>${item.nama}</div>
                                <div class="kartu-barcode">
                                    {!! DNS1D::getBarcodeHTML('${item.no_anggota}',"C128",0.8,24) !!}
                                </div>
                            </div>
                        </section>

                        <!-- right -->
                        <section class="kartu-right">
                            <div class="kartu-data">
                                <p class="kartu-user-tipe">${item.role}</p>
                                <p class="kartu-user-id">${item.no_anggota}</p>
                                <!-- <p>Masa berlaku</p> -->
                            </div>
                            <div class="kartu-user">
                                <img src="${item.foto}" style="height: 60px;">
                            </div>
                        </section>
                    </div>
                </div>
                ${kartu_back}

            </div>
            </div>
            `;

            console.log(kartu_back);
        }

        $.ajax({
            type : "get",
            url  : "{{route('kartu-get-one')}}",
            success : (data) =>{
               label = data.data;
               kartu_back = `
                <!-- belakang -->
                    <div class="kartu">
                        <div class="kartu-body-back" style="background-image:url('${label.cover}') !important;">
                            <div class="kartu-head">
                                <img class="kartu-logo-back" src="${label.logo}" style="height: 35px;">
                                <h4>${label.nama_sistem}</h4>
                            </div>
                            <div class="kartu-ket">
                                <p id="kartu-peraturan" style="margin: 5px;">Peraturan</p>
                                {!! '${label.peraturan}' !!}

                            </div>

                            <div class="kartu-bottom">
                                <div class="kartu-foot">
                                    <div class="kartu-ttd" style="position: absolute;">
                                        <img class="kartu-logos" src="${label.ttd_jabatan}" style="height: 35px;">
                                    </div>
                                    <div class="kartu-stempel">
                                        <img src="${label.stempel}" style="height: 35px;">
                                    </div>
                                </div>

                                <div class="kartu-footer">
                                    <div class="kartu-nama-ttd">
                                        <div class="kartu-ttds">${label.nama_jabatan}</div>
                                        <div class="kartu-ttds">${label.jabatan}</div>
                                        <div class="kartu-ttds">NIP. ${label.nip_jabatan}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               `;

               //user ajax
               $.ajax({
                    type : "post",
                    url  : "{{route('user-perpus-find-many')}}",
                    data : {
                        'id' : kode
                    },
                    success : (data) =>{
                        user = data.data;
                        console.log(user);
                        user.forEach(display);

                        $('#printArea').html(items);
                        PrintWindow();
                    },
                    error: function(data) {
                    }
                });

            },
            error: function(data) {
            }
        });







    }

</script>



@endsection

