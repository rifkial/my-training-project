@extends('content.perpus.admin')
@section('content')

	<div class="container">
		<div class="container d-flex flex-row justify-content-between align-items-center my-2">
			<h4 class="mb-1 mt-0">Cari nama user
			</h4>

			<form id="form-search-kembali" action="javascript:void(0)" method="post">
			@csrf
			  <input class="form-control" type="text" name="nama">
			  <button id="btn-search" type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>
			</form>
		</div>

		<div class="container">
			<table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Buku</th>
                  <th scope="col">Kode</th>
                  <th scope="col">Tanggal pinjam</th>
                  <th scope="col">Tanggal kembali</th>
                  <th scope="col">Status</th>
                  <th scope="col">Kembali</th>
                </tr>
              </thead>
              <tbody id="tabel-body">
                
              </tbody>
            </table>
		</div>

	</div>


@endsection