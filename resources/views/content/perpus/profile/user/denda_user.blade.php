@extends('content.perpus.app')
@section('content')

<style type="text/css">
	.carousel{
		display: none;
	}
</style>

<div class="container bg-white p-3 mb-5">
    <h5 class="mb-0">Denda berjalan : </h5>
    <h5>Rp. <?php echo number_format($total) ?></h5>
</div>

<div class="container bg-white p-3">
	<h5>Riwayat Denda Anda</h5>
	<table class="table table-striped table-responsive" data-toggle="datatables" id="tabel-denda">
        <thead>
            <tr>
                <th>No</th>
                <th>Buku</th>
                <th>Tanggal kembali</th>
                <th>Tanggal dikembalikan</th>
                <th>Hari terlambat</th>
                <th>Denda</th>
                <th>Petugas</th>
            </tr>
        </thead>
        <tbody>  
                        
		</tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
    </table>
</div>

<div class="modal modal-info fade bs-modal-lg" tabindex="-1" role="dialog" id="modal-detail-denda" aria-hidden="true" style="display: none">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            <h5 class="modal-title" id="myLargeModalLabel">Detail denda</h5>
	        </div>
	        <div id="modal-body-denda" class="modal-body">
	            
	        </div>
	        <div class="modal-footer py-1">
	            <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Tutup</button>
	        </div>
	    </div>
	</div>
</div>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script>

	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

	const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: true,
            paging: true,
            searching: true,
       };

	let tabel = $('#tabel-denda');
	let url = "{{ route('denda-user-perpus') }}";
 	let column = 
	    [
	        {
	            data: 'DT_RowIndex',
	            name: 'DT_RowIndex',
	            width: "5%"
	        },
            {
                data: 'buku',
                name: 'buku',
                width: "20%",
                orderable :true
            },
            {
                data: 'tanggal_kembali',
                name: 'tanggal_kembali',
                width: "10%",
                orderable :true
            },
	        {
	            data: 'tanggal_dikembalikan',
	            name: 'tanggal_dikembalikan',
	            width: "10%",
	            orderable :true
	        },
            {
                data: 'hari_terlewat',
                name: 'hari_terlewat',
                width: "5%",
                orderable :true
            },
	        {
	            data: 'denda',
	            name: 'denda',
	            width: "10%",
	            orderable :true,
	        },
	        {
	            data: 'nama_petugas',
	            name: 'nama_petugas',
	            width: "20%"
	        }
	    ];


    tabel.DataTable({
        ...konfigUmum,
        dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
        ajax: {
            "url": url,
            "method": "GET"
        },
        columns: column,
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var totalHarga = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
            }, 0 );

            $( api.column( 4 ).footer() ).html('Total : ');
            $( api.column( 5 ).footer() ).html(totalHarga.toLocaleString());
        },
        columnDefs:
            [
                {
                    targets: 5,
                    render: $.fn.dataTable.render.number(',', '.', 0, '')
                },
        ]
    });


    MakeTable(tabel,url,column);

    function modalDetailDenda(id){
    	$.ajax({
    		type : "POST",
    		url	 : "{{ route('denda-detail-perpus')}}",
    		data : {
    			'id' : id
    		},
    		// beforeSend : function(){
    		// 	$('#btnInfo').html(
      //               '<i class="fa fa-spin fa-spinner"></i>'
      //           );
    		// },
    		success : function(data){
    			console.log(data);
    			$('#modal-detail-denda').modal('show');
    			$('#modal-body-denda').html(`
                    <div class="row">
                        <div class="col-4">
                            <h6>Nama petugas</h6>
                            <h6>Buku</h6>
                            <h6>Kode buku</h6>
                            <h6>Tanggal pinjam</h6>
                            <h6>Tanggal kembali</h6>
                            <h6>Tanggal dikembalikan</h6>
                            <h6>Keterangan</h6>
                            <h6>Denda</h6>
                            <h6>Hari terlewat</h6>
                        </div>
                        <div class="col-8">
                            <h6>: ${data.data.nama_petugas}</h6>
                            <h6>: ${data.data.buku}</h6>
                            <h6>: ${data.data.kode_buku}</h6>
                            <h6>: ${data.data.tanggal_pinjam}</h6>
                            <h6>: ${data.data.tanggal_kembali}</h6>
                            <h6>: ${data.data.tanggal_dikembalikan}</h6>
                            <h6>: ${data.data.keterangan}</h6>
                            <h6>: ${data.data.denda}</h6>
                            <h6>: ${data.data.hari_terlewat}</h6>
                            
                        </div>
                    </div>
                `);
    		}
    	});
    }

</script>


@endsection