@extends('content.perpus.app')

@section('carousel')
	@include('components.perpus.carousel.carousel')
@endsection

@section('content')
	<style type="text/css">
		#img-banner{
			height: 200px;
		}
	</style>
	<div class="container mb-5 pb-5">
		<div class="row px-4">
			@if(isset($search) && !empty($search))
				<h4 class="mb-4">Hasil pencarian : {{$search}}</h4>
			@else
				<h4 class="mb-4">Hasil pencarian Anda</h4>
			@endif

			@if(empty($bukus) && empty($pusdigs))
				<h5>Maaf,item Anda cari tidak ada</h5>
			@else
				<h6>Pustaka</h6>
				@include('components.perpus.buku.list')
				<h6>Pustaka digital</h6>
				@include('components.perpus.pusdig.list')
			@endif


		</div>
	</div>
@endsection
