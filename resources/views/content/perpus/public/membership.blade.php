@extends('content.perpus.app')

@section('content')
	<div class="container mb-5 px-3">
		<h5>Membership</h5>
        <div class="row">
            @foreach($data as $dt)
                <div class="col-sm-4 mr-b-20">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{$dt['nama']}}</h5>
                            <p class="card-text">Member {{$dt['nama']}} memiliki lama masa keanggotaan {{$dt['masa_keanggotaan']}} hari dengan maksimal jumlah pinjaman sebanyak {{$dt['jumlah_pinjaman']}} kali. Sekali peminjaman memiliki waktu pinjam {{$dt['lama_pinjaman']}}, dan memiliki denda perhari Rp.{{$dt['denda_perhari']}}  </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>



	</div>

		
@endsection