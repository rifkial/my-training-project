@extends('content.perpus.app')

@section('content')
	<div class="container mb-5 bg-white px-3">
		<div class="row flex-column">

			<div class="col-md-6 px-4">
                <div class="user-card user-card-default">
                    <div class="user-card-details">
                        <h4>{{$pengarang['nama']}}</h4><span class="user-role">penulis</span>
                        <hr>
                        <p>{{$pengarang['biodata']}}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12 pb-4">
            	<h5>Daftar buku penulis</h5>
            	@include('components.perpus.buku.list')
            	@include('components.perpus.pusdig.list')
            </div>

		</div>
	</div>


@endsection
