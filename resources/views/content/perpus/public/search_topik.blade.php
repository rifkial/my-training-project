@extends('content.perpus.app')

@section('carousel')
	@include('components.perpus.carousel.carousel')
@endsection

@section('content')
	<style type="text/css">
		#img-banner{
			height: 200px;
		}
	</style>
	<div class="container mb-5">
		<div class="row justify-content-center">
			<h4 class="mb-4">Hasil pencarian : {{ ($_GET["nama"]) }}</h4>
			@include('components.perpus.buku.list')
		</div>
	</div>
@endsection