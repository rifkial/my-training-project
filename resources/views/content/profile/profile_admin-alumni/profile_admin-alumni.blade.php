@extends('content/profile/layout/profile')
@section('content_profile')
    <div>
        <form method="POST" action="{{ route('update-profile_tanpa_slug') }}" enctype="multipart/form-data"
            onsubmit="return loader()">
            @csrf
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Personal Info</h3>
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">USERNAME</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="username" value="{{ $data['username'] }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">NAMA</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Contact Info</h3>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">JENIS KELAMIN</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">AGAMA</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">TEMPAT LAHIR</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">TANGGAL LAHIR</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker"
                                value="{{ date('d-m-Y', strtotime($data['tgl_lahir'])) }}"
                                data-plugin-options="{'autoclose': true}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">EMAIL</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="email" class="form-control" name="email" value="{{ $data['email'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">TELEPON</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" value="{{ $data['telepon'] }}" name="telepon">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">ALAMAT</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <textarea name="alamat" id="alamat" class="form-control"
                                rows="3">{{ $data['alamat'] }}</textarea>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function loader() {
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $(".btn-update").attr("disabled", true);
        }
    </script>
@endsection
