@extends('content/profile/layout/profile')
@section('content_profile')
    <div>
        <form class="form-horizontal" id="formUpdate" action="javascript:void(0)">
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Personal Info</h3>
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">NIP</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nip" value="{{ $data['nip'] }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h3 class="fieldset-title">Contact Info</h3>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">NIK</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nik" value="{{ $data['nik'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">NUPTK</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nuptk" value="{{ $data['nuptk'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Jenkel</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="l">Laki - laki</option>
                                <option value="p">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Agama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="hindu">Hindu</option>
                                <option value="budha">Budha</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tempat lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tanggal lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="email" class="form-control" name="email" value="{{ $data['email'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Telepon</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" value="{{ $data['telepon'] }}" name="telepon">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Alamat</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <textarea name="alamat" id="alamat" class="form-control"
                                rows="3">{{ $data['alamat'] }}</textarea>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        $('body').on('submit', '#formUpdate', function(e) {
            e.preventDefault();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $(".btn-update").attr("disabled", true);
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('update-profile_tanpa_slug') }}",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        location.reload(true);
                    }
                    noti(data.icon, data.message);
                    $('.btn-update').html('Update Profile');
                    $(".btn-update").attr("disabled", false);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('.btn-update').html('Update Profile');
                }
            });
        });
    </script>
@endsection
