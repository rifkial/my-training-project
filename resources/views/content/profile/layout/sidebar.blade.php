<style>
    @media (max-width: 767px) {
        .view-account .side-bar .user-info .img-profile {
            width: 60px !important;
            height: 60px !important;
        }
    }

</style>
<div class="side-bar">
    {{-- {{ dd($data) }} --}}
    <div class="user-info pb-0">
        <img class="img-profile img-circle img-responsive center-block" src="{{ $data['file'] }}" alt="" style="width: 100%">
        <ul class="meta list list-unstyled">
            <li class="name">{{ ucwords($data['nama']) }}
                <br><label class="label label-info" style="color: #6a6a70;">{{ ucwords(session('role')) }}</label>
            </li>
            <li class="email"><a href="#">{{ $data['email'] }}</a></li>
            <li class="activity">Last logged in:
                {{ $data['before_last_login'] != null ? (new \App\Helpers\Help())->getTanggalLengkap($data['before_last_login']) : 'Ini adalah pertama kalinya anda login' }}
            </li>
        </ul>
    </div>
    <nav class="side-menu">
        <ul class="nav">

            @if (session('role') != 'admin')
                <li class="{{ Request::segment(4) == 'edit' ? 'active' : '' }}">
                    <a href="{{ url('program/' . session('config') . '/profile/edit') }}"><span
                            class="fa fa-user"></span>
                        Profile</a>
                </li>
                <li class="{{ Request::segment(4) == 'v_change-password' ? 'active' : '' }}">
                    <a href="{{ url('program/' . session('config') . '/profile/v_change-password') }}"><span
                            class="fa fa-key"></span> Change
                        Password</a>
                </li>
                <li class="{{ Request::segment(4) == 'reset_password' ? 'active' : '' }}">
                    <a href="{{ url('program/' . session('config') . '/profile/reset_password') }}"><span
                            class="fa fa-unlock"></span> Reset
                        Password</a>
                </li>
                @if (session('role') == 'walikelas' || session('role') == 'supervisor')
                    <li class="{{ Request::segment(4) == 'reset_password' ? 'active' : '' }}">
                        <a href="{{ url('program/' . session('config') . '/profile/upload_paraf') }}"><span
                                class="fa fa-hand-holding-water"></span> Paraf</a>
                    </li>
                @endif


            @else
                <li class="{{ Request::segment(1) == 'edit' ? 'active' : '' }}">
                    <a href="{{ route('edit-profile_admin') }}"><span class="fa fa-user"></span> Profile</a>
                </li>
                <li class="{{ Request::segment(1) == 'v_change-password' ? 'active' : '' }}">
                    <a href="{{ route('view_change_password-admin') }}"><span class="fa fa-key"></span> Change
                        Password</a>
                </li>
                <li class="{{ Request::segment(1) == 'reset-password' ? 'active' : '' }}">
                    <a href="{{ route('reset_password-profile_admin') }}"><span class="fa fa-unlock"></span> Reset
                        Password</a>
                </li>
            @endif
        </ul>
    </nav>
</div>
