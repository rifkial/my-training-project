@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .form-material .form-group>label {
            top: 0%;
        }

    </style>
    <div class="row">
        <div class="col-12 col-md-4 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="contact-info">
                        <header class="text-center">
                            <div class="float-right dropdown"><a href="javascript:void(0);" data-toggle="dropdown"
                                    class="dropdown-toggle"><i
                                        class="material-icons list-icon text-color-scheme-dark">menu</i></a>
                                <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item"
                                        href="javascript:void(0);"><i
                                            class="material-icons list-icon float-left mr-r-10">date_range</i> History
                                    </a><a class="dropdown-item" href="javascript:void(0);"><i
                                            class="material-icons list-icon float-left mr-r-10">format_list_bulleted</i>
                                        Detailed logs </a>
                                    <a class="dropdown-item" href="javascript:void(0);"><i
                                            class="material-icons list-icon float-left mr-r-10">pie_chart_outlined</i>
                                        Statistics</a>
                                    <div role="separator" class="dropdown-divider"></div><a class="dropdown-item"
                                        href="javascript:void(0);"><i
                                            class="material-icons list-icon float-left mr-r-10">close</i> <strong>Clear
                                            list</strong></a>
                                </div>
                            </div>
                            <!-- /.dropdown -->
                            <div class="text-center">
                                <figure class="inline-block user--online thumb-lg">
                                    <img src="{{ $data['file'] }}" class="img-thumbnail" alt="">
                                </figure>
                            </div>
                            <h4 class="mt-1"><a href="#">{{ $data['nama'] }}</a> <span
                                    class="badge text-uppercase badge-warning align-middle">Pro</span></h4>
                            <div class="contact-info-address"><i class="material-icons list-icon">location_on</i>
                                <p>{{ $data['alamat'] }}</p>
                            </div>
                        </header>
                        <section class="padded-reverse">
                            <h5>User Status <small class="float-right">Role:
                                    <strong>{{ session('role') }}</strong></small></h5>
                            <div class="row text-center">
                                <div class="col-4"><span>14,563</span>
                                    <br><small>views</small>
                                </div>
                                <div class="col-4"><span>762</span>
                                    <br><small>likes</small>
                                </div>
                                <div class="col-4"><span>54</span>
                                    <br><small>comments</small>
                                </div>
                            </div>
                        </section>
                        <footer class="clearfix"><a href="#" class="btn btn-success btn-rounded"><i
                                    class="material-icons list-icon">done</i> Following</a> <a href="#"
                                class="float-right btn btn-default btn-rounded">Message</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="tabs mr-t-10">
                        @if ($pesan = Session::get('pesan'))
                            <div class="alert alert-icon alert-{{ $pesan['icon'] }} border-{{ $pesan['icon'] }}alert-dismissible fade show"
                                role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                                </button> <i class="material-icons list-icon"></i>
                                <center><strong>{{ $pesan['message'] }}</strong></center>
                            </div>
                        @endif

                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#profile-tab-bordered-1" class="nav-link active" data-toggle="tab"
                                    aria-expanded="true">Profile</a>
                            </li>
                            <li class="nav-item"><a href="#edit-tab-bordered-1" class="nav-link" data-toggle="tab"
                                    aria-expanded="true">Edit Detail</a>
                            </li>
                            <li class="nav-item"><a href="#password-tab-bordered-1" class="nav-link" data-toggle="tab"
                                    aria-expanded="true">Edit Password</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="profile-tab-bordered-1">
                                <div class="contact-details-profile pd-lr-30">
                                    <h4>Interested In</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Lives in</h6>
                                            <p class="mr-t-0">San Fransisco</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Major</h6>
                                            <p class="mr-t-0">Computer Engineering</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Year</h6>
                                            <p class="mr-t-0">2017</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Season</h6>
                                            <p class="mr-t-0">Spring</p>
                                        </div>
                                    </div>
                                    <hr class="mr-tb-50">
                                    <h4>Education Details</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Degree</h6>
                                            <p class="mr-t-0">San Fransisco</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Major</h6>
                                            <p class="mr-t-0">Computer Engineering</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">University</h6>
                                            <p class="mr-t-0">University of Southern Australia</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Scores</h6>
                                            <p class="mr-t-0">75.5% (3.40 GPA)</p>
                                        </div>
                                    </div>
                                    <hr class="mr-tb-50">
                                    <h4>Work Experience</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Worked At</h6>
                                            <p class="mr-t-0">Google Inc.</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Post</h6>
                                            <p class="mr-t-0">Senior System Engineer</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="text-muted text-uppercase">Duration</h6>
                                            <p class="mr-t-0">June 2015 - Aug 2017</p>
                                        </div>
                                    </div>
                                    <hr class="border-0 mr-tb-50">
                                </div>
                            </div>


                            <div class="tab-pane" id="edit-tab-bordered-1">
                                <form class="form-material pt-3" method="POST"
                                    action="{{ route('update-profile', Request::segment(2)) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="media align-items-center p-3 mb-4 bg-light-grey">
                                        <div class="d-flex mr-4 w-25 justify-content-end">
                                            <figure class="mb-0 thumb-md">
                                                <img id="modal-preview" alt="Preview" src="{{ $data['file'] }}"
                                                    class="img-thumbnail">
                                            </figure>
                                        </div>
                                        <div class="media-body w-75">
                                            <div class="form-group">
                                                <input type="file" id="exampleInputFile-btn" class="form-control-file"
                                                    type="file" name="image" accept="*" onchange="readURL(this);" />
                                                <label for="exampleInputFile">File input</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="nama" placeholder="Masukan Nama"
                                            class="form-control form-control-line" value="{{ $data['nama'] }}">
                                        <label>Nama</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                                        <input type="text" name="nip" placeholder="Masukan Nama"
                                            class="form-control form-control-line" value="{{ $data['nip'] }}">
                                        <label>NIP</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Masukan NIK"
                                                    class="form-control form-control-line" name="nik"
                                                    value="{{ $data['nik'] }}">
                                                <label for="example-email">NIK</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nuptk" placeholder="Masukan NUPTK"
                                                    class="form-control form-control-line" value="{{ $data['nuptk'] }}">
                                                <label>NUPTK</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="email" placeholder="email@site.com"
                                                    class="form-control form-control-line" name="email"
                                                    value="{{ $data['email'] }}" id="example-email">
                                                <label for="example-email">Email</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="jenkel" class="form-control form-control-line" id="jenkel">
                                                    <option value="l">Laki-laki</option>
                                                    <option value="p">Perempuan</option>
                                                </select>
                                                <label for="example-email">Jenkel</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="agama" class="form-control form-control-line" id="agama">
                                                    <option value="islam">Islam</option>
                                                    <option value="kristen">Kristen</option>
                                                    <option value="katolik">Katolik</option>
                                                    <option value="hindu">Hindu</option>
                                                    <option value="budha">Budha</option>
                                                </select>
                                                <label for="example-email">Agama</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-line" name="telepon"
                                                    value="{{ $data['telepon'] }}">
                                                <label for="example-email">Telepon</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-line" name="tahun_masuk"
                                                    value="{{ $data['tahun_masuk'] }}">
                                                <label for="example-email">Tahun Masuk</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-line"
                                                    name="tempat_lahir" value="{{ $data['tempat_lahir'] }}">
                                                <label for="example-email">Tempat Lahir</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-line datepicker"
                                                    name="tanggal_lahir" value="{{ $data['tanggal'] }}"
                                                    data-plugin-options='{"autoclose": true}'>
                                                <label for="example-email">Tanggal Lahir</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="3" class="form-control form-control-line"
                                            name="alamat">{{ $data['alamat'] }}</textarea>
                                        <label>Alamat</label>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="3" class="form-control form-control-line"
                                            name="informasi_lain">{{ $data['informasi_lain'] }}</textarea>
                                        <label>Informasi Lain</label>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success ripple">Update Profile</button>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="password-tab-bordered-1">
                                <div class="contact-details-profile pd-lr-30">
                                    <style>
                                        .pass_show {
                                            position: relative
                                        }

                                        .pass_show .ptxt {
                                            position: absolute;
                                            top: 50%;
                                            right: 10px;
                                            z-index: 1;
                                            color: #f36c01;
                                            margin-top: -10px;
                                            cursor: pointer;
                                            transition: .3s ease all;
                                        }

                                        .pass_show .ptxt:hover {
                                            color: #333333;
                                        }

                                    </style>
                                    <form method="post" id="passwordForm" style="position:relative">
                                        @csrf
                                        <label>Password Terdahulu</label>
                                        <div class="form-group pass_show">
                                            <input type="password" value="" name="current_password" class="form-control"
                                                placeholder="Password Terdahulu" required>
                                        </div>
                                        <label>Password baru</label>
                                        <div class="form-group pass_show">
                                            <input type="password" value="" class="form-control" id="new_password"
                                                name="new_password" placeholder="Password baru">
                                        </div>
                                        <label>Konfirmasi Password Baru</label>
                                        <div class="form-group pass_show">
                                            <input type="password" class="form-control" id="confirm_password"
                                                name="confirm_password" placeholder="Confirm Password">
                                        </div>
                                        <small id='message' style="position: absolute; top: 271px;"></small>
                                        <div class="btn-list dropdown d-none d-md-flex">
                                            <button class="btn btn-sm btn-primary dropdown-toggle ripple btn-save"><i
                                                    class="material-icons list-icon fs-24">https</i> Update Password
                                            </button>
                                        </div>
                                    </form>
                                    <hr class="border-0 mr-tb-50">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        })
        $('#passwordForm').on('submit', function(event) {
            console.log("tets");
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            $.ajax({
                url: "change-password",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $(".btn-save").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".btn-save").html(
                        '<i class="material-icons list-icon fs-24">https</i> Update Password');
                    noti(data.icon, data.success);

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }

    </script>

@endsection
