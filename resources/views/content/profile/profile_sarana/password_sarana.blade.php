@extends('template.template_default.app')
@section('content')


	<div class="container p-5 bg-white">
        <form id="formPassword" action="javascript:void(0)">
        	@csrf
            <fieldset class="fieldset">
                <h3 class="title-content mb-4">Ubah Password</h3>
            </fieldset>

            <fieldset class="fieldset">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label" name="current_password">Password Lama</label>
                    <input type="text" class="form-control" id="current_password">
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label" name="new_password">Password Baru</label>
                    <input type="text" class="form-control" id="new_password">
                </div>

                <div class="form-group">
                    <label for="message-text" class="col-form-label" name="confirm_password">Konfirmasi Password Baru</label>
                    <input class="form-control" id="confirm_password">
                </div>

                <button type="submit" id="btn-password" class="btn btn-primary">Ubah Password</button>
        </form>
    </div>


    <script>
    	$.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}
        });

        function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'bottom-right',
        });
        return true;
        }

    	$('body').on('submit', '#formPassword', function(e) {
                e.preventDefault();
                $("#btn-password").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btn-password").attr("disabled", true);
            
                let data = {
                	current_password :$("#current_password").val(),
                	new_password : $("#new_password").val() ,
                	confirm_password : $("#confirm_password").val()
                };

                $.ajax({
                    type: "POST",
                    url: "{{ route('change_password') }}",
                    data: data,
                    dataType: 'json',
                    success: (data) => {
                        swa(data.status + "!", data.success, data.icon);

                        $('#modal-password').modal("hide");
                        $('#btn-password').html('Password Profile');
                        $("#btn-password").attr("disabled", false);

                    },
                    error: function(data) {
                        swa(data.status + "!", data.success, data.icon);
                        $('#btn-password').html('Update Profile');
                    }
                });
        });

        $('#modal-password').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });

        </script>

@endsection