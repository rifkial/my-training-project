@extends('template/template_default/app')
@section('content')
    <style>
        #avatar {
            background-image: url('{{ $data['file'] }}');
            background-size: 200px 200px;
            height: 200px;
            width: 200px;
            border: 3px solid #0af;
            border-radius: 50%;
            transition: background ease-out 200ms;
        }

        #preview {
            position: relative;
        }

        input[type="file"] {
            display: none;
        }

        #upload-button {
            padding: 14px;
            width: 47px;
            height: 47px;
            border-radius: 50%;
            border: none;
            cursor: pointer;
            background-color: #08f;
            box-shadow: 0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%);
            transition: background-color ease-out 120ms;
            position: absolute;
            /* right: 9%; */
            bottom: 5%;
        }

        #upload-button:hover {
            background-color: #45a;
        }

    </style>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-2"></div>
        <div class="col-md-8 widget-holder col-md-offset-2">
            <div class="widget-bg mt-3">
                <h5 class="box-title"><i class="fa fa-user"></i> Profil Saya &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0)" onclick="tampilEdit()" id="aksi_hide"><i
                            class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0)" onclick="tampilPassword()" id="password_hide"><i class="fa fa-key"></i></a>
                </h5>
                <table id="data-profile" style="display: block">
                    <tr>
                        <td width="200px">Nama</td>
                        <td>:</td>
                        <td>{{ $data['nama'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $data['jenkel'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nomor Telepon</td>
                        <td>:</td>
                        <td>{{ $data['telepon'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ $data['email'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">Alamat</td>
                        <td style="vertical-align: top">:</td>
                        <td>{{ $data['alamat'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tempat, Tanggal lahir</td>
                        <td>:</td>
                        <td>{{ $data['tempat_lahir'] . ', ' . (new \App\Helpers\Help())->getTanggal($data['tgl_lahir']) }}
                        </td>
                    </tr>
                </table>

                <div id="page_edit" style="display: none">
                    <form action="javascript:void(0)" id="formProfile" enctype="multipart/form-data">
                        @csrf
                        <div class="row mr-b-50">
                            <div class="col-md-12 mb-3">
                                <label for="validationServer01">Gambar</label>
                                <input type="file" name="image" id="image" accept="image/*" />
                                <div id="preview">
                                    <div id="avatar"></div>
                                    <a href="javascript:void(0)" id="upload-button" aria-labelledby="image"
                                        aria-describedby="image">
                                        <i class="fa fa-upload" style="color: #fff"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationServer01">Nama</label>
                                <input type="hidden" name="id" value="{{ $data['id'] }}">
                                <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}" required>
                            </div>

                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Tempat Lahir</label>
                                            <input type="text" class="form-control" placeholder="Tempat Lahir"
                                                name="tempat_lahir" value="{{ $data['tempat_lahir'] }}" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">NIP</label>
                                            <input type="text" class="form-control" name="nip"
                                                value="{{ $data['nip'] }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">NUPTK</label>
                                            <input type="text" class="form-control" name="nuptk"
                                                value="{{ $data['nuptk'] }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Jenis Kelamin</label>
                                            <select name="jenkel" class="form-control" id="">
                                                <option value="l" {{ $data['jenkel'] == 'Laki-laki' ? 'selected' : '' }}>
                                                    Laki - laki
                                                </option>
                                                <option value="p" {{ $data['jenkel'] == 'Perempuan' ? 'selected' : '' }}>
                                                    Perempuan
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Telepon</label>
                                            <input type="text" class="form-control" name="telepon"
                                                value="{{ $data['telepon'] }}" placeholder="telepon">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Kecamatan</label>
                                            <input type="text" class="form-control" placeholder="telepon">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Tanggal Lahir</label>
                                            <input type="text" class="form-control form-control-line datepicker"
                                                name="tanggal_lahir" readonly value="{{ $data['tanggal'] }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">NIK</label>
                                            <input type="text" class="form-control" name="nik"
                                                value="{{ $data['nik'] }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Email</label>
                                            <input type="text" class="form-control" name="email"
                                                value="{{ $data['email'] }}" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Kota</label>
                                            <input type="text" class="form-control" placeholder="telepon">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Kode Pos</label>
                                            <input type="text" class="form-control" placeholder="telepon">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="l30">Agama</label>
                                            <select name="agama" class="form-control" id="">
                                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>
                                                    Islam
                                                </option>
                                                <option value="kristen"
                                                    {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>
                                                    Kristen
                                                </option>
                                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>
                                                    Hindu
                                                </option>
                                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>
                                                    Budha
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="l30">Alamat sekarang</label>
                                <input type="text" class="form-control" name="alamat" value="{{ $data['alamat'] }}"
                                    placeholder="alamat">
                            </div>
                            <div class="col-sm-12 btn-list">
                                <button type="submit" class="btn btn-success" id="btn-update"><i class="fa fa-refresh"></i>
                                    Update</button>
                                <a href="{{ url('program/bursa_kerja/profile/edit') }}" class="btn btn-info"
                                    id="btn-cancel">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="password_edit" style="display: none">
                    <form method="post" id="passwordForm">
                        <input type="password" class="input-lg form-control mb-3" name="password_current"
                            placeholder="Password terdahulu" autocomplete="off">
                        <input type="password" class="input-lg form-control" name="password1" id="password1"
                            placeholder="New Password" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="8char" class="fa fa-remove" style="color:#FF0004;"></span> 8 Characters Long<br>
                                <span id="ucase" class="fa fa-remove" style="color:#FF0004;"></span> One Uppercase Letter
                            </div>
                            <div class="col-sm-6">
                                <span id="lcase" class="fa fa-remove" style="color:#FF0004;"></span> One Lowercase
                                Letter<br>
                                <span id="num" class="fa fa-remove" style="color:#FF0004;"></span> One Number
                            </div>
                        </div>
                        <input type="password" class="input-lg form-control" name="password2" id="password2"
                            placeholder="Repeat Password" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-12">
                                <span id="pwmatch" class="fa fa-remove" style="color:#FF0004;"></span> Passwords Match
                            </div>
                        </div>
                        <input type="submit" class="col-xs-12 btn btn-info btn-load btn-sm"
                            data-loading-text="Changing Password..." value="Change Password">
                    </form>
                </div>

            </div>
        </div>


    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function tampilEdit() {
            $('#page_edit').show();
            $('#data-profile').hide();
            $('#aksi_hide').hide();
            $('#password_hide').hide();
        }

        function tampilPassword() {
            $('#password_edit').show();
            $('#data-profile').hide();
            $('#password_hide').hide();
            $('#aksi_hide').hide();
        }

        const UPLOAD_BUTTON = document.getElementById("upload-button");
        const FILE_INPUT = document.querySelector("input[type=file]");
        const AVATAR = document.getElementById("avatar");

        UPLOAD_BUTTON.addEventListener("click", () => FILE_INPUT.click());

        FILE_INPUT.addEventListener("change", event => {
            const file = event.target.files[0];

            const reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onloadend = () => {
                AVATAR.setAttribute("aria-label", file.name);
                AVATAR.style.background = `url(${reader.result}) center center/cover`;
            };
        });

        $('body').on('submit', '#formProfile', function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: "{{ url('program/bursa_kerja/profile/update') }}",
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("#btn-update").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#btn-update").attr("disabled", true);
                },
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        location.reload(true);
                    }
                    swa(data.status + "!", data.message, data.icon);
                    $('#btn-update').html('<i class="fa fa-refresh"></i> Update');
                    // noti(data.icon, data.success);
                    $("#btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btn-update').html('Simpan');
                }
            });
        });

        $("input[type=password]").keyup(function() {
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");

            if ($("#password1").val().length >= 8) {
                $("#8char").removeClass("fa-remove");
                $("#8char").addClass("fa-check");
                $("#8char").css("color", "#00A41E");
            } else {
                $("#8char").removeClass("fa-check");
                $("#8char").addClass("fa-remove");
                $("#8char").css("color", "#FF0004");
            }

            if (ucase.test($("#password1").val())) {
                $("#ucase").removeClass("fa-remove");
                $("#ucase").addClass("fa-check");
                $("#ucase").css("color", "#00A41E");
            } else {
                $("#ucase").removeClass("fa-check");
                $("#ucase").addClass("fa-remove");
                $("#ucase").css("color", "#FF0004");
            }

            if (lcase.test($("#password1").val())) {
                $("#lcase").removeClass("fa-remove");
                $("#lcase").addClass("fa-check");
                $("#lcase").css("color", "#00A41E");
            } else {
                $("#lcase").removeClass("fa-check");
                $("#lcase").addClass("fa-remove");
                $("#lcase").css("color", "#FF0004");
            }

            if (num.test($("#password1").val())) {
                $("#num").removeClass("fa-remove");
                $("#num").addClass("fa-check");
                $("#num").css("color", "#00A41E");
            } else {
                $("#num").removeClass("fa-check");
                $("#num").addClass("fa-remove");
                $("#num").css("color", "#FF0004");
            }

            if ($("#password1").val() == $("#password2").val()) {
                $("#pwmatch").removeClass("fa-remove");
                $("#pwmatch").addClass("fa-check");
                $("#pwmatch").css("color", "#00A41E");
            } else {
                $("#pwmatch").removeClass("fa-check");
                $("#pwmatch").addClass("fa-remove");
                $("#pwmatch").css("color", "#FF0004");
            }
        });

        $('#passwordForm').on('submit', function(event) {
            event.preventDefault();
            $(".btn-load").val('Silhakan tunggu...');
            $(".btn-load").attr("disabled", true);
            $.ajax({
                url: "{{ url('change-password') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.status == 'berhasil') {
                        location.reload(true);
                    }
                    noti(data.icon, data.success);
                    $('.btn-load').val('Change Password');
                    $(".btn-load").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

    </script>

@endsection
