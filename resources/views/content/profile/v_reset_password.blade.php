@extends('content/profile/layout/profile')
@section('content_profile')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">
            <h2>Forgot your password?</h2>
            <p>Reset password anda dengan mudah.</p>
            <ol class="list-unstyled">
                <li><span class="text-primary text-medium">1. </span>Klik dapatkan password untuk mereset password.</li>
                <li><span class="text-primary text-medium">2. </span>Sistem akan mereset password anda.</li>
                <li><span class="text-primary text-medium">3. </span>Setelah berhasil password akan di tampilkan di
                    kolom.</li>
            </ol>
            <form class="card mt-4">
                <div class="card-body">
                    <div class="form-group">
                        <label for="email-for-pass">Password baru anda akan tampil disini</label>
                        <input class="form-control" type="text" id="email-for-pass" readonly><small
                            class="form-text text-muted">Kolom ini akan berisi otomatis, setelah password anda berhasil
                            di reset.</small>
                    </div>
                </div>
                <div class="card-footer" style="background: #fff">
                    <a href="javascript:void(0)" class="btn btn-info" id="resetPassword" onclick="resetPassword()">Dapatkan
                        Password</a>
                </div>
            </form>
        </div>
    </div>
    <script>
        function resetPassword() {
            var confirmdelete = confirm(
                "Mereset data, berarti akan mengganti password anda sebelumnya. Apa kamu masih yakin?");
            if (confirmdelete == true) {
                $("#resetPassword").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#resetPassword").attr("disabled", true);
                $("#email-for-pass").attr("disabled", true);
                $.ajax({
                    url: "{{ route('reset_password') }}",
                    type: 'GET',
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $("#email-for-pass").val(data.data);
                        }
                        noti(data.icon, data.success);
                        $('#resetPassword').html(
                            'Dapatkan Password');
                        $("#resetPassword").attr("disabled", false);
                    }
                });
            }
        }

    </script>
@endsection
