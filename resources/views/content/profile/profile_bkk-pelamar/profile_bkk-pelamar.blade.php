@extends('content/profile/layout/profile_bkk')
@section('content_edit_profile')
    <div class="content-panel mx-0">
        <form class="form-horizontal" id="formUpdate" method="POST" action="{{ route('update-profile_tanpa_slug') }}"
            enctype="multipart/form-data">
            @csrf
            <fieldset class="fieldset">
                <h2 class="box-title">PERSONAL INFO</h2>
                <hr>
                <div class="form-group avatar">
                    <div class="row">
                        <figure class="figure col-md-2 col-sm-3 col-xs-12">
                            <img class="img-rounded img-responsive" id="modal-preview" src="{{ $data['file'] }}" alt="">
                        </figure>
                        <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                            <input type="file" class="file-uploader pull-left" name="image" accept="*"
                                onchange="readURL(this);">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Nama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="nama" value="{{ $data['nama'] }}">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <h2 class="box-title">CONTACT INFO</h2>
                <hr>

                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Jenkel</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="jenkel" id="jenkel" class="form-control">
                                <option value="l" {{ $data['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki</option>
                                <option value="p" {{ $data['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Agama</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam" {{ $data['agama'] == 'islam' ? 'selected' : '' }}>Islam</option>
                                <option value="kristen" {{ $data['agama'] == 'kristen' ? 'selected' : '' }}>Kristen</option>
                                <option value="hindu" {{ $data['agama'] == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="budha" {{ $data['agama'] == 'budha' ? 'selected' : '' }}>Budha</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tempat
                            lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="tempat_lahir"
                                value="{{ $data['tempat_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Tanggal
                            lahir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="date" class="form-control" name="tgl_lahir" value="{{ $data['tgl_lahir'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="email" class="form-control" name="email" value="{{ $data['email'] }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Telepon</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" value="{{ $data['telepon'] }}" name="telepon">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Pendidikan Akhir</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <select name="pendidikan_akhir" id="pendidikan_akhir" class="form-control">
                                <option value="">Pilih Pendidikan Akhir..</option>
                                <option value="sd" {{ $data['pendidikan_akhir'] == 'sd' ? 'selected' : '' }}>SD sederajat</option>
                                <option value="smp" {{ $data['pendidikan_akhir'] == 'smp' ? 'selected' : '' }}>SMP sederajat</option>
                                <option value="sma" {{ $data['pendidikan_akhir'] == 'sma' ? 'selected' : '' }}>SMA sederajat</option>
                                <option value="d3" {{ $data['pendidikan_akhir'] == 'd3' ? 'selected' : '' }}>D3</option>
                                <option value="s1" {{ $data['pendidikan_akhir'] == 's1' ? 'selected' : '' }}>S1</option>
                                <option value="s2" {{ $data['pendidikan_akhir'] == 's2' ? 'selected' : '' }}>S2</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2  col-sm-3 col-xs-12 control-label">Alamat</label>
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <textarea name="alamat" id="alamat" class="form-control"
                                rows="3">{{ $data['alamat'] }}</textarea>
                        </div>
                    </div>
                </div>
            </fieldset>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                        <button type="submit" class="btn btn-info btn-update">Update
                            Profile</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })
    </script>
@endsection
