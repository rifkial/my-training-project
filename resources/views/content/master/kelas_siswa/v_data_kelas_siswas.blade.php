@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .selectdiv {
            position: relative;

            /* float: left; */
            /* min-width: 200px;
      margin: 50px 33%; */
        }

        /* IE11 hide native button (thanks Matt!) */
        select::-ms-expand {
            display: none;
        }

        .selectdiv:after {
            content: '<>';
            font: 17px "Consolas", monospace;
            color: #333;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
            right: 380px;
            /*Adjust for position however you want*/

            top: 18px;
            padding: 0 0 2px;
            border-bottom: 1px solid #999;
            /*left line */

            position: absolute;
            pointer-events: none;
        }

        .selectdiv select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            /* Add some styling */

            display: block;
            width: 100%;
            max-width: 320px;
            height: 50px;
            float: right;
            margin: 5px 0px;
            padding: 0px 24px;
            font-size: 16px;
            line-height: 1.75;
            color: #333;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            -ms-word-break: normal;
            word-break: normal;
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <form class="form-inline" id="CustomerForm" style="margin-bottom: 12px; margin-top: 93px;">
                <div class="full" style="width: 100%">
                    <center>
                        <div class="selectdiv">
                            <label>
                                <select name="from">
                                    <option selected> Pilih Rombel </option>
                                    @foreach ($rombel as $item)
                                        <option value="{{ $item['id'] }}">
                                            {{ $item['nama'] . ' / ' . $item['kelas'] . ' / ' . $item['jurusan'] }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="col-sm-12">
                            <select id="siswa" class="form-control" name="siswa[]" data-toggle="multiselect"
                                multiple="multiple">
                            </select>
                        </div>
                    </center>
                </div>
                <div class="bawah" style="width: 100%">
                    <div class="form-group mr-2">
                        <label for="" class="form-label" style="margin-right: 12px">Pindahkan</label>
                        <div class="input-group">
                            {{-- <input class="form-control" id="exampleInputAmount" placeholder="Amount" type="text"> --}}
                            <div class="selectdiv">
                                <label>
                                    <select name="rombel">
                                        <option selected> Pilih Rombel </option>
                                        @foreach ($rombel as $item)
                                            <option value="{{ $item['id'] }}">
                                                {{ $item['nama'] . ' / ' . $item['kelas'] . ' / ' . $item['jurusan'] }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>



    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#siswa").bootstrapDualListbox({
                // see next for specifications
            });

            $('.md-select').on('click', function() {
                $(this).toggleClass('active')
            })

            $('.md-select ul li').on('click', function() {
                var v = $(this).text();
                $('.md-select ul li').not($(this)).removeClass('active');
                $(this).addClass('active');
                $('.md-select label button').text(v)
            })
            $('#rombel').change(function() {
                console.log($(this).data('rombel'));
                table.column($(this).data('column'))
                    .search($(this).val())
                    .draw();
            });

            $('select[name="from"]').on('change', function() {
                var id_rombel = $(this).val();
                if (id_rombel) {
                    $.ajax({
                        url: "{{ route('rombel-kelas_siswa') }}",
                        type: "POST",
                        data: {
                            id_rombel: id_rombel
                        },
                        success: function(data) {
                            $('#siswa').html("");
                            $.each(data, function(i, val) {
                                var opt = "<option value=\'" + val.id + "\'>" + val
                                    .nama + "</option>";

                                $("#siswa").append(opt);
                            });
                            $('#siswa').bootstrapDualListbox('refresh', true);
                        }
                    });
                }
            })

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                action_url = "{{ route('update-kelas_siswa') }}";
                method_url = "POST";
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset"); //form reset
                        $('#saveBtn').html('Simpan'); //tombol simpan
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



        });

    </script>
@endsection
