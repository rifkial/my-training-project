@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header bg-info">
                <h2 class="box-title mt-1 text-white">{{ Session::get('title') }}</h2>
            </div>
            <div class="card-body">
                <form id="formFilter" method="post">
                    <div class="row bg-success py-3 m-2 rounded">
                        <div class="col-md-12">
                            <h2 class="box-title mt-1 text-white">Filter Siswa</h2>
                            <hr class="text-white">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Asal Sekolah</label>
                                <select name="asal_data" id="asal_data" class="form-control">
                                    <option value="">Pilih Asal Data..</option>
                                    <option value="siswa">Siswa Baru</option>
                                    <option value="kelas" selected>Data Siswa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 filter_siswa">
                            <div class="form-group">
                                <label for="">Tahun Ajaran</label>
                                <select name="id_tahun" id="id_tahun" class="form-control">
                                    <option value="">Pilih Tahun Ajaran...</option>
                                    @foreach ($tahun as $thn)
                                        <option value="{{ substr($thn['nama'], 0, 4) }}">
                                            {{ $thn['tahun_ajaran'] . ' (' . $thn['semester'] . ')' }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 filter_siswa">
                            <div class="form-group">
                                <label for="">Jurusan</label>
                                <select name="id_jurusan" class="form-control" id="id_jurusan">
                                    <option value="">Pilih jurusan..</option>
                                    @foreach ($jurusan as $jr)

                                        <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">
                                            {{ $jr['nama'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 filter_siswa">
                            <div class="form-group">
                                <label for="">Kelas</label>
                                <select name="id_kelas" id="id_kelas" class="form-control" disabled>
                                    <option value="">Pilih Kelas..</option>
                                    @foreach ($kelas as $kl)
                                        <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">{{ $kl['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 filter_siswa">
                            <div class="form-group">
                                <label for="">Rombel</label>
                                <select name="id_rombel" id="id_rombel" class="form-control" disabled>
                                    <option value="">--- Pilih Rombel ---</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">&nbsp; &nbsp;</label>
                                <button type="submit" class="btn btn-info btn-block" id="btnSearch">Pencarian</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title">Data Siswa</h5>
                            <hr>
                            <div class="row mb-2">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button class="btn btn-info" id="import"><i class="fas fa-file-upload"></i>
                                            Import</button>
                                        <button class="btn btn-danger" id="deleteData"><i class="fas fa-trash-alt"></i>
                                            Hapus</button>
                                    </div>
                                </div>
                            </div>
                            <form id="moveForm" method="post">
                                <input type="hidden" name="temp_jurusan" id="temp_jurusan">
                                <input type="hidden" name="temp_kelas" id="temp_kelas">
                                <input type="hidden" name="temp_rombel" id="temp_rombel">
                                <input type="hidden" name="temp_tahun" id="temp_tahun">
                                <input type="hidden" name="temp_aksi" id="temp_aksi">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="select_all" value="1"
                                                        id="example-select-all">
                                                </th>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>NIS</th>
                                                <th>NISN</th>
                                                <th width="50"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="data_siswa">
                                            <tr>
                                                <td colspan="6" class="text-center">Harap pilih filter dahulu</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form id="importKelasSiswa" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-indo">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#asal_data').on('change', function() {
                var asal = $(this).val();
                if (asal == 'siswa') {
                    $('.filter_siswa').hide();
                } else {
                    $('.filter_siswa').show();
                }
            })

            $('#formFilter').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('kelas_siswa-load_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#btnSearch').html(
                            '<i class="fa fa-spin fa-spinner"></i> Mencari...');
                    },
                    success: function(data) {
                        $('#btnSearch').html('Pencarian');
                        $('#data_siswa').html(data.html);
                        if (data.aksi == 'siswa') {
                            $('#temp_aksi').val(data.aksi)
                        } else {
                            $('#temp_kelas').val(data.id_kelas);
                            $('#temp_rombel').val(data.id_rombel);
                            $('#temp_tahun').val(data.tahun);
                            $('#temp_jurusan').val(data.id_jurusan);
                            $('#temp_aksi').val(data.aksi);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });

            })

            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                if (id_jurusan) {
                    $('select[name="id_kelas"]').attr('disabled', 'disabled')
                    $('select[name="id_rombel"]').html('-- Pilih Rombel --');
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    var action_url = '';
                    if (id_jurusan != 0) {
                        action_url = "{{ route('get_kelas-jurusan') }}";
                        method_url = 'POST';
                    }

                    if (id_jurusan == 0) {
                        action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                        method_url = 'GET';
                    }
                    $.ajax({
                        url: action_url,
                        type: method_url,
                        data: {
                            id: id_jurusan
                        },
                        dataType: "json",
                        success: function(data) {
                            var s = '<option value="">---select---</option>';
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama +
                                    '</option>';

                            })
                            $('select[name="id_kelas"]').removeAttr('disabled')
                            $('select[name="id_kelas"]').html(s)
                        }
                    });
                } else {
                    $('select[name="id_kelas"]').attr("disabled", true);
                    $('#rombel').html('<option value="">--Pilih Rombel--</option>');
                    $('#rombel').attr("disabled", true);
                    rombels = 0;
                    table.ajax.reload().draw();
                }
            })

            $('#id_kelas').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="id_rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#id_rombel').html(
                                '<option value="">--Load data Rombel--</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#id_rombel').removeAttr('disabled')
                            $('#id_rombel').html(s);
                        }
                    });
                }
            })

            $('#example-select-all').click(function(e) {
                var table = $(e.target).closest('table');
                $('td input:checkbox', table).prop('checked', this.checked);
            });

            $(document).on('change', 'select[name="jurusan"]', function() {
                let id = $(this).val();
                if (id) {
                    $('select[name="kelas"]').attr('disabled', 'disabled');
                    $('select[name="rombel"]').html('-- Pilih Rombel --');
                    $('select[name="rombel"]').attr('disabled', 'disabled');
                    var action_url = '';
                    if (id_jurusan != 0) {
                        action_url = "{{ route('get_kelas-jurusan') }}";
                        method_url = 'POST';
                    }

                    if (id_jurusan == 0) {
                        action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                        method_url = 'GET'
                    }
                    $.ajax({
                        url: action_url,
                        type: method_url,
                        data: {
                            id
                        },
                        dataType: "json",
                        success: function(data) {
                            var s = '<option value="">---select---</option>';
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama +
                                    '</option>';

                            })
                            $('select[name="kelas"]').removeAttr('disabled')
                            $('select[name="kelas"]').html(s)
                        }
                    });
                }
            });
            $(document).on('change', 'select[name="kelas"]', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#rombel').html('<option value="">--Load data Rombel--</option>');
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#rombel').removeAttr('disabled')
                            $('#rombel').html(s);
                        }
                    });
                }
            });

            $('#moveForm').on('submit', function(event) {
                event.preventDefault();
                if ($('input[name="siswa[]"]:checked').length < 1 || $('.id_rombel').val() == '') {
                    alert('Rombel dan checkbox harus terisi');
                } else {
                    var id_kelas_siswa = [];
                    $("input:checkbox[class=manual_entry_cb]:checked").each(function() {
                        id_kelas_siswa.push($(this).val());
                    });
                    if (id_kelas_siswa.length > 0) {
                        let moveConfirm = confirm("Do you really want move class student?");
                        if (moveConfirm == true) {
                            if ($('#asal_data').val() == 'kelas') {
                                var aksi = 'update';
                            } else {
                                var aksi = 'tambah';
                            }
                            $.ajax({
                                url: "{{ route('update-kelas_siswa') }}",
                                type: 'post',
                                data: $(this).serialize() + '&aksi=' + aksi,
                                dataType: "json",
                                beforeSend: function() {
                                    $('#btnStart').html(
                                        '<i class="fa fa-spin fa-spinner"></i> Sedang memindah...'
                                    );
                                },
                                success: function(data) {
                                    $('#btnStart').html('Mulai Pindahkan');
                                    if (data.status == 'berhasil') {
                                        $('#data_siswa').html(data.data_siswa.original.html);
                                        $("#example-select-all").prop("checked", false);
                                    }

                                    swa(data.status + "!", data.message, data.icon);
                                },
                            });
                        }
                    }
                }
            })

            $('#deleteData').click(function() {
                var id_kelas_siswa = [];
                $("input:checkbox[class=manual_entry_cb]:checked").each(function() {
                    id_kelas_siswa.push($(this).val());
                });
                if (id_kelas_siswa.length > 0) {
                    var confirmdelete = confirm("Apa kamu yakin ingin menghapus data ini?");
                    if (confirmdelete == true) {
                        if ($('#asal_data').val() == 'kelas') {
                            var aksi = 'kelas_siswa';
                        } else {
                            var aksi = 'siswa';
                        }
                        $.ajax({
                            url: "kelas_siswa/soft_delete",
                            type: 'post',
                            data: {
                                id_kelas_siswa: id_kelas_siswa,
                                aksi: aksi,
                                temp_jurusan: $('#temp_jurusan').val(),
                                temp_tahun: $('#temp_tahun').val(),
                                temp_kelas: $('#temp_kelas').val(),
                                temp_rombel: $('#temp_rombel').val(),
                                temp_aksi: $('#temp_aksi').val()
                            },
                            beforeSend: function() {
                                $("#deleteData").html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                                $("#deleteData").attr("disabled", true);
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#data_siswa').html(data.data_siswa.original.html);
                                    $("#example-select-all").prop("checked", false);
                                }
                                swa(data.status + "!", data.message, data.icon);
                                $('#deleteData').html(
                                    '<i class="fa fa-trash"></i>');
                                $("#deleteData").attr("disabled", false);
                            }
                        });
                    }
                }
            });
            
            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });
            
            $('body').on('submit', '#importKelasSiswa', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importKelasSiswa"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-kelas_siswa') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#importFile').trigger("reset");
                            $('#importModal').modal('hide');
                            location.reload()
                        }
                        $('#importBtn').html('Simpan');
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });
        })
    </script>
@endsection
