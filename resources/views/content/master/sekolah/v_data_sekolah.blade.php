@extends('template.template_horizontal_nav_icons.app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .search {
            position: relative;
            box-shadow: 0 0 40px rgba(51, 51, 51, .1)
        }

        .search input {
            height: 60px;
            text-indent: 25px;
            border: 2px solid #d6d4d4
        }

        .search input:focus {
            box-shadow: none;
            border: 2px solid #51d2b7
        }

        .search .fa-search {
            position: absolute;
            top: 20px;
            left: 16px
        }

        .search button {
            position: absolute;
            top: 5px;
            right: 5px;
            height: 50px;
            width: 110px;
        }


        .card::after {
            display: block;
            position: absolute;
            bottom: -10px;
            left: 20px;
            width: calc(100% - 40px);
            height: 35px;
            background-color: #fff;
            -webkit-box-shadow: 0 19px 28px 5px rgba(64, 64, 64, 0.09);
            box-shadow: 0 19px 28px 5px rgba(64, 64, 64, 0.09);
            content: '';
            z-index: -1;
        }

        a.card {
            text-decoration: none;
        }

        .card {
            position: relative;
            border: 0;
            border-radius: 0;
            background-color: #fff;
            -webkit-box-shadow: 0 12px 20px 1px rgba(64, 64, 64, 0.09);
            box-shadow: 0 12px 20px 1px rgba(64, 64, 64, 0.09);
        }

        .card {
            position: relative;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: .25rem;
        }

        .box-shadow {
            -webkit-box-shadow: 0 12px 20px 1px rgba(64, 64, 64, 0.09) !important;
            box-shadow: 0 12px 20px 1px rgba(64, 64, 64, 0.09) !important;
        }

        .ml-auto,
        .mx-auto {
            margin-left: auto !important;
        }

        .mr-auto,
        .mx-auto {
            margin-right: auto !important;
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .bg-white {
            background-color: #fff !important;
        }

        .ml-auto,
        .mx-auto {
            margin-left: auto !important;
        }

        .mr-auto,
        .mx-auto {
            margin-right: auto !important;
        }

        .d-block {
            display: block !important;
        }

        img,
        figure {
            max-width: 100%;
            height: auto;
            vertical-align: middle;
        }

        .card-text {
            padding-top: 12px;
            color: #8c8c8c;
        }

        .text-sm {
            font-size: 12px !important;
        }

        p,
        .p {
            margin: 0 0 16px;
        }

        .card-title {
            margin: 0;
            font-family: "Montserrat", sans-serif;
            font-size: 18px;
            font-weight: 900;
        }

        .pt-1,
        .py-1 {
            padding-top: .25rem !important;
        }

        .head-icon {
            margin-top: 18px;
            color: #FF4500
        }

    </style>
    <div class="row height d-flex justify-content-center align-items-center animated fadeInDown">
        <div class="col-md-8">
            <form action="javascript:void(0)" id="search_sekolah" name="search_sekolah">
                <div class="search"> <i class="fa fa-search"></i> <input type="text" name="nama" class="form-control"
                        placeholder="Masukan Nama Sekolah" required> <button type="submit"
                        class="btn btn-primary">Search</button>
                </div>
            </form>
        </div>
    </div>
    <section class="container pt-3 mb-3">
        <div class="row pt-5 mt-30" id="list_sekolah">
            <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                <a class="card" style="position: relative;" href="javascript:void(0)" onclick="addData()">
                    <div class="card-body text-center">
                        <i class="fa fa-plus-circle" style="font-size: 189px; color: #03a9f3;"></i>
                    </div>
                </a>
            </div>
            @foreach ($sekolah as $skl)
                <div class="col-lg-4 col-sm-6 mb-5 pb-5">
                    <div class="card" style="position: relative;">
                        <img src="{{ $skl['file'] }}" class="box-shadow mx-auto text-center" alt=""
                            style="width: 90px; height: 90px; margin-top: -45px;">
                        <div style="position: absolute; right: 7px; top: 7px;">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="javascript:void(0)" class="btn btn-info btn-sm edit"
                                    data-id="{{ $skl['id'] }}"><i class="fa fa-edit"></i></a>
                                <a href="javascript:void(0)" onclick="deleteData({{ $skl['id'] }})"
                                    class="btn btn-danger btn-sm delete-{{ $skl['id'] }}"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <div class="card-body text-center">
                            <h3 class="card-title pt-1">{{ $skl['nama'] }}</h3>
                            <p class="card-text text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip.</p>
                            <a class="text-sm text-uppercase font-weight-bold"
                                href="{{ url('super_admin/master/sekolah', $skl['id_code']) }}">Lihat Selengkapnya&nbsp;<i
                                    class="fe-icon-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">

                                <input type="hidden" name="id" id="id_sekolah">

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Sekolah</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">NPSN</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="npsn" name="npsn" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jenjang</label>
                                    <div class="col-sm-12">
                                        <select name="jenjang" id="jenjang" class="form-control">
                                            <option value="dasar">Dasar</option>
                                            <option value="menengah">Menengah</option>
                                            <option value="atas">Atas</option>
                                            <option value="kejuruan">Kejuruan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status Sekolah</label>
                                    <div class="col-sm-12">
                                        <select name="status_sekolah" class="form-control" id="">
                                            <option value="negeri">Negeri</option>
                                            <option value="swasta">Swasta</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">SK Pendirian</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="sk_pendirian" name="sk_pendirian"
                                            value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tangaal SK Pendirian</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="tgl_sk_pendirian"
                                            name="tgl_sk_pendirian" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Email</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="email" name="email" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Fax</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="fax" name="fax" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Website</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="website" name="website" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Domain</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="subdomain" name="subdomain" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Provinsi</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="provinsi" name="provinsi" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kabupaten</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kabupaten" name="kabupaten" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kecamatan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kelurahan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Alamat</label>
                                    <div class="col-sm-12">
                                        <textarea name="alamat" id="alamat" cols="30" rows="5"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" style="padding:15px">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="name" class=" control-label">RT</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="rt" name="rt" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="name" class=" control-label">RW</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="rw" name="rw" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kode Pos</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Logo</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <input type="hidden" name="hidden_image" id="hidden_image">
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function addData() {
            $('#saveBtn').val("create-Customer");
            $('#Customer_id').val('');
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah Data Sekolah");
            $('#ajaxModel').modal('show');
        }
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#search_sekolah').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('filter-sekolah') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        $('#list_sekolah').html(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-sekolah') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-sekolah') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            // var oTable = $('#data-tabel').dataTable();
                            // oTable.fnDraw(false);
                            $("#list_sekolah").load(" #list_sekolah");
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'sekolah/edit/' + id, function(data) {
                    // console.log(id);
                    $('#modelHeading').html("Edit Data Sekolah");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#id_sekolah').val(data.id);
                    $('#nama').val(data.nama);
                    $('#subdomain').val(data.subdomain);
                    $('#npsn').val(data.npsn);
                    $('#jenjang').val(data.jenjang).trigger('change');
                    $('#status_sekolah').val(data.status_sekolah);
                    $('#alamat').val(data.alamat);
                    $('#kode_pos').val(data.kode_pos);
                    $('#kelurahan').val(data.kelurahan);
                    $('#kecamatan').val(data.kecamatan);
                    $('#kabupaten').val(data.kabupaten);
                    $('#provinsi').val(data.provinsi);
                    $('#rt').val(data.rt);
                    $('#rw').val(data.rw);
                    $('#dusun').val(data.dusun);
                    $('#sk_pendirian').val(data.sk_pendirian);
                    $('#tgl_sk_pendirian').val(data.tgl_sk_pendirian);
                    $('#fax').val(data.fax);
                    $('#email').val(data.email);
                    $('#website').val(data.website);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "sekolah/delete/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $("#list_sekolah").load(" #list_sekolah");
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }


        // $('#createNewCustomer').click(function() {
        //     $('#saveBtn').val("create-Customer");
        //     $('#Customer_id').val('');
        //     $('#CustomerForm').trigger("reset");
        //     $('#modelHeading').html("Tambah Data Sekolah");
        //     $('#ajaxModel').modal('show');
        // });
    </script>
@endsection
