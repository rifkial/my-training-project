@extends('template/template_default/app')
@section('content')
    <script src="{{ asset('asset/js/jquery.bootstrap-duallistbox.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/bootstrap-duallistbox.min.css') }}">
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        table.dataTable tr th.select-checkbox.selected::after {
            content: "✔";
            margin-top: -11px;
            margin-left: -4px;
            text-align: center;
            text-shadow: rgb(176, 190, 217) 1px 1px, rgb(176, 190, 217) -1px -1px, rgb(176, 190, 217) 1px -1px, rgb(176, 190, 217) -1px 1px;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped mr-b-0 hover">
                            <thead>
                                <tr class="bg-purple">
                                    <th class="text-white">Nomer</th>
                                    <th class="text-white">Jurusan</th>
                                    <th class="text-white">Kelas</th>
                                    <th class="text-white">Rombel</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($kelas))
                                    <tr>
                                        <td colspan="5" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($kelas as $kls)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $kls['jurusan'] }}</td>
                                            <td>{{ $kls['kelas'] }}</td>
                                            <td>{{ $kls['nama'] }}</td>
                                            <td class="text-center">
                                                <button class="btn btn-sm btn-purple" data-toggle="collapse"
                                                    data-target="#kelas{{ $kls['id'] }}"><i
                                                        class="fas fa-eye"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="kelas{{ $kls['id'] }}">
                                                    <button class="btn btn-info btn-sm my-3 pull-right"
                                                        onclick="tambahGuru({{ $kls['id'] }})"><i
                                                            class="fas fa-plus-circle"></i>
                                                        Tambah Guru Pelajaran</button>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>No</th>
                                                                <th>Mapel</th>
                                                                <th>NIP</th>
                                                                <th>Guru</th>
                                                                <th>Semester</th>
                                                                <th class="text-center">Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="dataGuru{{ $kls['id'] }}">

                                                            @if (empty($kls['guru_pelajaran']))
                                                                <tr>
                                                                    <td class="text-center" colspan="6">Data saat ini
                                                                        tidak tersedia</td>
                                                                </tr>
                                                            @else
                                                                @php
                                                                    $nomer = 1;
                                                                @endphp
                                                                @foreach ($kls['guru_pelajaran'] as $gp)
                                                                    <tr>
                                                                        <td>{{ $nomer++ }}</td>
                                                                        <td>{{ $gp['mapel'] }}</td>
                                                                        <td>{{ $gp['nip'] != null ? $gp['nip'] : '-' }}
                                                                        </td>
                                                                        <td>{{ $gp['guru'] }}</td>
                                                                        <td>{{ $gp['tahun_ajaran'] . ' ' . $gp['semester'] }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-info btn-sm edit"
                                                                                data-id="{{ $gp['id'] }}"
                                                                                data-rombel="{{ $kls['id'] }}"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-danger btn-sm delete"
                                                                                data-id="{{ $gp['id'] }}"
                                                                                data-rombel="{{ $kls['id'] }}"><i
                                                                                    class="fas fa-trash-alt"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
            aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header text-inverse bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="modelHeading"></h5>
                    </div>
                    <form id="CustomerForm" class="form-horizontal">
                        @csrf
                        <div class="modal-body">
                            <span id="form_result"></span>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" id="id_guru_pelajaran" name="id">
                                    <input type="hidden" name="id_rombel" id="id_rombel">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-12 control-label">Tahun Ajaran</label>
                                        <div class="col-sm-12">
                                            <select name="id_tahun_ajaran" id="id_tahun_ajaran" class="form-control"
                                                required>
                                                <option value="">Pilih Tahun Ajaran..</option>
                                                @foreach ($tahun as $th)
                                                    <option value="{{ $th['id'] }}" {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                        {{ $th['tahun_ajaran'] . '  ' . $th['semester'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" id="list_select">
                                    <div class="row">
                                        <div class="col-sm-6 mb-2">
                                            <div class="form-group mb-1">
                                                <label for="name" class="col-sm-12 control-label">Guru</label>
                                                <div class="col-sm-12">
                                                    <select name="id_guru[]" id="id_guru" class="form-control" required>
                                                        <option value="">Pilih Guru..</option>
                                                        @foreach ($guru as $gr)
                                                            <option value="{{ $gr['id'] }}">
                                                                {{ $gr['nama'] . ' NIP ' . $gr['nip'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mb-2">
                                            <div class="form-group mb-1">
                                                <label for="name" class="col-sm-12 control-label">Mapel</label>
                                                <div class="col-sm-12">
                                                    <select name="mapel[]" id="mapel" class="form-control" required>
                                                        <option value="">Pilih Mapel..</option>
                                                        @foreach ($mapel as $mp)
                                                            <option value="{{ $mp['id'] }}">{{ $mp['nama'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12" id="show_select">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12 mx-3">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('content.admin.master.import')


        <script type="text/javascript">
            var url_import = "{{ route('import-guru_pelajaran') }}";
            $(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#CustomerForm').on('submit', function(event) {
                    $('#saveBtn').html('Sending..');
                    event.preventDefault();
                    var action_url = '';

                    if ($('#action').val() == 'Add') {
                        action_url = "{{ route('store-guru_pelajaran') }}";
                        method_url = "POST";
                    }

                    if ($('#action').val() == 'Edit') {
                        action_url = "{{ route('update-guru_pelajaran') }}";
                        method_url = "PUT";
                    }

                    let id_rombel = $('#id_rombel').val();

                    $.ajax({
                        url: action_url,
                        method: method_url,
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#CustomerForm').trigger("reset");
                                $('#ajaxModel').modal('hide');
                                $('#dataGuru' + id_rombel).html(data.guru);

                            }
                            noti(data.icon, data.message);
                            $('#saveBtn').html('Simpan');


                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Simpan');
                        }
                    });
                });


                $(document).on('click', '.edit', function() {
                    var string = $(this).data('id');
                    var id_guru = string[0];
                    var id_mapel = string[1];
                    var id_kelas = string[2];
                    $('#form_result').html('');
                    var loader = $(this);
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('edit-guru_pelajaran') }}",
                        data: {
                            id_guru: id_guru,
                            id_mapel: id_mapel,
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            $(loader).html(
                                '<i class="fa fa-pencil-square-o"></i> Edit');
                            $('select[name="kelas"]').removeAttr('disabled')
                            $('#modelHeading').html("Edit Data Guru Pelajaran");
                            $('#saveBtn').val("edit-user");
                            $('#id_guru').val(data.id_guru).trigger('change');
                            $('#mapel').val(data.id_mapel).trigger('change');
                            if (data.id_jurusan == null) {
                                $('.input_jurusan').hide();
                                $('#id_jurusan').val('').change();
                                $('a[data-toggle="pilihan_check"]').not('[data-title="Y"]')
                                    .removeClass('notActive').addClass(
                                        'active');
                                $('a[data-toggle="pilihan_check"][data-title="Y"]')
                                    .removeClass('active').addClass('notActive');
                            } else {
                                $('.input_jurusan').show();
                                $('a[data-toggle="pilihan_check"]').not('[data-title="N"]')
                                    .removeClass('notActive').addClass(
                                        'active');
                                $('a[data-toggle="pilihan_check"][data-title="N"]')
                                    .removeClass('active').addClass('notActive');
                            }
                            $('#id_jurusan').val(data.id_jurusan).trigger('change');
                            var id_jurusan = data.id_jurusan;
                            var id_kelas = data.id_kelas;
                            edit_kelas(id_jurusan, id_kelas);
                            edit_rombel(id_kelas, data.rombel)
                            $('#action_button').val('Edit');
                            $('#action').val('Edit');
                            $('#ajaxModel').modal('show');
                        }
                    });
                })

                $(document).on('click', '.delete', function() {
                    let id = $(this).data('id');
                    let id_rombel = $(this).data('rombel');
                    let loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        $.ajax({
                            url: "{{ route('trash-guru_pelajaran') }}",
                            type: "POST",
                            data: {
                                id, id_rombel
                            },
                            beforeSend: function() {
                                $(loader).html(
                                    '<i class="fa fa-spin fa-spinner"></i>');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#dataGuru' + id_rombel).html(data.guru);
                                } else {
                                    $(loader).html(
                                        '<i class="fas fa-trash-alt"></i>');
                                }
                                swa(data.status + "!", data.message, data.icon);
                            }
                        })
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    })
                })


                $('#import').click(function() {
                    $('#importModal').modal('show');
                    $('#file-chosen').html('No file choosen');
                    $('#HeadingImport').html('Import Data Guru Pelajaran');
                });


            })

            function tambahGuru(id) {
                $('#CustomerForm').trigger("reset");
                $('#id_rombel').val(id);
                $('#modelHeading').html("Tambah Guru Pelajaran");
                $('#show_select').html('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            }




            function deleteData(id_lots) {
                var id_guru = id_lots[0];
                var mapel = id_lots[1];
                var kelas = id_lots[2];
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "guru_pelajaran/trash",
                            type: "POST",
                            data: {
                                id_guru: id_guru,
                                mapel: mapel,
                                kelas: kelas
                            },
                            beforeSend: function() {
                                $(".delete-" + id_guru + mapel + kelas).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    data.message,
                                    'success'
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    'Proses Penghapusan Gagal :)',
                                    'error'
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
            }

            function template() {
                window.location.href = "{{ $url }}";
            }

            var i = 1;

            function tambahData() {
                i++;
                var select_opsi = $('#list_select').html();
                let delete_opsi =
                    '<div class="row mx-0"><div class="col-md-12 mx-0 mb-3"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                    i + '">Hapus Baris</a></div></div>'
                let seleksi = '<div id="row' + i + '">' + select_opsi + '' + delete_opsi + '<div>';
                $('#show_select').append(seleksi);
            }



            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
        </script>
    @endsection
