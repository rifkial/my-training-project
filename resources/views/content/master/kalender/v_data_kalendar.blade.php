@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        #popup {
            width: 100%;
            height: 100%;
            position: fixed;
            background: rgba(0, 0, 0, .7);
            top: 0;
            left: 0;
            z-index: 9999;
            visibility: hidden;
        }

        .window {
            width: 375px;
            height: 186px;
            background: #313a46;
            border-radius: 10px;
            position: relative;
            padding: 10px;
            box-shadow: 0 0 5px rgba(0, 0, 0, .4);
            text-align: center;
            margin: 15% auto;
            border: 2px solid #fff;
        }

        .close-button {
            font-size: 1.28571em;
            position: absolute;
            height: 2em;
            width: 2em;
            background-color: #313a46;
            opacity: 1;
            border: 2px solid #ffffff;
            text-shadow: none;
            color: #ffffff;
            border-radius: 50%;
            text-align: center;
            line-height: 1.83333em;
            position: absolute;
            top: -10px;
            right: -10px;
        }

        #popup:target {
            visibility: visible;
        }

        .swal2-modal.swal2-show {
            background: rgb(255 255 255 / 84%) !important;
        }

        @media (max-width: 960px) {
            .download {
                float: left !important;
            }

        }

    </style>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>{{ Session::get('title') }}</h5>
                </div>
                <div id="calendar"></div>

            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_jurusan">

                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Jurusan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                        {{-- <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close this</button> --}}
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxData" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-inline" action="#" method="post">
                                @csrf
                                <label for="" style="width: 140px;">Kelas yang terkoneksi</label>
                                <select class="form-control" multiple="multiple" data-toggle="select2"
                                    data-plugin-options='{"minimumResultsForSearch": -1}'
                                    style="display:inline; width:48%; margin-left: 12px;">
                                    <option selected="selected">Mustard</option>
                                    <option selected="selected">Ketchup</option>
                                    <option>Relish</option>
                                </select>
                                {{-- <input type="text" id="search" name="search" class="input-small form-control" placeholder="Pilih Data Kelas" style="display:inline; width:48%; margin-left: 12px;"> --}}
                                <button type="submit" class="btn btn-sm btn-success" style="margin-left: 15px;"><i
                                        class="fa fa-refresh"></i> Update</button>
                                <a href="#" class="btn btn-sm btn-primary" style="margin-left: 12px;"><i
                                        class="fa fa-share"></i> Tambah Data Kelas</a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: "kalendar",
                displayEventTime: true,
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    if (title) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                        $.ajax({
                            url: "kalendar/store",
                            data: 'title=' + title + '&start=' + start + '&end=' + end,
                            type: "POST",
                            success: function(data) {
                                noti('success', "Added Successfully");
                            }
                        });
                        calendar.fullCalendar('renderEvent', {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay,
                            },
                            true
                        );
                    }
                    calendar.fullCalendar('unselect');
                },
                eventDrop: function(event, delta) {
                    console.log(event);
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'kalendar/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id,
                        type: "PUT",
                        success: function(response) {
                            noti('success', "Updated Successfully");
                            // displayMessage("Updated Successfully");
                        }
                    });
                },
                eventClick: function(event) {
                    var deleteMsg = confirm("Do you really want to delete?");
                    if (deleteMsg) {
                        $.ajax({
                            type: "DELETE",
                            url: 'kalendar/trash',
                            data: "&id=" + event.id,
                            success: function(response) {
                                if (response['success'] == true) {
                                    // console.log("tes");
                                    $('#calendar').fullCalendar('removeEvents', event.id);
                                    noti('warning', "tanggal berhasil dihapus");
                                }
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
