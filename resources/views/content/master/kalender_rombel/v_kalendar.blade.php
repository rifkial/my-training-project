@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

    </style>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading clearfix">
                    <h5>{{ Session::get('title') }}</h5>
                </div>
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form action="javascript:void(0)" id="formUpdateKalender">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="" class="form-label mb-0">Judul Pengumuman</label>
                                <input type="text" name="title" id="title" class="form-control">
                            </div>
                            <div class="col-md-9 mt-2">
                                <label for="" class="form-label mb-0">Isi dari Pengumuman</label>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" id="action" value="add">
                                <input type="hidden" name="id" id="id_kalender">
                                <input type="hidden" name="start" id="start">
                                <input type="hidden" name="end" id="end">
                                <textarea name="isi" id="isi" rows="6" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-update"><i class="fa fa-pencil-square"></i>
                            Update Pengumuman</button>
                        <a href="javascript:void(0)" id="deleteKalender" onclick="deleteKalender()"
                            class="btn btn-danger"><i class="fa fa-exclamation-triangle"></i> Hapus pengumuman</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var calendar = $('#calendar').fullCalendar({
                    events: function(start, end, timezone, callback) {
                        $.ajax({
                            url: "{{ route('e-learning-kalender_rombel') }}",
                            type: "GET",
                            success: function(obj) {
                                var events = [];
                                $.each(obj, function(index, value) {
                                    events.push({
                                        id: value['id'],
                                        start: value['tgl_mulai'],
                                        end: value['tgl_akhir'],
                                        title: value['judul'],
                                        isi: value['isi'],
                                        id_rombel: value[
                                            'id_rombel'],
                                    });
                                });
                                callback(events);
                            }

                        });
                    },
                    displayEventTime: false,
                    eventColor: '#2471d2',
                    eventTextColor: '#FFF',
                    editable: true,
                    eventRender: function(event, element, view) {
                        if (event.isi != null) {
                            element.children().last().append(
                                "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Isi : " +
                                event
                                .isi + "</span>");
                        }
                        if (event.allDay === 'true') {
                            event.allDay = true;
                        } else {
                            event.allDay = false;
                        }
                    },
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end, allDay) {
                        var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        $('#formUpdateKalender').trigger("reset");
                        $('#action').val('add');
                        $('#deleteKalender').hide();
                        $('.btn-update').html('<i class="fa fa-plus-circle"></i> Tambah Pengumuman');
                        $('#modelHeading').html('Tambah Pengumuman');
                        $('#start').val($.fullCalendar.formatDate(start,
                            "Y-MM-DD HH:mm:ss"));
                        $('#end').val($.fullCalendar.formatDate(end,
                            "Y-MM-DD HH:mm:ss"));
                        $('#ajaxModel').modal('show');
                    },
                    eventDrop: function(event, delta) {
                        var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                        $.ajax({
                            url: '/admin/master/kalender_rombel/update',
                            data: 'title=' + event.title + '&start=' + start + '&end=' +
                                end + '&id=' + event.id + '&isi=' + event.isi,
                            type: "PUT",
                            success: function(data) {
                                noti(data.icon, data.success)
                            }
                        });
                    },
                    eventClick: function(event, jsEvent, view) {
                        $('#informasi').html('');
                        $('#action').val('edit');
                        $('.btn-update').html('Update');
                        $('#deleteKalender').show();
                        $('#isi').val(event.isi);
                        $('#title').val(event.title);
                        $('#modelHeading').html('Edit Pengumuman');
                        $('.btn-update').html('<i class="fa fa-pencil-square"></i> Update Pengumuman');
                        $('#id_kalender').val(event.id);
                        $('#start').val($.fullCalendar.formatDate(event.start,
                            "Y-MM-DD HH:mm:ss"));
                        $('#end').val($.fullCalendar.formatDate(event.start,
                            "Y-MM-DD HH:mm:ss"));
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        $('#formUpdateKalender').on('submit', function(event) {
            event.preventDefault();
            $(".btn-update").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".btn-update").attr("disabled", true);
            var action_url = '';

            if ($('#action').val() == 'add') {
                action_url = "{{ route('store-kalender_rombel') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'edit') {
                action_url = "{{ route('update-kalender_rombel') }}";
                method_url = "PUT";
            }
            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    // console.log(data);
                    if (data.status == 'berhasil') {
                        $('#formUpdateKalender').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                    noti(data.icon, data.success);
                    $('.btn-update').html('Update');
                    $(".btn-update").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        function deleteKalender() {
            var id = $('#id_kalender').val();
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                })
                .then(
                    function() {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('trash-kalender_rombel') }}",
                            data: {
                                id
                            },
                            beforeSend: function() {
                                $("#deleteKalender").html(
                                    '<i class="fa fa-spin fa-spinner"></i>');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#ajaxModel').modal('hide');
                                    $('#calendar').fullCalendar('refetchEvents');
                                }
                                swa(data.status + "!", data.success, data.icon);
                                $("#deleteKalender").html('Delete');
                            }
                        });
                    },
                    function(dismiss) {
                        if (dismiss === 'cancel') {
                            swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    }
                );
        }
    </script>
@endsection
