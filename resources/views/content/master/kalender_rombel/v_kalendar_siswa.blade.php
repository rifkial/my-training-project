@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <style>
        .pace {
            display: none !important;
        }

        @media (min-width: 0) {
            .g-mr-15 {
                margin-right: 1.07143rem !important;
            }
        }

        @media (min-width: 0) {
            .g-mt-3 {
                margin-top: 0.21429rem !important;
            }
        }

        .g-height-50 {
            height: 50px;
        }

        .g-width-50 {
            width: 50px !important;
        }

        @media (min-width: 0) {
            .g-pa-30 {
                padding: 2.14286rem !important;
            }
        }

        .g-bg-secondary {
            background-color: #fafafa !important;
        }

        .u-shadow-v18 {
            box-shadow: 0 5px 10px -6px rgba(0, 0, 0, 0.15);
        }

        .g-color-gray-dark-v4 {
            color: #777 !important;
        }

        .g-font-size-12 {
            font-size: 0.85714rem !important;
        }

        .media-comment {
            margin-top: 20px
        }

        .page-heading {
            border-top: 0;
            padding: 0 10px 20px 10px;
        }

        .forum-post-container .media {
            margin: 10px 10px 10px 10px;
            padding: 20px 10px 20px 10px;
            border-bottom: 1px solid #f1f1f1;
        }

        .forum-avatar {
            float: left;
            margin-right: 20px;
            text-align: center;
            width: 110px;
        }

        .forum-avatar .img-circle {
            height: 48px;
            width: 48px;
        }

        .author-info {
            color: #676a6c;
            font-size: 11px;
            margin-top: 5px;
            text-align: center;
        }

        .forum-post-info {
            padding: 9px 12px 6px 12px;
            background: #f9f9f9;
            border: 1px solid #f1f1f1;
        }

        .media-body>.media {
            background: #f9f9f9;
            border-radius: 3px;
            border: 1px solid #f1f1f1;
        }

        .forum-post-container .media-body .photos {
            margin: 10px 0;
        }

        .forum-photo {
            max-width: 140px;
            border-radius: 3px;
        }

        .media-body>.media .forum-avatar {
            width: 70px;
            margin-right: 10px;
        }

        .media-body>.media .forum-avatar .img-circle {
            height: 38px;
            width: 38px;
        }

        .mid-icon {
            font-size: 66px;
        }

        .forum-item {
            margin: 10px 0;
            padding: 10px 0 20px;
            border-bottom: 1px solid #f1f1f1;
        }

        .views-number {
            font-size: 24px;
            line-height: 18px;
            font-weight: 400;
        }

        .forum-container,
        .forum-post-container {
            padding: 30px !important;
        }

        .forum-item small {
            color: #999;
        }

        .forum-item .forum-sub-title {
            color: #999;
            margin-left: 50px;
        }

        .forum-title {
            margin: 15px 0 15px 0;
        }

        .forum-info {
            text-align: center;
        }

        .forum-desc {
            color: #999;
        }

        .forum-icon {
            float: left;
            width: 30px;
            margin-right: 20px;
            text-align: center;
        }

        a.forum-item-title {
            color: inherit;
            display: block;
            font-size: 18px;
            font-weight: 600;
        }

        a.forum-item-title:hover {
            color: inherit;
        }

        .forum-icon .fa {
            font-size: 30px;
            margin-top: 8px;
            color: #9b9b9b;
        }

        .forum-item.active .fa {
            color: #1ab394;
        }

        .forum-item.active a.forum-item-title {
            color: #1ab394;
        }

        @media (max-width: 992px) {
            .forum-info {
                margin: 15px 0 10px 0;
                /* Comment this is you want to show forum info in small devices */
                display: none;
            }

            .forum-desc {
                float: none !important;
            }
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox.collapsed .fa.fa-chevron-up:before {
            content: "\f078";
        }

        .ibox.collapsed .fa.fa-chevron-down:before {
            content: "\f077";
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        .message-input {
            height: 90px !important;
        }

        .form-control,
        .single-line {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 6px 12px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 100%;
            font-size: 14px;
        }

        .text-navy {
            color: #1ab394;
        }

        .mid-icon {
            font-size: 66px !important;
        }

        .m-b-sm {
            margin-bottom: 10px;
        }

    </style>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="ibox-content m-b-sm border-bottom">
                        <div class="p-xs">
                            <div class="pull-left m-r-md mr-2">
                                <i class="fa fa-globe text-navy mid-icon"></i>
                            </div>
                            <h2>Pengumuman kelas</h2>
                            <span> Dapatkan pengumuman dan informasi kelas disini</span>
                        </div>
                    </div>

                    <div class="ibox-content forum-container p-2">

                        <div class="forum-title">
                            <div class="pull-right forum-desc">
                                <samll>Total posts: {{ count($kalender) }}</samll>
                            </div>
                            <h3>Pengumuman untuk anda</h3>
                        </div>

                        @foreach ($kalender as $kal)
                            <div class="forum-item active">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="forum-icon">
                                            <i class="fa fa-bullhorn"></i>
                                        </div>


                                        <a href="javascript:void(0)" onclick="detailKalender({{ $kal['id'] }})"
                                            class="forum-item-title">{{ $kal['judul'] }}</a>
                                        <div class="forum-sub-title">{{ substr($kal['isi'], 0, 60) }}...</div>
                                        {{-- <div class="forum-sub-title">{{ $kal['isi'] }}.</div> --}}
                                    </div>

                                    <div class="col-md-2 forum-info">
                                        <span class="views-number">
                                            Acara
                                        </span>
                                        <div>
                                            <small>{{ (new \App\Helpers\Help())->getDay($kal['tgl_mulai']) }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-2 forum-info">
                                        <span class="views-number">
                                            Diposting
                                        </span>
                                        <div>
                                            <small>{{ $kal['dibuat'] }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="modalKalender" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content p-0" style="background-color: #fafafa">
                <div class="text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
                    <div id="isi_pengumuman">
                        {{-- <div class="g-mb-15">
                            <h5 class="h5 g-color-gray-dark-v1 mb-0">John Doe</h5>
                            <span class="g-color-gray-dark-v4 g-font-size-12">5 days ago</span>
                        </div>
                        <h5 class="h5 g-color-gray-dark-v1 mb-0">John Doe</h5>
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                            Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc
                            ac nisi vulputate fringilla. Donec lacinia congue
                            felis in faucibus ras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>

                        <ul class="list-inline d-sm-flex my-0">
                            <li class="list-inline-item ml-auto">
                                <a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                                    <i class="fa fa-reply g-pos-rel g-top-1 g-mr-3"></i>
                                    Reply
                                </a>
                            </li>
                        </ul> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        function detailKalender(id) {
            // $('#action_button').val('Edit');
            // $('#action').val('Edit');
            // $('#modalKalender').modal('show');
            $.ajax({
                type: 'GET',
                url: "/admin/master/kalender_rombel/edit" + '/' + id,
                dataType: "json",
                beforeSend: function() {
                    $(".editData-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    // console.log(data);
                    $('#isi_pengumuman').html(data)
                    // $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                    // $('#modelHeading').html("Edit Data Jurusan");
                    // $('#saveBtn').val("edit-user");
                    // $('#id_jurusan').val(data.id);
                    // $('#nama').val(data.nama);
                    // $('#action_button').val('Edit');
                    // $('#action').val('Edit');
                    $('#modalKalender').modal('show');
                }
            });
        }
    </script>
@endsection
