@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <style>
        .pace {
            display: none;
        }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
        }

        .user-left .media-body p.mt-1.mb-2 {
            display: inline-block;
            background: #e7f1f6;
            padding: 1.07143em;
            border-radius: 10px;
        }

        .user-right .media-body p.mt-1.mb-2 {
            display: inline-block;
            background: #fff;
            padding: 1.07143em;
            border-radius: 10px;
            margin-right: 1.42857em;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: reverse;
            -ms-flex-direction: row-reverse;
            flex-direction: row-reverse;
            text-align: right !important;
        }

    </style>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="d-flex no-gutters pos-relative">
                        <div class="col-sm-12 col-md-9 chat-left">
                            <form name="send_chat" method="post" action="#" id="n_ekstra">
                                <input type="hidden" name="email_pengirim" id="email_pengirim" value="">
                                <div id="load_chat">

                                </div>
                            </form>
                            {{-- <header class="d-flex justify-content-between"><a href="#"
                                    class="btn btn-sm btn-secondary">Back</a>
                                <h4 class="my-0 sub-heading-font-family fw-400">Nick Lampard</h4><a href="#"
                                    class="btn btn-sm btn-secondary">View Profile</a>
                            </header> --}}

                            {{-- <footer class="d-flex mt-4 mx-3">
                                <a href="#" class="thumb-xs mr-2">
                                    <img class="rounded-circle" src="{{ asset('asset/demo/users/user1.jpg') }}" alt="">
                                </a>
                                <div class="form-group input-group mr-2">
                                    <input class="form-control" placeholder="Type your message..." type="text"> <a href="#"
                                        class="input-group-addon"><i class="fa fa-paperclip list-icon"></i></a>
                                </div>
                                <div><a href="#" class="btn btn-success px-4">Send</a>
                                </div>
                            </footer> --}}
                        </div>
                        <div class="col-md-3 d-none d-sm-block scrollbar-enabled">
                            <div class="chat-search">
                                <div class="form-input-icon">
                                    <select name="email_penerima" id="email_penerimas" class="select3"
                                        onchange="return view_chat()">
                                        <option value="">Pilih Penerima</option>
                                        @foreach ($user as $us)
                                            <option value="{{ $us['email'] }}">
                                                {{ $us['nama'] . ' ' . $us['kode'] . ' (' . $us['role'] . ')' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="chat-contact-list">
                                <ul class="list-unstyled widget-user-list">
                                    @foreach ($chats as $chat)
                                        <li class="media">
                                            <div class="d-flex mr-3">
                                                <a href="#" class="block user--online thumb-xs" onclick="newChat()">
                                                    <img src="{{ asset('asset/demo/users/user1.jpg') }}"
                                                        class="rounded-circle" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading mb-0 d-flex"><a href="#" class="mr-auto"
                                                        onclick="newChat()">{{ $chat['email'] }}</a>
                                                    <span class="text-muted mr-4 fw-300 fs-14 d-none d-sm-block">3:27
                                                        pm</span>
                                                </h5><small class="mr-4">{{ $chat['pesan'] }}</small>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let email_penerima = 0;
        let my_email = "{{ $my_email }}";
        view_chat();

        function newChat() {
            console.log("id");
        }
        // })

        $('#email_penerimas').on('change', function() {
            email_penerima = $(this).val();
        })

        function view_chat() {
            // console.log(email_penerima);
            email = email_penerima
            console.log(email);
            if (email == 0) {
                $("#load_chat").html(
                    '<section style="height: auto; padding: 20px;"><h5 class="center" style="text-align: center">Silahkan pilih penerima terlebih dahulu, untuk memulai chat</h5></section>'
                );
            } else {
                // $("#load_chat").html(
                //     ''
                // );
                // $("#id_ekstra").val(id);

                // $("#load_nilai").html(
                //     '<center><i class="fa fa-spin fa-spinner"></i> Loading</center>'
                // );
                $.ajax({
                    url: "pesan/get_create",
                    type: "POST",
                    data: {
                        email,
                        my_email
                    },
                    success: function(data) {
                        console.log(data);
                        $("#load_chat").show('slow');
                        html =
                            '<header class="d-flex justify-content-between"><a href="#" class="btn btn-sm btn-secondary">Back</a> <h4 class="my-0 sub-heading-font-family fw-400">"' +
                            email +
                            '"</h4><a href="#" class="btn btn-sm btn-secondary">View Profile</a> </header> <ul class="list-unstyled pd-t-20 mr-l-20">';
                        // var i = 1;
                        if ($.trim(data)) {
                            $.each(data, function(k, v) {
                                if (v.email_pengirim == my_email) {
                                    html +=
                                        '<li class="media user-right"><div class="d-flex mr-3"><a href="#"><img class="rounded-circle" src="assets/demo/users/user1.jpg" alt=""> </a></div><div class="media-body"> <p class="mt-1 mb-2">"' +
                                        v.pesan +
                                        '"</p><small class="block">2:19 PM</small> </div></li>';
                                } else {
                                    html +=
                                        '<li class="media user-left"><div class="d-flex mr-3"><a href="#"><img class="rounded-circle" src="assets/demo/users/user1.jpg" alt=""> </a></div><div class="media-body"> <p class="mt-1 mb-2">"' +
                                        v.pesan +
                                        '"</p><small class="block">2:19 PM</small> </div></li>';
                                }

                            });
                        } else {
                            html += '<h5 style="text-align: center">History chat kosong</h5>';
                        }

                        html +=
                            '</ul><footer class="d-flex mt-4 mx-3"> <a href="#" class="thumb-xs mr-2"> <img class="rounded-circle" src="" alt=""> </a> <div class="form-group input-group mr-2"> <input class="form-control" placeholder="Type your message..." type="text"> <a href="#" class="input-group-addon"><i class="fa fa-paperclip list-icon"></i></a> </div> <div><a href="#" class="btn btn-success px-4">Send</a> </div> </footer>';
                        $("#load_chat").html(html);
                    },
                })

            }
            return false;
        }

    </script>
@endsection
