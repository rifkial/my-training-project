@extends('main.app')
@section('content')
    <section class="content" style="padding:15px .5rem">
        <div class="container">

            <div class="card">
                <div class="card-body">
                    <!-- MULAI TOMBOL TAMBAH -->
                    <a href="javascript:void(0)" class="btn btn-primary" id="createNewCustomer">Tambah Rombel</a>
                    <br><br>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-sm data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Kelas</th>
                                        <th>RombBel</th>
                                        <th>Periode</th>
                                        <th>Semester</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- AKHIR TABLE -->
                </div>
            </div>
        </div>
    </section>

    <!-- MULAI MODAL FORM TAMBAH/EDIT-->
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modelHeading"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="Customer_id" id="Customer_id">
                                <div class="form-group">
                                    <label>Pilih Kelas</label>
                                    <select class="form-control" style="width: 100%;" name="id_kelas" id="id_kelas">
                                        @foreach ($kelas as $item)
                                            <option value="{{ $item['id_kelas'] }}">{{ $item['nama_kelas'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Nama Rombongan Belajar</label>
                                    <input type="text" name="nama_rombel" class="form-control" id="nama_rombel">
                                </div>
                                <div class="form-group">
                                    <label>Pilih Periode</label>
                                    <select class="form-control select2" id="periode" style="width: 100%;" name="periode">
                                        <?php for ($i = date('Y'); $i >= date('Y') - 32; $i -= 1) {
                                        echo "<option value='$i'> $i </option>";
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Semester</label>
                                    <select class="form-control select2" style="width: 100%;" id="semester" name="semester">
                                        <option value="1">Ganjil</option>
                                        <option value="2">Genap</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-offset-2 col-sm-12">
                                <input type="hidden" name="action" id="action" value="Add" />
                                <input type="hidden" name="hidden_id" id="hidden_id" />
                                <button type="submit" class="btn btn-primary btn-block" id="saveBtn" value="create">Simpan
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- AKHIR MODAL -->

    <!-- MULAI MODAL KONFIRMASI DELETE-->

    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">PERHATIAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Jika menghapus Pegawai maka</b></p>
                    <p>*data pegawai tersebut hilang selamanya, apakah anda yakin?</p>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                        Data</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {


            $(document).ready(function() {
                console.log("ready!");
                Toast.fire({
                    icon: 'success',
                    title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
                })
            });

        });



        $(function() {

            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'nama_rombel',
                        name: 'nama_rombel'
                    },
                    {
                        data: 'periode',
                        name: 'periode'
                    },
                    {
                        data: 'semester',
                        name: 'semester'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Rombongan Belajar");
                $('#ajaxModel').modal('show');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('rombel.store') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('rombel.update') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset"); //form reset
                        $('#ajaxModel').modal('hide'); //modal hide
                        $('#saveBtn').html('Simpan'); //tombol simpan
                        var oTable = $('.data-table').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        Toast.fire({
                            icon: 'success',
                            title: data.success
                        })
                    },
                    error: function(data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'rombel/edit/' + id, function(data) {
                    console.log(data);
                    $('#modelHeading').html("Edit Data Rombongan Belajar");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#Customer_id').val(data.id_rombel);
                    $('#nama_rombel').val(data.nama_rombel);
                    // $('#id_kelas').val(data.id_kelas);
                    $("#id_kelas").val(data.id_kelas).trigger("change");
                    $('#semester').val(data.angka_semester).trigger("change");
                    $('#periode').val(data.periode).trigger("change");
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                });
            });
        });

    </script>

@endsection
