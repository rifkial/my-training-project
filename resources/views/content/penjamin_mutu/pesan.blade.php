@extends('template.template_default.app')
@section('content')

<style>
  #data-pesan{
    width: 100% !important;
  }

  .pace{
    display: none;
  }
</style>

<div class="container-fluid bg-white d-flex flex-row p-3">

  <section class="col-3">
    <span class="row justify-content-center">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-pesan">Buat Pesan</button>
    </span>
    
    <ul class="list-group">
      <li class="list-group-item">
        <a onclick="Inbox()">Pesan Masuk</a>
      </li>
      <li class="list-group-item">
        <a onclick="Send()">Pesan Keluar</a>
      </li>
    </ul>
  </section>

  <section class="col-9">
    <div class="table-responsive">
      <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pesan">
            <thead>
                <tr>
                    <th>Pengirim</th>
                    <th>Subject</th>
                </tr>
            </thead>
            <tbody>  
                                
            </tbody>
      </table>
    </div>
  </section>

</div>

<!-- modal pesan create-->
      <div class="modal fade" id="modal-pesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Buat Pesan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('buat-pesan') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                  <div class="form-group col-md-9">
                    <label for="inputState">Email</label>
                    <select name="email" id="inputState" class="form-control">
                      @foreach($dataUser as $User)
                      <option value="{{$User['email']}}">{{$User['email']}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">          
                  <input type="text" col="4" name="subject" class="form-control" placeholder="Subject">
                </div>

                <div class="form-group">
                  
                  <textarea type="text" col="4" name="isi" class="form-control" placeholder="Pesan"></textarea>
                </div>

                <div class="form-group">
                    <label for="inputNama" class="col-sm-5 col-form-label">Upload file : </label>
                    <div id="inputNama" class="col-sm-12">
                    <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*">                               
                    </div>
                </div>
 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->


<script type="text/javascript">

$(function(){

    $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
      });

      const konfigUmum = {
          responsive: true,
          serverSide: true,
          processing: true,
          ordering: true,
      };

      $('#data-pesan').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('main-pesan') }}",
                "method": "GET"
            },
            columns: [
                {
                    data: 'pengirim',
                    name: 'pengirim',
                    width: "10%"
                },
                {
                    data: 'subject',
                    name: 'subject',
                    width: "60%"
                }
            ]
      });

});

      function noti(tipe, value) {
          $.toast({
              icon: tipe,
              text: value,
              hideAfter: 5000,
              showConfirmButton: true,
              position: 'top-right',
          });
          return true;
      };

      let table = $('#data-pesan').DataTable();

      function Inbox(){
        console.log("inbox");
        $.ajax({
          type: "GET",
            url : "{{ route('main-pesan') }}",
            data : {
                jenis : 'inbox'
            },
            dataType: 'json',
            success : function(data){
                console.log(data);
                $('#data-pesan').DataTable().clear();
                $('#data-pesan').DataTable().rows.add(data).draw(); 
                $('#data-pesan').DataTable().columns.adjust();
                
            },
            error : function(data){
                console.log(data);
            }
        })
      }

      function Send(){
        console.log("send");
        $.ajax({
          type: "GET",
            url : "{{ route('main-pesan') }}",
            data : {
                jenis : 'send'
            },
            dataType: 'json',
            success : function(data){
                console.log(data);
                console.log(data);
                $('#data-pesan').DataTable().clear().draw();
                $('#data-pesan').DataTable().rows.add(data); 
                $('#data-pesan').DataTable().columns.adjust().draw();
            },
            error : function(data){
                console.log(data);
            }
        })
      }


</script>
@endsection