@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

	.input-group a{
	    color: #fff !important;
	 }

	body {
	    background-color: #f9f9fa
	}

	.padding {
	    padding: 3rem !important
	}

	.user-card-full {
	    overflow: hidden
	}

	.card {
	    border: none;
	    margin-bottom: 0;
	}

	.m-r-0 {
	    margin-right: 0px
	}

	.m-l-0 {
	    margin-left: 0px
	}

	.user-card-full .user-profile {
	    border-radius: 5px 0 0 5px
	}

	.bg-c-lite-green {
	    background: -webkit-gradient(linear, left top, right top, from(#f29263), to(#ee5a6f));
	    background: linear-gradient(to right, #ee5a6f, #f29263)
	}

	.user-profile {
	    padding: 20px 0
	}

	.card-block {
	    padding: 1.25rem
	}

	.m-b-25 {
	    margin-bottom: 25px
	}

	.img-radius {
	    border-radius: 5px
	}

	h6 {
	    font-size: 14px
	}

	.card .card-block p {
	    line-height: 25px
	}

	@media only screen and (min-width: 1400px) {
	    p {
	        font-size: 14px
	    }
	}

	.card-block {
	    padding: 1.25rem
	}

	.card-text p {
		line-height: 8px !important;
	}


	.b-b-default {
	    border-bottom: 1px solid #e0e0e0
	}

	.m-b-20 {
	    margin-bottom: 20px
	}

	.p-b-5 {
	    padding-bottom: 5px !important
	}

	.card .card-block p {
	    line-height: 25px
	}

	.m-b-10 {
	    margin-bottom: 0;
	}

	.text-muted {
	    color: #919aa3 !important
	}

	.b-b-default {
	    border-bottom: 1px solid #e0e0e0
	}

	.f-w-600 {
	    font-weight: 600
	}

	.m-b-20 {
	    margin-bottom: 20px
	}

	.m-t-40 {
	    margin-top: 20px
	}

	.p-b-5 {
	    padding-bottom: 5px !important
	}

	.m-b-10 {
	    margin-bottom: 10px
	}

	.m-t-40 {
	    margin-top: 20px
	}

	.user-card-full .social-link li {
	    display: inline-block
	}

	.user-card-full .social-link li a {
	    font-size: 20px;
	    margin: 0 10px 0 0;
	    -webkit-transition: all 0.3s ease-in-out;
	    transition: all 0.3s ease-in-out
	}

</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
		<h4 class="my-2 title-content">Daftar User</h4>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-create">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah User
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-user">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Role</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>



<!-- modal create user -->
	<div class="modal fade bd-example-modal-lg" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
          	<!-- modal header -->
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- modal body -->
            <div class="modal-body px-5">
              <form id="create-user" action="javascript:void(0)">
                @csrf
				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Nama</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="nama">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Nomor Induk</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="number" class="form-control" name="no_induk">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Password</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="password" class="form-control" name="password">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Email</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="email" class="form-control" name="email" >
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Role</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select name="role" id="role" class="form-control">
								<option value="admin">Admin</option>
								<option value="staf">Staf</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Jenkel</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select name="jenkel" id="jenkel" class="form-control">
								<option value="l">Laki - laki</option>
								<option value="p">Perempuan</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Agama</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select name="agama" id="agama" class="form-control">
								<option value="islam">Islam</option>
								<option value="kristen">Kristen</option>
								<option value="hindu">Hindu</option>
								<option value="budha">Budha</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Tempat lahir</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="tempat_lahir"
								>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Tanggal lahir</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="date" class="form-control" name="tgl_lahir"
								>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Telepon</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="number" class="form-control" name="telepon">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row align-items-center">
						<label class="col-md-3  col-sm-3 col-xs-12 control-label">Alamat</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input name="alamat" id="alamat" class="form-control"
								rows="3">
						
						</div>
					</div>
				</div>
            </div>
           <!--  end modal body -->
            <div class="modal-footer">
				<button type="submit" class="btn btn-success">Tambah</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->

<!-- modal edit user-->
      <div class="modal fade bd-example-modal-lg" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
          	<!-- modal header -->
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- modal body -->
            <div class="modal-body px-4">
              <form action="javascript:void(0)" id="edit-user">
                @csrf
                <div id="content-edit">
	               
	            </div>
	            <!-- end content edit -->
            </div>
           <!--  end modal body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->

<!-- modal edit pass-->
	<div class="modal fade" id="modal-edit-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          	<!-- modal header -->
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- modal body -->
            <div class="modal-body">
              <form action="javascript:void(0)" id="edit-user-pass">
                @csrf
                <div id="content-edit-pass">
	               
	            </div>
	            <!-- end content edit -->
            </div>
           <!--  end modal body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

          </div>
        </div>
    </div>
<!-- end modal -->

<!-- modal detail -->
      <div class="modal fade bd-example-modal-lg" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
             	<div class="" id="detailUser">

             	</div>
            </div>

          </div>
        </div>
      </div>
<!-- end modal detail -->

<!-- modal password -->
	<div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
            <div class="modal-body px-4">
            <form id="formPassword" action="javascript:void(0)">
                <div class="form-group">
                <label for="recipient-name" class="col-form-label" name="current_password">Password Lama</label>
                <input type="text" class="form-control" id="current_password" name="current_password">
                </div>

                <div class="form-group">
                <label for="recipient-name" class="col-form-label" name="new_password">Password Baru</label>
                <input type="text" class="form-control" id="new_password" name="new_password">
                </div>

                <div class="form-group">
                <label for="message-text" class="col-form-label" name="confirm_password">Konfirmasi Password Baru</label>
                <input class="form-control" id="confirm_password" name="confirm_password">
                </div>

            
            </div>
	      <div class="modal-footer">
			<button type="submit" id="btn-password" class="btn btn-success">Ubah Password</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
<!-- end modal password -->

<script>

$(function(){

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: false,
    };

    $('#data-user').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('user-mutu') }}",
                "method": "GET"
            },
            columns: [
            	{
                	data: 'DT_RowIndex',
					name: 'DT_RowIndex',
					width: "10%"
                },
                {
                    data: 'nama',
                  	name: 'nama',
                  	width: "60%"
                },
                {
                    data: 'role',
                  	name: 'role',
                  	width: "10%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
    });

});


    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    }

	function Detail(id){
		let data;
		let url = "{{ route('user-detail') }}";
		$("#modal-detail").modal("show");
		$.ajax({
                type: "GET",
                url : "{{ route('user-detail') }}",
                data : {
                	id : id
                },
                dataType: 'json',
				beforeSend: function() {
                  $('#detailUser').html(
                      '<i class="fa fa-spin fa-spinner"></i> Loading');
            	},
                success: function(data) {
					data = data.body.data;
					$("#detailUser").html(`
						<div class="row container d-flex justify-content-center p-0">
							<div class="col-12">
								<div class="card user-card-full">
									<div class="row m-l-0 m-r-0">
										<div class="col-sm-4 bg-c-lite-green user-profile">
											<div class="card-block card-text text-center text-white">
												<div class="m-b-25"> <img src="${data.file}" class="img-radius" alt="User-Profile-Image"> </div>
												<h4 class="f-w-600 text-white">${data.nama}</h4>
												<p>${data.no_induk}</p>
												<p>${data.jenkel}</p>  
												
												<i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
											</div>
										</div>
										<div class="col-sm-8">
											<div class="card-block">
												<span class="d-flex justify-content-between align-items-center">
													<h5 class="m-b-20 p-b-5 b-b-default f-w-600">Informasi</h5>
													<span>
													
													</span>
													
												</span>
												

													<div class="row">
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Email</p>
														<h6 class="text-muted f-w-400 mt-0">${data.email}</h6>
													</div>
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Phone</p>
														<h6 class="text-muted f-w-400 mt-0">${data.telepon}</h6>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Agama</p>
														<h6 class="text-muted f-w-400 mt-0">${data.agama}</h6>
													</div>
													
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Role</p>
														<h6 class="text-muted f-w-400 mt-0">${data.role}</h6>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Tempat,tanggal lahir</p>
														<h6 class="text-muted f-w-400 mt-0">${data.tempat_lahir},${data.tgl_lahir}</h6>
													</div>
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Alamat</p>
														<h6 class="text-muted f-w-400 mt-0">${data.alamat}</h6>
													</div>
													
												</div>

												<h5 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600 mb-2">Data Akses</h5>
												<div class="row">
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">IP</p>
														<h6 class="text-muted f-w-400 mt-0">${data.ip_terakhir}</h6>
													</div>
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Login Terakhir</p>
														<h6 class="text-muted f-w-400 mt-0">${data.terakhir_login}</h6>
													</div>
												</div>

												<div class="row">
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Password Reset</p>
														<h6 class="text-muted f-w-400 mt-0">${data.first_password}</h6>
													</div>
													<div class="col-sm-6">
														<p class="m-b-10 f-w-600 mb-0">Password Awal</p>
														<h6 class="text-muted f-w-400 mt-0">${data.no_induk}</h6>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					`);
                        
                        
                }
                
            })
	}

	$("#modal-detail").on("hidden.bs.modal", function(){
    	
	});

	$("#modal-edit").on("hidden.bs.modal", function(){
    	$(this).find('form').trigger('reset');
	});


	function Edit(id){
		let datas;
		$("#modal-edit").modal("show");
		$.ajax({
			type: "GET",
			url : "{{ route('user-detail') }}",
			data : {
				id : id
			},
			dataType: 'json',
			beforeSend: function() {
				$('#content-edit').html(
					'<i class="fa fa-spin fa-spinner"></i> Loading');
			},
			success: function(data) {
				data = data.body.data;
				$("#content-edit").html(`
					<div class="form-group">
						<div class="row align-items-center align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Nama</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" class="form-control" name="nama" value="${data.nama}">
								<input type="hidden" class="form-control" name="id" value="${data.id}">
								
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Nomor Induk</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" class="form-control" name="no_induk" value="${data.no_induk}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Email</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="email" class="form-control" name="email" value="${data.email}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Role</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select name="role" id="role" value="${data.role}" class="form-control">
									<option value="admin">Admin</option>
									<option value="staf">Staf</option>
									
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Jenkel</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select name="jenkel" id="jenkel" value="${data.jenkel}" class="form-control">
									<option value="l">Laki - laki</option>
									<option value="p">Perempuan</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Agama</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select name="agama" id="agama" value="${data.agama}" class="form-control">
									<option value="islam">Islam</option>
									<option value="kristen">Kristen</option>
									<option value="hindu">Hindu</option>
									<option value="budha">Budha</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Tempat lahir</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" class="form-control" name="tempat_lahir"
									value="${data.tempat_lahir}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Tanggal lahir</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="date" class="form-control" name="tgl_lahir"
									value="${data.tgl_lahir}">
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Telepon</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" class="form-control" value="${data.telepon}" name="telepon">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row align-items-center">
							<label class="col-md-3  col-sm-3 col-xs-12 control-label">Alamat</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name="alamat" id="alamat" class="form-control"
								cols="4" placeholder="${data.alamat}" value="${data.alamat}">
							</div>
						</div>
					</div>
				`);
                        
                        
            }
                
        })

	}

	function Reset(id){
		$.ajax({
			type: "PUT",
			url: "{{env("API_URL")}}" + "api/password/reset-password/user-mutu/"+ id,
			dataType: 'json',
			beforeSend: function() {
				$('#btn-reset').html(
					'<i class="fa fa-spin fa-spinner"></i> Loading');
			},
			success: (data) => {
				noti(data.icon, data.message);   
				$("#modal-detail").modal('hide');             
			},
			error: function(data) {
				noti(data.icon, data.message);   
			}
		});
	}

	function Password(id){
		let datas;
		$("#modal-edit").modal("show");
		$.ajax({
			type: "GET",
			url : "{{ route('user-detail') }}",
			data : {
				id : id
			},
			dataType: 'json',
			success: function(data) {
				data = data.body.data;
				$("#content-edit-pass").html(`
					<div class="form-group">
						<div class="row">
							<label class="col-md-2  col-sm-3 col-xs-12 control-label">Nama</label>
							<div class="col-md-10 col-sm-9 col-xs-12">
								<input type="text" class="form-control" name="nama" value="${data.nama}">
								<input type="hidden" class="form-control" name="id" value="${data.id}">
								
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<label class="col-md-2  col-sm-3 col-xs-12 control-label">Nomor Induk</label>
							<div class="col-md-10 col-sm-9 col-xs-12">
								<input type="number" class="form-control" name="no_induk" value="${data.no_induk}">
							</div>
						</div>
					</div>
				`);
			}
        });
	}

	function ChangePass(id){
		$("#modal-password").modal("show");
	}

	function ResetPass(id){
		swal({
			title: "Apa kamu yakin?",
			text: "ingin me-reset password user ini ?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		}).then(function() {
			$.ajax({
				type: 'PUT',
				url: "{{env("API_URL")}}" + "api/password/reset-password/user-mutu/"+ id,
				success: function(data) {
					swa("Sukses !", 'Password baru user : '+ data.data , 'success');
					// noti(data.icon, data.message);  
					$("#modal-detail").modal('hide'); 
				}
			});
		},
		function(dismiss) {
			if (dismiss === 'cancel') {
				swa("Dibatalkan!", 'Password user tidak berubah', 'error');
			}
        })
	}

	$('#edit-user').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type : "post",
			url  : "{{route('user-update')}}",
			data : $(this).serialize(),
			success : (data) =>{
				$('#data-user').DataTable().ajax.reload();
				noti(data.icon, data.success);
				$("#modal-edit").modal('hide');
			},
			error: function(data) {
                noti(data.icon, data.success);   
            } 
		}); 

	});

	$('body').on('submit', '#formPassword', function(e) {
		e.preventDefault();
		$("#btn-password").html(
			'<i class="fa fa-spin fa-spinner"></i> Loading');
		$("#btn-password").attr("disabled", true);

		let datas = {
			current_password :$("#current_password").val(),
			new_password : $("#new_password").val() ,
			confirm_password : $("#confirm_password").val()
        };
	
		let data = $(this).serialize();
		console.log(data);
		console.log(datas);

		// $.ajax({
		// 	type: "POST",
		// 	url: "{{ route('change_password') }}",
		// 	data: data,
		// 	dataType: 'json',
		// 	success: (data) => {
		// 		swa(data.status + "!", data.success, data.icon);

		// 		$('#modal-password').modal("hide");
		// 		$('#btn-password').html('Password Profile');
		// 		$("#btn-password").attr("disabled", false);

		// 	},
		// 	error: function(data) {
		// 		swa(data.status + "!", data.success, data.icon);
		// 		$('#btn-password').html('Update Profile');
		// 	}
		// });
    });

	$('#create-user').on('submit', function(event) {
		event.preventDefault();
			$.ajax({
				type : "post",
				url  : "{{route('user-create')}}",
				data : $(this).serialize(),
				success : (data) =>{
				$('#data-user').DataTable().ajax.reload();
				noti(data.icon, data.message);
				$("#modal-create").modal('hide');
				},
				error: function(data) {
				noti(data.icon, data.message);   
				} 
			});
    });




</script>



@endsection