@extends('template.template_default.app')
@section('content')

<style>
  #data-pesan{
    width: 100% !important;
  }

  .pace{
    display: none;
  }

  .pesan section{
    border-radius: 20px;
  }

  a,.fa{
    cursor: pointer !important;
  }

  .delete{
    color: #bd2121 !important;
  }

  .content-image{
    padding: 0 !important;

  }

  .header-image{
    padding: 10px 10px 10px 50px !important;
  }

  .body-image{
    padding: 0 !important;
  }

  img{
    cursor: pointer;
  }

  .list-group-item a{
    color:#2471d2;;
  }

</style>

<div class="container">
  <h5 class="title-content">Pesan Masuk</h5>
  @if (Session::has('message'))
  <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
        </button>
        <p class="mb-1">{{ Session::get('message') }}</p>
      
  </div>
@endif
</div>

<div class="container-fluid d-flex flex-row pesan px-3">

  <section class="col-2 bg-white pt-5">
    <span class="row justify-content-center">
      <button type="button" class="badge badge-pill badge-info px-4 py-2" data-toggle="modal" data-target="#modal-pesan">
        <i class="fa fa-plus mr-2" aria-hidden="true"></i>Buat Pesan
      </button>
    </span>
    
    <ul class="list-group list-group-flush mt-3">
      <li class="list-group-item pl-0">
        <a href="{{route('inbox-pesan')}}" class="{{Request::segment(4) == 'inbox' ? '' : 'text-dark'}}">
          <i class="fa fa-inbox fa-lg mr-2" aria-hidden="true"></i>Pesan Masuk
        </a>
      </li>
      <li class="list-group-item pl-0">
        <a href="{{route('send-pesan')}}" class="{{Request::segment(4) == 'send' ? '' : 'text-dark'}}">
          <i class="fa fa-paper-plane fa-lg mr-2" aria-hidden="true"></i>Pesan Keluar
        </a>
      </li>
    </ul>
  </section>

  <section class="col-10 bg-white p-3 ml-3">
    <div class="table-responsive">
      <table class="table table-striped widget-status-table mr-b-0 hover" id="data-pesan">
            <thead>
                <tr>
                    <th>Pengirim</th>
                    <th>Subject</th>
                    <th>Waktu</th>
                </tr>
            </thead>
            <tbody>  
                                
            </tbody>
      </table>
    </div>
  </section>

</div>

<!-- modal preview image -->
  <div class="modal" id="modal-image" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content content-image">

        <div class="modal-header header-image">
          <h5 class="modal-title">Preview image</h5>
        </div>

        <div class="modal-body body-image">
          <img src="" id="main-image" width="100%" alt="IMAGE">
        </div>
        
      </div>
    </div>
  </div>
<!-- end modal -->

<!-- modal pesan create-->
      <div class="modal fade" id="modal-pesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Buat Pesan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form  action="{{ route('buat-pesan') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <input type="email" col="4" name="email" class="form-control" placeholder="Email tujuan">
                    <!-- <select name="email" id="inputState" class="form-control">
                      @foreach($dataUser as $User)
                      <option value="{{$User['email']}}">{{$User['email']}}</option>
                      @endforeach
                    </select> -->
                  </div>
                </div>

                <div class="form-group">          
                  <input type="text" col="4" name="subject" class="form-control" placeholder="Subject">
                </div>

                <div class="form-group">                  
                  <textarea type="text" col="4" row="5" name="isi" class="form-control" placeholder="Pesan"></textarea>
                </div>

                <div class="form-group">
                    <div class="pl-4 my-2 row flex-column">
                      <label for="inputNama" class="">Upload file : </label>
                      <img id="frame" src="https://via.placeholder.com/150" alt="Preview" width="100px">
                      
                    </div>
                    
                    <div id="inputNama" class="col-sm-12 clearfix">
                      <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*" onchange="preview()">
                    </div>

                    <div class="ml-1">
                      <small id="fileHelp" class="form-text text-muted text-danger">Format yang diterima jpg,png,pdf,doc,xls,pptx</small>                               
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-sm">
                <i class="fa fa-paper-plane mr-2" aria-hidden="true"></i>Send
              </button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->

<!-- modal detail -->
  <div class="modal fade bd-example-modal-lg" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="email-content">
          
        </div>
        <!-- end container -->
        
      </div>
    </div>
  </div>
<!-- end modal detail -->

<!-- modal pesan reply-->
      <div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Balas Pesan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('buat-pesan') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                  <div class="form-group col-md-9">
                    <label for="inputState">To :</label>
                    <input type="text" class="form-control" id="send-email" name="email" readonly>
                  </div>
                </div>

                <div class="form-group">          
                  <input type="text" col="4" name="subject" class="form-control" placeholder="Subject">
                </div>

                <div class="form-group">
                  
                  <textarea type="text" col="4" name="isi" class="form-control" placeholder="Pesan"></textarea>
                </div>

                <div class="form-group">
                    <label for="inputNama" class="col-sm-5 col-form-label">Upload file : </label>
                    <div id="inputNama" class="col-sm-12">
                    <input type="file" class="file-uploader pull-left" id="images" name="image" accept="*">                               
                    </div>
                </div>

            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success btn-sm">
                <i class="fa fa-paper-plane mr-2" aria-hidden="true"></i>Send
              </button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->


<script type="text/javascript">

  let tables = $('#data-pesan');

  $(function(){

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
              'Authorization': `Bearer {{Session::get('token')}}`
          }
      });

      const konfigUmum = {
          responsive: true,
          serverSide: true,
          processing: true,
          ordering: true,
      };

      tables.DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('inbox-pesan') }}",
                "method": "GET"
            },
            columns: [
                {
                    data: 'pengirim',
                    name: 'pengirim',
                    width: "10%"
                },
                {
                    data: 'subjects',
                    name: 'subjects',
                    width: "60%"
                },
                {
                    data: 'dikirim',
                    name: 'dikirim',
                    width: "20%",
                    orderable: true
                }
            ]
      });

  });

      function noti(tipe, value) {
          $.toast({
              icon: tipe,
              text: value,
              hideAfter: 5000,
              showConfirmButton: true,
              position: 'top-right',
          });
          return true;
      };

      function Detail(id){
        let data;
        let html_file;
        $('#modalDetail').modal('show');
          $.ajax({
          type: "GET",
            url : "{{ route('detail-pesan') }}",
            data : {
                id : id
            },
            dataType: 'json',
            beforeSend: function() {
                  $('#email-content').html(
                      '<i class="fa fa-spin fa-spinner"></i> Loading');
            },
            success : function(data){
                data = data.body.data;

                if(data.file != null){
                    let file = data.file.split('.').pop();
                    console.log(file);
                    if(file == 'jpg' || file == 'jpeg' || file == 'png' ){
                        html_file = `
                            <div class="col-6 mt-3 px-0">
                              <p>Lampiran : </p>
                              <img src="${data.file}" href="${data.file}" data-toggle="modal" 
                              data-target="#modal-image" onclick="change('${data.file}')" alt="lampiran" width="200px">
                            </div>
                        `;
                    }
                    else{
                        html_file = `
                            <div class="row px-3 mt-3 align-items-center">
                              <i class="fa fa-paperclip" aria-hidden="true"></i>
                              <a href="${data.file}" target="_blank" class="ml-1">Lampiran</a>
                            </div>
                        `
                    }
                }else{
                    html_file = '';
                }

                console.log(data);
                $('#email-content').html(`
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-9">
                          <h4 class="mt-0 text-dark">${data.subject}</h4>
                          <p>Pengirim : ${data.pengirim}, ${data.email_pengirim}</p>
                        </div>

                        <div class="col-3">
                            <span class="row justify-content-end pr-3 mb-2">
                              <a onclick="Reply('${data.email_pengirim}')" class="mx-2">
                                <i class="fa fa-reply fa-lg" aria-hidden="true"></i>
                                Balas
                              </a>
                              <a class="delete"onclick="Delete(${data.id})">
                                <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                Hapus
                              </a>                 
                            </span>
                            <p class="text-right">${data.dikirim}</p> 
                        </div>
                      </div>
                      <div class="row px-3">
                        ${data.pesan}
                      </div>

                      ${html_file}
                      

                    </div>
                  `);
                
            },
            error : function(data){
                console.log(data);
            }
        })
      }

      function Reply(email){
        $('#modalDetail').modal('hide');
        $('#modal-reply').modal('show');
        $('#send-email').val(email);
      }

      function Delete(id){
        $.ajax({
          type: "POST",
              url : "{{ route('hapus-pesan') }}",
              data : {
                  id : id
              },
          success : function(data){
              console.log(data);
              $('#modalDetail').modal('hide');
              noti(data.icon, data.success);
              tables.ajax.reload();

          }
        })
      }

      function preview() {
          frame.src=URL.createObjectURL(event.target.files[0]);
      }

      function change(src) {
          $('#main-image').attr('src', src);
          $('#modalDetail').modal('hide');
      }


      $('#modal-pesan').on('hidden.bs.modal', function () {
          $(this).find('form').trigger('reset');
          $('#frame').attr("src","");
      });

      $('#modal-reply').on('hidden.bs.modal', function () {
          $(this).find('form').trigger('reset');
      })



</script>
@endsection