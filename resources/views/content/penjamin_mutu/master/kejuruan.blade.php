@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

  .input-group a{
    color: #fff !important;
  }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">Kejuruan</h4>
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-create">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah Kejuruan
    </button>
	</span>
	
	   <table class="table table-striped widget-status-table mr-b-0 hover" id="data-kejuruan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>

<!-- modal detail -->
      <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <section class="row">
                <div class="col-2">
                  <h4 class="card-title">Nama</h4>
                  <h6 class="card-title">Jenjang</h6>
                  <h6 class="card-title">Fakultas</h6>
                  <h6 class="card-text"></h6>
                </div>
                <div class="col-10">
                  <h4 class="card-title" id="nama-detail"></h4>
                  <h6 class="card-title" id="detail-fakutas-jenjang"></h6>
                  <h6 class="card-title" id="detail-fakutas-fakultas"></h6>
                </div>
                
              </section>
              <section id="content-detail" class="container flex-column px-2">
                

              </section>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal detail -->

<!-- modal kejuruan create-->
      <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Kejuruan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="javascript:void(0)" id="create-kejuruan">
                @csrf
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nama</label>
                  <input type="text" name="nama" class="form-control" id="recipient-name">
                  <!-- id sekolah -->
                  <input type="hidden" name="id_sekolah" value="{{session('id_sekolah')}}">
                </div>

                <div class="form-row">

                  <div class="form-group col-md-6">
                    <label for="inputState">Fakultas</label>
                    <select name="id_fakultas" id="inputState" class="form-control">
                      @foreach($dataFakultas as $Fakultas)
                      <option value="{{$Fakultas['id']}}">{{$Fakultas['nama']}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="inputState">Jenjang</label>
                    <select name="id_jenjang" id="inputState" class="form-control">
                      @foreach($dataJenjang as $Jenjang)
                      <option value="{{$Jenjang['id']}}">{{$Jenjang['nama']}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                
              
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Buat</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

          </div>
        </div>
      </div>
<!-- end modal -->

<!-- modal edit kejuruan -->
      <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Kejuruan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="edit-kejuruan" action="javascript:void(0)">
                @csrf
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nama</label>
                  <input type="text" name="nama" class="form-control" id="namaEdit">
                  <!-- id -->
                  <input type="hidden" name="id" id="idEdit">
                </div>

                <div class="form-row">

                  <div class="form-group col-md-5">
                    <label for="inputState">Fakultas</label>
                    <select name="id_fakultas" id="id_fakultas" class="form-control">
                      @foreach($dataFakultas as $Fakultas)
                      <option value="{{$Fakultas['id']}}">{{$Fakultas['nama']}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group col-md-5">
                    <label for="inputState">Jenjang</label>
                    <select name="id_jenjang" id="id_jenjang" class="form-control">
                      @foreach($dataJenjang as $Jenjang)
                      <option value="{{$Jenjang['id']}}">{{$Jenjang['nama']}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                
              
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success text-white ">Update</button>
              <button type="button" class="btn btn-secondary text-white " data-dismiss="modal">Batal</button>
            </div>

            </form>

          </div>
        </div>
      </div>

      <!-- modal end -->

<script>

$(function(){

	$.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	const konfigUmum = {
      responsive: true,
      serverSide: true,
      processing: true,
      ordering: false,
  };

  $('#data-kejuruan').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('main-Kejuruan') }}",
                "method": "GET"
            },
            columns: [
              {
                  data: 'DT_RowIndex',
                  name: 'DT_RowIndex',
                  width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "60%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
    });

})

  function noti(tipe, value) {
      $.toast({
          icon: tipe,
          text: value,
          hideAfter: 5000,
          showConfirmButton: true,
          position: 'top-right',
      });
      return true;
  };

  $("#create-kejuruan").submit(function(){
      event.preventDefault();
      $.ajax({
        type : "post",
        url  : "{{ route('buat-Kejuruan') }}", 
        data : $(this).serialize(),
        success : (data) =>{
          $('#data-kejuruan').DataTable().ajax.reload();
          noti(data.icon, data.message);
          $("#modal-create").modal('hide');
        },
        error: function(data) {
          noti(data.icon, data.message);   
        } 
      }); 

  });

  $('#edit-kejuruan').on('submit', function(event) {
      event.preventDefault();
      $.ajax({
        type : "post",
        url  : "{{ route('edit-Kejuruan') }}", 
        data : $(this).serialize(),
        success : (data) =>{
          $('#data-kejuruan').DataTable().ajax.reload();
          noti(data.icon, data.message);
          $("#modal-edit").modal('hide');
        },
        error: function(data) {
          noti(data.icon, data.message);   
        } 
      }); 

    });

	function ShowModal(id){     
		let urlEdit = "{{env("API_URL")}}" + "api/data/mutu/kejuruan/"+ id;
		$.ajax({
                type: 'GET',
                url: urlEdit,
                success: function(data) {
                	 $('#modal-edit').modal('show');       
        			     $('#namaEdit').val(data.data.nama);
        			     $('#idEdit').val(data.data.id);
                   $('#id_jenjang').val(data.data.id_jenjang);
                   $('#id_fakultas').val(data.data.id_fakultas);
                }
        });
        
    };

    function clearForm(){
          $('#inputEditId').val("");
          $('#inputEdit').val("");
    };

    function Edit(){
          let idEdit = $('#inputEditId').val();
          let namaEdit = $('#inputEditNama').val();
          let urlEdit = "{{env("API_URL")}}" + "api/data/mutu/kejuruan/info/"+ idEdit;
          let dataEdit = {
              nama : namaEdit
          }

          $.ajax({
                type: "PUT",
                url : urlEdit,
                data :  dataEdit ,
                dataType: 'json',
                success: function(data) {    
                	$('#data-kejuruan').DataTable().ajax.reload();
                  $('#modal-edit').modal('hide');
                  noti(data.icon, data.message);
                }
            })
          
        };

    function Delete(id){
          let url = "{{env("API_URL")}}" + "api/data/mutu/kejuruan/delete/"+id;
          $.ajax({
              type: "POST",
              data: {
                'id' : id,
                '_method': 'DELETE'
              },
                url:url,
                success: function(data) {
                  $('#data-kejuruan').DataTable().ajax.reload();

                  noti(data.icon, data.message);  
                }
            })
        };

        //menampilkan data detail
    function Detail(id){
          let urlDetail = "{{env("API_URL")}}" + "api/data/mutu/kejuruan/"+ id;
          $.ajax({
                type: 'GET',
                url: urlDetail,
                beforeSend: function() {
                    
                },
                success: function(data) {
                  $('#modal-detail').modal('show');
                  $('#nama-detail').html(': ' + data.data.nama);
                  $('#detail-fakutas-jenjang').html(': '+ data.data.jenjang);
                  $('#detail-fakutas-fakultas').html(': '+data.data.fakultas);

                }
          });
        };

    $('#modal-create').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });

    $('#modal-edit').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });


</script>



@endsection