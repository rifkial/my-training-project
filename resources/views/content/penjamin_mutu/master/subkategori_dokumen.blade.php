@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

  .input-group a{
    color: #fff !important;
  }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
  <h4 class="my-2 title-content">SubKategori Dokumen</h4>
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-create">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-subkategori">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>


<!-- modal kategori create-->
    <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Sub Kategori Dokumen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="javascript:void(0)" id="create-subkategori">
                @csrf
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" >   
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" >
                </div>

                <div class="form-group col-md-8">
                    <label for="inputState">Kategori</label>
                    <select name="id_kategori" id="inputState" class="form-control">
                      @foreach($dataKategori as $Kategori)
                      <option value="{{$Kategori['id']}}">{{$Kategori['nama']}}</option>
                      @endforeach
                    </select>
                </div>

            </div>
            <div class="modal-footer">
              <button type="submit" id="addkategori" class="btn btn-success">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

            </div>
        </div>
    </div>
    <!-- end modal -->

<!-- modal detail -->
      <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <section class="container">
                <h4 class="text-dark my-1" id="nama-detail"></h4>
                <h6 class="card-text my-0" id="kategori-detail"></h6>
                <h5 class="card-text text-dark" id="keterangan-detail"></h5>
                
              </section>
              <section id="content-detail" class="container flex-column px-2">
                

              </section>
            </div>
          </div>
        </div>
      </div>
<!-- end modal detail -->

<!-- modal edit kategori -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edit Kategori Dokumen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form action="javascript:void(0)" id="edit-subkategori">
                @csrf
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="namaEditDocs">
                    <input type="hidden" id="id_item" name="id_item" >
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" id="keteranganEditDocs">
                </div>

                <div class="form-group col-md-6">
                    <label for="inputState">Kategori</label>
                    <select name="id_kategori" id="inputIDKat" class="form-control">
                      @foreach($dataKategori as $Kategori)
                      <option value="{{$Kategori['id']}}">{{$Kategori['nama']}}</option>
                      @endforeach
                    </select>
                </div>

            </div>

            <div class="modal-footer">
                <button type="submit" id="addsub-kategori" class="btn btn-success">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>

            </form>

            </div>
        </div>
    </div>
    <!-- end modal -->


<script>

  $(function(){

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: false,
    };

    $('#data-subkategori').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('main-subkategori') }}",
                "method": "GET"
            },
            columns: [
              {
                   data: 'DT_RowIndex',
                   name: 'DT_RowIndex',
                   width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "60%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
         });
  });

    $('#modal-detail').on('hidden', function() {
		    clear();
		});

		$('#modal-edit').on('hidden', function() {
		    clear();
		});

        

        function noti(tipe, value) {
	        $.toast({
	            icon: tipe,
	            text: value,
	            hideAfter: 5000,
	            showConfirmButton: true,
	            position: 'top-right',
	        });
        	return true;
        }


        function Delete(id){
          let url = "{{env("API_URL")}}" + "api/data/mutu/sub-kategori/delete/"+id;
          $.ajax({
              type: "POST",
              data: {
                'id' : id,
                '_method': 'DELETE'
              },
                url:url,
                success: function(data) {
                  $('#data-subkategori').DataTable().ajax.reload();
                  noti(data.icon, data.message);  
                }

            })
        }

        function ShowModal(id){
            let url = "{{env("API_URL")}}" + "api/data/mutu/sub-kategori/"+id;
            $.ajax({
                type: "GET",
                url:url,
                success: function(data) {
                    $("#namaEditDocs").val(data.data.nama);
                    $("#keteranganEditDocs").val(data.data.keterangan);
                    $("#id_item").val(data.data.id);
                    $('#inputIDKat').val(data.data.id_kategori);
                }
            })
            $("#modal-edit").modal("show");
        }

        function Detail(id){
          let urlDetail = "{{env("API_URL")}}" + "api/data/mutu/sub-kategori/"+ id;
          $.ajax({
                type: 'GET',
                url: urlDetail,
                success: function(data) {
                  
                  $('#modal-detail').modal('show');
                  $('#nama-detail').html('' + data.data.nama);

                  if(data.data.keterangan != null){
                      $('#keterangan-detail').html(' ' + data.data.keterangan);
                  }

                  if(data.data.kategori != null){
                    $('#kategori-detail').html('kategori : '+data.data.kategori);
                  }

                      
                  
                }
          });
        };

    $('#create-subkategori').on('submit', function(event) {
          event.preventDefault();
          $.ajax({
            type : "post",
            url  : "{{route('buat-subkategori')}}",
            data : $(this).serialize(),
            success : (data) =>{
              $('#data-subkategori').DataTable().ajax.reload();
              noti(data.icon, data.message);
              $("#modal-create").modal('hide');
            },
            error: function(data) {
              noti(data.icon, data.message);   
            } 
          }); 
        });

        $('#edit-subkategori').on('submit', function(event) {
          event.preventDefault();
          $.ajax({
            type : "post",
            url  : "{{route('edit-subkategori')}}",
            data : $(this).serialize(),
            success : (data) =>{
              $('#data-subkategori').DataTable().ajax.reload();
              noti(data.icon, data.message);
              $("#modal-edit").modal('hide');
            },
            error: function(data) {
              noti(data.icon, data.message);   
            } 
          }); 

        });

    $('#modal-detail').on('hidden', function() {
        clear();
    });

    $('#modal-edit').on('hidden.bs.modal', function() {
      $(this).find('form').trigger('reset');
    });

    $('#modal-create').on('hidden.bs.modal', function() {
      $(this).find('form').trigger('reset');
    });


</script>


@endsection