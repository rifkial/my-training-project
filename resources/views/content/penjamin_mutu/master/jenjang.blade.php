@extends('template.template_default.app')
@section('content')

<style type="text/css">
	.pace{
		display: none;
	}

  .input-group a{
    color: #fff !important;
  }

  .modal-body .container h4,h5{
    color: #444444 !important;
  }
</style>

<div class="container bg-white p-5">
	<span class="d-flex flex-row justify-content-between align-items-center">
		<h4 class="my-2 title-content">Jenjang</h4>
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-create">
      <i class="fa fa-plus mr-1" aria-hidden="true"></i>Tambah Jenjang
    </button>
	</span>
	
	<table class="table table-striped widget-status-table mr-b-0 hover" id="data-jenjang">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>  
                        
			</tbody>
    </table>
</div>

<!-- modal detail -->
      <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <section class="container">
                <h4 class="card-title mb-0" id="nama-detail"></h4>
                <h5 class="card-text mt-0" id="jumlah-detail"></h5>
                <h5 class="card-text" id="daftar-detail"></h5>
              </section>
              <section id="content-detail" class="container flex-column px-2">
                

              </section>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal detail -->

<!-- modal kejuruan create-->
      <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Jenjang</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="create-kejuruan" action="javascript:void(0)">
                @csrf
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nama</label>
                  <input type="text" name="nama" class="form-control" id="recipient-name">
                  <!-- id sekolah -->
                  <input type="hidden" name="id_sekolah" value="{{session('id_sekolah')}}">
                </div>

                <div class="form-row">

                </div>
                
              
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Buat</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
            </div>

            </form>

          </div>
        </div>
      </div>
      <!-- end modal -->

<!-- modal edit -->
      <div class="modal" id="modal-edit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Edit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputPassword1">Nama</label>
                <input type="hidden" class="form-control" id="inputEditId">
                <input type="text" class="form-control" id="inputEditNama">
              </div>
            </div>
            <div class="modal-footer">
              <a onclick="Edit()" type="button" class="btn btn-success text-white">Update</a>
              <a onclick="clearForm()" type="button" class="btn btn-secondary text-white" data-dismiss="modal">Batal</a>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal -->

<script>

$(function(){

	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

	const konfigUmum = {
            responsive: true,
            serverSide: true,
            processing: true,
            ordering: false,
        };

  $('#data-jenjang').DataTable({
            ...konfigUmum,
            paging: true,
            searching: true,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": "{{ route('main-Jenjang') }}",
                "method": "GET"
            },
            columns: [
              {
                  data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: "10%"
                },
                {
                    data: 'nama',
                    name: 'nama',
                    width: "60%"
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    width: "10%"
                }
            ]
    });

});


    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    };


	

	 function ShowModal(id){     
    		let urlEdit = "{{env("API_URL")}}" + "api/data/mutu/jenjang/"+ id;
    		$.ajax({
                type: 'GET',
                url: urlEdit,
                success: function(data) {
                	$('#modal-edit').modal('show');       
                  $('#inputEditId').val(data.data.id);
                  $('#inputEditNama').val(data.data.nama);
                }
        });      
    };

    function clearForm(){
          $('#inputEditId').val("");
          $('#inputEdit').val("");
    };

    function Edit(){
          let idEdit = $('#inputEditId').val();
          let namaEdit = $('#inputEditNama').val();
          let urlEdit = "{{env("API_URL")}}" + "api/data/mutu/jenjang/info/"+ idEdit;
          let dataEdit = {
              nama : namaEdit
          }

          $.ajax({
                type: "PUT",
                url : urlEdit,
                data :  dataEdit ,
                dataType: 'json',
                success: function(data) {    
                	$('#data-jenjang').DataTable().ajax.reload();
                    $('#modal-edit').modal('hide');
                    noti(data.icon, data.message);
                }
            })
          
        };

    function Delete(id){
          let url = "{{env("API_URL")}}" + "api/data/mutu/jenjang/delete/"+id;
          $.ajax({
              type: "POST",
              data: {
                'id' : id,
                '_method': 'DELETE'
              },
                url:url,
                success: function(data) {
                  $('#data-jenjang').DataTable().ajax.reload();

                  noti(data.icon, data.message);  
                }
            })
        };

        //menampilkan data detail
    function Detail(id){
          let urlDetail = "{{env("API_URL")}}" + "api/data/mutu/jenjang/"+ id;
          let dataDetails = "";
          let dataDetail ="";
          $.ajax({
                type: 'GET',
                url: urlDetail,
                success: function(data) {
                  $('#modal-detail').modal('show');
                  $('#nama-detail').html(data.data.nama);

                  $('#jumlah-detail').html('Jumlah kejuruan: ' + data.data.jumlah_kejuruan);

                    if(data.data.jumlah_kejuruan != 0 ){
                        dataDetails = "";
                        dataDetail ="";
                        $.each(data.data.kejuruan, function(index, value) {
                          dataDetail +=
                              '<ul class="list-unstyled sub-menu ml-2 mb-0" ><li><i class="fa fa-minus mr-1" aria-hidden="true"></i>'+ value.nama + '</li></ul>'
                        });
                        dataDetails = '<h5 class="card-text ml-1">Daftar kejuruan </h5>'+ dataDetail + '';

                        $('#content-detail').html(dataDetails);
                    }else{
                        dataDetails = "";
                        dataDetail ="";
                        $('#content-detail').html(dataDetails);
                    }

                }
          });
        };

    $('#create-kejuruan').on('submit', function(event) {
      event.preventDefault();
      $.ajax({
        type : "post",
        url  : "{{route('buat-Jenjang')}}",
        data : $(this).serialize(),
        success : (data) =>{
          console.log(data);
          $('#data-jenjang').DataTable().ajax.reload();
          noti(data.icon, data.message);
          $("#modal-create").modal('hide');
        },
        error: function(data) {
          noti(data.icon, data.message);   
        } 
      }); 

    });

    $('#modal-create').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });

    $('#modal-edit').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });


</script>



@endsection