@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet"
        type="text/css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <style>
        .pace {
            display: none;
        }

        #loading {
            /* text-align: center; */
            background: url("{{ asset('asset/img/loading.gif') }}") no-repeat center;
            height: 290px;
            width: 290px;
            display: block;
            margin: auto;
            position: absolute;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            text-align: center;
            top: 48px;
        }

        .panel {
            border: none;
            box-shadow: none;
        }

        .panel-heading {
            border-color: #eff2f7;
            font-size: 16px;
            font-weight: 300;
        }

        .panel-title {
            color: #2A3542;
            font-size: 14px;
            font-weight: 400;
            margin-bottom: 0;
            margin-top: 0;
            font-family: 'Open Sans', sans-serif;
        }


        /*product list*/

        .prod-cat li a {
            border-bottom: 1px dashed #d9d9d9;
        }

        .prod-cat li a {
            color: #3b3b3b;
        }

        .prod-cat li ul {
            margin-left: 30px;
        }

        .prod-cat li ul li a {
            border-bottom: none;
        }

        .prod-cat li ul li a:hover,
        .prod-cat li ul li a:focus,
        .prod-cat li ul li.active a,
        .prod-cat li a:hover,
        .prod-cat li a:focus,
        .prod-cat li a.active {
            background: none;
            color: #ff7261;
        }

        .pro-lab {
            margin-right: 20px;
            font-weight: normal;
        }

        .pro-sort {
            padding-right: 20px;
            float: left;
        }

        .pro-page-list {
            margin: 5px 0 0 0;
        }

        .product-list img {
            width: 100%;
            border-radius: 4px 4px 0 0;
            -webkit-border-radius: 4px 4px 0 0;
        }

        .product-list .pro-img-box {
            position: relative;
        }

        .adtocart {
            background: #fc5959;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            color: #fff;
            display: inline-block;
            text-align: center;
            border: 3px solid #fff;
            left: 45%;
            bottom: -25px;
            position: absolute;
        }

        .adtocart i {
            color: #fff;
            font-size: 25px;
            line-height: 42px;
        }

        .pro-title {
            color: #5A5A5A;
            display: inline-block;
            margin-top: 20px;
            font-size: 16px;
        }

        .product-list .price {
            color: #fc5959;
            font-size: 15px;
        }

        .pro-img-details {
            margin-left: -15px;
        }

        .pro-img-details img {
            width: 100%;
        }

        .pro-d-title {
            font-size: 16px;
            margin-top: 0;
        }

        .product_meta {
            border-top: 1px solid #eee;
            border-bottom: 1px solid #eee;
            padding: 10px 0;
            margin: 15px 0;
        }

        .product_meta span {
            display: block;
            margin-bottom: 10px;
        }

        .product_meta a,
        .pro-price {
            color: #fc5959;
        }

        .pro-price,
        .amount-old {
            font-size: 18px;
            padding: 0 10px;
        }

        .amount-old {
            text-decoration: line-through;
        }

        .quantity {
            width: 120px;
        }

        .pro-img-list {
            margin: 10px 0 0 -15px;
            width: 100%;
            display: inline-block;
        }

        .pro-img-list a {
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .pro-d-head {
            font-size: 18px;
            font-weight: 300;
        }

        .nav>li>a {
            position: relative;
            display: block;
            padding: 10px 15px;
        }

        .full_width,
        .full_w {
            position: relative;
            display: block;
            width: 100%;
        }

    </style>

    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="row bootdey">
                    <div class="col-md-3">
                        <div class="accordion" id="accordion-2" role="tablist" aria-multiselectable="true">
                            @foreach ($menu as $mn)
                                <div class="card card-outline-info">
                                    <div class="card-header" role="tab" id="heading7">
                                        <h5 class="m-0"><a role="button" data-toggle="collapse"
                                                href="#detailJurusan{{ $mn['id'] }}">{{ $mn['nama'] }}</a></h5>
                                    </div>
                                    <!-- /.card-header -->
                                    <div id="detailJurusan{{ $mn['id'] }}" class="card-collapse collapse" role="tabpanel">
                                        <div class="card-body">
                                            <ul class="nav prod-cat">
                                                @foreach ($mn['kelas'] as $kelas)
                                                    <li class="full_width">
                                                        <a href="#" class="active"
                                                            style="border: none !important"><i
                                                                class="fa fa-angle-right"></i>
                                                            {{ $kelas['nama_romawi'] }}</a>
                                                        <ul class="nav">
                                                            @foreach ($kelas['rombel'] as $rombel)
                                                                <li class="full_w"><a href="javascript:void(0)"
                                                                        onclick="getCalendar({{ $rombel['id'] }})"
                                                                        style="border-bottom: 1px dashed #d9d9d9;">-
                                                                        {{ $rombel['nama'] }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card-collapse -->
                                </div>
                            @endforeach

                        </div>
                        {{-- <section class="panel">
                            <div class="panel-body">
                                <ul class="nav prod-cat">
                                    @foreach ($menu as $mn)
                                        <li class="full_width">
                                            <a href="#" class="active" style="border: none !important"><i
                                                    class="fa fa-angle-right"></i>
                                                {{ $mn['id_jurusan'] != null ? $mn['nama_romawi'] . ' - ' . $mn['jurusan'] : $mn['nama_romawi'] }}</a>
                                            <ul class="nav">
                                                @if (!empty($mn['rombel']))
                                                    @foreach ($mn['rombel'] as $rombel)
                                                        <li class="full_w"><a href="javascript:void(0)"
                                                                onclick="getCalendar({{ $rombel['id'] }})"
                                                                style="border-bottom: 1px dashed #d9d9d9;">-
                                                                {{ $rombel['nama'] }}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </section> --}}
                    </div>
                    <div class="col-md-9">
                        <div class="proses"></div>
                        <div class="row product-list">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        function getCalendar(eId) {
            // $('.proses').show();
            // $('.proses').html('<div id="loading"></div>');
            calendar(eId)
        }

        function calendar(id) {
            $('.product-list').html('<div id="calendar_' + id + '"></div>');
            var calendar = $('#calendar_' + id).fullCalendar({
                editable: true,
                events: {
                    url: "{{ route('e-learning_supervisor-kalender_rombel') }}",
                    cache: true,
                    type: 'POST',
                    data: {
                        id_rombel: id
                    },
                    // beforeSend: function() {
                    //     $('.proses').show();
                    // },
                    error: function() {
                        alert('there was an error while fetching events!');
                    },
                },
                // $('.proses').hide();
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: false,
                eventRender: function(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
            });
        }
    </script>
@endsection
