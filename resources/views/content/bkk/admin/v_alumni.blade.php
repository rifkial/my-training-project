@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-9 col-md-9"></div>
                            <div class="col-sm-3 col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tbody>
                                @if (empty($peserta))
                                    <tr>
                                        <td colspan="8" class="text-center">Data untuk saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($peserta as $ps)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($ps['nama']) }}</td>
                                            <td>{{ $ps['nis'] }}</td>
                                            <td>{{ $ps['nisn'] }}</td>
                                            <td>{{ $ps['email'] }}</td>
                                            <td>{{ $ps['telepon'] }}</td>
                                            <td>{{ $ps['industri'] }}</td>
                                            <td>{{ $ps['pemb_industri'] }}</td>
                                            <td>{{ $ps['alamat_industri'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                <tr class="bg-info">
                                    <td class="text-center font-weight-bold" colspan="8">Jumlah Siswa</td>
                                    <td class="text-center font-weight-bold">{{ count($peserta) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div class="widget-bg" style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Nama</h6>
                        <p rel="field-nama" class="mr-t-0"></p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Tempat, Tanggal Lahir</h6>
                        <p rel="field-tempat_tgl_lahir" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Jenis Kelamin</h6>
                        <p rel="field-jenkel" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Email</h6>
                        <p rel="field-email" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Nomor Telepon</h6>
                        <p rel="field-telepon" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Alamat</h6>
                        <p rel="field-alamat" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Jurusan</h6>
                        <p rel="field-jurusan" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Angkatan</h6>
                        <p rel="field-angkatan" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-uppercase">Pekerjaan</h6>
                        <p rel="field-pekerjaan" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6" id="sesuai">
                        <h6 class="text-uppercase">Pekerjaan Sesuai Jurusan</h6>
                        <p rel="field-sesuai_jurusan" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6" id="bidang">
                        <h6 class="text-uppercase">Bidang Usaha</h6>
                        <p rel="field-bidang_usaha" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6" id="sekolah_universitas">
                        <h6 class="text-uppercase">Sekolah / Universitas</h6>
                        <p rel="field-universitas" class="mr-t-0">Namamamama</p>
                    </div>
                    <div class="col-md-6" id="program_studi">
                        <h6 class="text-uppercase">Program Studi</h6>
                        <p rel="field-prodi" class="mr-t-0">Namamamama</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var url_import = "{{ route('import-alumni') }}";
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'ponsel',
                        name: 'ponsel'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },
                ]
            });




            $('body').on('click', '.detail', function() {
                var id = $(this).data('id');
                const loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-alumni') }}",
                    data: {
                        id_alumni: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $('#modelHeading').html("Detail Data Alumni");
                        $('[rel="field-nama"]').text(data.nama);
                        $('[rel="field-tempat_tgl_lahir"]').text(data.tempat_lahir + ", " + data
                            .date_birth);
                        $('[rel="field-jenkel"]').text(data.jenkel);
                        $('[rel="field-email"]').text(data.email);
                        $('[rel="field-telepon"]').text(data.ponsel);
                        $('[rel="field-alamat"]').text(data.alamat);
                        $('[rel="field-jurusan"]').text(data.jurusan != null ? data.jurusan :
                            "-");
                        $('[rel="field-angkatan"]').text(data.angkatan);
                        $('[rel="field-pekerjaan"]').text(data.pekerjaan != null ? data
                            .pekerjaan : "-");
                        $('[rel="field-sesuai_jurusan"]').text(data.sesuai_jurusan);
                        $('[rel="field-bidang_usaha"]').text(data.bidang_usaha);
                        $('[rel="field-universitas"]').text(data.universitas);
                        $('[rel="field-prodi"]').text(data.program_studi);
                        if (data.pekerjaan == 'usaha') {
                            $('#sesuai').hide();
                            $('#sekolah_universitas').hide();
                            $('#program_studi').hide();
                            $('#bidang').show();
                        } else if (data.pekerjaan == 'kerja') {
                            $('#sesuai').show();
                            $('#sekolah_universitas').hide();
                            $('#program_studi').hide();
                            $('#bidang').hide();
                        } else if (data.pekerjaan == 'kuliah') {
                            $('#sesuai').hide();
                            $('#sekolah_universitas').show();
                            $('#program_studi').show();
                            $('#bidang').hide();
                        } else {
                            $('#sesuai').hide();
                            $('#sekolah_universitas').hide();
                            $('#program_studi').hide();
                            $('#bidang').hide();
                        }
                        $(loader).html(
                            '<i class="fa fa-eye"></i> Detail');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
