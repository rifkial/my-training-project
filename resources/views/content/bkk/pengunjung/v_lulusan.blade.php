@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.0/css/all.min.css" rel="stylesheet"
        type="text/css">
    <div class="content-wrapper">
        <main class="main-wrapper clearfix">

            <div class="panel panel-primary m-5">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Registrasi Alumni</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-info border-info" role="alert">
                        <p class="m-0">
                            Selamat datang ALUMNI SMK Negeri 1 Bawen.
                            Laman ini kami gunakan untuk mendapatkan data tentang alumni. Perna serta
                            alumni untuk berpartisipasi dengan mengisi laman penelusuran tamatan ini
                            sangat kami harapkan agar kami dapat memetakan kebutuhan kompetensi yang
                            nantinya akan kami gunakan untuk perencanaan program peningkatan mutu SMK
                            Negeri 1 Bawen. Kami menjamin keamanan data yang anda kirimkan.
                            Terima kasih.
                        </p>
                    </div>
                    <form id="insertAlumni">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="name">Nama Lengkap* </label>
                                <input type="text" class="form-control input-sm" id="nama" name="nama" placeholder="">
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="email">NIK*</label>
                                <input type="number" class="form-control input-sm" id="nik" name="nik" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12" id="deceased">
                            <div class="form-group col-md-3 col-sm-3">
                                <label for="name">Email*</label>
                                <input type="email" class="form-control input-sm" id="email" name="email" placeholder="">
                            </div>
                            <div class="form-group col-md-3 col-sm-3">
                                <label for="gender">Jenis Kelamin*</label>
                                <select class="form-control input-sm" id="jenkel" name="jenkel">
                                    <option>-- Pilih Jenis Kelamin --</option>
                                    <option value="l">Laki - laki</option>
                                    <option value="p">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-3">
                                <label for="age">Tempat Lahir*</label>
                                <input type="text" class="form-control input-sm" id="tempat_lahir" name="tempat_lahir"
                                    placeholder="">
                            </div>
                            <div class="form-group col-md-3 col-sm-3">
                                <label for="DOB">Tanggal Lahir*</label>
                                <input type="text" class="form-control input-sm datepicker" id="tgl_lahir" name="tgl_lahir"
                                    placeholder="">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="mobile">Telepon*</label>
                                <input type="text" class="form-control input-sm" id="no_hp" name="no_hp" placeholder="">
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="address">Alamat*</label>
                                <textarea class="form-control input-sm" id="alamat" name="alamat" rows="3"></textarea>
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="jurusan">Jurusan*</label>
                                <select class="form-control input-sm" id="id_jurusan" name="id_jurusan">
                                    <option value="">-- Pilih Jurusan --</option>
                                    @foreach ($jurusan as $jr)
                                        <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                    @endforeach
                                    <option value="">Lainnya</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="state">Angkatan*</label>
                                <input type="text" class="form-control input-sm" id="tahun_lulus" name="tahun_lulus"
                                    placeholder="">
                            </div>

                            <div class="form-group col-md-6 col-sm-6">
                                <label for="country">Pekerjaan*</label>
                                <select class="form-control input-sm" id="pekerjaan" name="pekerjaan"
                                    onchange="changePekerjaan(this)">
                                    <option value="kerja">Bekerja</option>
                                    <option value="usaha">Wirausaha</option>
                                    <option value="kuliah">Lanjut Sekolah</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-6" id="kerja">
                                <label class="ml-2" for="inputRkerja">Apakah pekerjaan anda sesuai
                                    jurusan?</label>

                                <select class="form-control input-sm" name="sesuai">
                                    <option value="1">Ya</option>
                                    <option value="2">Tidak</option>
                                </select>
                            </div>
                            <div id="kuliah" style="display: none">
                                <div class="form-group  col-md-6 col-sm-6">
                                    <label for="state">Nama Sekolah /
                                        Universitas?*</label>
                                    <input type="text" class="form-control input-sm" id="universitas" name="universitas"
                                        placeholder="">
                                </div>
                                <div class="form-group  col-md-6 col-sm-6">
                                    <label for="state">Program Studi / Jurusan?*</label>
                                    <input type="text" class="form-control input-sm" id="studi" name="studi" placeholder="">
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-sm-6" id="usaha" style="display: none">
                                <label for="state">Bidang Usaha*</label>
                                <input type="text" class="form-control input-sm" id="bidang_usaha" name="bidang_usaha"
                                    placeholder="">
                            </div>
                            <div class="form-group col-md-3 col-sm-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="https://via.placeholder.com/150" id="modal-preview" alt=""
                                            class="m-auto" width="100px">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="photo" class="mt-3">Photo</label>
                                        <input type="file" id="photo" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <p class="help-block mb-0">Tolong untuk upload foto sendiri. Foto berkelompok tidak
                                            akan di
                                            terima</p>
                                        <small class="text-danger">Bisa dikosongi</small>
                                    </div>
                                </div>

                                {{-- <p class="help-block">Please upload individual photo. Group photo is not acceptable.</p> --}}
                            </div>
                            <div class="form-group col-md-3 col-sm-3">

                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-3 col-sm-3">
                                <button type="submit" class="btn btn-primary" id="btnRegister"><i class="fas fa-sign-in-alt"></i> Daftar
                                    Sekarang</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#insertAlumni', function(e) {
                e.preventDefault();
                $("#btnRegister").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Mendaftar..');
                $("#btnRegister").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('bursa_kerja-simpan_alumni') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#insertAlumni').trigger("reset");
                            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                        }
                        swa(data.status + "!", data.message, data.icon);
                        $('#btnRegister').html('<i class="fas fa-sign-in-alt"> Daftar Sekarang</i>');
                        $("#btnRegister").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            // $('#insertAlumni').on('submit', function(event) {
            //     event.preventDefault();
            //     $(".txt2").html(
            //         '<i class="fa fa-spin fa-spinner"></i> Loading');
            //     $(".txt2").attr("disabled", true);
            //     $.ajax({
            //         url: "{{ route('bursa_kerja-simpan_alumni') }}",
            //         method: "POST",
            //         data: $(this).serialize(),
            //         dataType: "json",
            //         success: function(data) {
            //             if (data.status == 'berhasil') {
            //                 $('#insertAlumni').trigger("reset");
            //             }
            //             // noti(data.icon, data.success);
            //             swa(data.status + "!", data.success, data.icon);
            //             $('.txt2').html('Simpan');
            //             $(".txt2").attr("disabled", false);
            //         },
            //         error: function(data) {
            //             console.log('Error:', data);
            //             $('#saveBtn').html('Simpan');
            //         }
            //     });
            // });
        });

        function changePekerjaan(isi) {
            const pekerjaan = isi.value;
            // alert(pekerjaan);
            if (pekerjaan == 'kerja') {
                $('#kuliah').hide();
                $('#kerja').show();
                $('#usaha').hide();
            } else if (pekerjaan == 'usaha') {
                $('#kuliah').hide();
                $('#kerja').hide();
                $('#usaha').show();
            } else {
                $('#kuliah').show();
                $('#kerja').hide();
                $('#usaha').hide();
            }
        }
    </script>
@endsection
