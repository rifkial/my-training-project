<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
    href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .navbar-default {
        border: none;
        border-radius: 0px;
        background: #009BDD;
    }

    .navbar-collapse {
        padding: 0px;
        margin: 0px;
    }

    nav ul {
        width: 100% !important;
    }

    nav ul li {
        display: inline;
        position: relative;
    }

    nav ul li ul {
        width: 175px !important;
        position: absolute;
        top: 50px;
        left: 0;
        padding: 0;
        display: none;
    }

    nav ul li:hover {
        background-color: #007DA7;
    }

    nav ul li ul li a {
        display: block;
        padding: 10px;
        background: #009BDD;
        border-bottom: 1px solid #fff;
    }

    nav ul li ul li ul {
        left: 175px;
        top: -27px;
        position: absolute;
        display: none;
    }

    nav ul .fa-sort-desc {
        position: relative;
        top: -3px;
        right: -5px;
    }

    nav ul li ul li i {
        margin-left: 20px !important;
    }

    nav ul li a:hover {
        background: #007DA7 !important;
        text-decoration: none;
    }

    nav ul li a {
        text-decoration: none;
        font-weight: bold;
        text-transform: uppercase;
        color: #fff !important;
    }

    nav ul li:hover>ul {
        display: block;
    }

    .stylish-input-group .input-group-addon {
        background: white !important;
    }

    .stylish-input-group .form-control {
        border-right: 0;
        box-shadow: 0 0 0;
        border-color: #ccc;
    }

    .stylish-input-group button {
        border: 0;
        background: transparent;
    }

    nav ul .search,
    nav ul .custom-login,
    nav ul .custom-register {
        float: right;
    }

    nav ul .search {
        margin: 7px 10px 0px 0px;
        border-radius: 4px;
        width: 100px;
    }

    @media only screen and (max-width:480px) {
        button.navbar-toggle {
            z-index: 999;
        }

        .navbar-default {
            background-color: #7f378c;
        }

        .navbar-default .navbar-toggle .icon-bar {
            background-color: #fff;
        }

        .navbar-toggle {
            position: absolute;
            float: right;
            right: 0;
        }


        .navbar-collapse {
            padding: 10px 20px;
        }

        nav ul li ul {
            top: 0px;
            width: 100% !important;
            position: relative;
        }

        nav ul ul li ul {
            position: relative !important;
            width: 100% !important;
        }

        nav ul li ul li ul {
            top: 1px !important;
            left: 0px !important;
        }

        nav ul .search,
        nav ul .custom-login,
        nav ul .custom-register {
            float: unset;
        }
    }

</style>
<div class="container m-0 p-0" style="width: 100%; max-width: none">
    <nav class="navbar navbar-default mb-0" style="height: auto; background: #009BDD">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#navbar-collapse-2">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <ul class="nav navbar-nav collapse navbar-collapse" id="navbar-collapse-2">
            <li><a href="{{ route('bursa_kerja-home') }}"> <i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            <li><a href="{{ route('bursa_kerja-beranda') }}"> <i class="fa fa-user-md" aria-hidden="true"></i> Lowker</a></li>
            <li><a href="{{ route('bursa_kerja-perusahaan') }}"> <i class="fa fa-building-o" aria-hidden="true"></i> Perusahaan</a></li>
            <li><a href="{{ route('bursa_kerja-lulusan') }}"><i class="fa fa-address-card-o" aria-hidden="true"></i> Lulusan</a></li>
            <li><a href="{{ route('bkk_loker-diskusi') }}"><i class="fa fa-phone" aria-hidden="true"></i> Question</a></li>
            <li class="custom-login"><a href="{{ url('program', 'bursa_kerja') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> login</a></li>
            <li class="custom-register"><a href="{{ route('home') }}"> <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    Kembali Ke LMS</a></li>
        </ul>
    </nav>
</div>
