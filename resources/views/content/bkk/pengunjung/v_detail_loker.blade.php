@extends('content/bkk/pengunjung/main')
@section('content_bkk')

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/jquery.readall.min.css') }}">
    <script src="{{ asset('asset/js/jquery.readall.min.js') }}"></script>
    <style>
        body {
            background: #f2f4f8 !important;
        }

        .pace {
            display: none;
        }

        .slider.autoplay.slick-initialized.slick-slider {
            width: 95%;
            margin: auto
        }

        section {
            /* text-align: center; */
            margin: 0px auto 0px auto;
        }


        .funny-boxes {
            overflow: hidden;
            padding: 15px 20px;
            margin-bottom: 25px;
            background: #f7f7f7;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        .funny-boxes h2 a {
            color: #555;
        }

        .funny-boxes p a {
            color: #72c02c;
        }

        .funny-boxes .funny-boxes-img li i {
            color: #72c02c;
            font-size: 12px;
            margin-right: 5px;
        }

        .funny-boxes .funny-boxes-img img {
            display: block;
            margin: 5px 10px 10px 0;
        }

        .funny-boxes h2 {
            margin-top: 0;
            font-size: 20px;
            line-height: 20px;
        }

        .funny-boxes ul.funny-boxes-rating li {
            display: inline;
        }

        .funny-boxes ul.funny-boxes-rating li i {
            color: #f8be2c;
            cursor: pointer;
            font-size: 16px;
        }

        .funny-boxes ul.funny-boxes-rating li i:hover {
            color: #f8be2c;
        }

        /*Funny Colored Boxes*/
        .funny-boxes-colored p,
        .funny-boxes-colored h2 a,
        .funny-boxes-colored .funny-boxes-img li,
        .funny-boxes-colored .funny-boxes-img li i {
            color: #fff;
        }

        /*Red Funny Box*/
        .funny-boxes-red {
            background: #e74c3c;
        }

        /*Blue Funny Box*/
        .funny-boxes-blue {
            background: #3498db;
        }

        /*Grey Funny Box*/
        .funny-boxes-grey {
            background: #95a5a6;
        }

        /*Turquoise Funny Box*/
        .funny-boxes-sea {
            background: #1abc9c;
        }

        /*Turquoise Top Bordered Funny Box*/
        .funny-boxes-top-sea {
            border-top: solid 2px #1abc9c;
        }

        .funny-boxes-top-sea:hover {
            border-top-color: #16a085;
        }

        /*Yellow Top Bordered Funny Box**/
        .funny-boxes-top-yellow {
            border-top: solid 2px #f1c40f;
        }

        .funny-boxes-top-yellow:hover {
            border-top-color: #f39c12;
        }

        /*Orange Left Bordered Funny Box**/
        .funny-boxes-left-orange {
            border-left: solid 2px #e67e22;
        }

        .funny-boxes-left-orange:hover {
            border-left-color: #d35400;
        }

        /*Green Left Bordered Funny Box**/
        .funny-boxes-left-green {
            border-left: solid 2px #72c02c;
        }

        .funny-boxes-left-green:hover {
            border-left-color: #5fb611;
        }

        /*Green Right Bordered Funny Box**/
        .funny-boxes-right-u {
            border-right: solid 2px #72c02c;
        }

        .funny-boxes-right-u:hover {
            border-right-color: #5fb611;
        }

        .btn {
            box-shadow: none;
        }

        .btn-u {
            white-space: nowrap;
            border: 0;
            color: #fff;
            font-size: 14px;
            cursor: pointer;
            font-weight: 400;
            padding: 6px 13px;
            position: relative;
            background: #72c02c;
            display: inline-block;
            text-decoration: none;
        }

        .btn-u:hover {
            color: #fff;
            text-decoration: none;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        a.btn-u {
            /*padding: 4px 13px;*/
            /*vertical-align: middle;*/
        }

        .btn-u-sm,
        a.btn-u-sm {
            padding: 3px 12px;
        }

        .btn-u-lg,
        a.btn-u-lg {
            font-size: 18px;
            padding: 10px 25px;
        }

        .btn-u-xs,
        a.btn-u-xs {
            font-size: 12px;
            padding: 2px 12px;
            line-height: 18px;
        }

        /*Button Groups*/
        .btn-group .dropdown-menu>li>a {
            padding: 3px 13px;
        }

        .btn-group>.btn-u,
        .btn-group-vertical>.btn-u {
            float: left;
            position: relative;
        }

        .btn-group>.btn-u:first-child {
            margin-left: 0;
        }

        /*For FF Only*/
        @-moz-document url-prefix() {
            .footer-subsribe .btn-u {
                padding-bottom: 4px;
            }
        }

        @media (max-width: 768px) {
            @-moz-document url-prefix() {
                .btn-u {
                    padding-bottom: 6px;
                }
            }
        }

        /*Buttons Color*/
        .btn-u:hover,
        .btn-u:focus,
        .btn-u:active,
        .btn-u.active,
        .open .dropdown-toggle.btn-u {
            background: #5fb611;
        }

        .btn-u-split.dropdown-toggle {
            border-left: solid 1px #5fb611;
        }

        .btn-u.btn-u-blue {
            background: #3498db;
        }

        .btn-u.btn-u-blue:hover,
        .btn-u.btn-u-blue:focus,
        .btn-u.btn-u-blue:active,
        .btn-u.btn-u-blue.active,
        .open .dropdown-toggle.btn-u.btn-u-blue {
            background: #2980b9;
        }

        .btn-u.btn-u-split-blue.dropdown-toggle {
            border-left: solid 1px #2980b9;
        }

        .btn-u.btn-u-red {
            background: #e74c3c;
        }

        .btn-u.btn-u-red:hover,
        .btn-u.btn-u-red:focus,
        .btn-u.btn-u-red:active,
        .btn-u.btn-u-red.active,
        .open .dropdown-toggle.btn-u.btn-u-red {
            background: #c0392b;
        }

        .btn-u.btn-u-split-red.dropdown-toggle {
            border-left: solid 1px #c0392b;
        }

        .btn-u.btn-u-orange {
            background: #e67e22;
        }

        .btn-u.btn-u-orange:hover,
        .btn-u.btn-u-orange:focus,
        .btn-u.btn-u-orange:active,
        .btn-u.btn-u-orange.active,
        .open .dropdown-toggle.btn-u.btn-u-orange {
            background: #d35400;
        }

        .btn-u.btn-u-split-orange.dropdown-toggle {
            border-left: solid 1px #d35400;
        }

        .btn-u.btn-u-sea {
            background: #1abc9c;
        }

        .btn-u.btn-u-sea:hover,
        .btn-u.btn-u-sea:focus,
        .btn-u.btn-u-sea:active,
        .btn-u.btn-u-sea.active,
        .open .dropdown-toggle.btn-u.btn-u-sea {
            background: #16a085;
        }

        .btn-u.btn-u-split-sea.dropdown-toggle {
            border-left: solid 1px #16a085;
        }

        .btn-u.btn-u-green {
            background: #2ecc71;
        }

        .btn-u.btn-u-green:hover,
        .btn-u.btn-u-green:focus,
        .btn-u.btn-u-green:active,
        .btn-u.btn-u-green.active,
        .open .dropdown-toggle.btn-u.btn-u-green {
            background: #27ae60;
        }

        .btn-u.btn-u-split-green.dropdown-toggle {
            border-left: solid 1px #27ae60;
        }

        .btn-u.btn-u-yellow {
            background: #f1c40f;
        }

        .btn-u.btn-u-yellow:hover,
        .btn-u.btn-u-yellow:focus,
        .btn-u.btn-u-yellow:active,
        .btn-u.btn-u-yellow.active,
        .open .dropdown-toggle.btn-u.btn-u-yellow {
            background: #f39c12;
        }

        .btn-u.btn-u-split-yellow.dropdown-toggle {
            border-left: solid 1px #f39c12;
        }

        .btn-u.btn-u-default {
            background: #95a5a6;
        }

        .btn-u.btn-u-default:hover,
        .btn-u.btn-u-default:focus,
        .btn-u.btn-u-default:active,
        .btn-u.btn-u-default.active,
        .open .dropdown-toggle.btn-u.btn-u-default {
            background: #7f8c8d;
        }

        .btn-u.btn-u-split-default.dropdown-toggle {
            border-left: solid 1px #7f8c8d;
        }

        .btn-u.btn-u-purple {
            background: #9b6bcc;
        }

        .btn-u.btn-u-purple:hover,
        .btn-u.btn-u-purple:focus,
        .btn-u.btn-u-purple:active,
        .btn-u.btn-u-purple.active,
        .open .dropdown-toggle.btn-u.btn-u-purple {
            background: #814fb5;
        }

        .btn-u.btn-u-split-purple.dropdown-toggle {
            border-left: solid 1px #814fb5;
        }

        .btn-u.btn-u-aqua {
            background: #27d7e7;
        }

        .btn-u.btn-u-aqua:hover,
        .btn-u.btn-u-aqua:focus,
        .btn-u.btn-u-aqua:active,
        .btn-u.btn-u-aqua.active,
        .open .dropdown-toggle.btn-u.btn-u-aqua {
            background: #26bac8;
        }

        .btn-u.btn-u-split-aqua.dropdown-toggle {
            border-left: solid 1px #26bac8;
        }

        .btn-u.btn-u-brown {
            background: #9c8061;
        }

        .btn-u.btn-u-brown:hover,
        .btn-u.btn-u-brown:focus,
        .btn-u.btn-u-brown:active,
        .btn-u.btn-u-brown.active,
        .open .dropdown-toggle.btn-u.btn-u-brown {
            background: #81674b;
        }

        .btn-u.btn-u-split-brown.dropdown-toggle {
            border-left: solid 1px #81674b;
        }

        .btn-u.btn-u-dark-blue {
            background: #4765a0;
        }

        .btn-u.btn-u-dark-blue:hover,
        .btn-u.btn-u-dark-blue:focus,
        .btn-u.btn-u-dark-blue:active,
        .btn-u.btn-u-dark-blue.active,
        .open .dropdown-toggle.btn-u.btn-u-dark-blue {
            background: #324c80;
        }

        .btn-u.btn-u-split-dark.dropdown-toggle {
            border-left: solid 1px #324c80;
        }

        .btn-u.btn-u-light-green {
            background: #79d5b3;
        }

        .btn-u.btn-u-light-green:hover,
        .btn-u.btn-u-light-green:focus,
        .btn-u.btn-u-light-green:active,
        .btn-u.btn-u-light-green.active,
        .open .dropdown-toggle.btn-u.btn-u-light-green {
            background: #59b795;
        }

        .btn-u.btn-u-split-light-green.dropdown-toggle {
            border-left: solid 1px #59b795;
        }

        .btn-u.btn-u-dark {
            background: #555;
        }

        .btn-u.btn-u-dark:hover,
        .btn-u.btn-u-dark:focus,
        .btn-u.btn-u-dark:active,
        .btn-u.btn-u-dark.active,
        .open .dropdown-toggle.btn-u.btn-u-dark {
            background: #333;
        }

        .btn-u.btn-u-split-dark.dropdown-toggle {
            border-left: solid 1px #333;
        }

        .btn-u.btn-u-light-grey {
            background: #585f69;
        }

        .btn-u.btn-u-light-grey:hover,
        .btn-u.btn-u-light-grey:focus,
        .btn-u.btn-u-light-grey:active,
        .btn-u.btn-u-light-grey.active,
        .open .dropdown-toggle.btn-u.btn-u-light-grey {
            background: #484f58;
        }

        .btn-u.btn-u-split-light-grey.dropdown-toggle {
            border-left: solid 1px #484f58;
        }

        /*Bordered Buttons*/
        .btn-u.btn-brd {
            color: #555;
            font-weight: 200;
            background: none;
            border: solid 1px transparent;
            -webkit-transition: all 0.1s ease-in-out;
            -moz-transition: all 0.1s ease-in-out;
            -o-transition: all 0.1s ease-in-out;
            transition: all 0.1s ease-in-out;
        }

        .btn-u.btn-brd:hover {
            background: none;
            border: solid 1px #eee;
        }

        .btn-u.btn-brd:focus {
            background: none;
        }

        .btn-u.btn-brd.btn-brd-hover:hover {
            color: #fff !important;
        }

        .btn-u.btn-brd {
            border-color: #72c02c;
        }

        .btn-u.btn-brd:hover {
            color: #5fb611;
            border-color: #5fb611;
        }

        .btn-u.btn-brd.btn-brd-hover:hover {
            background: #5fb611;
        }

        .btn-u.btn-brd.btn-u-blue {
            border-color: #3498db;
        }

        .btn-u.btn-brd.btn-u-blue:hover {
            color: #2980b9;
            border-color: #2980b9;
        }

        .btn-u.btn-brd.btn-u-blue.btn-brd-hover:hover {
            background: #2980b9;
        }

        .btn-u.btn-brd.btn-u-red {
            border-color: #e74c3c;
        }

        .btn-u.btn-brd.btn-u-red:hover {
            color: #c0392b;
            border-color: #c0392b;
        }

        .btn-u.btn-brd.btn-u-red.btn-brd-hover:hover {
            background: #c0392b;
        }

        .btn-u.btn-brd.btn-u-orange {
            border-color: #e67e22;
        }

        .btn-u.btn-brd.btn-u-orange:hover {
            color: #d35400;
            border-color: #d35400;
        }

        .btn-u.btn-brd.btn-u-orange.btn-brd-hover:hover {
            background: #d35400;
        }

        .btn-u.btn-brd.btn-u-sea {
            border-color: #1abc9c;
        }

        .btn-u.btn-brd.btn-u-sea:hover {
            color: #16a085;
            border-color: #16a085;
        }

        .btn-u.btn-brd.btn-u-sea.btn-brd-hover:hover {
            background: #16a085;
        }

        .btn-u.btn-brd.btn-u-green {
            border-color: #2ecc71;
        }

        .btn-u.btn-brd.btn-u-green:hover {
            color: #27ae60;
            border-color: #27ae60;
        }

        .btn-u.btn-brd.btn-u-green.btn-brd-hover:hover {
            background: #27ae60;
        }

        .btn-u.btn-brd.btn-u-yellow {
            border-color: #f1c40f;
        }

        .btn-u.btn-brd.btn-u-yellow:hover {
            color: #f39c12;
            border-color: #f39c12;
        }

        .btn-u.btn-brd.btn-u-yellow.btn-brd-hover:hover {
            background: #f39c12;
        }

        .btn-u.btn-brd.btn-u-default {
            border-color: #95a5a6;
        }

        .btn-u.btn-brd.btn-u-default:hover {
            color: #7f8c8d;
            border-color: #7f8c8d;
        }

        .btn-u.btn-brd.btn-u-default.btn-brd-hover:hover {
            background: #7f8c8d;
        }

        .btn-u.btn-brd.btn-u-dark {
            border-color: #555;
        }

        .btn-u.btn-brd.btn-u-dark:hover {
            color: #333;
            border-color: #333;
        }

        .btn-u.btn-brd.btn-u-dark.btn-brd-hover:hover {
            background: #333;
        }

        .btn-u.btn-brd.btn-u-light-grey {
            border-color: #585f69;
        }

        .btn-u.btn-brd.btn-u-light-grey:hover {
            color: #484f58;
            border-color: #484f58;
        }

        .btn-u.btn-brd.btn-u-light-grey.btn-brd-hover:hover {
            background: #484f58;
        }

        .btn-u.btn-brd.btn-u-purple {
            border-color: #9b6bcc;
        }

        .btn-u.btn-brd.btn-u-purple:hover {
            color: #814fb5;
            border-color: #814fb5;
        }

        .btn-u.btn-brd.btn-u-purple.btn-brd-hover:hover {
            background: #814fb5;
        }

        .btn-u.btn-brd.btn-u-aqua {
            border-color: #27d7e7;
        }

        .btn-u.btn-brd.btn-u-aqua:hover {
            color: #26bac8;
            border-color: #26bac8;
        }

        .btn-u.btn-brd.btn-u-aqua.btn-brd-hover:hover {
            background: #26bac8;
        }

        .btn-u.btn-brd.btn-u-brown {
            border-color: #9c8061;
        }

        .btn-u.btn-brd.btn-u-brown:hover {
            color: #81674b;
            border-color: #81674b;
        }

        .btn-u.btn-brd.btn-u-brown.btn-brd-hover:hover {
            background: #81674b;
        }

        .btn-u.btn-brd.btn-u-dark-blue {
            border-color: #4765a0;
        }

        .btn-u.btn-brd.btn-u-dark-blue:hover {
            color: #324c80;
            border-color: #324c80;
        }

        .btn-u.btn-brd.btn-u-dark-blue.btn-brd-hover:hover {
            background: #324c80;
        }

        .btn-u.btn-brd.btn-u-light-green {
            border-color: #79d5b3;
        }

        .btn-u.btn-brd.btn-u-light-green:hover {
            color: #59b795;
            border-color: #59b795;
        }

        .btn-u.btn-brd.btn-u-light-green.btn-brd-hover:hover {
            background: #59b795;
        }

        .btn-u.btn-brd.btn-u-light {
            color: #fff;
            border-color: #fff;
        }

        .btn-u.btn-brd.btn-u-light:hover {
            border-color: #fff;
        }

        .btn-u.btn-brd.btn-u-light.btn-brd-hover:hover {
            background: #fff;
            color: #555 !important;
        }

        .card {
            background-color: #fff;
            border: none;
            border-radius: 10px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
        }

        .image-container {
            position: relative
        }

        .thumbnail-image {
            border-top-left-radius: 10px !important;
            border-top-right-radius: 10px !important
        }

        .discount {
            background-color: red;
            padding-top: 1px;
            padding-bottom: 1px;
            padding-left: 4px;
            padding-right: 4px;
            font-size: 10px;
            border-radius: 6px;
            color: #fff
        }

        .wishlist {
            height: 25px;
            width: 25px;
            background-color: #eee;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
        }

        .first {
            position: absolute;
            width: 100%;
            padding: 9px
        }

        .dress-name {
            font-size: 13px;
            font-weight: bold;
            width: 75%
        }

        .new-price {
            font-size: 13px;
            font-weight: bold;
            color: red
        }

        .old-price {
            font-size: 8px;
            font-weight: bold;
            color: grey
        }

        .creme {
            background-color: #fff;
            border: 2px solid grey
        }

        .creme:hover {
            border: 3px solid grey
        }

        .creme:focus {
            background-color: grey
        }

        .red {
            background-color: #fff;
            border: 2px solid red
        }

        .red:hover {
            border: 3px solid red
        }

        .red:focus {
            background-color: red
        }

        .blue {
            background-color: #fff;
            border: 2px solid #40C4FF
        }

        .blue:hover {
            border: 3px solid #40C4FF
        }

        .blue:focus {
            background-color: #40C4FF
        }

        .darkblue {
            background-color: #fff;
            border: 2px solid #01579B
        }

        .darkblue:hover {
            border: 3px solid #01579B
        }

        .darkblue:focus {
            background-color: #01579B
        }

        .yellow {
            background-color: #fff;
            border: 2px solid #FFCA28
        }

        .yellow:hover {
            border-radius: 3px solid #FFCA28
        }

        .yellow:focus {
            background-color: #FFCA28
        }

        .item-size {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background: #fff;
            border: 1px solid grey;
            color: grey;
            font-size: 10px;
            text-align: center;
            align-items: center;
            display: flex;
            justify-content: center
        }

        .rating-star {
            font-size: 10px !important
        }

        .rating-number {
            font-size: 10px;
            color: grey
        }

        .buy {
            font-size: 12px;
            color: purple;
            font-weight: 500
        }

        .voutchers {
            background-color: #fff;
            border: none;
            border-radius: 10px;
            width: 190px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            overflow: hidden
        }

        .voutcher-divider {
            display: flex
        }

        .voutcher-left {
            width: 60%
        }

        .voutcher-name {
            color: grey;
            font-size: 9px;
            font-weight: 500
        }

        .voutcher-code {
            color: red;
            font-size: 11px;
            font-weight: bold
        }

        .voutcher-right {
            width: 40%;
            background-color: purple;
            color: #fff
        }

        .discount-percent {
            font-size: 12px;
            font-weight: bold;
            position: relative;
            top: 5px
        }

        .off {
            font-size: 14px;
            position: relative;
            bottom: 5px
        }

    </style>

    <div class="widget-list row  justify-content-center mx-0">
        <div class="col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-9">
                        <img src="{{ $loker['file_industri'] }}" alt="Image 1" style="height: 90px;">
                        <h4 class="mt-0">{{ $loker['judul'] }}</h4>
                        <p class="mb-0"><a href="#" style="color: #03a9f3">{{ $loker['industri'] }}</a> </p>
                        <p class="mb-0">Posted on {{ (new \App\Helpers\Help())->getDay($loker['tanggal_post']) }}
                        </p>
                        <p class="m-0"><b>{{ $loker['quota'] != null ? 'Dibutuhkan quota '.$loker['quota'].' pelamar' : '-'  }}</b></p>
                        <small>{{ $loker['pelamar'] }} Pelamar yang sudah mendaftar</small>
                    </div>
                    <div class="col-md-3" style="position: relative;">
                        <div class="vertical-center"
                            style="width: 80%; position: absolute; top: 50%; -ms-transform: translateY(-50%); transform: translateY(-50%);">
                            <a href="{{ route('bkk_pelamar-create', (new \App\Helpers\Help())->encode($loker['id'])) }}"
                                class="btn btn-info btn-block">Aplly Now</a>
                            <button class="btn btn-default btn-block" style="border: 1px solid #ff5a39; color: #ff5a39;"><i
                                    class="fa fa-bookmark"></i>
                                Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-10 widget-holder">
            <div class="widget-bg">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mt-0">Deskripsi Lowongan</h4>
                        <h6><b>Deskripsi : </b></h6>
                        <p class="text-justify">{{ $loker['deskripsi'] != null ? $loker['deskripsi'] : '-' }}</p>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h6><b>Persyaratan : </b></h6>
                        <p class="text-justify">{{ $loker['syarat'] != null ? $loker['syarat'] : '-' }}</p>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h4 class="mt-0">Informasi Tambahan</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><b>Kisaran Gaji : </b></h6>
                                <p>{{ number_format($loker['gaji_awal']) }} -
                                    {{ number_format($loker['gaji_sampai']) }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Tanggal Lowongan : </b></h6>
                                <span>Tanggal Buka {{ (new \App\Helpers\Help())->getTanggal($loker['tgl_buka']) }} -
                                    {{ (new \App\Helpers\Help())->getTanggal($loker['tgl_tutup']) }}</span>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h4 class="mt-0">Informasi Perusahaan</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <h6><b>Perusahaan : </b></h6>
                                <p>{{ $loker['industri'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Alamat : </b></h6>
                                <p>{{ $loker['alamat'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Deskripsi Perusahaan : </b></h6>
                                <p>{{ $loker['deskripsi_perusahaan'] }}</p>
                            </div>
                            <div class="col-md-6">
                                <h6><b>Kabupaten : </b></h6>
                                <p>{{ $loker['kabupaten'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($loker['file'] != null)
            <div class="col-md-3">
                <div class="card">
                    <div class="image-container">
                        <div class="first">
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="wishlist"><i class="fa fa-heart-o"></i></span>
                            </div>
                        </div> <img src="{{ $loker['file'] }}" class="img-fluid rounded thumbnail-image">
                    </div>
                    <div class="product-detail-container p-2">
                        <center>
                            <div style="text-align: center" class="d-flex justify-content-between align-items-center">
                                <h5 class="dress-name m-0">Flyer dari lowongan terkait</h5>
                                <a href="{{ $loker['file'] }}" target="_blank" download><span><i
                                            class="fa fa-download"></i></span></a>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="row page-title clearfix mb-2">
        <div class="page-title-left">
            <h2 class="title-box text-center">Lowongan Lainnya Untuk anda</h2>
        </div>
    </div>




    <script>
        $(document).ready(function() {
            $('.autoplay').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });

            $('.deskripsi').readall({
                showheight: 96,
                showrows: null,
                animationspeed: 200,
                btnTextShowmore: 'Selengkapya',
                btnTextShowless: 'Sembunyikan',
                btnClassShowmore: 'readall-button',
                btnClassShowless: 'readall-button'
            });
        });
    </script>
@endsection
