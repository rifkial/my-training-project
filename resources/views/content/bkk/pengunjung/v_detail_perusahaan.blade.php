@extends('content/bkk/pengunjung/main')
@section('content_bkk')

    <style>
        .wrapper {
            min-height: 100vh !important;
            margin: 0px auto !important;
            overflow: hidden !important;
        }

    </style>
    <div class="content-wrapper">
        <main class="main-wrapper clearfix">
            <section class="rounded m-5">
                <div class="row gutters-sm">
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <img src="{{ $industri['file'] }}" alt="Admin" class="rounded-circle">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <h4>{{ $industri['nama'] }}</h4>
                                <b>{{ $industri['alamat'] }}</b>
                                <p>{{ $industri['deskripsi'] }}</p>
                                <div class="sosmed">
                                    @if ($industri['email'] != null)
                                        <a href="mailto:{{ $industri['email'] }}" class="btn btn-googleplus"><i
                                                id="social-tw" class="fa fa-envelope fa-3x social"></i>&nbsp;
                                            {{ $industri['email'] }}</a>
                                    @endif
                                    @if ($industri['telepon'] != null)
                                        <a href="tel:{{ $industri['telepon'] }}" class="btn btn-twitter"><i id="social-tw"
                                                class="fa fa-phone-square fa-3x social"></i>&nbsp;
                                            {{ $industri['telepon'] }}</a>
                                    @endif
                                    @if ($industri['website'] != null)
                                        <a href="{{ $industri['website'] }}" class="btn btn-facebook"><i id="social-tw"
                                                class="fa fa-globe fa-3x social"></i>&nbsp;
                                            {{ $industri['website'] }}</a>
                                    @endif
                                </div>
                                <div class="card my-5">
                                    <div class="card-header d-flex justify-content-between alert-info">
                                        <div class="aksi">
                                            <h4>Statistic Perusahaan</h4>
                                        </div>
                                        <ul class="nav nav-pills">
                                            <li class="nav-item active"><a class="nav-link btn-purple mx-1" href="#loker"
                                                    data-toggle="tab">{{ count($industri['loker']) }} Lowongan</a>
                                            </li>
                                            <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#biosiswa"
                                                    data-toggle="tab">{{ count($industri['riwayat']) }} User yang
                                                    bekerja</a></li>
                                        </ul>

                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content pt-2">
                                            <div class="tab-pane active" id="loker">
                                                <div class="responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-facebook">
                                                                <th class="text-white">No</th>
                                                                <th class="text-white">Loker</th>
                                                                <th class="text-white">Syarat</th>
                                                                <th class="text-white">Tanggal</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (!empty($industri['loker']))
                                                                @php
                                                                    $no = 1;
                                                                @endphp
                                                                @foreach ($industri['loker'] as $loker)
                                                                    <tr>
                                                                        <td class="vertical-middle">{{ $no++ }}
                                                                        </td>
                                                                        <td class="vertical-middle">
                                                                            <a
                                                                                href="{{ route('bursa_kerja-detail_lowongan', ['code' => (new \App\Helpers\Help())->encode($loker['id']), 'title' => str_slug($loker['judul'])]) }}">
                                                                                <b>{{ ucwords($loker['judul']) }}</b>
                                                                                <p class="m-0">Bidang
                                                                                    {{ $loker['bidang'] }}</p>
                                                                                <small>{{ $loker['diposting'] }}</small>
                                                                            </a>
                                                                        </td>
                                                                        <td class="vertical-middle">
                                                                            <a
                                                                                href="{{ route('bursa_kerja-detail_lowongan', ['code' => (new \App\Helpers\Help())->encode($loker['id']), 'title' => str_slug($loker['judul'])]) }}">
                                                                                {{ str_limit($loker['syarat'], 200, '...') }}
                                                                            </a>
                                                                        </td>
                                                                        <td class="vertical-middle"><b>Dibuka</b>
                                                                            <p>{{ (new \App\Helpers\Help())->getTanggal($loker['tgl_buka']) }}
                                                                            </p>

                                                                            <b>Ditutup</b>
                                                                            <p>
                                                                                {{ (new \App\Helpers\Help())->getTanggal($loker['tgl_tutup']) }}
                                                                            </p>

                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="4" class="text-center">Loker saat ini
                                                                        belum tersedia</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="biosiswa">
                                                <div class="responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama</th>
                                                                <th>Tanggal diterima</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (!empty($industri['riwayat']))
                                                                @php
                                                                    $nomer = 1;
                                                                @endphp
                                                                @foreach ($industri['riwayat'] as $riwayat)
                                                                    <tr>
                                                                        <td>{{ $nomer++ }}</td>
                                                                        <td>{{ ucwords($riwayat['nama']) }}</td>
                                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($riwayat['tgl_diterima']) }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="3" class="text-center">Belum ada user
                                                                        yang bekerja disini</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </section>

        </main>
    </div>
@endsection
