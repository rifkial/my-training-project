@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <div class="row" style="background-color: #fff">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Profil BKK</b></h2>
                </div>
                <div class="box-body">
                    <b>1. Pengertian</b>
                    <br>
                    Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan
                    Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana
                    pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi.
                    <br><br>
                    <b>2. Latar Belakang</b>
                    <br>
                    Sekolah Menengah Kejuruan (SMK) sebagai sub sistem pendidikan nasional yang bertanggungjawab dalam
                    penyiapan SDM tingkat menengah yang handal, berorientasi kepada kebutuhan pasar harus mampu
                    mengembangkan inovasi untuk mempengaruhi perubahan kebutuhan pasar sehingga dapat mewujudkan kepuasan
                    pencari kerja. BKK SMK merupakan salah satu komponen penting dalam mengukur keberhasilan pendidikan di
                    SMK, karena BKK menjadi lembaga yang berperan mengoptimalkan penyaluran tamatan SMK dan sumber informasi
                    untuk pencari kerja. Pemberdayaan BKK SMK merupakan salah satu fungsi dalam manajemen sekolah yaitu
                    sebagai bagian pembinaan terhadap proses pelaksanaan kegiatan BKK SMK yang telah direncanakan dalam
                    upaya mencapai tujuan pendidikan SMK. BKK SMK merupakan salah satu komponen pelaksanaan pendidikan
                    sistem ganda, karena tidak mungkin bisa dilaksanakan proses pembelajaran yang mengarah kepada kompetensi
                    jika tidak ada pasangan industri/usaha kerja, sebagai lingkungan kerja dimana siswa belajar keahlian dan
                    profesional serta etos kerja sesuai dengan tuntutan dunia kerja.
                    <br><br>
                    <b>3. Tujuan</b>
                    <br>
                    Sebagai wadah dalam mempertemukan tamatan dengan pencari kerja.
                    Memberikan layanan kepada tamatan sesuai dengan tugas dan fungsi masing-masing seksi yang ada dalam BKK.
                    Sebagai wadah dalam pelatihan tamatan yang sesuai dengan permintaan pencari kerja
                    Sebagai wadah untuk menanamkan jiwa wirausaha bagi tamatan melalui pelatihan.
                    <br>
                    <br>
                    <b>4. Ruang Lingkup Kegiatan </b>
                    <br>
                    Penyusunan database siswa lulusan SMK pencari kerja dan perusahaan pencari tenaga kerja dan penelusuran
                    tamatan siswa SMK.
                    Menjaring informasi tentang pasar kerja melalui iklan di media massa, internet, kunjunagn ke dunia usaha
                    (industri) maupun kerjasama dengan lembaga penyalur tenaga kerja dan Depnakertrans.
                    Membuat leaflet informasi dan pemasaran lulusan SMK yang dikirim kedunia usaha/industri yang terkait
                    Depnakertrans.
                    Penyaluran calon tenaga kerja lulusan SMK ke dunia usaha dan industri.
                    Melakukan proses tindak lanjut hasil pengiriman dan penempatan tenaga kerja melalui kegiatan penjajakan
                    dan verifikasi.
                    Mengadakan program pelatihan ketrampilan tambahan/khusus bagi siswa dan lulusan SMK disesuaikan dengan
                    bidang keahlian yang diperlukan.
                    Mengadakan program bimbingan menghadapi tahapan proses penerimaan siswa dalam suatu pekerjaan
                    (wawancara, psikotest).
                    Memberikan informasi kepada para ALUMNI ataupun para lulusan SMK lain yang membutuhkan informasi tentang
                    lowongan kerja.
                    <br><br>
                    <b>5. Penyaluran Dan Penempatan Tamatan
                    </b>
                    <br>
                    Adapun pelaksanaan penyaluran dan penempatan tamatan yang dapat dilakukan BKK SMK adalah sebagai berikut
                    :
                    <br>
                    Menindaklanjuti kerjasama dengan industri pasangan yang telah menjadi mitra kerja dengan BKK sekolah.
                    Melakukan penelusuran alumni dan dimasukkan ke dalam database sekolah.
                    Merangkul pengurus Majelis Sekolah yang peduli dengan penempatan tenaga kerja dari alumni.
                    Membuat website khusus BKK yang selalu up to date yang dapat di link dengan situs-situs JOB CARRIER.
                    Menanamkan jiwa enterpreunership kepada siswa melalui pelatihan ketrampilan untuk menjadi seorang
                    wirausaha (enterpreuneur).
                    <br><br>
                    <b>
                        6. Kegiatan Bursa Kerja Khusus
                    </b>
                    <br>
                    Merencanakan program kerja hubungan industri setiap program studi.
                    Mengadakan pertemuan dengan Kajur tentang penempatan siswa-siswi prakerin.
                    Mengadakan koordinasi dengan panitia PSG tentang penempatan siswa-siswi prakerin.
                    Mengadakan koordinasi dengan panitia PSG tentang guru monitoring.
                    Melakukan proses negosiasi dengan DU/DI dan pemerintah sebagai mitra dalam penempatan siswa-siswi
                    prakerin.
                    Menjalin kerjasama (MOU) dengan DU/DI dalam :
                    Sinkronisasi Kurikulum
                    Pelatihan
                    Penempatan tamatan
                    Pemetaan DU/DI
                    Menjalin kerjasama dengan Depnakertrans tentang pelatihan (Magang) dan penempatan tamatan.
                    Membentuk Majelis Sekolah.
                    Membuat database penelusuran tamatan baik yang sudah bekerja maupun belum bekerja.
                    Membentuk Ikatan alumni.
                    Membuat mading informasi lowongan kerja.
                    Membuat website khusus BKK
                    Membuat Laporan Kegiatan
                    Monitoring dan Evaluasi
                </div>
            </div>
        </div>

    </div>
@endsection
