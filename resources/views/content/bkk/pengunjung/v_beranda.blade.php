@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <style>
        .wrapper,
        section {
            min-height: 100vh !important;
            margin: 0px auto !important;
            overflow: hidden !important;
        }

        .footer {
            position: relative !important;
        }

    </style>
    <div class="head bg-purple">
        <form action="javascript:void(0)" id="filterLoker" name="filterLoker">
            @csrf
            <div class="row p-3 mr-0">
                <div class="form-group col-md-3 col-xs-6 my-1">
                    <input type="text" name="judul" id="judul" placeholder="Masukan Kata kunci pencarian"
                        class="filter-type filter form-control">
                </div>
                <div class="form-group col-md-4 col-xs-6 my-1">
                    <select data-filter="type" name="provinsi" class="filter-type filter form-control select3"
                        style="height: auto">
                        <option value="">Select Provinsi</option>
                        @foreach ($provinsi as $pro)
                            <option value="{{ $pro['id'] }}">{{ $pro['name'] }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group col-md-4 col-xs-6 my-1">
                    <select data-filter="type" name="bidang" class="filter-type filter form-control" style="height: auto">
                        <option value="">Select Bidang</option>
                        @foreach ($bidang as $bd)
                            <option value="{{ $bd['id'] }}">{{ $bd['nama'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-1 col-xs-6 my-1">
                    <button type="submit" class="btn btn-primary btn-block btn-search">Cari</button>
                </div>
            </div>
        </form>
    </div>
    <div class="content-wrapper">
        <section class="pb-5" style="background: #f5f5f5">
            <div class="container">
                <div class="row md-m-25px-b m-45px-b justify-content-center text-center">
                    <div class="col-lg-8">
                        <h3 class="h1 m-15px-b">Daftar Pekerjaan</h3>
                        <p class="m-0px font-2">Temukan segera pekerjaan yang cocok dengan keahlian anda dengan mudah.</p>
                    </div>
                </div>
                <div class="row" id="listLoker">
                    @if (!empty($loker))
                        @foreach ($loker as $lk)
                            <div class="col-md-4 my-3">
                                <div class="card rounded p-2"
                                    style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)">
                                    <div class="d-flex justify-content-end px-3 pt-1"><i
                                            class="mdi mdi-star-outline pr-1 star"></i><i
                                            class="mdi mdi-dots-horizontal dot"></i>
                                    </div>
                                    <div class="px-3">
                                        <div class="round d-flex justify-content-between m-2">
                                            <img src="{{ $lk['file_industri'] }}" width="32" class="imgfix" />
                                            <span>Diposting {{ $lk['diposting'] }}</span>
                                        </div>
                                    </div>
                                    <div class="px-3 pt-3">
                                        <h3 class="name mb-0">{{ ucwords($lk['judul']) }} <span
                                                class="badge badge-success pull-right">{{ $lk['pelamar'] }}
                                                Pelamar</span></h3>
                                        <p class="mt-0">
                                            <b>{{ $lk['kabupaten'] != null ? $lk['kabupaten'] : '-' }}</b>
                                        </p>
                                        <p class="quote2">{{ Str::limit($lk['deskripsi'], 200, '...') }}
                                        </p>
                                    </div>
                                    <div class="d-flex justify-content-start px-3 align-items-center"> <i
                                            class="mdi mdi-view-comfy task"></i>
                                        <span class="quote2 pl-2">Perusahaan: {{ $lk['industri'] }}</span>
                                    </div>
                                    <div class="px-3 align-items-center pb-3">
                                        <div class="d-flex justify-content-start align-items-center"> <i
                                                class="mdi mdi-calendar-clock date"></i> <span
                                                class="quote2 pl-2">Berakhir:
                                                {{ (new \App\Helpers\Help())->getTanggal($lk['tgl_tutup']) }}</span>
                                        </div>

                                    </div>
                                    <div class="px-3 pt-3 w-100">
                                        <center>
                                            <a href="{{ route('bkk_pelamar-create', (new \App\Helpers\Help())->encode($lk['id'])) }}"
                                                class="btn btn-success"><i class="fa fa-paper-plane"></i> Lamar</a>
                                            <a class="btn btn-info"
                                                href="{{ route('bursa_kerja-detail_lowongan', ['code' => (new \App\Helpers\Help())->encode($lk['id']), 'title' => str_slug($lk['judul'])]) }}"><i
                                                    class="fa fa-info-circle"></i> Detail</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-12">
                            <div class="justify-content-center text-center">
                                <h1 class="display-5 fw-bold">Loker tidak ditemukan</h1>
                                <p class="m-0px font-2">Mohon maaf untuk saat ini loker masih belum tersedia.</p>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </section>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            lihatDetail(0);

            call_ajax('');

            $('select[name="bidang"]').on('change', function() {
                call_ajax('');
            })

            $('select[name="provinsi"]').on('change', function() {
                call_ajax('');
            })

            $('.btn-search').on('click', function() {
                call_ajax('');
            })
            let id_provinsi = null;
            let id_bidang = null;
            let judul = null;
            let vertical = true;

            $(document).on('click', '#load_more_button', function() {
                let id = $(this).data('id');
                $('#load_more_button').html('<b>Loading...</b>');
                call_ajax(id);
            });

        })




        function call_ajax(id = "") {
            $('#listLoker').html('');
            id_provinsi = $('select[name="provinsi"]').val();
            id_bidang = $('select[name="bidang"]').val();
            judul = $('input[name="judul"]').val();
            $.ajax({
                url: "{{ route('bkk_loker-search_pelamars') }}",
                method: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    id_provinsi,
                    id_bidang,
                    judul,
                    vertical: true,
                },
                success: function(data) {
                    $('#load_more_button').remove();
                    // $('#load_more_button').remove();
                    $('#listLoker').append(data.html);
                }
            });
        };

        function lihatDetail(id) {
            $('#detailLamaran').html('<div id="loading" style="" ></div>');
            $.ajax({
                url: "{{ route('bkk_loker-side_details') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id
                },
                success: function(data) {
                    $('#detailLamaran').html(data);
                }
            });
        }
    </script>
@endsection
