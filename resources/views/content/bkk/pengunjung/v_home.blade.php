@extends('content/bkk/pengunjung/main')
@section('content_bkk')
    <style>
        .carousel-item {
            height: 100vh;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .carousel-caption {
            position: absolute;
            top: 25%;
            left: 0;
            right: 0;
            bottom: 0;
            transform: translateY(-32%);
            text-align: center;
            max-height: 30vh;
        }

        ol.carousel-indicators {
            display: none !important;
        }

    </style>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active"
                    style="background-image: url('https://hrdspot.com/wp-content/uploads/job-description.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                        <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <p class="lead">{{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">{{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item"
                    style="background-image: url('https://2e8ram2s1li74atce18qz5y1-wpengine.netdna-ssl.com/wp-content/uploads/2019/01/cropped-shutterstock_669226057-1-1024x576.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                         <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                       <p class="lead">{{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">{{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item"
                    style="background-image: url('https://image.freepik.com/free-photo/business-job-interview-concept_1421-77.jpg')">
                    <div class="carousel-caption d-none d-md-block">
                         <img src=" {{ $config != null ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                       <p class="lead">{{ $config != null ? $config['deskripsi'] : 'Selamat datang di Program BURSA KERJA' }}</p>
                        <h2 class="display-4">{{ $config != null ? strtoupper($config['judul']) : strtoupper($sekolah['nama']) }}</h2>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>

@endsection
