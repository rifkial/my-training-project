@extends('template/template_default/app')
@section('content')
    <style>
        div#table_riwayat_length {
            padding-top: .755em;
            margin-top: 1.42857em;
        }

    </style>

    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60 widget-bg">
                    <div class="col-md-12">
                        <div class="card card-outline-info">
                            <div class="card-header p-1">
                                <h5 class="panel-title m-2"><i class="fa fa-database" aria-hidden="true"></i> Data User
                                    {{ Session::get('title') }}</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><strong>Status :</strong></label>
                                            <select id="status_from" class="form-control">
                                                <option value="">--Select status--</option>
                                                <option value="1" selected>Active</option>
                                                <option value="0">Non Aktif / Pending</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="data-tabel" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Gambar</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telepon</th>
                                                <th>Status</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="riwayatModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Riwayat Pendaftaran</h5>
                </div>
                <div class="modal-body">
                    <div id="riwayat" style="width: 100%;">

                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        var url_import = "{{ route('import-user_bkk') }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        datables(1, "perusahaan")
        function datables(ids, data_aksi) {
            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('user_bkk-get_datatable') }}",
                    "type": "POST",
                    "data": function(d) {
                        d.status = ids;
                        d.aksi = data_aksi;
                    },
                },
                destroy : true,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'aksi_supervisor',
                        name: 'aksi_supervisor',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }

        $('#status_from').on('change', function() {
            ids = $(this).val();
            datables(ids, "perusahaan");
        })

        

        $(document).on('click', '.riwayat', function() {
            var id = $(this).data('id');
            $('#riwayat').html(
                '<div class="table-responsive" style="margin-top: 14px;"><table class="table table-striped" id="table_riwayat" style="width: 100%"><thead><tr><th>#</th><th>Nama</th><th>Posisi</th><th>Industri</th><th>Status</th><th>Tanggal Apply</th></tr></thead></table></div>'
            );
            $('#riwayatModal').modal('show');
            riwayatData(id);
        });

        

        function riwayatData(id_user) {
            var table_trash = $('#table_riwayat').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('bkk_pelamar-admin_riwayat') }}",
                    "type": "POST",
                    "data": function(d) {
                        d.id_user = id_user;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'loker',
                        name: 'loker'
                    },
                    {
                        data: 'industri',
                        name: 'industri'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },

                ]
            });
        }

        

    </script>

@endsection
