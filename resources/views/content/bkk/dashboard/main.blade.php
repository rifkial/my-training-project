<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet"
        type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <style>
            .pace {
                display: none;
            }

        </style>
        @php
            $program = Session::get('config');
            $role = Session::get('role');
        @endphp
        @include('content.bkk.dashboard.menu.menu-'.$role)
        {{-- @include('content.bkk.dashboard.menu-'.$role) --}}
        <div class="content-wrapper">
            <main class="main-wrapper clearfix">
                @yield('content_dashboard')
            </main>
        </div>
        @include('includes.footer')
    </div>
    @include('includes.foot')
</body>

</html>
