@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        #loading {
            text-align: center;
            background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
        }

        .error-template {
            padding: 40px 15px;
            text-align: center;
        }

        .error-actions {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .error-actions .btn {
            margin-right: 10px;
        }

        section.row.custom-scroll-content.scrollbar-enabled {
            overflow-y: scroll;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Lamaran Pekerjaan Kamu</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Bursa Kerja</li>
                <li class="breadcrumb-item active">Pekerjaan</li>
            </ol>
        </div>
    </div>

    <div class="widget-list">
        <div class="row" style="padding: 10px; background-color: #fff">
            <div class="col-lg-12 col-md-12" style="margin-top: 12px">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(10)">Semua Lamaran</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(20)">Lamaran yang aktif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(1)">Terkirim</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(2)">Diproses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(4)">Diterima</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" data-toggle="tab" aria-expanded="true"
                                onclick="return load_lamaran(3)">Ditolak</a>
                        </li>
                    </ul>
                    <!-- /.nav-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="row" id="tampilanLoker">
                                @if ($pelamar == null)
                                    <div class="col-md-12">
                                        <div class="error-template">
                                            <h1>Oops!</h1>
                                            <h2>404 Data Not Found</h2>
                                            <div class="error-details">Sorry, an error has occured, Requested page not
                                                found!</div>
                                            <div class="error-actions"><a href="http://www.jquery2dotnet.com"
                                                    class="btn btn-primary btn-lg"><span
                                                        class="glyphicon glyphicon-home"></span>Take Me Home </a><a
                                                    href="http://www.jquery2dotnet.com" class="btn btn-default btn-lg"><span
                                                        class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @foreach ($pelamar as $pel)
                                    <div class="col-sm-6 mt-3">
                                        <div class="card">
                                            <header class="text-center">
                                                <div class="float-right dropdown mr-1">
                                                    <a href="javascript:void(0);" data-toggle="dropdown"
                                                        class="dropdown-toggle">
                                                        <i class="material-icons list-icon text-color-scheme-dark">menu</i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right p-0"
                                                        style="width: 13.85714em">
                                                        <a class="dropdown-item" href="javascript:void(0);">
                                                            Lihat Profil yang diunggah </a>
                                                        @if ($pel['status'] != 'Dibatalkan')
                                                            <a class="dropdown-item" href="javascript:void(0);">
                                                                Batalkan Aplikasi </a>

                                                        @endif
                                                    </div>
                                                </div>
                                            </header>
                                            <div class="card-body row">
                                                <div class="col-sm-8">
                                                    <h5 class="card-title mb-1">{{ ucwords($pel['loker']) }} &nbsp;&nbsp;
                                                        <a href="javascript:void(0)"
                                                            onclick="detailLoker({{ $pel['id_loker'] }})" id="detail-loader" class="detail"><small> <i
                                                                    class="fa fa-eye"></i> detail</small></a>
                                                    </h5>

                                                    <p>{{ ucwords($pel['industri']) }}</p>
                                                    @php
                                                        $icon = '';
                                                        $color = '';
                                                        if ($pel['status'] == 'Terkirim') {
                                                            $icon = '-check-circle';
                                                            $color = 'blue';
                                                        } elseif ($pel['status'] == 'Diproses') {
                                                            $icon = '-clock-o';
                                                            $color = 'red';
                                                        } elseif ($pel['status'] == 'Diterima') {
                                                            $icon = '-thumbs-up';
                                                            $color = 'green';
                                                        } else {
                                                            $icon = '-times-circle';
                                                            $color = '#556080';
                                                        }
                                                    @endphp
                                                    <p class="card-text mb-0"><i class="fa fa{{ $icon }}"
                                                            style="color: {{ $color }}"></i> {{ $pel['status'] }}
                                                        {{ $pel['waktu'] }}.</p>
                                                    <p class="card-text mb-0"><i class="fa fa-user"></i>
                                                        {{ $pel['pelamar'] }} pelamar
                                                        <small>Bandingkan</small>.
                                                    </p>
                                                    <p class="card-text"><i class="fa fa-calendar"></i>
                                                        {{ $pel['agenda'] }} Agenda.
                                                    </p>
                                                    <a href="#"><small>Sudah melamar tanggal
                                                            {{ $pel['waktu'] }}</small></a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <img src="http://localhost/smart_school/public/images/default.png"
                                                        alt="" style="height: 150px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg-color-scheme" id="detailModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0" style="border: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12" id="profilDetail">
                        <div id="content" class="content content-full-width">
                            <div class="profile">
                                <div class="profile-header">
                                    <div class="profile-header-cover"></div>
                                    <div class="profile-header-content">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <div class="profile-header-img m-0">
                                                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt=""
                                                            style="width: 148px;">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="profile-header-info p-2">
                                                        <h4 class="mt-0 m-b-5">Front End Developer</h4>
                                                        <p class="m-b-10">PT Trisula Indonesia</p>
                                                        <p class="mb-0">Bandung</p>
                                                        <p class="mb-0">Posted on 02 Juni 2021</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-content">
                                <div class="tab-content p-0">
                                    <section class="row custom-scroll-content scrollbar-enabled" style="height: 470px;">
                                        <div class="tab-pane fade in active show" id="profile-about"
                                            style="width: 100%; padding: 15px;">
                                            <h5 class="box-title"><i class="fa fa-info-circle"></i> Detail Lowongan Kerja
                                            </h5>
                                            <div>
                                                <p class="mb-0"><b>Deskripsi Pekerjaan</b></p>
                                                <p>It is a long established fact that a reader will be distracted by the
                                                    readable content of a page when looking at its layout. The point of
                                                    using Lorem Ipsum is that it has a more-or-less normal distribution of
                                                    letters, as opposed to using 'Content here, content here', making it
                                                    look like readable English. Many desktop publishing packages and web
                                                    page editors now use Lorem Ipsum as their default model text, and a
                                                    search for 'lorem ipsum' will uncover many web sites still in their
                                                    infancy. Various versions have evolved over the years, sometimes by
                                                    accident, sometimes on purpose (injected humour and the like)</p>
                                                <hr>
                                                <p class="mb-0"><b>Syarat Pekerjaan</b></p>
                                                <p>It is a long established fact that a reader will be distracted by the
                                                    readable content of a page when looking at its layout. The point of
                                                    using Lorem Ipsum is that it has a more-or-less normal distribution of
                                                    letters, as opposed to using 'Content here, content here', making it
                                                    look like readable English. Many desktop publishing packages and web
                                                    page editors now use Lorem Ipsum as their default model text, and a
                                                    search for 'lorem ipsum' will uncover many web sites still in their
                                                    infancy. Various versions have evolved over the years, sometimes by
                                                    accident, sometimes on purpose (injected humour and the like)</p>
                                                <div class="table-responsive">
                                                    <hr>
                                                    <table style="width: 100%">
                                                        <tbody id="outputPengalaman">
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: top">
                                                                    Gaji
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    10.0000 - 20.000
                                                                <td>
                                                            </tr>
                                                            <tr class="mt-2">
                                                                <td style="vertical-align: top">
                                                                    Tanggal Buka
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    20 agustus 2019 - 20 januari 2021
                                                                <td>
                                                            </tr>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <h5 class="box-title"><i class="fa fa-calendar"></i> Agenda Lowongan
                                                Pekerjaan
                                            </h5>
                                            <div>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="width: 100%">
                                                        <tr>
                                                            <th>Acara</th>
                                                            <th>Tanggal Mulai</th>
                                                            <th>Tanggal Selesai</th>
                                                            <th>Lokasi</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Industri</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industri</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industri</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                            <td>Travel / Pariwisata</td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // load_lamaran(10);

        function load_lamaran(id) {
            $('#tampilanLoker').html('<div id="loading" style="" ></div>');
            $.ajax({
                url: "/program/bursa_kerja/pelamar/get_data",
                type: "POST",
                data: {
                    id
                },
                success: function(data) {
                    if (data.count != 0) {
                        $('#tampilanLoker').html(data.html);
                        $('.countlist').html('Tersedia <span>' + data.count + '</span> Lowongan Kerja');
                    } else {
                        $('#tampilanLoker').html(
                            '<div class="col-md-12"><div class="error-template"><h1>Oops!</h1><h2>404 Data Not Found</h2><div class="error-details">Sorry, an error has occured, Requested page not found!</div><div class="error-actions"><a href="http://www.jquery2dotnet.com" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a><a href="http://www.jquery2dotnet.com" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a></div></div></div>'
                        );
                        $('.countlist').html('Tersedia <span>' + data.count + '</span> Lowongan Kerja');
                    }
                }
            });
        }

        function detailLoker(id) {
            $.ajax({
                url: "/program/bursa_kerja/lowongan/detail_by_pelamar",
                type: "POST",
                data: {
                    id
                },
                success: function(data) {
                    // console.log(data);
                    $('#profilDetail').html(data);
                    $('#detailModal').modal('show');
                }
            });
        }

    </script>

@endsection
