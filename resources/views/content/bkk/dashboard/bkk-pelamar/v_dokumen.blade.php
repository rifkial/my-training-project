@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        a.edit.btn.btn-info.btn-sm.edit {
            margin-right: 5px;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-lg-12 col-md-12" style="margin-top: 12px;">
                <div class="card">
                    <div class="card-header bg-info">
                        <i class="fa fa-list"></i> Dokumen Saya
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-sm" id="addDokumen"
                                style="line-height: normal;">
                                <i class="fa fa-plus"></i> Tambah Dokumen
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Jenis</th>
                                            <th>File</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="addForm" name="addForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_dokumen">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Dokumen</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="jenis" name="jenis"
                                            placeholder="contoh: KTP/Ijazah/SKHU dll" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">File</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="application/pdf">
                                        <br><small>*gunakan file type pdf max size 10Mb</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#data-tabel').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'jenis',
                    name: 'jenis'
                },
                {
                    data: 'download',
                    name: 'download'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        $('#addDokumen').click(function() {
            $('#saveBtn').val("create-Customer");
            $('#Customer_id').val('');
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah File Dokumen");
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        });

        $('body').on('submit', '#addForm', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $('#saveBtn').html('Sending..');
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('bkk_dokumen-save') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('bkk_dokumen-update') }}";
                method_url = "PUT";
            }

            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: action_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#addForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        noti(data.icon, data.success);
                    } else {
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                    }

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $(document).on('click', '.edit', function() {
            var id = $(this).data('id');
            var self = $(this);
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "dokumen/edit",
                data: {
                    id_dokumen: id
                },
                beforeSend: function() {
                    $(self).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(self).html('<i class="fa fa-pencil-square-o"></i> Edit');
                    $('#modelHeading').html("Edit File Dokumen");
                    $('#id_dokumen').val(data.id);
                    $('#jenis').val(data.jenis);
                    $('#action').val('Edit');
                    $('#ajaxModel').modal('show');
                }
            });
        });

    </script>
@endsection
