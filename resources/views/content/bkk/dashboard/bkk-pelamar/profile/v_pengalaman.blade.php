@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="tab-pane active" id="home-tab-v1">
        <h5 class="box-title"><i class="fa fa-shopping-bag"></i> Pengalaman
        </h5>
        <div>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20%">Tingkat Pengalaman</td>
                    <td>Tambahkan pengalamanmu untuk menarik recruitment
                    <td>
                    <td></td>
                </tr>
            </table>
            <hr>
            <table style="width: 100%">

                @if (empty($pengalaman))
                    <tr>
                        <td colspan="3">
                            <form id="formPengalaman" name="formPengalaman" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l0">Posisi *</label>
                                    <div class="col-md-9">
                                        <input name="id" id="id_pengalaman" type="hidden">
                                        <input name="id_user" id="id_user" type="hidden">
                                        <input class="form-control" name="nama" id="l0" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Nama Perusahaan</label>
                                    <div class="col-md-9">
                                        <input class="form-control" name="industri" id="l10" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l10">Provinsi</label>
                                    <div class="col-md-9">
                                        <select name="provinsi" id="provinsi" class="form-control select3">
                                            <option value="">Provinsi</option>
                                            @foreach ($provinsi as $pr)
                                                <option value="{{ $pr['name'] }}">{{ $pr['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l11">Lama Bekerja</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="bulan_dari" id="bulan_dari" class="form-control select3">
                                                    <option value="">Bulan</option>
                                                    <option value="01">Januari</option>
                                                    <option value="02">Februari</option>
                                                    <option value="03">Maret</option>
                                                    <option value="04">April</option>
                                                    <option value="05">Mei</option>
                                                    <option value="06">Juni</option>
                                                    <option value="07">Juli</option>
                                                    <option value="08">Agustus</option>
                                                    <option value="09">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="tahun_dari" class="form-control select3" id="tahun_dari">
                                                    <option value="">Tahun</option>
                                                    @php
                                                        $firstYear = (int) date('Y');
                                                        $lastYear = $firstYear - 20;
                                                        for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                            echo '<option value=' . $year . '>' . $year . '</option>';
                                                        }
                                                    @endphp
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="form-control-plaintext">sampai</p>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="bulan_sampai" class="form-control select3" id="bulan_sampai">
                                                    <option value="">Bulan</option>
                                                    <option value="01">Januari</option>
                                                    <option value="02">Februari</option>
                                                    <option value="03">Maret</option>
                                                    <option value="04">April</option>
                                                    <option value="05">Mei</option>
                                                    <option value="06">Juni</option>
                                                    <option value="07">Juli</option>
                                                    <option value="08">Agustus</option>
                                                    <option value="09">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="tahun_sampai" class="form-control select3" id="tahun_sampai">
                                                    <option value="">Tahun</option>
                                                    @php
                                                        $firstYear = (int) date('Y');
                                                        $lastYear = $firstYear - 20;
                                                        for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                            echo '<option value=' . $year . '>' . $year . '</option>';
                                                        }
                                                    @endphp
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="alamat" id="l15" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15">Deskripsi</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="deskripsi" id="l15" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l15"></label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="action" id="action" value="Add" />
                                        <button type="submit" class="btn btn-outline-info btn-sm" id="saveBtn"
                                            value="create">Simpan</button>
                                        <a class="btn btn-outline-danger btn-sm" id="btnCancel"
                                            style="color: #e6614f">Batalkan</a>
                                    </div>
                                </div>
        </div>
        </form>
        </td>
        </tr>
    @else
        <tr>
            <td style="width: 20%"></td>
            <td></td>
            <td></td>
            <td style="text-align: right;">
                <a href="javascript:void(0)" id="addExperience" class="btn btn-sm btn-outline-info">
                    <i class="fa fa-plus-circle"></i> Tambah
                </a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tbody id="outputPengalaman">

            @foreach ($pengalaman as $pl)
                <tr class="mt-2">
                    <td style="vertical-align: top">
                        {{ (new \App\Helpers\Help())->getMonthYear($pl['tgl_mulai']) . ' - ' . (new \App\Helpers\Help())->getMonthYear($pl['tgl_akhir']) }}
                        <br><small>{{ $pl['jumlah_bulan']  }} month</small>
                    </td>
                    <td style="vertical-align: top">
                        <h5 class="mt-0">{{ ucwords($pl['nama']) }}</h5>
                        <b>{{ ucwords($pl['industri']) }}
                            {{ $pl['provinsi'] != null ? '| ' . ucwords($pl['provinsi']) . ', INDONESIA' : '' }}</b>
                        <table style="width: 100%">
                            <tr>
                                <td>Industri</td>
                                <td>{{ ucwords($pl['industri']) }}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>{{ $pl['alamat'] }}</td>
                            </tr>
                        </table>
                        @if ($pl['deskripsi'] != null)
                            <small>Job desc : {{ $pl['deskripsi'] }}</small>
                        @endif

                    <td>
                    <td style="text-align: right; vertical-align: top">
                        <a href="javascript:void(0)" data-id="{{ $pl['id'] }}" class="editPengalaman"><i
                                class="fa fa-edit"></i></a> &nbsp; &nbsp;
                        <a href="javascript:void(0)" onclick="deletePengalaman({{ $pl['id'] }})"><i
                                class="fa fa-trash"></i></a>

                    </td>
                </tr>
            @endforeach
        </tbody>
        @endif
        </table>
    </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="pengalamanModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formPengalaman" name="formPengalaman" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Posisi *</label>
                            <div class="col-md-9">
                                <input name="id" id="id_pengalaman" type="hidden">
                                <input name="id_user" id="id_user" type="hidden">
                                <input class="form-control" name="nama" id="nama" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Nama Perusahaan</label>
                            <div class="col-md-9">
                                <input class="form-control" name="industri" id="industri" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Provinsi</label>
                            <div class="col-md-9">
                                <select name="provinsi" id="provinsi" class="form-control select3">
                                    <option value="">Provinsi</option>
                                    @foreach ($provinsi as $pr)
                                        <option value="{{ $pr['name'] }}">{{ $pr['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l11">Lama Bekerja</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="bulan_dari" id="bulan_dari" class="form-control select3">
                                            <option value="">Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_dari" class="form-control select3" id="tahun_dari">
                                            <option value="">Tahun</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="form-control-plaintext">sampai</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="bulan_sampai" class="form-control select3" id="bulan_sampai">
                                            <option value="">Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_sampai" class="form-control select3" id="tahun_sampai">
                                            <option value="">Tahun</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Deskripsi</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#addExperience').click(function() {
            $('#pengalamanModal').modal('show');
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah Data Pengalaman");
        })

        $('#formPengalaman').on('submit', function(event) {
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('bkk_pengalaman-create') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('bkk_pengalaman-update') }}";
                method_url = "PUT";
            }

            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    // console.log(data);
                    if (data.status == 'berhasil') {
                        $('#formPengalaman').trigger("reset");
                        $('#pengalamanModal').modal('hide');
                        location.reload(true);
                    }
                    $('#saveBtn').html('Simpan');
                    noti(data.success, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $(document).on('click', '.editPengalaman', function() {
            var id = $(this).data('id');
            var animasi = $(this);
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "pengalaman/edit",
                data: {
                    id
                },
                beforeSend: function() {
                    $(animasi).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(animasi).html('<i class="fa fa-edit"></i>');
                    $('#modelHeading').html("Edit Data Pengalaman");
                    $('#id_pengalaman').val(data.id);
                    $('#id_user').val(data.id_user);
                    $("#nama").val(data.nama);
                    $("#industri").val(data.industri);
                    $("#provinsi").val(data.provinsi).trigger('change');
                    $("#bulan_dari").val(data.bulan_mulai).trigger('change');
                    $("#tahun_dari").val(data.tahun_mulai).trigger('change');
                    $("#bulan_sampai").val(data.bulan_akhir).trigger('change');
                    $("#tahun_sampai").val(data.tahun_akhir).trigger('change');
                    $("#alamat").val(data.alamat);
                    $("#deskripsi").val(data.deskripsi);
                    $('#action').val('Edit');
                    $('#pengalamanModal').modal('show');
                }
            });
        });

        function deletePengalaman(id) {
            if (id) {
                var confirmdelete = confirm("Do you really want remove data?");
                var animasi = $(this);
                if (confirmdelete == true) {
                    $.ajax({
                        type: 'POST',
                        url: "pengalaman/soft_delete",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(animasi).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload(true);
                            }
                            swa(data.status + "!", data.message, data.success);

                        }
                    });
                }

            }
        }

    </script>
@endsection
