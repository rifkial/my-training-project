@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <style>
        #avatar {
            background-image: url('{{ $data['file'] }}');
            background-size: 200px 200px;
            height: 200px;
            width: 200px;
            border: 3px solid #0af;
            border-radius: 50%;
            transition: background ease-out 200ms;
        }

        #preview {
            position: relative;
        }

        input[type="file"] {
            display: none;
        }

        #upload-button {
            padding: 18px;
            width: 58px;
            height: 58px;
            border-radius: 50%;
            border: none;
            cursor: pointer;
            background-color: #08f;
            box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
                0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);
            transition: background-color ease-out 120ms;
            position: absolute;
            right: 9%;
            bottom: 0%;
        }

        #upload-button:hover {
            background-color: #45a;
        }

    </style>
    <div class="tab-pane active" id="home-tab-v1">
        <div>
            <table class="my-profile" style="width: 100%">
                <tr>
                    <td></td>
                    <td></td>
                    <td style="text-align: center">Last Updated: {{ (new \App\Helpers\Help())->getTanggal($data['updated_at']) }}</td>
                </tr>
                <tr>
                    <td rowspan="6">
                        <form id="formProfil" action="javascript:void(0)" enctype="multipart/form-data">
                            @csrf
                            <center>
                                <main>
                                    <input type="hidden" name="id" value="{{ $data['id'] }}">
                                    <input type="file" name="image" id="image" accept="image/*" />
                                    <div id="preview">
                                        <div id="avatar"></div>
                                        <a href="javascript:void(0)" id="upload-button" aria-labelledby="image"
                                            aria-describedby="image">
                                            <i class="fa fa-upload" style="color: #fff"></i>
                                        </a>
                                    </div>
                                </main>
                                @php
                                    $gambars = explode('/', $data['file']);
                                    $gambar = end($gambars);
                                @endphp
                                @if ($gambar != 'default.png')
                                    <input type="checkbox" name="remove_photo" value="delete_check"> Remove Photo <br>
                                @endif

                                <button type="submit" class="btn btn-success btn-sm btn-image mt-2"
                                    style="text-align: center"><i class="fa fa-refresh"></i> Update</button>
                            </center>
                        </form>
                    </td>
                    <td>{{ ucwords($data['nama']) }}</td>
                    <td style="text-align: center">
                        <a href="update/pelamar"><i class="fa fa-pencil-square-o"></i></a> &nbsp; &nbsp;
                        <i class="fa fa-print"></i>
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Str::upper($data['role']) }}
                    <td>
                    <td></td>
                </tr>
                <tr>
                    <td><i class="fa fa-phone"></i> {{ $data['telepon'] != null ? $data['telepon'] : '-' }} | <i
                            class="fa fa-envelope"></i>
                        {{ $data['email'] != null ? $data['email'] : '-' }}
                    <td>
                    <td></td>
                </tr>
                <tr>
                    <td><i class="fa fa-map-marker"></i> {{ $data['alamat'] != null ? $data['alamat'] : '-' }}
                    <td>
                    <td></td>
                </tr>
            </table>
            <hr>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20%">
                        <h5 class="box-title"><i class="fa fa-paperclip"></i> PENDIDIKAN
                        </h5>
                    </td>
                    <td></td>
                </tr>
                @if (empty($data['pendidikan']))
                    <tr>
                        <td colspan="2" class="text-center">Data pendidikan saat ini tidak tersedia</td>
                    </tr>
                @else
                    @foreach ($data['pendidikan'] as $pd)
                        <tr>
                            <td>
                                <small>{{ (new \App\Helpers\Help())->getMonthYear($pd['tahun_lulus']) }}</small>
                            </td>
                            <td>
                                <b>{{ Str::upper($pd['nama']) }}</b>
                                <br>
                                {{ $pd['jurusan'] }} | {{ $pd['jenjang'] }}
                            </td>
                        </tr>
                    @endforeach
                @endif


            </table>
            <hr>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20%; vertical-align: top">
                        <h5 class="box-title"><i class="fa fa-paperclip"></i> INFORMASI TAMBAHAN
                        </h5>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th class="vartical-middle">
                        Ketrampilan
                    </th>
                    <td>
                        <ul class="list-unstyled">
                            @if (empty($data['keterampilan']))
                                <small class="text-danger">*Anda belum menambahkan keterampilan</small>
                            @else
                                @foreach ($data['keterampilan'] as $kt)
                                    <li>{{ ucwords($kt['nama']) }}</li>
                                @endforeach
                            @endif
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td rowspan="{{ count($data['pengalaman'])+1 }}">
                        Pengalaman
                    </td>
                    <td>
                        @if (empty($data['pengalaman']))
                           <small class="text-danger">*Anda belum memabahkan pengalaman</small>
                        @endif
                    </td>
                </tr>
                @if (!empty($data['pengalaman']))
                    @foreach ($data['pengalaman'] as $pl)
                        <tr>
                            <td>
                                <b>{{ $pl['nama'] . ' ->  ' . $pl['industri'] }}
                                </b>
                            </td>
                        </tr>
                    @endforeach
                @endif

            </table>

        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="resumeModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeadings">Import Data Jurusan</h5>
                </div>
                <form id="importFile" name="importFile" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u>
                                            <a href="javascript:void(0)" onclick="return template()"
                                                style="color:#03a9f3">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        const UPLOAD_BUTTON = document.getElementById("upload-button");
        const FILE_INPUT = document.querySelector("input[type=file]");
        const AVATAR = document.getElementById("avatar");

        UPLOAD_BUTTON.addEventListener("click", () => FILE_INPUT.click());

        FILE_INPUT.addEventListener("change", event => {
            const file = event.target.files[0];

            const reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onloadend = () => {
                AVATAR.setAttribute("aria-label", file.name);
                AVATAR.style.background = `url(${reader.result}) center center/cover`;
            };
        });


        $('body').on('submit', '#formProfil', function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "update_image",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-image").attr("disabled", true);
                    $(".btn-image").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: (data) => {
                    if (data.status == 'berhasil') {
                        // noti(data.icon, data.success);
                        location.reload(true);
                    }
                    noti(data.icon, data.success);
                    $('#saveBtn').html('Simpan');
                    $(".btn-image").html('<i class="fa fa-refresh"></i> Update');
                    $(".btn-image").attr("disabled", false);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });
    </script>
@endsection
