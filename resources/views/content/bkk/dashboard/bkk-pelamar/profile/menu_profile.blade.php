<li class="nav-item {{ Request::segment(4) === 'edit' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/profile/edit') }}" class="nav-link">Profil saya</a>
</li>
<li class="nav-item {{ Request::segment(3) === 'pengalaman' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/pengalaman') }}" class="nav-link">Pengalaman</a>
</li>
<li class="nav-item {{ Request::segment(3) === 'pendidikan' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/pendidikan') }}" class="nav-link">Pendidikan</a>
</li>
<li class="nav-item {{ Request::segment(3) === 'keterampilan' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/keterampilan') }}" class="nav-link">Ketrampilan</a>
</li>
{{-- <li class="nav-item">
    <a class="nav-link">Bahasa</a>
</li>
<li class="nav-item">
    <a class="nav-link">Info lain</a>
</li> --}}
<li class="nav-item {{ Request::segment(4) === 'resume' ? 'active' : '' }}">
    <a href="{{ url('program/bursa_kerja/dokumen/resume') }}" class="nav-link">Resume diunggah</a>
</li>
