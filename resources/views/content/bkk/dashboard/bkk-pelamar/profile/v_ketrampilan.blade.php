@extends('content/bkk/dashboard/bkk-pelamar.profile.v_profile')
@section('content_profile_pelamar')
    <div class="tab-pane active" id="home-tab-v1">
        <h5 class="box-title"><i class="fa fa-cogs"></i> Ketrampilan
        </h5>
        <div>
            <table style="width: 100%">
                <tr>
                    <td>Tambahkan 2 jenjang pendidikan yang paling tinggi</td>
                </tr>
            </table>
            <hr>
            <table style="width: 100%">
                <tr>
                    <td colspan="3">
                        <form id="formPendidikan" name="formPendidikan" class="form-horizontal">
                            @csrf

                            <div class="table-responsive">
                                <table style="width: 90%" id="dynamic_field">
                                    <tr>
                                        <td>Keterampilan</td>
                                        <td>Tingkat</td>
                                    </tr>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($keterampilan as $kt)
                                        <tr id="row{{ $no }}">
                                            <td><input type="text" name="name[]" value="{{ $kt['nama'] }}"
                                                    class="form-control name_list" /></td>
                                            <td>
                                                <select name="tingkat[]" id="" class="form-control">
                                                    <option value="tingkat_lanjutan"
                                                        {{ $kt['tingkat'] == 'tingkat_lanjutan' ? 'selected' : '' }}>
                                                        Tingkat Lanjutan</option>
                                                    <option value="menengah"
                                                        {{ $kt['tingkat'] == 'menengah' ? 'selected' : '' }}>Menengah
                                                    </option>
                                                    <option value="pemula"
                                                        {{ $kt['tingkat'] == 'pemula' ? 'selected' : '' }}>Pemula</option>
                                                </select>
                                            </td>
                                            <td><button type="button" name="remove" id="{{ $no++ }}"
                                                    class="btn btn-danger btn_remove"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <div class="mt-2">
                                    <a href="javasript:void(0)" id="add"><i class="fa fa-plus-circle"></i> Tambahkan
                                        ketrampilan</a>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-9">
                                    <input type="hidden" name="action" id="action" value="Add" />
                                    <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                        value="create">Simpan</button>
                                    <a class="btn btn-outline-danger" id="btnCancel" style="color: #e6614f">Batalkan</a>
                                </div>
                            </div>
        </div>
        </form>
        </td>
        </tr>

        </table>
    </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var i = 1;
        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i +
                '"><td><input type="text" name="name[]"  class="form-control name_list" /></td><td><select name="tingkat[]" id="" class="form-control"><option value="tingkat_lanjutan">Tingkat Lanjutan</option><option value="menengah">Menengah</option><option value="pemula">Pemula</option></select></td><td><button type="button" name="remove" id="' +
                i + '" class="btn btn-danger btn_remove"><i class="fa fa-trash"></i></button></td></tr>');
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
        $('#formPendidikan').on('submit', function(event) {
            $('#saveBtn').html('Sending..');
            event.preventDefault();
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('bkk_keterampilan-create') }}";
                method_url = "POST";
            }

            if ($('#action').val() == 'Edit') {
                action_url = "{{ route('bkk_keterampilan-update') }}";
                method_url = "PUT";
            }

            $.ajax({
                url: action_url,
                method: method_url,
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    // if (data.status == 'berhasil') {
                    //     $('#formPendidikan').trigger("reset");
                    //     // $('#pendidikanModal').modal('hide');
                    //     // location.reload(true);
                    // }
                    $('#saveBtn').html('Simpan');
                    noti(data.success, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });

        });
    </script>
@endsection
