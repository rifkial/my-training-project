@extends('content/bkk/dashboard/main')
@section('content_dashboard')

    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h2 class="box-title m-3">Profle pelamar</h2>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Bursa Kerja</li>
                <li class="breadcrumb-item active">Info Profile</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="">
                        <div class="widget-body clearfix">
                            <div class="tabs tabs-vertical">
                                <ul class="nav nav-tabs flex-column widget-bg p-0" style="flex-basis: 15.71429em;">
                                    <div class="row">
                                        <a href="{{ url('program/bursa_kerja/profile/preview') }}">
                                            <div class="media align-items-center p-3 bg-light-grey">
                                                <div class="d-flex ml-1 mr-2 w-25 justify-content-end">
                                                    <figure class="mb-0 thumb-md">
                                                        <img id="modal-preview" alt="Preview" src="{{ $profile['file'] }}"
                                                            class="img-thumbnail">
                                                    </figure>
                                                </div>
                                                <div class="media-body w-75">
                                                    <h6 class="text-muted text-uppercase">{{ ucwords($profile['nama']) }}
                                                    </h6>
                                                    <p class="mr-t-0">Lihat profil saya</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @include('content.bkk.dashboard.bkk-pelamar.profile.menu_profile')
                                </ul>
                                <div class="tab-content widget-bg ml-3 p-2">
                                    @yield('content_profile_pelamar')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- </div> --}}
        </div>
    </div>
    <script>

    </script>
@endsection
