@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <div class="widget-list">
        <div class="row">
            {{-- <div class="col-12 col-md-4 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="contact-info">
                            <div class="media align-items-center p-3 bg-light-grey">
                                <div class="d-flex ml-1 mr-2 w-25 justify-content-end">
                                    <figure class="mb-0 thumb-md">
                                        <img id="modal-preview" alt="Preview"
                                            src="http://localhost/smart_school/public/images/default.png"
                                            class="img-thumbnail">
                                    </figure>
                                </div>
                                <div class="media-body w-75">
                                    <h6 class="text-muted text-uppercase">Receptionist (Front Office) For Dental Clinic
                                    </h6>
                                    <p class="mr-t-0">Aesthetics Dental Care</p>
                                </div>
                            </div>
                            <section class="p-10">
                                <table>
                                    <tr>
                                        <td style="width: 10%"><i class="fa fa-usd"></i></td>
                                        <td>IDR 3,500,000 - 4,100,000</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-usd"></i></td>
                                        <td>Minimal 1 tahun (Pegawai (non-manajemen & non-supervisor))</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-usd"></i></td>
                                        <td>Tangerang (Banten) - BSD City Tangerang Selatan</td>
                                    </tr>
                                </table>
                            </section>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="col-12 col-md-12widget-holder">
                <div class="alert alert-icon alert-warning border-warning alert-dismissible fade show" role="alert">
                    <p class="mb-0">PEMBERITAHUAN KEAMANAN YANG PENTING</p>
                    <p class="mb-0">Berhati-hatilah terhadap iklan yang mengharuskan Anda mengeluarkan biaya untuk melamar
                        atau memproses
                        lamaran, atau yang terdengar terlalu muluk-muluk.</p>
                    <p class="mb-0">Baca Panduan Mencari Kerja Secara Aman untuk info lebih lanjut</p>
                    <p class="mb-0">Sebelum Anda melamar, silakan baca peraturan pelamaran dan wawancara JobStreet.com</p>
                </div>
                <div class="widget-bg">
                    <form action="javascript:void(0)" id="formLamaran" name="formLamaran">
                        @csrf
                        <div class="widget-body clearfix">
                            <div class="media align-items-center bg-light-grey">
                                <div class="d-flex ml-1 mr-2 w-0 justify-content-end">
                                    <figure class="mb-0 thumb-md">
                                        <img id="modal-preview" alt="Preview" src="{{ $profile['file'] }}"
                                            class="img-thumbnail" style="height: 53px; width: auto;">
                                    </figure>
                                </div>
                                <div class="media-body w-75">
                                    <h6 class="text-muted text-uppercase"><b>{{ ucwords($profile['nama']) }} </b>
                                    </h6>
                                    <p class="mr-t-0"> IDR 2,500,000 | {{ $profile['email'] }} |
                                        {{ $profile['telepon'] }}
                                    </p>
                                </div>
                                <div class="media w-20">
                                    <div class="aksi">
                                        <a href="">Lihat</a> &nbsp;| &nbsp; <a href="">Ubah</a>
                                    </div>
                                </div>
                            </div>
                            <hr class="mt-0">
                            @if ($resume != null)
                                <div class="media align-items-center bg-light-grey">
                                    <div class="media-body w-75">
                                        <p class="m-0">Resume yang diunggah -
                                            {{ (new \App\Helpers\Help())->getTanggal($resume['updated_at']) }}</p>
                                    </div>
                                    <div class="media w-20">
                                        <div class="aksi">
                                            <a href="">Lihat</a> &nbsp;| &nbsp; <a href="">Ubah</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endif
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <p class="mb-0">Promosikan diri Anda! (Disarankan)</p>
                                        <input type="hidden" name="id_loker" value="{{ $lowongan['id'] }}">
                                        <textarea class="form-control" name="lamaran"
                                            placeholder="Beritahu perusahaan mengapa Anda paling cocok untuk posisi ini. Sebutkan ketrampilan khusus dan bagaimana Anda berkontribusi. Hindari hal generik seperti Saya bertanggung jawab."
                                            id="l38" rows="3"></textarea>
                                    </div>
                                    <p>Semua informasi pribadi yang kamu kirimkan sebagai bagian dari lamaran akan digunakan
                                        sesuai dengan Privacy Statement kami.</p>
                                    <small>
                                        Dengan menekan tombol "Kirim Lamaran", Saya telah membaca dan menyetujui peraturan
                                        JobStreet tentang pemanggilan interview
                                    </small>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-lg-12">
                                    <center>
                                        {{-- <input type="text" name="aksi" id="aksi" value="kirim"> --}}
                                        <button type="submit" class="btn btn-success" id="btnKirim">Kirim Lamaran</button>
                                        {{-- <a href="javascript:void" class="btn btn-danger">Simpan lamaran di Draft</button> --}}
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#formLamaran').on('submit', function(event) {
            event.preventDefault();
            $.ajax({
                url: "{{ route('bkk_pelamar-simpan') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $("#btnKirim").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#btnKirim").attr("disabled", true);
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        window.location = "/program/bursa_kerja/pelamar";
                    }
                    // noti(data.success, data.message);
                    swa(data.status + "!", data.message, data.success);
                    $("#btnKirim").html('Kirim Lamaran');
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

    </script>
@endsection
