{{-- {{dd($tema)}} --}}


<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/bursa_kerja') }}" class="ripple">
                <i class="list-icon material-icons">network_check</i> 
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li>
            <a href="{{ route('raport-mapel') }}" class="ripple">
                <i class="list-icon material-icons">local_library</i> 
                <span class="hide-menu">Mapel Diampu</span>
            </a>
        </li>
        <li>
            <a href="{{ route('raport-riwayat_mengajar') }}" class="ripple">
                <i class="list-icon material-icons">devices_other</i> 
                <span class="hide-menu">Riwayat Mengajar</span>
            </a>
        </li>
        <li>
            <a href="{{ route('auth.logout') }}" class="ripple">
                <i class="list-icon material-icons">power_settings_new</i> 
                <span class="hide-menu">Logout</span>
            </a>
        </li>
    </ul>
</nav>
