<style>
    button {
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
    }

</style>
<aside class="site-sidebar clearfix">
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
            <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
                <a href="{{ url('program/bursa_kerja') }}">
                    <span class="{{ empty(Request::segment(2)) ? 'color-color-scheme' : '' }}">
                        <span class="hide-menu">Beranda</span>
                    </span>
                </a>
            </li>
            <li class="{{ request()->segment(3) == 'industri' ? 'current-page active' : '' }}">
                <a href="{{ route('bkk_industi-detail') }}">
                    <span>
                        <span class="hide-menu">Profil Perusahaan</span>
                    </span>
                </a>
            </li>
            <li class="{{ request()->segment(4) == 'create' && request()->segment(3) == 'lowongan'  ? 'current-page active' : '' }}">
                <a href="{{ route('lowongan-create') }}">
                    <span class="{{ empty(Request::segment(2)) ? 'color-color-scheme' : '' }}">
                        <span class="hide-menu">Posting Lowongan</span>
                    </span>
                </a>
            </li>
            <li class="{{ request()->segment(3) == 'lowongan' && empty(Request::segment(4)) ? 'current-page active' : '' }}">
                <a href="{{ route('lowongan-beranda') }}">
                    <span class="{{ empty(Request::segment(2)) ? 'color-color-scheme' : '' }}">
                        <span class="hide-menu">Data Lowongan</span>
                    </span>
                </a>
            </li>
            
            <li class="{{ request()->segment(3) == 'agenda' ? 'current-page active' : '' }}">
                <a href="{{ route('bkk_agenda-beranda') }}">
                    <span>
                        <span class="hide-menu">Agenda</span>
                    </span>
                </a>
            </li>           

            <li class="dropdown user user-menu" style="position: absolute; right: 0; width: 343px;">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" style="right: 17px; position: absolute;">
                    <img src="{{ $profile['file'] }}" class="user-image"
                        alt="Gambar Pengguna" style="height: 43px;">
                    <span class="hidden-xs">{{ ucwords($profile['nama']) }}</span>
                </button>
                <div class="dropdown-menu" style="width: 100%;">
                    <div class="container d-flex justify-content-center">
                        <div class="card p-3" style="width: 90%;">
                            <div class="d-flex align-items-center">
                                <div class="image"> <img src="{{ $profile['file'] }}" class="rounded" style="width: 72px">
                                </div>
                                <div class="ml-3 w-100">
                                    <h4 class="mb-0 mt-0">{{ ucwords($profile['nama']) }}</h4>
                                    <span>{{ ucwords($profile['role']) }}</span>
                                    <div class="button mt-2 d-flex flex-row align-items-center">
                                        <a href="{{ route('edit-profile_admin') }}"
                                            class="btn btn-sm btn-info text-white w-100 mx-1"><i class="fa fa-pencil"></i>
                                            Edit</a>
                                        <a href="{{ route('auth.logout') }}"
                                            class="btn btn-sm btn-danger w-100 text-white mx-1"><i class="fa fa-sign-out"></i>
                                            Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </nav>
</aside>
