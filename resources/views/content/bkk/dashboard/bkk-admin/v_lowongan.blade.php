@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/css/jquery.readall.min.css') }}">
    <script src="{{ asset('asset/js/jquery.readall.min.js') }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <style type="text/css">
        section.row.custom-scroll-content.scrollbar-enabled {
            overflow-y: scroll;
        }

        div#social-links {
            margin: 0 auto;
        }

        div#social-links ul li {
            display: inline-block;
        }

        div#social-links ul li a {
            padding: 20px;
            margin: 1px;
            font-size: 52px;
            color: #222;
        }


        .search {
            width: 100%;
            margin-bottom: auto;
            margin-top: auto;
            height: 50px;
            background-color: #fff;
            padding: 10px;
            border-radius: 5px
        }

        .search-input {
            color: white;
            border: 0;
            outline: 0;
            background: none;
            width: 0;
            margin-top: 5px;
            caret-color: transparent;
            line-height: 20px;
            transition: width 0.4s linear
        }

        .search .search-input {
            padding: 0 10px;
            width: 100%;
            caret-color: #536bf6;
            font-size: 19px;
            font-weight: 300;
            color: black;
            transition: width 0.4s linear
        }

        .search-icon {
            cursor: pointer;
            height: 34px;
            width: 62px;
            float: right;
            display: flex;
            justify-content: center;
            align-items: center;
            color: white !important;
            background-color: #536bf6;
            font-size: 10px;
            bottom: 30px;
            position: relative;
            border-radius: 5px
        }

        .search-icon:hover {
            color: #fff !important
        }

        .readall-button {
            background: #fff !important;
        }

        .readall-button:hover {
            /* color: #000; */
            background: silver !important;
        }

        div#table_pelamar_length {
            padding-top: .755em;
            margin-top: 1.42857em;
        }

        .modal-header {
            padding-left: 0;
        }

    </style>
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60">
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="card card-default">
                            <div class="card-header" style="background: #5f27cd;">
                                <form name="formSearch" id="formSearch" action="javascript:void(0)">
                                    <div class="d-flex justify-content-center">
                                        <div class="search"> <input type="text" name="textSearch" class="search-input"
                                                placeholder="Search...">
                                            <button type="submit" class="search-icon btn-search"> <i
                                                    class="fa fa-search"></i> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row m-2">
                                <div class="col-md-6">
                                    <div class="vertical-center" id="jumlah_loker" style="width: 100%;">Menampilkan
                                        {{ count($loker) }} data
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="industri" style="width: 50%; float: right">
                                        <select name="id_industri" class="select3" id="id_industri"
                                            style="width: 100%; height: 27px;">
                                            <option value="">Pilih industri</option>
                                            @foreach ($industri as $ind)
                                                <option value="{{ $ind['id'] }}">{{ $ind['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="card-body p-2" style="background: #f7f7f7;" id="data-wrapper"> --}}
                            <div class="card-body p-2" style="background: #f7f7f7;" id="listLoker">
                                {{-- @foreach ($loker as $lk)
                                    <div class="well well-sm widget-bg p-3 mt-2">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-8">
                                                <h4 class="m-0"><b>{{ ucwords($lk['judul']) }}</b></h4>
                                                <div>
                                                    <b>
                                                        {{ ucwords($lk['industri']) }} |
                                                        {{ (new \App\Helpers\Help())->getTanggal($lk['tanggal_post']) }}</b>
                                                </div>
                                                <div class="syarat mt-2 tes" style="text-align: justify;">Syarat : <p
                                                        class="text-block m-0">
                                                        {{ $lk['syarat'] }}</p>

                                                </div>
                                                <div class="mt-2">
                                                    <a href="javascript:void(0)" onclick="agenda({{ $lk['id'] }})"><i
                                                            class="fa fa-calendar"
                                                            style="font-size: 26px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                        href=""><i style="font-size: 26px;"
                                                            class="fa fa-clock-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                        href="javascript:void(0)" onclick="pelamar({{ $lk['id'] }})"><i
                                                            style="font-size: 26px;" class="fa fa-users"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 text-center" style="position: relative;">
                                                <div class="vertical-center" style="width: 100%">
                                                    <div>Range Gaji {{ number_format($lk['gaji_awal']) }} >
                                                        {{ number_format($lk['gaji_sampai']) }}</div>
                                                    <div class="mt-3">
                                                        <a href="javascript:void(0)"
                                                            onclick="addPelamar({{ $lk['id'] }})"
                                                            class="btn btn-sm btn-success"><i class="fa fa-user-plus"></i>
                                                            Tambahkan Pelamar</a>
                                                    </div>
                                                    <div class="mt-2">
                                                        <a href="javascript:void(0)"
                                                            onclick="detailLoker({{ $lk['id'] }})"
                                                            class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View
                                                            Detail</a>
                                                        <a href="javascript:void(0)"
                                                            onclick="deleteLoker({{ $lk['id'] }})"
                                                            class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                                                            Hapus Loker</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach --}}
                            </div>
                            {{-- <div class="auto-load text-center">
                                <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="60"
                                    viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                    <path fill="#000"
                                        d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate"
                                            dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                    </path>
                                </svg>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg-color-scheme" id="agendaModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0 pm-0" style="border: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading" style="color: black !important"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12 p-4" id="profilDetail" style="padding-top: 0 !important">

                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $('.tes').readall({
                showheight: 96,
                showrows: null,
                animationspeed: 200,
                btnTextShowmore: 'Read more',
                btnTextShowless: 'Read less',
                btnClassShowmore: 'readall-button',
                btnClassShowless: 'readall-button'
            });
        });

        // call_ajax();

        $('#formSearch').on('submit', function(event) {
            event.preventDefault();
            call_ajax()
        });

        var judul = null;
        var id_industri = null;

        $('select[name="id_industri"]').on('change', function() {
            if (judul != "") {
                judul = $('input[name="textSearch"]').val();
            } else {
                judul = null;
            }
            if (id_industri != "") {
                judul = $('select[name="id_industri"]').val();
            } else {
                id_industri = null;
            }
            call_ajax();
        })

        function call_ajax() {
            judul = $('input[name="textSearch"]').val();
            id_industri = $('select[name="id_industri"]').val();
            if (judul != "") {
                judul = $('input[name="textSearch"]').val();
            } else {
                judul = null;
            }
            if (id_industri != "") {
                id_industri = $('select[name="id_industri"]').val();
            } else {
                id_industri = null;
            }

            $(".btn-search").html(
                '<i class="fa fa-spin fa-spinner"></i>');
            $(".pull-right").html(
                'Sedang memproses data...');
            $(".btn-search").attr("disabled", true);
            $('#listLoker').html('<div id="loading" style="" ></div>');

            $.ajax({
                url: "/program/bursa_kerja/lowongan/admin/search",
                type: "POST",
                data: {
                    id_industri,
                    judul,
                    vertical: true,
                },
                success: function(data) {
                    $(".btn-search").html(
                        '<i class="fa fa-search"></i>');
                    $(".btn-search").attr("disabled", false);
                    if (data.count != 0) {
                        $('#listLoker').html(data.html);
                        $('#jumlah_loker').html('Menampilakan ' + data.count + ' Lowongan Kerja untuk kamu');
                    } else {
                        $('#listLoker').html(
                            '<center><div class="col-md-12"><div class="error-template"><h1>Oops!</h1><h2>404 Data Not Found</h2><div class="error-details">Sorry, an error has occured, Requested page not found!</div><div class="error-actions"><a href="http://www.jquery2dotnet.com" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>Take Me Home </a></div></div></div></center>'
                        );
                        $('#jumlah_loker').html('Ada ' + data.count + ' Lowongan Kerja untuk kamu');
                    }
                }
            });
        };

        function agenda(id) {
            $("#modelHeading").html('<i class="fa fa-calendar" style="font-size: 26px;"></i> Daftar Agenda');
            $("#profilDetail").html('<div id="calendar_' + id + '"></div>');
            kalender(id);
        }

        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $(".message").text("link copied");
        }

        function share(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('lowongan-share') }}",
                data: {
                    id
                },
                success: function(data) {
                    $("#profilDetail").html(data);
                    $('#agendaModal').modal('show');
                }
            });
        }

        function pelamar(id) {
            $('#agendaModal').modal('show');
            $("#modelHeading").html('<i style="font-size: 26px;" class="fa fa-users"></i> Daftar Pelamar');
            $("#profilDetail").html(
                ' <div style="width: 100%;"><div class="table-responsive" style="margin-top: 14px;"><table class="table table-striped" id="table_pelamar" style="width: 100%"><thead><tr><th>#</th><th>Nama</th><th>Telepon</th><th>Alamat</th><th>Status</th><th>Aksi</th></tr></thead></table></div></div>'
            );
            pelamarDatatable(id, "sudah_melamar");
        }

        function addPelamar(id) {
            $('#agendaModal').modal('show');
            $("#modelHeading").html('<i style="font-size: 26px;" class="fa fa-users"></i> Daftar Pelamar');
            $("#profilDetail").html(
                ' <div style="width: 100%;"><div class="table-responsive" style="margin-top: 14px;"><table class="table table-striped" id="table_pelamar" style="width: 100%"><thead><tr><th>#</th><th>Nama</th><th>Telepon</th><th>Alamat</th><th>Jenis Kelamin</th><th>Aksi</th></tr></thead></table></div></div>'
            );
            pelamarDatatable(id, "belum_melamar");
        }

        function pelamarDatatable(id, aksi) {
            var table_trash = $('#table_pelamar').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('bkk_pelamar-loker_datatable') }}",
                    "type": "POST",
                    "data": function(d) {
                        d.id = id;
                        d.aksi = aksi;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'alamat',
                        name: 'alamat'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }

        function kalender(id) {
            var calendar = $('#calendar_' + id).fullCalendar({
                editable: true,
                events: "{{ url('program/bursa_kerja/agenda/by_loker') }}" + '/' + id,
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    element.children().last().append("<br>Lokasi " + event.alamat);
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    var alamat = prompt('Event Location:');
                    if (title && alamat) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                        var event = {
                            alamat: alamat,
                            title: title,
                            start: start,
                            end: end,
                            id_loker: id
                        };
                        $.ajax({
                            url: "{{ route('bkk_agenda-simpan') }}",
                            data: event,
                            type: "POST",
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    $('#calendar_' + id).fullCalendar('refetchEvents');
                                }
                                noti(data.success, data.message)

                            }
                        });
                    }
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: "{{ route('bkk_agenda-update') }}",
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end +
                            '&id=' + event.id + '&id_loker=' + event.id_loker + '&alamat=' + event
                            .alamat,
                        type: "PUT",
                        success: function(data) {
                            noti(data.success, data.message)
                        }
                    });
                },
                eventClick: function(event) {
                    var deleteMsg = confirm("Do you really want to delete?");
                    if (deleteMsg) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('bkk_agenda-soft_delete') }}",
                            data: "&id=" + event.id,
                            success: function(response) {
                                if (response['status'] == "berhasil") {
                                    $('#calendar_' + id).fullCalendar('removeEvents', event.id);
                                    noti(response.icon, response.message)
                                } else {
                                    noti(response.icon, response.message)
                                }
                            }
                        });
                    }
                }
            });
            $('#agendaModal').modal('show');
            $('#agendaModal').on('shown.bs.modal', function() {
                $("#calendar_" + id).fullCalendar('render');
            });
        }


        function deleteLoker(id) {
            if (id) {
                var confirmdelete = confirm("Do you really want remove data?");
                var animasi = $(this);
                if (confirmdelete == true) {
                    $.ajax({
                        type: 'POST',
                        url: "lowongan/soft_delete",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(animasi).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                call_ajax();
                            }
                            swa(data.status + "!", data.message, data.success);
                        }
                    });
                }
            }
        }

        function applyJob(id_user, id_loker) {
            var animasi = $(this);
            console.log(animasi);
            $('#applyLamaran_' + id_user).html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $('#applyLamaran_' + id_user).attr("disabled", true);
            $.ajax({
                url: "/program/bursa_kerja/pelamar/simpan",
                type: "POST",
                data: {
                    id_user,
                    id_loker,
                },
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        var oTable = $('#table_pelamar').dataTable();
                        oTable.fnDraw(false);
                    }
                    noti(data.success, data.message);
                    $('#applyLamaran_' + id_user).html(
                        '<i class="fa fa-paper-plane"></i> Apply');
                    $('#applyLamaran_' + id_user).attr("disabled", false);
                }
            });
        }

        function detailLoker(id) {
            $.ajax({
                url: "{{ route('bkk_loker-detail_by_pelamar') }}",
                type: "POST",
                data: {
                    id,
                },
                success: function(data) {
                    $("#profilDetail").html(data);
                    $('#agendaModal').modal('show')
                }
            });
        }

        load_data('');

        function load_data(id = "") {
            $.ajax({
                url: "lowongan/load_more",
                method: "POST",
                data: {
                    id: id,
                },
                success: function(data) {
                    $('#load_more_button').remove();
                    $('#listLoker').append(data);
                }
            })
        }

        $(document).on('click', '#load_more_button', function() {
            var id = $(this).data('id');
            $('#load_more_button').html('<b>Loading...</b>');
            load_data(id);
        });
    </script>

@endsection
