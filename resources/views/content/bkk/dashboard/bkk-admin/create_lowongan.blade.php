@extends('template/template_default/app')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <style>
        div#social-links {
            margin: 0 auto;
            max-width: 500px;
        }

        div#social-links ul li {
            display: inline-block;
        }

        div#social-links ul li a {
            padding: 20px;
            margin: 1px;
            font-size: 52px;
            color: #222;
        }

        .mt-100 {
            margin-top: 100px
        }

        .modal {
            background-image: linear-gradient(rgb(35, 79, 71) 0%, rgb(36, 121, 106) 100.2%)
        }

        .modal-title {
            font-weight: 900
        }

        .modal-content {
            border-radius: 13px
        }

        .modal-body {
            color: #3b3b3b
        }

        .img-thumbnail {
            border-radius: 33px;
            width: 61px;
            height: 61px
        }

        .fab:before {
            position: relative;
            top: 13px
        }

        .smd {
            width: 200px;
            font-size: small;
            text-align: center
        }

        .modal-footer {
            display: block
        }

        .ur {
            border: none;
            background-color: #e6e2e2;
            border-bottom-left-radius: 4px;
            border-top-left-radius: 4px
        }

        .cpy {
            border: none;
            background-color: #e6e2e2;
            border-bottom-right-radius: 4px;
            border-top-right-radius: 4px;
            cursor: pointer
        }

        button.focus,
        button:focus {
            outline: 0;
            box-shadow: none !important
        }

        .ur.focus,
        .ur:focus {
            outline: 0;
            box-shadow: none !important
        }

        .message {
            font-size: 11px;
            color: #ee5535
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <div class="row">
        <div class="content" style="width: 100%">
            <div class="container-fluid">
                <div class="row mt-60">
                    <div class="col-md-12">
                        <form action="javascript:void(0)" name="lokerCreate" id="lokerCreate">
                            @csrf
                            <div class="table-responsive">
                                <table class="table widget-bg" id="data-tabel" width="100%" cellspacing="0">
                                    <tr style="background: #0047a2;">
                                        <td colspan="3" style="color: #fff"><b>Input Loker</b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">Nama Industri</td>
                                        <td>
                                            <select name="id_industri" class="select3"
                                                style="width: 50%; height: 33px;">
                                                <option value="">Pilih Industri</option>
                                                @foreach ($perusahaan as $pr)
                                                    <option value="{{ $pr['id'] }}">{{ ucwords($pr['nama']) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width: 34%"></td>
                                    </tr>
                                    <tr>
                                        <td>Judul Lowongan</td>
                                        <td><input type="text" name="judul" required style="width: 100%"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Link Loker</td>
                                        <td><input type="text" name="link" style="width: 100%"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Bidang Lowongan</td>
                                        <td>
                                            <select name="id_bidang_loker" class="select3"
                                                style="width: 50%; height: 33px;">
                                                <option value="">Pilih Industri</option>
                                                @foreach ($bidang as $bd)
                                                    <option value="{{ $bd['id'] }}">{{ ucwords($bd['nama']) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi Lowongan</td>
                                        <td><textarea id="" name="deskripsi" rows="3" style="width: 100%"></textarea></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Syarat</td>
                                        <td><textarea name="syarat" rows="3" style="width: 100%"></textarea></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Range Gaji</td>
                                        <td><input type="text" class="ribuan" name="gaji_awal"> sampai <input class="ribuan"
                                                type="text" name="gaji_sampai">
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal dibuka</td>
                                        <td><input type="text" class="datepicker" style="width: 100%"
                                                value="{{ date('d-m-Y') }}" name="tgl_buka"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ditutup</td>
                                        <td><input type="text" class="datepicker" name="tgl_tutup" style="width: 100%"
                                                value="{{ date('d-m-Y') }}"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>CP Recruitment</td>
                                        <td><input type="text" name="recruitment" id="" style="width: 100%"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Quota yang dibutuhkan</td>
                                        <td><input type="number" name="quota" id="" style="width: 100%"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>File Flyer</td>
                                        <td><input id="image" type="file" name="image" accept="image/*"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="hidden" name="optionAksi" id="optionAksi" value="add">
                                            <button class="btn btn-success" type="submit" id="btnSave">Simpan</button>
                                            <a href="" class="btn btn-danger">Batal</a>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content col-12">
                <div class="modal-header">
                    <h5 class="modal-title">Share</h5> <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body" id="isi_sosials">
                    <div id="isi_sosial">

                    </div>
                </div>

                <div class="modal-footer"> <label style="font-weight: 600">Page Link <span
                            class="message"></span></label><br />
                    <div class="row">
                        <input class="col-10 ur" type="url" value="tes" id="myInput"
                            aria-describedby="inputGroup-sizing-default" readonly style="height: 40px;">
                        <button class="cpy" onclick="myFunction()"><i class="far fa-clone"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('submit', '#lokerCreate', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $("#saveBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveBtn").attr("disabled", true);
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('lowongan-save_create') }}",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("#btnSave").html(
                        '<i class="fa fa-spin fa-spinner"></i> Processing Save');
                },
                success: (data) => {
                    if (data.status == 'berhasil') {
                        $('#isi_sosials').html(data.share)
                        $('#myInput').val(data.link_url)
                        $('#exampleModal').modal('show');
                        $('#lokerCreate').trigger("reset");
                    }
                    $("#btnSave").attr("disabled", false);
                    $("#btnSave").html("Simpan");
                    noti(data.success, data.message);
                    // swa(data.status + "!", data.message, data.success);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $('#exampleModal').on('hidden.bs.modal', function() {
            window.location = "/program/bursa_kerja/lowongan";
        })

        function myFunction() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $(".message").text("link copied");
        }
    </script>

@endsection
