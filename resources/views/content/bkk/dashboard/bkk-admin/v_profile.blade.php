@extends('template/template_default/app')
@section('content')

    <div class="row">
        <div class="content" style="width: 100%; padding: 0 31px;">
            <div class="container-fluid">
                <form action="javascript:void(0)" name="formConfig" id="formConfig" enctype="multipart/form-data">
                    @csrf
                    <div class="row mt-60 widget-bg">
                        <div class="col-md-12">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                            <hr>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Judul Web</label>
                                <div class="col-md-7">
                                    @if ($aksi == 'edit')
                                        <textarea name="deskripsi" id="deskripsi" class="form-control"
                                            rows="3">{{ $config['deskripsi'] }}</textarea>
                                    @else
                                        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3"></textarea>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Deskripsi Web</label>
                                <div class="col-md-7">
                                    <input type="hidden" name="" id="action" value="{{ $aksi }}">
                                    @if ($aksi == 'edit')
                                        <textarea name="judul" id="judul" class="form-control"
                                            rows="3">{{ $config['judul'] }}</textarea>
                                        <input type="hidden" name="id" value="{{ $config['id'] }}">
                                    @else
                                        <textarea name="judul" id="judul" class="form-control" rows="3"></textarea>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Fav Icon</label>
                                <div class="col-md-7">
                                    <div class="row">
                                        @if ($aksi == 'edit')
                                            <div class="col-md-2">
                                                <img id="modal-preview" src="{{ $config['fav_icon'] }}" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"> <input
                                                        type="checkbox" name="remove_photo[]" value="file"/> Remove photo</div>
                                            </div>
                                        @else
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        @endif
                                    </div>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                    <small>
                                        <p>Gunakan file PNG Transparan, width dan height sama, misal 64px x 64px</p>
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Logo Aplikasi</label>
                                <div class="col-md-7">
                                    <div class="row">
                                        @if ($aksi == 'edit')
                                            <div class="col-md-2">
                                                <img id="modal-logo" src="{{ $config['logo'] }}" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_logo" style="position: absolute; bottom: 0"><input
                                                        type="checkbox" name="remove_photo[]" value="file1" /> Remove photo</div>
                                            </div>
                                        @else
                                            <div class="col-md-2">
                                                <img id="modal-logo" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_logo" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        @endif
                                    </div>
                                    <input id="image" type="file" name="image1" accept="image/*" onchange="readLogo(this);">
                                    <small>
                                        <p>Gunakan file PNG Transparan, Lebar maksimal 150px, maksimal 300kb. Minimal 50px x
                                            50px, Tipe file JPG, JPEG, dan PNG</p>
                                    </small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10">Footer</label>
                                <div class="col-md-7">
                                    @if ($aksi == 'edit')
                                        <textarea name="footer" id="footer" class="form-control"
                                            rows="3">{{ $config['footer'] }}</textarea>
                                    @else
                                        <textarea name="footer" id="footer" class="form-control" rows="3"></textarea>

                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label" for="l10"></label>
                                <div class="col-md-7">
                                    <button type="submit" class="btn btn-info" id="saveConfig">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function readLogo(input, id) {
            id = id || '#modal-logo';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-logo').removeClass('hidden');
                $('#start').hide();
            }
        }

        $('body').on('submit', '#formConfig', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $("#saveConfig").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveConfig").attr("disabled", true);
            var action_url = '';

            if ($('#action').val() == 'add') {
                action_url = "{{ route('bkk_profile-create') }}";
            }

            if ($('#action').val() == 'edit') {
                action_url = "{{ route('bkk_profile-create') }}";
            }

            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: action_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        location.reload(true);
                    }
                    noti(data.icon, data.success);
                    $('#saveConfig').html('Simpan');
                    $("#saveConfig").attr("disabled", false);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveConfig').html('Simpan');
                }
            });
        });

    </script>

@endsection
