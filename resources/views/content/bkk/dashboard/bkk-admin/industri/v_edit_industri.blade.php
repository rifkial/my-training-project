@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">EDIT INDUSTRI</h5>
        </div>
        <div class="page-title-right">
            <a href="{{ route('bkk_industri-beranda') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i>
                Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Industri</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $industri['file'] }});">
                                </div>
                                <h4 class="my-2">{{ ucwords($industri['nama']) }}</h4>
                                <div class="user-button">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" data-toggle="collapse" data-target="#uploadFoto"
                                                class="btn btn-sm btn-purple btn-block"><i class="fas fa-image"></i>
                                                Ganti Foto
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger btn-sm btn-block delete"
                                                data-id="{{ $industri['id'] }}"><i class="fa fa-trash"></i> Hapus Foto
                                            </button>
                                        </div>
                                        <div class="col-md-12 mt-3 collapse" id="uploadFoto">
                                            <form id="form_uploadImage">
                                                <input type="hidden" name="id" value="{{ $industri['id'] }}">
                                                <div class="form-group">
                                                    <label for="l39">Gambar Profile</label>
                                                    <br>
                                                    <input id="image" type="file" name="image" accept="image/*" required>
                                                    <br><small class="text-muted">Harap pilih gambar yang ingin
                                                        dijadikan profile</small>
                                                </div>
                                                <div class="form-actions btn-list">
                                                    <button class="btn btn-info" type="submit" id="btnUpload"><i
                                                            class="fas fa-save"></i> Simpan</button>
                                                    <button class="btn btn-outline-default" type="button"><i
                                                            class="fas fa-times-circle"></i> Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#profile"
                                        data-toggle="tab">Profile</a>
                                </li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#pemilik"
                                        data-toggle="tab">Pemilik</a></li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#lokasi"
                                        data-toggle="tab">Lokasi</a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#targetMap"
                                        data-toggle="tab">MAP</a></li>
                            </ul>
                            <div class="aksi">
                                <button type="reset" class="btn btn-danger"><i class="fas fa-sync-alt"></i> Reset</button>
                                <button type="submit" class="btn btn-info" id="saveBtn"><i class="fas fa-save"></i>
                                    Simpan</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="profile">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $industri['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Industri</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="nama" name="nama" placeholder="Nama Industri"
                                                type="text" value="{{ $industri['nama'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Telepon</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="telepon" name="telepon"
                                                placeholder="No. Telepon" type="text" value="{{ $industri['telepon'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Deskripsi</label>
                                        <div class="col-md-9">
                                            <textarea name="deskripsi" id="deskripsi" rows="3"
                                                class="form-control">{{ $industri['deskripsi'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Website</label>
                                        <div class="col-md-9">
                                            <input class="form-control" id="website" name="website"
                                                placeholder="website Industri" type="text"
                                                value="{{ $industri['website'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pemilik">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="keyword" name="keyword" class="form-control"
                                                    placeholder="Masukan Keyword Pencarian">
                                                <div class="input-group-btn">
                                                    <a href="javascript:void(0)" onclick="mulaiCari()"
                                                        class="btn btn-info btnCari"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="data_user">
                                        @if (!empty($user))
                                            @foreach ($user as $us)
                                                <div class="col-md-6 my-2">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <table>
                                                                <tr>
                                                                    <td width="20"><input type="radio" name="id_user"
                                                                            value="{{ $us['id'] }}"
                                                                            {{ $us['id'] == $industri['id_user'] ? 'checked' : '' }}>
                                                                    </td>
                                                                    <td width="120"><img src="{{ $us['file'] }}" alt=""
                                                                            width="100"></td>
                                                                    <td>
                                                                        <b>{{ ucwords($us['nama']) }}</b>
                                                                        <p class="m-0">{{ $us['email'] }}</p>
                                                                        <small><i class="fas fa-phone"></i>
                                                                            {{ $us['telepon'] }}</small>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <table class="w-100">
                                                            <tr>
                                                                <td class="text-center">
                                                                    <h3>Belum ada user industri yang tersedia</h3>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="lokasi">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Provinsi</label>
                                        <div class="col-md-9">
                                            <select name="provinsi" id="provinsi" class="form-control">
                                                <option value="">PILIH PROVINSI..</option>
                                                @foreach ($provinsi as $pr)
                                                    <option value="{{ $pr['id'] }}"
                                                        {{ $pr['id'] == $industri['id_provinsi'] ? 'selected' : '' }}>
                                                        {{ ucwords($pr['name']) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kabupaten</label>
                                        <div class="col-md-9">
                                            <select name="kabupaten" id="kabupaten" class="form-control">
                                                <option value="">Pilih Kabupaten..</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Kecamatan</label>
                                        <div class="col-md-9">
                                            <select name="kecamatan" id="kecamatan" class="form-control">
                                                <option value="">Pilih Kecamatan..</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Desa</label>
                                        <div class="col-md-9">
                                            <select name="desa" id="desa" class="form-control">
                                                <option value="">Pilih Desa..</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat</label>
                                        <div class="col-md-9">
                                            <textarea name="alamat" id="alamat" rows="3"
                                                class="form-control">{{ $industri['alamat'] }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="targetMap">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input class="form-control" id="latitude" name="latitude"
                                                placeholder="Latitude Industri" type="text"
                                                value="{{ $industri['lat'] }}" readonly>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" id="longtitude" name="longtitude"
                                                placeholder="Longtitude Industri" type="text"
                                                value="{{ $industri['long'] }}" readonly>
                                        </div>
                                    </div>
                                    <div id="map" class="w-100" style="height: 300px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalReset" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Update Password</h5>
                </div>
                <form id="formReset">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" value="{{ $industri['id'] }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Lama</span>
                                        <input class="form-control" id="old_pass" placeholder="Password_lama" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Password Baru</span>
                                        <input class="form-control" id="password" name="password"
                                            placeholder="Password Baru" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group"><span class="input-group-addon">Konfirmasi Password</span>
                                        <input class="form-control" id="confirm_password" name="confirm_password"
                                            placeholder="Konfirmasi Password baru" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left"
                            id="btnUpdateReset">Update</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id_prov = "{{ $industri['id_provinsi'] }}";
            var id_kab = "{{ $industri['id_kabupaten'] }}";
            var id_kec = "{{ $industri['id_kecamatan'] }}";
            var id_des = "{{ $industri['id_desa'] }}";
            var lat_ind = "{{ $industri['lat'] }}";
            var long_ind = "{{ $industri['long'] }}";
            if (id_prov != null && id_kab != null) {
                edit_kabupaten(id_prov, id_kab);
            }
            if (id_kab != null && id_kec != null) {
                edit_kecamatan(id_kab, id_kec);
            }
            if (id_kec != null && id_des != null) {
                edit_desa(id_kec, id_des);
            }

            var map, newMarker, markerLocation;
            if (lat_ind != '' && long_ind != '') {
                map = L.map('map').setView([lat_ind, long_ind], 12);
                L.marker([lat_ind, long_ind]).addTo(map);
                console.log("ada");
            } else {
                console.log("tak ada");
                map = L.map('map').setView([-0.6852979, 118.3979053], 4);
            }
            var llet = "{{ $leaflet['token'] }}";
            if (llet) {
                L.tileLayer(
                    'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token='+llet, {
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                        maxZoom: 18
                    }).addTo(map);
            } else {
                alert('anda belum set configurasi maps di settingan admin');
            }


            var theMarker = {};

            map.on('click', function(e) {
                lat = e.latlng.lat;
                lon = e.latlng.lng;

                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker([lat, lon]).addTo(map);

                $('#latitude').val(lat);
                $('#longtitude').val(lon);
            });

            $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Menyimpan');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('bkk_industi-update') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                        swa(data.status + "!", data.message, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $('body').on('submit', '#form_uploadImage', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#btnUpload").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnUpload").attr("disabled", true);

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('upload_profile-bkk_industri') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $(".profile-img").css("background-image", "url(" + data.image +
                                ")");
                            $('#uploadFoto').collapse('hide');
                        }
                        noti(data.icon, data.message);
                        $('#btnUpload').html('<i class="fas fa-save"></i> Simpan');
                        $("#btnUpload").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnUpload').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('delete_profile-bkk_industri') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".profile-img").css("background-image", "url(" + data
                                    .image + ")");
                            }
                            $(loader).html(
                                '<i class="fa fa-trash"></i> Hapus Foto');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.refresh', function() {
                $('.refresh').html('<i class="fa fa-spin fa-redo"></i> Proses Refreshing..');
                location.reload();
            });
            $('select[name="provinsi"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $('select[name="kabupaten"]').attr('disabled', 'disabled')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('get_kabupaten-provinsi') }}",
                        data: {
                            id
                        },
                        success: function(data) {
                            // console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="kabupaten"]').html(
                                    '<option value="">--- No Kabupaten Found ---</option>');
                            } else {
                                var select = '';
                                var s =
                                    '<option value="">--Pilih Provinsi terlebih dahulu--</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';


                                })
                                $('select[name="kabupaten"]').removeAttr('disabled');
                            }
                            $('select[name="kabupaten"]').html(s)
                            $('select[name="kecamatan"]').html(
                                '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>'
                            );
                            $('select[name="kecamatan"]').attr('disabled', 'disabled');
                            $('select[name="desa"]').html(
                                '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                            );
                            $('select[name="desa"]').attr('disabled', 'disabled');
                        }
                    });
                }
            })

            $('select[name="kabupaten"]').on('change', function() {
                var id_kabupaten = $(this).val();
                if (id_kabupaten) {
                    $('select[name="kecamatan"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "/super_admin/master/indonesia/kecamatan" + '/' + id_kabupaten,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="kecamatan"]').html(
                                    '<option value="">--- No Kecamatan Found ---</option>');
                            } else {
                                var s =
                                    '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>';
                                data.districts.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';

                                })
                                $('select[name="kecamatan"]').removeAttr('disabled');
                            }
                            $('select[name="kecamatan"]').html(s)
                            $('select[name="desa"]').html(
                                '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                            );
                            $('select[name="desa"]').attr('disabled', 'disabled');

                        }
                    });
                }
            })


            $('select[name="kecamatan"]').on('change', function() {
                var id_kecamatan = $(this).val();
                // console.log(id_kecamatan)
                if (id_kecamatan) {
                    $('select[name="desa"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "/super_admin/master/indonesia/desa" + '/' + id_kecamatan,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            // console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="desa"]').html(
                                    '<option value="">--- No Desa Found ---</option>');
                            } else {
                                var s =
                                    '<option value="">--Pilih Kecamatan terlebih dahulu--</option>';
                                data.villages.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';

                                })
                                $('select[name="desa"]').removeAttr('disabled');
                            }
                            $('select[name="desa"]').html(s)
                        }
                    });
                }
            })

            $('select[name="desa"]').on('change', function() {
                var id_desa = $(this).val();
                console.log(id_desa);
                if (id_desa) {
                    $.ajax({
                        url: "/super_admin/master/indonesia/detail/desa" + '/' + id_desa,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            map.setView([data.meta.lat, data.meta.long], 12);
                        }
                    });
                }
            })


        })

        function edit_kabupaten(id_prov, id_kab) {
            $.ajax({
                url: "{{ route('get-edit_kabupaten') }}",
                type: "POST",
                data: {
                    id_prov: id_prov,
                    id_kab: id_kab
                },
                beforeSend: function() {
                    $('select[name="kabupaten"]').append(
                        '<option value="">--- No Kabupaten Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kabupaten"]').removeAttr('disabled');
                    $('select[name="kabupaten"]').html(fb);
                }
            });
            return false;
        }

        function edit_kecamatan(id_kab, id_kec) {
            $.ajax({
                url: "{{ route('get-edit_kecamatan') }}",
                type: "POST",
                data: {
                    id_kab: id_kab,
                    id_kec: id_kec
                },
                beforeSend: function() {
                    $('select[name="kecamatan"]').append(
                        '<option value="">--- No Kecamatan Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kecamatan"]').removeAttr('disabled');
                    $('select[name="kecamatan"]').html(fb);
                }
            });
            return false;
        }

        function edit_desa(id_kec, id_des) {
            console.log('desa ' + id_des)
            $.ajax({
                url: "{{ route('get-edit_desa') }}",
                type: "POST",
                data: {
                    id_kec: id_kec,
                    id_des: id_des,
                },
                beforeSend: function() {
                    $('select[name="desa"]').append(
                        '<option value="">--- No Desa Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="desa"]').removeAttr('disabled');
                    $('select[name="desa"]').html(fb);
                }
            });
            return false;
        }

        function mulaiCari() {
            let keyword = $('#keyword').val();
            if (keyword.length) {
                // console.log(keyword);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('bkk_user_filter_name') }}",
                    data: {
                        keyword
                    },
                    beforeSend: function() {
                        $('.btnCari').html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        console.log(data);
                        $('#data_user').html(data);
                        $('.btnCari').html(
                            '<i class="fa fa-search"></i>');
                    }
                });
            } else {
                alert('kata kunci pencarian harus diisi');
            }
        }

        // function buildMap(lat, lon) {
        //     document.getElementById('weathermap').innerHTML = "<div id='map' style='width: 100%; height: 100%;'></div>";
        //     var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        //         osmAttribution = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,' +
        //         ' <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        //         osmLayer = new L.TileLayer(osmUrl, {
        //             maxZoom: 18,
        //             attribution: osmAttribution
        //         });
        //     var map = new L.Map('map');
        //     map.setView(new L.LatLng(lat, lon), 9);
        //     map.addLayer(osmLayer);
        //     var validatorsLayer = new OsmJs.Weather.LeafletLayer({
        //         lang: 'en'
        //     });
        //     map.addLayer(validatorsLayer);
        // }
    </script>
@endsection
