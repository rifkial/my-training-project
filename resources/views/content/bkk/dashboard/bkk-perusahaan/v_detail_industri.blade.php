@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 widget-holder col-md-offset-2">
                <form id="msform" action="javascript:void(0)" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="widget-bg" style="padding: 0.42857em; border-radius: 4px;">
                        <div class="widget-body clearfix">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5 class="mt-2">Profile Perusahaan</h5>
                                </div>
                                <div class="col-md-8">
                                    <button class="btn btn-info btn-sm pull-right" id="btn-save"><i
                                            class="fa fa-refresh"></i>
                                        Perbarui</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-bg mt-3">
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Fill in the Industri ad details</h5>
                            <p class="text-muted">Industri details</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="id_perusahaan" value="{{ $industri['id'] }}">
                                        <label for="l30">Nama Perusahaan</label>
                                        <input class="form-control" id="l30" name="perusahaan" type="text"
                                            value="{{ $industri['nama'] }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Telepon</label>
                                        <input class="form-control" id="l30" name="telepon"
                                            value="{{ $industri['telepon'] }}" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Website</label>
                                        <input class="form-control" id="l30" name="website"
                                            value="{{ $industri['website'] }}" type="text">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Deskripsi</label>
                                        <textarea name="alamat" class="form-control" id="alamat" cols="30"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label for="l30">Gambar Perusahaan</label>
                                                <input id="image" type="file" class="form-control" name="image"
                                                    accept="image/*" onchange="readURL(this);"
                                                    style="border: none; box-shadow: none;">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <img id="modal-preview" src="{{ $industri['file'] }}" alt="Preview"
                                                class="form-group" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="widget-bg mt-3">
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Location Industri details</h5>
                            <p class="text-muted">Job type</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Provinsi</label>
                                        <select name="provinsi" class="form-control form-control-line" id="dropdownlist"
                                            required>
                                            <option value="">-- Pilih Provinsi --</option>
                                            @foreach ($provinsi as $pro)
                                                @php
                                                    $select = '';
                                                    if ($industri['id_provinsi'] == $pro['id']) {
                                                        $select = 'selected';
                                                    }
                                                @endphp
                                                <option value="{{ $pro['id'] }}" {{ $select }}>
                                                    {{ $pro['name'] }}</option>
                                            @endforeach
                                        </select>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kabupaten</label>
                                        <select name="kabupaten" class="form-control form-control-line" id="dropdownlist"
                                            disabled>
                                            <option value="">--Pilih Provinsi terlebih dahulu--
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kecamatan</label>
                                        <select name="kecamatan" class="form-control form-control-line" id="dropdownlist"
                                            required disabled>
                                            <option value="">--Pilih Kebupaten/Kota terlebih
                                                dahulu--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Desa</label>
                                        <select name="desa" class="form-control form-control-line" id="dropdownlist"
                                            required disabled>
                                            <option value="">--Pilih Kecamatan terlebih dahulu--
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Alamat</label>
                                        <textarea name="alamat" class="form-control" id="alamat" cols="30"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <small>*klik alamat di google map ketika anda sudah
                                        memilih desa</small>
                                    <div id="googleMap" style="width:100%;height:380px;">
                                    </div>
                                </div>
                                <input type="hidden" name="lat_perusahaan" id="lat" value="{{ $industri['lat'] }}">
                                <input type="hidden" name="long_perusahaan" id="long" value="{{ $industri['long'] }}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBROO3Md6_fZD5_fd1u8VTlRxd4VdJnAWU&libraries=places&sensor=false">
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }
        var id_prov = "{{ $industri['id_provinsi'] }}";
        var id_kab = "{{ $industri['id_kabupaten'] }}";
        var id_kec = "{{ $industri['id_kecamatan'] }}";
        var id_des = "{{ $industri['id_desa'] }}";
        if (id_prov != null && id_kab != null) {
            edit_kabupaten(id_prov, id_kab);
        }
        if (id_kab != null && id_kec != null) {
            edit_kecamatan(id_kab, id_kec);
        }
        if (id_kec != null && id_des != null) {
            edit_desa(id_kec, id_des);
        }

        $('select[name="provinsi"]').on('change', function() {
            var id = $(this).val();
            if (id) {
                $('select[name="kabupaten"]').attr('disabled', 'disabled')
                $.ajax({
                    type: 'POST',
                    url: "{{ route('get_kabupaten-provinsi') }}",
                    data: {
                        id
                    },
                    success: function(data) {
                        // console.log(data);
                        if (!$.trim(data)) {
                            $('select[name="kabupaten"]').html(
                                '<option value="">--- No Kabupaten Found ---</option>');
                        } else {
                            var select = '';
                            var s =
                                '<option value="">--Pilih Provinsi terlebih dahulu--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.name +
                                    '</option>';


                            })
                            $('select[name="kabupaten"]').removeAttr('disabled');
                        }
                        $('select[name="kabupaten"]').html(s)
                        $('select[name="kecamatan"]').html(
                            '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>'
                        );
                        $('select[name="kecamatan"]').attr('disabled', 'disabled');
                        $('select[name="desa"]').html(
                            '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                        );
                        $('select[name="desa"]').attr('disabled', 'disabled');
                    }
                });
            }
        })
        $('select[name="kabupaten"]').on('change', function() {
            var id_kabupaten = $(this).val();
            if (id_kabupaten) {
                $('select[name="kecamatan"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "/super_admin/master/indonesia/kecamatan" + '/' + id_kabupaten,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        if (!$.trim(data)) {
                            $('select[name="kecamatan"]').html(
                                '<option value="">--- No Kecamatan Found ---</option>');
                        } else {
                            var s =
                                '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>';
                            data.districts.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.name +
                                    '</option>';

                            })
                            $('select[name="kecamatan"]').removeAttr('disabled');
                        }
                        $('select[name="kecamatan"]').html(s)
                        $('select[name="desa"]').html(
                            '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                        );
                        $('select[name="desa"]').attr('disabled', 'disabled');

                    }
                });
            }
        })


        $('select[name="kecamatan"]').on('change', function() {
            var id_kecamatan = $(this).val();
            // console.log(id_kecamatan)
            if (id_kecamatan) {
                $('select[name="desa"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "/super_admin/master/indonesia/desa" + '/' + id_kecamatan,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);
                        if (!$.trim(data)) {
                            $('select[name="desa"]').html(
                                '<option value="">--- No Desa Found ---</option>');
                        } else {
                            var s =
                                '<option value="">--Pilih Kecamatan terlebih dahulu--</option>';
                            data.villages.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.name +
                                    '</option>';

                            })
                            $('select[name="desa"]').removeAttr('disabled');
                        }
                        $('select[name="desa"]').html(s)
                    }
                });
            }
        })

        $('select[name="desa"]').on('change', function() {
            var id_desa = $(this).val();
            console.log(id_desa);
            if (id_desa) {
                $.ajax({
                    url: "/super_admin/master/indonesia/detail/desa" + '/' + id_desa,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        var lat_str = data.meta.lat;
                        var lat_int = parseFloat(lat_str);
                        console.log(lat_int);
                        aw_lat = lat_int;
                        var long_str = data.meta.long;
                        var long_int = parseFloat(long_str)
                        console.log(long_int);
                        aw_lng = long_int;
                        var zoom_str = 16;
                        var zoom_int = parseInt(zoom_str);
                        aw_zoom = zoom_int;
                        initMap();
                    }
                });
            }
        })

        var lat_per = "{{ $industri['lat'] }}";
        var long_per = "{{ $industri['long'] }}";
        if (lat_per != null && long_per != null) {
            var aw_lat = lat_per;
            var aw_lng = long_per;
            var aw_zoom = 12;
        } else {
            var aw_lat = -2;
            var aw_lng = 115;
            var aw_zoom = 5;
        }
        console.log("aw_lat" + aw_lat);

        var map;
        var marker;
        var infowindow;
        var messagewindow;

        function initMap() {
            var california = {
                lat: aw_lat,
                lng: aw_lng
            };
            map = new google.maps.Map(document.getElementById('googleMap'), {
                center: california,
                zoom: aw_zoom,
            });

            function placeMarker(location) {
                if (marker) {
                    marker.setPosition(location);
                } else {
                    marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            }

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
                $("#lat").val(marker.getPosition().lat());
                $("#long").val(marker.getPosition().lng());
            });
        }

        function initialize() {
            console.log("set marker");
            var mapOptions = {
                zoom: 12,
                scrollwheel: false,
                center: new google.maps.LatLng(aw_lat, aw_lng)
            };

            var map = new google.maps.Map(document.getElementById('googleMap'),
                mapOptions);

            var image =
                "https://unpkg.com/leaflet@1.7.1/dist/images/marker-icon.png";
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                animation: google.maps.Animation.BOUNCE,
                icon: image,
                map: map
            });

        }

        function edit_kabupaten(id_prov, id_kab) {
            $.ajax({
                url: "{{ route('get-edit_kabupaten') }}",
                type: "POST",
                data: {
                    id_prov: id_prov,
                    id_kab: id_kab
                },
                beforeSend: function() {
                    $('select[name="kabupaten"]').append(
                        '<option value="">--- No Kabupaten Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kabupaten"]').removeAttr('disabled');
                    $('select[name="kabupaten"]').html(fb);
                }
            });
            return false;
        }

        function edit_kecamatan(id_kab, id_kec) {
            $.ajax({
                url: "{{ route('get-edit_kecamatan') }}",
                type: "POST",
                data: {
                    id_kab: id_kab,
                    id_kec: id_kec
                },
                beforeSend: function() {
                    $('select[name="kecamatan"]').append(
                        '<option value="">--- No Kecamatan Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kecamatan"]').removeAttr('disabled');
                    $('select[name="kecamatan"]').html(fb);
                }
            });
            return false;
        }

        function edit_desa(id_kec, id_des) {
            $.ajax({
                url: "{{ route('get-edit_desa') }}",
                type: "POST",
                data: {
                    id_kec: id_kec,
                    id_des: id_des,
                },
                beforeSend: function() {
                    $('select[name="desa"]').append(
                        '<option value="">--- No Desa Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="desa"]').removeAttr('disabled');
                    $('select[name="desa"]').html(fb);
                }
            });
            return false;
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        $('body').on('submit', '#msform', function(e) {
            e.preventDefault();
            var formDatas = new FormData(document.getElementById("msform"));
            $.ajax({
                type: "POST",
                url: "{{ route('bkk_industi-update') }}",
                data: formDatas,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("#btn-save").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#btn-save").attr("disabled", true);
                },
                success: (data) => {
                    if (data.status == 'berhasil') {
                        noti(data.success, data.message);
                    } else {
                        noti(data.success, data.message);
                    }
                    $("#btn-save").attr("disabled", false);
                    $("#btn-save").html('<i class="fa fa-refresh"></i> Perbarui');
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#importBtn').html('Simpan');
                }
            });
        });
    </script>
@endsection
