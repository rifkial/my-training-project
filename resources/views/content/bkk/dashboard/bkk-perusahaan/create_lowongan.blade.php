@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="alert alert-icon alert-info border-info alert-dismissible fade show mt-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">×</span>
        </button> <i class="material-icons list-icon">info</i> <strong>New!</strong> Buat lowongan pekerjaanmu jadi lebih
        mudah.
    </div>
    <div class="alert alert-icon alert-warning border-warning alert-dismissible fade show mt-2" role="alert"> <i
            class="material-icons list-icon">warning</i> <strong>Lengkapi detail perusahaanmu!</strong>
        <br>
        pastikan anda melengkapi detail perusahaan, agar meyakinkan pelamar untuk melamar di perusahaan anda. Untuk
        melengkapinya anda bisa klik <a href="{{ url('program/bursa_kerja/industri/detail') }}">disini</a>.
    </div>
    <div id="wrapper" class="row wrapper multi-step-signup" style="display: block !important">
        <div class="steps-tab clearfix" data-target="#multi-step-signup">
            <ul class="list-unstyled list-inline text-center mt-4">
                <li class="list-inline-item active"><a href="#" style="color: #03a9f3"><span class="step">1</span>
                        Tulis
                        Lowongan</a>
                </li>
                <li class="list-inline-item"><a href="#" style="color: #03a9f3"><span class="step">2</span>
                        Deskripsi Perusahaan</a>
                </li>
                <li class="list-inline-item"><a href="#" style="color: #03a9f3"><span class="step">3</span> Proses
                        Selesai</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 widget-holder col-md-offset-2">
                <form id="lokerDraft" name="lokerDraft" action="javascript:void(0)">
                    @csrf
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Mohon lengkapi detail pekerjaanmu</h5>
                            <p class="text-muted">Detail Pekerjaan</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Judul Pekerjaan?</label>
                                        @if (Request::segment(4) == 'edit')
                                            <input type="hidden" name="id" value="{{ $loker['id'] }}">
                                            <input class="form-control" id="l30" name="judul" type="text"
                                                value="{{ $loker['judul'] }}">
                                        @else
                                            <input class="form-control" id="l30" name="judul" type="text">
                                        @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <p class="mb-0">
                                            <small>Apakah ingin memposting dari situs lain?.</small>
                                        </p>
                                        <label for="l30">Link Pekerjaan</label>
                                        @if (Request::segment(4) == 'edit')
                                            <input class="form-control" id="l30" name="link" type="text"
                                                value="{{ $loker['link'] }}" placeholder="https://sample.com">
                                        @else
                                            <input class="form-control" placeholder="https://sample.com" id="l30" name="link" type="text">
                                        @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Apa bidang dari lowongan pekerjaan</label> <br>
                                        <small>Ceritakan kepada pelamar tentang bidang dari lowongan pekerjaan anda.</small>
                                        <select name="id_bidang_loker" id="" class="form-control">
                                            <option value="">bidang Pekerjaan</option>
                                            @foreach ($bidang as $bd)
                                                @if (Request::segment(4) == 'edit')
                                                    <option value="{{ $bd['id'] }}"
                                                        {{ $bd['id'] == $loker['id_bidang'] ? 'selected' : '' }}>
                                                        {{ $bd['nama'] }}</option>
                                                @else
                                                    <option value="{{ $bd['id'] }}">{{ $bd['nama'] }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-bg mt-3">
                        <div class="widget-body clearfix">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Deskripsi pekerjaan</label>
                                        <p class="mb-0">Tulis atau copy paste deskripsi dari lowongan pekerjaan
                                            anda, agar
                                            pelamar memahaminya.</p>
                                        @if (Request::segment(4) == 'edit')
                                            <textarea class="form-control" name="deskripsi" id="l38"
                                                rows="3">{{ $loker['deskripsi'] }}</textarea>
                                        @else
                                            <textarea class="form-control" name="deskripsi" id="l38" rows="3"></textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l31">Tanggal Buka Loker</label>
                                        <div class="input-daterange input-group datepicker">
                                            @if (Request::segment(4) == 'edit')
                                                <input type="text" class="form-control" name="tgl_buka"
                                                    value="{{ date('d-m-Y', strtotime($loker['tgl_buka'])) }}"> <span
                                                    class="input-group-addon bg-info text-inverse">to</span>
                                                <input type="text" class="form-control" name="tgl_tutup"
                                                    value="{{ date('d-m-Y', strtotime($loker['tgl_tutup'])) }}">
                                            @else
                                                <input type="text" class="form-control" name="tgl_buka"
                                                    value="{{ date('d-m-Y') }}"> <span
                                                    class="input-group-addon bg-info text-inverse">to</span>
                                                <input type="text" class="form-control" name="tgl_tutup"
                                                    value="{{ date('d-m-Y') }}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Syarat</label>
                                        <p class="mb-0">Tulislah persyaratan persyaratan untuk menemukan kandidat
                                            pelamar yg
                                            sesuai</p>
                                        @if (Request::segment(4) == 'edit')
                                            <textarea class="form-control" id="l38" name="syarat"
                                                rows="3">{{ $loker['syarat'] }}</textarea>
                                        @else
                                            <textarea class="form-control" id="l38" name="syarat" rows="3"></textarea>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l31">Kisaran Gaji</label>
                                        <p class="mb-0">Kandidat akan menyukai jika anda menambahkan kisaran gaji
                                            di
                                            lowongan pekerjaan ini.</p>
                                        <label for="l31">Berapa besaran kisaran gaji untuk pekerjaan ini?</label>
                                        <div class="input-group">
                                            @if (Request::segment(4) == 'edit')
                                                <input type="text" class="form-control ribuan" name="gaji_awal"
                                                    value="{{ number_format($loker['gaji_awal']) }}"> <span
                                                    class="input-group-addon bg-info text-inverse">sampai</span>
                                                <input type="text" class="form-control ribuans" name="gaji_sampai"
                                                    value="{{ number_format($loker['gaji_sampai']) }}">
                                            @else
                                                <input type="text" class="form-control ribuan" name="gaji_awal"> <span
                                                    class="input-group-addon bg-info text-inverse">sampai</span>
                                                <input type="text" class="form-control ribuans" name="gaji_sampai">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kontak Recruitment?</label>
                                        <p class="mb-0">bisa diisi dengan nomor telepon, email, dll.</p>
                                        <label for="l31">Pastikan kontak yang anda inputkan bisa dihungi</label>
                                        @if (Request::segment(4) == 'edit')
                                            <input type="hidden" name="id" value="{{ $loker['id'] }}">
                                            <input class="form-control" id="l30" name="recruitment" type="text"
                                                value="{{ $loker['recruitment'] }}">
                                        @else
                                            <input class="form-control" id="l30" name="recruitment" type="text">
                                        @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">Kouta</label>
                                        <p class="mb-0">Kuota karyawan yang dibutuhkan untuk posisi ini.</p>
                                        @if (Request::segment(4) == 'edit')
                                            <input class="form-control" id="l30" name="quota" type="number"
                                                value="{{ $loker['quota'] }}">
                                        @else
                                            <input class="form-control" id="l30" name="quota" type="number">
                                        @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="l30">File Flyer?</label>
                                        <p class="mb-0">bisa diisi dengan gambar brosur, flyer atau yang lainnya.
                                        </p>
                                        <input id="image" type="file" name="image" accept="image/*">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <center>
                        <div class="form-actions btn-list mt-3">
                            @if (Request::segment(4) == 'edit')
                                <input type="hidden" id="optionAksi" name="optionAksi" value="update">
                            @else
                                <input type="hidden" id="optionAksi" name="optionAksi" value="add">
                            @endif
                            <button class="btn btn-primary" type="submit" id="nextStep">Next</button>
                            <a href="#" class="btn btn-default">Preview</a>
                            <a href="#" id="infoSave">Save</a>
                    </center>
            </div>
            </form>
        </div>
    </div>
    <div class="col-md-2"></div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('submit', '#lokerDraft', function(e) {
            $('#nextStep').html('Sending..');
            event.preventDefault();
            var action_url = '';

            if ($('#optionAksi').val() == 'add') {
                action_url = "{{ route('bkk_loker-draft') }}";
                method_url = "POST";
            }

            if ($('#optionAksi').val() == 'update') {
                action_url = "{{ route('bkk_loker-update') }}";
                method_url = "PUT";
            }

            var formData = new FormData(this);
            $.ajax({
                type: method_url,
                url: action_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        window.location = "/program/bursa_kerja/lowongan/next/" + data.id_code;
                    } else {
                        noti(data.success, data.message);
                        $("#nextStep").attr("disabled", false);
                        $("#nextStep").html("Next");
                    }

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });



        // $('#lokerDraft').on('submit', function(event) {
        //     $('#nextStep').html('Sending..');
        //     event.preventDefault();
        //     var action_url = '';
        //     if ($('#optionAksi').val() == 'add') {
        //         action_url = "{{ route('bkk_loker-draft') }}";
        //         method_url = "POST";
        //     }

        //     if ($('#optionAksi').val() == 'update') {
        //         action_url = "{{ route('bkk_loker-update') }}";
        //         method_url = "PUT";
        //     }
        //     $.ajax({
        //         url: action_url,
        //         method: method_url,
        //         data: $(this).serialize(),
        //         dataType: "json",
        //         beforeSend: function() {
        //             $("#infoSave").html(
        //                 '<i class="fa fa-spin fa-spinner"></i> Processing Saving to Draft');
        //             $("#nextStep").attr("disabled", true);
        //         },
        //         success: function(data) {
        //             console.log(data);
        //             if (data.status == 'berhasil') {
        //                 window.location = "/program/bursa_kerja/lowongan/next/" + data.id_code;
        //             } else {
        //                 noti(data.success, data.message);
        //                 $("#nextStep").attr("disabled", false);
        //             }
        //             // $("#btnKirim").html('Kirim Lamaran');
        //         },
        //         error: function(data) {
        //             console.log('Error:', data);
        //             $('#saveBtn').html('Simpan');
        //         }
        //     });
        // });
    </script>
@endsection
