@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">Hi. Kamu mempunyai beberapa agenda</h5>
                        <hr>
                        <div class="row" style="margin-top: 15px">
                            <div class="col-sm-12 col-md-6 col-lg-4"></div>
                            <div class="col-sm-12 col-md-6 col-lg-8">
                                <div class="widget-bg-transparent">
                                    <div class="widget-body clearfix">
                                        <div id='calendar'> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function() {
                var calendar = $('#calendar').fullCalendar({
                    editable: true,
                    events: function(start, end, timezone, callback) {
                        $.ajax({
                            url: "",
                            type: "GET",
                            success: function(obj) {
                                var events = [];
                                $.each(obj, function(index, value) {
                                    events.push({
                                        id: value['id'],
                                        id_loker: value['id_loker'],
                                        start: value['tgl_mulai'],
                                        end: value['tgl_selesai'],
                                        title: value['judul'],
                                        loker: value['loker'],
                                        alamat: value['alamat'],
                                    });
                                });
                                callback(events);
                            }

                        });
                    },
                    displayEventTime: false,
                    eventColor: '#2471d2',
                    eventTextColor: '#FFF',
                    editable: false,
                    eventRender: function(event, element, view) {
                        element.children().last().append(
                            "<br><span class='loker' style='background-color: #fff; color: #2471d2'>LOWONGAN KERJA : " +
                            event
                            .loker + "</span>");
                        if (event.allDay === 'true') {
                            event.allDay = true;
                        } else {
                            event.allDay = false;
                        }
                    }
                });
            });
        });
    </script>
@endsection
