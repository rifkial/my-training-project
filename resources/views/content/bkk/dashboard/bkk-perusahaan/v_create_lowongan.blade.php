@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.7/css/ion.rangeSlider.min.css" rel="stylesheet"
        type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.7/js/ion.rangeSlider.min.js"></script>
    <style>
        .pace {
            display: none;
        }

        #grad1 {
            background-color: #87ceeb;
        }

        #msform {
            text-align: center;
            position: relative;
            margin-top: 20px
        }

        #msform fieldset .form-card {
            background: white;
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            padding: 20px 40px 30px 40px;
            box-sizing: border-box;
            width: 94%;
            margin: 0 3% 20px 3%;
            position: relative
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 0.5rem;
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding-bottom: 20px;
            position: relative
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }

        #msform fieldset .form-card {
            text-align: left;
            color: #9E9E9E
        }

        #msform input,
        select,
        #msform textarea {
            padding: 0px 8px 4px 8px;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 25px;
            margin-top: 2px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 16px;
            letter-spacing: 1px
        }

        #msform input:focus,
        #msform textarea:focus {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            border: none;
            font-weight: bold;
            border-bottom: 2px solid skyblue;
            outline-width: 0
        }

        #msform .action-button {
            width: 100px;
            background: skyblue;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
        }

        select.list-dt {
            border: none;
            outline: 0;
            border-bottom: 1px solid #ccc;
            padding: 2px 5px 3px 5px;
            margin: 2px
        }

        select.list-dt:focus {
            border-bottom: 2px solid skyblue
        }

        .card {
            z-index: 0;
            border: none;
            border-radius: 0.5rem;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #2C3E50;
            margin-bottom: 10px;
            font-weight: bold;
            text-align: left
        }

        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #000000
        }

        #progressbar li {
            list-style-type: none;
            font-size: 12px;
            width: 25%;
            float: left;
            position: relative
        }

        #progressbar #account:before {
            font-family: FontAwesome;
            content: "\f023"
        }

        #progressbar #personal:before {
            font-family: FontAwesome;
            content: "\f0f7"
        }

        #progressbar #payment:before {
            font-family: FontAwesome;
            content: "\f09d"
        }

        #progressbar #confirm:before {
            font-family: FontAwesome;
            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 18px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: skyblue
        }

        .radio-group {
            position: relative;
            margin-bottom: 25px
        }

        .radio {
            display: inline-block;
            width: 204;
            height: 104;
            border-radius: 0;
            background: lightblue;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            box-sizing: border-box;
            cursor: pointer;
            margin: 8px 2px
        }

        .radio:hover {
            box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
        }

        .radio.selected {
            box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        input[type="radio"] {
            margin-left: 20px;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="container-fluid" id="grad1">
                            <div class="row justify-content-center mt-0">
                                <div class="col-11 col-sm-9 col-md-7 col-lg-9 text-center p-0 mt-3 mb-2">
                                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                                        <h2><strong>Pendaftaran Lowongan Kerja</strong></h2>
                                        <p>Fill all form field to go to next step</p>
                                        <div class="row">
                                            <div class="col-md-12 mx-0">
                                                <form id="msform" action="javascript:void(0)" method="POST"
                                                    enctype="multipart/form-data">
                                                    @csrf
                                                    <ul id="progressbar"
                                                        style="width: 70%; margin: auto; margin-bottom: 18px;">
                                                        <div class="row justify-content-center">
                                                            <li class="active" id="account"><strong>Pekerjaan</strong></li>
                                                            <li id="personal"><strong>Perusahaan</strong></li>
                                                            <li id="confirm"><strong>Finish</strong></li>
                                                        </div>
                                                    </ul>
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Tipe Pekerjaan</h2>
                                                            <input type="text" name="judul"
                                                                placeholder="Masukan Judul Pekerjaan" />
                                                            <label for="">Pilih bidang pekerjaan</label> <br>
                                                            @foreach ($bidang as $bd)
                                                                <input type="radio" name="id_bidang_loker" id=""
                                                                    value="{{ $bd['id'] }}"
                                                                    style="width: auto !important">
                                                                <label for=""
                                                                    style="font-family: montserrat; font-size: 16px; letter-spacing: 1px; font-weight: 400; color: #766488;">{{ $bd['nama'] }}</label>
                                                                <br>
                                                            @endforeach
                                                            <label for="">Tanggal Buka Lowongan</label> <br>
                                                            <div class="input-daterange input-group datepicker">
                                                                <input type="text" class="form-control" name="tgl_buka"
                                                                    style="margin-bottom: 0">
                                                                <span
                                                                    class="input-group-addon bg-info text-inverse">to</span>
                                                                <input type="text" class="form-control" name="tgl_tutup"
                                                                    style="margin-bottom: 0">
                                                            </div>
                                                        </div>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Range Gaji</h2>
                                                            <input type="text" data-toggle="rangeslider" name="gaji"
                                                                data-grid-num="100000" data-min="0" data-max="10000000"
                                                                data-from="1000000" data-to="5000000" data-type="double"
                                                                data-step="100000">
                                                        </div>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Syarat Pekerjaan</h2>
                                                            <textarea data-toggle="wysiwyg" name="syarat"></textarea>
                                                        </div>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Deskripsi Pekerjaan</h2>
                                                            <textarea data-toggle="wysiwyg"
                                                                name="deskripsi_pekerjaan"></textarea>
                                                        </div>
                                                        <input type="button" name="next" class="next action-button"
                                                            value="Next Step" />
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title">Informasi Perusahaan</h2>
                                                            <input type="hidden" name="id_perusahaan"
                                                                value="{{ $perusahaan['id'] }}">
                                                            <input type="text" name="perusahaan"
                                                                placeholder="Nama Perusahaan"
                                                                value="{{ $perusahaan['nama'] }}" />
                                                            <input type="text" name="telepon"
                                                                placeholder="Telepon Perusahaan"
                                                                value="{{ $perusahaan['telepon'] }}" />
                                                            <input type="text" name="website"
                                                                placeholder="Website Perusahaan"
                                                                value="{{ $perusahaan['website'] }}" />
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <label class="pay">Provinsi Perusahaan*</label>
                                                                    <select name="provinsi"
                                                                        class="form-control form-control-line"
                                                                        id="dropdownlist" required>
                                                                        <option value="">-- Pilih Provinsi --</option>
                                                                        @foreach ($provinsi as $pro)
                                                                            @php
                                                                                $select = '';
                                                                                if ($perusahaan['id_provinsi'] == $pro['id']) {
                                                                                    $select = 'selected';
                                                                                }
                                                                            @endphp
                                                                            <option value="{{ $pro['id'] }}"
                                                                                {{ $select }}>
                                                                                {{ $pro['name'] }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <label class="pay">Kabupaten/Kota*</label>
                                                                    <select name="kabupaten"
                                                                        class="form-control form-control-line"
                                                                        id="dropdownlist" disabled>
                                                                        <option value="">--Pilih Provinsi terlebih dahulu--
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <label class="pay">Kecamatan*</label>
                                                                    <select name="kecamatan"
                                                                        class="form-control form-control-line"
                                                                        id="dropdownlist" required disabled>
                                                                        <option value="">--Pilih Kebupaten/Kota terlebih
                                                                            dahulu--</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <label class="pay">Desa*</label>
                                                                    <select name="desa"
                                                                        class="form-control form-control-line"
                                                                        id="dropdownlist" required disabled>
                                                                        <option value="">--Pilih Kecamatan terlebih dahulu--
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <small>*klik alamat di google map ketika anda sudah
                                                                        memilih desa</small>
                                                                    <div id="googleMap" style="width:100%;height:380px;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="lat_perusahaan" id="lat"
                                                                value="{{ $perusahaan['lat'] }}">
                                                            <input type="hidden" name="long_perusahaan" id="long"
                                                                value="{{ $perusahaan['long'] }}">
                                                            <textarea name="alamat" placeholder="Alamat" cols="30" rows="4"
                                                                style="margin-top: 12px">{{ $perusahaan['alamat'] }}</textarea>
                                                            <textarea name="deskripsi_perusahaan" data-toggle="wysiwyg"
                                                                placeholder="Deskripsi Perusahaan"
                                                                {{ $perusahaan['deskripsi'] }}></textarea>
                                                            <div class="row">
                                                                <div class="col-8">
                                                                    <label class="pay">Gambar Perusahaan</label>
                                                                    <input id="image" type="file" name="image"
                                                                        accept="image/*" onchange="readURL(this);">
                                                                </div>
                                                                <div class="col-4">
                                                                    <img id="modal-preview"
                                                                        src="{{ $perusahaan['file'] }}" alt="Preview"
                                                                        class="form-group" width="100%"
                                                                        style="margin-top: 10px">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <input type="button" name="previous"
                                                            class="previous action-button-previous" value="Previous" />
                                                        <button type="submit"
                                                            class="btn btn-info ripple text-left action-button"
                                                            id="saveLoker"
                                                            style="width: 100px; font-weight: bold; color: white; border: 0 none; border-radius: 0px; cursor: pointer; padding: none; margin: 10px 5px; font-size: 16px; font-family: montserrat; letter-spacing: 1px">Confirm</button>
                                                        {{-- <input type="button" name="make_payment"
                                                                class="next action-button" value="Confirm" /> --}}
                                                    </fieldset>

                                                    <fieldset>
                                                        <div class="form-card">
                                                            <h2 class="fs-title text-center">Success !</h2> <br><br>
                                                            <div class="row justify-content-center">
                                                                <div class="col-3"> <img
                                                                        src="https://img.icons8.com/color/96/000000/ok--v2.png"
                                                                        class="fit-image"> </div>
                                                            </div> <br><br>
                                                            <div class="row justify-content-center">
                                                                <div class="col-7 text-center">
                                                                    <h5>You Have Successfully Signed Up</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBROO3Md6_fZD5_fd1u8VTlRxd4VdJnAWU&libraries=places&sensor=false">
    </script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id_prov = "{{ $perusahaan['id_provinsi'] }}";
            var id_kab = "{{ $perusahaan['id_kabupaten'] }}";
            var id_kec = "{{ $perusahaan['id_kecamatan'] }}";
            var id_des = "{{ $perusahaan['id_desa'] }}";
            if (id_prov != null && id_kab != null) {
                edit_kabupaten(id_prov, id_kab);
            }
            if (id_kab != null && id_kec != null) {
                edit_kecamatan(id_kab, id_kec);
            }
            if (id_kec != null && id_des != null) {
                edit_desa(id_kec, id_des);
            }


            var current_fs, next_fs, previous_fs;
            var opacity;

            $(".next").click(function() {

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        opacity = 1 - now;
                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
            });

            $(".previous").click(function() {
                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                previous_fs.show();
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        opacity = 1 - now;
                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function() {
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });

            // $(".submit").click(function() {
            //     return false;
            // })


            $('body').on('submit', '#msform', function(e) {
                // return false;
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveLoker').html('Sending..');
                var formDatas = new FormData(document.getElementById("msform"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('lowongan-save_create') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#saveLoker').addClass("active")
                            current_fs = $(this).parent();
                            console.log(current_fs);
                            next_fs = $(this).parent().next();
                            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass(
                                "active");
                            next_fs.show();
                            current_fs.animate({
                                opacity: 0
                            }, {
                                step: function(now) {
                                    opacity = 1 - now;
                                    current_fs.css({
                                        'display': 'none',
                                        'position': 'relative'
                                    });
                                    next_fs.css({
                                        'opacity': opacity
                                    });
                                },
                                duration: 600
                            });
                            noti(data.success, data.message);
                        } else {
                            noti(data.success, data.message);
                            $('#saveLoker').html('Simpan');
                        }

                        // if (data.status == 'berhasil') {
                        //     $('#importFile').trigger("reset");
                        //     $('#importModal').modal('hide');
                        //     $('#importBtn').html('Simpan');
                        //     var oTable = $('#data-tabel').dataTable();
                        //     oTable.fnDraw(false);
                        //     noti(data.icon, data.success);
                        // } else {
                        //     noti(data.icon, data.success);
                        //     $('#importBtn').html('Simpan');
                        // }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $('select[name="provinsi"]').on('change', function() {
                var id_provinsi = $(this).val();
                if (id_provinsi) {
                    $('select[name="kabupaten"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "/admin/master/indonesia/kabupaten" + '/' + id_provinsi,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            // console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="kabupaten"]').html(
                                    '<option value="">--- No Kabupaten Found ---</option>');
                            } else {
                                var select = '';
                                var s =
                                    '<option value="">--Pilih Provinsi terlebih dahulu--</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';


                                })
                                $('select[name="kabupaten"]').removeAttr('disabled');
                            }
                            $('select[name="kabupaten"]').html(s)
                            $('select[name="kecamatan"]').html(
                                '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>'
                            );
                            $('select[name="kecamatan"]').attr('disabled', 'disabled');
                            $('select[name="desa"]').html(
                                '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                            );
                            $('select[name="desa"]').attr('disabled', 'disabled');
                        }
                    });
                }
            })
            $('select[name="kabupaten"]').on('change', function() {
                var id_kabupaten = $(this).val();
                if (id_kabupaten) {
                    $('select[name="kecamatan"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "/admin/master/indonesia/kecamatan" + '/' + id_kabupaten,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="kecamatan"]').html(
                                    '<option value="">--- No Kecamatan Found ---</option>');
                            } else {
                                var s =
                                    '<option value="">--Pilih Kebupaten/Kota terlebih dahulu--</option>';
                                data.districts.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';

                                })
                                $('select[name="kecamatan"]').removeAttr('disabled');
                            }
                            $('select[name="kecamatan"]').html(s)
                            $('select[name="desa"]').html(
                                '<option value="">--Pilih Kecamatan terlebih dahulu--</option>'
                            );
                            $('select[name="desa"]').attr('disabled', 'disabled');

                        }
                    });
                }
            })


            $('select[name="kecamatan"]').on('change', function() {
                var id_kecamatan = $(this).val();
                // console.log(id_kecamatan)
                if (id_kecamatan) {
                    $('select[name="desa"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "/admin/master/indonesia/desa" + '/' + id_kecamatan,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            // console.log(data);
                            if (!$.trim(data)) {
                                $('select[name="desa"]').html(
                                    '<option value="">--- No Desa Found ---</option>');
                            } else {
                                var s =
                                    '<option value="">--Pilih Kecamatan terlebih dahulu--</option>';
                                data.villages.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row.name +
                                        '</option>';

                                })
                                $('select[name="desa"]').removeAttr('disabled');
                            }
                            $('select[name="desa"]').html(s)
                        }
                    });
                }
            })

            $('select[name="desa"]').on('change', function() {
                var id_desa = $(this).val();
                console.log(id_desa);
                if (id_desa) {
                    $.ajax({
                        url: "/admin/master/indonesia/detail/desa" + '/' + id_desa,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            console.log(data);
                            var lat_str = data.meta.lat;
                            var lat_int = parseFloat(lat_str);
                            console.log(lat_int);
                            aw_lat = lat_int;
                            var long_str = data.meta.long;
                            var long_int = parseFloat(long_str)
                            console.log(long_int);
                            aw_lng = long_int;
                            var zoom_str = 16;
                            var zoom_int = parseInt(zoom_str);
                            aw_zoom = zoom_int;
                            initMap();
                        }
                    });
                }
            })
        });
        var lat_per = "{{ $perusahaan['lat'] }}";
        var long_per = "{{ $perusahaan['long'] }}";
        if (lat_per != null && long_per != null) {
            var aw_lat = lat_per;
            var aw_lng = long_per;
            var aw_zoom = 12;
        } else {
            var aw_lat = -2;
            var aw_lng = 115;
            var aw_zoom = 5;
        }
        console.log("aw_lat" + aw_lat);

        var map;
        var marker;
        var infowindow;
        var messagewindow;

        function initMap() {
            var california = {
                lat: aw_lat,
                lng: aw_lng
            };
            map = new google.maps.Map(document.getElementById('googleMap'), {
                center: california,
                zoom: aw_zoom,
            });

            function placeMarker(location) {
                if (marker) {
                    marker.setPosition(location);
                } else {
                    marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            }

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
                $("#lat").val(marker.getPosition().lat());
                $("#long").val(marker.getPosition().lng());
            });
        }

        function initialize() {
            console.log("set marker");
            var mapOptions = {
                zoom: 12,
                scrollwheel: false,
                center: new google.maps.LatLng(aw_lat, aw_lng)
            };

            var map = new google.maps.Map(document.getElementById('googleMap'),
                mapOptions);

            var image =
                "https://unpkg.com/leaflet@1.7.1/dist/images/marker-icon.png";
            var marker = new google.maps.Marker({
                position: map.getCenter(),
                animation: google.maps.Animation.BOUNCE,
                icon: image,
                map: map
            });

        }

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }

        function edit_kabupaten(id_prov, id_kab) {
            $.ajax({
                url: "{{ route('get-edit_kabupaten') }}",
                type: "POST",
                data: {
                    id_prov: id_prov,
                    id_kab: id_kab
                },
                beforeSend: function() {
                    $('select[name="kabupaten"]').append(
                        '<option value="">--- No Kabupaten Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kabupaten"]').removeAttr('disabled');
                    $('select[name="kabupaten"]').html(fb);
                }
            });
            return false;
        }

        function edit_kecamatan(id_kab, id_kec) {
            $.ajax({
                url: "{{ route('get-edit_kecamatan') }}",
                type: "POST",
                data: {
                    id_kab: id_kab,
                    id_kec: id_kec
                },
                beforeSend: function() {
                    $('select[name="kecamatan"]').append(
                        '<option value="">--- No Kecamatan Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kecamatan"]').removeAttr('disabled');
                    $('select[name="kecamatan"]').html(fb);
                }
            });
            return false;
        }

        function edit_desa(id_kec, id_des) {
            $.ajax({
                url: "{{ route('get-edit_desa') }}",
                type: "POST",
                data: {
                    id_kec: id_kec,
                    id_des: id_des,
                },
                beforeSend: function() {
                    $('select[name="desa"]').append(
                        '<option value="">--- No Desa Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="desa"]').removeAttr('disabled');
                    $('select[name="desa"]').html(fb);
                }
            });
            return false;
        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
@endsection
