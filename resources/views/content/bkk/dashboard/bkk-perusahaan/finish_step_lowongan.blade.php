@extends('content/bkk/dashboard/main')
@section('content_dashboard')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Bursa Kerja</h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Bursa Kerja</li>
            </ol>
        </div>
    </div>
    <div class="alert alert-icon alert-info border-info alert-dismissible fade show mt-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button> <i class="material-icons list-icon">info</i> <strong>New!</strong> Buat lowongan pekerjaanmu jadi lebih mudah.
    </div>
    <div class="alert alert-icon alert-warning border-warning alert-dismissible fade show mt-2" role="alert"> <i
            class="material-icons list-icon">warning</i> <strong>Lengkapi detail perusahaanmu!</strong>
        <br>
       pastikan anda melengkapi detail perusahaan, agar meyakinkan pelamar untuk melamar di perusahaan anda. Untuk melengkapinya anda bisa klik <a href="{{ url('program/bursa_kerja/industri/detail') }}">disini</a>.
    </div>
    <div id="wrapper" class="row wrapper multi-step-signup" style="display: block !important">
        <div class="steps-tab clearfix" data-target="#multi-step-signup">
            <ul class="list-unstyled list-inline text-center mt-4">
                <li class="list-inline-item active"><a href="#" style="color: #03a9f3"><span class="step">1</span> Tulis
                        Lowongan</a>
                </li>
                <li class="list-inline-item active"><a href="#" style="color: #03a9f3"><span class="step">2</span>
                        Deskripsi Perusahaan</a>
                </li>
                <li class="list-inline-item active"><a href="#" style="color: #03a9f3"><span class="step">3</span> Proses Selesai</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 widget-holder col-md-offset-2">
                <div class="widget-bg mt-3">
                    <div class="row text-center">
                        <div class="col-sm-3 col-sm-offset-3"></div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <br><br>
                            <h2 style="color:#0fad00">Success</h2>
                            <img src="https://nylawblog.typepad.com/suigeneris/images/2007/06/12/checkmark.jpg">
                            <h3>Kepada, {{ ucwords($profile['nama']) }}</h3>
                            <p style="font-size:20px;color:#5C5C5C;">Terimakasih telah berkontribusi dalam memberikan informasi lowongan kerja.
                                Lowongan Kerja dengan nama <b>{{ $loker['judul'] }}</b> telah berhasil diupload.</p>
                            <a href="{{ url('program/bursa_kerja/lowongan') }}" class="btn btn-info">Kembali</a>
                            <br><br>
                        </div>

                        <div class="col-sm-3 col-sm-offset-3"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#lokerUpdate').on('submit', function(event) {
            $('#btnSave').html('Sending..');
            event.preventDefault();
            $.ajax({
                url: "{{ route('bkk_loker-update_deskripsi') }}",
                method: "PUT",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    //     if (data.status == 'berhasil') {                       
                    //        window.location = "/program/bursa_kerja/lowongan/next2";
                    //    }
                    noti(data.success, data.message);
                    $("#btnSave").html('Kirim Lamaran');
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btnSave').html('Simpan');
                }
            });
        });

    </script>
@endsection
