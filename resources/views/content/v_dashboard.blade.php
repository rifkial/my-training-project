<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;900&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" sizes="20x20"
        href="{{ $config != null && $config['fav_icon'] ? $config['fav_icon'] : $sekolah['file'] }}">
    <title>{{ !empty(Session::get('title')) ? session('title') : ucwords($sekolah['nama']) }}</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
        rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>
</head>
<style>
    .carousel-item.active {
        filter: brightness(83%) !important;
    }

    .carousel-item {
        height: 633px;
        min-height: 350px;
        background: no-repeat center center scroll;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .carousel-caption {
        position: absolute;
        top: 25%;
        left: 0;
        right: 0;
        bottom: 0;
        transform: translateY(-32%);
        text-align: center;
        max-height: 30vh;
    }

    .shadow {
        color: #D5E2D6;
        text-align: center;
        text-shadow: 1px 1px #1e262d, 2px 2px #1e262d, 3px 3px #1e262d, 4px 4px #1e262d, 5px 5px #1e262d, 6px 6px #1e262d, 7px 7px #1e262d, 8px 8px #1e262d;
    }

    .px-4.py-5.text-center {
        background: #00aeff14;
        border-bottom: 4px solid #aee0f7;
    }



    .slide-image {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .slide {
        display: block;
    }

    .content {
        display: none;
        width: 100%;
        height: 100vh;
    }

    .px-4.py-5.text-center {
        background: #00aeff14;
        border-bottom: 4px solid #aee0f7;
    }

    /* .container-fluid.p-0 {
        position: relative;
    } */

    nav.navbar {
        width: 100%;
        position: relative;
        background: #333;
        padding: 10px 0;
        color: #fff;
        top: 0;
        border-bottom: 3px solid #fff;
    }

    .navbar-header {
        position: absolute;
        top: 0;
        width: 6rem !important;
    }

    .navbar-brand {
        margin-bottom: 5px;
        height: 87px;
    }

    .card {
        overflow: hidden;
        border: none;
        /* width:13rem; */
        border: 1px solid #e2e2e2;
        background: #fff;
    }


    .card-text {
        color: #989188;
        font-weight: 500;
    }



    .badge {
        position: absolute;
        top: 50px;
        padding: 12px 40px;
        display: none;
        font-size: 15px;
        background: #ffcc02;
        color: white;
    }

    .container {
        background: #fff;
    }

    .display-5 {
        color: #806966;
    }

    .img-logo {
        height: 100px;
    }

    .logo-expand,
    .logo-collapse {
        max-width: 100% !important;
    }

    .img-wrap {
        position: relative;
    }

    .img-wrap:hover {
        background: #cde8f5;
    }

    .img-wrap:hover>.badge {
        display: block;
    }


    .title,
    .title-sch {
        color: #144054;
    }

    .footer {
        position: static;
        border-top: 3px solid #fff;
    }

    @media (max-width: 960px) {
        .footer {
            position: static;
        }

        .container.bg-white.p-3.m-0 {
            margin-top: 92px !important;
        }

        .navbar .logo-collapse {
            display: inline-block;
            width: auto;
        }
    }

    @keyframes example {
        0% {
            left: 1000px;
            top: 0px;
        }

        20% {
            left: 800px;
            top: 0px;
        }
    }


    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 3.5s;
    }

    @-webkit-keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

    @keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

</style>

@if (Session::has('message'))
    @if(!empty(session('message')['status']))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['success'] }}',
            '{{ session('message')['icon'] }}');
    </script>
    @endif
@endif

<body class="sidebar-light sidebar-expand">
    <nav class="navbar" style="background-color: #6f42c1">
        <div class="navbar-header">
            <a href="javacript:void(0)" class="navbar-brand bg-transparent" style="display: block !important">
                <img class="logo-expand p-2" alt=""
                    src="{{ $config != null && $config['logo'] != null ? $config['logo'] : $sekolah['file'] }}"
                    style="height: inherit;">
                <img class="logo-collapse p-2" alt=""
                    src="{{ $config != null && $config['logo'] != null ? $config['logo'] : $sekolah['file'] }}"
                    style="height: inherit;">
                <span class="text-white logo-expand text-uppercase">
                    {{ $config != null && $config['header'] != null ? $config['header'] : $sekolah['nama'] }} </span>
                <span class="text-white logo-collapse text-uppercase">
                    {{ $config != null && $config['header'] != null ? $config['header'] : $sekolah['nama'] }} </span>
            </a>
        </div>
        <div class="nav-header" style="position: absolute;top: 0;right: 20px; display: none">
            <a href="{{ url('auth/login/adm') }}" class="navbar-brand text-center bg-transparent">
                <i class="fa fa-user-circle"></i>
            </a>
        </div>
    </nav>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            @php
                $first_data = reset($slider);
                array_shift($slider);
            @endphp
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active"
                    style="background-image: url({{ !empty($first_data['file']) ? $first_data['file'] : 'https://images.unsplash.com/photo-1580582932707-520aed937b7b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80' }})">
                    <div class="carousel-caption d-none d-md-block">
                        <img src="{{ !empty($config) ? $config['logo'] : $sekolah['file'] }}" alt="" height="200">
                        <h3 class="shadow text-light my-0">
                            {{ $config != null && $config['text1'] != null ? $config['text1'] : 'Learning Management System' }}
                        </h3>
                        <h2 class="shadow text-light display-4">
                            {{ $config != null && $config['text1'] != null ? $config['text2'] : $sekolah['nama'] }}
                        </h2>
                    </div>
                </div>
                @foreach ($slider as $sd)
                    <div class="carousel-item" style="background-image: url({{ $sd['file'] }})">
                        <div class="carousel-caption d-none d-md-block">
                            <img src="{{ !empty($config) ? $config['logo'] : $sekolah['file'] }}" alt=""
                                height="200">
                            <h3 class="shadow text-light my-0">
                                {{ $config != null && $config['text1'] != null ? $config['text1'] : 'Learning Management System' }}
                            </h3>
                            <h2 class="shadow text-light display-4">
                                {{ $config != null && $config['text1'] != null ? $config['text2'] : $sekolah['nama'] }}
                            </h2>
                        </div>
                    </div>
                @endforeach

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
    </header>
    <div class="px-4 py-5 text-center">
        <h1 class="display-5 fw-bold text-info">{{ !empty($config) ? $config['title'] : 'Selamat Datang' }}</h1>
        <div class="col-lg-8 mx-auto">
            <p class="lead mb-4">
                {{ !empty($config) && $config['deskripsi'] != null
                    ? $config['deskripsi']
                    : 'Program Smart School "Learning Management System" menyediakan berbagai fitur
                                                                aplikasi menarik dan lengkap untuk menunjang pengelolaan management sekolah Anda.
                                                                Smart School dapat digunakan oleh siapa saja dan dapat diakses kapan saja secara online' }}
            </p>
            <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <a href="#main-fitur" type="button" class="btn btn-outline-info btn-lg px-4 gap-3">Jelajahi fitur</a>
            </div>
        </div>
    </div>
    <div id="main-fitur" class="container p-3" style="max-width: 100%;">
        <div class="row mt-2 g-4 justify-content-center">
            <div class="header-caption"></div>
            <div class="row flex-column align-items-center">
                <h2 class="font-weight-bold" style="color: #205b5d;"></h2>
                <h4 class="mt-1 mt-1 text-center" style="color: #205b5d;"> BERBAGAI APLIKASI PEMBELAJARAN </h4>
            </div>
            <div class="container p-3 m-0" style="max-width: 100%;">

                <div class="row mt-2 g-4 justify-content-center">
                    <div class="col-md-10 col-12">
                        <div class="row">
                            @foreach ($data as $pro)
                                <div class="col-md-4 col-6 px-1">
                                    @if ($pro['status'] == 0)
                                        <a href="javascript:void(0)" onclick="nonAktif()">
                                        @elseif($pro['status'] == 2)
                                            <a href="{{ $pro['link'] }}">
                                            @else
                                                @if ($pro['kode'] == 'bursa_kerja')
                                                    <a href="{{ route('bursa_kerja-home') }}">
                                                    @elseif($pro['kode'] == 'sistem_pkl')
                                                        <a href="{{ route('prakerin-beranda') }}">
                                                        @elseif($pro['kode'] == 'ppdb')
                                                            <a href="{{ route('ppdb-public') }}">
                                                            @elseif($pro['kode'] == 'perpustakaan')
                                                                <a href="{{ route('perpus-public') }}">
                                                                @else
                                                                    <a href="{{ url('program', $pro['kode']) }}">
                                                @endif
                                    @endif
                                    <div class="card m-2">
                                        <span class="d-flex justify-content-center p-3 img-wrap"
                                            style="background: {{ $pro['status'] == 0 ? '#f7aeae' : '#9768a085' }}">
                                            <span class="badge badge-pill">{{ strtoupper($pro['nama']) }}</span>
                                            <img src="{{ $pro['file'] }}" class="img-logo" />
                                        </span>
                                        <div class="card-body px-1 bg-light text-center">
                                            <h5 class="card-title mb-1 font-weight-bold">
                                                {{ strtoupper($pro['nama']) }}</h5>
                                            <small class="card-text">{{ $pro['deskripsi'] }}</small>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <footer class="footer text-center clearfix footer-main ml-0 text-light bg-purple">
        {{ !empty($config) ? $config['footer'] : '2021 © ' . strtoupper($sekolah['nama']) }}
    </footer>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".save_program").click(function() {
                var id = $(this).data('id')
                $.ajax({
                    url: "{{ route('session-program') }}",
                    method: "POST",
                    data: {
                        'id_program': id
                    },
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                    }
                });
            });

            var slideIndex = 0;
            window.showSlides = function showSlides() {
                var i;
                var slides = document.getElementsByClassName("content");
                if (slides.length > 0) {
                    for (i = 0; i < slides.length; i++) {
                        slides[i].style.display = "none";

                    }
                    slideIndex++;
                    if (slideIndex > slides.length) {
                        slideIndex = 1
                    }

                    slides[slideIndex - 1].style.display = "block";
                    setTimeout(showSlides, 7000); // Change image every 2 seconds
                }
            }

            showSlides();

        });

        function nonAktif() {
            swal('Maaf!', 'Anda tidak diijinkan untuk mengkases program ini', 'error');
        }
    </script>
</body>

</html>
