@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    @if (session('role') == 'bkk-admin')
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-info addPelamar"><i class="fas fa-plus-circle"></i> Tambah
                                    User Industri</button>
                                <button class="btn btn-purple" id="import"><i class="fas fa-file-upload"></i> Import
                                    User Industri</button>
                            </div>
                        </div>

                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <form class="navbar-form pull-right" role="search">
                                <div class="form-row align-items-center">
                                    <div class="col-auto">
                                        <select name="status" id="status" class="form-control">
                                            <option value="all" {{ $status == 'all' ? 'selected' : '' }}>Semua..</option>
                                            <option value="active" {{ $status == 'active' ? 'selected' : '' }}>Aktif
                                            </option>
                                            <option value="deactive" {{ $status == 'deactive' ? 'selected' : '' }}>Tidak
                                                Aktif/Pending</option>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <div class="input-group">
                                            @php
                                                $serc = str_replace('-', ' ', $search);
                                            @endphp
                                            <input type="text" value="{{ $serc }}" id="search" name="search"
                                                class="form-control" placeholder="Search">
                                            <div class="input-group-btn">
                                                <a href="javascript:void(0)" id="fil"
                                                    onclick="filter('{{ $routes }}')" class="btn btn-info"><i
                                                        class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($pelamar))
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($pelamar as $plm)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($plm['nama']) }}</td>
                                            <td>{{ $plm['telepon'] }}</td>
                                            <td>{{ $plm['email'] }}</td>
                                            <td><span
                                                    class="p-2 badge badge-{{ $plm['status'] == 1 ? 'success' : 'danger' }} text-inverse">{{ $plm['status'] == 1 ? 'Aktif' : 'Pending / Tidak Aktif' }}</span>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" data-toggle="collapse"
                                                    data-target="#demo{{ $plm['id'] }}" class="btn btn-sm btn-purple"><i
                                                        class="fas fa-info-circle"></i></a>
                                                @if (session('role') == 'bkk-admin')
                                                    <a href="javascript:void(0)" data-id="{{ $plm['id'] }}"
                                                        class="btn btn-sm btn-info edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                        data-id="{{ $plm['id'] }}"><i class="fas fa-trash-alt"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="demo{{ $plm['id'] }}">
                                                    <table class="table table-striped">
                                                        <tr class="bg-purple">
                                                            <th colspan="5" class="text-white"><i
                                                                    class="fas fa-user-alt"></i> Profile Alumni</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="3" rowspan="5" class="text-center vertical-middle">
                                                                <img src="{{ env('API_URL') . 'public/' . $plm['file'] }}"
                                                                    alt="" style="max-width: 200px">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Nama</th>
                                                            <td class="vertical-middle">{{ Str::upper($plm['nama']) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Email</th>
                                                            <td class="vertical-middle">{{ $plm['email'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Password Reset</th>
                                                            <td class="vertical-middle">{{ $plm['first_password'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Telepon</th>
                                                            <td class="vertical-middle">{{ $plm['telepon'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Agama</th>
                                                            <td class="vertical-middle">{{ $plm['agama'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Jenkel</th>
                                                            <td class="vertical-middle">
                                                                {{ $plm['jenkel'] == 'l' ? 'Laki - laki' : 'Perempuan' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Tempat Lahir</th>
                                                            <td class="vertical-middle">{{ $plm['tempat_lahir'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Tanggal Lahir</th>
                                                            <td>{{ $plm['tgl_lahir'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Pendidikan Akhir</th>
                                                            <td class="vertical-middle">
                                                                {{ Str::upper($plm['pendidikan_akhir']) }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Terakhir Login</th>
                                                            <td class="vertical-middle">{{ $plm['last_login'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Alamat</th>
                                                            <td colspan="4" class="vertical-middle">{{ $plm['alamat'] }}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th class="vertical-middle">Dibuat</th>
                                                            <td class="vertical-middle">{{ $plm['created_at'] }}</td>
                                                            <td class="vertical-middle"></td>
                                                            <th class="vertical-middle">Diupdate</th>
                                                            <td class="vertical-middle">{{ $plm['updated_at'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $pagination !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_pelamar">
                        <input type="hidden" name="role" value="industri">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" name="nama" id="nama" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-froup">
                                    <label for="">Email</label>
                                    <input type="text" name="email" id="email" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6" id="kerja">
                                <div class="form-group">
                                    <label for="">Agama</label>
                                    <select name="agama" id="agama" class="form-control">
                                        <option value="islam">Islam</option>
                                        <option value="kristen">Kristen</option>
                                        <option value="hindu">Hindu</option>
                                        <option value="budha">Budha</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Jenis Kelamin</label>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Telepon</label>
                                    <input type="text" name="telepon" id="telepon" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Pendidikan Akhir</label>
                                    <select name="pendidikan_akhir" id="pendidikan_akhir" class="form-control">
                                        <option value="sd">SD / Sederajat</option>
                                        <option value="smp">SMP / Sederajat</option>
                                        <option value="sma">SMA / Sederajat</option>
                                        <option value="d3">D3</option>
                                        <option value="s1">S1</option>
                                        <option value="s2">S2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Tanggal Lahir</label>
                                    <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Alamat</label>
                                    <textarea name="alamat" id="alamat" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="l1">Gambar</label>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6" style="position: relative">
                                            <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                        </div>
                                    </div>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Pelamar</h5>
                </div>
                <form id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <input type="hidden" name="role" value="industri">
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('click', '.addPelamar', function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah User Industri");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('click', '#import', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import User Industri');
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importFile"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-user_bkk') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formImport').trigger("reset");
                            $('#importModal').modal('hide');
                            location.reload()
                        }
                        noti(data.icon, data.success);
                        $('#importBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $('body').on('click', '.edit', function() {
                var id = $(this).data('id');
                const loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('user_bkk-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $('#modelHeading').html("Detail Data User Industri");
                        $('#id_pelamar').val(data.id);
                        $('#nama').val(data.nama);
                        $('#jenkel').val(data.jk);
                        $('#email').val(data.email);
                        $('#telepon').val(data.telepon);
                        $('#pendidikan_akhir').val(data.pendidikan_akhir);
                        $('#tgl_lahir').val(data.tgl_lahir);
                        $('#agama').val(data.agama);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#alamat').val(data.alamat);
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_check != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action').val('Edit');
                        $(loader).html(
                            '<i class="fas fa-pencil-alt"></i>');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('user_bkk-create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('user_bkk-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            location.reload();
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('click', '.delete', function() {
                let id = $(this).data('id');
                const loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('user_bkk-sof_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash-alt"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?status=" + status + "&search=" + search;
            document.location = url;
        }
    </script>
@endsection
