@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Gambar</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>TTL</th>
                        <th>Password Reset</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_admin">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nik" id="nik" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NUPTK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nuptk" id="nuptk" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIP</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nip" id="nip" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="old_image" id="old_image">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group" width="100%" style="margin-top: 10px">
                                        <div id="delete_foto" style="text-align: center"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="status" id="status" class="form-control">
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        text: '<i class="fa fa-upload"></i>',
                        className: 'btn btn-sm',
                        attr: {
                            title: 'Import Data',
                            id: 'import'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'ttl',
                        name: 'ttl'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-siswa') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'tahun_angkatan',
                        name: 'tahun_angkatan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Admin BKK");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('bkk_admin-save') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('bkk_admin-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#importFile', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importFile"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-siswa') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#importFile').trigger("reset");
                            $('#importModal').modal('hide');
                            $('#importBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#importBtn').html('Simpan');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                var loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "admin_bkk/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Admin BKK");
                        $('#saveBtn').val("edit-user");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nik').val(data.nik);
                        $('#nuptk').val(data.nuptk);
                        $('#nip').val(data.nip);
                        $('#agama').val(data.agama).trigger("change");
                        $('#jenkel').val(data.jenkel).trigger("change");
                        $('#status').val(data.status).trigger("change");
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#alamat').val(data.alamat);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tanggal_lahir').val(data.tgl_lahir);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != "default.png") {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo when saving');
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "admin_bkk/soft_delete/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            // $('#data-trash').dataTable().fnDraw(false);
                        }
                            swa(data.status+"!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })

        }

        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            if (id_jurusan) {
                $('select[name="id_kelas"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ url('program/kelas/jurusan') }}" + '/' + id_jurusan,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        var s = '<option value="">---select---</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama + '</option>';

                        })
                        $('select[name="id_kelas"]').removeAttr('disabled')
                        $('select[name="id_kelas"]').html(s)
                    }
                });
            }
        })

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })

        function template() {
            window.location.href = "{{ $url }}";
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/user/siswa/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/user/siswa/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

    </script>
@endsection
