@extends('content.admin.setting.v_data_setting')
@section('content_setting')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        div#data-tabel_length {
            padding-top: 0;
            margin-top: 0;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Program</th>
                        <th>Template</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row" id="data_template">

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'program',
                        name: 'program'
                    },
                    {
                        data: 'template',
                        name: 'template'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $('#saveBtn').html('Sending..');
                $.ajax({
                    url: "{{ route('update-program_sekolah') }}",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#data_template').html('');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "program_template/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Program Sekolah");
                        $('#data_template').html(data);
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        function terapkan(id, id_template, id_program) {
            console.log("menerapkan");
            $.ajax({
                type: 'POST',
                url: "{{ route('update-template') }}",
                data: {
                    id,
                    id_template,
                    id_program,
                },
                beforeSend: function() {
                    $(".ter" + id + id_template + id_program).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#data-tabel').dataTable().fnDraw(false);
                        $('#ajaxModel').modal('hide');
                        $(".ter" + id + id_template + id_program).html(
                            'Terpasang');
                    } else {
                        $(".ter" + id + id_template + id_program).html(
                            'Terapkan');
                    }
                    swa(data.status + " !", data.success, data.icon);
                }
            });
        }

    </script>
@endsection
