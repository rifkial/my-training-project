@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Gambar</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Password Reset</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_ortu">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <input type="text" name="nama" id="nama" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <input type="text" name="nik" id="nik" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Rombel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="rombel" id="rombel" class="form-control" required>
                                                <option value="">Pilih Rombel</option>
                                                @foreach ($rombel as $r)
                                                    <option value="{{ $r['id'] }}">{{ $r['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Siswa</label>
                                    <div class="col-md-9">
                                        <select name="siswa" id="siswa" class="form-control" required></select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pekerjaan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Orang Tua</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')


    <script type="text/javascript">
        var url_import = "{{ route('import-ortu') }}";
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('#data-tabel').DataTable({
                ...settingTable,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-orang_tua') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Orang Tua');
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Orang Tua");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var actionType = $('#saveBtn').val();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-orang_tua') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-orang_tua') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status != "gagal") {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "ortu/edit",
                    data: {
                        id_ortu: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Orang Tua");
                        $('#saveBtn').val("edit-user");
                        $('#id_ortu').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nik').val(data.nik);
                        $('#rombel').val(data.id_rombel).trigger("change");
                        $('#agama').val(data.agama).trigger("change");
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#pekerjaan').val(data.pekerjaan);
                        $('#old_image').val(data.old_image);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        ortu_edit(data.id_siswa, data.id_rombel);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "ortu/delete",
                    type: "POST",
                    data: {
                        id_ortu: id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        $('select[name="rombel"]').on('change', function() {
            var id_rombel = $(this).val();
            if (id_rombel) {
                $('select[name="siswa"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('rombel-kelas_siswa') }}",
                    type: "POST",
                    data: {
                        id_rombel: id_rombel
                    },
                    success: function(data) {
                        var s = '<option value="">---select---</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama + '</option>';

                        })
                        $('select[name="siswa"]').removeAttr('disabled')
                        $('select[name="siswa"]').html(s)
                    }
                });
            }
        })

        function ortu_edit(id_siswa, id_rombel) {
            $.ajax({
                url: "ortu/ortu_edit",
                type: "POST",
                data: {
                    id_siswa: id_siswa,
                    id_rombel: id_rombel
                },
                beforeSend: function() {
                    $('#siswa').append(
                        '<option value="">--- No Siswa Found ---</option>');
                },
                success: function(fb) {
                    console.log(fb);
                    $('#siswa').html(fb);
                }
            });
            return false;
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/user/ortu/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/user/ortu/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }
    </script>
@endsection
