@extends('content.admin.user.v_data_user')
@section('content_user')
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="m-portlet__body">
                        <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        <hr>
                        <button class="btn btn-info addData">Tambah Data</button>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jurusan</th>
                                        <th>Nama Guru</th>
                                        <th>NIP</th>
                                        <th>NIK</th>
                                        <th>NUPTK</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data-kaprodi">
                                    @if (!empty($kaprodi))
                                        @php
                                            $nomer = 1;
                                        @endphp
                                        @foreach ($kaprodi as $kd)
                                            <tr>
                                                <td>{{ $nomer++ }}</td>
                                                <td>{{ $kd['jurusan'] }}</td>
                                                <td>{{ $kd['nama'] }}</td>
                                                <td>{{ $kd['nip'] }}</td>
                                                <td>{{ $kd['nik'] }}</td>
                                                <td>{{ $kd['nuptk'] }}</td>
                                                <td>
                                                    <a href="{{ route('detail-user_kaprodi', ['code' => (new \App\Helpers\Help())->encode($kd['id']), 'name' => str_slug($kd['nama'])]) }}"><i
                                                            class="material-icons list-icon md-18 text-success">info</i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $kd['id'] }}"
                                                        class="edit"><i
                                                            class="material-icons list-icon md-18 text-info">edit</i></a>
                                                    <a href="javascript:void(0)"
                                                        onclick="deleteKaprodi({{ $kd['id'] }})"><i
                                                            class="material-icons list-icon md-18 text-danger">delete</i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">Data Kaprodi saat ini kosong</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalForm" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingForm"></h5>
                </div>
                <form id="userForm" action="javascript:void(0)" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kaprodi">
                        <div class="form-group">
                            <label for="">Pilih Guru yang ingin dijadikan Kaprodi</label>
                            <select name="id_guru" id="id_guru" class="form-control">
                                <option value="">-- Pilih Guru --</option>
                                @foreach ($guru as $gr)
                                    <option value="{{ $gr['id'] }}">{{ $gr['nama'] . ',  NIP ' . $gr['nip'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Pilih Jurusan</label>
                            <select name="id_jurusan" id="id_jurusan" class="form-control">
                                <option value="">-- Pilih Jurusan --</option>
                                @foreach ($jurusan as $jr)
                                    <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.addData').click(function() {
                $('#userForm').trigger("reset");
                $('#headingForm').html("Tambah Kaprodi");
                $('#modalForm').modal('show');
                $('#action').val('Add');
            });

            $('#userForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-user_kaprodi') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-user_kaprodi') }}";
                    method_url = "POST";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {},
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalForm').modal('hide');
                            $('#userForm').trigger("reset");
                        }
                        $('#data-kaprodi').html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-user_kaprodi') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-user-edit"></i>');
                        $('#modelHeading').html("Edit Data");
                        $('#id_kaprodi').val(data.id);
                        $('#id_jurusan').val(data.id_jurusan).trigger('change');
                        $('#id_guru').val(data.id_guru).trigger('change');
                        $('#modalForm').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function deleteKaprodi(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('delete-user_kaprodi') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    // beforeSend: function() {
                    //     $(".delete-" + id).html(
                    //         '<i class="fa fa-spin fa-spinner"></i> Loading');
                    // },
                    success: function(data) {
                        $('#data-kaprodi').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
