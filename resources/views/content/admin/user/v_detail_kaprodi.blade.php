@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .profile-img {
            width: 140px;
            height: 140px;
            border: 3px solid #ADB5BE;
            padding: 5px;
            border-radius: 50%;
            background-position: center center;
            background-size: cover;
        }

        .nav-pills .nav-link.active,
        .show>.nav-pills .nav-link {
            background: #38d57a;
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right">
            <a href="{{ route('user-kaprodi') }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="javascript:void(0)" class="btn btn-facebook refresh"><i class="fas fa-redo"></i> Refresh</a>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-4 widget-holder">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white my-0">Detail Data Kaprodi</h5>
                    </div>
                    <div class="card-body">
                        <div class="box-info text-center user-profile-2">
                            <div class="user-profile-inner">
                                <div class="profile-img m-auto" style="background-image: url({{ $kaprodi['file'] }});"></div>
                                <h4 class="my-2">{{ ucwords($kaprodi['nama']) }}</h4>
                                <p class="m-0">Jurusan {{ $kaprodi['jurusan'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 widget-holder">
                <form id="formEdit">
                    <div class="card">
                        <div class="card-header alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#informasi"
                                        data-toggle="tab">Informasi</a>
                                </li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#detail"
                                        data-toggle="tab">Detail Kaprodi</a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#password"
                                        data-toggle="tab">Password</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="informasi">
                                    <div class="form-group row">
                                        <input type="hidden" name="id" value="{{ $kaprodi['id'] }}">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Lengkap</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ ucwords($kaprodi['nama']) }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIP</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['nip'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NIK</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['nik'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NUPTK</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['nuptk'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">No. HP</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['telepon'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Email</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['email'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tempat, Tanggal lahir</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['tempat_lahir'] .",".$kaprodi['tgl_lahir'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Alamat</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['alamat'] }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="detail">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Jurusan</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['jurusan'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Informasi Lain</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['informasi_lain'] }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Agama</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['agama'] }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="password">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Password</label>
                                        <div class="col-md-9">
                                            <p class="form-control-plaintext">{{ $kaprodi['first_password'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        })


    </script>
@endsection
