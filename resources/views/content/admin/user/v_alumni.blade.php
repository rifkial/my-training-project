@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h5 class="box-title">{{ session('title') }}</h5>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-info addAlumni"><i class="fas fa-plus-circle"></i> Tambah
                                        Alumni</button>
                                    <button class="btn btn-purple" id="import"><i class="fas fa-file-upload"></i> Import
                                        Alumni</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button class="btn btn-danger" id="delete_select"><i class="fas fa-trash"></i></button>
                            <form class="navbar-form" role="search">
                                <div class="form-row">
                                    <div class="col">
                                        <select name="tahun_angkatan" id="tahun_angkatan" class="form-control">
                                            <option value="">Pilih Tahun Angkatan..</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" class="form-control" id="search"
                                            placeholder="Search">
                                    </div>
                                    {{-- <div class="col"> --}}

                                    <button class="btn btn-info" type="submit" id="fil"
                                        onclick="filter('{{ $routes }}')"><i class="fa fa-search"></i></button>
                                    {{-- </div> --}}
                                </div>
                                {{-- <div class="input-group">
                                    @php
                                        $serc = str_replace('-', ' ', $search);
                                    @endphp
                                    <input type="text" value="{{ $serc }}" id="search" name="search"
                                        class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                            class="btn btn-info"><i class="fa fa-search"></i></a>
                                    </div>
                                </div> --}}
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive mt-2">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">
                                        <div class="form-check m-0">
                                            <input class="form-check-input m-0" type="checkbox" id="checkAll">
                                        </div>
                                    </th>
                                    <th class="text-center">NO</th>
                                    <th>Profile</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($alumni))
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($alumni as $al)
                                        <tr>
                                            <td class="text-center vertical-middle">
                                                <div class="form-check m-0">
                                                    <input class="form-check-input m-0 siswa_entry" type="checkbox"
                                                        name="alumni[]" value="{{ $al['id'] }}">
                                                </div>
                                            </td>
                                            <td class="vertical-middle text-center">{{ $no++ }}</td>
                                            <td>
                                                <b>{{ Str::upper($al['nama']) }}</b>
                                                <p class="m-0">NIS/NISN.
                                                    {{ $al['nis'] != null ? $al['nis'] : '-' }} /
                                                    {{ $al['nisn'] != null ? $al['nisn'] : '-' }}</p><small>Jurusan.
                                                    {{ $al['jurusan'] }}</small>
                                            </td>
                                            <td class="vertical-middle">{{ $al['ponsel'] }}</td>
                                            <td class="vertical-middle">{{ $al['email'] }}</td>
                                            <td class="vertical-middle text-center">
                                                <a href="javascript:void(0)" data-toggle="collapse"
                                                    data-target="#demo{{ $al['id'] }}" class="btn btn-sm btn-purple"><i
                                                        class="fas fa-info-circle"></i></a>
                                                {{-- @if (session('role') == 'bkk-admin') --}}
                                                <a href="{{ route('detail-alumni', ['code' => (new \App\Helpers\Help())->encode($al['id']), 'name' => str_slug($al['nama'])]) }}"
                                                    class="btn btn-sm btn-info"><i class="fas fa-pencil-alt"></i></a>
                                                {{-- <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                    data-id="{{ $al['id'] }}"><i class="fas fa-trash-alt"></i></a> --}}
                                                {{-- @endif --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="demo{{ $al['id'] }}">
                                                    <table class="table table-striped">
                                                        <tr class="bg-purple">
                                                            <th colspan="5" class="text-white"><i
                                                                    class="fas fa-user-alt"></i> Profile Alumni</th>
                                                        </tr>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <td>{{ Str::upper($al['nama']) }}</td>
                                                            <td></td>
                                                            <th>NIS</th>
                                                            <td>{{ $al['nis'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>NISN</th>
                                                            <td>{{ $al['nisn'] }}</td>
                                                            <td></td>
                                                            <th>Email</th>
                                                            <td>{{ $al['email'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Pekerjaan</th>
                                                            <td>{{ $al['pekerjaan'] }}</td>
                                                            <td></td>
                                                            <th>Bidang Usaha</th>
                                                            <td>{{ $al['bidang_usaha'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Sesuai Jurusan</th>
                                                            <td>{{ $al['sesuai_jurusan'] }}</td>
                                                            <td></td>
                                                            <th>Universitas</th>
                                                            <td>{{ $al['universitas'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Telepon</th>
                                                            <td>{{ $al['ponsel'] }}</td>
                                                            <td></td>
                                                            <th>Tahun Angkatan</th>
                                                            <td>{{ $al['angkatan'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tempat Lahir</th>
                                                            <td>{{ $al['tempat_lahir'] }}</td>
                                                            <td></td>
                                                            <th>Tanggal Lahir</th>
                                                            <td>{{ $al['tgl_lahir'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Jurusan</th>
                                                            <td>{{ $al['jurusan'] }}</td>
                                                            <td></td>
                                                            <th>Terkahir Login</th>
                                                            <td>{{ $al['last_login'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Alamat</th>
                                                            <td colspan="4">{{ $al['alamat'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $pagination !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalAlumni" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAlumni">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Nama Alumni</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i>
                                    </span>
                                    <input class="form-control" id="nama" name="nama" placeholder="Nama Alumni"
                                        type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NIS</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-id-card"></i>
                                    </span>
                                    <input class="form-control" id="nis" name="nis" placeholder="NIS" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">NISN</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-address-card"></i> </span>
                                    <input class="form-control" id="nisn" name="nisn" placeholder="NISN" type="number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-venus-mars"></i> </span>
                                    <select name="jenkel" id="jenkel" class="form-control">
                                        <option value="" disabled>Pilih Jenis Kelamin..</option>
                                        <option value="l">Laki - laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Jurusan</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-graduation-cap"></i> </span>
                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                        <option value="">Pilih Jurusan..</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Email</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i class="fas fa-at"></i>
                                    </span>
                                    <input class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Angkatan</label>
                            <div class="col-md-5">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-calendar"></i>
                                    </span>
                                    <input class="form-control datepickerYear" id="tahun" name="tahun_angkatan1" readonly
                                        onchange="getYear(this.value)" placeholder="Tahun" value="{{ date('Y') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input class="form-control" value="{{ date('Y') + 1 }}" readonly id="tahun_ajaran2" name="tahun_ajaran2"
                                        placeholder="Tahun">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">No. HP</label>
                            <div class="col-md-9">
                                <div class="input-group"><span class="input-group-addon"><i
                                            class="fas fa-mobile-alt"></i>
                                    </span>
                                    <input type="number" class="form-control" id="telepon" name="telepon"
                                        placeholder="No. HP">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l2">Password</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-key"></i></span>
                                    <input class="form-control" id="password" name="password" placeholder="PASSWORD"
                                        type="text">
                                    <span class="input-group-btn">
                                        <a class="btn btn-danger generate" href="javascript: void(0);"><i
                                                class="fas fa-sync-alt"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Pelamar</h5>
                </div>
                <form id="formImport" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-info">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function() {
            var url_import = "{{ route('import-alumni') }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(document).on('click', '.generate', function() {
                $('#password').val(Math.floor(Math.random() * 90000) + 10000);
            });

            $('body').on('click', '.addAlumni', function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Alumni");
                $('#modalAlumni').modal('show');
                $('#action').val('Add');
            });

            $('body').on('click', '#import', function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Alumni');
            });



            $('body').on('submit', '#formAlumni', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('store-alumni') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            location.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importFile"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('import-alumni') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formImport').trigger("reset");
                            $('#importModal').modal('hide');
                            location.reload()
                        }
                        noti(data.icon, data.success);
                        $('#importBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $('#delete_select').prop('disabled', !$('.siswa_entry:checked')
                .length); //initially disable/enable button based on checked length
            $('input[type=checkbox]').click(function() {
                if ($('.siswa_entry:checkbox:checked').length > 0) {
                    $('#delete_select').prop('disabled', false);
                } else {
                    $('#delete_select').prop('disabled', true);
                }
            });

            $(document).on('click', '#delete_select', function() {
                var id_alumni = [];
                $('input[name="alumni[]"]:checked').each(function() {
                    id_alumni.push($(this).val());
                });
                console.log(id_alumni);
                if (id_alumni.length > 0) {
                    var confirmdelete = confirm("Apa kamu yakin ingin menghapus Alumni?");
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('delete_multiple-alumni') }}",
                            type: 'post',
                            data: {
                                id_alumni
                            },
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    location.reload();
                                }
                                noti(data.icon, data.message);

                            }
                        });
                    }
                }
            });
        });

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }

        function getYear(value) {
            var yearsend = parseInt(value) + 1;
            $("#tahun_ajaran2").val(yearsend);
        }
    </script>
@endsection
