@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="data-tabel">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Rombel</th>
                                        <th>Nama</th>
                                        <th>Gambar</th>
                                        <th>NIP</th>
                                        <th>Telepon</th>
                                        <th>Email</th>
                                        <th>Password Reset</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_wali_kelas">
                                <div class="form-group row" id="gurus">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Guru</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="id_guru" id="id_guru" class="form-control" required>
                                                <option value="">Pilih Guru</option>
                                                @foreach ($guru as $gr)
                                                    <option value="{{ $gr['id'] }}">{{ $gr['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Data mempunyai Jurusan?</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <div id="radioBtn" class="btn-group">
                                                <a class="btn btn-primary btn-sm active" onclick="showJurusan(1)"
                                                    data-toggle="pilihan_check" data-title="Y">YES</a>
                                                <a class="btn btn-primary btn-sm notActive" onclick="showJurusan(0)"
                                                    data-toggle="pilihan_check" data-title="N">NO</a>
                                            </div>
                                            <input type="hidden" name="haveJurusan" id="pilihan_check">
                                        </div>
                                    </div>
                                </div>
                                <div class="input_jurusan">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l1">Pilih Jurusan</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <select name="jurusan" id="jurusan" class="form-control">
                                                    <option value="">Pilih Jurusan</option>
                                                    @foreach ($jurusan as $jrs)
                                                        <option
                                                            value="{{ (new \App\Helpers\Help())->encode($jrs['id']) }}">
                                                            {{ $jrs['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                {{-- <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Jurusan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jurusan" id="jurusan" class="form-control">
                                                <option value="">Pilih Jurusan</option>
                                                @foreach ($jurusan as $jrs)
                                                    <option value="{{ $jrs['id'] }}">{{ $jrs['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Tingkat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="kelas" id="kelas" class="form-control" disabled>
                                                <option value="">--Pilih jurusan terlebih dahulu--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Rombel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="rombel" id="rombel" class="form-control" disabled>
                                                <option value="">--Pilih tingakat terlebih dahulu--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $id_tahun_ajar = session('id_tahun_ajar');
                                @endphp
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Tahun Ajar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="tahun" id="tahun" class="form-control" required>
                                                <option value="">Pilih Tahun Ajar</option>
                                                @foreach ($tahun as $th)
                                                    <option value="{{ $th['id'] }}"
                                                        {{ $th['id'] == session('id_tahun_ajar') ? 'selected' : '' }}>
                                                        {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Wali Kelas</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            showJurusan(1);

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-clipboard"></i>',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-wali_kelas') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Wali Kelas");
                $('#kelas').attr('disabled', 'disabled')
                $('#kelas').html('<option value="">--Pilih jurusan terlebih dahulu--</option>');
                $('#rombel').attr('disabled', 'disabled')
                $('#rombel').html('<option value="">--Pilih tingakat terlebih dahulu--</option>');
                $('#ajaxModel').modal('show');
                $('#delete_foto').html('');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-wali_kelas') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-wali_kelas') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "wali-kelas/edit",
                    data: {
                        id_wali_kelas: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Wali Kelas");
                        $('#saveBtn').val("edit-user");
                        $('#id_wali_kelas').val(data.id);
                        $('#tahun').val(data.id_tahun_ajaran).trigger("change");
                        $('#jurusan').val(data.id_jurusan).trigger("change");
                        $('#rombel').val(data.id_rombel).trigger("change");
                        // document.getElementById( 'gurus' ).style.display = 'none';
                        edit_kelas(data.id_jurusan, data.id_kelas);
                        edit_rombel(data.id_kelas, data.id_kelas);
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $('select[name="jurusan"]').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $('select[name="kelas"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        success: function(data) {
                            var s = '<option value="">--Pilih Kelas--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama +
                                    '</option>';

                            })
                            $('select[name="kelas"]').removeAttr('disabled')
                            $('select[name="kelas"]').html(s)
                            $('select[name="rombel"]').html(
                                '<option value="">--Pilih tingkat terlebih dahulu--</option>'
                                )
                            $('select[name="rombel"]').attr('disabled', 'disabled')
                        }
                    });
                }
            })

            $('select[name="kelas"]').on('change', function() {
                var id_kelas = $(this).val();
                if (id_kelas) {
                    $('select[name="rombel"]').attr('disabled', 'disabled')
                    $.ajax({
                        url: "{{ route('get-rombel_kelas') }}",
                        type: "POST",
                        data: {
                            id_kelas: id_kelas
                        },
                        beforeSend: function() {
                            $('#rombel').html('<option value="">--Load data Rombel--</option>');
                        },
                        success: function(data) {
                            // console.log(data);
                            var s = '<option value="">--Pilih Rombel--</option>';
                            data = JSON.parse(data);
                            data.forEach(function(val) {
                                s += '<option value="' + val.id + '">' + val.nama +
                                    '</option>';
                            })
                            $('#rombel').removeAttr('disabled')
                            $('#rombel').html(s);
                        }
                    });
                }
            })

        })

        function showJurusan(id) {
            if (id == 1) {
                $('.input_jurusan').show();
                $('select[name="kelas"]').attr('disabled', true)
                $('select[name="rombel"]').attr('disabled', true)
                $('select[name="kelas"]').html('<option>-- Pilih Jurusan terlebih dahulu --</option>');
                $('a[data-toggle="pilihan_check"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('select[name="id_kelas"]').attr('disabled', false)
                $('.input_jurusan').hide();
                $('#id_jurusan').val('').change();
                $('a[data-toggle="pilihan_check"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                changeJurusan();
            }
        }

        function changeJurusan() {
            $.ajax({
                url: "{{ route('get_kelas-jurusan_kosong') }}",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    if (!$.trim(data)) {
                        $('select[name="id_kelas"]').html(
                            '<option value="">--- No Kelas Found ---</option>');
                    } else {
                        var s = '<option value="">--Pilih Kelas--</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama_romawi +
                                '</option>';

                        })
                        $('select[name="kelas"]').removeAttr('disabled');
                    }
                    $('select[name="kelas"]').html(s)
                }
            });
        }




        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "wali-kelas/delete",
                    type: "POST",
                    data: {
                        id_wali_kelas: id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }



        function edit_kelas(id_jurusan, id_kelas) {
            $.ajax({
                url: "{{ route('edit_data-jurusan') }}",
                type: "POST",
                data: {
                    id_jurusan: id_jurusan,
                    id_kelas: id_kelas
                },
                beforeSend: function() {
                    $('select[name="kelas"]').append(
                        '<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="kelas"]').html('');
                    $('select[name="kelas"]').html(fb);
                }
            });
            return false;
        }

        function edit_rombel(id_kelas, rombel) {
            $.ajax({
                url: "{{ route('load_rombel-kelas_select') }}",
                type: "POST",
                data: {
                    id_kelas: id_kelas,
                    rombel: rombel
                },
                beforeSend: function() {
                    $('select[name="rombel"]').append(
                        '<option value="">--- No Rombel Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="rombel"]').html('');
                    $('select[name="rombel"]').html(fb);
                    $('#rombel').removeAttr('disabled')
                }
            });
            return false;

        }

        $('#data_trash').click(function() {
            table_trash.ajax.reload().draw();
            $('#modalTrash').modal('show');
        });

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/user/wali-kelas/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/user/wali-kelas/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
