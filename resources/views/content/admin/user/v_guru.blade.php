@extends('content.admin.user.v_data_user')
@section('content_user')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover responsive nowrap" id="data-tabel"
                                style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Gambar</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Telepon</th>
                                        <th>Email</th>
                                        <th>Password Reset</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_guru">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nik" id="nik" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NUPTK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nuptk" id="nuptk" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIP</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nip" id="nip" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tahun Masuk</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tahun_masuk" id="tahun_masuk" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Informasi</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="informasi_lain" id="informasi_lain" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash User Guru</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalDetailGuru" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">DETAIL GURU</h5>
                </div>
                <div style="width: 100%;">
                    <div class="team-single">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="font-size38 sm-font-size32 xs-font-size30 text-capitalize text-info"
                                    id="dt_nama_atas">
                                    Buckle Giarza</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 xs-margin-30px-bottom"
                                style="display: flex; align-items: center; justify-content: center;">
                                <div class="team-single-img my-2">
                                    <img id="gambar_guru" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt=""
                                        width="200">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="team-single-text padding-50px-left sm-no-padding-left">

                                    <div class="bg-info p-1">
                                        <h5 class="font-size24 text-white m-1 sm-font-size22 xs-font-size20">Detail
                                            Informasi</h5>
                                    </div>
                                    <div class="contact-info-section margin-40px-tb border border-info p-2 mb-3">
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Nama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIK:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nik">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NIP:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nip">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">NUPTK:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_nuptk">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Agama:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_agama">Master's Degrees</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Alamat.:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_alamat">4 Year in Education</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Email:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_email">Design Category</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left text-info">Jenis Kelamin:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_jenkel">Regina ST, London, SK.</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong
                                                    class="margin-10px-left xs-margin-four-left text-info">Telephone:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_telepon">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Tempat,
                                                    Tanggal Lahir:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="ttl"><a href="javascript:void(0)">addyour@emailhere</a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Tahun
                                                    Masuk:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_tahun_masuk">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 col-5">
                                                <strong class="margin-10px-left xs-margin-four-left text-info">Informasi
                                                    Lain:</strong>
                                            </div>
                                            <div class="col-md-7 col-7">
                                                <p id="dt_informasi_lain">(+44) 123 456 789</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-guru') }}";
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                ...settingTable,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-guru') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'telepon',
                        name: 'telepon'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Guru");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });



            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Guru');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-guru') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-guru') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "guru/edit",
                    data: {
                        id_guru: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Guru");
                        $('#saveBtn').val("edit-user");
                        $('#id_guru').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nip').val(data.nip);
                        $('#nik').val(data.nik);
                        $('#nuptk').val(data.nuptk);
                        $('#tahun_masuk').val(data.tahun_masuk);
                        $('#informasi_lain').val(data.informasi_lain);
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#alamat').val(data.alamat);
                        $('#old_image').val(data.old_image);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#jenkel').val(data.jenkel).trigger('change');
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function detailGuru(id) {
            $.ajax({
                type: 'POST',
                url: "{{ route('edit-guru') }}",
                data: {
                    id_guru: id
                },
                beforeSend: function() {
                    $(".dt-" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".dt-" + id).html('<i class="fa fa-info-circle"></i> Detail');
                    $('#dt_nama_atas').html(data.nama == null ? '-' : data.nama);
                    $('#dt_nik').html(data.nik == null ? '-' : data.nik);
                    $('#dt_nip').html(data.nip == null ? '-' : data.nip);
                    $('#dt_nuptk').html(data.nuptk == null ? '-' : data.nuptk);
                    $('#dt_jenkel').html(data.jenkel == null ? '-' : data.jenkel);
                    $('#dt_telepon').html(data.telepon == null ? '-' : data.telepon);
                    $('#dt_nama').html(data.nama == null ? '-' : data.nama);
                    $('#dt_agama').html(data.agama == null ? '-' : data.agama);
                    $('#dt_alamat').html(data.alamat == null ? '-' : data.alamat);
                    $('#dt_email').html(data.email == null ? '-' : data.email);
                    $('#ttl').html(data.tempat_lahir + ", " + data.tgl_lahir);
                    $('#dt_tahun_masuk').html(data.tahun_masuk == null ? '-' : data.tahun_masuk);
                    $('#dt_informasi_lain').html(data.informasi_lain == null ? '-' : data.informasi_lain);
                    $('#gambar_guru').attr('src', data.file);
                    $('#modalDetailGuru').modal('show');
                }
            });

        }



        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('delete-guru') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/user/guru/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/user/guru/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
