@extends('template/template_default/app')
@section('content')
    @if (Session::has('error_api'))
        <script>
            swal({
                title: 'Gagal!',
                text: "{{ session('error_api')['message'] }}",
                timer: 5000,
                type: "{{ session('error_api')['icon'] }}"
            }).then((value) => {
                //location.reload();
            }).catch(swal.noop);

        </script>
    @endif
    <div class="row">
        <div class="col-lg-12">
            @yield('content_user')
        </div>

    </div>

@endsection
