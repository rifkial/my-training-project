@extends('template/template_default/app')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <hr>
                    <form action="javascript:void(0)" id="cari_rombel">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Jurusan</label>
                                    <select name="id_jurusan" class="form-control" id="id_jurusan">
                                        <option value="">Pilih Jurusan..</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">
                                                {{ $jr['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Kelas</label>
                                    <select name="id_kelas" class="form-control" id="id_kelas" disabled>
                                        <option value="">Pilih Kelas..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">&nbsp; &nbsp;</label>
                                    <button type="submit" class="btn btn-success btn-block" id="btn_cari">Pencarian</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered mr-b-0 hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">Nomer</th>
                                    <th class="text-center">Jurusan</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Rombel</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_rombel">
                                <tr>
                                    <td colspan="5" class="text-center">Harap lakukan pencarian terlebih dulu</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_room">
                                <input type="hidden" name="id_rombel" id="id_rombel">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Mapel</label>
                                    <div class="col-sm-12">
                                        <select name="id_mapel" id="id_mapel" class="form-control">
                                            <option value="">Pilih Mapel..</option>
                                            @foreach ($mapel as $item)
                                                <option value="{{ $item['id'] }}">
                                                    {{ $item['kode_mapel'] . ' | ' . $item['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Guru</label>
                                    <div class="col-sm-12">
                                        <select name="id_guru" id="id_guru" class="form-control" disabled>
                                            <option value="">Pilih Guru..</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih File</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <input type="hidden" name="hidden_image" id="hidden_image">
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-sm-3">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group" width="100%" style="margin-top: 10px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('select[name="id_jurusan"]').on('change', function() {
                let id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('get_kelas-jurusan') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $('#id_kelas').html(
                                '<option value="">Memproses kelas...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('select[name="id_kelas"]').html(
                                    '<option value="">Tidak ada Kelas</option>');
                            } else {
                                var s = '<option value="">Pilih Kelas..</option>';
                                data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .nama_romawi +
                                        '</option>';

                                })
                                $('select[name="id_kelas"]').removeAttr('disabled');
                            }
                            $('select[name="id_kelas"]').html(s)
                        }
                    });
                }
            })

            $('select[name="id_mapel"]').on('change', function() {
                let id_mapel = $(this).val();
                if (id_mapel) {
                    $.ajax({
                        url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                        type: "POST",
                        data: {
                            id_mapel
                        },
                        beforeSend: function() {
                            $('#id_guru').html(
                                '<option value="">Memproses Pengajar...</option>');
                        },
                        success: function(data) {
                            if (!$.trim(data)) {
                                $('#id_guru').html(
                                    '<option value="">Guru untuk mapel ini belum diset</option>'
                                );
                            } else {
                                var s = '<option value="">Pilih Guru..</option>';
                                // data = JSON.parse(data);
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id_guru + '">' + row
                                        .guru +
                                        '</option>';

                                })
                                $('#id_guru').removeAttr('disabled');
                            }
                            $('#id_guru').html(s)
                        }
                    });
                }
            })

            $('#cari_rombel').on('submit', function(event) {
                event.preventDefault();
                let id_kelas = $('#id_kelas').val();
                $("#btn_cari").html(
                    '<i class="fa fa-spin fa-spinner"></i> Proses mencari');
                $("#btn_cari").attr("disabled", true);
                if (id_kelas) {
                    $.ajax({
                        url: "{{ route('admin_room-get_by_kelas') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            $('#data_rombel').html(data)
                            $('#btn_cari').html('Pencarian');
                            $("#btn_cari").attr("disabled", false);
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Simpan');
                        }
                    });
                } else {
                    $('#btn_cari').html('Pencarian');
                    $("#btn_cari").attr("disabled", false);
                    alert('Harap pilih kelas terlebih dulu');
                }

            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('admin_learning-store_room') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('admin_learning-update_room') }}";
                }
                let id_rombel = $('#id_rombel').val();

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data_room' + id_rombel).html(data.room);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let id_rombel = $(this).data('rombel');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin_learning-edit_room') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Room");
                        $('#saveBtn').val("edit-user");
                        $('#id_room').val(data.id);
                        $('#id_rombel').val(data.id_rombel);
                        $('#id_mapel').val(data.id_mapel);
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            if (data.file_edit != 'study.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        edit_mapel(data.id_guru, data.id_mapel);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let id_rombel = $(this).data('rombel');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('admin_learning-delete') }}",
                        type: "POST",
                        data: {
                            id,
                            id_rombel
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_room' + id_rombel).html(data.room);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.open_key', function() {
                let id = $(this).data('id');
                let isi = $(this).data('value');
                let id_rombel = $(this).data('rombel');
                let loader = $(this);
                let notif_key = "ingin membuka room ini!";
                let hasil = 1;
                if (isi == 1) {
                    notif_key = "ingin menutup room ini!";
                    hasil = 2
                }
                swal({
                    title: "Apa kamu yakin?",
                    text: notif_key,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('admin_room-update_status') }}",
                        type: "POST",
                        data: {
                            id,
                            hasil,
                            id_rombel
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_room' + id_rombel).html(data.room);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function tambahRoom(id_rombel) {
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah Room");
            $('#id_rombel').val(id_rombel);
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
            $('#delete_foto').html('');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        }

        function edit_mapel(id_guru, id_mapel) {
            $.ajax({
                url: "{{ route('get_by_mapel-guru_pelajaran_load_select') }}",
                type: "POST",
                data: {
                    id_guru,
                    id_mapel
                },
                beforeSend: function() {
                    $('#id_guru').html('<option value="">Memproses Guru</option>');
                },
                success: function(fb) {
                    $('#id_guru').html('');
                    $('#id_guru').html(fb);
                    $("#id_guru").attr("disabled", false);
                }
            });
            return false;
        }
    </script>
@endsection
