@extends('template/template_default/app')
@section('content')
    <style>
        .main-box-layout {
            margin: 0px;
            margin-top: 30px;
            position: relative;
            box-shadow: -3px 3px 3px 0px #c1c1c1;
        }

        .main-box-layout:hover .box-icon-section i {
            font-size: 50px;
            transform: rotate(360deg);
            transition: 1s;
        }

        .box-icon-section {
            display: table;
            height: 100px;
            color: #fff;
        }

        .box-icon-section i {
            font-size: 30px;
            display: table-cell;
            vertical-align: middle;
            transition: transform 0.4s ease-in-out;
            transition: 1s;
        }

        .box-text-section {
            background-color: #c3c3c3;
        }

        .box-text-section p {
            margin: 0px;
            color: #fff;
            padding: 10px 0px;
        }

        .label .badge {
            position: absolute;
            font-size: 11px;
            top: -19px;
            left: 50%;
            transform: translateX(-50%);
            background-color: #f1f1f1;
            color: #fff;
            box-shadow: 0px 0px 3px 0px #fff;
            border: 3px solid #fff;
        }

        .label {
            padding: 0
        }

    </style>
    <div class="row justify-content-md-center">
        @foreach ($rombel as $rmb)
            @php
                $warna = ['secondary', 'success', 'info', 'danger', 'warning', 'primary'];
            @endphp
            <div class="col-lg-4 col-sm-4 col-12 text-center">
                <a href="{{ route('multiple_rombel', $rmb['id_code']) }}">
                    <div class="row main-box-layout img-thumbnail">
                        <div class="col-lg-12 col-sm-12 col-12 box-icon-section bg-{{ $warna[array_rand($warna, 1)] }}">
                            <i class="fa fa-users" aria-hidden="true"> {{ $rmb['nama'] }}</i>
                        </div>
                        <div class="col-lg-12 col-sm-12 col-12 box-text-section">
                            <p>{{ $rmb['kelas_romawi'] }}</p>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endsection
