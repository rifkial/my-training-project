@extends('content.admin.master.v_data_master')
@section('content_master')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                        <th>#</th>
                        <th>Rombel</th>
                        <th>Kelas</th>
                        <th>Jurusan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_rombel">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Punya Jurusan?</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <div id="radioBtn" class="btn-group">
                                                <a class="btn btn-primary btn-sm active" onclick="showJurusan(1)"
                                                    data-toggle="pilihan_check" data-title="Y">YES</a>
                                                <a class="btn btn-primary btn-sm notActive" onclick="showJurusan(0)"
                                                    data-toggle="pilihan_check" data-title="N">NO</a>
                                            </div>
                                            <input type="hidden" name="haveJurusan" id="pilihan_check">
                                        </div>
                                    </div>
                                </div>
                                <div class="input_jurusan">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l1">Pilih Jurusan</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <select name="id_jurusan" id="id_jurusan" class="form-control">
                                                    <option value="">---Pilih Jurusan---</option>
                                                    @foreach ($jurusan as $item)
                                                        <option value="{{ $item['id'] }}">{{ $item['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Kelas</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="id_kelas" id="id_kelas" class="form-control">
                                                <option value="">--Pilih Kelas--</option>
                                                @foreach ($kelas as $kl)
                                                    <option value="{{ $kl['id'] }}">{{ $kl['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-1">
                                    <label class="col-md-3 col-form-label" for="l1">Nama Rombel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama[]" id="nama" autocomplete="off"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="fomAddRombel">

                                </div>
                                <div class="form-group row tambahBaris">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Rombongan Belajar</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Rombel</th>
                                    <th>Kelas</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')


    <script type="text/javascript">
        var url_import = "{{ route('import-rombel') }}";
        showJurusan(1);

        function showJurusan(id) {
            if (id == 1) {
                $('.input_jurusan').show();
                $('select[name="id_kelas"]').attr('disabled', true)
                $('select[name="id_kelas"]').html('Pilih Jurusan terlebih dahulu');
                $('a[data-toggle="pilihan_check"]').not('[data-title="N"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('select[name="id_kelas"]').attr('disabled', false)
                $('.input_jurusan').hide();
                $('#id_jurusan').val('').change();
                $('a[data-toggle="pilihan_check"]').not('[data-title="Y"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_check"][data-title="Y"]')
                    .removeClass('active').addClass('notActive');
                changeJurusan();
            }
        }
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        text: '<i class="fa fa-upload"></i>',
                        className: 'btn btn-sm',
                        attr: {
                            title: 'Import Data',
                            id: 'import'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    {
                        text: '<i class="fa fa-trash"></i>',
                        attr: {
                            title: 'removes',
                            id: 'action_remove'
                        }
                    },
                    'colvis',
                ],
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                columns: [{
                        data: 'checkbox',
                        name: 'checkbox'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'kelas_romawi',
                        name: 'kelas_romawi'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#example-select-all').on('click', function() {
                var rows = table.rows({
                    'search': 'applied'
                }).nodes();
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });


            $('#example tbody').on('change', 'input[type="checkbox"]', function() {
                if (!this.checked) {
                    var el = $('#example-select-all').get(0);
                    if (el && el.checked && ('indeterminate' in el)) {
                        el.indeterminate = true;
                    }
                }
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-rombel') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'kelas_romawi',
                        name: 'kelas_romawi'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Rombel');
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Rombel");
                $('select[name="id_kelas"]').val('').trigger('change');
                // $('select[name="id_kelas"]').html('<option value="">Pilih Jurusan dahulu </option>');
                $('.fomAddRombel').html('');
                $('.tambahBaris').show('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-rombel') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-rombel') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        $("#saveBtn").attr("disabled", false);
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('.fomAddRombel').html('');
                $('.tambahBaris').hide('');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "rombongan_belajar/edit",
                    data: {
                        id_rombel: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Rombel");
                        $('#saveBtn').val("edit-user");
                        $("#id_jurusan").val(data.id_jurusan).trigger("change");
                        $('#id_rombel').val(data.id);
                        if (data.id_jurusan == null) {
                            $('select[name="id_kelas"]').attr('disabled', false)
                            $('.input_jurusan').hide();
                            $('#id_jurusan').val('').change();
                            $('a[data-toggle="pilihan_check"]').not('[data-title="Y"]')
                                .removeClass('notActive').addClass(
                                    'active');
                            $('a[data-toggle="pilihan_check"][data-title="Y"]')
                                .removeClass('active').addClass('notActive');
                        } else {
                            $('.input_jurusan').show();
                            $('select[name="id_kelas"]').attr('disabled', true)
                            $('select[name="id_kelas"]').html('Pilih Jurusan terlebih dahulu');
                            $('a[data-toggle="pilihan_check"]').not('[data-title="N"]')
                                .removeClass('notActive').addClass(
                                    'active');
                            $('a[data-toggle="pilihan_check"][data-title="N"]')
                                .removeClass('active').addClass('notActive');
                        }
                        $('#nama').val(data.nama);
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        edit_kelas(data.id_jurusan, data.id_kelas);
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then((willDelete) => {
                $.ajax({
                    url: "rombongan_belajar/trash/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function changeJurusan() {
            $.ajax({
                url: "kelas/habis/semua/kosong",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    // console.log(data);
                    if (!$.trim(data)) {
                        $('select[name="id_kelas"]').html(
                            '<option value="">--- No Kelas Found ---</option>');
                    } else {
                        var s = '<option value="">--Pilih Kelas--</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama_romawi +
                                '</option>';

                        })
                        $('select[name="id_kelas"]').removeAttr('disabled');
                    }
                    $('select[name="id_kelas"]').html(s)
                }
            });
        }
        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            if (id_jurusan) {
                $.ajax({
                    url: "{{ route('get_kelas-jurusan') }}",
                    type: "POST",
                    data: {
                        id_jurusan
                    },
                    success: function(data) {
                        if (!$.trim(data)) {
                            $('select[name="id_kelas"]').html(
                                '<option value="">--- No Kelas Found ---</option>');
                        } else {
                            var s = '';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row.nama_romawi +
                                    '</option>';

                            })
                            $('select[name="id_kelas"]').removeAttr('disabled');
                        }


                        $('select[name="id_kelas"]').html(s)
                    }
                });
            }
        })

        function edit_kelas(id_jurusan, id_kelas) {
            $.ajax({
                url: "{{ route('edit_data-jurusan') }}",
                type: "POST",
                data: {
                    id_jurusan: id_jurusan,
                    id_kelas: id_kelas
                },
                beforeSend: function() {
                    $('select[name="id_kelas"]').append('<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="id_kelas"]').html('');
                    $('select[name="id_kelas"]').html(fb);
                }
            });
            return false;
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/master/rombongan_belajar/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/master/rombongan_belajar/delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddRombel').append(
                '<div class="form-group row mb-1 mt-3" id="row' + i +
                '"><label class="col-md-3 col-form-label" for="l1">Nama Rombel</label><div class="col-md-9"><div class="input-group"><input type="text" name="nama[]" id="nama" autocomplete="off" class="form-control"></div> </div> <label class="col-md-3 col-form-label" for="l1"></label> <div class="col-md-9 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        function edit_kelas(id_jurusan, id_kelas) {
            $.ajax({
                url: "{{ route('edit_data-jurusan') }}",
                type: "POST",
                data: {
                    id_jurusan: id_jurusan,
                    id_kelas: id_kelas
                },
                beforeSend: function() {
                    $('select[name="id_kelas"]').append('<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    console.log(fb);
                    $('select[name="id_kelas"]').html('');
                    $('select[name="id_kelas"]').html(fb);
                    $('select[name="id_kelas"]').removeAttr('disabled');
                }
            });
            return false;
        }

        $(document).on('click', '#action_remove', function() {
            if ($('input[name="rombel[]"]:checked').length < 1 ) {
                alert('Rombel dan checkbox harus terisi');
            } else {
                var id_rombel = [];
                $("input:checkbox[class=manual_entry_cb]:checked").each(function() {
                    id_rombel.push($(this).val());
                });
                console.log(id_rombel);
                if (id_rombel.length > 0) {
                    var confirmdelete = confirm("Do you really want move class student?");
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('deletes-rombel') }}",
                            type: 'post',
                            data: {
                                id_rombel,
                            },
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'berhasil') {
                                    $('#data-tabel').dataTable().fnDraw(false);
                                    $('#example-select-all').prop('checked', false);
                                }
                                noti(data.success, data.message);
                                // $('#createNewCustomer').html(
                                //     '<i class="fa fa-plus"></i> Pindah Data');
                                // $("#createNewCustomer").attr("disabled", false);

                            }
                        });
                    }
                }
            }
        });
    </script>
@endsection
