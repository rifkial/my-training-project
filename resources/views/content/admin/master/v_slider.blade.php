@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="addMutasi">
                                <button class="btn btn-info addSlider"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped widget-status-table mr-b-0 hover">
                            <thead>
                                <tr class="bg-info">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Gambar</th>

                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody id="data_slider">
                                @if (empty($slider))
                                    <tr>
                                        <td colspan="5" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($slider as $sd)
                                        <tr>
                                            <td class="vertical-middle text-center">{{ $no++ }}</td>
                                            <td class="vertical-middle text-center"><img src="{{ $sd['file'] }}" alt=""
                                                    height="70"></td>
                                            <td class="vertical-middle text-center">
                                                <a href="javascript:void(0)" class="edit text-info"
                                                    data-id="{{ $sd['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" class="delete text-danger"
                                                    data-id="{{ $sd['id'] }}"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalForm" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formSlider" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_slider">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Gambar Slider</label>
                                    <div class="col-sm-4">
                                        <img id="modal-preview" src="https://via.placeholder.com/200x100" alt="Preview"
                                            class="my-1 w-100">
                                    </div>
                                    <div class="col-sm-12">

                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <p class="my-0">
                                            <small class="text-danger">*disarankan ukuran 1349 x 377 px</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.addSlider').click(function() {
                $('#formSlider').trigger("reset");
                $('#modelHeading').html("Tambah Slider");
                $('#modal-preview').attr('src', 'https://via.placeholder.com/200x100');
                $('#modalForm').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#formSlider', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('master_slider-create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('master_slider-update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formSlider').trigger("reset");
                            $('#modalForm').modal('hide');
                            $('#data_slider').html(data.data_slide);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });



            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('master_slider-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Slider");
                        $('#id_slider').val(data.id);
                        $('#modal-preview').attr('src', data.file);
                        $('#action').val('Edit');
                        $('#modalForm').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('master_slider-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_slider').html(data.data_slide);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>

@endsection
