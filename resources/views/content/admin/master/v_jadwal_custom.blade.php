@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .btn-xs {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        thead {
            background: #2471d2;
            color: #fff;
        }

        #loading {
            background: url("{{ asset('asset/img/load.gif') }}") no-repeat center;
            height: 300px;
            width: 300px;
            display: block;
            margin: auto;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            text-align: center;
            top: 66px;
        }

    </style>
    <div class="row widget-bg">
        <div class="col-md-8">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
        </div>
        <hr>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <select name="id_jurusan" id="id_jurusan" class="form-control">
                                        <option value="0">--- Tidak Punya Jurusan ---</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{ $jr['id'] }}">{{ $jr['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <select name="id_kelas" id="id_kelas" class="form-control">
                                        <option value="">--- Pilih Kelas ---</option>
                                        @foreach ($kelas as $kl)
                                            <option value="{{ $kl['id'] }}">{{ $kl['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select name="rombel" id="rombel" class="form-control" disabled>
                                        <option value="">--- Pilih Rombel ---</option>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <a href="javascript:void(0)" class="btn btn-outline-success btn-block"
                                        id="createNewCustomer"><i class="fa fa-plus"></i> Tambah Jadwal</a>
                                </div>
                                <div class="col-lg-2">
                                    <a href="javascript:void(0)" class="btn btn-outline-primary btn-block" id="import"><i
                                            class="fa fa-upload"></i> Import</a>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-lg-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input value="{{ $search }}" id="search" name="search" type="text"
                                            class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <select id="status" name="status" class="form-control">
                                        @foreach ($statuses as $st)
                                            @if ($st['value'] == $status)
                                                <option selected value="{{ $st['value'] }}">{{ $st['label'] }}
                                                </option>
                                            @else
                                                <option value="{{ $st['value'] }}">{{ $st['label'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <select id="sort" name="sort" class="form-control">
                                        @foreach ($sorts as $s)
                                            @if ($s == $sort)
                                                <option selected value="{{ $s }}">{{ $s }}
                                                </option>
                                            @else
                                                <option value="{{ $s }}">{{ $s }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <select id="per_page" name="per_page" class="form-control">
                                        @foreach ($pages as $pg)
                                            @if ($pg == $perPage)
                                                <option selected value="{{ $pg }}">{{ $pg }}
                                                </option>
                                            @else
                                                <option value="{{ $pg }}">{{ $pg }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <a href="javascript:void(0)" id="fil" onclick="search_jadwal('{{ $routes }}')"
                                        class="btn btn-outline-info btn-block">Display</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row mt-2" id="fresh_table">
                        {{-- <div id="loading"></div> --}}
                        <div class="col-md-12" id="data-tabel">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Hari</th>
                                        <th>Jam</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Guru</th>
                                        <th>Rombel</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @if (empty($data))
                                        <tr>
                                            <td colspan="8" style="text-align: center">Data saat ini kosong</td>
                                        </tr>
                                    @else
                                        @foreach ($data as $item)
                                            @php
                                                $status = 'Aktif';
                                                
                                                $color = 'badge badge-success text-inverse';
                                                if ($item['status'] == '2') {
                                                    $status = 'Tidak Aktif';
                                                    $color = 'badge badge-danger text-inverse';
                                                }
                                            @endphp
                                            <tr>
                                                <td><span class="{{ $color }}">{{ $status }}</span></td>
                                                <td>{{ $item['hari'] }}</td>
                                                <td>{{ $item['jam'] }}</td>
                                                <td>{{ $item['nama_mapel'] }}</td>
                                                <td>{{ $item['nama_guru'] }}</td>
                                                <td>{{ $item['nama_rombel'] }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                                        data-id="{{ $item['id'] }}" data-original-title="Edit"
                                                        class="edit btn btn-info btn-sm edit"><i
                                                            class="fa fa-pencil-square-o"></i> Edit</a>
                                                    <button type="button" name="delete" data-id="{{ $item['id'] }}"
                                                        class="btn btn-danger btn-sm"
                                                        onclick="deleteData({{ $item['id'] }})"><i
                                                            class="fa fa-trash"></i> Delete</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                            {!! $pagination !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_jadwal">
                                {{-- <input type="text" name="id_rombel" id="id_rombel"> --}}
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Mapel</label>
                                    <select name="id_mapel" id="id_mapel" class="form-control">
                                        <option value="">-- Pilih Mapel --</option>
                                        @foreach ($mapel as $mpl)
                                            <option value="{{ $mpl['id'] }}">{{ $mpl['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Guru</label>
                                    <select name="id_guru" id="id_guru" class="form-control" disabled>
                                        <option value="">--- Pilih Guru ---</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jam</label>
                                    <input type="text" name="jam" id="jam" class="form-control">
                                    <small>*contoh 08.00 - 09.00</small>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Hari</label>
                                    <select name="hari" id="hari" class="form-control">
                                        <option value="senin">Senin</option>
                                        <option value="selasa">Selasa</option>
                                        <option value="rabu">Rabu</option>
                                        <option value="kamis">Kamis</option>
                                        <option value="jumat">Jumat</option>
                                        <option value="sabtu">Sabtu</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <select name="status" id="status_aktif" class="form-control">
                                        <option value="1">Aktif</option>
                                        <option value="2">Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Jadwal Pelajaran</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hari</th>
                                    <th>Jam</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Guru</th>
                                    <th>Ruang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')
    <script type="text/javascript">
        var url_import = "{{ route('import-jadwal') }}";
        var rombels = 0;



        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#createNewCustomer').click(function() {
                var isi_rombel = $('#rombel').val();
                if (isi_rombel == '') {
                    alert("Mohon maaf, proses bisa dilakukan jika anda telah memilih rombel dahulu");
                } else {
                    $('#saveBtn').val("create-Customer");
                    $('#Customer_id').val('');
                    $('#CustomerForm').trigger("reset");
                    $('#modelHeading').html("Tambah Data Jadwal");
                    $('#ajaxModel').modal('show');
                    $('#action').val('Add');
                }
            });

            $(document).on('click', '#createNewCustomer', function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Jadwal");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            })

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Jadwal');
            });

            $('#CustomerForm').on('submit', function(event) {

                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-jadwal') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-jadwal') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize() + '&id_rombel=' + rombels,
                    dataType: "json",
                    beforeSend: function() {
                        $("#fresh_table").html('<div id="loading"></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $("#fresh_table").load(" #data-tabel");
                        }
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.success);
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "jadwal_pelajaran/edit",
                    data: {
                        id_jadwal: id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Jadwal Sekolah");
                        $('#saveBtn').val("edit-user");
                        $("#id_jadwal").val(data.id);
                        $("#jurusan").val(data.id_jurusan).trigger("change");
                        $('#ruang').val(data.ruang);
                        $('#status_aktif').val(data.status).trigger('change');
                        $('#jam').val(data.jam);
                        $('#hari').val(data.hari).trigger("change");
                        $('#id_mapel').val(data.id_mapel).trigger("change");
                        edit_guru(data.id_mapel, data.id_guru);
                        $('#action_button').val('Edit');
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
        });

        function deleteData(id) {
            var loaders = $(this);
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then((willDelete) => {
                $.ajax({
                    url: "{{ route('soft_delete-jadwal') }}",
                    type: "POST",
                    data: {
                        id
                    },

                    beforeSend: function() {
                        $(loaders).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $("#fresh_table").load(" #data-tabel");
                        }
                        swa(data.status + "!", data.message, data.success);
                        $("#fresh_table").load(" #data-tabel");
                        $(loader).html(
                            '<i class="fa fa-trash"></i> Delete');
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            if (id_jurusan) {
                $('select[name="id_kelas"]').attr("disabled", true);
                $('#rombel').html('<option value="">--Pilih Rombel--</option>');
                $('#rombel').attr("disabled", true);
                var action_url = '';

                if (id_jurusan != 0) {
                    action_url = "{{ route('get-jurusan') }}";
                    method_url = "POST";
                }

                if (id_jurusan == 0) {
                    action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                    method_url = "GET";
                }
                $.ajax({
                    url: action_url,
                    type: method_url,
                    data: {
                        id_jurusan
                    },
                    success: function(data) {
                        var s = '<option value="">---Pilih Kelas---</option>';
                        data = JSON.parse(data);
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama + '</option>';

                        })
                        $('select[name="id_kelas"]').attr("disabled", false);
                        $('select[name="id_kelas"]').html(s)

                    }
                });
            } else {
                $('select[name="id_kelas"]').attr("disabled", true);
                $('#rombel').html('<option value="">--Pilih Rombel--</option>');
                $('#rombel').attr("disabled", true);
                rombels = 0;
                table.ajax.reload().draw();
            }
        })




        $('select[name="id_kelas"]').on('change', function() {
            var id_kelas = $(this).val();
            if (id_kelas) {
                $('#rombel').html('<option value="">--Pilih Rombel--</option>');
                $('#rombel').attr("disabled", true);
                $.ajax({
                    url: "{{ route('get-rombel_kelas') }}",
                    type: "POST",
                    data: {
                        id_kelas: id_kelas
                    },
                    beforeSend: function() {
                        $('#rombel').html('<option value="">--Load data Rombel--</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">--Pilih Rombel--</option>';
                        data = JSON.parse(data);
                        data.forEach(function(val) {
                            s += '<option value="' + val.id + '">' + val.nama + '</option>';
                        })
                        $('#rombel').attr("disabled", false);
                        $('#rombel').html(s);
                    }
                });
            } else {
                $('#rombel').html('<option value="">--Pilih Rombel--</option>');
                $('#rombel').attr("disabled", true);
                rombels = 0;
                table.ajax.reload().draw();
            }
        })

        // $('#rombel').on('change', function(e) {
        //     rombels = $(this).find('option:selected').val();
        //     table.ajax.reload().draw();
        // });


        $('select[name="id_mapel"]').on('change', function() {
            var id_mapel = $(this).val();
            if (id_mapel) {
                $('select[name="id_guru"]').attr('disabled', 'disabled')
                $.ajax({
                    url: "{{ route('get_by_mapel-guru_pelajaran') }}",
                    type: "POST",
                    data: {
                        id_mapel: id_mapel
                    },
                    beforeSend: function() {
                        $('#id_guru').html(
                            '<option value="">--Load data Guru Pelajaran--</option>');
                    },
                    success: function(data) {
                        var s = '<option value="">---Pilih Guru---</option>';
                        data.forEach(function(row) {
                            s += '<option value="' + row.id_guru + '">' + row.guru +
                                '</option>';

                        })
                        $('select[name="id_guru"]').removeAttr('disabled')
                        $('select[name="id_guru"]').html(s);
                    }
                });
            }
        })


        function edit_guru(id_mapel, id_guru) {
            $.ajax({
                url: "{{ route('get_by_mapel-guru_pelajaran_load_select') }}",
                type: "POST",
                data: {
                    id_mapel: id_mapel,
                    id_guru: id_guru
                },
                beforeSend: function() {
                    $('select[name="id_guru"]').append('<option value="">--- No Guru Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="id_guru"]').html('');
                    $('select[name="id_guru"]').html(fb);
                    $('select[name="id_guru"]').removeAttr('disabled')
                }
            });
            return false;
        }



        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/master/jadwal_pelajaran/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/master/jadwal_pelajaran/delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        function template() {
            window.location.href = "{{ $url }}";
        }

        function search_jadwal(routes) {
            // console.log(routes);
            var search = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var status = (document.getElementById("status") != null) ? document.getElementById("status").value : "";
            var sort = (document.getElementById("sort") != null) ? document.getElementById("sort").value : "";
            var per_page = (document.getElementById("per_page") != null) ? document.getElementById("per_page").value : "10";
            var id_jurusan = (document.getElementById("id_jurusan") != null) ? document.getElementById("id_jurusan").value :
                "0";
            var id_kelas = (document.getElementById("id_kelas") != null) ? document.getElementById("id_kelas").value : "0";
            var rombel = (document.getElementById("rombel") != null) ? document.getElementById("rombel").value : "0";

            var url = window.location.href + "?search=" + search + "&status=" + status + "&sort=" + sort + "&per_page=" +
                per_page + "&jurusan=" + id_jurusan + "&kelas=" + id_kelas + "&rombel=" + rombel;
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search + "&status=" + status + "&sort=" + sort + "&per_page=" +
                per_page + "&jurusan=" + id_jurusan + "&kelas=" + id_kelas + "&rombel=" + rombel;
            document.location = url;
        }
    </script>
@endsection
