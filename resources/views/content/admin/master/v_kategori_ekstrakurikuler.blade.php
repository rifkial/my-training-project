@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mb-2">
                        <div class="col-md-12 col-12">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        </div>
                        <hr>
                        <div class="col-md-12 col-12">
                            <div class="">
                                <button id="addData" class="btn btn-outline-info mt-1"><i class="fas fa-plus-circle"></i>
                                    Tambah</button>
                                <button id="importKategori" class="btn btn-outline-purple mt-1"><i
                                        class="fas fa-file-export"></i>
                                    Import Kategori</button>
                                <button id="importEkstra" class="btn btn-outline-success mt-1 pull-right"><i
                                        class="fas fa-file-export"></i>
                                    Import Ekstra</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_kategori">
                                    @if (!empty($kategori))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($kategori as $kt)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $kt['nama'] }}</td>
                                                <td class="text-center">
                                                    <a href="javaript:void(0)" data-toggle="collapse"
                                                        data-target="#ekstra{{ $kt['id'] }}"
                                                        class="text-purple accordion-toggle"><i
                                                            class="fas fa-info-circle"></i></a>
                                                    <a href="javascript:void(0)" class="edit text-info"
                                                        data-id="{{ $kt['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" class="delete text-danger"
                                                        data-id="{{ $kt['id'] }}"><i class="fas fa-trash-alt"></i></a>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="ekstra{{ $kt['id'] }}">
                                                        <button class="btn btn-purple btn-sm my-3 pull-right"
                                                            onclick="tambahEkstra({{ $kt['id'] }})"><i
                                                                class="fas fa-plus-circle"></i>
                                                            Tambah Ekstra</button>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="bg-purple text-white">
                                                                    <th class="text-center">No</th>
                                                                    <th class="text-center">Ekstrakurikuler</th>
                                                                    <th class="text-center">Pembimbing</th>
                                                                    <th class="text-center">File Absensi</th>
                                                                    <th class="text-center">File Jurnal</th>
                                                                    <th class="text-center">File Nilai</th>
                                                                    <th class="text-center">File Dokumentasi</th>
                                                                    <th class="text-center">Opsi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="dataEkstra{{ $kt['id'] }}">
                                                                @if (!empty($kt['ekstra']))
                                                                    @php
                                                                        $nomer = 1;
                                                                    @endphp
                                                                    @foreach ($kt['ekstra'] as $ekt)
                                                                        <tr>
                                                                            <td>{{ $nomer++ }}</td>
                                                                            <td>{{ $ekt['nama'] }}</td>
                                                                            <td>{{ $ekt['pembimbing'] }}
                                                                            </td>
                                                                            <td class="text-center">
                                                                                @if (!empty($ekt['absen']))
                                                                                    <a href="{{ route('download-file', ['absen', (new \App\Helpers\Help())->encode($ekt['id'])]) }}"
                                                                                        target="_blank"
                                                                                        class="text-purple"><i
                                                                                            class="fas fa-file-download"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-danger delFile"
                                                                                        data-based="absen"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-trash"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-info edit_file"
                                                                                        data-based="absen"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-exchange-alt"></i></a>
                                                                                @else
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        data-based="absen"
                                                                                        data-kategori="{{ $kt['id'] }}"
                                                                                        class="text-success upload_file"><i
                                                                                            class="fas fa-file-upload"></i></a>
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center">
                                                                                @if (!empty($ekt['jurnal']))
                                                                                    <a href="{{ route('download-file', ['jurnal', (new \App\Helpers\Help())->encode($ekt['id'])]) }}"
                                                                                        target="_blank"
                                                                                        class="text-purple"><i
                                                                                            class="fas fa-file-download"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-danger delFile"
                                                                                        data-based="jurnal"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-trash"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-info edit_file"
                                                                                        data-based="jurnal"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-exchange-alt"></i></a>
                                                                                @else
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        data-based="jurnal"
                                                                                        data-kategori="{{ $kt['id'] }}"
                                                                                        class="text-success upload_file"><i
                                                                                            class="fas fa-file-upload"></i></a>
                                                                                @endif
                                                                            </td>

                                                                            <td class="text-center">
                                                                                @if (!empty($ekt['nilai']))
                                                                                    <a href="{{ route('download-file', ['nilai', (new \App\Helpers\Help())->encode($ekt['id'])]) }}"
                                                                                        target="_blank"
                                                                                        class="text-purple"><i
                                                                                            class="fas fa-file-download"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-danger delFile"
                                                                                        data-based="nilai"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-trash"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-info edit_file"
                                                                                        data-based="nilai"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-exchange-alt"></i></a>
                                                                                @else
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        data-based="nilai"
                                                                                        data-kategori="{{ $kt['id'] }}"
                                                                                        class="text-success upload_file"><i
                                                                                            class="fas fa-file-upload"></i></a>
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center">
                                                                                @if (!empty($ekt['dokumentasi']))
                                                                                    <a href="{{ route('download-file', ['dokumentasi', (new \App\Helpers\Help())->encode($ekt['id'])]) }}"
                                                                                        target="_blank"
                                                                                        class="text-purple"><i
                                                                                            class="fas fa-file-download"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-danger delFile"
                                                                                        data-based="dokumentasi"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-trash"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        class="text-info edit_file"
                                                                                        data-based="dokumentasi"
                                                                                        data-kategori="{{ $kt['id'] }}"><i
                                                                                            class="fas fa-exchange-alt"></i></a>
                                                                                @else
                                                                                    <a href="javascript:void(0)"
                                                                                        data-id="{{ $ekt['id'] }}"
                                                                                        data-based="dokumentasi"
                                                                                        data-kategori="{{ $kt['id'] }}"
                                                                                        class="text-success upload_file"><i
                                                                                            class="fas fa-file-upload"></i></a>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $ekt['id'] }}"
                                                                                    class="btn btn-sm btn-info editEkstra"
                                                                                    data-kategori="{{ $kt['id'] }}"><i
                                                                                        class="fas fa-pencil-alt"></i></a>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $ekt['id'] }}"
                                                                                    class="btn btn-sm btn-danger deleteEkstra"
                                                                                    data-kategori="{{ $kt['id'] }}"><i
                                                                                        class="fas fa-trash"></i></a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @else
                                                                    <tr>
                                                                        <td colspan="8" class="text-center">Data saat ini
                                                                            tidak tersedia</td>
                                                                    </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="3">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kat_ekstra">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Kategori</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="fomAddEkstra">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="actionKategori" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalEkstra" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-purple">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="ekstraTitle"></h5>
                </div>
                <form id="formEkstra" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_ekstra">
                        <input type="hidden" name="id_kategori" id="id_kategori">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">Nama Ekstra</label>
                                    <input type="text" name="ekstra" id="ekstra" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Pilih Pembimbing</label>
                                    <input type="text" name="pembimbing" id="pembimbing" class="form-control">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionEkstra" value="Add" />
                        <button type="submit" class="btn btn-purple btn-rounded ripple text-left" id="btnEkstra">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalFile" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="fileTitle"></h5>
                </div>
                <form id="formFile" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_file">
                        <input type="hidden" name="based" id="based">
                        <input type="hidden" name="id_kategori" id="id_kat_file">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">Pilih File</label>
                                    <input type="file" type="file" name="image" accept="*" class="form-control" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionFile" value="Add" />
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left" id="btnFile">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
                </div>
                <form action="javascript:void(0)" id="formImport" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" style="cursor: pointer"
                                            class="bg-info p-2 rounded"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="javascript:void(0)" target="_blank" class="text-info link_donwload">Download
                                                sample template for import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionImport" value="kategori" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#importKategori').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Kategori');
                $('.link_donwload').attr('href', "{{ $url_kategori }}");
                $('#actionImport').val('kategori');
            });
            
            $('#importEkstra').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Ekstrakurikuler');
                $('.link_donwload').attr('href', "{{ $url_ekstra }}");
                $('#actionImport').val('ekstra');
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var action_url = '';
                if ($('#actionImport').val() == 'kategori') {
                    action_url = "{{ route('import-kategori_ekstra') }}";
                }

                if ($('#actionImport').val() == 'ekstra') {
                    action_url = "{{ route('import-ekstra') }}";
                }
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#importBtn').html('Simpan');
                        }
                            noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });

            $('#addData').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('.fomAddEkstra').html('');
                $('.tambahBaris').show('');
                $('#modelHeading').html("Tambah Kategori Ekstra");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });


            $('#CustomerForm').on('submit', function(event) {
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#actionKategori').val() == 'Add') {
                    action_url = "{{ route('store-kategori_ekstrakurikuler') }}";
                    method_url = "POST";
                }

                if ($('#actionKategori').val() == 'Edit') {
                    action_url = "{{ route('update-kategori_ekstrakurikuler') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data_kategori').html(data.kategori);
                        }
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.message);
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formEkstra').on('submit', function(event) {
                $("#btnEkstra").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnEkstra").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#actionEkstra').val() == 'Add') {
                    action_url = "{{ route('store-ekstrakurikuler') }}";
                    method_url = "POST";
                }

                if ($('#actionEkstra').val() == 'Edit') {
                    action_url = "{{ route('update-ekstrakurikuler') }}";
                    method_url = "PUT";
                }
                let id_kategori = $('#id_kategori').val();

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formEkstra').trigger("reset");
                            $('#modalEkstra').modal('hide');
                            $('#dataEkstra' + id_kategori).html(data.ekstra);
                        }
                        $('#btnEkstra').html('Simpan');
                        noti(data.icon, data.message);
                        $("#btnEkstra").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnEkstra').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-kategori_ekstrakurikuler') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Kategori Ekstra");
                        $('#id_kat_ekstra').val(data.id);
                        $('#nama').val(data.nama);
                        $('.tambahBaris').hide();
                        $('.fomAddEkstra').html('');
                        $('#actionKategori').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $(document).on('click', '.editEkstra', function() {
                let id = $(this).data('id');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('edit-ekstrakurikuler') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#ekstraTitle').html("Edit  Ekstra");
                        $('#id_ekstra').val(data.id);
                        $('#ekstra').val(data.nama);
                        $('#id_kategori').val(id_kategori);
                        $('#pembimbing').val(data.pembimbing);
                        $('#actionEkstra').val('Edit');
                        $('#modalEkstra').modal('show');
                    }
                });
            });

            $(document).on('click', '.upload_file', function() {
                let id = $(this).data('id');
                let based = $(this).data('based');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                if (based == 'absen') {
                    $('#fileTitle').html("Tambah File Absensi");
                } else if (based == 'jurnal') {
                    $('#fileTitle').html("Tambah File Jurnal");
                } else if (based == 'nilai') {
                    $('#fileTitle').html("Tambah File Nilai");
                } else {
                    $('#fileTitle').html("Tambah File Dokumentasi");
                }
                $('#formFile').trigger("reset");
                $('#actionFile').val('Add');
                $('#id_file').val(id);
                $('#id_kat_file').val(id_kategori);
                $('#based').val(based);
                $('#modalFile').modal('show');
            });

            $(document).on('click', '.edit_file', function() {
                let id = $(this).data('id');
                let based = $(this).data('based');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                if (based == 'absen') {
                    $('#fileTitle').html("Edit File Absensi");
                } else if (based == 'jurnal') {
                    $('#fileTitle').html("Edit File Jurnal");
                } else if (based == 'nilai') {
                    $('#fileTitle').html("Edit File Nilai");
                } else {
                    $('#fileTitle').html("Edit File Dokumentasi");
                }
                $('#formFile').trigger("reset");
                $('#actionFile').val('Add');
                $('#id_file').val(id);
                $('#id_kat_file').val(id_kategori);
                $('#based').val(based);
                $('#modalFile').modal('show');
            });

            $('body').on('submit', '#formFile', function(e) {
                e.preventDefault();
                $("#btnFile").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnFile").attr("disabled", true);
                var action_url = '';

                if ($('#actionFile').val() == 'Add') {
                    action_url = "{{ route('upload-file_ekstra') }}";
                }

                if ($('#actionFile').val() == 'Edit') {
                    action_url = "bahan_ajar/update";
                }
                let id_kategori = $('#id_kat_file').val();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formFile').trigger("reset");
                            $('#modalFile').modal('hide');
                            $('#dataEkstra' + id_kategori).html(data.ekstra);
                        }
                        noti(data.icon, data.message);
                        $('#btnFile').html('Simpan');
                        $("#btnFile").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then((willDelete) => {
                    $.ajax({
                        url: "{{ route('trash-kategori_ekstrakurikuler') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_kategori').html(data.kategori);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash-alt"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.deleteEkstra', function() {
                let id = $(this).data('id');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then((willDelete) => {
                    $.ajax({
                        url: "{{ route('trash-ekstrakurikuler') }}",
                        type: "POST",
                        data: {
                            id,
                            id_kategori
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataEkstra' + id_kategori).html(data.ekstra);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.delFile', function() {
                let id = $(this).data('id');
                let based = $(this).data('based');
                let id_kategori = $(this).data('kategori');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then((willDelete) => {
                    $.ajax({
                        url: "{{ route('delete-file_ekstra') }}",
                        type: "POST",
                        data: {
                            id,
                            based,
                            id_kategori
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataEkstra' + id_kategori).html(data.ekstra);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });


        function tambahEkstra(id) {
            $('#CustomerForm').trigger("reset");
            $('#ekstraTitle').html("Tambah Ekstra");
            $('#modalEkstra').modal('show');
            $('#id_kategori').val(id);
            $('#action').val('Add');
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddEkstra').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Kategori</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
