<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background-color: #03a9f3">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="HeadingImport">Import Data Jurusan</h5>
            </div>
            <form action="javascript:void(0)" id="importFile" name="importFile" class="form-horizontal"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <center>
                                <div class="centr">
                                    <i class="material-icons"
                                        style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                    <b style="color: #03a9f3">Choose the file to be imported</b>
                                    <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                    <p>Maximum upload file size is 5 MB.</p>
                                    <div class="input_kelas"></div>

                                    <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                        accept="*" hidden />
                                    <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                        Upload File</label>
                                    <span id="file-chosen">No file chosen</span>
                                    <br>
                                    <u id="template_import">
                                        <a href="javascript:void(0)" onclick="return template()"
                                            style="color:#03a9f3">Download sample template
                                            for
                                            import
                                        </a>
                                    </u>
                                </div>
                            </center>
                        </div>
                        <div class="col-md-3"></div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                        value="create">Simpan</a>
                </div>
            </form>
        </div>
    </div>
</div>
