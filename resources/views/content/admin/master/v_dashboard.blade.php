@extends('content.admin.master.v_data_master')
@section('content_master')

    <style>
        .publish {
            border-radius: 0px !important;
            background: #51d2b7;
            color: #fff;
            cursor: pointer;
            margin: 3px;
            width: 123px;
            height: 37px;
        }

        .badge-white,
        .progress-bar-white,
        .bg-white {
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
        }

        .share-input:focus {
            outline: 0 !important;
            box-shadow: none
        }

        .share-input {
            outline: 0 !important;
            box-shadow: none
        }

        .custom-scroll-content {
            height: 37.42857em;
        }

        .share i {
            color: rgb(159, 159, 242) !important
        }

        .share {
            cursor: pointer
        }


        .time {
            font-size: 9px !important
        }

        .socials i {
            margin-right: 33px;
            font-size: 17px;
            color: #d2c8c8;
            cursor: pointer
        }

        .feed-image img {
            width: 100%;
            height: auto
        }

        .count {
            margin-left: 2px;
        }

    </style>
    <div class="feed p-2">
        <div class="share border bg-white">
            <form method="post" id="productForm">
                @csrf
                {{-- <div class="d-flex flex-row inputs p-2 py-4"><img class="rounded-circle" src="" width="40"><input type="text" name="berita" class="border-0 form-control share-input" placeholder="Share your thoughts"></div> --}}
                <div class="d-flex flex-row inputs p-2 py-4"><img class="rounded-circle"
                        src="https://i.imgur.com/44HzzUN.jpg" width="40"><input type="text" name="berita"
                        class="border-0 form-control share-input" placeholder="Share your thoughts"></div>
                <div class="d-flex flex-row justify-content-between border-top">
                    <div class="d-flex flex-row publish-options">
                        <div class="align-items-center border-right p-2 share"><i
                                class="fa fa-camera text-black-50"></i><span class="ml-1">Photo</span></div>
                        <div class="align-items-center border-right p-2 share"><i
                                class="fa fa-video-camera text-black-50"></i><span class="ml-1">Video</span></div>
                        <div class="align-items-center border-right p-2 share"><i class="fa fa-file text-black-50"></i><span
                                class="ml-1">Files</span></div>
                    </div>
                    <button type="submit" class="publish">Publish</button>
                </div>
            </form>
        </div>
        <div id="feeds" class="custom-scroll-content scrollbar-enabled">
            {{-- @foreach ($feed as $item) --}}
            <div class="bg-white border mt-2">
                <div>
                    <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                        <div class="d-flex flex-row align-items-center feed-text px-2"><img class="rounded-circle" src=""
                                width="45">
                            <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">paijo</span><span
                                    class="text-black-50 time">3 hours ago</span></div>
                        </div>
                        <div class="feed-icon px-2"><i class="fa fa-ellipsis-v text-black-50"></i></div>
                    </div>
                </div>
                <div class="p-2 px-3"><span>tesss</span></div>
                <div class="d-flex justify-content-end socials p-2 py-3"><i class="fa fa-thumbs-up"></i><a href="#"><i
                            class="fa fa-comments-o"><span class="count">5</span></a></i> <i class="fa fa-share"></i></div>
            </div>
            {{-- @endforeach --}}

        </div>
    </div>
@endsection
