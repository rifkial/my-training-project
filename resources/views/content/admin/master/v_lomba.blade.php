@extends('content.admin.master.v_data_master')
@section('content_master')
    <style>
        td.hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div class="tombol mb-3">
        @if (session('role') != 'supervisor')
            <button class="btn btn-info btn-sm" id="addKategori"><i class="fas fa-plus-circle"></i> Tambah Kategori</button>
            <button class="btn btn-success btn-sm" id="importKategori"><i class="fas fa-file-excel"></i> Import
                Kategori</button>
            <button class="btn btn-success btn-sm pull-right" id="importPrestasi"><i class="fas fa-file-excel"></i> Import
                Prestasi</button>

        @endif
    </div>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Opsi</th>
                    </tr>
                </thead>

                <tbody id="data-kategori">
                    @if (empty($kategori))
                        <tr>
                            <td colspan="5" class="text-center">Data saat ini tidak tersedia</td>
                        </tr>
                    @else
                        @php
                            $nomer = 1;
                        @endphp
                        @foreach ($kategori as $kt)
                            <tr>
                                <td>{{ $nomer++ }}</td>
                                <td>{{ ucfirst($kt['nama']) }}</td>
                                <td>{{ $kt['keterangan'] }}</td>
                                <td>Aktif</td>
                                <td>
                                    <button data-toggle="collapse" data-target="#demo{{ $kt['id'] }}"
                                        class="btn btn-sm btn-success accordion-toggle"><i
                                            class="fas fa-info-circle"></i></button>
                                    @if (session('role') != 'supervisor')
                                        <a href="javascript:void(0)" data-id="{{ $kt['id'] }}"
                                            class="btn btn-info btn-sm editKategori"><i class="fas fa-pencil-alt"></i></a>
                                        <button class="btn btn-danger btn-sm"
                                            onclick="deleteKategori({{ $kt['id'] }})"><i
                                                class="fas fa-trash"></i></button>
                                    @endif

                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="hiddenRow">
                                    <div class="accordian-body collapse" id="demo{{ $kt['id'] }}">
                                        @if (session('role') != 'supervisor')
                                            <button class="btn btn-info btn-sm my-3 pull-right"
                                                onclick="tambahPeserta({{ $kt['id'] }})"><i
                                                    class="fas fa-plus-circle"></i>
                                                Tambah Peserta</button>
                                        @endif
                                        <table class="table table-striped">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Nama Lomba</th>
                                                    <th class="text-center">Peserta</th>
                                                    <th class="text-center">Kelas</th>
                                                    <th class="text-center">Opsi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="dataPeserta{{ $kt['id'] }}">
                                                @if (empty($kt['prestasi']))
                                                    <tr>
                                                        <td colspan="5" class="text-center">Data saat ini kosong</td>
                                                    </tr>
                                                @else
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($kt['prestasi'] as $pt)
                                                        <tr>
                                                            <td class="text-center">{{ $no++ }}</td>
                                                            <td class="text-center">{{ $pt['nama_lomba'] }}</td>
                                                            <td class="text-center">{{ $pt['nama_peserta'] }}</td>
                                                            <td class="text-center">{{ $pt['kelas'] }}</td>
                                                            <td class="text-center">
                                                                <button class="btn btn-sm btn-info"><i
                                                                        class="fas fa-info-circle"></i></button>
                                                                @if (session('role') != 'supervisor')
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $pt['id'] }}"
                                                                        class="btn btn-info btn-sm editPrestasi"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <button class="btn btn-danger btn-sm"
                                                                        onclick="deletePrestasi({{ $pt['id'] }}, {{ $kt['id'] }})"><i
                                                                            class="fas fa-trash"></i></button>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalKategori" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingKategori"></h5>
                </div>
                <form id="formKategori" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kategori">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Kategori</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan[]" id="keterangan" cols="30" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris mt-2">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPeserta" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingPrestasi"></h5>
                </div>
                <form id="formPeserta" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_prestasi">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="id_kategori" id="kategori_prestasi">
                                    <label for="name" class="col-sm-12 control-label">Nama Lomba</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama_lomba" name="nama"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Peserta</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama_peserta" name="nama_peserta">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kelas</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kelas_lomba" name="kelas">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun Lomba</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="tahun_lomba" name="tahun"
                                            value="{{ session('tahun') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Peringkat Kabupaten</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="peringkat_kab" name="peringkat_kab">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Peringkat Provinsi</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="peringkat_prov"
                                            name="peringkat_prov">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Peringkat Nasional</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="peringkat_nas" name="peringkat_nas">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan_prestasi" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionPeserta" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left"
                            id="btnSavePeserta">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="importModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport"></h5>
                </div>
                <form action="javascript:void(0)" id="importForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="tujuan" id="tujuan" value="kategori">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons"
                                            style="font-size: 4.5rem; color: #03a9f3">file_download</i><br>
                                        <b style="color: #03a9f3">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file"> <i class="material-icons">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a id="downloadTemplate" href="javascript:void(0)" target="_blank"
                                                style="color:#03a9f3">Download sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="importBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#importKategori').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#tujuan').val('kategori');
                $('#HeadingImport').html('Import Kategori');
                $("#downloadTemplate").attr("href", "{{ route('kategori_lomba-download_sample') }}");
            });
            $('#importPrestasi').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#tujuan').val('prestasi');
                $('#HeadingImport').html('Import Prestasi Siswa');
                $("#downloadTemplate").attr("href", "{{ route('prestasi_siswa-download_sample') }}");
            });

            $('body').on('submit', '#importForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("importForm"));
                var action_url = '';
                if ($('#tujuan').val() == 'kategori') {
                    action_url = "{{ route('kategori_lomba-import') }}";
                }

                if ($('#tujuan').val() == 'prestasi') {
                    action_url = "{{ route('prestasi_siswa-import') }}";
                }
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#importFile').trigger("reset");
                            $('#importModal').modal('hide');
                        }
                        $('#data-kategori').html(data.html);
                        $('#importBtn').html('Simpan');
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });


            $('#addKategori').click(function() {
                $('#formKategori').trigger("reset");
                $('#headingKategori').html("Tambah Kategori Lomba");
                $('.tambahBaris').show('');
                $('#modalKategori').modal('show');
                $('#action').val('Add');
            });

            $(document).on('click', '.btn-remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

            $('#formKategori').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('kategori_lomba-simpan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('kategori_lomba-update') }}";
                    method_url = "POST";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        // $('.table-responsive').html('<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalKategori').modal('hide');
                            $('#formKategori').trigger("reset");
                        }
                        $('#data-kategori').html(data.html);
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editKategori', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('kategori_lomba-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#headingKategori').html("Edit Data Kategori");
                        $('#id_kategori').val(data.id);
                        $('#nama').val(data.nama);
                        $('#keterangan').val(data.keterangan);
                        $('#modalKategori').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $('#formPeserta').on('submit', function(event) {
                event.preventDefault();
                $("#btnSavePeserta").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnSavePeserta").attr("disabled", true);
                let id_kategori = $('#kategori_prestasi').val();
                let action_url = '';

                if ($('#actionPeserta').val() == 'Add') {
                    action_url = "{{ route('prestasi_siswa-simpan') }}";
                    method_url = "POST";
                }

                if ($('#actionPeserta').val() == 'Edit') {
                    action_url = "{{ route('prestasi_siswa-update') }}";
                    method_url = "POST";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalPeserta').modal('hide');
                            $('#formPeserta').trigger("reset");
                        }
                        $('#dataPeserta' + id_kategori).html(data.html);
                        noti(data.icon, data.message);
                        $('#btnSavePeserta').html('Simpan');
                        $("#btnSavePeserta").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editPrestasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('prestasi_siswa-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#headingPrestasi').html("Edit Data Prestasi");
                        $('#id_prestasi').val(data.id);
                        $('#kategori_prestasi').val(data.id_kategori_lomba).trigger('change');
                        $('#nama_lomba').val(data.nama_lomba);
                        $('#nama_peserta').val(data.nama_peserta);
                        $('#kelas_lomba').val(data.kelas);
                        $('#tahun_lomba').val(data.tahun);
                        $('#peringkat_kab').val(data.peringkat_kab);
                        $('#peringkat_prov').val(data.peringkat_prov);
                        $('#peringkat_nas').val(data.peringkat_nas);
                        $('#keterangan_prestasi').val(data.keterangan);
                        $('#modalPeserta').modal('show');
                        $('#actionPeserta').val('Edit')
                    }
                });
            });


        })

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div id="row' + i +
                '"><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Nama Kategori</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div></div><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Keterangan</label><div class="col-sm-12"><textarea name="keterangan[]" id="keterangan" cols="30" rows="3" class="form-control"></textarea></div></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        function deleteKategori(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('kategori_lomba-delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    success: function(data) {
                        console.log(data);
                        $('#data-kategori').html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function tambahPeserta(id) {
            $('#formAnggota').trigger("reset");
            $('#kategori_prestasi').val(id);
            $('#headingPrestasi').html("Tambah Prestasi Siswa");
            $('#modalPeserta').modal('show');
            $('#actionPeserta').val('Add');
        }

        function deletePrestasi(id, id_kategori) {
            // console.log('#dataPeserta' + id_kategori);
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('prestasi_siswa-delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        // $(".delete-" + id).html(
                        //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $('#dataPeserta' + id_kategori).html(data.html);
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });
    </script>
@endsection
