@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        th#nomor select,
        th#aksion select {
            display: contents;
        }

        tfoot tr th select {
            font-family: inherit;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            display: block;
            width: 100%;
            max-width: 320px;
            height: 36px;
            float: right;
            margin: 5px 0px;
            padding: 0px 24px;
            font-size: 16px;
            line-height: 1.75;
            color: #333;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            -ms-word-break: normal;
            word-break: normal;
        }

        tfoot tr th select:after {
            content: '<>';
            font: 17px "Consolas", monospace;
            color: #333;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
            padding: 0 0 2px;
            border-bottom: 1px solid #999;
            pointer-events: none;
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Jenis Kompetensi</th>
                                <th>Pelajaran</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Kompetensi Inti</th>
                                <th>Indikator</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th id="nomor">#</th>
                                <th>Jenis Kompetensi</th>
                                <th>Pelajaran</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Kompetensi Inti</th>
                                <th>Indikator</th>
                                <th id="aksion">Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_kompetensi">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Jenis Kompetensi</label>
                                    <div class="col-sm-12">
                                        <select name="jenis_kompetensi" id="jenis_kompetensi" class="form-control">
                                            <option value="keterampilan">Keterampilan</option>
                                            <option value="pengetahuan">Pengetahuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Mapel</label>
                                    <div class="col-sm-12">
                                        <select name="id_mapel" id="id_mapel" class="form-control">
                                            <option value="">--- Pilih Mapel ---</option>
                                            @foreach ($mapel as $item)
                                                <option value="{{ $item['id'] }}">
                                                    {{ $item['kode_mapel'] . ' | ' . $item['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="input_jurusan">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-12 control-label">Pilih Jurusan</label>
                                        <div class="col-sm-12">
                                            <select name="id_jurusan" id="id_jurusan" class="form-control">
                                                <option value="0">--- Tidak Mempunyai Jurusan ---</option>
                                                @foreach ($jurusan as $jrs)
                                                    <option value="{{ $jrs['id'] }}">{{ $jrs['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Kelas</label>
                                    <div class="col-sm-12">
                                        <select name="id_kelas" id="id_kelas" class="form-control">
                                            <option value="">Pilih Kelas</option>
                                            @foreach ($kelas as $kl)
                                                <option value="{{ $kl['id'] }}">{{ $kl['nama_romawi'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kompetensi Inti</label>
                                    <div class="col-sm-12">
                                        <textarea name="kompetensi_inti" id="kompetensi_inti" cols="30" rows="3"
                                            class="form-control" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Indikator</label>
                                    <div class="col-sm-12">
                                        <textarea name="indikator" id="indikator" cols="30" rows="3" class="form-control"
                                            required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle">Data Trash Kompetensi</h5>
                </div>
                <div style="width: 100%;">
                    <div class="table-responsive" style="margin-top: 14px;">
                        <table class="table table-striped" id="data-trash" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Jenis Kompetensi</th>
                                    <th>Pelajaran</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Kompetensi Inti</th>
                                    <th>Indikator</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme bs-modal-lg-color-scheme" id="selectMapel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background-color: #03a9f3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeadings">Pilih Mata Pelajaran</h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                @php
                                    $class_button = ['primary', 'info', 'success', 'danger', 'warning'];
                                @endphp
                                @if (!empty($guru_pel))
                                    @foreach ($guru_pel as $mp)
                                        <a href="javascript:void(0)"
                                            onclick="return selectMapel({{ $mp['id_mapel'] }}, '{{ $mp['id_kelas'] }}');"
                                            class="btn btn-{{ $class_button[array_rand($class_button)] }}"
                                            style="margin-top: 3px;">{{ 'Mapel ' . $mp['mapel'] . '   Kelas ' . $mp['kelas'] }}</a>
                                    @endforeach

                                @endif
                            </center>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('content.admin.master.import')


    <script type="text/javascript">
        var url_import = "{{ route('import-kompetensi_inti') }}";
        $(".input_kelas").html(
            '<input type="hidden" name="id_mapels" id="id_mapels"><input type="hidden" name="id_kelass" id="id_kelass">'
        );
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        var select = $(
                                '<select><option value="" class="form-control"></option></select>'
                            )
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d +
                                '</option>')
                        });
                    });
                },
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        text: '<i class="fa fa-upload"></i>',
                        className: 'btn btn-sm',
                        attr: {
                            title: 'Import Data',
                            id: 'mapel'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        },
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    {
                        text: '<i class="fa fa-undo"></i>',
                        attr: {
                            title: 'Data Trash',
                            id: 'data_trash'
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'jenis_kompetensi',
                        name: 'jenis_kompetensi'
                    },
                    {
                        data: 'pelajaran',
                        name: 'pelajaran'
                    },
                    {
                        data: 'kelas_romawi',
                        name: 'kelas_romawi'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'kompetensi_inti',
                        name: 'kompetensi_inti'
                    },
                    {
                        data: 'indikator',
                        name: 'indikator'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-kompetensi_inti') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'jenis_kompetensi',
                        name: 'jenis_kompetensi'
                    },
                    {
                        data: 'pelajaran',
                        name: 'pelajaran'
                    },
                    {
                        data: 'kelas',
                        name: 'kelas'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'kompetensi_inti',
                        name: 'kompetensi_inti'
                    },
                    {
                        data: 'indikator',
                        name: 'indikator'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#mapel').click(function() {
                $('#selectMapel').modal('show');
            });


            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Kompetensi Inti");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "kompetensi_inti/store";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "kompetensi_inti/update";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        $("#saveBtn").attr("disabled", false);
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.success);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "kompetensi_inti/edit",
                    data: {
                        id_kompetensi: id
                    },
                    success: function(data) {
                        $('#modelHeading').html("Edit Data Kompetensi");
                        $('#saveBtn').val("edit-user");
                        $('#id_kompetensi').val(data.id);
                        $('#jenis_kompetensi').val(data.jenis_kompetensi).trigger("change");
                        $('#id_mapel').val(data.id_mapel).trigger('change');
                        $('#id_jurusan').val(data.id_jurusan).trigger('change');
                        var id_jurusan = data.id_jurusan;
                        var id_kelas = data.id_kelas;
                        $('#kompetensi_inti').val(data.kompetensi_inti);
                        $('#indikator').val(data.indikator);
                        edit_kelas(id_jurusan, id_kelas);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $('select[name="id_jurusan"]').on('change', function() {
                var id_jurusan = $(this).val();
                let action_url = '';
                let method_url = '';
                if (id_jurusan) {
                    if (id_jurusan != 0) {
                        method_url = "POST";
                        action_url = "{{ route('get-jurusan') }}";                       
                    } else {
                        method_url = "GET";
                        action_url = "{{ route('get_kelas-jurusan_kosong') }}";
                    }
                    $.ajax({
                        url: action_url,
                        type: method_url,
                        data: {
                            id_jurusan
                        },
                        success: function(data) {
                            var s = '';
                            data = JSON.parse(data);
                            data.forEach(function(row) {
                                s += '<option value="' + row.id + '">' + row
                                    .nama_romawi +
                                    '</option>';
                            })
                            $('select[name="id_kelas"]').removeAttr('disabled')
                            $('select[name="id_kelas"]').html(s)
                        }
                    });
                }
            })
        });

        function selectMapel(id_mapel, id_kelas) {
            console.log("id_mapel" + id_mapel + "id_kelasnya adalah" + id_kelas);
            $('#id_mapels').val(id_mapel);
            $('#id_kelass').val(id_kelas);
            $('#importModal').modal('show');
            $('#template_import').html('<a href="javascript:void(0)" onclick="return template(' + id_mapel + ',' +
                id_kelas + ')"style="color:#03a9f3">Download sample template for imports </a>');
            $('#file-chosen').html('No file choosen');
            $('#HeadingImport').html('Import Data Kompetensi Inti');
        }

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then((willDelete) => {
                $.ajax({
                    url: "kompetensi_inti/delete",
                    type: "POST",
                    data: {
                        id_kompetensi: id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }


        $('select[name="id_jurusan"]').on('change', function() {
            var id_jurusan = $(this).val();
            
            let action_url = "{{route('get-jurusan')}}";
            if (id_jurusan) {
                if (id_jurusan != 0) {
                    method_url = "POST";
                    action_url = "{{route('get-jurusan')}}";
                }else{

                }
                $.ajax({
                    url: action_url,
                    type: method_url,
                    data: {
                        id_jurusan : id_jurusan
                    },
                    success: function(data) {
                        console.log(data);
                        var s = '';
                        data = JSON.parse(data);
                        data.forEach(function(row) {
                            s += '<option value="' + row.id + '">' + row.nama_romawi +
                                '</option>';
                        })
                        $('select[name="id_kelas"]').removeAttr('disabled')
                        $('select[name="id_kelas"]').html(s)
                    }
                });
            }
        })


        function edit_kelas(id_jurusan, id_kelas) {
            $.ajax({
                url: "{{ route('edit_data-jurusan') }}",
                type: "POST",
                data: {
                    id_jurusan: id_jurusan,
                    id_kelas: id_kelas
                },
                beforeSend: function() {
                    $('select[name="id_kelas"]').append('<option value="">--- No Kelas Found ---</option>');
                },
                success: function(fb) {
                    $('select[name="id_kelas"]').html('');
                    $('select[name="id_kelas"]').html(fb);
                }
            });
            return false;
        }

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/master/kompetensi_inti/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/master/kompetensi_inti/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });


        function template(id_mapel, id_kelas) {
            // console.log("id_mapelnya "+ id_mapel + ", id kelasnya "+ id_kelas);
            $.ajax({
                type: 'POST',
                url: "kompetensi_inti/template",
                data: {
                    id_mapel,
                    id_kelas
                },
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    </script>
@endsection
