@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <hr>
                    <div class="alert alert-info border-info" role="alert">
                        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                aria-hidden="true">×</span>
                        </button>
                        <p><strong><i class="fas fa-info-circle"></i> Pemberitahuan:</strong>
                        </p>
                        <p>Bila ada data lebih dari satu didalam satu tahun ajar, maka settingan yang akan digunakan adalah data yang
                            pertama</p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered mr-b-0 hover">
                            <thead>
                                <tr class="bg-purple">
                                    <th class="text-center text-white">No</th>
                                    <th class="text-center text-white">Tahun Ajaran</th>
                                    <th class="text-center text-white">Semester</th>
                                    <th class="text-center text-white">Status</th>
                                    <th class="text-center text-white">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($config))
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada data yang tersedia</td>
                                    </tr>
                                @else
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($config as $cfg)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $cfg['tahun_ajaran'] }}</td>
                                            <td>{{ $cfg['semester'] }}</td>
                                            <td class="text-center">
                                                <span
                                                    class="badge badge-{{ $cfg['status_kode'] == 1 ? 'success' : 'danger' }} text-inverse">{{ $cfg['status_kode'] == 1 ? 'Aktif' : 'Tidak Aktif' }}</span>
                                            </td>
                                            <td>
                                                <button class="btn btn-purple btn-sm" data-toggle="collapse"
                                                    data-target="#config{{ $cfg['id'] }}"><i
                                                        class="fas fa-eye"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="config{{ $cfg['id'] }}">
                                                    <button class="btn btn-info btn-sm my-3 pull-right"
                                                        onclick="tambahConfig({{ $cfg['id'] }})"><i
                                                            class="fas fa-plus-circle"></i>
                                                        Tambah Config</button>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>#</th>
                                                                <th>Jenis</th>
                                                                <th>Kepala Sekolah</th>
                                                                <th>Tgl Raport</th>
                                                                <th>Tgl Raport Kelas Akhir</th>
                                                                <th>Paraf</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="data_config{{ $cfg['id'] }}">
                                                            @if (empty($cfg['config_raport']))
                                                                <tr>
                                                                    <td colspan="7" class="text-center">Belum ada Config
                                                                        Raport yang tersedia</td>
                                                                </tr>
                                                            @else
                                                                @php
                                                                    $nomer = 1;
                                                                @endphp
                                                                @foreach ($cfg['config_raport'] as $config)
                                                                    <tr>
                                                                        <td>{{ $nomer++ }}</td>
                                                                        <td>{{ $config['jenis'] }}</td>
                                                                        <td>{{ $config['kepsek'] }}</td>
                                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($config['tgl_raport']) }}
                                                                        </td>
                                                                        <td>{{ (new \App\Helpers\Help())->getTanggal($config['tgl_raport_kelas_akhir']) }}
                                                                        </td>
                                                                        <td><img src="{{ $config['paraf'] }}" alt=""
                                                                                width="100"></td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-info btn-sm edit"
                                                                                data-id="{{ $config['id'] }}"
                                                                                data-tahun="{{ $cfg['id'] }}"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="javascript:void(0)"
                                                                                class="btn btn-danger btn-sm delete"
                                                                                data-id="{{ $config['id'] }}"
                                                                                data-tahun="{{ $cfg['id'] }}"><i
                                                                                    class="fas fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id_config">
                                    <input type="hidden" name="id_tahun_ajar" id="id_tahun_ajar">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Raport</label>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" name="tanggal" id="tanggal" class="form-control datepicker"
                                                value="{{ date('d-m-Y') }}" data-plugin-options='{"autoclose": true}'>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Raport Untuk Kelas
                                        Akhir</label>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_kelas_akhir" id="tanggal_kelas_akhir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Kepala Sekolah</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="kepsek" name="kepsek" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">NIP Kepala Sekolah</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nip_kepsek" name="nip_kepsek">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Jenis</label>
                                    <div class="col-sm-12">
                                        <select name="jenis" id="jenis" class="form-control">
                                            <option value="kd">Kompetensi Dasar</option>
                                            <option value="manual">Manual</option>
                                            <option value="k16">K-16</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Paraf</label>
                                    <div class="row mx-0">
                                        <div class="col-md-2">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6" style="position: relative">
                                            <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-save_config') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_config') }}";
                }
                let id_tahun = $('#id_tahun_ajar').val();

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#data_config' + id_tahun).html(data.config);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('raport-edit_config') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // console.log("tes");
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Raport Config");
                        $('#id_config').val(data.id);
                        $('#id_tahun_ajar').val(data.id_ta_sm);
                        $('#tanggal').val(data.tgl_raport);
                        $('#tanggal_kelas_akhir').val(data.tgl_raport_kelas_akhir);
                        $('#kepsek').val(data.kepsek);
                        $('#nip_kepsek').val(data.nip_kepsek);
                        $('#jenis').val(data.jenis).trigger("change");
                        $('#delete_foto').html('');
                        if (data.paraf) {
                            $('#modal-preview').attr('src', data.paraf);
                            $('#delete_foto').html(
                                '<input type="checkbox" name="remove_photo" value="' +
                                data
                                .paraf + '"/> Remove photo');
                        } else {
                            $('#modal-preview').attr('src',
                                'https://via.placeholder.com/150');
                        }
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let id_tahun = $(this).data('tahun');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('raport-delete_config') }}",
                        type: "POST",
                        data: {
                            id,
                            id_tahun
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_config' + id_tahun).html(data.config);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                });
            });
        });

        function tambahConfig(id) {
            $('#CustomerForm').trigger("reset");
            $('#id_tahun_ajar').val(id);
            $('#modelHeading').html("Tambah Raport Config");
            $('#delete_foto').html('');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        }

        function deleteData(id) {

        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })
    </script>
@endsection
