@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            {{-- <a href="javascript:void(0)" onclick="return template()" class="btn btn-success">download file</a> --}}
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Sub Judul</th>
                                <th>Gambar 1</th>
                                <th>Gambar 2</th>
                                <th>Footer</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id_sampul">
                                    <label for="name" class="col-sm-12 control-label">Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Sub Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="sub_title" name="sub_title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Footer</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="footer" name="footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Gambar 1</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image[]" accept="image/*"
                                            onchange="readURL(this);">
                                    </div>
                                </div>
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group" width="100%" style="margin-top: 10px">
                                <div id="delete_foto" style="text-align: center"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Gambar 2</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image[]" accept="image/*"
                                            onchange="readURLS(this);">
                                    </div>
                                </div>
                                <img id="modal-previews" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group" width="100%" style="margin-top: 10px">
                                <div id="delete_foto1" style="text-align: center"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                    'colvis',
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'sub_title',
                        name: 'sub_title'

                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'gambar1',
                        name: 'gambar1'
                    },
                    {
                        data: 'footer',
                        name: 'footer'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Sampul ");
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                $('#modal-previews').attr('src', 'https://via.placeholder.com/150');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-save_template_sampul') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_template_sampul') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
                $('#modal-previews').attr('src', 'https://via.placeholder.com/150');
                $.ajax({
                    type: 'POST',
                    url: "template_sampul/edit",
                    data: {
                        id_sampul: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // console.log("tes");
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Template Raport");
                        $('#saveBtn').val("edit-user");
                        $('#id_sampul').val(data.id);
                        $('#title').val(data.title);
                        $('#sub_title').val(data.sub_title);
                        $('#footer').val(data.footer);
                        $('#delete_foto').html('');
                        $('#delete_foto1').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            // $('#hidden_image').attr('src', data.file);
                            $('#delete_foto').append(
                                '<input type="checkbox" name="remove_photo" value="' + data
                                .file + '"/> Remove photo when saving'); // remove photo
                        }
                        if (data.file1) {
                            $('#modal-previews').attr('src', data.file1);
                            // $('#hidden_image').attr('src', data.file);
                            $('#delete_foto1').append(
                                '<input type="checkbox" name="remove_photo1" value="' + data
                                .file1 + '"/> Remove photo when saving'); // remove photo
                        }
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "template_sampul/trash/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }

        function readURLS(input, id) {
            id = id || '#modal-previews';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-previews').removeClass('hidden');
                $('#starts').hide();
            }
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })

    </script>
@endsection
