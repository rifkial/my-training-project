@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        #label_file {
            background-color: #03a9f3;
            border: 1px solid #0bbd98;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">{{ Session::get('title') }}</h3>
                </div>
                <div class="col-md-6">
                    <a href="" class="btn btn-success btn-sm pull-right">Lihat Kop Surat</a>
                </div>
            </div>

            <hr>
            
            @if(Session::has('respons'))
            {{-- {{ dd(session()->get('respons')) }} --}}
            <div class="row">
                <div class="col-md-12 p-0">
                    <div class="alert alert-icon alert-{{ session()->get('respons')['icon'] }} border-{{ session()->get('respons')['icon'] }} alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span>
                        </button> <i class="material-icons list-icon">check_circle</i> <strong>Well done!</strong> {{ session()->get('respons')['message'] }}.
                    </div>
                </div>
            </div>
            @endif
            <form action="{{ route('raport-save_kop_sampul') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row widget-bg" style="border: 3px solid #2471d2 !important;">
                    <div class="col-md-2">
                        <div class="text-center">
                            @if ($kop['file'] != null)
                                <img id="modal-preview" src="{{ $kop['file'] }}" class="avatar img-circle" alt="avatar">
                            @else
                                <img id="modal-preview" src="https://alpha.sam.gov/assets/img/logo-not-found.png"
                                    class="avatar img-circle" alt="avatar">
                            @endif
                            <h6>Upload Logo Kiri...</h6>
                            <input type="file" name="image[]" accept="image/*" class="form-control"
                                onchange="readURL(this);">
                        </div>
                    </div>
                    <div class="col-md-8 personal-info">
                        <input type="hidden" name="id" value="{{ $kop['id'] }}">
                        <input type="text" name="teks1" placeholder="ex:KOTA BANDUNG" id="" class="form-control"
                            style="text-align: center; font-size: 1.372rem;" value="{{ strtoupper($kop['teks1']) }}">
                        <input type="text" name="teks2" placeholder="ex:DINAS PENDIDIKAN" id="" class="form-control mt-1"
                            style="text-align: center; font-size: 14px;" value="{{ strtoupper($kop['teks2']) }}">
                        <input type="text" name="teks3" placeholder="ex:SMK NEGERI 1 BANDUNG" id=""
                            class="form-control mt-1" style="text-align: center; font-size: 26px; font-weight: bold"
                            value="{{ strtoupper($kop['teks3']) }}">
                        <input type="text" name="teks4" placeholder="ex:KECAMATAN SUMUR BANDUNG" id=""
                            class="form-control mt-1" style="text-align: center; font-size: 16px;"
                            value="{{ strtoupper($kop['teks4']) }}">
                        @if ($kop['teks5'] != null)
                            <textarea name="teks5" id="" cols="30" class="form-control mt-1" rows="3"
                                placeholder="Detail sekolah" style="text-align: center"> {{ $kop['teks5'] }}</textarea>
                        @else
                            <textarea name="teks5" id="" cols="30" class="form-control mt-1" rows="3"
                                placeholder="Detail sekolah"
                                style="text-align: center"> STATUS TERAKREDITASI B N88:301.640.303 NPBN:3095959 &#13;&#10; Jl Wastukencana No.3, Babakan Ciamis Telp : (022) 4204514  Kode Pos: 40117 &#13;&#10; Web: http://www.smknegeri1bandung.com/ Email: sekolah@gmail.com</textarea>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <div class="text-center">
                            @if ($kop['file1'] != null)
                                <img id="modal-previews" src="{{ $kop['file1'] }}" class="avatar img-circle" alt="avatar">
                            @else
                                <img id="modal-previews" src="https://alpha.sam.gov/assets/img/logo-not-found.png"
                                    class="avatar img-circle" alt="avatar">
                            @endif

                            <h6>Upload Logo Kanan...</h6>

                            <input type="file" name="image[]" accept="image/*" class="form-control"
                                onchange="readURLS(this);">
                        </div>
                    </div>
                </div>
                <div class="row widget-bg p-1 mt-1">
                    <div class="col-md-12 p-0">
                        <button type="submit" class="btn btn-info btn-block">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-save_kop_sampul') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_kop_sampul') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


        });

        function readURL(input, id) {
            id = id || '#modal-preview';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-preview').removeClass('hidden');
                $('#start').hide();
            }
        }

        function readURLS(input, id) {
            id = id || '#modal-previews';
            if (input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                $('#modal-previews').removeClass('hidden');
                $('#starts').hide();
            }
        }

        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })

        function restoreData(id) {
            swal(
                'Pulihkan?',
                'data anda akan kembali pulih',
                'question'
            ).then((lanjut) => {
                if (lanjut) {
                    $.ajax({
                        url: "{{ url('admin/program/raport/kop_sampul/restore') }}" + '/' + id,
                        type: "POST",
                        data: {
                            '_method': 'PATCH'
                        },
                        beforeSend: function() {
                            $(".restore-" + id).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                swa("Berhasil!", data.message, data.success);
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                swa("Gagal!", data.message, data.success);
                            }
                        }
                    });
                } else {
                    swal("Proses dibatalkan!");
                }
            });
        }

        function forceDelete(id) {
            swal({
                title: 'Apa anda yakin?',
                text: "Data anda nantinya tidak dapat dipulihkan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ url('admin/program/raport/kop_sampul/hard_delete') }}" + '/' + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    beforeSend: function() {
                        $(".hardDelete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
