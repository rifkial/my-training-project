@extends('content.admin.user.v_data_user')
@section('content_user')
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <h5 class="box-title">{{ session('title') }}</h5>
                    <hr>
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <button class="btn btn-info" id="addData"><i class="fas fa-plus-circle"></i> Tambah
                                Data</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered mr-b-0 hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Telepon</th>
                                    <th>Email</th>
                                    <th>Password Reset</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="data_admin">
                                @if (!empty($admin))
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($admin as $adm)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ Str::upper($adm['nama']) }}</td>
                                            <td>{{ $adm['telepon'] }}</td>
                                            <td>{{ $adm['email'] }}</td>
                                            <td>{{ $adm['first_password'] }}</td>
                                            <td>
                                                <button data-toggle="collapse" data-target="#detail{{ $adm['id'] }}"
                                                    class="btn btn-sm btn-purple"><i
                                                        class="fas fa-info-circle"></i></button>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="edit btn btn-sm btn-info"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="delete btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                                <a href="javascript:void(0)" data-id="{{ $adm['id'] }}"
                                                    class="reset_pass btn btn-sm btn-warning"><i
                                                        class="fas fa-key"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="hiddenRow">
                                                <div class="accordian-body collapse" id="detail{{ $adm['id'] }}">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <th class="text-center vertical-middle" colspan="2" rowspan="5">
                                                                <img src="{{ $adm['file'] }}" alt="" height="200px">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <th class="text-center">Nama</th>
                                                            <td>{{ Str::ucfirst($adm['nama']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <th class="text-center">NIP</th>
                                                            <td>{{ $adm['nip'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <th class="text-center">NIK</th>
                                                            <td>{{ Str::ucfirst($adm['nik']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <th class="text-center">NUPTK</th>
                                                            <td>{{ $adm['nuptk'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Jenis Kelamin</th>
                                                            <td>{{ $adm['jenkel'] == 'l' ? 'laki - laki' : 'Perempuan' }}
                                                            </td>
                                                            <td></td>
                                                            <th class="text-center">Agama</th>
                                                            <td>{{ Str::ucfirst($adm['agama']) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Telepon</th>
                                                            <td>{{ $adm['telepon'] }}</td>
                                                            <td></td>
                                                            <th class="text-center">Email</th>
                                                            <td>{{ $adm['email'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Tempat Lahir</th>
                                                            <td>{{ $adm['tempat_lahir'] }}</td>
                                                            <td></td>
                                                            <th class="text-center">Tanggal Lahir</th>
                                                            <td>{{ (new \App\Helpers\Help())->getTanggal($adm['tgl_lahir']) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Alamat</th>
                                                            <td>{{ $adm['alamat'] }}</td>
                                                            <td></td>
                                                            <th class="text-center">Reset Password</th>
                                                            <td>{{ $adm['first_password'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Terakhir Login</th>
                                                            <td>{{ (new \App\Helpers\Help())->getTanggal($adm['last_login']) }}
                                                            </td>
                                                            <td></td>
                                                            <th class="text-center">Status</th>
                                                            <td>{{ $adm['status'] == 1 ? 'Enabled' : 'Disabled' }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center">Dibuat</th>
                                                            <td>{{ (new \App\Helpers\Help())->getTanggal($adm['created_at']) }}
                                                            </td>
                                                            <td></td>
                                                            <th class="text-center">Terakhir di update</th>
                                                            <td>{{ (new \App\Helpers\Help())->getTanggalLengkap($adm['updated_at']) }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">Data saat ini tidak tersedia</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAdmin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formAdmin" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_admin">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nik" id="nik" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NUPTK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nuptk" id="nuptk" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIP</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nip" id="nip" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <input type="hidden" name="old_image" id="old_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#addData').click(function() {
                $('#formAdmin').trigger("reset");
                $('#modelHeading').html("Tambah Admin Raport");
                $('#modalAdmin').modal('show');
                $('#action').val('Add');
                $('#delete_foto').html('');
                $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
            });

            $('body').on('submit', '#formAdmin', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                var action_url = '';
                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('admin_raport-store') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('admin_raport-update') }}";
                }
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formAdmin').trigger("reset");
                            $('#modalAdmin').modal('hide');
                            $('#data_admin').html(data.admin);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin_raport-edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Admin Raport");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#nip').val(data.nip);
                        $('#nik').val(data.nik);
                        $('#nuptk').val(data.nuptk);
                        $('#tahun_masuk').val(data.tahun_masuk);
                        $('#informasi_lain').val(data.informasi_lain);
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#alamat').val(data.alamat);
                        $('#old_image').val(data.old_image);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#jenkel').val(data.jenkel).trigger('change');
                        $('#delete_foto').html('');
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            $('#hidden_image').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#modalAdmin').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('admin_raport-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_admin').html(data.admin);
                            }else{
                                $(loader).html('<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.reset_pass', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin mereset password user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('reset_password-admin_raport') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html('<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_admin').html(data.admin);
                            }else{
                                $(loader).html('<i class="fas fa-key"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
    </script>
@endsection
