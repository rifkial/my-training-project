@extends('template/template_default/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <p class="text-muted">Digunakan untuk memberikan predikat nilai dan deskripsi otomatis saat cetak
                            raport.</p>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-outline-info" id="addData"><i class="fas fa-plus"></i>
                                    Settingan Nilai</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center">Tahun Ajaran</th>
                                        <th class="text-center">Nilai</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_config">
                                    @if (empty($config))
                                        <tr>
                                            <td colspan="3" style="text-align: center">Data saat ini tidak tersedia</td>
                                        </tr>
                                    @else
                                        @foreach ($config as $cf)
                                            <tr>
                                                <td class="vertical-middle text-center">
                                                    <h3 class="m-0">{{ $cf['tahun_ajaran'] }}</h3>
                                                    <small>{{ $cf['semester'] }}</small>
                                                </td>
                                                <td>
                                                    <table class="table">
                                                        <tr>
                                                            <td>Bobot Ulangan Harian</td>
                                                            <td>{{ $cf['nilai_uh_rata'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bobot UTS</td>
                                                            <td>{{ $cf['nilai_uts'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bobot UAS</td>
                                                            <td>{{ $cf['nilai_uas'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Jumlah</th>
                                                            <th>100</th>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="vertical-middle text-center">
                                                    <a href="javascript:void(0)" data-id="{{ $cf['id'] }}"
                                                        class="btn btn-sm btn-info edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" data-id="{{ $cf['id'] }}"
                                                        class="btn btn-sm btn-danger delete"><i
                                                            class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-lg-12" style="background: #fff; box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);">
            <h3 class="box-title">{{ Session::get('title') }}</h3>
            <hr>
            <div style="width: 100%;">
                <div class="table-responsive">
                    <table class="table table-striped" id="data-tabel">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nilai Ulangan Harian</th>
                                <th>Nilai UTS</th>
                                <th>Nilai UAS</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalConfig" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleConfig"></h5>
                </div>
                <form id="formConfig">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_nilai">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Pilih Tahun Ajaran</label>
                                    <div class="col-sm-12">
                                        <select name="id_tahun" id="id_tahun" class="form-control">
                                            <option value="">-- Pilih tahun ajaran --</option>
                                            @foreach ($tahun as $th)
                                                <option value="{{ $th['id'] }}"
                                                    {{ session('id_tahun_ajar') == $th['id'] ? 'selected' : '' }}>
                                                    {{ $th['tahun_ajaran'] . ' ' . $th['semester'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Ulangan Harian</label>
                                    <div class="col-sm-12">
                                        <input type="number" onblur="findTotal()" class="form-control" id="nilai_uh_rata"
                                            name="nilai_uh_rata" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nilai UTS</label>
                                    <div class="col-sm-12">
                                        <input type="number" onblur="findTotal()" class="form-control" id="nilai_uts"
                                            name="nilai_uts" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nilai UAS</label>
                                    <div class="col-sm-12">
                                        <input type="number" onblur="findTotal()" class="form-control" id="nilai_uas"
                                            name="nilai_uas" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12" id="total">
                                <h2 class="m-0 text-center text-danger">Total Nilai <b>0</b></h2>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            $('#addData').click(function() {
                $('#formConfig').trigger("reset");
                $('#titleConfig').html("Tambah Config");
                $('#modalConfig').modal('show');
                $('#total').html('<h2 class="m-0 text-center text-danger">Total Nilai <b>0</b></h2>');
                $('#action').val('Add');
            });

            $('#formConfig').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('raport-store_setting_nilai') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('raport-update_setting_nilai') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status != "gagal") {
                            $('#formConfig').trigger("reset");
                            $('#modalConfig').modal('hide');
                            $('#data_config').html(data.config);
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('raport-edit_setting_nilai') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleConfig').html("Edit Config");
                        $('#id_nilai').val(data.id);
                        $('#nilai_uh_rata').val(data.nilai_uh_rata);
                        $('#nilai_uts').val(data.nilai_uts);
                        $('#nilai_uas').val(data.nilai_uas);
                        $('#id_tahun').val(data.id_ta_sm).trigger("change");
                        $('#total').html('<h2 class="m-0 text-center text-success">Total Nilai <b>100</b></h2>');
                        $('#action').val('Edit');
                        $('#modalConfig').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('raport-trash_setting_nilai') }}",
                        type: "POST",
                        data: {
                            id,
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_config').html(data.config);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                });
            });
        });

        
        function findTotal() {
            let uh = $("#nilai_uh_rata").val() == '' ? 0 : $("#nilai_uh_rata").val();
            let uts = $("#nilai_uts").val() == '' ? 0 : $("#nilai_uts").val();
            let uas = $("#nilai_uas").val() == '' ? 0 : $("#nilai_uas").val();
            let tot = parseInt(uh) + parseInt(uts) + parseInt(uas);
            let data = '';
            if (tot == 100) {
                data = '<h2 class="m-0 text-center text-success">Total Nilai <b>' + tot + '</b></h2>';
            } else {
                data = '<h2 class="m-0 text-center text-danger">Total Nilai <b>' + tot + '</b></h2>';
            }
            $("#total").html(data);
        }
    </script>
@endsection
