<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    @if (Session::has('message'))
        <script>
            swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
                '{{ session('message')['icon'] }}');
        </script>
    @endif
    <style>
        .pace {
            display: none;
        }

        .head {
            height: 115px;
            background-image: url('https://pixy.org/src/476/4764112.jpg');
        }

        @media only screen and (max-width: 768px) {

            table {
                position: absolute;
            }

            .kembali {
                top: 10px;
                left: 17px !important;
            }
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <main>
        <header class="head pt-4">
            <a class="kembali text-white" href="{{ url('program/absensi') }}"
                style="position: absolute; left: 30px;"><i class="fas fa-arrow-left fa-2x"></i></a>

        </header>
        <div class="container-fluid">
            <div class="main-content" style="margin-top: -70px">
                <div class="container-fluid mx-auto" style="max-width: 80%;">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="content bg-white py-4 px-5 mb-4">
                                <div class="alert alert-icon alert-{{ $informasi['status'] == true ? 'success' : 'danger' }} border-{{ $informasi['status'] == true ? 'success' : 'danger' }} alert-dismissible fade show"
                                    role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close"><span aria-hidden="true">×</span>
                                    </button> <i class="material-icons list-icon">info</i> <strong>{{ $informasi['message'] }}</strong>
                                </div>
                                <div class="row">

                                    <div class="col-md-5" id="baseCamera">
                                        @if ($informasi['status'] != true)
                                            <img src="{{ asset('images/camera.jpg') }}" alt="">
                                        @else
                                            <video id="preview" class="w-100" style="height: 231px;"></video>
                                        @endif
                                    </div>
                                    <div class="col-md-7 d-flex align-items-center">
                                        <div class="informasi w-100">
                                            <h3 class="text-danger"><i class="fas fa-exclamation-circle"></i>
                                                Informasi!!</h3>
                                            <div class="d-flex justify-content-start">
                                                <div class="d-flex align-items-center">
                                                    <i class="fas fa-qrcode fa-3x"></i>
                                                </div>
                                                <div class="ml-2">
                                                    <p class="my-0"> 1. Scan barcode anda disamping</p>
                                                    <p class="my-0"> 2. Sistem akan memproses</p>
                                                    <p class="my-0"> 3. Informasi sukses akan tampil bila
                                                        absensi anda berhasil</p>
                                                    <p class="my-0"> 4. Camera tidak akan terbuka bila selisih lebih 1 jam dari jadwal absen</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <section id="content" class="current">
                                    <div class="container text-center">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <h2 class="m-0">E ABSENSI</h2>
                                                <h2 class="mt-0"
                                                    style="font-size: 60px;line-height: 60px;margin-bottom: 20px;font-weight: 900;">
                                                    Kemudahan <span class="highlight">Dalam ABSEN</span></h2>
                                                <p>Fitur Lengkap dan masih terus berkembang.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-offset-1 col-sm-12 col-md-12 col-lg-12">
                                                <div class="features-list">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-6 col-lg-6 my-3">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-3">
                                                                    <i class="fab fa-android fa-3x text-green"></i>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="text-left"><b>Responsive Mobile</b>
                                                                        <p>Dengan kemudahan nya responsice mobile,
                                                                            mobile F1, mobile lamborgini dan mobil lain
                                                                            lain</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-6 my-3">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-3">
                                                                    <i
                                                                        class="fas fa-hand-holding-medical fa-3x text-info"></i>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="text-left"><b>Perijinan Mudah</b>
                                                                        <p>Proses Perizinan bila sakit ataupun tidak
                                                                            masuk lebih mudah. tak perlu nitip surat ke
                                                                            teman dll</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-6 my-3">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-3">
                                                                    <i
                                                                        class="fas fa-file-signature fa-3x text-purple"></i>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="text-left"><b>Rekapan Lengkap</b>
                                                                        <p> Lorem ipsum dolor sit amet, consectetur
                                                                            adipiscing elit. Pellentesque habitant morbi
                                                                            tristique senectus et netus et malesuada
                                                                            fames </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-6 my-3">
                                                            <div class="row d-flex align-items-center">
                                                                <div class="col-md-3">
                                                                    <i class="fas fa-stopwatch fa-3x text-facebook"></i>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="text-left"><b>Settingan Waktu &
                                                                            Libur</b>
                                                                        <p> Lorem ipsum dolor sit amet, consectetur
                                                                            adipiscing elit. Pellentesque habitant morbi
                                                                            tristique senectus et netus et malesuada
                                                                            fames </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="content login-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
            </div>
        </div>
    </main>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let scanner = new Instascan.Scanner({
                video: document.getElementById('preview')
            });
            scanner.addListener('scan', function(content) {
                $.ajax({
                    url: "{{ route('absensi-save_absen') }}",
                    method: "POST",
                    data: {
                        kode: content
                    },
                    // beforeSend: function() {
                    //     $(loader).html(
                    //         '<i class="fa fa-spin fa-spinner"></i>');
                    // },
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            scanner.stop();
                            $("#baseCamera").html(
                                '<div class="w-100" style="display: flex; height: 231px;"><div id="icon" class="position-absolute top-50 start-50 translate-middle m-auto"><a href="" class="loader text-info"><i class="fas fa-redo-alt fa-7x"></i></a></div></div>'
                            );
                        }
                        $('.informasi').html(data.profile);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });



                // scanner.stop()
            });
            Instascan.Camera.getCameras().then(function(cameras) {
                if (cameras.length > 0) {
                    scanner.start(cameras[0]);
                } else {
                    console.error('No cameras found.');
                }
            }).catch(function(e) {
                console.error(e);
            });

            $(document).on('click', '.loader', function() {
                $('.loader').html(
                    '<i class="fas fa-spin fa-redo-alt fa-7x"></i>');
            });
        });
    </script>
    <script src="{{ asset('asset/js/service_worker.js') }}" defer></script>
</body>

</html>
