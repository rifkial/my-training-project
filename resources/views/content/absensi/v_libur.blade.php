@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .dataTables_wrapper .dataTables_length {
            margin: 0 !important;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="box-title">Jadwal Libur</h5>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success pull-right mx-1" id="addLibur"><i class="fas fa-plus-circle"></i>
                                Tambah</button>
                            <button class="btn btn-info pull-right mx-1" id="listLibur"><i class="fas fa-list"></i>
                                Libur Nasional</button>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal</th>
                                    <th>Informasi</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalLibur" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formLibur" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_libur">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tanggal Libur</label>
                                    <div class="col-sm-12">
                                        <input type="date" class="form-control" id="tgl_libur" name="tgl_libur" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" class="form-control" rows="3"
                                            id="keterangan"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalList" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-purple">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleLibur">Daftar Perayaan Hari</h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <input type="hidden" name="id" id="id_libur">
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn btn-facebook pull-right mx-1" id="syncronLibur"><i
                                    class="fas fa-sync"></i>
                                Syncronkan Data</button>
                            <form id="formBulan" class="pull-right">
                                <div class="form-row align-items-center">
                                    <div class="col-auto">
                                        <select name="bulan" id="bulan" class="form-control mb-2">
                                            <option value="">Pilih Bulan..</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <select name="tahun" id="tahun" class="form-control mb-2">
                                            <option value="">Pilih Tahun</option>
                                            @for ($i = date('Y') - 4; $i <= date('Y') + 3; $i++)
                                                <option value="{{ $i }}"
                                                    {{ $i == date('Y') ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>

                                    <div class="col-auto">
                                        <button type="submit" id="btnFilter" class="btn btn-purple mb-2">Filter</button>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Nama</th>
                                            <th>Status Libur</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_libur">
                                        @if (!empty($libur))
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($libur as $lb)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ (new \App\Helpers\Help())->getDay($lb['holiday_date']) }}</td>
                                                    <td>{{ $lb['holiday_name'] }}</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                {{ $lb['is_national_holiday'] == true ? 'checked' : '' }}
                                                                disabled>
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('absensi-libur') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '#addLibur', function() {
                $('#formLibur').trigger("reset");
                $('#modelHeading').html("Tambah Hari Libur");
                $('#modalLibur').modal('show');
                $('#action').val('Add');
            });

            $(document).on('click', '#listLibur', function() {
                $('#modalList').modal('show');
            });

            $(document).on('click', '#syncronLibur', function() {
                let tahun = $('#tahun').val();
                if (tahun == '') {
                    alert("tahun tidak boleh kosong");
                } else {
                    if (confirm(
                            'Data dengan pilihan tahun tersebut akan terganti. Apa kamu ingin melanjutkan'
                        )) {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('absensi-libur_syncrone') }}",
                            data: {
                                tahun
                            },
                            beforeSend: function() {
                                $('#syncronLibur').html(
                                    '<i class="fas fa-spin fa-sync"></i> Proses Syncronisasi..');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#data-tabel').dataTable().fnDraw(false);
                                }
                                $('#syncronLibur').html('<i class="fas fa-sync"></i> Syncronkan Data');
                                noti(data.icon, data.message);
                                
                            }
                        });
                    }
                }
            });

            $('#formLibur').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('absensi-libur_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('absensi-libur_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#modalLibur').modal('hide');
                            $('#formLibur').trigger("reset");
                            $('#data-tabel').dataTable().fnDraw(false);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formBulan').on('submit', function(event) {
                event.preventDefault();
                $("#btnFilter").html(
                    'Memfilter..');
                $("#btnFilter").attr("disabled", true);

                $.ajax({
                    url: "{{ route('absensi-libur_filter') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#data_libur').html(data);
                        $('#btnFilter').html('Filter');
                        $("#btnFilter").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-libur_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Data Libur");
                        $('#id_libur').val(data.id);
                        $('#nama').val(data.nama);
                        $('#tgl_libur').val(data.tgl_libur);
                        $('#keterangan').val(data.keterangan);
                        $('#action').val('Edit');
                        $('#modalLibur').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-libur_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })
    </script>
@endsection
