@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Menu Access User</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">

                        <table class="table table-responsive-md">
                            <thead>
                                <tr>

                                    <th rowspan="2">Menu</th>

                                    <th>Administrator</th>
                                    <th>Wali Kelas</th>
                                    <th>Guru</th>
                                    <th>Sub</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td class="role" id=""><span class="badge badge-danger badge-rounded">Kelola
                                            Menu</span></td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="2"
                                                role="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" status="on" class="toggle" menu="2" role="2">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="2"
                                                role="3">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td> <button idmenu="2" data-toggle="modal" data-target="#modal2"
                                            class="btn-sm btn btn-secondary submenu"> <i
                                                class="flaticon-381-controls-3"></i> Sub Menu</button></td>
                                </tr>
                                <tr>

                                    <td class="role" id=""><span class="badge badge-danger badge-rounded">Kelola
                                            Absensi</span></td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="3"
                                                role="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="3"
                                                role="2">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="3"
                                                role="3">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td> <button idmenu="3" data-toggle="modal" data-target="#modal3"
                                            class="btn-sm btn btn-secondary submenu"> <i
                                                class="flaticon-381-controls-3"></i> Sub Menu</button></td>
                                </tr>
                                <tr>

                                    <td class="role" id=""><span
                                            class="badge badge-danger badge-rounded">Siswa</span></td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="4"
                                                role="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" status="on" class="toggle" menu="4" role="2">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="4"
                                                role="3">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td> <button idmenu="4" data-toggle="modal" data-target="#modal4"
                                            class="btn-sm btn btn-secondary submenu"> <i
                                                class="flaticon-381-controls-3"></i> Sub Menu</button></td>
                                </tr>
                                <tr>

                                    <td class="role" id=""><span
                                            class="badge badge-danger badge-rounded">Users</span></td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="5"
                                                role="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="5"
                                                role="2">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="5"
                                                role="3">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td> <button idmenu="5" data-toggle="modal" data-target="#modal5"
                                            class="btn-sm btn btn-secondary submenu"> <i
                                                class="flaticon-381-controls-3"></i> Sub Menu</button></td>
                                </tr>
                                <tr>

                                    <td class="role" id=""><span
                                            class="badge badge-danger badge-rounded">Tambahan</span></td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" checked status="off" class="toggle" menu="12"
                                                role="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" status="on" class="toggle" menu="12" role="2">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>

                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" status="on" class="toggle" menu="12" role="3">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td> <button idmenu="12" data-toggle="modal" data-target="#modal12"
                                            class="btn-sm btn btn-secondary submenu"> <i
                                                class="flaticon-381-controls-3"></i> Sub Menu</button></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })
    </script>
@endsection
