<!DOCTYPE html>
<html>

<head>
    <title>{{ ucfirst(session('title')) }}</title>
    <style type="text/css">
        .table {
            border-collapse: collapse;
            border: solid 1px #999;
            width: 100%
        }

        @page {
            size: 38cm 22cm;
            margin-left: 0;
            margin-right: 0;
        }

        .table tr td,
        .table tr th {
            border: solid 1px #999;
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        td.fitwidth {
            width: 1px;
            white-space: nowrap;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        .dontsplit {
            /* page-break-inside: avoid; */
            /* page-break-inside: auto; */
            page-break-before: always;
        }

        .gantiAuto {
            /* page-break-inside: avoid; */
            page-break-inside: auto;
            /* page-break-before: always; */
        }

    </style>

</head>

<body>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">DAFTAR KEHADIRAN
                    {{ $waktu['role'] == 'guru' ? 'GURU' : 'SISWA' }} <br>
                    {{ $waktu['role'] == 'siswa' ? 'KELAS ' . $waktu['rombel'] : '' }} <br>
                    BULAN {{ strtoupper($waktu['bulan']) }} TAHUN {{ $waktu['tahun'] }}
                </td>
            </tr>
            <tr>
                <td style="height: 10px"></td>
            </tr>
            <tr>
                <td colspan="3">

                    <table style="width: 100%; border: 1px solid; border-collapse: collapse;">
                        <thead>
                            <tr style="background: #f2f4f8">
                                <th rowspan="2" style="border: 1px solid; border-collapse: collapse;">NO</th>
                                <th rowspan="2" style="border: 1px solid; border-collapse: collapse;">INFORMASI</th>
                                @php
                                    $tanggal = cal_days_in_month(CAL_GREGORIAN, $waktu['no_bulan'], $waktu['tahun']);
                                @endphp
                                <th colspan="{{ $tanggal }}" style="border: 1px solid; border-collapse: collapse;">
                                    TANGGAL</th>
                            </tr>
                            <tr style="background: #f2f4f8">
                                @for ($i = 1; $i <= $tanggal; $i++)
                                    <th style="border: 1px solid; border-collapse: collapse;">{{ $i }}</th>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($hadir))
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($hadir as $hd)
                                    <tr>
                                        <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                            {{ $no++ }}</td>
                                        <td style="border: 1px solid; border-collapse: collapse;">
                                            <b>{{ ucwords($hd['nama']) }}</b>
                                            <p style="margin: 0">
                                                {{ $waktu['role'] == 'guru' ? 'NIP. ' . $hd['nip'] : 'NIS/NISN. ' . $hd['nis'] . '/' . $hd['nisn'] }}
                                            </p>
                                        </td>
                                        @foreach ($hd['kehadiran'] as $kd)
                                            @if ($kd['absensi'] == null)
                                                <td
                                                    style="text-align: center; border: 1px solid; border-collapse: collapse; vertical-align: middle; background: orange">
                                                    Libur</td>
                                            @else
                                                @php
                                                    if ($kd['absensi'] == 'izin') {
                                                        $kelas_absen = 'blue';
                                                    } elseif ($kd['absensi'] == 'alpha') {
                                                        $kelas_absen = 'red';
                                                    } elseif ($kd['absensi'] == 'sakit') {
                                                        $kelas_absen = 'yellow';
                                                    }else{
                                                        $kelas_absen = 'white';
                                                    }
                                                @endphp


                                                <td
                                                    style="text-align: center; border: 1px solid; border-collapse: collapse; vertical-align: middle; background: {{ $kelas_absen }}">
                                                    {{ ucwords($kd['absensi']) }}</td>
                                            @endif
                                        @endforeach

                                    </tr>
                                @endforeach
                                {{-- <tr style="background: #f2f4f8">
                                    <td colspan="3"
                                        style="text-align: center; border: 1px solid; border-collapse: collapse;">Jumlah
                                    </td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                        {{ $total_hadir }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                        {{ $total_sakit }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                        {{ $total_ijin }}</td>
                                    <td style="text-align: center; border: 1px solid; border-collapse: collapse;">
                                        {{ $total_alfa }}</td>
                                </tr> --}}
                            @else
                                <tr>
                                    <td colspan="{{ $tanggal + 2 }}" style="text-align: center;">Saat ini data tidak
                                        tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
