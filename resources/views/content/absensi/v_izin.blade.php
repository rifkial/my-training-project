@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        @media (min-width: 992px) {
            #modalIzin .modal-lg {
                max-width: 1000px !important;
            }
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="card">
                <div class="card-header bg-info p-1 d-flex justify-content-between">
                    <h2 class="box-title m-1 text-white"><strong>Cari Izin</strong></h2>
                    <div class="pamit">
                        <button class="btn btn-success my-1" id="btnIzin"><i class="fas fa-calendar-day"></i> Pengajuan Hari
                            ini</button>
                        <a href="{{ route('absensi-izin_sstatistic_siswa') }}" class="btn btn-purple my-1"><i
                                class="fas fa-chart-bar"></i> Statistik Izin</a>

                    </div>
                </div>
                <div class="card-body">
                    <form id="formFilter" class="form-horizontal">
                        <div class="form-row">
                            <input type="hidden" name="based" value="siswa">
                            <div class="form-group col-md-4">
                                <label class="control-label">Kelas </label>
                                <div class="controls">
                                    <select name="rombel" id="rombel" required="true" class="form-control">
                                        <option value="" disabled="" selected="">Pilih rombel..</option>
                                        @foreach ($rombel as $rm)
                                            <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label">Bulan</label>
                                <div class="controls">
                                    <select name="bulan" required="true" class="form-control">
                                        <option value="" disabled="" selected="">Pilih bulan..</option>
                                        <option value="01" {{ date('m') == '01' ? 'selected' : '' }}>Januari</option>
                                        <option value="02" {{ date('m') == '02' ? 'selected' : '' }}>Februari</option>
                                        <option value="03" {{ date('m') == '03' ? 'selected' : '' }}>Maret</option>
                                        <option value="04" {{ date('m') == '04' ? 'selected' : '' }}>April</option>
                                        <option value="05" {{ date('m') == '05' ? 'selected' : '' }}>Mei</option>
                                        <option value="06" {{ date('m') == '06' ? 'selected' : '' }}>Juni</option>
                                        <option value="07" {{ date('m') == '07' ? 'selected' : '' }}>Juli</option>
                                        <option value="08" {{ date('m') == '08' ? 'selected' : '' }}>Agustus</option>
                                        <option value="09" {{ date('m') == '09' ? 'selected' : '' }}>September</option>
                                        <option value="10" {{ date('m') == '10' ? 'selected' : '' }}>Oktober</option>
                                        <option value="11" {{ date('m') == '11' ? 'selected' : '' }}>November</option>
                                        <option value="12" {{ date('m') == '12' ? 'selected' : '' }}>Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label class="control-label">Tahun</label>
                                <div class="controls">
                                    <select name="tahun" required="true" class="form-control">
                                        <option value="" disabled>Pilih Tahun</option>
                                        @php
                                            $firstYear = (int) date('Y');
                                            $lastYear = $firstYear - 20;
                                            for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                echo '<option value=' . $year . '>' . $year . '</option>';
                                            }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="btnCari" class="btn-block btn btn-info">
                                <i class="fas fa-search"></i> Cari Izin</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info p-1">
                    <h2 class="box-title m-1 text-white">Rekapan Data Izin Siswa</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive recentOrderTable">
                        <table class="table verticle-middle table-responsive-md table-hover">
                            <thead>
                                <tr>

                                    <th scope="col">Informasi</th>
                                    <th scope="col">tanggal</th>
                                    <th scope="col">Alasan</th>
                                    <th scope="col">Status Konfirmasi</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="data_izin">
                                <tr>
                                    <td colspan="5" class="text-center">Harap Filter data terlebih dahulu</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalMaps" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nama</th>
                                    <td id="nama"></td>
                                    <td></td>
                                    <th>Tanggal Absensi</th>
                                    <td id="tgl_kehadiran"></td>
                                </tr>
                                <tr>
                                    <th>Kode Akun</th>
                                    <td id="kode_akun"></td>
                                    <td></td>
                                    <th>Status</th>
                                    <td id="status_kehadiran"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12">

                            <div id="pre_map" class="w-100" style="height: 300px"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                        value="create">Simpan</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalIzin" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleIzin">Daftar Izin Hari ini</h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Profile</th>
                                            <th>Status</th>
                                            <th>Kelas</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_izin_hari_ini">
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>


    <script type="text/javascript">
        var lat;
        var lon;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.izin_check', function() {
                let id = $(this).data('id');
                let value = 1;
                let loader = $(this);
                if (confirm('Apa kamu yakin ingin mengkonfirmasi, perizinan ini?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-izin_update_status') }}",
                        data: {
                            id,
                            value,
                            base : 'siswa'
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_izin_hari_ini').html(data.izin);
                            }
                            swa(data.status + "!", data.message, data.icon);

                        }
                    });
                }
            });

            $(document).on('click', '.batal', function() {
                let id = $(this).data('id');
                let value = 0;
                let loader = $(this);
                if (confirm('Apa kamu yakin ingin membatalkan, proses perizinan ini?')) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('absensi-izin_update_status') }}",
                        data: {
                            id,
                            value,
                            base : 'siswa'
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_izin_hari_ini').html(data.izin);
                            }
                            swa(data.status + "!", data.message, data.icon);

                        }
                    });
                }

            });

            $(document).on('click', '.reject', function() {
                let id = $(this).data('id');
                let value = 2;
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menolak pengajuan izin ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Tolak!',
                    cancelButtonText: 'Tidak, Batalkan!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-izin_update_status') }}",
                        type: "POST",
                        data: {
                            id,
                            value,
                            base : 'siswa'
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_izin_hari_ini').html(data.izin);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('#formFilter').on('submit', function(event) {
                event.preventDefault();
                $("#btnCari").html(
                    '<i class="fa fa-spin fa-spinner"></i> Mencari..');
                $("#btnCari").attr("disabled", true);
                $.ajax({
                    url: "{{ route('absensi-izin_rekap_filter') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#data_izin').html(data);
                        $('#btnCari').html('<i class="fas fa-search"></i> Cari Izin');
                        $("#btnCari").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.lihatMaps', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-izin_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        lat = data.lat;
                        lon = data.long;
                        show_map(lat, lon);
                        $(loader).html('<i class="fas fa-location-arrow"></i>');
                        $('#modelHeading').html("Lokasi Absensi Siswa");
                        $('#nama').html(data.nama);
                        $('#kode_akun').html(data.kode_akun);
                        $('#status_kehadiran').html(data.status_kehadiran);
                        $('#tgl_kehadiran').html(data.tgl_kehadiran);
                        $('#modalMaps').modal('show');
                    }
                });
            });

            $(document).on('click', '#btnIzin', function() {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-izin_hari_ini') }}",
                    data: {
                        base : 'siswa'
                    },
                    beforeSend: function() {
                        $('#btnIzin').html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading..');
                    },
                    success: function(data) {
                        $('#data_izin_hari_ini').html(data);
                        $('#btnIzin').html(
                            '<i class="fas fa-calendar-day"></i> Pengajuan Hari ini');
                        $('#modalIzin').modal('show');
                    }
                });
            });

        })

        function show_map(lat, lon) {
            document.getElementById('pre_map').innerHTML = "<div id='map' style='width: 100%; height: 100%;'></div>";
            var mymap = new L.map('map');
            mymap.setView([lat, lon], 13);
            L.tileLayer(
                'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token={{ env('TOKEN_LEAFLET') }}', {
                    maxZoom: 20,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    id: 'mapbox.streets'
                }).addTo(mymap);
            L.marker([lat, lon]).addTo(mymap);
            var popup = L.popup();

            function onMapClick(e) {
                popup
                    .setLatLng(e.latlng)
                    .setContent("You clicked the map at " + e.latlng.toString())
                    .openOn(mymap);
            }
            mymap.on('click', onMapClick);
        }
    </script>
@endsection
