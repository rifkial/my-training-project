@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Sub Menu Management</h4>
                </div>
                <div class="card-body">
                    <button type="submit" class="btn-sm btn btn-primary " data-toggle="modal" data-target="#tambah"> <i
                            class="flaticon-381-add-2"> </i> Tambah</button>
                    <div class="table-responsive">

                        <table class="table table-responsive-md">
                            <thead>
                                <tr>
                                    <th class="width80">ID</th>
                                    <th>Menu</th>
                                    <th>Title</th>
                                    <th>Url</th>
                                    <th>is_active</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><span class="badge light badge-success">Kelola Absensi</span></td>
                                    <td>Atur libur</td>
                                    <td><span class="badge light badge-primary">absen/libur</span></td>
                                    <td>1</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-success light sharp"
                                                data-toggle="dropdown">
                                                <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <circle fill="#000000" cx="5" cy="12" r="2" />
                                                        <circle fill="#000000" cx="12" cy="12" r="2" />
                                                        <circle fill="#000000" cx="19" cy="12" r="2" />
                                                    </g>
                                                </svg>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item"
                                                    href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/MQ==">Delete</a>
                                                <button class="dropdown-item" data-toggle="modal"
                                                    data-target="#ubah1">Edit</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tr>
                                <td>3</td>
                                <td><span class="badge light badge-success">Kelola Absensi</span></td>
                                <td>Data Absensi</td>
                                <td><span class="badge light badge-primary">absen</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/Mw==">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah3">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>4</td>
                                <td><span class="badge light badge-success">Users</span></td>
                                <td>Data User</td>
                                <td><span class="badge light badge-primary">user</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NA==">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah4">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>6</td>
                                <td><span class="badge light badge-success">Siswa</span></td>
                                <td>Data seluruh siswa</td>
                                <td><span class="badge light badge-primary">siswa</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/Ng==">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah6">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>30</td>
                                <td><span class="badge light badge-success">Kelola Menu</span></td>
                                <td>Acces User</td>
                                <td><span class="badge light badge-primary">Menu/Access</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/MzA=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah30">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>36</td>
                                <td><span class="badge light badge-success">Kelola Menu</span></td>
                                <td>Menu</td>
                                <td><span class="badge light badge-primary">menu/management</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/MzY=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah36">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>37</td>
                                <td><span class="badge light badge-success">Siswa</span></td>
                                <td>Kelas & Jurusan</td>
                                <td><span class="badge light badge-primary">siswa/kelas</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/Mzc=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah37">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>39</td>
                                <td><span class="badge light badge-success">Kelola Absensi</span></td>
                                <td>Rekap Absen</td>
                                <td><span class="badge light badge-primary">absen/rekap</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/Mzk=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah39">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>41</td>
                                <td><span class="badge light badge-success">Kelola Absensi</span></td>
                                <td>Jam absen</td>
                                <td><span class="badge light badge-primary">absen/jam</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDE=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah41">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>43</td>
                                <td><span class="badge light badge-success">Siswa</span></td>
                                <td>Siswa per kelas</td>
                                <td><span class="badge light badge-primary">siswa/daftar_kelas</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDM=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah43">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>44</td>
                                <td><span class="badge light badge-success">Siswa</span></td>
                                <td>Data Izin</td>
                                <td><span class="badge light badge-primary">izin</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDQ=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah44">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>46</td>
                                <td><span class="badge light badge-success">Kelola Menu</span></td>
                                <td>Sub menu</td>
                                <td><span class="badge light badge-primary">menu/submanagement</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDY=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah46">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>47</td>
                                <td><span class="badge light badge-success">Users</span></td>
                                <td>Data User Siswa</td>
                                <td><span class="badge light badge-primary">user/user_siswa</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDc=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah47">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                                <td>48</td>
                                <td><span class="badge light badge-success">Tambahan</span></td>
                                <td>Sub Menu Tambahan</td>
                                <td><span class="badge light badge-primary">sub</span></td>
                                <td>1</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-success light sharp" data-toggle="dropdown">
                                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <circle fill="#000000" cx="5" cy="12" r="2" />
                                                    <circle fill="#000000" cx="12" cy="12" r="2" />
                                                    <circle fill="#000000" cx="19" cy="12" r="2" />
                                                </g>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="https://Presentsiswa.mcrosss.com/menu/hapussubmenu/NDg=">Delete</a>
                                            <button class="dropdown-item" data-toggle="modal"
                                                data-target="#ubah48">Edit</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })
    </script>
@endsection
