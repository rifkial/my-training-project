@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

        @media (min-width: 992px) {
            #modalIzin .modal-lg {
                max-width: 1000px !important;
            }
        }

        .table {
            display: block !important;
            overflow-x: auto !important;
            width: 100% !important;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            <a href="{{ route('absensi-hadir_guru') }}" class="btn btn-purple"><i class="fas fa-user-tie"></i></a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="card">
                <div class="card-header bg-info p-1 d-flex justify-content-between">
                    <h2 class="box-title m-1 text-white"><strong>Filter Kehadiran Siswa</strong></h2>
                </div>
                <div class="card-body">
                    <form class="form-inline justify-content-center" id="filter_siswa">
                        <input type="hidden" name="based" value="siswa">
                        <label class="my-1 mx-2" for="inlineFormCustomSelectPref">Rombel</label>
                        <select class="form-control my-1 mr-sm-2" name="rombel">
                            <option value="" selected="">Pilih rombel..</option>
                            @foreach ($rombel as $rmb)
                                <option value="{{ $rmb['id'] }}">{{ $rmb['nama'] }}</option>
                            @endforeach
                        </select>
                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Bulan</label>
                        <select class="form-control my-1 mr-sm-2" name="bulan">
                            <option value="" disabled="" selected="">Pilih bulan..</option>
                            <option value="01" {{ date('m') == '01' ? 'selected' : '' }}>Januari</option>
                            <option value="02" {{ date('m') == '02' ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ date('m') == '03' ? 'selected' : '' }}>Maret</option>
                            <option value="04" {{ date('m') == '04' ? 'selected' : '' }}>April</option>
                            <option value="05" {{ date('m') == '05' ? 'selected' : '' }}>Mei</option>
                            <option value="06" {{ date('m') == '06' ? 'selected' : '' }}>Juni</option>
                            <option value="07" {{ date('m') == '07' ? 'selected' : '' }}>Juli</option>
                            <option value="08" {{ date('m') == '08' ? 'selected' : '' }}>Agustus</option>
                            <option value="09" {{ date('m') == '09' ? 'selected' : '' }}>September</option>
                            <option value="10" {{ date('m') == '10' ? 'selected' : '' }}>Oktober</option>
                            <option value="11" {{ date('m') == '11' ? 'selected' : '' }}>November</option>
                            <option value="12" {{ date('m') == '12' ? 'selected' : '' }}>Desember</option>
                        </select>
                        <label class="my-1 mx-2" for="inlineFormCustomSelectPref">Tahun</label>
                        <select class="form-control my-1 mr-sm-2" name="tahun">
                            <option value="" disabled>Pilih Tahun</option>
                            @php
                                $firstYear = (int) date('Y');
                                $lastYear = $firstYear - 20;
                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                }
                            @endphp
                        </select>
                        <button type="submit" class="btn btn-info my-1" id="btnSearch"><i
                                class="fas fa-search"></i></button>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-body" id="data_absen">
                        <h3 class="m-0 text-center">DAFTAR KEHADIRAN</h3>
                        <div class="table-responsive recentOrderTable">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" rowspan="2" class="vertical-middle">No</th>
                                        <th scope="col" rowspan="2" class="vertical-middle">Informasi</th>
                                        @php
                                            $tanggal = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
                                        @endphp
                                        <th colspan="{{ $tanggal }}" class="text-center">Tanggal</th>
                                    </tr>
                                    <tr>
                                        @for ($i = 1; $i <= $tanggal; $i++)
                                            <th width="30px">{{ $i }}</th>
                                        @endfor
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="{{ $tanggal + 2 }}" class="text-center">Harap Filter data terlebih
                                            dahulu</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.izin_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('absensi-izin_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.reject', function() {
                let id = $(this).data('id');
                let value = 2;
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menolak pengajuan izin ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('absensi-izin_update_status') }}",
                        type: "POST",
                        data: {
                            id,
                            value
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_izin_hari_ini').html(data.izin);
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $('#filter_siswa').on('submit', function(event) {
                event.preventDefault();
                $("#btnSearch").html(
                    '<i class="fa fa-spin fa-spinner"></i>');
                $("#btnSearch").attr("disabled", true);
                $.ajax({
                    url: "{{ route('absensi-hadir_fil_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#data_absen').html(data);
                        $('#btnSearch').html('<i class="fas fa-search"></i>');
                        $("#btnSearch").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSearch').html('<i class="fas fa-search"></i>');
                        $("#btnSearch").attr("disabled", false);
                    }
                });
            });




        })
    </script>
@endsection
