@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        #delete_record {
            color: #fff;
            background-color: #3b5998;
            border-color: #3b5998;
            -webkit-box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
            box-shadow: inset 0 1px 0 rgb(255 255 255 / 15%), 0 1px 1px rgb(0 0 0 / 8%);
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NISN</th>
                        <th>NIS</th>
                        <th>NIK</th>
                        <th>Rombel</th>
                        <th>Jurusan</th>
                        <th>STATUS</th>
                        <th>Check All <input type="checkbox" class='checkall' id='checkall'></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    text: 'Gabungkan siswa yang dipilih',
                    attr: {
                        title: 'Gabungkan semua',
                        id: 'delete_record'
                    }
                }],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'nis',
                        name: 'nis'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'rombel',
                        name: 'rombel'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'select_orders',
                        name: 'select_orders',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                ]
            });

            $('#checkall').click(function() {
                if ($(this).is(':checked')) {
                    $('.delete_check').prop('checked', true);
                } else {
                    $('.delete_check').prop('checked', false);
                }
            });

            // Delete record
            $('#delete_record').click(function() {
                var deleteids_arr = [];
                $("input:checkbox[class=delete_check]:checked").each(function() {
                    deleteids_arr.push($(this).val());
                });

                if (deleteids_arr.length > 0) {
                    var confirmdelete = confirm("Do you really want to Delete records?");
                    if (confirmdelete == true) {
                        $.ajax({
                            url: "{{ route('store-data_siswa') }}",
                            type: 'post',
                            data: {
                                kotak: 2,
                                deleteids_arr: deleteids_arr
                            },
                            success: function(response) {
                                var oTable = $('#data-tabel').dataTable();
                                oTable.fnDraw(false);
                                swal(
                                    'Berhasil!',
                                    'Siswa Berhasil ditambahkan.',
                                    'success'
                                );
                            }
                        });
                    }
                }
            });
        });

        // Checkbox checked
        function checkcheckbox() {

            // Total checkboxes
            var length = $('.delete_check').length;

            // Total checked checkboxes
            var totalchecked = 0;
            $('.delete_check').each(function() {
                if ($(this).is(':checked')) {
                    totalchecked += 1;
                }
            });

            // Checked unchecked checkbox
            if (totalchecked == length) {
                $("#checkall").prop('checked', true);
            } else {
                $('#checkall').prop('checked', false);
            }
        }

    </script>
@endsection
