@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>

    <div class="card home-profile bg-white pl-4 pr-4">
        <div class="row">
            <div class="col-md-3 img p-3">
                <img src="{{ $siswa['file'] }}" alt="{{ $siswa['nama'] }}" class="rounded-circle card p-1 mx-auto">
            </div>
            <div class="col-md-9 details p-3 mt-3">
                <a href="#" data-toggle="modal" data-target="#infoModal">
                    <span class="badge rounded-pill bg-dark float-right fw-light">
                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Know me more"></i>
                        <span class="d-none d-sm-inline">About Me</span>
                    </span>
                </a>

                <h3>{{ ucfirst($siswa['nama']) }} <sup>
                        <i class="fa fa-check-circle text-info" aria-hidden="true"></i></sup>
                </h3>
                <small><cite title="Source Title">NISN : {{ $siswa['nisn'] }}</cite></small>
                <hr>
                <div class="action-btns mt-3">
                    <div class="d-flex justify-content-between">
                        <ul class="list-inline mr-3">
                            <li class="list-inline-item mb-1"><a data-toggle="modal" data-target="#contactModal" href="#"
                                    class="btn btn-sm btn-outline-primary"><i class="fa fa-envelope"></i> <span
                                        class="">Message</span></a></li>
                  <li class=" list-inline-item"
                                        data-toggle="tooltip" data-placement="bottom" title="Help me create more"><a
                                            target="_blank" href="https://www.buymeacoffee.com/josephlariosa"
                                            class="btn btn-sm btn-outline-dark"><i class="fa fa-coffee text-warning"></i>
                                            <span class="d-none d-sm-inline">Buy me a coffee</span></a></li>
                        </ul>
                        <ul class="list-inline ">
                            <li class="list-inline-item"><a target="_blank" href="https://twitter.com/JahzDesigns"
                                    class="btn btn--twitter btn-sm d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a target="_blank" href="https://www.linkedin.com/in/jahz/"
                                    class="btn btn--linkedin btn-sm d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item"><a target="_blank" href="https://github.com/jahzlariosa"
                                    class="btn btn--github btn-sm d-block"><i class="fa fa-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7"></div>
            <div class="col-md-3">
                <div class="form-group ">
                    <select id="pertemuan" name="pertemuan" class="form-control">
                        @foreach ($pertemuan as $pt)
                            <option value="{{ $pt['pertemuan'] }}" data-id="{{ $pt['pertemuan'] }}">
                                {{ $pt['pertemuan'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <input type="button" id="filter" class="btn btn-info btn-block" value="Filter">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div style="width: 100%;">
                    <div class="table-responsive">
                        <table class="table table-striped" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Pertemuan</th>
                                    <th>Nama</th>
                                    <th>NISN</th>
                                    <th>Absensi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAbsensi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle"></h5>
                </div>
                <form id="formAbsensi" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_data_siswa">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Absensi Siswa</label>
                                    <select name="absensi" id="absensi" class="form-control">
                                        <option value="hadir">Masuk</option>
                                        <option value="alfa">Alfa</option>
                                        <option value="izin">Izin</option>
                                        <option value="terlambat">Terlambat</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        // var pertemuan = $('#pertemuan').val();
        var pertemuan = document.getElementById("pertemuan").value;
        console.log(pertemuan);
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // var selectBox = document.getElementById("pertemuan");
            // selectBox.addEventListener('change', changeFunc);
            // console.log(pertemuan);

            $('#pertemuan').on('change', function(e) {
                pertemuan = $(this).find('option:selected').val();
                table.ajax.reload().draw();
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('get_datatable_data_siswa') }}",
                    "method": "POST",
                    "data": function(d) {
                        d.pertemuan = pertemuan;
                        d.id_kelas_siswa = '{{ $id_kelas_siswa }}';
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'aksi',
                        name: 'aksi'
                    },
                ]
            });

            $('#formAbsensi').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update_absensi-data_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formAbsensi').trigger("reset");
                            $('#modalAbsensi').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#filter').on('click', function(e) {
                var tes = $(this).find(':selected').data('id')
                console.log(tes);
                table.ajax.reload().draw();
            });

        });

        function gantiAbsensi(id, absensi) {
            $('#id_data_siswa').val(id);
            $('#absensi').val(absensi);
            $('#modelTitle').html("Edit Absensi");
            $('#modalAbsensi').modal('show');
        }

        $('#pertemuan').change(function() {
            pertemuan = $(this).data('id');
            console.log(pertemuan);
        });
    </script>
@endsection
