@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>

    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ol>
            <li>Kelas Mata Pelajaran yang anda buat ditunjukkan untuk siswa yang ada di Kelas {{ $rombel['kelas'] }} -
                Rombel {{ $rombel['nama'] }}</li>
            <li>Siswa di bawah ini adalah siswa yang berada di kelas yang anda buat</li>
            <li>Anda dapat memasukkan seluruh siswa yang berada di kelas di bawah ini agar tergabung di kelas yang anda buat
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table widget-status-table table-bordered table-hover">

                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Nama</th>
                                <th>NIS</th>
                                <th>NISN</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if (!empty($siswa))
                                @foreach ($siswa as $sw)
                                    <tr data-toggle="collapse" data-target="#demo{{ $sw['id'] }}"
                                        class="accordion-toggle">
                                        <td>
                                            <button class="btn btn-default btn-xs">
                                                <span class="fa fa-info-circle text-info"></span>
                                            </button>
                                        </td>
                                        <td>{{ ucfirst($sw['nama']) }}</td>
                                        <td>{{ $sw['nis'] }}</td>
                                        <td>{{ $sw['nisn'] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="hiddenRow">
                                            <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr class="bg-info">
                                                            <th class="text-center">Pertemuan</th>
                                                            <th class="text-center">Kehadiran</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($sw['pertemuan'] as $pt)
                                                            @if ($pt['id_data_siswa'] != null)
                                                                <tr>
                                                                    <td class="text-center">{{ $pt['pertemuan'] }}</td>
                                                                    <td class="text-center">
                                                                        <a href="javasript:void(0)"
                                                                            onclick="gantiAbsensi({{ $pt['id_data_siswa'] }}, '{{ $pt['kehadiran'] }}')">
                                                                            <span
                                                                                class="badge badge-{{ $pt['kehadiran'] == 'hadir' ? 'success' : 'danger' }} text-inverse">{{ $pt['kehadiran'] != null ? $pt['kehadiran'] : 'Tidak Absensi' }}</span></a>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td colspan="2" class="text-center">Siswa Belum
                                                                        pernah mengikuti pembeljaran</td>
                                                                </tr>

                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">Data saat ini tidak tersedia</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAbsensi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelTitle"></h5>
                </div>
                <form id="formAbsensi" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_data_siswa">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Absensi Siswa</label>
                                    <select name="absensi" id="absensi" class="form-control">
                                        <option value="hadir">Masuk</option>
                                        <option value="alfa">Alfa</option>
                                        <option value="izin">Izin</option>
                                        <option value="terlambat">Terlambat</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NISN</th>
                        <th>NAMA</th>
                        <th>JENKEL</th>
                        <th>STATUS</th>
                        <th>DETAIL</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div> --}}
    {{-- <div class="clearfix"></div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="modal-badan">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-rombel" style="margin-bottom: 10px !important;">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>NISN</th>
                                            <th>NIK</th>
                                            <th>Rombel</th>
                                            <th>Jurusan</th>
                                            <th>STATUS</th>
                                            <th>Check All <input type="checkbox" class='checkall' id='checkall'></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data_rombel as $item)
                                            <tr>
                                                <th>{{ $item['nama'] }}</th>
                                                <th>{{ $item['nisn'] }}</th>
                                                <th>{{ $item['nik'] }}</th>
                                                <th>{{ $item['rombel'] }}</th>
                                                <th>{{ $item['jurusan'] }}</th>
                                                <th>{{ $item['status'] }}</th>
                                                <th><input type="checkbox" class="delete_check"
                                                        id="delcheck_{{ $item['id'] }}" onclick="checkcheckbox();"
                                                        value="{{ $item['id'] }}"></th>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="delete_record"
                        value="create">Gabungkan Siswa</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div> --}}


    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // var selectBox = document.getElementById("pertemuan");
            // selectBox.addEventListener('change', changeFunc);
            // console.log(pertemuan);

            $('#formAbsensi').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                $.ajax({
                    url: "{{ route('update_absensi-data_siswa') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#formAbsensi').trigger("reset");
                            $('#modalAbsensi').modal('hide');
                            location.reload();
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


        });

        function gantiAbsensi(id, absensi) {
            $('#id_data_siswa').val(id);
            $('#absensi').val(absensi);
            $('#modelTitle').html("Edit Absensi");
            $('#modalAbsensi').modal('show');
        }
        // $(function() {
        //     $('#data-rombel').DataTable({
        //         "columnDefs": [{
        //             "targets": 0,
        //             "orderable": false
        //         }]
        //     })

        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });

        //     var table = $('#data-tabel').DataTable({
        //         dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
        //         buttons: [{
        //                 extend: 'copyHtml5',
        //                 exportOptions: {
        //                     columns: [0, ':visible']
        //                 }
        //             },
        //             {
        //                 extend: 'print',
        //                 text: '<i class="fa fa-print"></i>',
        //                 exportOptions: {
        //                     columns: ':visible'
        //                 }
        //             },
        //             {
        //                 extend: 'excelHtml5',
        //                 text: '<i class="fa fa-file-excel-o"></i>',
        //                 exportOptions: {
        //                     columns: ':visible'
        //                 }
        //             },

        //             {
        //                 extend: 'pdfHtml5',
        //                 text: '<i class="fa fa-file-pdf-o"></i>',
        //                 exportOptions: {
        //                     columns: ':visible'
        //                 },
        //                 customize: function(doc) {
        //                     doc.content[1].table.widths =
        //                         Array(doc.content[1].table.body[0].length + 1).join('*').split('');
        //                 }
        //             },
        //             'colvis',
        //             {
        //                 text: '<i class="fa fa-refresh"></i>',
        //                 action: function(e, dt, node, config) {
        //                     dt.ajax.reload(null, false);
        //                 }
        //             }
        //         ],
        //         processing: true,
        //         serverSide: true,
        //         responsive: true,
        //         ajax: "",
        //         columns: [{
        //                 data: 'DT_RowIndex',
        //                 name: 'DT_RowIndex'
        //             },
        //             {
        //                 data: 'nisn',
        //                 name: 'nisn'
        //             },
        //             {
        //                 data: 'nama',
        //                 name: 'nama'
        //             },
        //             {
        //                 data: 'jenkel',
        //                 name: 'jenkel'
        //             },
        //             {
        //                 data: 'status',
        //                 name: 'status'
        //             },
        //             {
        //                 data: 'action',
        //                 name: 'action'
        //             },
        //         ]
        //     });

        //     $('#createNewCustomer').click(function() {
        //         $('#modelHeading').html("Data Siswa Rombel");
        //         $('#ajaxModel').modal('show');
        //     });


        //     $('#checkall').click(function() {
        //         if ($(this).is(':checked')) {
        //             $('.delete_check').prop('checked', true);
        //         } else {
        //             $('.delete_check').prop('checked', false);
        //         }
        //     });

        //     $('#delete_record').click(function() {
        //         var deleteids_arr = [];
        //         $("input:checkbox[class=delete_check]:checked").each(function() {
        //             deleteids_arr.push($(this).val());
        //         });

        //         if (deleteids_arr.length > 0) {

        //             var confirmdelete = confirm("Do you really want to Delete records?");
        //             if (confirmdelete == true) {
        //                 $.ajax({
        //                     url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/data_siswa/store') }}",
        //                     type: 'post',
        //                     data: {
        //                         kotak: 2,
        //                         deleteids_arr: deleteids_arr
        //                     },
        //                     success: function(response) {
        //                         var oTable = $('#data-tabel')
        //                             .dataTable();
        //                         oTable.fnDraw(false);
        //                         swal(
        //                             'Berhasil!',
        //                             'Siswa Berhasil ditambahkan.',
        //                             'success'
        //                         );
        //                         $('.checkall').prop('checked', false);
        //                         $('.delete_check').prop('checked', false);
        //                         $('#ajaxModel').modal('hide');
        //                     }
        //                 });
        //             }
        //         }
        //     });
        // });

        // function checkcheckbox() {
        //     var length = $('.delete_check').length;
        //     var totalchecked = 0;
        //     $('.delete_check').each(function() {
        //         if ($(this).is(':checked')) {
        //             totalchecked += 1;
        //         }
        //     });

        //     if (totalchecked == length) {
        //         $("#checkall").prop('checked', true);
        //     } else {
        //         $('#checkall').prop('checked', false);
        //     }
        // }
    </script>
@endsection
