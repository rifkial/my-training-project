@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        @media (max-width:481px) {
            .col-lg-9 {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>

    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ol>
            <li>Kelas Mata Pelajaran yang anda buat ditunjukkan untuk siswa yang ada di Kelas {{ $rombel['kelas'] }} - Rombel {{ $rombel['nama'] }}</li>
            <li>Siswa di bawah ini adalah siswa yang berada di kelas yang anda buat</li>
            <li>Anda dapat memasukkan seluruh siswa yang berada di kelas di bawah ini agar tergabung di kelas yang anda buat
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover widget-status-table">

                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Nama Siswa</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($siswa as $sw)
                                <tr data-toggle="collapse" data-target="#demo{{ $sw['id'] }}" class="accordion-toggle">
                                    <td>
                                        <button class="btn btn-default btn-xs">
                                            <span class="fa fa-info-circle text-info"></span>
                                        </button>
                                    </td>
                                    <td>{{ ucfirst($sw['nama']) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="hiddenRow">
                                        <div class="accordian-body collapse" id="demo{{ $sw['id'] }}">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr class="bg-info">
                                                        <th class="text-center">Pertemuan</th>
                                                        <th class="text-center">Kehadiran</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($sw['pertemuan'] as $pt)
                                                        @if ($pt['id_data_siswa'] != null)
                                                            <tr>
                                                                <td class="text-center">{{ $pt['pertemuan'] }}</td>
                                                                <td class="text-center">
                                                                    <span
                                                                        class="badge badge-{{ $pt['kehadiran'] == 'hadir' ? 'success' : 'danger' }} text-inverse">{{ $pt['kehadiran'] != null ? $pt['kehadiran'] : 'Tidak Absensi' }}</span>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td colspan="2" class="text-center">Siswa Belum pernah
                                                                    mengikuti pembeljaran</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
