@extends('content.e_learning.room.v_data_room')
@section('content_room')

    <style>
        .pace {
            display: none;
        }

        .col-lg-9 {
            background: #f2f4f8 !important;
        }

        .panel-shadow {
            box-shadow: rgba(0, 0, 0, 0.3) 7px 7px 7px;
        }

        .panel-white {
            border: 1px solid #dddddd;
        }

        .panel-white .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #ddd;
        }

        .panel-white .panel-footer {
            background-color: #fff;
            border-color: #ddd;
        }

        .post .post-heading {
            height: 95px;
            padding: 20px 15px;
        }

        .post .post-heading .avatar {
            width: 60px;
            height: 60px;
            display: block;
            margin-right: 15px;
        }

        .post .post-heading .meta .title {
            margin-bottom: 0;
        }

        .post .post-heading .meta .title a {
            color: black;
        }

        .post .post-heading .meta .title a:hover {
            color: #aaaaaa;
        }

        .post .post-heading .meta .time {
            margin-top: 8px;
            color: #999;
        }

        .post .post-image .image {
            width: 100%;
            height: auto;
        }

        .post .post-description {
            padding: 15px;
        }

        .post .post-description p {
            font-size: 14px;
        }

        .post .post-description .stats {
            margin-top: 20px;
        }

        .post .post-description .stats .stat-item {
            display: inline-block;
            margin-right: 15px;
        }

        .post .post-description .stats .stat-item .icon {
            margin-right: 8px;
        }

        .post .post-footer {
            border-top: 1px solid #ddd;
            padding: 15px;
        }

        .post .post-footer .input-group-addon a {
            color: #454545;
        }

        .post .post-footer .comments-list {
            padding: 0;
            margin-top: 20px;
            list-style-type: none;
        }

        .post .post-footer .comments-list .comment {
            display: block;
            width: 100%;
            margin: 20px 0;
        }

        .post .post-footer .comments-list .comment .avatar {
            width: 35px;
            height: 35px;
        }

        .post .post-footer .comments-list .comment .comment-heading {
            display: block;
            width: 100%;
        }

        .post .post-footer .comments-list .comment .comment-heading .user {
            font-size: 14px;
            font-weight: bold;
            display: inline;
            margin-top: 0;
            margin-right: 10px;
        }

        .post .post-footer .comments-list .comment .comment-heading .time {
            font-size: 12px;
            color: #aaa;
            margin-top: 0;
            display: inline;
        }

        .post .post-footer .comments-list .comment .comment-body {
            margin-left: 50px;
        }

        .post .post-footer .comments-list .comment>.comments-list {
            margin-left: 50px;
        }

    </style>

    <div class="panel panel-white post panel-shadow" style="background: #fff">
        <div class="post-heading">
            <div class="pull-left image">
                <img src="{{ $feed['user_profil'] }}" class="img-circle avatar" alt="user profile image">
            </div>
            <div class="pull-left meta">
                <div class="title h5">
                    <a href="#"><b>{{ $feed['pengirim'] }}</b></a>
                    memposting...
                </div>
                <h6 class="text-muted time">{{ $feed['terbit'] }}</h6>
            </div>
        </div>
        <div class="post-description">
            <p>{{ $feed['berita'] }}</p>
            {{-- <div class="stats">
                <a href="#" class="btn btn-default stat-item">
                    <i class="fa fa-thumbs-up icon"></i>2
                </a>
                <a href="#" class="btn btn-default stat-item">
                    <i class="fa fa-share icon"></i>12
                </a>
            </div> --}}
        </div>
        <div class="post-footer">
            <form action="javascript:void(0)" method="post" id="productForm">
                @csrf
                <input type="hidden" name="id_feed" value="{{ $feed['id'] }}">
                <div class="input-group">
                    <input class="form-control" name="komentar" placeholder="Add a comment" type="text">
                    <button type="submit" class="input-group-addon" id="addComment">
                        <i class="fa fa-paper-plane"></i>
                    </button>
                </div>
            </form>
            <div class="koment">
                {{-- {{ dd($feed['komentar']) }} --}}
                @foreach ($feed['komentar'] as $item)

                    <ul class="comments-list">
                        <li class="comment">
                            <a class="pull-left" href="#">
                                <img class="avatar" src="{{ $item['user_profil'] }}" alt="avatar">
                            </a>
                            <div class="comment-body">
                                <div class="comment-heading">
                                    <h4 class="user">{{ $item['nama'] }}</h4>
                                    <h5 class="time">{{ $item['terbit'] }}</h5>
                                    @if ($my_feed['id_sosial'] == $item['id_sosial'])
                                        <div class="pull-right">
                                            <a href="javascript:void(0)" onclick="editData({{ $item['id'] }},'{{ $item['komentar'] }}')"><small><i class="fa fa-pencil"></i></small></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="deleteData({{ $item['id'] }})"><small><i class="fa fa-trash"></i></small></a>
                                        </div>
                                    @endif
                                </div>
                                <p class="pesan-{{ $item['id'] }}">{{ $item['komentar'] }}</p>
                            </div>
                        </li>
                    </ul>

                @endforeach
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#productForm').on('submit', function(event) {
                event.preventDefault();
                var action_url = '';
                var actionType = $('#btn-save').val();
                $('.publish').html('Sending..');
                action_url =
                    "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/store_comment') }}";
                method_url = "POST";

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $("#addComment").html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $("#productForm").trigger("reset");
                            $(".koment").load(" .koment");
                        }
                        $("#addComment").html(
                            '<i class="fa fa-paper-plane"></i>');
                        noti(data.icon, data.success);
                        // $('.publish').html('Publish');
                        // $('.koment').prepend(data).show('slow');;
                        // $('#productForm').trigger("reset"); //form reset
                        // noti('success', "Komentar berhasil ditambahkan");
                    },
                });
            });
        });

        function editData(id, berita){
            var title = prompt('Event Title:', berita);
            if (title) {
                event.title = title;
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/comment_update') }}",
                    data: {
                        id,
                        title
                    },
                    beforeSend: function() {
                        $(".pesan-" + id).html(
                            'Sedang mengupdate komentar...');
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $(".koment").load(" .koment");
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }

        function deleteData(id){
            var konfirmasi = confirm("Apa anda yakin ingin menghapus komentar ini?");
            if (konfirmasi == true) {
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/delete_comment') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".pesan-" + id).html(
                            'Sedang menghapus pesan...');
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $(".koment").load(" .koment");
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }

    </script>

@endsection
