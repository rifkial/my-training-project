@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        h3.display-3,
        h6 {
            color: #fff;
        }

        .text-white {
            height: 180px;
        }

        .card-guru {
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
        }

        .card-guru:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        button.text-success {
            background: transparent;
            width: 100%;
            height: 41px;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
        }

    </style>

    @if (Session::has('error_api'))
        <script>
            swal({
                title: 'Gagal!',
                text: "{{ session('error_api')['message'] }}",
                timer: 5000,
                type: "{{ session('error_api')['icon'] }}"
            }).then((value) => {
                //location.reload();
            }).catch(swal.noop);
        </script>
    @endif
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" rel="stylesheet"
        type="text/css">
    <div class="row">
        @foreach ($rombel as $item)
            <div class="col-md-3 mt-2">
                <div class="card card-guru">
                    <div class="card-header bg-{{ $item['status'] == 1 ? 'success' : 'primary' }} text-white">
                        <div class="row align-items-center">
                            <div class="col col-12" style="">
                                <div class="row">
                                    <div class="col-6 d-flex align-items-center">
                                        {{ $item['mapel'] }}
                                    </div>
                                    <div class="col-6">
                                        <i class="fas fa-chalkboard-teacher fa-4x pull-right"></i>
                                    </div>
                                </div>

                            </div>
                            <div class="col col-12">
                                <h2 class="text-light text-center mt4">{{ $item['rombel'] }}</h2>
                                <p class="text-light text-center">Kelas {{ $item['status'] == 1 ? 'Dibuka' : 'Berakhir' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        @if ($item['status'] == 1)
                            <div class="row">
                                <div class="col col-7">
                                    <a href="{{ url('program/e_learning/room', $item['id_encode']) }}"
                                        data-id="{{ $item['id_encode'] }}"
                                        class="masuk btn btn-outline-success btn-block">Masuk
                                        ke kelas</a>
                                </div>
                                <div class="col col-5">
                                    <a href="{{ url('program/e_learning/room/' . $item['id_encode'] . '/logout', $item['id_encode']) }}"
                                        data-id="{{ $item['id_encode'] }}"
                                        class="akhiri btn btn-default btn-block text-danger"><i
                                            class="fas fa-times-circle"></i> Akhiri</a>
                                </div>
                            </div>
                        @else
                            <a href="{{ url('program/e_learning/room', $item['id_encode']) }}"
                                data-id="{{ $item['id_encode'] }}" class="masuk btn btn-outline-primary btn-block">Masuk
                                ke kelas</a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <script>
        $(function() {
            $('.masuk, .akhiri').click(function() {
                let loader = $(this);
                $(loader).html('<i class="fa fa-spin fa-spinner"></i> Loading');
            });
        });
    </script>

@endsection
