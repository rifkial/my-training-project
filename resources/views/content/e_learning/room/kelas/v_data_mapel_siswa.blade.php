@extends('template.template_mobile.app')
@section('content')

    <style>
        .h7 {
            font-size: 0.8rem;
        }

        .gedf-wrapper {
            margin-top: 0.97rem;
        }

        @media (min-width: 992px) {
            .gedf-main {
                padding-left: 4rem;
                padding-right: 4rem;
            }

            .gedf-card {
                margin-bottom: 2.77rem;
            }
        }

        .dropdown-toggle::after {
            content: none;
            display: none;
        }

        .card {
            margin-bottom: 15px;
        }

    </style>

    <div class="section mt-2">
        <div class="row">
            @foreach ($mapel as $item)
                <div class="col-6">
                    <div class="card product-card">
                        <div class="card-body">
                            <img src="{{ asset('asset/mobile/img/sample/photo/product1.jpg') }}" class="image"
                                alt="product image">
                            <h2 class="title">{{ $item['mapel'] }}</h2>
                            <p class="text">1 kg</p>
                            <div class="price">$ 1.50</div>
                            @if ($item['room_status'] == 1)
                                <a href="{{ route('room-dashboard', $item['id_room_encode']) }}"
                                    class="btn btn-sm btn-primary btn-block">Masuk</a>
                            @else
                                <a href="#" class="btn btn-sm btn-danger btn-block">Kelas Berakhir</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
