@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

        @media (max-width:481px) {
            .col-lg-9 {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hari/Tanggal</th>
                        <th>jam</th>
                        <th>Pertemuan</th>
                        <th>Materi</th>
                        <th>Hambatan</th>
                        <th>Pemecahan</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>




    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'jam',
                        name: 'jam'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'bahan_ajar',
                        name: 'bahan_ajar'
                    },
                    {
                        data: 'hambatan',
                        name: 'hambatan'
                    },
                    {
                        data: 'pemecahan',
                        name: 'pemecahan'
                    },
                ]
            });


        });
    </script>


@endsection
