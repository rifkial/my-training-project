@if (session('media_template') == 'mobile')
    @php
        $temp = 'template/template_mobile/app';
    @endphp
@else
    @php
        $ext = '';
    @endphp
    @if ($template == 'collapsed_nav')
        @php
            $ext = '_collapsed_nav';
        @endphp
    @elseif($template == "e_commerce")
        @php
            $ext = '_e_commerce';
        @endphp
    @elseif($template == "real_state")
        @php
            $ext = '_real_state';
        @endphp
    @elseif($template == "university")
        @php
            $ext = '_university';
        @endphp
    @elseif($template == "default")
        @php
            $ext = '_default';
        @endphp
    @else
        @php
            $ext = '_horizontal_nav_icons';
        @endphp
    @endif
    @php
        $temp = 'template/template' . $ext . '/app';
    @endphp
@endif
@extends($temp)
@section('content')
    <style>
        .dataTables_wrapper .dataTables_paginate {
            margin-bottom: 11px;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #067d10 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #b70000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-collection.buttons-colvis {
            background: #d46200 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-copy.buttons-html5 {
            background: #188e83 !important;
            color: #fff !important;
        }

        button.dt-button {
            background: #000 !important;
            color: #fff !important;
        }

        button.dt-button.buttons-print {
            background: #634141 !important;
            color: #fff !important;
        }

        button#createNewCustomer {
            background: #031e80 !important;
            color: #fff !important;
        }

        div#data-tabel_length {
            padding-top: .755em;
            margin-top: 1.42857em;
        }

    </style>

    <div class="row">
        @if (empty(request()->segment(5)))
            <div class="col-lg-12" id="alert_wellcome">
                <div class="alert alert-info border-info" role="alert">
                    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                            aria-hidden="true">×</span>
                    </button>
                    <h4 class="alert-heading">Anda berhasil masuk Kelas!</h4>
                    <p>Selamat datang di kelas elearning. Kelas ini menjadikan kegiatan belajar jadi lebih mudah. dan
                        berikut
                        adalah seputar dari kelas anda</p>
                    <hr>
                    <p class="mb-0"><strong>Tentang Room</strong></p>
                    <ul class="mr-t-10">
                        <li>Mata Pelajaran <strong class="text-danger">{{ Str::ucfirst($detail['mapel']) }}</strong>
                        </li>
                        <li>Pertemuan Saat ini/yang terakhir <strong
                                class="text-danger">{{ $detail['pertemuan_terakhir'] }}</strong></li>
                        <li>Guru Pengajar <strong class="text-danger">{{ Str::upper($detail['guru']) }}</strong></li>
                        <li>Kelas <strong class="text-danger">{{ $detail['rombel'] }}</strong></li>
                        <li>Jurusan <strong class="text-danger">{{ $detail['jurusan'] }}</strong></li>
                    </ul>
                </div>
            </div>
        @endif

        @if (session('media_template') != 'mobile')
            <div class="col-lg-3">
                @include('includes.program-e_learning.room.menu-room')
            </div>
        @endif
        <div class="col-lg-9" style="background: #fff;">
            @yield('content_room')
        </div>
        {{-- {{ dd($room) }} --}}
        @if (session('media_template') == 'mobile')
            @if (session('role') == 'siswa' && $room['status_kode'] == 1)
                @if ($status['absensi'] != 'hadir')
                    <div class="bungkus">
                        <div class="fab-button text top-right">
                            <a href="javascript:void(0);" data-id="[{{ $room['id'] . ',' . $room['id_mapel'] }}]"
                                class="fab absensi">
                                <ion-icon name="alarm-outline"></ion-icon>
                                Absensi Kehadiran
                            </a>
                        </div>
                    </div>
                @endif
            @endif
        @endif
    </div>


    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        })
    </script>

@endsection
