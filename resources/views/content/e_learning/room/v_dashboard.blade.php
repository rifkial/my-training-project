@extends('content.e_learning.room.v_data_room')
@section('content_room')

    <style>
        .share.border.bg-white.p-2 {
            border: 2px solid #105d4d  !important;
            border-radius: 20px;
        }

        #loading {
            background: url("{{ asset('asset/img/load.gif') }}") no-repeat center;
            height: 300px;
            width: 100%;
            display: block;
            margin: auto;
        }

        .pace {
            display: none;
        }

        .publish {
            border-radius: 9px !important;
            background: #51d2b7;
            color: #fff;
            cursor: pointer;
            margin: 3px;
            width: 123px;
            height: 37px;
        }

        .col-lg-9 {
            background: #f2f4f8 !important;
        }

        .share-input:focus {
            outline: 0 !important;
            box-shadow: none
        }

        .share-input {
            outline: 0 !important;
            box-shadow: none
        }

        .custom-scroll-content {
            height: 60em;
        }

        .share i {
            color: rgb(159, 159, 242) !important
        }

        .share {
            cursor: pointer
        }


        .time {
            font-size: 9px !important
        }

        .socials i {
            margin-right: 33px;
            font-size: 17px;
            color: #696464;
            cursor: pointer
        }

        .feed-image img {
            width: 100%;
            height: auto
        }

        .count {
            margin-left: 2px;
        }

        .chat-right {
            background: #c6d9ec;
            width: 90%;
            height: auto;
            border-radius: 20px 20px 0 20px;
            border: 2px solid #105d4d  !important;
        }

        .chat-left {
            background: #fff;
            width: 90%;
            height: auto;
            border-radius: 20px 20px 20px 0px;
        }

    </style>

    <div class="row justify-content-center p3-5">
        <h2 class="mb-2" style="color:unset">
            Silakan Berdiskusi
        </h2>
    </div>

    <div class="feed p-2">
        <div class="share border bg-white p-2">
            <form action="javascript:void(0)" id="productForm">
                @csrf
                <div class="d-flex flex-row inputs p-2 py-4">
                    <img class="rounded-circle" src="{{ $profile['file'] }}" width="40" height="40">
                    <input type="text" name="berita" id="berita" class="border-0 form-control share-input ml-1"
                        placeholder="Share your thoughts">
                </div>
                <div class="d-flex flex-row justify-content-between">
                    <div class="d-flex flex-row publish-options">
                        {{-- <div class="align-items-center border-right p-2 share"><i
                                class="fa fa-camera text-black-50"></i><span class="ml-1">Photo</span></div>
                        <div class="align-items-center border-right p-2 share"><i
                                class="fa fa-video-camera text-black-50"></i><span class="ml-1">Video</span></div>
                        <div class="align-items-center border-right p-2 share"><i class="fa fa-file text-black-50"></i><span
                                class="ml-1">Files</span></div> --}}
                    </div>
                    <button type="submit" class="publish">Publish</button>
                </div>
            </form>
        </div>

        <div id="feeds" class="custom-scroll-content scrollbar-enabled">
            {{-- {{ dd($feed) }} --}}
            @foreach ($feed as $item)
                <div
                    class="border mt-3 chat-{{ $my_feed['id_sosial'] == $item['id_sosial'] ? 'right float-right' : 'left float-left' }}">
                    <div>
                        <div class="d-flex flex-row justify-content-between align-items-center px-2 py-1 border-bottom">
                            <div class="d-flex flex-row align-items-center feed-text px-2"><img class="rounded-circle"
                                    src="{{ $item['user_profil'] }}" width="30" height="30">
                                <div class="d-flex flex-column flex-wrap ml-2"><span
                                        class="font-weight-bold">{{ $item['pengirim'] }}</span><span
                                        class="text-black-50 time">{{ $item['terbit'] }}</span></div>
                            </div>
                            @if ($my_feed['id_sosial'] == $item['id_sosial'])
                                <div class="feed-icon px-2">
                                    <a href="javascript:void(0);" data-toggle="dropdown"
                                        class="dropdown-toggle">&nbsp;&nbsp;<i
                                            class="fa fa-ellipsis-v text-black-50"></i>&nbsp;&nbsp;</a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);"
                                            onclick="aksiEdit({{ $item['id'] }},'{{ $item['berita'] }}')"><i
                                                class="material-icons list-icon float-left mr-r-10">edit</i>
                                            Edit</a>
                                        <div role="separator" class="dropdown-divider"></div><a class="dropdown-item"
                                            href="javascript:void(0);" onclick="aksiDelete({{ $item['id'] }})"><i
                                                class="material-icons list-icon float-left mr-r-10">close</i> <strong>Delete
                                                Chat</strong></a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="p-2 px-3 pesan-{{ $item['id'] }}"><span>{{ $item['berita'] }}</span></div>
                    <div class="d-flex justify-content-end socials px-2 pb-1">
                        {{-- <i class="fa fa-thumbs-up"></i> --}}
                        <a
                            href="{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/detail', $item['id_feed_encode']) }}"><i
                                class="fa fa-comments-o"><span
                                    class="count">{{ $item['jumlah_komentar'] }}</span></a></i>
                        {{-- <i class="fa fa-share"></i> --}}
                    </div>
                    {{-- <div class="d-flex justify-content-end socials p-2 py-3"><i class="fa fa-thumbs-up"></i><a href="{{ route('room-detail_post', $item['id_feed_encode']) }}"><i class="fa fa-comments-o"><span class="count">{{ $item['jumlah_komentar'] }}</span></a></i> <i class="fa fa-share"></i></div> --}}
                </div>
            @endforeach
        </div>

        {{ $feed->links() }}
    </div>
    


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {

            $('#productForm').on('submit', function(event) {
                var berita = $('#berita').val();
                if (berita == '') {
                    alert("Mohon maaf, postingan tidak boleh kosong");
                } else {
                    event.preventDefault();
                    var action_url = '';
                    var actionType = $('#btn-save').val();
                    $('.publish').html('Sending..');
                    action_url =
                        "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/store') }}";
                    method_url = "POST";

                    $.ajax({
                        url: action_url,
                        method: method_url,
                        data: $(this).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#feeds").html(
                                '<div id="loading"></div>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#productForm').trigger("reset");
                                $("#feeds").load(" #feeds");
                            } else {
                                noti(data.icon, data.success);
                            }
                            $('.publish').html('Publish');
                        },
                    });
                }
            });
        });

        function aksiEdit(id, berita) {
            var title = prompt('Event Title:', berita);
            if (title) {
                event.title = title;
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/update') }}",
                    data: {
                        id,
                        title
                    },
                    beforeSend: function() {
                        $(".pesan-" + id).html(
                            'Sedang mengupdate pesan...');
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $("#feeds").load(" #feeds");
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }

        function aksiDelete(id) {
            if (id) {
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/postingan/soft_delete') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".pesan-" + id).html(
                            'Sedang menghapus pesan...');
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $("#feeds").load(" #feeds");
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }
    </script>
@endsection
