@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn {
            margin-top: 6px;
        }

        #label {
            background-color: #11af49;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            border-radius: 0.3rem;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    @if (session('role') == 'guru')
    @endif
    <div style="width: 100%;">
        <div class="table-responsive">
            <table id="data-tabel" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr class="bg-info">
                        <th>No</th>
                        <th>Nama</th>
                        {{-- <th>KD</th> --}}
                        <th>Keterangan</th>
                        <th>Batas Pengumpulan</th>
                        <th>File</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">

                                <input type="hidden" name="id" id="id_ketrampilan">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama Penilaian </label>
                                    <div class="col-md-9">
                                        <input type="text" name="nama" id="nama" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Kompetensi Inti </label>
                                    <div class="col-md-9">
                                        <select name="kompetensi_inti" id="kompetensi_inti" class="form-control">
                                            <option value="">Pilih kompetensi</option>
                                            @foreach ($kompetensi as $item)
                                                <option value="{{ $item['id'] }}">{{ $item['kompetensi_inti'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Kompetensi Dasar</label>
                                    <div class="col-md-9">
                                        <div id="kd_select">
                                            <small style="color: red">*Harap pilih Kompetensi Inti terlebih dahulu</small>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Keterangan</label>
                                    <div class="col-md-9">
                                        <textarea name="keterangan" id="keterangan" cols="30" rows="5"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Maksimal Batas Pengumpulan</label>
                                    <div class="col-md-5">
                                        <input type="date" name="tgl_maks_pengumpulan" id="tgl_maks_pengumpulan"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="time" name="waktu_maks_pengumpulan" id="waktu_maks_pengumpulan"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">File Tugas</label>
                                    <div class="col-md-9">
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label">Pilih File</label>
                                        <span id="file-chosen">No file chosen</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Hasil Kerja Siswa Tugas Ketrampilan</h5>
                </div>
                <div class="modal-body">
                    <span id="trd"></span>
                    <table class="table table-stripped table-bordered" id="tabel-hasil" style="width: 100%">
                        <thead>
                            <tr class="bg-info">
                                <th>#</th>
                                <th>NISN</th>
                                <th>Nama Siswa</th>
                                <th>Waktu Pengumpulan</th>
                                <th>Keterlambatan</th>
                                <th>File & Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalInstruksi" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Detail Insftruksi</h5>
                </div>
                <div class="modal-body">
                    <div id="hasil_instruksi">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        id_tugas = 0;
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'colvis',
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    }
                ],
                processing: true,
                serverSide: true,
                "responsive": true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    // {
                    //     data: 'kd',
                    //     name: 'kd'
                    // },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            var tabel_jawaban = $('#tabel-hasil').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "jawaban_ketrampilan/get_by_tugas",
                    "method": "POST",
                    "data": function(d) {
                        d.id_tugas = id_tugas;
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'siswa',
                        name: 'siswa'
                    },
                    {
                        data: 'dikumpul',
                        name: 'dikumpul'
                    },
                    {
                        data: 'terlambat',
                        name: 'terlambat'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    }
                ]
            });

            const actualBtn = document.getElementById('actual-btn');

            const fileChosen = document.getElementById('file-chosen');

            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            })

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#file-chosen').html('');
                $('#Customer_id').val('');
                $('.select2').val(null).trigger('change');
                $('#kd_select').html(
                    '<small style="color: red">*Harap pilih Kompetensi Inti terlebih dahulu</small>');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Tugas Ketrampilan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = 'tugas_ketrampilan/store';
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = 'tugas_ketrampilan/update';
                    method_url = "PUT";
                }
                var formData = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');

                $.ajax({
                    type: 'POST',
                    url: "tugas_ketrampilan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Tugas Ketrampilan");
                        $('#file-chosen').html(data.nama_file);
                        $('#saveBtn').val("edit-user");
                        $('#ajaxModel').modal('show');
                        $('#id_ketrampilan').val(data.id);
                        $('#kompetensi_inti').val(data.id_kompetensi_inti).trigger("change");
                        $('#tgl_maks_pengumpulan').val(data.tgl_pengumpulan);
                        $('#waktu_maks_pengumpulan').val(data.waktu_pengumpulan);
                        var s2 = $('#skema_penilaian');
                        var vals = data.skema;
                        vals.forEach(function(key) {
                            if (!s2.find('option:contains(' + key + ')').length)
                                s2.append($('<option>').text(key));
                        });
                        s2.val(vals).trigger("change");
                        // edit_dasar(data.id_kompetensi_inti, data.kompetensi_dasar)
                        $('#nama').val(data.nama);

                        $('#keterangan').val(data.keterangan);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                    }
                });

            });

            $(document).on('click', '.hasil', function() {
                id_tugas = $(this).data('id');
                tabel_jawaban.ajax.reload().draw();
                $('#hasilModel').modal('show');
            });

            $(document).on('click', '.delete', function() {
                var id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "tugas_ketrampilan/trash",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $("#data-tabel").dataTable().fnDraw()
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.detailInstruksi ', function() {
                var id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "tugas_ketrampilan/edit",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-eye"></i>');
                        $('#hasil_instruksi').html(data.keterangan);
                        $('#modalInstruksi').modal('show');

                    }
                });
            });
        });

        $(".wysihtml5-sandbox").hide()

        function input_nilai(id, params) {
            let text;
            let person = prompt("Silahkan masukan nilai:", params);
            if (person) {
                $.ajax({
                    url: "jawaban_ketrampilan/save_nilai",
                    type: "POST",
                    data: {
                        id,
                        nilai: person
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            var oTable = $('#tabel-hasil').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                    }
                });
            }
        }
    </script>
@endsection
