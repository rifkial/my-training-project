@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        button.btn.btn-success {
            margin-left: 3px;
            background-color: #51d2b7;
        }

        input#l8 {
            width: -webkit-fill-available;
        }

        button.delete.btn.btn-danger.btn-sm {
            margin-top: 4px;
        }

        a.edit.btn.btn-info.btn-sm.edit {
            margin-top: 4px;
        }

        .pace {
            display: none;
        }

        .card {
            border-radius: 4px;
            box-shadow: 0 1px 2px rgb(0 0 0 / 5%), 0 0 0 1px rgb(63 63 68 / 10%);
            background-color: #FFFFFF;
            margin-bottom: 30px;
        }

        .card .header {
            padding: 15px 15px 0;
        }

        .card .content {
            padding: 15px 15px 10px 15px;
            overflow: auto;
        }

        .cart {
            padding: 30px;
            background-color: transparent
        }

        a.btn.btn-xs.btn-success {
            margin-right: 3px;
        }

    </style>
    <div class="row" style="padding: 12px;">

        <div class="col-md-12">
            @php
                $detail = [];
                if (!empty($pengetahuan)) {
                    $detail = end($pengetahuan);
                } elseif (!empty($keterampilan)) {
                    $detail = end($keterampilan);
                }
            @endphp
            @if (!empty($detail))
                <div class="alert alert-warning" style="color: #000">
                    <b>Petunjuk : </b><br>
                    <ul>
                        <li>Menu ini digunakan untuk menginput nilai pengetahuan pada mata pelajaran <b><i>{{ ucwords($detail['pelajaran']) }},
                                    kelas {{ $detail['kelas'] }}</i></b> </li>
                        <li>Jika kompetensi dasar belum ada, silakan klik tombol <b><i>Tambah KD</i></b>. Untuk mengubah
                            atau
                            menghapus nama KD, silakan klik tombol "<i class="fa fa-pencil"></i>" atau "<i
                                class="fa fa-remove"></i>". </li>
                        <li>Untuk mengisikan nilai pengetahuan pada masing-masing KD, silakan klik nama KD, dan akan muncul
                            daftar siswa serta isian nilai. Nilai dalam <b><i>skala 1-100</i></b>. Jangan lupa klik tombol
                            <b><i>Simpan</i></b> di sebelah bawah.
                        </li>
                    </ul>
                </div>
            @endif

        </div>
        <div class="col-md-6">
            @php
                $num = 1;
            @endphp
            @if (empty($pengetahuan))
                <div class="card-body cart">
                    <div class="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png"
                            width="130" height="130" class="img-fluid mb-4 mr-3">
                        <h3><strong>Kompetensi Pengetahuan is Empty</strong></h3>
                        <h4>Add something to make me happy :)</h4>
                    </div>
                </div>
            @endif
            @foreach ($pengetahuan as $peng)
                <div class="card">
                    <div class="header">
                        <h5 class="title">{{ $num++ }}. Nilai {{ $peng['jenis_kompetensi'] }} &raquo;
                            {{ $peng['pelajaran'] . ' - ' . $peng['kelas'] }}</h5>
                    </div>
                    <div class="content">
                        <ul class="list-group" id="list_kd">
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Kompetensi Inti :</p>
                                <p class="mb-0">{{ $peng['kompetensi_inti'] }}</p>
                            </li>
                            <li class="list-group-item" onclick="return view_kd(6, 7,'t');">
                                <p class="mb-0">Indikator :</p>
                                <p class="mb-0">{{ $peng['indikator'] }}</p>
                            </li>
                        </ul>
                        <p style="margin-top: 1.42857em;">
                            <a href="javascript:void(0)" onclick="addData({{ $peng['id'] }})" class="btn btn-info"><i
                                    class="fa fa-plus-circle"></i> Tambah KD</a>
                        </p>
                        <ul class="list-group" id="list_kd">
                            {{-- <li class="list-group-item" onclick="return view_kd(6, 7,'t');"><a href="#"><i class="fa fa-chevron-right"></i>  ULANGAN TENGAH SEMESTER</a></li>
                        <li class="list-group-item" onclick="return view_kd(6, 7,'a');"><a href="#"><i class="fa fa-chevron-right"></i>  ULANGAN AKHIR SEMESTER</a></li> --}}
                            <div id="list_kd_{{ $peng['id'] }}" style="margin-bottom: 10px">
                                @foreach ($peng['kompetensi_dasar'] as $dasar_peng)
                                    <li class="list-group-item">
                                        {{ $dasar_peng['nama'] }}
                                        <div class="pull-right">
                                            <a href="javascript:void(0)" onclick="return edit({{ $dasar_peng['id'] }});"
                                                class="btn btn-xs btn-success">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="return hapus({{ $dasar_peng['id'] }});"
                                                class="btn btn-xs btn-danger">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </div>

                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-6">
            @php
                $no = 1;
            @endphp
            @if (empty($keterampilan))
                <div class="card-body cart">
                    <div class="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png"
                            width="130" height="130" class="img-fluid mb-4 mr-3">
                        <h3><strong>Kompetensi Ketrampilan is Empty</strong></h3>
                        <h4>Add something to make me happy :)</h4>
                    </div>
                </div>
            @endif
            @foreach ($keterampilan as $ktrmpln)
                <div class="card">
                    <div class="header">
                        <h5 class="title">{{ $no++ }}. Nilai {{ $ktrmpln['jenis_kompetensi'] }} &raquo;
                            {{ $ktrmpln['pelajaran'] . ' - ' . $ktrmpln['kelas'] }}</h5>
                    </div>
                    <div class="content">
                        <ul class="list-group" id="list_kd">
                            <li class="list-group-item" style="border: 0px; padding-top: 3px; padding-bottom: 3px">
                                <p class="mb-0">Kompetensi Inti :</p>
                                <p class="mb-0">{{ $ktrmpln['kompetensi_inti'] }}</p>
                            </li>
                            <li class="list-group-item" style="border: 0px; padding-top: 3px; padding-bottom: 3px">
                                <p class="mb-0">Indikator :</p>
                                <p class="mb-0">{{ $ktrmpln['indikator'] }}</p>
                            </li>
                        </ul>
                        <p style="margin-top: 1.42857em;">
                            <a href="javascript:void(0)" onclick="addData({{ $ktrmpln['id'] }})" class="btn btn-info"><i
                                    class="fa fa-plus-circle"></i> Tambah KD</a>
                        </p>
                        <ul class="list-group" id="list_kd">
                            {{-- <li class="list-group-item" onclick="return view_kd(6, 7,'t');"><a href="#"><i class="fa fa-chevron-right"></i>  ULANGAN TENGAH SEMESTER</a></li>
                        <li class="list-group-item" onclick="return view_kd(6, 7,'a');"><a href="#"><i class="fa fa-chevron-right"></i>  ULANGAN AKHIR SEMESTER</a></li> --}}
                            <div id="list_kd_{{ $ktrmpln['id'] }}" style="margin-bottom: 10px">
                                @foreach ($ktrmpln['kompetensi_dasar'] as $dasar_ktrm)
                                    <li class="list-group-item">
                                        {{ $dasar_ktrm['nama'] }}
                                        <div class="pull-right">
                                            <a href="javascript:void(0)" onclick="return edit({{ $dasar_ktrm['id'] }});"
                                                class="btn btn-xs btn-success">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="return hapus({{ $dasar_ktrm['id'] }});"
                                                class="btn btn-xs btn-danger">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </div>

                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md modal-side modal-bottom-left">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="kompetensi_inti" id="kompetensi_inti">
                                <input type="hidden" name="id_kd" id="id_kd">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Kode</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="kode" name="kode" type="text" autocomplete="false">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="nama" name="nama" type="text" autocomplete="false">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // list_kd();

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "standar_kompetensi/store";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "standar_kompetensi/update";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            var id_kompetensi_inti = data.id_kompetensi_inti
                            $.ajax({
                                type: "POST",
                                url: "standar_kompetensi/load",
                                data: {
                                    id_kompetensi_inti: id_kompetensi_inti
                                },
                                beforeSend: function() {
                                    $("#list_kd_" + id_kompetensi_inti).html(
                                        '<i class="fa fa-spin fa-spinner"></i> Loading'
                                    );
                                },
                                success: function(data) {
                                    var h = '';
                                    if (data.length > 0) {
                                        $.each(data, function(i, v) {
                                            h += '<li class="list-group-item">' +
                                                v.nama +
                                                '<div class="pull-right"><a href="javascript:void(0)" onclick="return edit(' +
                                                v.id +
                                                ');" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a><a href="javascript:void(0)" onclick="return hapus(' +
                                                v.id +
                                                ');" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> </a></div></li>';
                                        });
                                    } else {
                                        h +=
                                            '<div class="alert alert-info">KD Belum satupun diinputkan</div>';
                                    }

                                    $("#list_kd_" + id_kompetensi_inti).html(h);
                                }
                            });
                        }
                        $('#saveBtn').html('Simpan');
                        noti(data.icon, data.success);


                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });



        function addData(id) {
            // console.log(id);
            $('#saveBtn').val("create-Customer");
            $('#kompetensi_inti').val(id);
            $('#id_kd').val('');
            $('#CustomerForm').trigger("reset");
            $('#modelHeading').html("Tambah Kompetensi Dasar");
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        }

        function edit(id) {
            $('#form_result').html('');
            $.ajax({
                type: 'POST',
                url: "standar_kompetensi/edit_kd",
                data: {
                    id_kompetensi_dasar: id
                },
                success: function(data) {
                    console.log(data);
                    $('#modelHeading').html("Edit Data Kompetensi Dasar");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#id_kd').val(data.id);
                    $('#kompetensi_inti').val(data.id_kompetensi_inti);
                    $('#kode').val(data.kode_kompetensi);
                    $('#nama').val(data.nama_kompetensi);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                }
            });
        }

        function hapus(id) {
            // console.log(id);
            if (id == 0) {
                noti("danger", "Silakan pilih datanya..!");
            } else {
                if (confirm('Anda yakin...?')) {
                    $.ajax({
                        type: "POST",
                        url: "standar_kompetensi/trash_kd",
                        data: {
                            id_kompetensi_dasar: id
                        },
                        success: function(data) {
                            // console.log(data);
                            var id_kompetensi_inti = data
                            $.ajax({
                                type: "POST",
                                url: "standar_kompetensi/load",
                                data: {
                                    id_kompetensi_inti: id_kompetensi_inti
                                },
                                beforeSend: function() {
                                    $("#list_kd_" + id_kompetensi_inti).html(
                                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                                },
                                success: function(data) {
                                    var h = '';
                                    if (data.length > 0) {
                                        $.each(data, function(i, v) {
                                            h += '<li class="list-group-item">' + v.nama +
                                                '<div class="pull-right"><a href="#" onclick="return edit(' +
                                                v.id +
                                                ');" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a><a href="#" onclick="return hapus(' +
                                                v.id +
                                                ');" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> </a></div></li>';
                                        });
                                    } else {
                                        h +=
                                            '<div class="alert alert-info">KD Belum satupun diinputkan</div>';
                                    }

                                    $("#list_kd_" + id_kompetensi_inti).html(h);
                                }
                            });
                        }
                    });
                }
            }
            return false;
        }
    </script>
@endsection
