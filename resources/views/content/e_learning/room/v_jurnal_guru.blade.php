@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin-top: 4px;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hari/Tanggal</th>
                        <th>Pertemuan</th>
                        <th>Materi</th>
                        <th>Hambatan</th>
                        <th>Pemecahan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_jurnal">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tanggal</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tanggal" id="tanggal" class="form-control datepicker"
                                                value="{{ date('d-m-Y') }}" data-plugin-options="{'autoclose': true}"
                                                readonly>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jam Ke</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="jam" name="jam" placeholder="Enter text" type="text"
                                            onkeypress="return hanyaAngka(event)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pertemuan Ke</label>
                                    <div class="col-md-9">
                                        <select name="pertemuan" id="pertemuan" class="form-control">
                                            @foreach ($pertemuan as $per)
                                                <option value="{{ $per['id'] }}">{{ $per['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">bahasan Materi</label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="materi" name="materi" placeholder="Enter text"
                                            type="text"> <small class="text-muted">Topik/Materi, contoh: cara memandikan
                                            mayat</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Hambatan</label>
                                    <div class="col-md-9">
                                        <textarea name="hambatan" id="hambatan" cols="30" rows="2"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pemecahan</label>
                                    <div class="col-md-9">
                                        <textarea name="pemecahan" id="pemecahan" cols="30" rows="2"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'colvis',
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    }
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'bahan_ajar',
                        name: 'bahan_ajar'
                    },
                    {
                        data: 'hambatan',
                        name: 'hambatan'
                    },
                    {
                        data: 'pemecahan',
                        name: 'pemecahan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Jurnal Guru");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "jurnal_guru/store";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "jurnal_guru/update";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "jurnal_guru/edit",
                    data: {
                        id_jurnal: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i>');
                        $('#modelHeading').html("Edit Data Jurnal Guru");
                        $('#saveBtn').val("edit-user");
                        $('#ajaxModel').modal('show');
                        $('#id_jurnal').val(data.id);
                        $('#tanggal').val(convertDate(data.tanggal_input));
                        $('#jam').val(data.jam);
                        $('#pertemuan').val(data.pertemuan).trigger("change");
                        $('#materi').val(data.bahan_ajar);
                        $('#hambatan').val(data.hambatan);
                        $('#pemecahan').val(data.pemecahan);
                        $('#action_button').val('Edit');
                        $('#action').val('Edit');
                    }
                });
            });
        });





        function deleteData(id) {
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            url: "jurnal_guru/trash",
                            type: "POST",
                            data: {
                                id_jurnal: id
                            },
                            beforeSend: function() {
                                $(".delete-" + id).html(
                                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                            },
                            success: function(data) {
                                swal(
                                    'Deleted!',
                                    data.message,
                                    'success'
                                )

                            },
                            error: function() {

                                swal(
                                    'Cancelled',
                                    data.message,
                                    'error'
                                )
                            }
                        })
                        $("#data-tabel").dataTable().fnDraw()
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }



    </script>


@endsection
