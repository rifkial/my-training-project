@extends('template.template_mobile.app')
@section('content')

    <div class="section full">
        <div class="wide-block pt-2 pb-2 product-detail-header">
            <div class="rate-block mb-1">
                <ion-icon name="star" class="active"></ion-icon>
                <ion-icon name="star" class="active"></ion-icon>
                <ion-icon name="star" class="active"></ion-icon>
                <ion-icon name="star" class="active"></ion-icon>
                <ion-icon name="star"></ion-icon>
            </div>
            <h1 class="title">Red Apple</h1>
            <div class="text">1 kg - Packed</div>
            <div class="detail-footer">
                <!-- price -->
                <div class="price">
                    <div class="old-price">$ 74.99</div>
                    <div class="current-price">$ 49.99</div>
                </div>
                <!-- * price -->
                <!-- amount -->
                <div class="amount">
                    <div class="stepper stepper-secondary">
                        <a href="#" class="stepper-button stepper-down">-</a>
                        <input type="text" class="form-control" value="1" disabled />
                        <a href="#" class="stepper-button stepper-up">+</a>
                    </div>
                </div>
                <!-- * amount -->
            </div>
            <button class="btn btn-primary btn-lg btn-block">
                <ion-icon name="cart-outline"></ion-icon>
                Add to Cart
            </button>
        </div>
    </div>


    <div class="section full mt-2">
        <div class="section-title">Product Details</div>
        <div class="wide-block pt-2 pb-2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla, nibh sed viverra dictum,
            ligula mauris lobortis tortor, eu efficitur leo nulla at metus. Aliquam malesuada enim augue, semper
            sagittis enim tempus sed.
        </div>

    </div>

    <div class="section full mt-2">
        <div class="section-title">Reviews (2)</div>
        <div class="wide-block pt-2 pb-2">
            <div class="comment-block">
                <div class="item">
                    <div class="avatar">
                        <img src="assets/img/sample/avatar/avatar1.jpg" alt="avatar" class="imaged w32 rounded">
                    </div>
                    <div class="in">
                        <div class="comment-header">
                            <h4 class="title">Diego Morata</h4>
                            <span class="time">just now</span>
                        </div>
                        <div class="rate-block mb-1 mt-05">
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star"></ion-icon>
                        </div>
                        <div class="text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </div>
                        <div class="comment-footer">
                            <a href="javascript:;" class="comment-button">
                                <ion-icon name="happy-outline"></ion-icon>
                                Helpful (523)
                            </a>
                            <a href="javascript:;" class="comment-button">
                                <ion-icon name="flag-outline"></ion-icon>
                                Report
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="avatar">
                        <img src="assets/img/sample/avatar/avatar4.jpg" alt="avatar" class="imaged w32 rounded">
                    </div>
                    <div class="in">
                        <div class="comment-header">
                            <h4 class="title">Carmelita Marsham</h4>
                            <span class="time">Sep 23, 2020</span>
                        </div>
                        <div class="rate-block mb-1 mt-05">
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                            <ion-icon name="star" class="active"></ion-icon>
                        </div>
                        <div class="text">
                            Vivamus lobortis, orci et commodo pulvinar, eros nibh volutpat ipsum, in rhoncus risus
                            dolor
                            sed ipsum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec nisi
                            odio,
                            dapibus in felis vel, lobortis iaculis quam.
                        </div>
                        <div class="comment-footer">
                            <a href="javascript:;" class="comment-button">
                                <ion-icon name="happy-outline"></ion-icon>
                                Helpful (43)
                            </a>
                            <a href="javascript:;" class="comment-button">
                                <ion-icon name="flag-outline"></ion-icon>
                                Report
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider mt-3 mb-2"></div>
            <a href="#" class="btn btn-block btn-primary">Add a review</a>
        </div>
    </div>
@endsection
