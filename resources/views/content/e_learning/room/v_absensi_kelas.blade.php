@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .batas {
            border: 1px solid #30e4bf;
            padding: 9px;
            border-radius: 6px;
            margin-top: 7px;
            background-color: #cafff4;
        }

        @media (max-width:481px) {
            .col-lg-9 {
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>


    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Absensi Siswa dalam Kelas Mata Pelajaran otomatis akan melakukan absensi tehadap Guru Mata Pelajaran akan
        Kehadirannya dalam setiap Pertemuan
    </div>
    <br>
    <div class="row input-daterange">
        <div class="col-md-3" style="margin-top: 5px">
            <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
        </div>
        <div class="col-md-3" style="margin-top: 5px">
            <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
        </div>
        <div class="col-md-3" style="margin-top: 5px">
            <select name="pertemuan" id="pertemuans" class="form-control">
                <option value="">Pilih Pertemuan</option>
                @foreach ($pertemuan as $pert)
                    <option value="{{ $pert['pertemuan'] }}">{{ $pert['pertemuan'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3" style="margin-top: 5px">
            <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
        </div>
    </div>
    <br>

    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>PERTEMUAN</th>
                        <th>NISN</th>
                        <th>NAMA</th>
                        <th>ROMBEL</th>
                        <th>STATUS ABSENSI</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body pt-0">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-4 border-right">
                                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                    <img class="rounded-circle mt-5 profile_detail" src="https://i.imgur.com/0eg0aG0.jpg"
                                        width="90">
                                    <span class="font-weight-bold" id="nama_detail">John Doe</span>
                                    <span class="text-black-50"
                                        id="email_detail">john_doe12@bbb.com</span><span>Siswa</span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="p-3 py-5">
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <select name="absensi" id="absensi" class="form-control">
                                                <option value="hadir">hadir</option>
                                                <option value="alfa">alfa</option>
                                                <option value="sakit">sakit</option>
                                                <option value="izin">izin</option>
                                                <option value="terlambat">terlambat</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" id="id_data_siswa">
                                    <input type="hidden" name="id_mapel" id="id_mapel">
                                    <input type="hidden" name="id_kelas_siswa" id="id_kelas_siswa">
                                    <input type="hidden" name="id_kelas" id="id_kelas">
                                    <input type="hidden" name="id_room" id="id_room">
                                    <input type="hidden" name="id_rombel" id="id_rombel">
                                    <input type="hidden" name="id_pertemuan" id="id_pertemuan">
                                    <input type="hidden" name="id_guru" id="id_guru">
                                    <div class="batas">
                                        <div class="row mt-2">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Nama" id="nama"
                                                    readonly>
                                                <small>*Nama</small>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="nisn" placeholder="NISN"
                                                    readonly>
                                                <small>*NISN</small>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Email" id="email"
                                                    readonly>
                                                <small>*Email</small>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="telepon"
                                                    placeholder="Phone number" readonly>
                                                <small>*Phone number</small>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Mapel" id="mapel"
                                                    readonly>
                                                <small>*Mapel</small>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="status" placeholder="Status"
                                                    readonly>
                                                <small>*Status</small>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Tempat Lahir"
                                                    id="tempat_lahir" readonly>
                                                <small>*Tempat lahir</small>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="tanggal_lahir"
                                                    placeholder="Tanggal Lahir" readonly>
                                                <small>*Tanggal lahir</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-5 text-right"><button class="btn btn-success profile-button"
                                            type="submit" id="saveBtn">Update Absensi</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        // pertemuan = 1;
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.input-daterange').datepicker({
                todayBtn: 'linked',
                format: 'yyyy-mm-dd',
                autoclose: true
            });


            load_data();

            function load_data(from_date = '', to_date = '', pertemuan = '') {
                $('#data-tabel').DataTable({
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: [0, ':visible']
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },

                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }
                    ],
                    processing: true,
                    serverSide: true,
                    "responsive": true,
                    ajax: {
                        url: 'absensi/filter_date',
                        method: "POST",
                        data: {
                            from_date: from_date,
                            to_date: to_date,
                            pertemuan: pertemuan,
                        }
                    },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'pertemuan',
                            name: 'pertemuan'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'rombel',
                            name: 'rombel'
                        },
                        {
                            data: 'absensi',
                            name: 'absensi'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ]
                });
            }

            $('#filter').click(function() {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var pertemuan = $('#pertemuans').val();
                // console.log(pertemuan);
                if (from_date != '' && to_date != '') {
                    $('#data-tabel').DataTable().destroy();
                    load_data(from_date, to_date, pertemuan);
                } else {
                    alert('Both Date is required');
                }
            });

            $('#CustomerForm').on('submit', function(event) {
                $('#saveBtn').html('Sending...');
                event.preventDefault();
                $.ajax({
                    url: "absensi/update",
                    method: "PUT",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#CustomerForm').trigger("reset");
                            $('#ajaxModel').modal('hide');
                            $('#saveBtn').html('Simpan');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                            noti(data.icon, data.success);
                        } else {
                            noti(data.icon, data.success);
                            $('#saveBtn').html('Simpan');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/absensi/edit') }}",
                    data: {
                        id_absensi: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        // console.log(data);
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i> Edit');
                        $('#modelHeading').html("Edit Data Absensi");
                        $('#saveBtn').val("edit-user");
                        $('#ajaxModel').modal('show');
                        $('#id_data_siswa').val(data.id);
                        $('#nisn').val(data.nisn);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#nama').val(data.nama);
                        $('#nama_detail').html(data.nama);
                        $('#email').val(data.email);
                        $('#telepon').val(data.telepon);
                        $('#email_detail').html(data.email);
                        $('#tanggal_lahir').val(data.tanggal);
                        $('#mapel').val(data.mapel);
                        $('#status').val(data.status);
                        if (data.file) {
                            $('.profile_detail').attr('src', data.file);
                        }
                        $('#id_mapel').val(data.id_mapel);
                        $('#id_kelas_siswa').val(data.id_kelas_siswa);
                        $('#id_kelas').val(data.id_kelas);
                        $('#id_room').val(data.id_room);
                        $('#id_rombel').val(data.id_rombel);
                        $('#id_pertemuan').val(data.pertemuan);
                        $('#id_guru').val(data.id_guru);
                        $('#absensi').val(data.absensi).trigger("change");
                        $('#action_button').val('Edit');
                        $('#actions').val('Edit');
                    }
                });
            });
        });
    </script>
@endsection
