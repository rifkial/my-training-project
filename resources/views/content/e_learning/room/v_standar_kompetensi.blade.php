@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        button.btn.btn-success {
            margin-left: 3px;
            background-color: #51d2b7;
        }

        input#l8 {
            width: -webkit-fill-available;
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    @if (session('role') == 'guru')
        <a href="javascript:void(0)" class="mr-3 btn btn-outline-color-scheme" id="createNewCustomer"
            style="float: right; margin-bottom: 12px;"> Tambah Data</a>
    @endif
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-bordered table-responsive" min-width="100%">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>KOMPETENSI INTI (KI)</th>
                    <th width="375px">KOMPETENSI DASAR (KD)</th>
                    <th>INDIKATOR</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($kompetensi as $item)

                    <tr>
                        <td rowspan='2'>{{ $i++ }}</td>
                        <td colspan='3'>
                            {{ $item['jenis_kompetensi'] }}
                        </td>
                    <tr>
                        <td>
                            <div>
                                @php
                                    $kompetensi_inti = $item['kompetensi_inti'];
                                    $pecah_kompetensi_inti = explode('#', $kompetensi_inti);
                                @endphp
                                @if ($kompetensi_inti != '')
                                    @foreach ($pecah_kompetensi_inti as $kmptn_inti)
                                        @if ($kmptn_inti != '')
                                            {{ $kmptn_inti }}
                                            <a href=""><i class="fa fa-pencil"></i></a> <br>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </td>
                        <td>
                            <form action="{{ route('room-standar_kompetensi') }}">
                                <div class="input-group" style="height:47px">
                                    <input id="l8" placeholder="Ketik disini" type="text"><button type="submit"
                                        class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </form>
                            <div>
                                @php
                                    $kompetensi_dasar = $item['kompetensi_dasar'];
                                    $pecah_kompetensi_dasar = explode('#', $kompetensi_dasar);
                                @endphp
                                @if ($kompetensi_dasar != '')
                                    @foreach ($pecah_kompetensi_dasar as $kmptn_dasar)
                                        @if ($kmptn_dasar != '')
                                            {{ $kmptn_dasar }}
                                            <a href=""><i class="fa fa-trash"></i></a>
                                            <a href=""><i class="fa fa-pencil"></i></a> <br>
                                        @endif

                                    @endforeach
                                @endif
                            </div>
                        </td>
                        <td>
                            <div>
                                @php
                                    $indikator = $item['indikator'];
                                    $pecah_indikator = explode('#', $indikator);
                                @endphp
                                @if ($indikator != '')
                                    @foreach ($pecah_indikator as $indktr)
                                        @if ($indktr != '')
                                            {{ $indktr }}
                                            <a href=""><i class="fa fa-pencil"></i></a> <br>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </td>
                    </tr>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-sm-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="mySmallModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body" style="padding-bottom: 0px;">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id-kompetensi">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Nama Kompetensi</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn" value="create"
                        style="background-color: #51d2b7; border-color:#51d2b7">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable1').DataTable();
        });

        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Jurusan");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('store-standar_kompetensi') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('update-standar_kompetensi') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('#saveBtn').html('Simpan');
                        var oTable = $('#data-tabel').dataTable();
                        oTable.fnDraw(false);
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.get("" + 'jurusan/edit/' + id, function(data) {
                    console.log(id);
                    $('#modelHeading').html("Edit Data Jurusan");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#id_jurusan').val(data.id);
                    $('#nama').val(data.nama);
                    $('#action_button').val('Edit');
                    $('#action').val('Edit');
                });
            });
        });

    </script>
@endsection
