@extends('content.e_learning.room.v_data_room')
@section('content_room')
    <style>
        .pace {
            display: none;
        }

        .btn-sm {
            margin: 2px;
        }

        #label_file {
            background-color: #11af49;
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            border-radius: 0.3rem;
            cursor: pointer;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

        .blog-post.blog-post-card.text-center {
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
        }

        .blog-post.blog-post-card.text-center:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        .form-control {
            -webkit-box-shadow: black !important;
            box-shadow: none;
        }

        input.line {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: none;
            border-bottom: 2px solid #049c1e;
        }

        .line::-webkit-input-placeholder {
            text-align: center;
        }

        .card .card-body {
            padding: 30px;
            background-color: transparent
        }

        .cart {
            padding: 30px;
            background-color: transparent
        }

    </style>
    <h3 class="box-title">{{ Session::get('title') }}</h3>
    <hr>
    <div style="width: 100%;">
        <div class="table-responsive">
            <table class="table table-striped" id="data-tabel">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tanggal Upload</th>
                        <th>Judul</th>
                        <th>Pertemuan</th>
                        <th>Ekstensi</th>
                        <th>Link</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajax-product-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="productForm" action="javascript:void(0)" name="CustomerForm" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-warning">
                                    1. Anda dapat mengupload bahan ajar yang anda punya yng berhunungan dengan materi
                                    reading text <br>
                                    2. Anda dapat membuat bahan ajar berbentuk dokumen (pdf/word/excel/power point) atau
                                    membagikan URL dari &nbsp;&nbsp;&nbsp; website dan juga menambahkan video sebagai bahan
                                    ajar <br>
                                    3. anda dapat mencari bahan ajar dari guru berbagi yang dibuat oleh guru lain atau anda
                                    sendiri <br>
                                    4. bahan ajar yang anda share dapat dipelajari oleh siswa yang tergabung dikelas anda
                                    <br>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Pertemuan</label>
                                    <input type="hidden" name="id" id="id_bahan_ajar">
                                    <div class="col-md-9">
                                        <select name="pertemuan" id="pertemuan" class="form-control">
                                            @foreach ($pertemuan as $pert)
                                                <option value="{{ $pert['id'] }}">{{ $pert['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Judul</label>
                                    <div class="col-md-9">
                                        <input type="text" name="judul" id="judul" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">File Tugas</label>
                                    <div class="col-md-9">
                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file">Pilih File</label>
                                        <span id="file-chosen">No file chosen</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Link Url</label>
                                    <div class="col-md-9">
                                        <input type="url" name="link" id="link" class="form-control" placeholder="https://docs.google.com/">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actions" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        text: 'Tambah Data',
                        className: 'btn btn-sm btn-facebook',
                        attr: {
                            title: 'Tambah Data',
                            id: 'createNewCustomer'
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'colvis',
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    }
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'tanggal_indo',
                        name: 'tanggal_indo'
                    },
                    {
                        data: 'judul',
                        name: 'judul'
                    },
                    {
                        data: 'pertemuan',
                        name: 'pertemuan'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'link',
                        name: 'link'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            const actualBtn = document.getElementById('actual-btn');
            const fileChosen = document.getElementById('file-chosen');
            actualBtn.addEventListener('change', function() {
                fileChosen.textContent = this.files[0].name
            })

            $('#createNewCustomer').click(function() {
                $('#saveBtn').val("create-Customer");
                $('#file-chosen').html('');
                $('#Customer_id').val('');
                $('#productForm').trigger("reset");
                $('#modelHeading').html("Tambah Data Bahan Ajar");
                $('#ajax-product-modal').modal('show');
                $('#actions').val('Add');
            });

            $('body').on('submit', '#productForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#saveBtn').html('Sending..');
                var action_url = '';

                if ($('#actions').val() == 'Add') {
                    action_url = "bahan_ajar/store";
                    method_url = "POST";
                }

                if ($('#actions').val() == 'Edit') {
                    action_url = "bahan_ajar/update";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#CustomerForm').trigger("reset");
                            $('#ajax-product-modal').modal('hide');
                            var oTable = $('#data-tabel').dataTable();
                            oTable.fnDraw(false);
                        }
                        noti(data.icon, data.success);
                        $('#saveBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                var id = $(this).data('id');
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "bahan_ajar/edit",
                    data: {
                        id_bahan_ajar: id
                    },
                    beforeSend: function() {
                        $(".editData-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        console.log(data);
                        $(".editData-" + id).html('<i class="fa fa-pencil-square-o"></i>');
                        $('#modelHeading').html("Edit Data Bahan Ajar");
                        $('#action_button').val('Edit');
                        $('#actions').val('Edit');
                        $('#btn-save').val("edit-user");
                        $('#ajax-product-modal').modal('show');
                        $('#id_bahan_ajar').val(data.id);
                        $('#judul').val(data.judul);
                        $('#link').val(data.link);
                        $('#file-chosen').html(data.files);
                    }
                });
            });
        });

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "bahan_ajar/trash",
                    type: "POST",
                    data: {
                        id_bahan_ajar: id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            swa("Berhasil!", data.message, data.success);
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#data-trash').dataTable().fnDraw(false);
                        } else {
                            swa("Gagal!", data.message, data.success);
                            $(".delete-" + id).html(
                                '<i class="fa fa-trash"></i>');
                        }
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
