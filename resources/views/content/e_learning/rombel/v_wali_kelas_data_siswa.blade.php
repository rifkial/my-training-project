@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')
    <style>
        .pace {
            display: none;
        }

        .btn-xs {
            margin-top: 4px;
        }

        a.edit {
            padding-left: 9px;
        }

        .panel {
            box-shadow: none;
        }

        .panel-heading {
            border-bottom: 0;
        }

        .panel-title {
            font-size: 17px;
        }

        .panel-title>small {
            font-size: .75em;
            color: #999999;
        }

        .panel-body *:first-child {
            margin-top: 0;
        }

        .panel-footer {
            border-top: 0;
        }

        .panel-default>.panel-heading {
            color: #333333;
            background-color: transparent;
            border-color: rgba(0, 0, 0, 0.07);
        }

        /**
     * Profile
     */
        /*** Profile: Header  ***/
        .profile__avatar {
            float: left;
            width: 60px;
            height: 60px;
            border-radius: 50%;
            margin-right: 20px;
            overflow: hidden;
        }

        @media (min-width: 768px) {
            .profile__avatar {
                width: 100px;
                height: 100px;
            }
        }

        .profile__avatar>img {
            width: 100%;
            height: auto;
        }

        .profile__header {
            overflow: hidden;
        }

        .profile__header p {
            margin: 20px 0;
        }

        /*** Profile: Table ***/
        @media (min-width: 992px) {
            .profile__table tbody th {
                width: 200px;
                border: 0;
            }
        }

        /*** Profile: Recent activity ***/
        .profile-comments__item {
            position: relative;
            padding: 15px 16px;
            border-bottom: 1px solid rgba(0, 0, 0, 0.05);
        }

        .profile-comments__item:last-child {
            border-bottom: 0;
        }

        .profile-comments__item:hover,
        .profile-comments__item:focus {
            background-color: #f5f5f5;
        }

        .profile-comments__item:hover .profile-comments__controls,
        .profile-comments__item:focus .profile-comments__controls {
            visibility: visible;
        }

        .profile-comments__controls {
            position: absolute;
            top: 0;
            right: 0;
            padding: 5px;
            visibility: hidden;
        }

        .profile-comments__controls>a {
            display: inline-block;
            padding: 2px;
            color: #999999;
        }

        .profile-comments__controls>a:hover,
        .profile-comments__controls>a:focus {
            color: #333333;
        }

        .profile-comments__avatar {
            display: block;
            float: left;
            margin-right: 20px;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            overflow: hidden;
        }

        .profile-comments__avatar>img {
            width: 100%;
            height: auto;
        }

        .profile-comments__body {
            overflow: hidden;
        }

        .profile-comments__sender {
            color: #333333;
            font-weight: 500;
            margin: 5px 0;
        }

        .profile-comments__sender>small {
            margin-left: 5px;
            font-size: 12px;
            font-weight: 400;
            color: #999999;
        }

        @media (max-width: 767px) {
            .profile-comments__sender>small {
                display: block;
                margin: 5px 0 10px;
            }
        }

        .profile-comments__content {
            color: #999999;
        }

        /*** Profile: Contact ***/
        .profile__contact-btn {
            padding: 12px 20px;
            margin-bottom: 20px;
        }

        .profile__contact-hr {
            position: relative;
            border-color: rgba(0, 0, 0, 0.1);
            margin: 40px 0;
        }

        .profile__contact-hr:before {
            content: "OR";
            display: block;
            padding: 10px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            background-color: #f5f5f5;
            color: #c6c6cc;
        }

        .profile__contact-info-item {
            margin-bottom: 30px;
        }

        .profile__contact-info-item:before,
        .profile__contact-info-item:after {
            content: " ";
            display: table;
        }

        .profile__contact-info-item:after {
            clear: both;
        }

        .profile__contact-info-item:before,
        .profile__contact-info-item:after {
            content: " ";
            display: table;
        }

        .profile__contact-info-item:after {
            clear: both;
        }

        .profile__contact-info-icon {
            float: left;
            font-size: 18px;
            color: #999999;
        }

        .profile__contact-info-body {
            overflow: hidden;
            padding-left: 20px;
            color: #999999;
        }

        .profile__contact-info-body a {
            color: #999999;
        }

        .profile__contact-info-body a:hover,
        .profile__contact-info-body a:focus {
            color: #999999;
            text-decoration: none;
        }

        .profile__contact-info-heading {
            margin-top: 2px;
            margin-bottom: 5px;
            font-weight: 500;
            color: #999999;
        }

    </style>

    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header">
                {{ Session::get('title') }}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div style="width: 100%;">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NISN</th>
                                            <th>Nama</th>
                                            <th>Jenis Kelamin</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingAdd"></h5>
                </div>
                <form id="addForm" name="addForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_mapel_kelas">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Status</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width:50%">
                                            <select name="status" id="status" class="form-control">
                                                <option value="1">Aktif</option>
                                                <option value="2">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tahun Ajaran</label>
                                    <div class="col-md-9">
                                        <div class="input-group" style="width:50%">
                                            <select name="id_tahun_ajaran" id="id_tahun_ajaran" class="form-control">
                                                <option value="">---Pilih Tahun Ajaran---</option>
                                                @foreach ($tahun_ajaran as $key)
                                                    <option value="{{ $key['id'] }}">
                                                        {{ $key['tahun_ajaran'] . '  ' . $key['semester'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Pilih Siswa</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="id_siswa" id="id_siswa" class="form-control select3">
                                                <option value="">NIS | NAMA</option>
                                                @foreach ($siswa as $item)
                                                    <option value="{{ $item['id'] }}">
                                                        {{ $item['nis'] . ' | ' . $item['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxEdit" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id" id="id_siswa_edit">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nik" id="nik" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NIS</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nis" id="nis" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NISN</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nisn" id="nisn" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Gambar</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="old_image" id="old_image">
                                        <div class="input-group">
                                            <input id="image" type="file" name="image" accept="image/*"
                                                onchange="readURL(this);">
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group" width="100%" style="margin-top: 10px">
                                        <div id="delete_foto" style="text-align: center"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katolik">Katolik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenkel</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select name="jenkel" id="jenkel" class="form-control">
                                                <option value="l">Laki-laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Telepon</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="telepon" id="telepon" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tahun Angkatan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tahun_angkatan" id="tahun_angkatan"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">TTL</label>
                                    <div class="col-md-5" style="padding-right: 4px">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px">
                                        <div class="input-group">
                                            <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                class="form-control datepicker" value="{{ date('d-m-Y') }}"
                                                data-plugin-options='{"autoclose": true}'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat" cols="30" rows="3"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" id="action" value="Add" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="updateBtn"
                        value="create">Simpan</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg-color-scheme" id="detailModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse m-0" style="border: 0">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="row">
                    <div class="col-md-12" id="profilDetail">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({

                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jenkel',
                        name: 'jenkel'
                    },
                ]
            });


            $(document).on('click', '.tampil', function() {
                var id = $(this).data('id');
                var loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'GET',
                    url: "siswa/" + id,
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-eye"></i> Show');
                        $('#profilDetail').html(data);
                        $('#detailModal').modal('show');
                    }
                });
            });
        });



    </script>
@endsection
