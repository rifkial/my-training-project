@extends('content.e_learning.room.kelas.v_data_rombel')
@section('content_rombel')

    <div class="row">
        <div class="card" style="width: 100%">
            <div class="card-header row mx-0 bg-success">
                <div class="col-md-7">
                    <h2 class="box-title text-white">{{ Session::get('title') }}</h2>

                </div>
                <div class="col-md-5">
                    <form class="navbar-form" action="javascript:void(0)" id="searchGuru">
                        <div class="input-group">
                            <select name="id_guru" {{ empty($mapel) ? 'disabled' : '' }} class="form-control"
                                id="id_guru">
                                @if (empty($mapel))
                                    <option value="">Kelas anda Belum set guru pelajaran</option>
                                @else
                                    <option value="">Pilih Guru...</option>
                                    @foreach ($mapel as $mpl)
                                        <option value="{{ $mpl['id'] }}">
                                            {{ $mpl['guru'] }}</option>
                                    @endforeach
                                @endif

                            </select>
                            <div class="input-group-btn">
                                <button type="submit" id="fil" class="btn btn-info"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body" style="min-height: 200px;">
                <div class="row">
                    <div class="col-md-12" id="informasi">
                        <div class="alert alert-info border-info" role="alert">
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                                    aria-hidden="true">×</span>
                            </button>
                            <p><strong>Tentang Halaman ini:</strong>
                            </p>
                            <ul class="mr-t-10">
                                <li>Memuat Halaman Jurnal</li>
                                <li>Informasi Guru pengajar</li>
                                <li><small class="text-danger">*Semua akan termuat bila telah memilih guru</small></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="hasilModel" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Jadwal Ajar Guru</h5>
                </div>
                <div class="modal-body">
                    <span id="trd"></span>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-stripped" id="tabel-hasil">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Hari</th>
                                        <th>Jam</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Ruang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div> --}}

    <script type="text/javascript">
        var id_guru = 0;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // var tabel_jadwal = $('#tabel-hasil').DataTable({
            //     dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            //     buttons: [{
            //             extend: 'copyHtml5',
            //             text: '<i class="fa fa-clipboard"></i>',
            //             exportOptions: {
            //                 columns: [0, ':visible']
            //             }
            //         },
            //         {
            //             extend: 'print',
            //             text: '<i class="fa fa-print"></i>',
            //             exportOptions: {
            //                 columns: ':visible'
            //             }
            //         },
            //         {
            //             extend: 'excelHtml5',
            //             text: '<i class="fa fa-file-excel-o"></i>',
            //             exportOptions: {
            //                 columns: ':visible'
            //             }
            //         },

            //         {
            //             extend: 'pdfHtml5',
            //             text: '<i class="fa fa-file-pdf-o"></i>',
            //             exportOptions: {
            //                 columns: ':visible'
            //             },
            //             customize: function(doc) {
            //                 doc.content[1].table.widths =
            //                     Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            //             }
            //         },
            //         {
            //             text: '<i class="fa fa-refresh"></i>',
            //             action: function(e, dt, node, config) {
            //                 dt.ajax.reload(null, false);
            //             }
            //         },
            //         'colvis',
            //     ],
            //     processing: true,
            //     serverSide: true,
            //     "responsive": true,
            //     ajax: {
            //         "url": "guru_mapel/jadwal",
            //         "method": "POST",
            //         "data": function(d) {
            //             d.id_guru = id_guru;
            //         },
            //     },
            //     columns: [{
            //             data: 'DT_RowIndex',
            //             name: 'DT_RowIndex'
            //         },
            //         {
            //             data: 'hari',
            //             name: 'hari'
            //         },
            //         {
            //             data: 'jam',
            //             name: 'jam'
            //         },
            //         {
            //             data: 'mapel',
            //             name: 'mapel'
            //         },
            //         {
            //             data: 'ruang',
            //             name: 'ruang'
            //         }
            //     ]
            // });

            $('#searchGuru').on('submit', function(event) {
                event.preventDefault();
                // alert('testing');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('walikelas_detail-guru_mapel') }}",
                    data: {
                        id_guru: $('#id_guru').val()
                    },
                    success: function(data) {
                        $("#informasi").html(data);
                    }
                });
            });

        })

        // function view_kd(id) {
        //     $('#informasi').html('<div id="loading" style="" ></div>');

        // }

        // function jadwal(id) {
        //     tabel_jadwal.ajax.reload().draw();
        //     $('#hasilModel').modal('show');
        // }
    </script>
@endsection
