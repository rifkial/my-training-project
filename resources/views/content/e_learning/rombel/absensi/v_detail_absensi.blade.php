<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body class="header-centered sidebar-horizontal">
    <style>
        td {
            word-break: break-all !important;
        }

    </style>
    <div id="wrapper" class="wrapper" style="padding: 30px; background: #fff;">
        <div style="width: 100%;">
            <div class="table-responsive">
                <table class="table table-striped" id="data-tabel" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Hadir</th>
                            <th>Jam Masuk</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#data-tabel').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [0, ':visible']
                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        },
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function(doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                    '');
                        }
                    },
                    'colvis'
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                autoWidth: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'absensi',
                        name: 'absensi'
                    },
                    {
                        data: 'masuk',
                        name: 'masuk'
                    }
                ]
            });


        });

    </script>
    <!-- Scripts -->
    @include('includes.foot')
</body>

</html>
