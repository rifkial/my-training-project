@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="widget-list">
        <div class="row">
            {{-- {{ dd($fk) }} --}}
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">Daftar Alumni</h5>
                        <p class="text-muted">Data Jurusan tersedia di dalam fakultas.</p>
                        <div class="row">
                            @if (session('role') == 'admin-alumni')
                                <div class="col-md-12">
                                    <button class="btn btn-outline-info" id="addFakultas"><i class="fas fa-plus"></i>
                                        Fakultas</button>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form action="javascript:void(0)" onsubmit="filter('{{ $routes }}')"
                                            method="post">
                                            <div class="form-group">
                                                <label for="l30">Keyword</label>
                                                @php
                                                    $serc = str_replace('-', ' ', $search);
                                                @endphp
                                                <input type="search" value="{{ $serc }}" name="search" id="search"
                                                    class="form-control" placeholder="Kata Kunci Pencarian">
                                            </div>
                                            <div class="form-group">
                                                <label for="l30">Fakultas</label>
                                                <select name="id_fakultas" id="id_fakultas" class="form-control">
                                                    <option value="">Pilih Fakultas..</option>
                                                    @foreach ($fakultas as $fk)
                                                        <option value="{{ (new \App\Helpers\Help())->encode($fk['id']) }}"
                                                            {{ (new \App\Helpers\Help())->encode($fk['id']) == $fkl ? 'selected' : '' }}>
                                                            {{ $fk['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="l30">Jurusan</label>
                                                <select name="id_jurusan" id="id_jurusan" class="form-control">
                                                    <option value="">Pilih Jurusan..</option>
                                                    @foreach ($jurusan as $jr)
                                                        <option value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}"
                                                            {{ (new \App\Helpers\Help())->encode($jr['id']) == $jrs ? 'selected' : '' }}>
                                                            {{ $jr['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="l30">Angkatan</label>
                                                <select name="angkatan" id="angkatan" class="form-control">
                                                    <option value="">Pilih Angkatan..</option>
                                                    @foreach ($angkatan as $ak)
                                                        <option value="{{ $ak['nama'] }}"
                                                            {{ $ak['nama'] == $th_ak ? 'selected' : '' }}>
                                                            {{ $ak['nama'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-actions btn-list">
                                                <button class="btn btn-info btnSearch" type="submit">Search</button>
                                                <button class="btn btn-outline-default" type="reset">Clear</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>Jurusan</th>
                                                        <th>No. Ponsel</th>
                                                        @if (session('role') == 'admin-alumni')
                                                            <th>Status</th>
                                                        @endif
                                                        <th>Opsi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (!empty($alumni))
                                                        @foreach ($alumni as $al)
                                                            <tr>
                                                                <td>{{ $al['nama'] }}</td>
                                                                <td>{{ $al['email'] }}</td>
                                                                <td>{{ $al['jurusan'] }}</td>
                                                                <td>{{ !empty($al['telepon']) ? $al['telepon'] : '-' }}
                                                                </td>
                                                                @if (session('role') == 'admin-alumni')
                                                                    <td>
                                                                        <label class="switch">
                                                                            <input type="checkbox"
                                                                                {{ $al['status'] == 1 ? 'checked' : '' }}
                                                                                class="alumni_check"
                                                                                data-id="{{ $al['id'] }}">
                                                                            <span class="slider round"></span>
                                                                        </label>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    <a href="javascript:void(0)" class="btn btn-sm btn-info"
                                                                        data-toggle="collapse"
                                                                        data-target="#detail{{ $al['id'] }}"><i
                                                                            class="fas fa-info-circle"></i></a>
                                                                    @if (session('role') == 'admin-alumni')
                                                                        <a href="#" class="btn btn-sm btn-facebook"><i
                                                                                class="fas fa-pencil-alt"></i></a>
                                                                        <a href="javascript:void(0)"
                                                                            data-id="{{ $al['id'] }}"
                                                                            class="btn btn-danger btn-sm delete"><i
                                                                                class="fas fa-trash"></i></a>
                                                                    @endif
                                                                    <a href="{{ route('detail-alumni_alumni', (new \App\Helpers\Help())->encode($al['id'])) }}"
                                                                        target="_blank" class="btn btn-sm btn-purple"><i
                                                                            class="fas fa-angle-double-right"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="7" class="hiddenRow">
                                                                    <div class="accordian-body collapse"
                                                                        id="detail{{ $al['id'] }}">
                                                                        <table class="table table-striped">
                                                                            <tr class="bg-info">
                                                                                <th colspan="5"><i
                                                                                        class="fas fa-info-circle"></i>
                                                                                    Detail</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td rowspan="5" colspan="2" class="text-center"><img
                                                                                        src="{{ $al['file'] }}" alt="" width="200">
                                                                                </td>
                                                                                <td></td>
                                                                                <th>Nama</th>
                                                                                <td>{{ $al['nama'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <th>Email</th>
                                                                                <td>{{ $al['email'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <th>No Induk</th>
                                                                                <td>{{ $al['no_induk'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <th>Jurusan</th>
                                                                                <td>{{ $al['jurusan'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <th>Telepon</th>
                                                                                <td>{{ $al['telepon'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Jenis Kelamin</th>
                                                                                <td>{{ $al['jenkel'] }}</td>
                                                                                <td></td>
                                                                                <th>Agama</th>
                                                                                <td>{{ $al['agama'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Tahun Lulus</th>
                                                                                <td>{{ $al['tahun_lulus'] }}</td>
                                                                                <td></td>
                                                                                <th>Tempat, Tanggal Lahir</th>
                                                                                <td>{{ $al['tempat_lahir'] . ', ' . $al['tgl_lahir'] }}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Pendidikan Terakhir</th>
                                                                                <td>{{ $al['pendidikan_terakhir'] }}</td>
                                                                                <td></td>
                                                                                <th>Tahun Angkatan</th>
                                                                                <td>{{ $al['tahun_angkatan'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Nomor Ijazah</th>
                                                                                <td>{{ $al['no_ijazah'] }}</td>
                                                                                <td></td>
                                                                                <th>Terakhir Login</th>
                                                                                <td>{{ $al['last_login'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Ip Address Login</th>
                                                                                <td>{{ $al['last_ip'] }}</td>
                                                                                <td></td>
                                                                                <th>Bio</th>
                                                                                <td>{{ $al['bio'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Alamat</th>
                                                                                <td>{{ $al['alamat'] }}</td>
                                                                                <td></td>
                                                                                <th>Published</th>
                                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($al['created_at']) }}
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6" class="text-center">Data tidak tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.alumni_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-user_alumni') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function filter(routes) {
            $(".btnSearch").attr("disabled", true);
            $(".btnSearch").html(
                '<i class="fa fa-spin fa-spinner"></i> Searching..');
            var searchs = (($('#search') != null) ? $('#search').val() : '');
            var fakultas = (($('#id_fakultas') != null) ? $('#id_fakultas').val() : '');
            var jurusan = (($('#id_jurusan') != null) ? $('#id_jurusan').val() : '');
            var angkatan = (($('#angkatan') != null) ? $('#angkatan').val() : '');
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?fakultas=" + fakultas + "&jurusan=" + jurusan + "&angkatan=" + angkatan + "&search=" +
                search;
            document.location = url;
        }
    </script>
@endsection
