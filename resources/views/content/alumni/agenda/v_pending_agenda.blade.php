@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .event-schedule-area .section-title .title-text {
            margin-bottom: 50px;
        }

        .event-schedule-area .tab-area .nav-tabs {
            border-bottom: inherit;
        }

        .event-schedule-area .tab-area .nav {
            border-bottom: inherit;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            margin-top: 80px;
        }

        .event-schedule-area .tab-area .nav-item {
            margin-bottom: 75px;
        }

        .event-schedule-area .tab-area .nav-item .nav-link {
            text-align: center;
            font-size: 22px;
            color: #333;
            font-weight: 600;
            border-radius: inherit;
            border: inherit;
            padding: 0px;
            text-transform: capitalize !important;
        }

        .event-schedule-area .tab-area .nav-item .nav-link.active {
            color: #4125dd;
            background-color: transparent;
        }

        .event-schedule-area .tab-area .tab-content .table {
            margin-bottom: 0;
            width: 80%;
        }

        .event-schedule-area .tab-area .tab-content .table thead td,
        .event-schedule-area .tab-area .tab-content .table thead th {
            border-bottom-width: 1px;
            font-size: 20px;
            font-weight: 600;
            color: #252525;
        }

        .event-schedule-area .tab-area .tab-content .table td,
        .event-schedule-area .tab-area .tab-content .table th {
            border: 1px solid #b7b7b7;
            padding-left: 30px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th .heading,
        .event-schedule-area .tab-area .tab-content .table tbody td .heading {
            font-size: 16px;
            text-transform: capitalize;
            margin-bottom: 16px;
            font-weight: 500;
            color: #252525;
            margin-bottom: 6px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th span,
        .event-schedule-area .tab-area .tab-content .table tbody td span {
            color: #4125dd;
            font-size: 18px;
            text-transform: uppercase;
            margin-bottom: 6px;
            display: block;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th span.date,
        .event-schedule-area .tab-area .tab-content .table tbody td span.date {
            color: #656565;
            font-size: 14px;
            font-weight: 500;
            margin-top: 15px;
        }

        .event-schedule-area .tab-area .tab-content .table tbody th p {
            font-size: 14px;
            margin: 0;
            font-weight: normal;
        }

        .event-schedule-area-two .section-title .title-text h2 {
            margin: 0px 0 15px;
        }

        .event-schedule-area-two ul.custom-tab {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            border-bottom: 1px solid #dee2e6;
            margin-bottom: 30px;
        }

        .event-schedule-area-two ul.custom-tab li {
            margin-right: 70px;
            position: relative;
        }

        .event-schedule-area-two ul.custom-tab li a {
            color: #252525;
            font-size: 25px;
            line-height: 25px;
            font-weight: 600;
            text-transform: capitalize;
            padding: 35px 0;
            position: relative;
        }

        .event-schedule-area-two ul.custom-tab li a:hover:before {
            width: 100%;
        }

        .event-schedule-area-two ul.custom-tab li a:before {
            position: absolute;
            left: 0;
            bottom: 0;
            content: "";
            background: #4125dd;
            width: 0;
            height: 2px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two ul.custom-tab li a.active {
            color: #4125dd;
        }

        .event-schedule-area-two .primary-btn {
            margin-top: 40px;
        }

        .event-schedule-area-two .tab-content .table {
            -webkit-box-shadow: 0 1px 30px rgba(0, 0, 0, 0.1);
            box-shadow: 0 1px 30px rgba(0, 0, 0, 0.1);
            margin-bottom: 0;
        }

        .event-schedule-area-two .tab-content .table thead {
            background-color: #007bff;
            color: #fff;
            font-size: 20px;
        }

        .event-schedule-area-two .tab-content .table thead tr th {
            padding: 20px;
            border: 0;
        }

        .event-schedule-area-two .tab-content .table tbody {
            background: #fff;
        }

        .event-schedule-area-two .tab-content .table tbody tr.inner-box {
            border-bottom: 1px solid #dee2e6;
        }

        .event-schedule-area-two .tab-content .table tbody tr th {
            border: 0;
            padding: 30px 20px;
            vertical-align: middle;
        }

        .event-schedule-area-two .tab-content .table tbody tr th .event-date {
            color: #252525;
            text-align: center;
        }

        .event-schedule-area-two .tab-content .table tbody tr th .event-date span {
            font-size: 50px;
            line-height: 50px;
            font-weight: normal;
        }

        .event-schedule-area-two .tab-content .table tbody tr td {
            padding: 30px 20px;
            vertical-align: middle;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .r-no span {
            color: #252525;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap h3 a {
            font-size: 20px;
            line-height: 20px;
            color: #cf057c;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap h3 a:hover {
            color: #4125dd;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            margin: 10px 0;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories a {
            color: #252525;
            font-size: 16px;
            margin-left: 10px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .categories a:before {
            content: "\f017";
            font-family: fontawesome;
            padding-right: 5px;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .time span {
            color: #252525;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            margin: 10px 0;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a {
            color: #4125dd;
            font-size: 16px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a:hover {
            color: #4125dd;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-wrap .organizers a:before {
            content: "\f007";
            font-family: fontawesome;
            padding-right: 5px;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .primary-btn {
            margin-top: 0;
            text-align: center;
        }

        .event-schedule-area-two .tab-content .table tbody tr td .event-img img {
            width: 100px;
            height: 100px;
            border-radius: 8px;
        }

    </style>

    <div class="event-schedule-area-two bg-color pad100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <div class="title-text">
                            <h2>Event Schedule</h2>
                        </div>
                        <p>
                            In ludus latine mea, eos paulo quaestio an. Meis possit ea sit. Vidisse molestie<br />
                            cum te, sea lorem instructior at.
                        </p>
                    </div>
                </div>
                <!-- /.col end-->
            </div>
            <!-- row end-->
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav custom-tab" id="myTab" role="tablist">
                    </ul>
                    <a class="btn btn-info" href="{{ route('alumni-agenda') }}">
                        <i class="fas fa-chevron-circle-left"></i> Kembali
                    </a>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="home" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" scope="col">Date</th>
                                            <th scope="col">Speakers</th>
                                            <th scope="col">Info Singkat</th>
                                            <th scope="col">Tempat</th>
                                            <th class="text-center" scope="col">Venue</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($agenda))
                                            @foreach ($agenda as $ag)
                                                <tr class="inner-box">
                                                    <th scope="row">
                                                        <div class="event-date">
                                                            <span>{{ (new \App\Helpers\Help())->getDayOnly($ag['tgl_mulai']) }}</span>
                                                            <p>{{ (new \App\Helpers\Help())->getMonthOnly($ag['tgl_mulai']) }}
                                                            </p>
                                                        </div>
                                                    </th>
                                                    <td>
                                                        <div class="event-img">
                                                            <img src="{{ $ag['avatar'] }}" alt="" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="event-wrap">
                                                            <h3><a href="javascript:void(0)" id="title"
                                                                    data-id="{{ $ag['id'] }}">{{ $ag['judul'] }}</a>
                                                            </h3>
                                                            <div class="meta">
                                                                <div class="organizers">
                                                                    <a href="#">{{ $ag['pembuat'] }}</a>
                                                                </div>
                                                                <div class="categories">
                                                                    <a href="#">{{ $ag['waktu'] }}</a>
                                                                </div>
                                                                <div class="time">
                                                                    <span>{{ (new \App\Helpers\Help())->getDayMonth($ag['tgl_mulai']) . ' - ' . (new \App\Helpers\Help())->getDayMonth($ag['tgl_selesai']) }}</span>
                                                                </div>
                                                                <div class="time">
                                                                    <span>Diposting : {{ $ag['dibuat'] }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="r-no">
                                                            <span>{{ $ag['tempat'] }}</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="primary-btn">
                                                            <a class="btn btn-info" href="javascript:void(0)"
                                                                data-id="{{ $ag['id'] }}" id="info"><i
                                                                    class="fas fa-info-circle"></i></a>
                                                            <a class="btn btn-success"
                                                                onclick="return confirm('Apa kamu yakin ingin mempubikasikan?')"
                                                                href="{{ route('alumni-agenda_update_status', ['id' => $ag['id'], 'params' => 'aktif']) }}"><i
                                                                    class="fas fa-check-circle"></i></a>
                                                            <a class="btn btn-danger"
                                                                onclick="return confirm('Apa kamu yakin ingin menghapus data ini?')"
                                                                href="{{ route('alumni-agenda_delete', $ag['id']) }}"><i
                                                                    class="fas fa-toilet-paper-slash"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5" style="text-align: center">Belum ada data yang tersedia</td>
                                            </tr>
                                        @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pricing6 py-5 bg-light">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-md-8 text-center">
                                            <h3 class="mb-3" id="title_detail">Pricing to make your Work Effective
                                            </h3>
                                            <h6 class="subtitle font-weight-normal" id="keterangan_detail">We offer 100%
                                                satisafaction and Money
                                                back Guarantee</h6>
                                        </div>
                                    </div>
                                    <!-- row  -->
                                    <div class="row mt-4">
                                        <!-- column  -->
                                        <div class="col-md-12">
                                            <div class="card card-shadow border-0 mb-4">
                                                <div class="card-body p-4">
                                                    <img id="previews"
                                                        src="https://via.placeholder.com/728x90.png"
                                                        alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- column  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#title, #info').click(function() {
                let id = $(this).data('id');
                $('#modelHeading').html("Detail Agenda");
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-agenda_get_by_id') }}",
                    data: {
                        id
                    },
                    success: function(data) {
                        console.log(data);
                        // $('#modelHeading').html("Edit Data Jurusan");
                        $('#title_detail').html(data.judul);
                        $('#keterangan_detail').html(data.keterangan);
                        if (data.file != null) {
                            $('#previews').attr('src', data.file);
                        }else{
                            $('#previews').attr('src', 'https://via.placeholder.com/728x90.png');
                        }
                        $('#ajaxModel').modal('show');
                    }
                });
            });
        })
    </script>
@endsection
