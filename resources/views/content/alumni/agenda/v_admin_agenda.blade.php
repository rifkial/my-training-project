@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')

    <div class="widget-bg">
        <div class="row">

            <div class="col-md-12 mb-2">
                <h4>Kalender</h4>
                <a href="{{ route('alumni-agenda_pending') }}" class="btn btn-info">
                    <i class="fas fa-bell"></i> Pending
                    <span class="badge badge-pill bg-primary">{{ $approve }}</span>
                </a>
            </div>
            <div class="col-md-12">
                <div id="calendar">

                </div>
            </div>
        </div>

    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="agendaModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleAgenda"></h5>
                </div>
                <div class="dataModal" style="width: 100%;">
                    <form id="formModalAgenda" action="javascript:void(0)" name="formModalAgenda" class="form-horizontal">
                        <div class="modal-body">
                            <span id="form_result"></span>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Judul</label>
                                <div class="col-md-9">
                                    <input type="text" name="judul_agenda" class="form-control" id="judul_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Tempat</label>
                                <div class="col-md-9">
                                    <input type="text" name="tempat_agenda" class="form-control" id="tempat_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l10">Waktu</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="id" id="id_agenda">
                                    <input type="hidden" name="start" id="start">
                                    <input type="hidden" name="end" id="end">
                                    <input type="time" name="waktu_agenda" class="form-control" id="waktu_agenda">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                                <div class="col-md-9">
                                    <textarea name="keterangan_agenda" id="keterangan_agenda" class="form-control"
                                        rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label class="col-md-3 col-form-label" for="l1"></label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="modal-previews" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="100%" style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6" style="position: relative">
                                            <div id="delete_foto_agenda" style="position: absolute; bottom: 0"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURLS(this);">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="actionAgenda" id="actionAgenda" value="Add" />
                            <button type="submit" class="btn btn-info" id="btnAgenda" value="create">Simpan</button>
                            <a href="javascript:void(0)" class="btn btn-danger" id="delAgenda" onclick="deleteAgenda()"><i
                                    class="fas fa-exclamation"></i> Hapus Agenda</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "",
                        type: "GET",
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    isi: value['keterangan'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value['keterangan'],
                                    file: value['file'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.isi != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' + event.waktu +
                            '</p></td></tr></table>');
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formModalAgenda').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#modal-previews').attr('src', 'https://via.placeholder.com/150');
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventClick: function(event, jsEvent, view) {
                    console.log(event);
                    $('#actionAgenda').val('Edit');
                    $('#judul_agenda').val(event.title);
                    $('#tempat_agenda').val(event.tempat);
                    $('#titleAgenda').html('Edit Agenda');
                    $('#id_agenda').val(event.id);
                    $('#delAgenda').show();
                    $('#waktu_agenda').val(event.waktu);
                    $('#keterangan_agenda').val(event.keterangan);
                    $('#start').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    if (event.file) {
                        $('#modal-previews').attr('src', event.file);
                    }
                    $('#agendaModal').modal('show');
                }
            });

            $(function() {
                $('body').on('submit', '#formModalAgenda', function(e) {
                    e.preventDefault();
                    $("#btnAgenda").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    $("#btnAgenda").attr("disabled", true);
                    let action_url = '';
                    if ($('#actionAgenda').val() == 'Add') {
                        action_url = "{{ route('alumni-agenda_create') }}";
                    }

                    if ($('#actionAgenda').val() == 'Edit') {
                        action_url = "{{ route('alumni-agenda_update') }}";
                    }

                    var formData = new FormData(this);
                    $.ajax({
                        type: "POST",
                        url: action_url,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            if (data.status == 'berhasil') {
                                $('#formModalAgenda').trigger("reset");
                                $('#agendaModal').modal('hide');
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                            noti(data.icon, data.message);
                            $('#btnAgenda').html('Simpan');
                            $("#btnAgenda").attr("disabled", false);

                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#saveBtn').html('Simpan');
                        }
                    });
                });
            })

        })

        function deleteAgenda() {
            var id = $('#id_agenda').val();
            swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                })
                .then(
                    function() {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('alumni-agenda_soft_delete') }}",
                            data: {
                                id
                            },
                            beforeSend: function() {
                                $("#delAgenda").html(
                                    '<i class="fa fa-spin fa-spinner"></i>');
                            },
                            success: function(data) {
                                if (data.status == 'berhasil') {
                                    $('#agendaModal').modal('hide');
                                    $('#calendar').fullCalendar('refetchEvents');
                                }
                                swa(data.status + "!", data.success, data.icon);
                                $("#delAgenda").html('<i class="fas fa-exclamation"></i> Hapus Agenda');
                            }
                        });
                    },
                    function(dismiss) {
                        if (dismiss === 'cancel') {
                            swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    }
                );
        }
    </script>

@endsection
