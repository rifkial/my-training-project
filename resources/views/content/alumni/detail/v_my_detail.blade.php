@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-12">
                <table>
                    <tr>
                        <td rowspan="8" class="text-center vertical-middle"><img src="{{ $alumni['file'] }}" alt=""
                                height="200"></td>
                        <td width="10"></td>
                        <th>Nama</th>
                        <td>:</td>
                        <td>{{ Str::upper($alumni['nama']) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Jurusan</th>
                        <td>:</td>
                        <td>{{ $alumni['jurusan'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>No HP</th>
                        <td>:</td>
                        <td>{{ $alumni['telepon'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Email</th>
                        <td>:</td>
                        <td>{{ $alumni['email'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Tahun Angkatan</th>
                        <td>:</td>
                        <td>{{ $alumni['tahun_angkatan'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Tahun Lulus</th>
                        <td>:</td>
                        <td>{{ $alumni['tahun_lulus'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Bio</th>
                        <td>:</td>
                        <td>{{ $alumni['bio'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Last Login</th>
                        <td>:</td>
                        <td>{{ $alumni['last_login'] != null ? (new \App\Helpers\Help())->getTanggalLengkap($alumni['last_login']) . ' dengan IP Adress ' . $alumni['last_ip'] : '-' }}
                        </td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        <hr class="mr-tb-40">
                    </div>
                </div>
                <div class="widget-body clearfix my-3">
                    <div class="tabs tabs-vertical">
                        <ul class="nav nav-tabs flex-column">
                            <li class="nav-item active"><a class="nav-link" href="#profile-biodata" data-toggle="tab"
                                    aria-expanded="true">Profile</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-pendidikan"
                                    data-toggle="tab" aria-expanded="true">Pendidikan</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-pekerjaan" data-toggle="tab"
                                    aria-expanded="true">Pekerjaan</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-organisasi"
                                    data-toggle="tab" aria-expanded="true">Organisasi</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-sosmed" data-toggle="tab"
                                    aria-expanded="true">Sosial Media</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-agenda" data-toggle="tab"
                                    aria-expanded="true">Agenda</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-artikel" data-toggle="tab"
                                    aria-expanded="true">Artikel</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#profile-galeri" data-toggle="tab"
                                    aria-expanded="true">Galeri</a>
                            </li>
                        </ul>
                        <!-- /.nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile-biodata">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Update Profile</h5>
                                </div>
                                <form method="POST" action="{{ route('update-profile_tanpa_slug') }}"
                                    enctype="multipart/form-data" onsubmit="return loader()">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l30">Nomor Induk</label>
                                                <input class="form-control" id="no_induk" name="no_induk" placeholder=""
                                                    value="{{ $alumni['no_induk'] }}" type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l31">Nama</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="nama" name="nama"
                                                        value="{{ $alumni['nama'] }}" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l32">Email</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="email" name="email"
                                                        value="{{ $alumni['email'] }}" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l33">Jurusan</label>
                                                <div class="input-group">
                                                    <select name="id_jurusan" class="form-control" id="id_jurusan">
                                                        <option value="">Pilih Jurusan..</option>
                                                        @foreach ($jurusan as $jr)
                                                            <option
                                                                value="{{ (new \App\Helpers\Help())->encode($jr['id']) }}">
                                                                {{ $jr['nama'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l32">Telepon</label>
                                                <div class="input-group">
                                                    <input class="form-control" id="telepon" name="telepon"
                                                        value="{{ $alumni['telepon'] }}" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l32">Agama</label>
                                                <div class="input-group">
                                                    <select name="agama" id="agama" class="form-control">
                                                        <option value="">Pilih Agama..</option>
                                                        <option value="islam"
                                                            {{ $alumni['agama'] == 'islam' ? 'selected' : '' }}>Islam
                                                        </option>
                                                        <option value="kristen"
                                                            {{ $alumni['agama'] == 'kristen' ? 'selected' : '' }}>Kristen
                                                        </option>
                                                        <option value="hindu"
                                                            {{ $alumni['agama'] == 'hindu' ? 'selected' : '' }}>Hindu
                                                        </option>
                                                        <option value="budha"
                                                            {{ $alumni['agama'] == 'budha' ? 'selected' : '' }}>Budha
                                                        </option>
                                                        <option value="katolik"
                                                            {{ $alumni['agama'] == 'katolik' ? 'selected' : '' }}>Katolik
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="l32">Jenis Kelamin</label>
                                                <div class="input-group">
                                                    <select name="jenkel" id="jenkel" class="form-control">
                                                        <option value="">Pilih Jenis Kelamin..</option>
                                                        <option value="l"
                                                            {{ $alumni['jenkel'] == 'l' ? 'selected' : '' }}>Laki - laki
                                                        </option>
                                                        <option value="p"
                                                            {{ $alumni['jenkel'] == 'p' ? 'selected' : '' }}>Perempuan
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="l36">Tempat Lahir</label>
                                                <input class="form-control" name="tempat_lahir" id="tempat_lahir"
                                                    type="text" value="{{ $alumni['tempat_lahir'] }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="l37">Tanggal Lahir</label>
                                                <input class="form-control" name="tgl_lahir" id="tgl_lahir"
                                                    value="{{ $alumni['tgl_lahir'] }}" type="date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="l38">Tahun Lulus</label>
                                        <input type="number" name="tahun_lulus" id="tahun_lulus"
                                            value="{{ $alumni['tahun_lulus'] }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="l38">No Ijazah</label>
                                        <input type="number" name="no_ijazah" id="no_ijazah"
                                            value="{{ $alumni['no_ijazah'] }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="l38">Alamat</label>
                                        <textarea class="form-control" id="alamat" name="alamat"
                                            rows="3">{{ $alumni['alamat'] }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="l39">File input</label>
                                        <br>
                                        <input id="l39" type="file">
                                        <br><small class="text-muted">Technical information for user</small>
                                    </div>
                                    <div class="form-actions btn-list">
                                        <button class="btn btn-primary" id="btnAddProfile" type="submit">Submit</button>
                                        <button class="btn btn-outline-default" type="button">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="profile-pendidikan">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Pendidikan</h5>
                                </div>
                                <form id="formPendidikan">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addPendidikan">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titlePendidikan">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input type="hidden" name="id" id="id_pendidikan">
                                                            <label for="l30">Institusi / Sekolah</label>
                                                            <input class="form-control" id="nama_sekolah" name="nama"
                                                                type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Jurusan</label>
                                                            <div class="input-group">
                                                                <input class="form-control" id="jurusan" name="jurusan"
                                                                    type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Jenjang</label>
                                                            <div class="input-group">
                                                                <select name="jenjang" class="form-control" id="jenjang">
                                                                    <option value="">Pilih jenjang..</option>
                                                                    <option value="s1">S1</option>
                                                                    <option value="d4">D4</option>
                                                                    <option value="d3">D3</option>
                                                                    <option value="sma">SMA Sederajat</option>
                                                                    <option value="smp">SMP Sederajat</option>
                                                                    <option value="sd">SD Sederajat</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l36">tahun Masuk</label>
                                                                    @php
                                                                        $firstYear = (int) date('Y');
                                                                        $lastYear = $firstYear - 20;
                                                                    @endphp
                                                                    <select name="tahun_masuk" id="tahun_masuk"
                                                                        class="form-control">
                                                                        @for ($year = $firstYear; $year >= $lastYear; $year--)
                                                                            <option value="{{ $year }}">
                                                                                {{ $year }}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l37">Tahun Lulus</label>
                                                                    <select name="tahun_keluar" id="tahun_keluar"
                                                                        class="form-control">
                                                                        @for ($year = $firstYear; $year >= $lastYear; $year--)
                                                                            <option value="{{ $year }}">
                                                                                {{ $year }}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Alamat Sekolah</label>
                                                            <div class="input-group">
                                                                <textarea name="alamat" id="alamat_sekolah"
                                                                    class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Status</label>
                                                            <div class="input-group">
                                                                <select name="status" class="form-control"
                                                                    id="status_pendidikan">
                                                                    <option value="">Pilih Status..</option>
                                                                    <option value="0">Disabled</option>
                                                                    <option value="1" selected>Enabled</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionPendidikan" value="Add" />
                                                            <button class="btn btn-success" type="submit"
                                                                id="btnPendidikan"><i class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger btnCancel"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddPendidikan"><i
                                                class="fas fa-plus"></i>
                                            Pendidikan</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th></th>
                                                    <th>Jenjang</th>
                                                    <th>Nama Sekolah</th>
                                                    <th>Jurusan</th>
                                                    <th>Tahun Angkatan</th>
                                                    <th>Alamat Sekolah</th>
                                                    <th></th>
                                                </tr>
                                                <tbody id="data_pendidikan">
                                                    @if (!empty($alumni['pendidikan']))
                                                        @foreach ($alumni['pendidikan'] as $pd)
                                                            <tr>
                                                                <td>
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $pd['status'] == 1 ? 'checked' : '' }}
                                                                            class="pendidikan_check"
                                                                            data-id="{{ $pd['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td>{{ Str::upper($pd['jenjang']) }}</td>
                                                                <td>{{ $pd['nama'] }}</td>
                                                                <td>{{ $pd['jurusan'] }}</td>
                                                                <td>Angkatan
                                                                    {{ $pd['th_masuk'] . ' - ' . $pd['th_lulus'] }}
                                                                </td>
                                                                <td>{{ $pd['alamat'] }}</td>
                                                                <td>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $pd['id'] }}"
                                                                        class="btn btn-info btn-sm editPendidikan"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $pd['id'] }}"
                                                                        class="btn btn-danger btn-sm deletePendidikan"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="7" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="profile-pekerjaan">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Pekerjaan</h5>
                                </div>
                                <form id="formPekerjaan">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addPekerjaan">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titlePekerjaan">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input type="hidden" name="id" id="id_pekerjaan">
                                                            <label for="l30">Posisi</label>
                                                            <input class="form-control" id="posisi" name="posisi"
                                                                type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Industri</label>
                                                            <div class="input-group">
                                                                <input class="form-control" id="pekerjaan_industri"
                                                                    name="industri" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Masa Kerja</label>
                                                            <div class="form-check">
                                                                <input class="form-check-input mx-0" name="checkOrg"
                                                                    type="checkbox" id="checkPekerjaan"
                                                                    onclick="displayPekerjaan()">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Masih Bekerja
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="aktifPekerjaan"
                                                            style="display: none">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="l36">Tanggal Mulai</label>
                                                                    <input type="date" name="tanggal_mulai_on"
                                                                        id="pekerjaan_tanggal_mulai_on"
                                                                        class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row" id="notPekerjaan">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l36">Tanggal Mulai</label>
                                                                    <input type="date" name="tgl_mulai" id="tgl_mulai"
                                                                        class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l37">Tanggal Selesai</label>
                                                                    <input type="date" name="tgl_selesai" id="tgl_selesai"
                                                                        class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Keterangan</label>
                                                            <div class="input-group">
                                                                <textarea name="keterangan" id="pekerjaan_keterangan"
                                                                    class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Status</label>
                                                            <div class="input-group">
                                                                <select name="status" class="form-control"
                                                                    id="status_pekerjaan">
                                                                    <option value="">Pilih Status..</option>
                                                                    <option value="0">Disabled</option>
                                                                    <option value="1" selected>Enabled</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionPekerjaan" value="Add" />
                                                            <button class="btn btn-success" type="submit"
                                                                id="btnPekerjaan"><i class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger cancelPekerjaan"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddPekerjaan"><i class="fas fa-plus"></i>
                                            Pekerjaan</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th rowspan="2"></th>
                                                    <th rowspan="2" class="vertical-middle">Posisi</th>
                                                    <th rowspan="2" class="vertical-middle">Industri</th>
                                                    <th colspan="2" class="text-center">Masa Kerja</th>
                                                    <th rowspan="2" class="vertical-middle">Keterangan</th>
                                                    <th rowspan="2"></th>
                                                </tr>
                                                <tr class="bg-info">
                                                    <th>Tanggal Mulai</th>
                                                    <th>Tanggal Selesai</th>
                                                </tr>
                                                <tbody id="data_pekerjaan">
                                                    @if (!empty($alumni['pekerjaan']))
                                                        @foreach ($alumni['pekerjaan'] as $pj)
                                                            <tr>
                                                                <td>
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $pj['status'] == 1 ? 'checked' : '' }}
                                                                            class="pekerjaan_check"
                                                                            data-id="{{ $pj['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td>{{ $pj['posisi'] }}</td>
                                                                <td>{{ $pj['industri'] }}</td>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($pj['tgl_mulai']) }}
                                                                </td>
                                                                <td>
                                                                    @if ($pj['aktif'] == 1)
                                                                        <p class="text-info">saat ini masih bekerja
                                                                        </p>
                                                                    @else
                                                                        {{ (new \App\Helpers\Help())->getTanggal($pj['tgl_selesai']) }}
                                                                    @endif
                                                                </td>
                                                                <td>{{ $pj['keterangan'] }}</td>
                                                                <td>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $pj['id'] }}"
                                                                        class="btn btn-info btn-sm editPekerjaan"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $pj['id'] }}"
                                                                        class="btn btn-danger btn-sm deletePekerjaan"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-organisasi">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Organisasi</h5>
                                </div>
                                <form id="formOrganisasi">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addOrganisasi">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titleOrganisasi">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input type="hidden" name="id" id="id_organisasi">
                                                            <label for="l30">Posisi</label>
                                                            <input class="form-control" id="posisi_organisasi"
                                                                name="posisi" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Organisasi</label>
                                                            <div class="input-group">
                                                                <input class="form-control" id="nama_organisasi"
                                                                    name="nama" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Masa Kerja</label>
                                                            <div class="form-check">
                                                                <input class="form-check-input mx-0" name="checkOrg"
                                                                    type="checkbox" id="checkOrganisasi"
                                                                    onclick="displayOrganisasi()">
                                                                <label class="form-check-label" for="flexCheckDefault">
                                                                    Masih Bekerja
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="aktifOrganisasi"
                                                            style="display: none">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="l36">Tanggal Mulai</label>
                                                                    <input type="date" name="tanggal_mulai_on"
                                                                        id="organisasi_tanggal_mulai_on"
                                                                        class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row" id="notOrganisasi">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l36">Tanggal Mulai</label>
                                                                    <input type="date" name="tgl_mulai"
                                                                        id="organisasi_tgl_mulai" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="l37">Tanggal Selesai</label>
                                                                    <input type="date" name="tgl_selesai"
                                                                        id="organisasi_tgl_selesai" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Keterangan</label>
                                                            <div class="input-group">
                                                                <textarea name="keterangan" id="organisasi_keterangan"
                                                                    class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Status</label>
                                                            <div class="input-group">
                                                                <select name="status" class="form-control"
                                                                    id="status_organisasi">
                                                                    <option value="">Pilih Status..</option>
                                                                    <option value="0">Disabled</option>
                                                                    <option value="1" selected>Enabled</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionOrganisasi" value="Add" />
                                                            <button class="btn btn-success" type="submit"
                                                                id="btnOrganisasi"><i class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger cancelOrganisasi"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddOrganisasi"><i
                                                class="fas fa-plus"></i>
                                            Organisasi</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th rowspan="2"></th>
                                                    <th rowspan="2" class="vertical-middle">Posisi</th>
                                                    <th rowspan="2" class="vertical-middle">Organisasi</th>
                                                    <th colspan="2" class="text-center">Masa Partisipasi</th>
                                                    <th rowspan="2" class="vertical-middle">Keterangan</th>
                                                    <th rowspan="2"></th>
                                                </tr>
                                                <tr class="bg-info">
                                                    <th>Tanggal Mulai</th>
                                                    <th>Tanggal Selesai</th>
                                                </tr>
                                                <tbody id="data_organisasi">
                                                    @if (!empty($alumni['organisasi']))
                                                        @foreach ($alumni['organisasi'] as $og)
                                                            <tr>
                                                                <td>
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $og['status'] == 1 ? 'checked' : '' }}
                                                                            class="organisasi_check"
                                                                            data-id="{{ $og['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td>{{ $og['posisi'] }}</td>
                                                                <td>{{ $og['organisasi'] }}</td>
                                                                <td>{{ (new \App\Helpers\Help())->getTanggal($og['tgl_mulai']) }}
                                                                </td>
                                                                <td>
                                                                    @if ($og['aktif'] == 1)
                                                                        <p class="text-info">saat ini masih bekerja
                                                                        </p>
                                                                    @else
                                                                        {{ (new \App\Helpers\Help())->getTanggal($og['tgl_selesai']) }}
                                                                    @endif
                                                                </td>
                                                                <td>{{ $og['keterangan'] }}</td>
                                                                <td>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $og['id'] }}"
                                                                        class="btn btn-info btn-sm editOrganiasi"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $og['id'] }}"
                                                                        class="btn btn-danger btn-sm deleteOrganisasi"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="7" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-sosmed">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Sosial Media</h5>
                                </div>
                                <form id="formSosmed">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addSosmed">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titleSosmed">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <div class="form-group">
                                                                    <input type="hidden" name="id" id="id_sosmed">
                                                                    <label for="l36">Jenis Sosmed</label>
                                                                    <select name="media_sosmed" id="media_sosmed"
                                                                        class="form-control">
                                                                        <option value="facebook">Facebook</option>
                                                                        <option value="instagram">Instagram</option>
                                                                        <option value="linkedin">Linkedin</option>
                                                                        <option value="twitter">Twiiter</option>
                                                                        <option value="website">Website</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <div class="form-group">
                                                                    <label for="l37">URL Sosmed</label>
                                                                    <input class="form-control" type="url" name="nama"
                                                                        id="nama_sosmed">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Status</label>
                                                            <div class="input-group">
                                                                <select name="status" class="form-control"
                                                                    id="status_sosmed">
                                                                    <option value="">Pilih Status..</option>
                                                                    <option value="0">Disabled</option>
                                                                    <option value="1" selected>Enabled</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionSosmed" value="Add" />
                                                            <button class="btn btn-success" type="submit" id="btnSosmed"><i
                                                                    class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger cancelSosmed"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddSosmed"><i class="fas fa-plus"></i>
                                            Sosial Media</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th></th>
                                                    <th>Sosial Media</th>
                                                    <th>URL Akun Profile</th>
                                                    <th></th>
                                                </tr>
                                                <tbody id="data_sosmed">
                                                    @if (!empty($alumni['sosial']))
                                                        @foreach ($alumni['sosial'] as $ss)
                                                            <tr>
                                                                <td>
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $ss['status'] == 1 ? 'checked' : '' }}
                                                                            class="organisasi_check"
                                                                            data-id="{{ $ss['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td>{{ $ss['media'] }}</td>
                                                                <td><a href="{{ $ss['nama'] }}" class="text-info"> {{ Str::limit($ss['nama'], 50, '...') }}</a></td>
                                                                
                                                                <td>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $ss['id'] }}"
                                                                        class="btn btn-info btn-sm editSosmed"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $ss['id'] }}"
                                                                        class="btn btn-danger btn-sm deleteSosmed"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="4" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-agenda">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Agenda</h5>
                                </div>
                                <div id="detailAg">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-artikel">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Artikel</h5>
                                </div>
                                <form id="formArtikel">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addArtikel">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titleArtikel">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input type="hidden" name="id" id="id_artikel">
                                                            <label for="l30">Kategori</label>
                                                            <select name="id_kategori" id="id_kategori_blog"
                                                                class="form-control">
                                                                <option value="">Pilih kategori blog..</option>
                                                                @foreach ($kategori as $kt)
                                                                    <option
                                                                        value="{{ (new \App\Helpers\Help())->encode($kt['id']) }}">
                                                                        {{ $kt['nama'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Judul</label>
                                                            <div class="input-group">
                                                                <input class="form-control" id="judul_blog" name="judul"
                                                                    type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Isi</label>
                                                            <div class="input-group">
                                                                <textarea name="isi" id="blog_isi"
                                                                    class="form-control editor" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l39">File input</label>
                                                            <br>
                                                            <input id="l16" type="file" name="image" accept="image/*">
                                                            <br><small class="text-muted">Dijadikan Sampul Blog</small>
                                                        </div>
                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionArtikel" value="Add" />
                                                            <button class="btn btn-success" type="submit"
                                                                id="btnArtikel"><i class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger cancelArtikel"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddArtikel"><i class="fas fa-plus"></i>
                                            Artikel</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th></th>
                                                    <th class="vertical-middle">Artikel</th>
                                                    <th class="vertical-middle">Kategori</th>
                                                    <th class="text-center">Published</th>
                                                    <th></th>
                                                </tr>
                                                <tbody id="data_artikel">
                                                    @if (!empty($alumni['blog']))
                                                        @foreach ($alumni['blog'] as $bl)
                                                            <tr>
                                                                <td class="vertical-middle">
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $bl['status'] == 1 ? 'checked' : '' }}
                                                                            class="artikel_check"
                                                                            data-id="{{ $bl['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td class="vertical-middle">
                                                                    <a
                                                                        href="{{ route('alumni-blog_detail', ['id' => (new \App\Helpers\Help())->encode($bl['id']), 'title' => Str::slug($bl['judul'], '-')]) }}">
                                                                        <b
                                                                            class="text-dark">{!! Str::limit($bl['judul'], 50, ' ...') !!}</b>
                                                                        <span class="text-dark">by
                                                                            {{ $bl['pembuat'] }}</span>
                                                                        <p class="m-0"><small
                                                                                class="text-dark">{{ $bl['dilihat'] == null ? 0 : $bl['dilihat'] }}x
                                                                                Dilihat</small></p>
                                                                        <p class="m-0 text-info">
                                                                            {{ Request::getSchemeAndHttpHost() . '/program/alumni/blog/detail/' . (new \App\Helpers\Help())->encode($bl['id']) . '/' . Str::slug($bl['judul'], '-') }}
                                                                        </p>
                                                                    </a>
                                                                </td>
                                                                <td class="vertical-middle">{{ $bl['kategori'] }}</td>
                                                                <td class="vertical-middle">
                                                                    {{ (new \App\Helpers\Help())->getTanggalLengkap($bl['created_at']) }}
                                                                </td>
                                                                <td class="vertical-middle">
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $bl['id'] }}"
                                                                        class="btn btn-info btn-sm editArtikel"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $bl['id'] }}"
                                                                        class="btn btn-danger btn-sm deleteArtikel"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="5" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-galeri">
                                <div class="bg-purple my-3">
                                    <h5 class="box-title p-2 text-white">Galeri</h5>
                                </div>
                                <form id="formGaleri">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="accordian-body collapse" id="addGaleri">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h5 class="box-title" id="titleGaleri">Tambah</h5>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="l31">Kategori</label>
                                                            <div class="input-group">
                                                                <select name="id_kategori" id="id_kategori_galeri"
                                                                    class="form-control">
                                                                    <option value="">Pilih Kategori..</option>
                                                                    @foreach ($katGaleri as $kg)
                                                                        <option value="{{ $kg['id'] }}">
                                                                            {{ $kg['nama'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="hidden" name="id" id="id_galeri">
                                                            <label for="l30">Nama</label>
                                                            <input class="form-control" id="nama_galeri" name="nama"
                                                                type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l31">Keterangan</label>
                                                            <div class="input-group">
                                                                <input class="form-control" id="keterangan_galeri"
                                                                    name="nama" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="l39">File input</label>
                                                            <br>
                                                            <input type="file" name="image" accept="image/*">
                                                            <br><small class="text-muted">Wajib diisi</small>
                                                        </div>

                                                        <div class="form-actions btn-list">
                                                            <input type="hidden" id="actionGaleri" value="Add" />
                                                            <button class="btn btn-success" type="submit" id="btnGaleri"><i
                                                                    class="fas fa-check-circle"></i>
                                                                Konfirmasi</button>
                                                            <button class="btn btn-outline-danger cancelGaleri"
                                                                type="button"><i class="fas fa-times-circle"></i>
                                                                Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-info btn-sm btnAddGaleri"><i class="fas fa-plus"></i>
                                            Galeri</button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <p></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tr class="bg-info">
                                                    <th>Gambar</th>
                                                    <th>Deskripsi</th>
                                                    <th>Kategori</th>
                                                    <th>Status</th>
                                                    <th>Published</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                <tbody id="data_galeri">
                                                    @if (!empty($alumni['galeri']))
                                                        @foreach ($alumni['galeri'] as $glr)
                                                            <tr>
                                                                <td><img src="{{ $glr['file'] }}" alt="" height="50">
                                                                </td>
                                                                <td>
                                                                    <b class="text-dark">{!! $glr['nama'] !!}</b>
                                                                    <span class="text-dark">by
                                                                        {{ $glr['pembuat'] }}</span>
                                                                    <br>
                                                                    <small class="m-0 text-dark">
                                                                        {!! Str::limit($glr['keterangan'], 100, ' ...') !!}
                                                                    </small>
                                                                </td>
                                                                <td>{{ $glr['kategori'] }}</td>
                                                                <td>
                                                                    <label class="switch">
                                                                        <input type="checkbox"
                                                                            {{ $glr['status'] == 1 ? 'checked' : '' }}
                                                                            class="galeri_check"
                                                                            data-id="{{ $glr['id'] }}">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    {{ (new \App\Helpers\Help())->getTanggalLengkap($glr['created_at']) }}
                                                                    <br>
                                                                    <small>{{ $glr['dibuat'] }}</small>
                                                                </td>

                                                                <td>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $glr['id'] }}"
                                                                        class="btn btn-info btn-sm editGaleri"><i
                                                                            class="fas fa-pencil-alt"></i></a>
                                                                    <a href="javascript:void(0)"
                                                                        data-id="{{ $glr['id'] }}"
                                                                        class="btn btn-danger btn-sm deleteGaleri"><i
                                                                            class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6" class="text-center">Data saat ini belum
                                                                tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    {{-- <div class="tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="nav-item active col-12">
                                <a class="nav-link" href="#detail1" data-toggle="tab" aria-expanded="true"
                                    onclick="load_nav('profil', 1)">Profil</a>
                            </li>
                            <li class="nav-item col-12"><a class="nav-link" href="#detail4" data-toggle="tab"
                                    aria-expanded="true" onclick="load_nav('pendidikan', 4)">Pendidikan</a>
                            </li>
                            <li class="nav-item col-12"><a class="nav-link" href="#detail2" data-toggle="tab"
                                    aria-expanded="true" onclick="load_nav('pekerjaan', 2)">Pekerjaan</a>
                            </li>
                            <li class="nav-item col-12"><a class="nav-link" href="#detail5" data-toggle="tab"
                                    aria-expanded="true" onclick="load_nav('organisasi', 5)">Organisasi</a>
                            </li>
                            <li class="nav-item col-12"><a class="nav-link" href="#detail6" data-toggle="tab"
                                    aria-expanded="true" onclick="load_nav('sosial', 6)">Sosial Media</a>
                            </li>
                            <li class="col-12 nav-item">
                                <a class="nav-link" href="#agendaDetail" id="detailAgenda" data-toggle="tab"
                                    aria-expanded="true">Agenda</a>
                            </li>
                            <li class="col-12 nav-item"><a class="nav-link" href="#detail3" data-toggle="tab"
                                    aria-expanded="true" onclick="load_nav('blog', 3)">Artikel</a>
                            </li>
                            <li class="nav-item col-12"><a class="nav-link" href="#galeri" data-toggle="tab"
                                    aria-expanded="true">Galeri</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane result_detail active" id="detail1">
                                <div id="loading"></div>
                            </div>
                            @for ($i = 2; $i <= 6; $i++)
                                <div class="tab-pane result_detail" id="detail{{ $i }}">
                                </div>
                            @endfor
                            <div class="tab-pane" id="agendaDetail">
                                <div id="detailAg">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                            <div class="tab-pane" id="galeri">
                                <div class="row photos">
                                    @if (!empty($alumni['galeri']))
                                        @foreach ($alumni['galeri'] as $gl)
                                            <div class="col-sm-6 col-md-4 col-lg-3 item">
                                                <a href="{{ $gl['file'] }}" data-lightbox="photos">
                                                    <img class="img-fluid" src="{{ $gl['file'] }}">
                                                </a>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-md-12">
                                            <p style="text-align: center">mohon maaf data galeri alumni saat ini kosong
                                            </p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    {{-- @include('content.alumni.detail.helper.detail.v_modal_pendidikan')
    @include('content.alumni.detail.helper.detail.v_modal_organisasi')
    @include('content.alumni.detail.helper.detail.v_modal_pekerjaan')
    @include('content.alumni.detail.helper.detail.v_modal_blog')
    @include('content.alumni.detail.helper.detail.v_modal_agenda')
    @include('content.alumni.detail.helper.detail.v_modal_sosial') --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //Proses Update Profile

            //Proses Pendidikan
            $(document).on('click', '.btnAddPendidikan', function() {
                $('#formPendidikan').trigger("reset");
                $('#titlePendidikan').html("Tambah Pendidikan");
                $('#actionPendidikan').val('Add')
                $('#addPendidikan').collapse('show');
            })

            $(document).on('click', '.btnCancel', function() {
                $('#formPendidikan').trigger("reset");
                $('#addPendidikan').collapse('hide');
            })

            $('body').on('submit', '#formPendidikan', function(e) {
                e.preventDefault();
                $("#btnPendidikan").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnPendidikan").attr("disabled", true);
                var action_url = '';

                if ($('#actionPendidikan').val() == 'Add') {
                    action_url = "{{ route('alumni-pendidikan_create') }}";
                    method_url = "POST";
                }

                if ($('#actionPendidikan').val() == 'Edit') {
                    action_url = "{{ route('alumni-pendidikan_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            console.log("tes");
                            $('#addPendidikan').collapse('hide');
                            $('#formPendidikan').trigger("reset");
                            $('#data_pendidikan').html(data.pendidikan);
                        }
                        noti(data.icon, data.message);
                        $('#btnPendidikan').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnPendidikan").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPendidikan').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editPendidikan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-pendidikan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePendidikan').html("Edit Pendidikan");
                        $('#id_pendidikan').val(data.id);
                        $('#nama_sekolah').val(data.nama);
                        $('#jurusan').val(data.jurusan);
                        $('#jenjang').val(data.jenjang).trigger('change');
                        $('#tahun_masuk').val(data.th_masuk);
                        $('#tahun_keluar').val(data.th_lulus);
                        $('#alamat_sekolah').val(data.alamat);
                        $('#status_pendidikan').val(data.status).trigger('change');
                        $('#addPendidikan').collapse('show');
                        $('#actionPendidikan').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deletePendidikan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pendidikan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pendidikan').html(data.pendidikan);
                                $('#addPendidikan').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            //Detail Pekerjaan
            $(document).on('click', '.btnAddPekerjaan', function() {
                $('#formPekerjaan').trigger("reset");
                $('#titlePekerjaan').html("Tambah Pekerjaan");
                $('#actionPekerjaan').val('Add')
                $('#addPekerjaan').collapse('show');
            })

            $(document).on('click', '.cancelPekerjaan', function() {
                $('#formPekerjaan').trigger("reset");
                $('#addPekerjaan').collapse('hide');
            })

            $('body').on('submit', '#formPekerjaan', function(e) {
                e.preventDefault();
                $("#btnPekerjaan").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnPekerjaan").attr("disabled", true);
                var action_url = '';

                if ($('#actionPekerjaan').val() == 'Add') {
                    action_url = "{{ route('alumni-pekerjaan_create') }}";
                    method_url = "POST";
                }

                if ($('#actionPekerjaan').val() == 'Edit') {
                    action_url = "{{ route('alumni-pekerjaan_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addPekerjaan').collapse('hide');
                            $('#formPekerjaan').trigger("reset");
                            $('#data_pekerjaan').html(data.pekerjaan);
                        }
                        noti(data.icon, data.message);
                        $('#btnPekerjaan').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnPekerjaan").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnPekerjaan').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editPekerjaan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-pekerjaan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titlePekerjaan').html("Edit Pekerjaan");
                        $('#id_pekerjaan').val(data.id);
                        $('#posisi').val(data.posisi);
                        $('#pekerjaan_industri').val(data.industri);
                        if (data.aktif == 1) {
                            $("#checkPekerjaan").prop("checked", true);
                            $('#pekerjaan_tanggal_mulai_on').val(data.tgl_mulai);
                            $("#aktifPekerjaan").show(); // checked
                            $('#notPekerjaan').hide();
                        } else {
                            $("#checkPekerjaan").prop("checked", false);
                            $('#tgl_mulai').val(data.tgl_mulai);
                            $('#tgl_selesai').val(data.tgl_selesai);
                            $("#aktifPekerjaan").hide(); // checked
                            $('#notPekerjaan').show();
                        }
                        $('#pekerjaan_keterangan').val(data.keterangan);

                        $('#addPekerjaan').collapse('show');
                        $('#actionPekerjaan').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deletePekerjaan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-pekerjaan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_pekerjaan').html(data.pekerjaan);
                                $('#addPekerjaan').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Organisasi
            $(document).on('click', '.btnAddOrganisasi', function() {
                $('#formOrganisasi').trigger("reset");
                $('#titleOrganisasi').html("Tambah Organisasi");
                $('#actionOrganisasi').val('Add')
                $('#addOrganisasi').collapse('show');
            })

            $(document).on('click', '.cancelOrganisasi', function() {
                $('#formOrganisasi').trigger("reset");
                $('#addOrganisasi').collapse('hide');
            })

            $('body').on('submit', '#formOrganisasi', function(e) {
                e.preventDefault();
                $("#btnOrganisasi").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnOrganisasi").attr("disabled", true);
                var action_url = '';

                if ($('#actionOrganisasi').val() == 'Add') {
                    action_url = "{{ route('alumni-organisasi_create') }}";
                    method_url = "POST";
                }

                if ($('#actionOrganisasi').val() == 'Edit') {
                    action_url = "{{ route('alumni-organisasi_update') }}";
                    method_url = "PUT";
                }

                var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addOrganisasi').collapse('hide');
                            $('#formOrganisasi').trigger("reset");
                            $('#data_organisasi').html(data.organisasi);
                        }
                        noti(data.icon, data.message);
                        $('#btnOrganisasi').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnOrganisasi").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnOrganisasi').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editOrganiasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-organisasi_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleOrganisasi').html("Edit Organisasi");
                        $('#id_organisasi').val(data.id);
                        $('#posisi_organisasi').val(data.posisi);
                        $('#nama_organisasi').val(data.organisasi);
                        if (data.aktif == 1) {
                            $("#checkOrganisasi").prop("checked", true);
                            $('#organisasi_tanggal_mulai_on').val(data.tgl_mulai);
                            $("#aktifOrganisasi").show(); // checked
                            $('#notOrganisasi').hide();
                        } else {
                            $("#checkOrganisasi").prop("checked", false);
                            $('#organisasi_tgl_mulai').val(data.tgl_mulai);
                            $('#organisasi_tgl_selesai').val(data.tgl_selesai);
                            $("#aktifOrganisasi").hide(); // checked
                            $('#notOrganisasi').show();
                        }
                        $('#organisasi_keterangan').val(data.keterangan);

                        $('#addOrganisasi').collapse('show');
                        $('#actionOrganisasi').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteOrganisasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-organisasi_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_organisasi').html(data.organisasi);
                                $('#addOrganisasi').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Sosisal Media
            $(document).on('click', '.btnAddSosmed', function() {
                $('#formSosmed').trigger("reset");
                $('#titleSosmed').html("Tambah Sosial Media");
                $('#actionSosmed').val('Add')
                $('#addSosmed').collapse('show');
            })

            $(document).on('click', '.cancelSosmed', function() {
                $('#formSosmed').trigger("reset");
                $('#addSosmed').collapse('hide');
            })

            $('body').on('submit', '#formSosmed', function(e) {
                e.preventDefault();
                $("#btnSosmed").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnSosmed").attr("disabled", true);
                var action_url = '';

                if ($('#actionSosmed').val() == 'Add') {
                    action_url = "{{ route('alumni-sosial_create') }}";
                    method_url = "POST";
                }

                if ($('#actionSosmed').val() == 'Edit') {
                    action_url = "{{ route('alumni-sosial_update') }}";
                    method_url = "PUT";
                }

                // var formData = new FormData(this);
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addSosmed').collapse('hide');
                            $('#formSosmed').trigger("reset");
                            $('#data_sosmed').html(data.sosial);
                        }
                        noti(data.icon, data.message);
                        $('#btnSosmed').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnSosmed").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnSosmed').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editSosmed', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-sosial_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleOrganisasi').html("Edit Organisasi");
                        $('#id_sosmed').val(data.id);
                        $('#media_sosmed').val(data.media).trigger('change');
                        $('#nama_sosmed').val(data.nama);
                        $('#addSosmed').collapse('show');
                        $('#actionSosmed').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteSosmed', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-sosial_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_sosmed').html(data.sosial);
                                $('#addSosmed').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            //Detail Agenda

            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function(e) {


            })
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "{{ route('alumni-get_agenda') }}",
                        type: "POST",
                        data: {
                            params: "auth"
                        },
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    isi: value['keterangan'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value[
                                        'keterangan'],
                                    file: value['file'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.isi != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' +
                            event.waktu +
                            '</p></td></tr></table>');
                        // element.children().last().append(
                        //     "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Isi : " +
                        //     event
                        //     .isi + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: '/admin/master/kalender_rombel/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' +
                            end + '&id=' + event.id + '&isi=' + event.isi,
                        type: "PUT",
                        success: function(data) {
                            noti(data.icon, data.success)
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    // console.log(event);
                    $('#actionAgenda').val('Edit');
                    $('#judul_agenda').val(event.title);
                    $('#tempat_agenda').val(event.tempat);
                    $('#titleAgenda').html('Edit Agenda');
                    $('#id_agenda').val(event.id);
                    $('#delAgenda').show();
                    $('#waktu_agenda').val(event.waktu);
                    $('#keterangan_agenda').val(event.keterangan),
                        $('#start').val($.fullCalendar.formatDate(event.start,
                            "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    if (event.file) {
                        $('#modal-previews').attr('src', event.file);
                    }
                    $('#agendaModal').modal('show');
                }
            });
            // calendar.render();
            $('#calendar').fullCalendar('render');
            $('.tabs .nav li a').click(function() {
                $('#calendar').fullCalendar('render');
            });



            $('body').on('submit', '#formModalAgenda', function(e) {
                e.preventDefault();
                $("#btnAgenda").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnAgenda").attr("disabled", true);
                let action_url = '';
                if ($('#actionAgenda').val() == 'Add') {
                    action_url = "{{ route('alumni-agenda_create') }}";
                }

                if ($('#actionAgenda').val() == 'Edit') {
                    action_url = "{{ route('alumni-agenda_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#formModalAgenda').trigger("reset");
                            $('#agendaModal').modal('hide');
                            $('#calendar').fullCalendar('refetchEvents');
                        }
                        noti(data.icon, data.message);
                        $('#btnAgenda').html('Simpan');
                        $("#btnAgenda").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            //Detail Artikel
            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });

            $(document).on('click', '.btnAddArtikel', function() {
                $('#formArtikel').trigger("reset");
                $('#titleArtikel').html("Tambah Artikel");
                $('#actionArtikel').val('Add')
                $('#addArtikel').collapse('show');
            })

            $(document).on('click', '.cancelArtikel', function() {
                $('#formArtikel').trigger("reset");
                $('#addArtikel').collapse('hide');
            })

            $('body').on('submit', '#formArtikel', function(e) {
                e.preventDefault();
                $("#btnArtikel").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnArtikel").attr("disabled", true);
                var action_url = '';

                if ($('#actionArtikel').val() == 'Add') {
                    action_url = "{{ route('alumni-blog_create') }}";
                }

                if ($('#actionArtikel').val() == 'Edit') {
                    action_url = "{{ route('alumni-blog_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addArtikel').collapse('hide');
                            $('#formArtikel').trigger("reset");
                            $('#data_artikel').html(data.blog);
                        }
                        noti(data.icon, data.message);
                        $('#btnArtikel').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnArtikel").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnArtikel').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editArtikel', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-blog_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleArtikel').html("Edit Artikel");
                        $('#id_artikel').val(data.id);
                        $('#id_kategori_blog').val(data.id_kategori);
                        $('#judul_blog').val(data.judul);
                        tinymce.activeEditor.setContent(data.isi);
                        $('#addArtikel').collapse('show');
                        $('#actionArtikel').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteArtikel', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-data_blog_soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_artikel').html(data.blog);
                                $('#addArtikel').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


            //Detail Galeri
            $(document).on('click', '.btnAddGaleri', function() {
                $('#formGaleri').trigger("reset");
                $('#titleGaleri').html("Tambah Galeri");
                $('#actionGaleri').val('Add')
                $('#addGaleri').collapse('show');
            })

            $(document).on('click', '.cancelGaleri', function() {
                $('#formGaleri').trigger("reset");
                $('#addGaleri').collapse('hide');
            })

            $('body').on('submit', '#formGaleri', function(e) {
                e.preventDefault();
                $("#btnGaleri").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnGaleri").attr("disabled", true);
                var action_url = '';

                if ($('#actionArtikel').val() == 'Add') {
                    action_url = "{{ route('alumni-galeri_create') }}";
                }

                if ($('#actionArtikel').val() == 'Edit') {
                    action_url = "{{ route('alumni-galeri_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#addGaleri').collapse('hide');
                            $('#formGaleri').trigger("reset");
                            $('#data_galeri').html(data.galeri);
                        }
                        noti(data.icon, data.message);
                        $('#btnGaleri').html(
                            '<i class="fas fa-check-circle"></i> Konfirmasi');
                        $("#btnGaleri").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnGaleri').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.editGaleri', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-organisasi_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleGaleri').html("Edit Organisasi");
                        $('#id_galeri').val(data.id);
                        $('#posisi_organisasi').val(data.posisi);
                        $('#nama_organisasi').val(data.organisasi);

                        $('#organisasi_keterangan').val(data.keterangan);

                        $('#addGaleri').collapse('show');
                        $('#actionGaleri').val('Edit')
                    }
                });
            });

            $(document).on('click', '.deleteGaleri', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-galeri_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_galeri').html(data.galeri);
                                $('#addGaleri').collapse('hide');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        })

        function displayPekerjaan() {
            if ($("#checkPekerjaan").is(':checked')) {
                $("#aktifPekerjaan").show();
                $('#notPekerjaan').hide();
            } else {
                $("#aktifPekerjaan").hide();
                $('#notPekerjaan').show();
            }
        }

        function displayOrganisasi() {
            if ($("#checkOrganisasi").is(':checked')) {
                $("#aktifOrganisasi").show();
                $('#notOrganisasi').hide();
            } else {
                $("#aktifOrganisasi").hide();
                $('#notOrganisasi').show();
            }
        }

        function loader() {
            $("#btnAddProfile").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btnAddProfile").attr("disabled", true);
        }
    </script>
@endsection
