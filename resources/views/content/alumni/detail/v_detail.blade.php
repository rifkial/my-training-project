@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <div class="row py-5 px-4">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header bg-info text-center">
                    <b class="">TENTANG USER</b>
                </div>
                <div class="card-header bg-secondary text-center">
                    <b class="">{{ ucwords($alumni['nama']) }}</b>
                </div>
                <div class="card-body">
                    <img src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
                        alt="">
                </div>
            </div>

        </div>
        <div class="col-md-9">
            <div class="card border border-info">
                <div class="card-header bg-info">
                    <b class="">PROFILE USER</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-stripped">
                                <tr>
                                    <td><b>Nama</b></td>
                                    <td>{{ ucwords($alumni['nama']) }}</td>
                                </tr>
                                <tr>
                                    <td><b>Email</b></td>
                                    <td>{{ $alumni['email'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Jurusan</b></td>
                                    <td>{{ $alumni['jurusan'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-stripped">
                                <tr>
                                    <td><b>Ponsel</b></td>
                                    <td>{{ $alumni['telepon'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Angkatan</b></td>
                                    <td>{{ $alumni['tahun_angkatan'] }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tahun Lulus</b></td>
                                    <td>{{ $alumni['tahun_lulus'] }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">Sosial Media</b>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="mr-b-20 btn-list">
                                            @if (!empty($alumni['sosial']))
                                                @foreach ($alumni['sosial'] as $sosial)
                                                    @php
                                                        $sosmed = 'facebook';
                                                        $btn_sosmed = 'facebook';
                                                        if ($sosial['media'] == 'website') {
                                                            $sosmed = 'wordpress';
                                                            $btn_sosmed = 'instagram';
                                                        }elseif($sosial['media'] == 'linkedin'){
                                                            $sosmed = 'linkedin';
                                                            $btn_sosmed = 'linkedin';
                                                        }elseif($sosial['media'] == 'instagram'){
                                                            $sosmed = 'instagram';
                                                            $btn_sosmed = 'pink';
                                                        }elseif($sosial['media'] == 'twitter'){
                                                            $sosmed = 'twitter';
                                                            $btn_sosmed = 'twitter';
                                                        }
                                                    @endphp
                                                    <a href="{{ $sosial['nama'] }}" target="_blank" class="btn btn-sm btn-{{ $btn_sosmed }}"><i
                                                            class="social-icons list-icon">{{ $sosmed }}</i> {{ ucwords($sosial['media']) }}</a>
                                                @endforeach
                                            @else
                                                <p class="text-danger m-0">Data saat ini tidak tersedia</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">GALERI</b>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        @foreach ($alumni['galeri'] as $glr)
                                            <div class="col-md-4 mr-b-30">
                                                <div class="card">
                                                    <img class="card-img-top" src="{{ $glr['file'] }}" alt="">
                                                    <div class="card-body">
                                                        <p><b>{{ Str::upper($glr['nama']) }}</b></p>
                                                        <p class="card-text"><b>{{ $glr['kategori'] }}</b>.
                                                            {{ $glr['keterangan'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">Pendidikan</b>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-facebook">
                                            <th class="text-white text-center">Jenjang</th>
                                            <th class="text-white">Universitas/Sekolah</th>
                                            <th class="text-white text-center">Angkatan</th>
                                        </tr>
                                        <tbody>
                                            @if (!empty($alumni['pendidikan']))
                                                @foreach ($alumni['pendidikan'] as $pd)
                                                    <tr>
                                                        <td class="vertical-middle text-center">
                                                            <h3 class="font-weight-bold">{{ Str::upper($pd['jenjang']) }}
                                                            </h3>
                                                        </td>
                                                        <td class="vertical-middle">
                                                            <b>{{ $pd['nama'] }}</b>
                                                            <br><small>Jurusan
                                                                {{ $pd['jurusan'] != null ? $pd['jurusan'] : '-' }}</small>
                                                            <p>Alamat : {{ $pd['alamat'] != null ? $pd['alamat'] : '-' }}
                                                            </p>
                                                        </td>
                                                        <td class="vertical-middle text-center">
                                                            <b>{{ $pd['th_masuk'] }} -
                                                                {{ $pd['th_lulus'] != null ? $pd['th_lulus'] : '-' }}</b>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data saat ini tidak tersedia</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">Pekerjaan</b>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-facebook">
                                            <th class="text-white text-center">Posisi</th>
                                            <th class="text-white">Industri</th>
                                            <th class="text-white text-center">Masa Kerja</th>
                                        </tr>
                                        <tbody>
                                            @if (!empty($alumni['pekerjaan']))
                                                @foreach ($alumni['pekerjaan'] as $pk)
                                                    <tr>
                                                        <td class="vertical-middle text-center">
                                                            <h4 class="font-weight-bold">{{ Str::upper($pk['posisi']) }}
                                                            </h4>
                                                        </td>
                                                        <td class="vertical-middle">
                                                            <b>{{ $pk['industri'] }}</b>
                                                            <br><small>Keterangan :
                                                                {{ $pk['keterangan'] != null ? $pk['keterangan'] : '-' }}</small>
                                                        </td>
                                                        <td class="vertical-middle text-center">
                                                            <b>{{ $pk['tgl_mulai'] }} -
                                                                {{ $pk['aktif'] == 0 ? $pk['tgl_selesai'] : ' saat ini' }}</b>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data saat ini tidak tersedia</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">Organisasi</b>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-facebook">
                                            <th class="text-white text-center">Posisi</th>
                                            <th class="text-white">Organisasi</th>
                                            <th class="text-white text-center">Masa Organisasi</th>
                                        </tr>
                                        <tbody>
                                            @if (!empty($alumni['organisasi']))
                                                @foreach ($alumni['organisasi'] as $og)
                                                    <tr>
                                                        <td class="vertical-middle text-center">
                                                            <h3 class="font-weight-bold">{{ Str::upper($og['posisi']) }}
                                                            </h3>
                                                        </td>
                                                        <td class="vertical-middle">
                                                            <b>{{ $og['organisasi'] }}</b>
                                                            <br>
                                                            <small>Keterangan :
                                                                {{ $og['keterangan'] != null ? $og['keterangan'] : '-' }}</small>
                                                        </td>
                                                        <td class="vertical-middle text-center">
                                                            <b>{{ $og['tgl_mulai'] }} -
                                                                {{ $og['aktif'] == 0 ? $og['tgl_selesai'] : ' saat ini' }}</b>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Data saat ini tidak tersedia</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card border border-white">
                                <div class="card-header bg-purple text-white">
                                    <b class="">Artikel</b>
                                </div>
                                <div class="card-body">
                                    @if (!empty($alumni['blog']))
                                        @foreach ($alumni['blog'] as $blog)
                                            <div class="col-md-12 post">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4>
                                                            <strong><a
                                                                    href="http://www.jquery2dotnet.com/2013/12/cool-share-button-effects-styles.html"
                                                                    class="text-info">{{ Str::limit($blog['judul'], 100, '...') }}</a></strong>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span class="fas fa-user"></span> by <a
                                                            href="#">{{ $blog['pembuat'] }}</a> | <span
                                                            class="fas fa-calendar-alt">
                                                        </span>
                                                        {{ (new \App\Helpers\Help())->getTanggalLengkap($blog['created_at']) }}
                                                        | <i class="fas fa-eye"></i>
                                                        {{ $blog['dilihat'] == null ? 0 : $blog['dilihat'] }}x Dilihat
                                                    </div>
                                                </div>
                                                <div class="row post-content">
                                                    <div class="col-md-3 d-flex align-items-center">
                                                        <a href="#">
                                                            <img src="http://4.bp.blogspot.com/-_lqoNpVXeU4/UkxQ7N-QW8I/AAAAAAAACTw/pni-TZyp17o/s1600/cool+share+button+effects+styles.png"
                                                                alt="" class="img-responsive">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-9">

                                                        <p>
                                                            {{ Str::limit(strip_tags($blog['isi']), 200, '...') }}
                                                        </p>
                                                        <p>
                                                            <a class="btn btn-info"
                                                                href="http://www.jquery2dotnet.com/2013/12/cool-share-button-effects-styles.html">Read
                                                                more</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    @else

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        {{-- <div class="col-md-12">
            <div class="bg-white shadow rounded overflow-hidden">
                <div class="px-4 pt-0 pb-4 cover">
                    <div class="media align-items-end profile-head">
                        <div class="profile mr-3"><img
                                src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
                                alt="..." width="130" class="rounded m-2 img-thumbnail"><a href="#"
                                class="btn btn-outline-dark btn-sm btn-block">Edit profile</a></div>
                        <div class="media-body mb-5 text-white">
                            <h4 class="mt-0 mb-0">{{ $alumni['nama'] }}</h4>
                            <p class="small mb-4"> <i class="fas fa-hammer mr-2"></i>{{ $alumni['jurusan'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="bg-light p-4 d-flex justify-content-end text-center">
                    <ul class="list-inline mb-0">
                        <a href="#pendidikan">
                            <li class="list-inline-item">
                                <h5 class="font-weight-bold mb-0 d-block">{{ count($alumni['pendidikan']) }}</h5>
                                <small class="text-muted"> <i class="fas fa-graduation-cap mr-1"></i>Pendidikan</small>
                            </li>
                        </a>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">{{ count($alumni['organisasi']) }}</h5>
                            <small class="text-muted"> <i class="fas fa-hiking mr-1"></i>Organisasi</small>
                        </li>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">{{ count($alumni['pendidikan']) }}</h5>
                            <small class="text-muted"> <i class="fas fa-city mr-1"></i>Pekerjaan</small>
                        </li>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">{{ count($alumni['galeri']) }}</h5><small
                                class="text-muted"> <i class="fas fa-image mr-1"></i>Photos</small>
                        </li>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">{{ count($alumni['blog']) }}</h5><small
                                class="text-muted"> <i class="fas fa-rss mr-1"></i>Artikel</small>
                        </li>
                    </ul>
                </div>
                <div class="px-4 py-3">
                    <h4 class="mb-0">Informasi</h4>
                    <div class="p-4 rounded shadow-sm bg-light">
                        <div class="row">
                            <div class="col-md-4">
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['nama']) ? $alumni['nama'] : '-' }}</h5><small
                                    class="text-muted">Nama</small>
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['jurusan']) ? $alumni['jurusan'] : '-' }}</h5><small
                                    class="text-muted">Jurusan</small>
                            </div>
                            <div class="col-md-4">
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['email']) ? $alumni['email'] : '-' }}</h5><small
                                    class="text-muted">Email</small>
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['telepon']) ? $alumni['telepon'] : '-' }}</h5><small
                                    class="text-muted">Telepon</small>
                            </div>
                            <div class="col-md-4">
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['tahun_angkatan']) ? $alumni['tahun_angkatan'] : '-' }}</h5><small
                                    class="text-muted">Tahun Angkatan</small>
                                <h5 class="font-weight-bold mb-0 d-block">
                                    {{ !empty($alumni['tahun_lulus']) ? $alumni['tahun_lulus'] : '-' }}</h5><small
                                    class="text-muted">Tahun Lulus</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-4 px-4">
                    <div class="card border border-info">
                        <div class="card-header bg-info">
                            <h2 class="box-title mt-1 text-white">Galeri</h2>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @if (!empty($alumni['galeri']))
                                    @foreach ($alumni['galeri'] as $glr)
                                        <div class="col-lg-4 m-2 p-1"><img
                                                src="https://images.unsplash.com/photo-1469594292607-7bd90f8d3ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
                                                alt="" class="img-fluid rounded shadow-sm"></div>
                                    @endforeach
                                @else
                                    <div class="col-md-12">
                                        Data saat ini tidak tersedia
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <hr class="mr-tb-40">
                </div>

                <div class="py-4 px-4">
                    <div class="d-flex align-items-center justify-content-between mb-1" id="pendidikan">
                        <h4 class="mb-0">Pendidikan</h4>
                    </div>
                    <div class="row">
                        @if (!empty($alumni['pendidikan']))
                            @foreach ($alumni['galeri'] as $glr)
                                <div class="col-lg-4 m-2 p-1"><img
                                        src="https://images.unsplash.com/photo-1469594292607-7bd90f8d3ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
                                        alt="" class="img-fluid rounded shadow-sm"></div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                Data saat ini tidak tersedia
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <hr class="mr-tb-40">
                </div>
                <div class="py-4 px-4">
                    <div class="d-flex align-items-center justify-content-between mb-1" id="pendidikan">
                        <h4 class="mb-0">Organisasi</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <p></p>
                                <table class="table table-hover">
                                    @if (!empty($alumni['organisasi']))

                                    @else
                                        <tr>
                                            <td colspan="4" class="text-danger">Data saat ini belum tersedia
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr class="mr-tb-40">
                </div>
                <div class="py-4 px-4">
                    <div class="d-flex align-items-center justify-content-between mb-1" id="pendidikan">
                        <h4 class="mb-0">Artikel</h4>
                    </div>
                    <div class="row">
                        @if (!empty($alumni['blog']))
                            @foreach ($alumni['blog'] as $blog)
                                <div class="col-md-12 post">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>
                                                <strong><a
                                                        href="http://www.jquery2dotnet.com/2013/12/cool-share-button-effects-styles.html"
                                                        class="text-info">{{ Str::limit($blog['judul'], 100, '...') }}</a></strong>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="fas fa-user"></span> by <a
                                                href="#">{{ $blog['pembuat'] }}</a> | <span class="fas fa-calendar-alt">
                                            </span>
                                            {{ (new \App\Helpers\Help())->getTanggalLengkap($blog['created_at']) }} | <i
                                                class="fas fa-eye"></i>
                                            {{ $blog['dilihat'] == null ? 0 : $blog['dilihat'] }}x Dilihat
                                        </div>
                                    </div>
                                    <div class="row post-content">
                                        <div class="col-md-3 d-flex align-items-center">
                                            <a href="#">
                                                <img src="http://4.bp.blogspot.com/-_lqoNpVXeU4/UkxQ7N-QW8I/AAAAAAAACTw/pni-TZyp17o/s1600/cool+share+button+effects+styles.png"
                                                    alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <p>
                                                {{ Str::limit($blog['isi'], 200, '...') }}
                                            </p>
                                            <p>
                                                <a class="btn btn-info"
                                                    href="http://www.jquery2dotnet.com/2013/12/cool-share-button-effects-styles.html">Read
                                                    more</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        @else

                        @endif
                    </div>
                </div>
            </div>
        </div> --}}
    </div>



    {{-- <div class="col-md-9 widget-bg">
        <div class="ui-bg-overlay bg-dark opacity-50"></div>
        <div class="container">
            <div class="media col-md-10 col-lg-8 col-xl-10 p-0 my-4 mx-auto">
                <img src="{{ $alumni['file'] }}" alt class="d-block ui-w-100 rounded-circle">
                <div class="media-body ml-5">
                    <h4 class="font-weight-bold mb-0">{{ ucfirst($alumni['nama']) }}</h4>
                    <div class="mb-4">
                        <small class="mb-4">Terkhir login :
                            {{ $alumni['last_login'] != null ? (new \App\Helpers\Help())->getTanggalLengkap($alumni['last_login']) . ' dengan IP Adress ' . $alumni['last_ip'] : '-' }}</small>
                    </div>
                    <div class="opacity-75 mb-4">
                        Lorem ipsum dolor sit amet, nibh suavitate qualisque ut nam. Ad harum primis electram
                        duo,
                        porro principes ei has.
                    </div>
                    <a href="#" class="d-inline-block text-white">
                        <strong>234</strong>
                        <span class="opacity-75">followers</span>
                    </a>
                    <a href="#" class="d-inline-block text-white ml-3">
                        <strong>111</strong>
                        <span class="opacity-75">following</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="widget-body clearfix">
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item active col-12">
                        <a class="nav-link" href="#detail1" data-toggle="tab" aria-expanded="true"
                            onclick="load_nav('profil', 1)">Profil</a>
                    </li>
                    <li class="nav-item col-12"><a class="nav-link" href="#detail4" data-toggle="tab"
                            aria-expanded="true" onclick="load_nav('pendidikan', 4)">Pendidikan</a>
                    </li>
                    <li class="nav-item col-12"><a class="nav-link" href="#detail2" data-toggle="tab"
                            aria-expanded="true" onclick="load_nav('pekerjaan', 2)">Pekerjaan</a>
                    </li>
                    <li class="nav-item col-12"><a class="nav-link" href="#detail5" data-toggle="tab"
                            aria-expanded="true" onclick="load_nav('organisasi', 5)">Organisasi</a>
                    </li>
                    <li class="nav-item col-12"><a class="nav-link" href="#detail6" data-toggle="tab"
                            aria-expanded="true" onclick="load_nav('sosial', 6)">Sosial Media</a>
                    </li>
                    <li class="col-12 nav-item">
                        <a class="nav-link" href="#agendaDetail" id="detailAgenda" data-toggle="tab"
                            aria-expanded="true">Agenda</a>
                    </li>
                    <li class="col-12 nav-item"><a class="nav-link" href="#detail3" data-toggle="tab"
                            aria-expanded="true" onclick="load_nav('blog', 3)">Artikel</a>
                    </li>
                    <li class="nav-item col-12"><a class="nav-link" href="#galeri" data-toggle="tab"
                            aria-expanded="true">Galeri</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane result_detail active" id="detail1">
                        <div id="loading"></div>
                    </div>
                    @for ($i = 2; $i <= 6; $i++)
                        <div class="tab-pane result_detail" id="detail{{ $i }}">
                        </div>
                    @endfor
                    <div class="tab-pane" id="agendaDetail">
                        <div id="detailAg">
                            <div id="calendar"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="galeri">
                        <div class="row photos">
                            @if (!empty($alumni['galeri']))
                                @foreach ($alumni['galeri'] as $gl)
                                    <div class="col-sm-6 col-md-4 col-lg-3 item">
                                        <a href="{{ $gl['file'] }}" data-lightbox="photos">
                                            <img class="img-fluid" src="{{ $gl['file'] }}">
                                        </a>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-md-12">
                                    <p style="text-align: center">mohon maaf data galeri alumni saat ini kosong</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {
            load_nav('profil', 1)
            $('#createData').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Alumni");
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('body').on('submit', '#alumniForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('alumni-alumni_create') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#resultAlumni').html('<div id="loading" style="" ></div>');
                    },
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#alumniForm').trigger("reset");
                        }
                        $("#resultAlumni").load(window.location.href +
                            " #resultAlumni");
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });




        })

        function editLoad(id) {
            $(".btnEdit" + id).html('<i class="fa fa-spin fa-spinner"></i> Loading');
        }

        function changeStatus(val, id) {
            if (id == 1) {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Tidak Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="Tidak Aktif"]')
                    .removeClass('active').addClass('notActive');
            }
            updateStatus(val, id);
        }

        function updateStatus(value, id) {

            $.ajax({
                type: "POST",
                url: "{{ route('update_status-user_alumni') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.success, data.message);
                }
            });
        }



        function load_nav(params, id) {

            let action_url = '';
            if (params == 'profil') {
                action_url = "{{ route('detail-data_alumni_profile') }}";
            }
            if (params == 'blog') {
                action_url = "{{ route('alumni-data_blog_detail') }}";
            }
            if (params == 'pendidikan') {
                action_url = "{{ route('alumni-pendidikan_by_alumni') }}";
            }

            if (params == 'organisasi') {
                action_url = "{{ route('alumni-organisasi_detail') }}";
            }

            if (params == 'pekerjaan') {
                action_url = "{{ route('alumni-pekerjaan_detail_by_alumni') }}";
            }

            if (params == 'sosial') {
                action_url = "{{ route('alumni-sosial_detail') }}";
            }

            $.ajax({
                type: "POST",
                url: action_url,
                data: {
                    params: "alumni",
                    id: "{{ $alumni['id'] }}"
                },
                beforeSend: function() {
                    $("#detail" + id).html('<div id="loading"></div>');
                },
                success: function(data) {
                    $("#detail" + id).html(data);
                }
            });
        }

        // Form Organisasi

        function pencarianBlog(obj) {

            $.ajax({
                url: "{{ route('alumni-search_blog') }}",
                method: "POST",
                data: $("#" + obj.id).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $("#hasil_pencarian_blog").html('<div id="loading"></div>');
                    $("#btn-search").html('<span class="fa fa-spin fa-spinner"></span>');
                },
                success: function(data) {
                    $("#hasil_pencarian_blog").html(data);
                    // if (data.status == 'berhasil') {
                    // } else {
                    //     $("#hasil_pencarian_blog").load(window.location.href +
                    //         " #hasil_pencarian_blog");
                    // }
                    $("#btn-search").html('<span class="fas fa-search"></span>');
                    // noti(data.icon, data.message);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }


        function btnAddBlog() {
            $('#titleBlog').html("Tambah Blog");
            $('#modalBlog').modal('show');
            $('#actionBlog').val('Add');
            $('#delete_foto').html('');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        }

        function formTambahBlog(obj) {
            $("#addBlog").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            var formData = new FormData(obj);
            $.ajax({
                url: "{{ route('alumni-blog_create') }}",
                method: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                beforeSend: function() {
                    $("#detail3").html('<div id="loading"></div>');
                },
                success: function(data) {
                    $("#detail3").html(data.html);
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        }
        //Blog Artikel

        // Agenda

        $(document).on('shown.bs.tab', 'a[id="detailAgenda"]', function(e) {
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: "{{ route('alumni-get_agenda') }}",
                        type: "POST",
                        data: {
                            params: "alumni",
                            id: "{{ $alumni['id'] }}"
                        },
                        success: function(obj) {
                            var events = [];
                            $.each(obj, function(index, value) {
                                events.push({
                                    id: value['id'],
                                    start: value['tgl_mulai'],
                                    end: value['tgl_selesai'],
                                    title: value['judul'],
                                    isi: value['keterangan'],
                                    waktu: value['waktu'],
                                    tempat: value['tempat'],
                                    keterangan: value['keterangan'],
                                    file: value['file'],
                                });
                            });
                            callback(events);
                        }

                    });
                },
                displayEventTime: false,
                eventColor: '#2471d2',
                eventTextColor: '#FFF',
                editable: true,
                eventRender: function(event, element, view) {
                    if (event.isi != null) {
                        element.children().last().html(
                            '<table><tr><td style="width: 57px; vertical-align: middle" rowspan="2"><img src="' +
                            event.file +
                            '" class="media-object img-thumbnail" style="width: 42px; height: 42px;border-radius: 50%;" /></td><td> <b>Title :</b> <p>' +
                            event.title + '</p></td></tr><tr><td><b>Waktu :</b><p>' + event.waktu +
                            '</p></td></tr></table>');
                        // element.children().last().append(
                        //     "<br><span class='catatanGuru' style='background-color: #fff; color: #2471d2'>Isi : " +
                        //     event
                        //     .isi + "</span>");
                    }
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    var tanggal = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    $('#formUpdateKalender').trigger("reset");
                    $('#actionAgenda').val('Add');
                    $('#titleAgenda').html('Tambah Agenda');
                    $('#delAgenda').hide();
                    $('#start').val($.fullCalendar.formatDate(start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(end,
                        "Y-MM-DD HH:mm:ss"));
                    $('#agendaModal').modal('show');
                },
                eventDrop: function(event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: '/admin/master/kalender_rombel/update',
                        data: 'title=' + event.title + '&start=' + start + '&end=' +
                            end + '&id=' + event.id + '&isi=' + event.isi,
                        type: "PUT",
                        success: function(data) {
                            noti(data.icon, data.success)
                        }
                    });
                },
                eventClick: function(event, jsEvent, view) {
                    console.log(event);
                    $('#actionAgenda').val('Edit');
                    $('#judul_agenda').val(event.title);
                    $('#tempat_agenda').val(event.tempat);
                    $('#titleAgenda').html('Edit Agenda');
                    $('#id_agenda').val(event.id);
                    $('#delAgenda').show();
                    $('#waktu_agenda').val(event.waktu);
                    $('#keterangan_agenda').val(event.keterangan);
                    $('#start').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    $('#end').val($.fullCalendar.formatDate(event.start,
                        "Y-MM-DD HH:mm:ss"));
                    if (event.file) {
                        $('#modal-previews').attr('src', event.file);
                    }
                    $('#agendaModal').modal('show');
                }
            });

        })
    </script>
@endsection
