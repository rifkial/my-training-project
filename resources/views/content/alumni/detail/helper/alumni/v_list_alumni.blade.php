<div class="card">
    <div class="card-body">
        <div class="media"
            style="display: flex; align-items: center; justify-content: center">
            <div class="image" style="width: 115px; display: flex; align-items: center; justify-content: center">
                <center>
                    <span
                        style="background-image: url('{{ env('API_URL') . $alm['file'] }}')"
                        class="avatar avatar-xl mb-3" id="avt">
                    </span>
                    @if (session('role') == 'admin-alumni')
                        <div class="input-group" style="">
                            <div id="radioBtn" class="btn-group">
                                <a class="btn btn-info btn-xs {{ $alm['status'] == 1 ? 'active' : 'notActive' }}"
                                    onclick="changeStatus(1, {{ $alm['id'] }})"
                                    data-toggle="pilihan_status{{ $alm['id'] }}"
                                    data-title="Aktif">Aktif</a>
                                <a class="btn btn-info btn-xs {{ $alm['status'] != 1 ? 'active' : 'notActive' }}"
                                    onclick="changeStatus(0, {{ $alm['id'] }})"
                                    data-toggle="pilihan_status{{ $alm['id'] }}"
                                    data-title="Tidak Aktif">Tidak</a>
                            </div>
                        </div>
                    @endif
                </center>
            </div>
            
            <div class="media-body" style="position: relative">
                
                <h4>{{ $alm['nama'] }}</h4>
                <p class="text-muted mb-0">Jurusan :
                    {{ $alm['nama_jurusan'] != null ? $alm['nama_jurusan'] : '-' }}
                </p>
                <p class="text-muted mb-0">Email :
                    {{ $alm['email'] != null ? $alm['email'] : '-' }}</p>
                <p class="text-muted mb-0">Alamat :
                    {{ $alm['alamat'] != null ? $alm['alamat'] : '-' }}
                </p>
                <ul class="social-links list-inline mb-0 mt-2">
                    <li class="list-inline-item">
                        <a href="{{ route('detail-alumni_alumni', $alm['id']) }}"
                            data-toggle="tooltip" data-placement="top"
                            class="btn btn-info btn-sm"
                            data-original-title="Detail Alumni">
                            <i class="fas fa-info-circle"></i> Detail
                        </a>
                    </li>
                    @if (session('role') == 'admin-alumni')
                        <li class="list-inline-item">
                            <a href="{{ route('alumni-alumni_edit', $alm['id']) }}"
                                data-toggle="tooltip" data-placement="top"
                                class="btn btn-success btn-sm btnEdit{{ $alm['id'] }}"
                                data-original-title="Edit Profil" onclick="editLoad({{ $alm['id'] }})">
                                <i class="fas fa-user-edit"></i> Edit Profil
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>