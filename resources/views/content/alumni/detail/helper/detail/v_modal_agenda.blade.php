<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="agendaModal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="titleAgenda"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalAgenda" action="javascript:void(0)" name="formModalAgenda" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Judul</label>
                            <div class="col-md-9">
                                <input type="text" name="judul_agenda" class="form-control" id="judul_agenda">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Tempat</label>
                            <div class="col-md-9">
                                <input type="text" name="tempat_agenda" class="form-control" id="tempat_agenda">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Waktu</label>
                            <div class="col-md-9">
                                <input type="hidden" name="id" id="id_agenda">
                                <input type="hidden" name="start" id="start">
                                <input type="hidden" name="end" id="end">
                                <input type="time" name="waktu_agenda" class="form-control" id="waktu_agenda">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                            <div class="col-md-9">
                                <textarea name="keterangan_agenda" id="keterangan_agenda" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-md-3 col-form-label" for="l1"></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img id="modal-previews" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group mb-1" width="100%" style="margin-top: 10px">
                                    </div>
                                    <div class="col-md-6" style="position: relative">
                                        <div id="delete_foto_agenda" style="position: absolute; bottom: 0"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label pt-1" for="l1">Gambar</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*"
                                        onchange="readURLS(this);">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="actionAgenda" id="actionAgenda" value="Add" />
                        <button type="submit" class="btn btn-info" id="btnAgenda" value="create">Simpan</button>
                        <a href="javascript:void(0)" class="btn btn-danger" id="delAgenda" onclick="deleteAgenda()"><i class="fas fa-exclamation"></i> Hapus Agenda</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
