<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalAjax" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="modelTitle"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalPendidikan" action="javascript:void(0)" name="formModalPendidikan" onsubmit="addPendidikan(this)" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_pendidikan">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Institusi/
                                Sekolah</label>
                            <div class="col-md-9">
                                <input class="form-control" name="nama" id="nama_pendidikan" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Jurusan</label>
                            <div class="col-md-9">
                                <input type="text" name="jurusan" class="form-control" id="jurusan_pendidikan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Jenjang</label>
                            <div class="col-md-9">
                                <select name="jenjang" class="form-control" id="jenjang_pendidikan">
                                    <option value="s1">S1</option>
                                    <option value="d4">D4</option>
                                    <option value="d3">D3</option>
                                    <option value="sma">SMA Sederajat</option>
                                    <option value="smp">SMP Sederajat</option>
                                    <option value="sd">SD Sederajat</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Angkatan</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="tahun_masuk" id="tahun_masuk_pendidikan" class="form-control select3">
                                            <option value="">Tahun Masuk</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="tahun_keluar" class="form-control select3" id="tahun_keluar_pendidikan">
                                            <option value="">Tahun Lulus</option>
                                            @php
                                                $firstYear = (int) date('Y');
                                                $lastYear = $firstYear - 20;
                                                for ($year = $firstYear; $year >= $lastYear; $year--) {
                                                    echo '<option value=' . $year . '>' . $year . '</option>';
                                                }
                                            @endphp
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Alamat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="alamat" id="alamat_pendidikan" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionPendidikan" value="Add" />
                        <button type="submit" class="btn btn-info" id="saveBtn" value="create">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
