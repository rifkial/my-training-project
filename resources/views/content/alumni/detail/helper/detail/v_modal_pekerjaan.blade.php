<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPekerjaan" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="titlePekerjaan"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalPekerjaan" action="javascript:void(0)" name="formModalPekerjaan" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_pekerjaan">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Posisi</label>
                            <div class="col-md-9">
                                <input class="form-control" name="posisi" id="pekerjaan_posisi" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Industri</label>
                            <div class="col-md-9">
                                <input type="text" name="industri" class="form-control" id="pekerjaan_industri">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                            <div class="col-md-9">
                                <textarea name="keterangan" id="pekerjaan_keterangan" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Masa Kerja</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" name="checkOrg" id="checkPekerjaan" onclick="displayPekerjaan()">
                                        <label for="" style="font-weight: normal">Masih Aktif</label>
                                    </div>
                                    <div class="col-md-12" id="aktifPekerjaan" style="display: none">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai_on" id="pekerjaan_tanggal_mulai_on"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row" id="notPekerjaan" style="display: none">
                                    <div class="col-md-6">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai" id="pekerjaan_tanggal_mulai"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <small>*tanggal selesai</small>
                                        <input type="date" name="tanggal_selesai" id="pekerjaan_tanggal_selesai"
                                            class="form-control" value="tanggal selesai">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionPekerjaan" value="Add" />
                        <button type="submit" class="btn btn-info" id="btnPkr" value="create">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- <script>
    displayPekerjaan();

    function displayPekerjaan() {
        if ($("#checkPekejaan").is(':checked')) {
            $("#aktifPekerjaan").show(); // checked
            $('#notPekerjaan').hide();
        } else {
            $("#aktifPekerjaan").hide(); // unchecked
            $('#notPekerjaan').show();
        }
    }
</script> --}}
