<div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalOrg" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-inverse" style="background: #03a9f3;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="titleOrganisasi"></h5>
            </div>
            <div class="dataModal" style="width: 100%;">
                <form id="formModalOrganisasi" action="javascript:void(0)" name="formModalOrganisasi" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="form-group row">
                            <input type="hidden" name="id" id="id_organisasi">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Posisi</label>
                            <div class="col-md-9">
                                <input class="form-control" name="posisi" id="posisi" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">organisasi</label>
                            <div class="col-md-9">
                                <input type="text" name="nama_organisasi" class="form-control" id="nama_organisasi">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Keterangan</label>
                            <div class="col-md-9">
                                <textarea name="keterangan" id="keterangan" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Waktu</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" name="checkOrg" id="checkOrg" onclick="displayOrg()">
                                        <label for="" style="font-weight: normal">Masih Aktif</label>
                                    </div>
                                    <div class="col-md-12" id="aktifOrg" style="display: none">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai_on" id="tanggal_mulai_on"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row" id="notOrg" style="display: none">
                                    <div class="col-md-6">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai" id="tanggal_mulai"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <small>*tanggal selesai</small>
                                        <input type="date" name="tanggal_selesai" id="tanggal_selesai"
                                            class="form-control" value="tanggal selesai">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionOrg" value="Add" />
                        <button type="submit" class="btn btn-info" id="btnOrg" value="create">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    displayOrg();

    function displayOrg() {
        if ($("#checkOrg").is(':checked')) {
            $("#aktifOrg").show(); // checked
            $('#notOrg').hide();
        } else {
            $("#aktifOrg").hide(); // unchecked
            $('#notOrg').show();
        }
    }
</script>
