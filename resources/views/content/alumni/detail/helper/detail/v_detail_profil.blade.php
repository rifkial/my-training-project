<style>
    .stretch-card>.card {
        width: 100%;
        min-width: 100%
    }

    body {
        background-color: #f9f9fa
    }

    .flex {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto
    }

    @media (max-width:991.98px) {
        .padding {
            padding: 1.5rem
        }
    }

    @media (max-width:767.98px) {
        .padding {
            padding: 1rem
        }
    }

    .padding {
        padding: 3rem
    }

    .card {
        box-shadow: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        -ms-box-shadow: none
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #3da5f;
        border-radius: 0
    }

    .card .card-body {
        padding: 1.25rem 1.75rem
    }

    .card .card-title {
        color: #000000;
        margin-bottom: 0.625rem;
        text-transform: capitalize;
        font-size: 0.875rem;
        font-weight: 500
    }

    .card .card-description {
        margin-bottom: .875rem;
        font-weight: 400;
        color: #76838f
    }

    .form-group label {
        font-size: 0.875rem;
        line-height: 1.4rem;
        vertical-align: top;
        margin-bottom: .5rem
    }

    .editable-form .editable {
        color: #000;
        font-size: .8125rem
    }

    .editable-form .editable-click {
        border-color: #000
    }

    .editable-container.editable-inline {
        max-width: 100%
    }

    .editable-container.editable-inline .editableform {
        max-width: 100%
    }

    .editable-container.editable-inline .editableform .control-group {
        max-width: 100%;
        white-space: initial
    }

    .editable-container.editable-inline .editableform .control-group>div {
        max-width: 100%
    }

    .editable-container.editable-inline .editableform .control-group .editable-input input,
    .editable-container.editable-inline .editableform .control-group .editable-input textarea {
        max-width: 100%;
        width: 100%
    }

    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .form-control,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .asColorPicker-input,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .dataTables_wrapper select,
    .dataTables_wrapper .editable-container.editable-inline .editableform .control-group .editable-input .combodate select,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .jsgrid .jsgrid-table .jsgrid-filter-row input[type=text],
    .jsgrid .jsgrid-table .jsgrid-filter-row .editable-container.editable-inline .editableform .control-group .editable-input .combodate input[type=text],
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .jsgrid .jsgrid-table .jsgrid-filter-row select,
    .jsgrid .jsgrid-table .jsgrid-filter-row .editable-container.editable-inline .editableform .control-group .editable-input .combodate select,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .jsgrid .jsgrid-table .jsgrid-filter-row input[type=number],
    .jsgrid .jsgrid-table .jsgrid-filter-row .editable-container.editable-inline .editableform .control-group .editable-input .combodate input[type=number],
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .select2-container--default .select2-selection--single,
    .select2-container--default .editable-container.editable-inline .editableform .control-group .editable-input .combodate .select2-selection--single,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .select2-container--default .select2-selection--single .select2-search__field,
    .select2-container--default .select2-selection--single .editable-container.editable-inline .editableform .control-group .editable-input .combodate .select2-search__field,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .typeahead,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .tt-query,
    .editable-container.editable-inline .editableform .control-group .editable-input .combodate .tt-hint {
        padding-left: 0;
        padding-right: 0
    }

    @media (max-width: 991px) {
        .editable-container.editable-inline .editableform .control-group .editable-buttons {
            display: block;
            margin-top: 10px
        }
    }

    .btn-group-sm>.btn,
    .btn-sm {
        padding: 0.49rem .5rem;
        font-size: .875rem;
        line-height: 1.5;
        border-radius: .2rem
    }

    .btn-warning {
        color: #fff
    }

</style>
<div class="row">
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">NAMA</h6>
        <p class="mr-t-0">{{ ucfirst($alumni['nama']) }}</p>
    </div>
    <!-- /.col-md-6 -->
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">JURUSAN</h6>
        <p class="mr-t-0">{{ $alumni['jurusan'] }}</p>
    </div>
    <!-- /.col-md-6 -->
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Nomor Induk</h6>
        <p class="mr-t-0">{{ $alumni['no_induk'] }}</p>
    </div>
    <!-- /.col-md-6 -->
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Telepon</h6>
        <p class="mr-t-0">
            {{ $alumni['telepon'] != null ? $alumni['telepon'] : '-' }}</p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Jenis Kelamin</h6>
        <p class="mr-t-0">{{ $alumni['jenkel'] }}</p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Agama</h6>
        <p class="mr-t-0">{{ $alumni['agama'] != null ? $alumni['agama'] : '-' }}
        </p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Tempat, Tanggal Lahir</h6>
        <p class="mr-t-0">
            {{ $alumni['tempat_lahir'] . ', ' . $alumni['tgl_lahir'] }}
        </p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Tahun Lulus</h6>
        <p class="mr-t-0">
            {{ $alumni['tahun_lulus'] != null ? $alumni['tahun_lulus'] : '-' }}</p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Nomor Ijazah</h6>
        <p class="mr-t-0">
            {{ $alumni['no_ijazah'] != null ? $alumni['no_ijazah'] : '-' }}</p>
    </div>
    <div class="col-md-6">
        <h6 class="text-muted text-uppercase">Alamat</h6>
        <p class="mr-t-0">{{ $alumni['alamat'] }}</p>
    </div>
</div>
{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
    rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
</script>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row">
            <div class="col-lg-12">
                <!--x-editable starts-->
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">X-editable Editor</h4>
                        <p class="card-description">Edit forms inline(click on the underline text to test below)</p>
                        <div class="template-demo">
                            <form id="editable-form" class="editable-form">
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Nama</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="username" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['nama']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label
                                        class="col-6 col-lg-4 col-form-label">Jurusan</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="jurusan" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['jurusan']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Nomor
                                        Induk</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Telepon</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Jenis Kelamin</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Agama</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Tempat Lahir</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Tanggal Lahir</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="tgl_lahir" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['tgl_lahir']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Tahun Lulus</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Nomor Ijazah</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="nomor_induk" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['no_induk']) }}</a>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-6 col-lg-4 col-form-label">Alamat</label>
                                    <div class="col-6 col-lg-8 d-flex align-items-center">
                                        <a href="#" id="address" data-type="text" data-pk="1"
                                            class="editable editable-click" data-abc="true"
                                            style="">{{ ucfirst($alumni['alamat']) }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function($) {
        'use strict';
        $(function() {
            if ($('#editable-form').length) {
                $.fn.editable.defaults.mode = 'inline';
                $.fn.editableform.buttons =
                    '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
                    '<i class="fa fa-fw fa-check"></i>' +
                    '</button>' +
                    '<button type="button" class="btn btn-warning btn-sm editable-cancel">' +
                    '<i class="fa fa-fw fa-times"></i>' +
                    '</button>';
                $('#username').editable({
                    type: 'text',
                    pk: 1,
                    name: 'username',
                    title: 'Enter username'
                });

                $('#firstname').editable({
                    validate: function(value) {
                        if ($.trim(value) === '') return 'This field is required';
                    }
                });

                $('#nomor_induk').editable({
                    validate: function(value) {
                        if ($.trim(value) === '') return 'This field is required';
                    }
                });

                $('#sex').editable({
                    source: [{
                            value: 1,
                            text: 'Male'
                        },
                        {
                            value: 2,
                            text: 'Female'
                        }
                    ]
                });

                $('#status').editable();

                $('#group').editable({
                    showbuttons: false
                });

                $('#vacation').editable({
                    datepicker: {
                        todayBtn: 'linked'
                    }
                });

                $('#dob').editable();

                $('#event').editable({
                    placement: 'right',
                    combodate: {
                        firstItem: 'name'
                    }
                });

                $('#tgl_lahir').editable({
                    format: 'yyyy-mm-dd hh:ii',
                    viewformat: 'dd/mm/yyyy hh:ii',
                    validate: function(v) {
                        if (v && v.getDate() === 10) return 'Day cant be 10!';
                    },
                    datetimepicker: {
                        todayBtn: 'linked',
                        weekStart: 1
                    }
                });

                $('#comments').editable({
                    showbuttons: 'bottom'
                });

                $('#note').editable();
                $('#pencil').on("click", function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $('#note').editable('toggle');
                });

                $('#state').editable({
                    source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado",
                        "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho",
                        "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana",
                        "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
                        "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
                        "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota",
                        "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania",
                        "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
                        "Utah", "Vermont", "Virginia", "Washington", "West Virginia",
                        "Wisconsin", "Wyoming"
                    ]
                });

                $('#state2').editable({
                    value: 'California',
                    typeahead: {
                        name: 'state',
                        local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California",
                            "Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
                            "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
                            "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts",
                            "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana",
                            "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico",
                            "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma",
                            "Oregon", "Pennsylvania", "Rhode Island", "South Carolina",
                            "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
                            "Washington", "West Virginia", "Wisconsin", "Wyoming"
                        ]
                    }
                });

                $('#fruits').editable({
                    pk: 1,
                    limit: 3,
                    source: [{
                            value: 1,
                            text: 'banana'
                        },
                        {
                            value: 2,
                            text: 'peach'
                        },
                        {
                            value: 3,
                            text: 'apple'
                        },
                        {
                            value: 4,
                            text: 'watermelon'
                        },
                        {
                            value: 5,
                            text: 'orange'
                        }
                    ]
                });

                $('#tags').editable({
                    inputclass: 'input-large',
                    select2: {
                        tags: ['html', 'javascript', 'css', 'ajax'],
                        tokenSeparators: [",", " "]
                    }
                });

                $('#address').editable({
                    url: '/post',
                    value: {
                        city: "Moscow",
                        street: "Lenina",
                        building: "12"
                    },
                    validate: function(value) {
                        if (value.city === '') return 'city is required!';
                    },
                    display: function(value) {
                        if (!value) {
                            $(this).empty();
                            return;
                        }
                        var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $(
                                '<div>').text(value.street).html() + ' st., bld. ' + $('<div>')
                            .text(value.building).html();
                        $(this).html(html);
                    }
                });

                $('#user .editable').on('hidden', function(e, reason) {
                    if (reason === 'save' || reason === 'nochange') {
                        var $next = $(this).closest('tr').next().find('.editable');
                        if ($('#autoopen').is(':checked')) {
                            setTimeout(function() {
                                $next.editable('show');
                            }, 300);
                        } else {
                            $next.focus();
                        }
                    }
                });
            }
        });
    })(jQuery);
</script> --}}
