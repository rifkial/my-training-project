<table style="width: 100%">
    @if (empty($alumni['organisasi']))
        @if (session('id') == $alumni['id'])
            <tr>
                <td colspan="3">
                    <form id="formOrganisasi" name="formOrganisasi" action="javascript:void(0)" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <input type="hidden" name="id_alumni" value="{{ $alumni['id'] }}">
                            <input type="hidden" name="byDetail" value="true">
                            <label class="col-md-3 col-form-label" for="l0">Posisi</label>
                            <div class="col-md-9">
                                <input class="form-control" name="posisi" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Organisasi</label>
                            <div class="col-md-9">
                                <input type="text" name="nama_organisasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Masa Organisasi</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" name="checkOrg" id="checkOrg" onclick="displayOrg()">
                                        <label for="" style="font-weight: normal">Masih Aktif</label>
                                    </div>
                                    <div class="col-md-12" id="aktifOrg" style="display: none">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai_on" id="tanggal_mulai_on"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="row" id="notOrg" style="display: none">
                                    <div class="col-md-6">
                                        <small>*tanggal mulai</small>
                                        <input type="date" name="tanggal_mulai" id="tanggal_mulai"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <small>*tanggal selesai</small>
                                        <input type="date" name="tanggal_selesai" id="tanggal_selesai"
                                            class="form-control" value="tanggal selesai">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l15"></label>
                            <div class="col-md-9">
                                <div class="button pull-right">
                                    <input type="hidden" name="action" id="action" value="Add" />
                                    <button type="submit" class="btn btn-outline-info" id="saveBtn"
                                        value="create">Simpan</button>
                                    <button class="btn btn-outline-danger" id="btnCancel">Batalkan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        @else
            <tr>
                <td style="text-align: center">Data Organisasi masih kosong</td>
            </tr>
        @endif
    @else
        @if (session('id') == $alumni['id'])
            <tr>
                <td style="width: 20%">
                    <a href="javascript:void(0)" id="addOrganisasi" class="btn btn-outline-info mb-3">
                        <i class="fa fa-plus-circle"></i> Tambah Organisasi
                    </a>
                </td>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endif
        <tbody id="outputPengalaman" class="mt-4">
            @foreach ($alumni['organisasi'] as $org)
                <tr class="mt-2" style="box-shadow: 0 4px 3px 1px rgb(0 0 0 / 20%)">
                    <td style="vertical-align: middle; text-align: center" class="pr-2 pl-2">
                        {{ (new \App\Helpers\Help())->getMonthYear($org['tgl_mulai']) }} <p class="mb-0">-
                        </p>
                        {{ $org['aktif'] == 1 ? 'saat ini' : (new \App\Helpers\Help())->getMonthYear($org['tgl_selesai']) }}
                    </td>
                    <td style="vertical-align: middle">
                        <h5 class="mt-0">
                            <b>{{ strtoupper($org['posisi']) }}</b>
                        </h5>
                        <h5 class="mt-0">{{ strtoupper($org['organisasi']) }}
                        </h5>
                        <table style="width: 100%">
                            <tr>
                                <td>Keterangan : {{ ucwords($org['keterangan']) }}</td>
                            </tr>
                        </table>
                    <td>
                        @if (session('id') == $alumni['id'])
                    <td style="text-align: right; vertical-align: middle" class="pr-2 pl-2">
                        <ul class="social-links list-inline mb-0 mt-2">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-info btn-sm editOrg" data-original-title="Edit Organisasi"
                                    data-id="{{ $org['id'] }}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                    class="btn btn-danger btn-sm deleteOrg" data-original-title="Hapus Organisasi"
                                    data-id="{{ $org['id'] }}">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </li>
                        </ul>
                    </td>
            @endif
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
    @endforeach
    </tbody>
    @endif
</table>

<script>
    displayOrg();

    function displayOrg() {
        if ($("#checkOrg").is(':checked')) {
            $("#aktifOrg").show(); // checked
            $('#notOrg').hide();
        } else {
            $("#aktifOrg").hide(); // unchecked
            $('#notOrg').show();
        }
    }
    $(document).on('click', '.editOrg', function() {
        let id = $(this).data('id');
        let loader = $(this);
        $('#form_result').html('');
        $.ajax({
            type: 'POST',
            url: "{{ route('alumni-organisasi_edit') }}",
            data: {
                id
            },
            beforeSend: function() {
                $(loader).html(
                    '<i class="fa fa-spin fa-spinner"></i>');
            },
            success: function(data) {
                $(loader).html('<i class="fas fa-pencil-alt"></i>');
                $('#titleOrganisasi').html("Edit Organisasi");
                $('#id_organisasi').val(data.id);
                $('#posisi').val(data.posisi);
                $('#nama_organisasi').val(data.organisasi);
                $('#keterangan').val(data.keterangan);
                if (data.aktif == 1) {
                    $("#checkOrg").prop("checked", true);
                    $('#tanggal_mulai_on').val(data.tgl_mulai);
                } else {
                    $("#checkOrg").prop("checked", false);
                    $('#tanggal_mulai').val(data.tgl_mulai);
                    $('#tanggal_selesai').val(data.tgl_selesai);
                }
                displayOrg();
                $('#action').val('Edit');
                $('#modalOrg').modal('show');
            }
        });
    });

    $('#formModalOrganisasi, #formOrganisasi').on('submit', function(event) {
        // alert("proses upload");
        event.preventDefault();
        $("#saveBtn").html(
            '<i class="fa fa-spin fa-spinner"></i> Loading');
        var action_url = '';

        if ($('#action').val() == 'Add') {
            action_url = "{{ route('alumni-organisasi_create') }}";
            method_url = "POST";
        }

        if ($('#action').val() == 'Edit') {
            action_url = "{{ route('alumni-organisasi_update') }}";
            method_url = "PUT";
        }
        $.ajax({
            url: action_url,
            method: method_url,
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function() {
                $('.organisasi').html('<div id="loading" style="" ></div>');
            },
            success: function(data) {
                if (data.status == 'berhasil') {
                    $('#formOrganisasi').trigger("reset");
                    $('#modalOrg').modal('hide');
                }
                $(".organisasi").load(window.location.href +
                    " .organisasi");
                // $(".modal_pendidikan").load(window.location.href +
                //     " .modal_pendidikan");
                noti(data.icon, data.success);
                $('#saveBtn').html('Simpan');
                $("#saveBtn").attr("disabled", false);
            },
            error: function(data) {
                console.log('Error:', data);
                $('#saveBtn').html('Simpan');
            }
        });
    });
    $('#addOrganisasi').click(function() {
        $('#titleOrganisasi').html("Tambah Organisasi");
        $('#modalOrg').modal('show');
        $('#tambahPendidikan').remove();
        $('#action').val('Add');
    });
</script>
