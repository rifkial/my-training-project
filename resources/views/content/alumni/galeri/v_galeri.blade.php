@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .btn:focus,
        .btn:active,
        button:focus,
        button:active {
            outline: none !important;
            box-shadow: none !important;
        }

        #image-gallery .modal-footer {
            display: block;
        }

        .thumb {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        a.delete:hover {
            color: red;
        }

        a.delete {
            position: absolute;
            bottom: 9px;
            right: 9px;
            color: #fff
        }


        .upload-options {
            position: relative;
            height: 75px;
            background-color: cadetblue;
            cursor: pointer;
            overflow: hidden;
            text-align: center;
            -webkit-transition: background-color ease-in-out 150ms;
            transition: background-color ease-in-out 150ms;
        }

        .upload-options:hover {
            background-color: #7fb1b3;
        }

        .upload-options input {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .upload-options label {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            width: 100%;
            height: 100%;
            font-weight: 400;
            text-overflow: ellipsis;
            white-space: nowrap;
            cursor: pointer;
            overflow: hidden;
        }

        .upload-options label::after {
            content: 'add';
            font-family: 'Material Icons';
            position: absolute;
            font-size: 2.5rem;
            color: #e6e6e6;
            top: calc(50% - 2.5rem);
            left: calc(50% - 1.25rem);
            z-index: 0;
        }

        .upload-options label span {
            display: inline-block;
            width: 50%;
            height: 100%;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            vertical-align: middle;
            text-align: center;
        }

        .upload-options label span:hover i.material-icons {
            color: lightgray;
        }

        .js--image-preview {
            height: 169px;
            width: 100%;
            position: relative;
            overflow: hidden;
            background-image: url("");
            background-color: white;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .js--image-preview::after {
            content: "photo_size_select_actual";
            font-family: 'Material Icons';
            position: relative;
            font-size: 4.5em;
            color: #e6e6e6;
            top: calc(50% - 3rem);
            left: calc(50% - 2.25rem);
            z-index: 0;
        }

        .js--image-preview.js--no-default::after {
            display: none;
        }

        i.material-icons {
            -webkit-transition: color 100ms ease-in-out;
            transition: color 100ms ease-in-out;
            font-size: 2.25em;
            line-height: 55px;
            color: white;
            display: block;
        }

        .drop {
            display: block;
            position: absolute;
            background: rgba(95, 158, 160, 0.2);
            border-radius: 100%;
            -webkit-transform: scale(0);
            transform: scale(0);
        }

        .animate {
            -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
        }

    </style>
    <div class="widget-bg">
        <div class="row" id="result_galeri">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <div class="js--image-preview"></div>
                <div class="upload-options">
                    <label>
                        <input class="image-upload" />
                    </label>
                </div>
            </div>
            @foreach ($galeri as $gl)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <div class="galeri" style="position: relative">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal"
                            data-title="{{ $gl['nama'] . ' - ' . $gl['kategori'] }}"
                            data-keterangan="{{ $gl['keterangan'] }}" data-image="{{ $gl['file'] }}"
                            data-target="#image-gallery">
                            <img class="img-thumbnail" src="{{ $gl['file'] }}" alt="Another alt text" style="width: 100%">
                        </a>

                        <div class="row mr-0 ml-0" style="position: absolute; bottom: 0; left: 0; background: #13121259;">
                            <div class="col-md-3" style="display: flex; align-items: center; justify-content: center">
                                <img src="{{ $gl['avatar'] }}" alt="">
                            </div>
                            <div class="col-md-9 mr-0">
                                <h5 class="m-0" style="color: #fff;"><b>{{ $gl['pembuat'] }}</b></h5>
                                <h6 class="m-0" style="color: #fff">{{ $gl['role'] }}</h6>
                                <small class="m-0" style="color: #fff">{{ $gl['dibuat'] }}</small>
                            </div>
                        </div>
                        @if ($gl['sosial'] == session('sosial'))
                            <a href="javascript:void(0)" class="delete" data-id="{{ $gl['id'] }}">
                                <i class="fas fa-trash-alt fa-2x"></i>
                            </a>
                        @endif
                    </div>
                </div>
            @endforeach

        </div>
    </div>


    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    <div class="p-3 m-3" style="border: 1px solid #e9ecef; border-radius: 6px; margin-top: 17px;">
                        <label for="">Keterangan :</label>
                        <p id="image-gallery-keterangan">ini adalah keterangan semua deskripis</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i
                            class="fa fa-arrow-left"></i>
                    </button>

                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i
                            class="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_kategori">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Kategori</label>
                                    <div class="col-sm-12">
                                        <select name="id_kategori" id="id_kategori" class="form-control">
                                            <option value="">-- Pilih Kategori Galeri --</option>
                                            @foreach ($kategori as $kt)
                                                <option value="{{ $kt['id'] }}">{{ $kt['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" id="keterangan" class="form-control"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">File</label>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="100%" style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-6" style="position: relative">
                                                <div id="delete_foto" style="position: absolute; bottom: 0"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.image-upload').click(function() {
            $('#Customer_id').val('');
            $('#CustomerForm').trigger("reset");
            $('.tambahBaris').show('');
            $('#modelHeading').html("Tambah {{ session('title') }}");
            $('#ajaxModel').modal('show');
            $('#action').val('Add');
        });

        $('body').on('submit', '#CustomerForm', function(e) {
            e.preventDefault();
            var actionType = $('#btn-save').val();
            $("#saveBtn").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#saveBtn").attr("disabled", true);
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('alumni-galeri_create') }}",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#result_galeri').html(
                        '<div id="loading" style="" ></div>');
                },
                success: (data) => {
                    if (data.status == 'berhasil') {
                        location.reload();
                        $('#ajaxModel').modal('hide');
                        swa(data.status + "!",
                            "Gambar berhasil diupoad, mohon nunggu persetujuan admin untuk di publikasikkan",
                            data.icon);
                    } else {
                        $("#result_galeri").load(window.location.href +
                            " #result_galeri");
                        swa(data.status + "!", data.message, data.icon);
                    }
                    $("#saveBtn").html('Simpan');
                    $("#saveBtn").attr("disabled", false);



                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        $(document).on('click', '.delete', function() {
            let id = $(this).data('id');
            let loader = $(this);
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('alumni-galeri_delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                        $('#result_galeri').html(
                            '<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $("#result_galeri").load(window.location.href +
                                " #result_galeri");
                        }
                        swa(data.status + "!", data.message, data.success);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        });
        let modalId = $('#image-gallery');

        $(document)
            .ready(function() {

                loadGallery(true, 'a.thumbnail');

                //This function disables buttons when needed
                function disableButtons(counter_max, counter_current) {
                    $('#show-previous-image, #show-next-image')
                        .show();
                    if (counter_max === counter_current) {
                        $('#show-next-image')
                            .hide();
                    } else if (counter_current === 1) {
                        $('#show-previous-image')
                            .hide();
                    }
                }

                function loadGallery(setIDs, setClickAttr) {
                    let current_image,
                        selector,
                        counter = 0;

                    $('#show-next-image, #show-previous-image')
                        .click(function() {
                            if ($(this)
                                .attr('id') === 'show-previous-image') {
                                current_image--;
                            } else {
                                current_image++;
                            }

                            selector = $('[data-image-id="' + current_image + '"]');
                            updateGallery(selector);
                        });

                    function updateGallery(selector) {
                        let $sel = selector;
                        current_image = $sel.data('image-id');
                        $('#image-gallery-title')
                            .text($sel.data('title'));
                        $('#image-gallery-keterangan')
                            .text($sel.data('keterangan'));
                        $('#image-gallery-image')
                            .attr('src', $sel.data('image'));
                        disableButtons(counter, $sel.data('image-id'));
                    }

                    if (setIDs == true) {
                        $('[data-image-id]')
                            .each(function() {
                                counter++;
                                $(this)
                                    .attr('data-image-id', counter);
                            });
                    }
                    $(setClickAttr)
                        .on('click', function() {
                            updateGallery($(this));
                        });
                }
            });

        // build key actions
        $(document)
            .keydown(function(e) {
                switch (e.which) {
                    case 37: // left
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                            $('#show-previous-image')
                                .click();
                        }
                        break;

                    case 39: // right
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                            $('#show-next-image')
                                .click();
                        }
                        break;

                    default:
                        return; // exit this handler for other keys
                }
                e.preventDefault(); // prevent the default action (scroll / move caret)
            });

        function tes() {
            alert("hallo tes");
        }
    </script>
@endsection
