@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left">

        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('alumni-kategori_blog') }}">Kategori</a>
                </li>
                <li class="breadcrumb-item active">Blog</li>
            </ol>
            @if (session('role') == 'admin-alumni')
                <div class="d-none d-sm-inline-flex justify-center align-items-center"><a
                        href="{{ route('alumni-blog_approve') }}"
                        class="btn btn-outline-info mr-l-20 btn-sm btn-rounded hidden-xs hidden-sm ripple"
                        target="_blank">Belum Diapprove <span
                            class="badge badge-pill bg-primary">{{ $approve }}</span></a>
                </div>
            @endif
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        {{-- <p class="text-muted">Data Jurusan tersedia di dalam fakultas.</p> --}}
                        <hr>
                        <div class="row my-2">
                            <div class="col-md-2">
                                @if (session('role') == 'admin-alumni')
                                    <a class="btn btn-outline-info addBlog" href="{{ route('alumni-blog_new_create') }}"
                                        onclick="return loader()"><i class="fas fa-plus"></i>
                                        Tambah</a>
                                @endif
                            </div>
                            <div class="col-md-7"></div>
                            <div class="col-md-3">
                                <form class="navbar-form" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                <thead>
                                    <tr>
                                        @if (session('role') == 'admin-alumni')
                                            <th class="vertical-middle"></th>
                                        @endif
                                        <th class="vertical-middle">Article</th>
                                        <th class="vertical-middle">Kategori</th>
                                        <th class="vertical-middle">Published</th>
                                        @if (session('role') == 'admin-alumni')
                                            <th></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($blog))
                                        @foreach ($blog as $bl)
                                            <tr>
                                                @if (session('role') == 'admin-alumni')
                                                    <td class="vertical-middle text-center">
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                {{ $bl['status'] == 1 ? 'checked' : '' }}
                                                                class="fakultas_check" data-id="{{ $bl['id'] }}">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                                @endif
                                                <td class="w-50"><a
                                                        href="{{ route('alumni-blog_detail', ['id' => (new \App\Helpers\Help())->encode($bl['id']), 'title' => Str::slug($bl['judul'], '-')]) }}">
                                                        <b class="text-dark">{!! Str::limit($bl['judul'], 50, ' ...') !!}</b>
                                                        <span class="text-dark">by
                                                            {{ $bl['pembuat'] }}</span>
                                                        <p class="m-0"><small
                                                                class="text-dark">{{ $bl['dilihat'] == null ? 0 : $bl['dilihat'] }}x
                                                                Dilihat</small></p>
                                                        <p class="m-0 text-info">
                                                            {{ Request::getSchemeAndHttpHost() . '/program/alumni/blog/detail/' . (new \App\Helpers\Help())->encode($bl['id']) . '/' . Str::slug($bl['judul'], '-') }}
                                                        </p>
                                                    </a>
                                                </td>
                                                <td class="vertical-middle">{{ $bl['kategori'] }}</td>

                                                <td class="vertical-middle">
                                                    {{ (new \App\Helpers\Help())->getTanggalLengkap($bl['created_at']) }}
                                                </td>
                                                @if (session('role') == 'admin-alumni')
                                                    <td class="vertical-middle">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-info edit"
                                                            data-id="{{ $bl['id'] }}"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                            data-id="{{ $bl['id'] }}"><i
                                                                class="fas fa-trash"></i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">Data Saat ini tidak tersedia
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {!! $pagination !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalBlog" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleBlog"></h5>
                </div>
                <form id="formBlog" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_blog">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Kategori</label>
                                    <div class="col-sm-12">
                                        <select name="id_kategori" id="id_kategori" class="form-control">
                                            <option value="">Pilih Kategori..</option>
                                            @foreach ($kategori as $kt)
                                                <option value="{{ (new \App\Helpers\Help())->encode($kt['id']) }}">
                                                    {{ $kt['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Judul</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="judul" id="judul">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Isi</label>
                                    <div class="col-sm-12">
                                        <textarea name="isi" id="isi" class="form-control editor"></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">File input</label>
                                    <br>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                        <br><small class="text-muted">Digunakan untuk thumbnail Blog</small>
                                    </div>
                                    <div class="col-md-3">
                                        <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                            class="form-group my-1 w-100">
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit', '#formBlog', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('alumni-blog_update') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            location.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.change_status', function() {
                let id = $(this).data('id');
                let status = $(this).data('param');
                let button = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-blog_change_status') }}",
                    data: {
                        id,
                        status
                    },
                    success: function(data) {
                        $(button).find("i").toggleClass("fa-eye fa-eye-slash");
                        console.log(data.message);
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-data_blog_soft_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-trash"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-blog_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleBlog').html("Edit Blog");
                        $('#id_blog').val(data.id);
                        $('#id_kategori').val(data.id_kategori).trigger('change');
                        $('#judul').val(data.judul);
                        tinymce.activeEditor.setContent(data.isi);
                        if (data.file) {
                            $('#modal-preview').attr('src', data.file);
                            if (data.file_edit != 'default.png') {
                                $('#delete_foto').html(
                                    '<input type="checkbox" name="remove_photo" value="' +
                                    data
                                    .file + '"/> Remove photo');
                            }
                        }
                        $('#modalBlog').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "350",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });
        })

        function filter(routes) {
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }

        function loader() {
            $(".addBlog").attr("disabled", true);
            $(".addBlog").html(
                '<i class="fa fa-spin fa-spinner"></i> Memproses..');
        }
    </script>
@endsection
