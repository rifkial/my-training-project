@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .ui-w-100 {
            width: 100px !important;
            height: auto;
        }

        .card {
            background-clip: padding-box;
            box-shadow: 0 1px 4px rgba(24, 28, 33, 0.012);
        }

        .user-view-table td:first-child {
            width: 9rem;
        }

        .user-view-table td {
            padding-right: 0;
            padding-left: 0;
            border: 0;
        }

        .text-light {
            color: #babbbc !important;
        }

        .card .row-bordered>[class*=" col-"]::after {
            border-color: rgba(24, 28, 33, 0.075);
        }

        .text-xlarge {
            font-size: 170% !important;
        }

    </style>
    <div class="widget-bg">
        <div class="row">
            <div class="col-md-6 col-12">
                <a class="btn btn-info" href="{{ route('alumni-alumni') }}">
                    <i class="fas fa-chevron-circle-left"></i> Kembali
                </a>
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <div style="width: 100%;">
                    <div class="table-responsive">
                        <table class="table table-striped" id="data-tabel">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Nomor Induk</th>
                                    <th>Jurusan</th>
                                    <th>Tahun Lulus</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container bootdey flex-grow-1 container-p-y">

                                    <div class="media align-items-center py-3 mb-3">
                                        <img id="preview" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt=""
                                            class="d-block ui-w-100 rounded-circle">
                                        <div class="media-body ml-4">
                                            <h4 class="font-weight-bold mb-0" id="nama">John Doe
                                                <span class="text-muted font-weight-normal">@johndoe</span>
                                            </h4>
                                            <div class="text-muted mb-2" id="no_induk">ID: 3425433</div>
                                            <div id="action">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-4">
                                        <div class="card-body">
                                            <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Registered:</td>
                                                        <td id="register">01/23/2017</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Latest Login:</td>
                                                        <td id="last_login">01/23/2018 (14 days ago)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status:</td>
                                                        <td>Pending</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <hr class="border-light m-0">
                                        <div class="card-body">

                                            <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Name:</td>
                                                        <td id="name">Nelle Maxwell</td>
                                                    </tr>
                                                    <tr>
                                                        <td>E-mail:</td>
                                                        <td id="email">nmaxwell@mail.com</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Kelamin:</td>
                                                        <td id="jenkel">Company Ltd.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tahun Lulus:</td>
                                                        <td id="tahun_lulus">Company Ltd.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jurusan:</td>
                                                        <td id="jurusan">Company Ltd.</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor Ijazah:</td>
                                                        <td id="no_ijazah">Company Ltd.</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            {{-- <h6 class="mt-4 mb-3">Social links</h6> --}}

                                            {{-- <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Twitter:</td>
                                                        <td><a href="javascript:void(0)">https://twitter.com/user</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Facebook:</td>
                                                        <td><a href="javascript:void(0)">https://www.facebook.com/user</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Instagram:</td>
                                                        <td><a href="javascript:void(0)">https://www.instagram.com/user</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> --}}

                                            <h6 class="mt-4 mb-3">Personal info</h6>

                                            <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Tempat, Tanggal lahir:</td>
                                                        <td id="ttl">May 3, 1995</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat:</td>
                                                        <td id="alamat">Canada</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <h6 class="mt-4 mb-3">Contacts</h6>

                                            <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Phone:</td>
                                                        <td id="telepon">+0 (123) 456 7891</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <h6 class="mt-4 mb-3">Selengkapnya</h6>

                                            <table class="table user-view-table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Bio:</td>
                                                        <td id="bio">
                                                            Rock,
                                                            Alternative,
                                                            Electro,
                                                            Drum &amp; Bass,
                                                            Dance
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'no_induk',
                        name: 'no_induk'
                    },
                    {
                        data: 'jurusan',
                        name: 'jurusan'
                    },
                    {
                        data: 'tahun_lulus',
                        name: 'tahun_lulus'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('click', '.info', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-detail_profile') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fa fa-info-circle"></i>');
                        $('#modelHeading').html("Detail Profil");
                        $('#nama').html(data.nama);
                        $('#name').html(data.nama);
                        $('#email').html(data.email);
                        $('#jenkel').html(data.jenkel);
                        $('#no_induk').html("Nomor Induk  " + data.no_induk);
                        $('#register').html(data.register);
                        $('#last_login').html(data.last_login);
                        $('#jurusan').html(data.jurusan);
                        $('#tahun_lulus').html(data.tahun_lulus);
                        $('#no_ijazah').html(data.no_ijazah);
                        $('#telepon').html(data.telepon);
                        $('#bio').html(data.bio);
                        $('#alamat').html(data.alamat);
                        $('#preview').attr('src', data.file);
                        $('#ttl').html(data.tempat_lahir + ", " + data.tgl_lahir);
                        $('#action').html(
                            ' <a href="javascript:void(0)" class="btn btn-success verifikasi btn-sm" data-id="' +
                            data.id +
                            '">Terima</a>&nbsp;<a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteData(' +
                            data.id + ')">Tolak/Hapus</a>')
                        $('#ajaxModel').modal('show');
                    }
                });
            });
            $(document).on('click', '.verifikasi', function() {
                let id = $(this).data('id');
                let loader = $(this);
                var confirmVerifikasi = confirm("Apa kamu yakin ingin menerima alumni?");
                if (confirmVerifikasi == true) {
                    $.ajax({
                        url: "{{ route('update_status-user_alumni') }}",
                        type: 'post',
                        data: {
                            id,
                            value: 1
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data-tabel').dataTable().fnDraw(false);
                                $('#ajaxModel').modal('hide');
                            }
                            swa(data.status + "!", data.message, data.success);

                        }
                    });
                }
            });
        })

        function deleteData(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('alumni-soft_delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(".delete-" + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#data-tabel').dataTable().fnDraw(false);
                            $('#ajaxModel').modal('hide');
                        }
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }
    </script>
@endsection
