@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }

    </style>
    <div class="widget-bg">
        @if (session('role') == 'alumni-alumni' || session('role') == 'admin')
            <div class="row">
                <div class="col-md-12">
                    <h2 class="box-title">{{ Session::get('title') }}</h2>
                </div>
            </div>
            <hr>
            <div class="row mb-3">
                <div class="col-md-12">

                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-6 col-12">
                <div class="pull-right">
                    <button class="btn btn-outline-info mt-1" id="createNewCustomer"><i class="fas fa-plus-circle"></i>
                        Tambah Data</button>
                    <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                        Import</button>
                    <button id="data_trash" class="btn btn-outline-danger mt-1"><i class="fas fa-trash-restore"></i>
                        Trash</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">

            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-4 mb-1">
                            <div class="m-input-icon m-input-icon--left">
                                @php
                                    $serc = str_replace('-', ' ', $search);
                                @endphp
                                <label class="filter-col" style="margin-right:0;" for="pref-search">Search:</label>
                                <input value="{{ $serc }}" id="search" name="search" type="text"
                                    class="form-control m-input" placeholder="Search..." id="generalSearch">
                            </div>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Filter:</label>
                            <select id="status" name="status" class="form-control">
                                @foreach ($statuses as $st)
                                    @if ($st['value'] == $status)
                                        <option selected value="{{ $st['value'] }}">{{ $st['label'] }}</option>
                                    @else
                                        <option value="{{ $st['value'] }}">{{ $st['label'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Order
                                by:</label>
                            <select id="sort" name="sort" class="form-control">
                                @foreach ($sorts as $s)
                                    @if ($s == $sort)
                                        <option selected value="{{ $s }}">{{ $s }}</option>
                                    @else
                                        <option value="{{ $s }}">{{ $s }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-perpage">Per
                                page:</label>
                            <select id="per_page" name="per_page" class="form-control">
                                @foreach ($pages as $pg)
                                    @if ($pg == $perPage)
                                        <option selected value="{{ $pg }}">{{ $pg }}</option>
                                    @else
                                        <option value="{{ $pg }}">{{ $pg }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">&nbsp;</label>
                            <a href="javascript:void(0)" id="fil" onclick="test('{{ $routes }}')"
                                class="btn btn-outline-info btn-block"><i class="fab fa-searchengin"></i> Display</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                {{-- <div id="loading" style=""></div> --}}
                <p></p>
                <table class="table table-striped- table-bordered table-hover" id="table-data">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Status</th>
                            @if (session('role') == 'admin-alumni')
                                <th>Aksi</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($data))
                            <tr>
                                @if (session('role') == 'admin-alumni')
                                    <td colspan="4" style="text-align: center">Data saat ini kosong</td>
                                @else
                                    <td colspan="3" style="text-align: center">Data saat ini kosong</td>
                                @endif
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item['nama'] }}</td>
                                    <td>
                                        <div class="input-group">
                                            <div id="radioBtn" class="btn-group">
                                                <a class="btn btn-info btn-sm {{ $item['status'] == 1 ? 'active' : 'notActive' }}"
                                                    onclick="changeStatus(1, {{ $item['id'] }})"
                                                    data-toggle="pilihan_status{{ $item['id'] }}"
                                                    data-title="Aktif">Aktif</a>
                                                <a class="btn btn-info btn-sm {{ $item['status'] != 1 ? 'active' : 'notActive' }}"
                                                    onclick="changeStatus(0, {{ $item['id'] }})"
                                                    data-toggle="pilihan_status{{ $item['id'] }}"
                                                    data-title="Tidak Aktif">Tidak Aktif</a>
                                            </div>
                                        </div>
                                    </td>
                                    @if (session('role') == 'admin-alumni')
                                        <td>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit"
                                                data-id="{{ $item['id'] }}" class="edit btn btn-info btn-sm edit"><i
                                                    class="fas fa-user-edit"></i></a>
                                            <button type="button" name="delete" data-id="{{ $item['id'] }}"
                                                class="btn btn-danger btn-sm delete"><i
                                                    class="fas fa-backspace"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            {!! $pagination !!}
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_jenis_pekerjaan">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Jenis Pekerjaan</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-siswa') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table_trash = $('#data-trash').DataTable({
                dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                buttons: [{
                    text: 'Refresh Tabel',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload(null, false);
                    }
                }, ],
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    "url": "{{ route('data_trash-siswa') }}",
                    "method": "GET"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'first_password',
                        name: 'first_password'
                    },
                    {
                        data: 'nisn',
                        name: 'nisn'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'tahun_angkatan',
                        name: 'tahun_angkatan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#data_trash').click(function() {
                table_trash.ajax.reload().draw();
                $('#modalTrash').modal('show');
            });

            $('#createNewCustomer').click(function() {
                $('#Customer_id').val('');
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah " + "{{ session('title') }}");
                $('.tambahBaris').show('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html("Import " + "{{ session('title') }}");
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-jenis_pekerjaan_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-jenis_pekerjaan_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload();
                        } else {
                            $('#saveBtn').html('Simpan');
                            $("#saveBtn").attr("disabled", false);
                        }
                        noti(data.icon, data.success);

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-jenis_pekerjaan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-user-edit"></i>');
                        $('#modelHeading').html("Edit " + "{{ session('title') }}");
                        $('#id_jenis_pekerjaan').val(data.id);
                        $('#nama').val(data.nama);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-jenis_pekerjaan_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                location.reload();
                            } else {
                                $(loader).html('<i class="fas fa-backspace"></i>');
                            }
                            swa(data.status + "!", data.message, data.success);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function changeStatus(val, id) {
            if (id == 1) {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Tidak Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="Tidak Aktif"]')
                    .removeClass('active').addClass('notActive');
            }
            updateStatus(val, id);
        }

        function updateStatus(value, id) {

            $.ajax({
                type: "POST",
                url: "{{ route('update_status-jenis_pekerjaan_alumni') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.success, data.message);
                }
            });
        }
        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama Jenis Pekerjaan</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
