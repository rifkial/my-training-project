@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="fas fa-recycle"></i> Trash Fakultas</a>
                </li>
                <li class="breadcrumb-item active">Jurusan</li>
            </ol>
        </div>
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        <p class="text-muted">Data Jurusan tersedia di dalam fakultas.</p>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-outline-info" id="addFakultas"><i class="fas fa-plus"></i>
                                    Fakultas</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <p></p>
                            <table class="table table-striped table-bordered table-hover" id="table-data">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="data_fakultas">
                                    @if (empty($fakultas))
                                        <tr>
                                            <td colspan="4" style="text-align: center">Data saat ini kosong</td>
                                        </tr>
                                    @else
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($fakultas as $fk)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $fk['nama'] }}</td>
                                                <td>
                                                    <label class="switch">
                                                        <input type="checkbox" {{ $fk['status'] == 1 ? 'checked' : '' }}
                                                            class="fakultas_check" data-id="{{ $fk['id'] }}">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <button data-toggle="collapse" data-target="#jurusan{{ $fk['id'] }}"
                                                        class="btn btn-sm btn-purple accordion-toggle"><i
                                                            class="fas fa-wrench"></i>
                                                    </button>
                                                    <a href="javascript:void(0)" data-id="{{ $fk['id'] }}"
                                                        class="edit btn btn-info btn-sm edit"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <button type="button" name="delete" data-id="{{ $fk['id'] }}"
                                                        class="btn btn-danger btn-sm delete"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" class="hiddenRow">
                                                    <div class="accordian-body collapse" id="jurusan{{ $fk['id'] }}">
                                                        <button class="btn btn-purple btn-sm my-3 pull-right"
                                                            onclick="addJurusan({{ $fk['id'] }})"><i
                                                                class="fas fa-plus-circle"></i>
                                                            Tambah Jurusan</button>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="bg-purple text-white">
                                                                    <th class="text-center">No</th>
                                                                    <th class="text-center">Nama</th>
                                                                    <th class="text-center">Status</th>
                                                                    <th class="text-center">Opsi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="dataJurusan{{ $fk['id'] }}">
                                                                @if (!empty($fk['jurusan']))
                                                                    @php
                                                                        $nomer = 1;
                                                                    @endphp
                                                                    @foreach ($fk['jurusan'] as $jr)
                                                                        <tr>
                                                                            <td>{{ $nomer++ }}</td>
                                                                            <td>{{ $jr['nama'] }}</td>
                                                                            <td>
                                                                                <label class="switch">
                                                                                    <input type="checkbox"
                                                                                        {{ $jr['status'] == 1 ? 'checked' : '' }} class="jurusan_check" data-id="{{ $jr['id'] }}">
                                                                                    <span class="slider round"></span>
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $jr['id'] }}"
                                                                                    class="editJurusan btn btn-info btn-sm"
                                                                                    data-fakultas="{{ $fk['id'] }}"><i
                                                                                        class="fas fa-pencil-alt"></i></a>
                                                                                <a href="javascript:void(0)"
                                                                                    data-id="{{ $jr['id'] }}"
                                                                                    data-fakultas="{{ $fk['id'] }}"
                                                                                    class="btn btn-danger btn-sm deleteJurusan"><i
                                                                                        class="fas fa-trash-alt"></i></a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @else
                                                                    <tr>
                                                                        <td colspan="4" class="text-center">Data saat ini
                                                                            tidak tersedia</td>
                                                                    </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_fakultas">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama_fakultas" name="nama[]"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status_fakultas" class="form-control">
                                            <option value="0">Disable</option>
                                            <option value="1" selected>Enable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalJurusan" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titleJurusan"></h5>
                </div>
                <form id="formJurusan" class="form-horizontal">
                    @csrf
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_jurusan">
                        <input type="hidden" name="id_fakultas" id="id_fakultas_jurusan">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama_jurusan" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="fomAddBaris">
                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="name" class="col-sm-12 control-label">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status_jurusan" class="form-control">
                                            <option value="0">Disable</option>
                                            <option value="1" selected>Enable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="actionJurusan" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="btnJurusan">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.fakultas_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-fakultas_alumni') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $(document).on('click', '.jurusan_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('update_status-jurusan_alumni') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            $('#addFakultas').click(function() {
                $('#CustomerForm').trigger("reset");
                $('#modelHeading').html("Tambah Fakultas");
                $('.tambahBaris').show('');
                $('.fomAddBaris').html('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#CustomerForm').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-fakultas_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-fakultas_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#CustomerForm').trigger("reset");
                            $('#data_fakultas').html(data.fakultas);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('#formJurusan').on('submit', function(event) {
                event.preventDefault();
                $("#btnJurusan").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#btnJurusan").attr("disabled", true);
                var action_url = '';

                if ($('#actionJurusan').val() == 'Add') {
                    action_url = "{{ route('alumni-jurusan_create') }}";
                    method_url = "POST";
                }

                if ($('#actionJurusan').val() == 'Edit') {
                    action_url = "{{ route('alumni-jurusan_update') }}";
                    method_url = "PUT";
                }
                let id_fakultas = $('#id_fakultas_jurusan').val();
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#modalJurusan').modal('hide');
                            $('#formJurusan').trigger("reset");
                            $('#dataJurusan'+id_fakultas).html(data.jurusan);
                        }
                        $('#btnJurusan').html('Simpan');
                        $("#btnJurusan").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-fakultas_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit Fakultas");
                        $('#id_fakultas').val(data.id);
                        $('#nama_fakultas').val(data.nama);
                        $('#status_fakultas').val(data.status);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });

            $(document).on('click', '.editJurusan', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $('.fomAddBaris').html('');
                $('.tambahBaris').hide('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-jurusan_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#titleJurusan').html("Edit Jurusan");
                        $('#id_jurusan').val(data.id);
                        $('#id_fakultas_jurusan').val(data.id_fakultas);
                        $('#nama_jurusan').val(data.nama);
                        $('#status_jurusan').val(data.status);
                        $('#modalJurusan').modal('show');
                        $('#actionJurusan').val('Edit')
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-fakultas_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_fakultas').html(data.fakultas);
                                $('#data-trash').dataTable().fnDraw(false);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-backspace"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });

            $(document).on('click', '.deleteJurusan', function() {
                let id = $(this).data('id');
                let id_fakultas = $(this).data('fakultas');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-jurusan_delete') }}",
                        type: "POST",
                        data: {
                            id, id_fakultas
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#dataJurusan'+id_fakultas).html(data.jurusan);
                            } else {
                                $(loader).html(
                                    '<i class="fas fa-trash-alt"></i>');
                            }
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });


        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddBaris').append(
                '<div class="form-group mb-1 mt-3" id="row' + i +
                '"><label for="name" class="col-sm-12 control-label">Nama</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        function addJurusan(id) {
            $('#formJurusan').trigger("reset");
            $('#titleJurusan').html("Tambah Jurusan");
            $('.fomAddBaris').html('');
            $('#modalJurusan').modal('show');
            $('#id_fakultas_jurusan').val(id);
            $('#actionJurusan').val('Add');
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
