@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <script src="{{ asset('asset/js/examwizard.min.js') }}"></script>
    <style>
        .privew {
            margin-bottom: 20px;
        }

        .questionsBox {
            display: block;
            border: solid 1px #e3e3e3;
            padding: 10px 20px 0px;
            box-shadow: inset 0 0 30px rgba(000, 000, 000, 0.1), inset 0 0 4px rgba(255, 255, 255, 1);
            border-radius: 3px;
            margin: 0 10px;
        }

        .questions {
            background: #007fbe;
            color: #FFF;
            font-size: 22px;
            padding: 8px 30px;
            font-weight: 300;
            margin: 0 -30px 10px;
            position: relative;
        }

        .questions:after {
            background: url(../img/icon.png) no-repeat left 0;
            display: block;
            position: absolute;
            top: 100%;
            width: 9px;
            height: 7px;
            content: '.';
            left: 0;
            text-align: left;
            font-size: 0;
        }

        .questions:after {
            left: auto;
            right: 0;
            background-position: -10px 0;
        }

        .questions:before,
        .questions:after {
            background: black;
            display: block;
            position: absolute;
            top: 100%;
            width: 9px;
            height: 7px;
            content: '.';
            left: 0;
            text-align: left;
            font-size: 0;
        }

        .answerList {
            margin-bottom: 15px;
        }


        ol,
        ul {
            list-style: none;
        }

        .answerList li:first-child {
            border-top-width: 0;
        }

        .answerList li {
            padding: 3px 0;
        }

        .answerList label {
            display: block;
            padding: 6px;
            border-radius: 6px;
            border: solid 1px #dde7e8;
            font-weight: 400;
            font-size: 13px;
            cursor: pointer;
            font-family: Arial, sans-serif;
        }

        input[type=checkbox],
        input[type=radio] {
            margin: 4px 0 0;
            margin-top: 1px;
            line-height: normal;
        }

        .questionsRow {
            background: #dee3e6;
            margin: 0 -20px;
            padding: 10px 20px;
            border-radius: 0 0 3px 3px;
        }

        .button,
        .greyButton {
            background-color: #f2f2f2;
            color: #888888;
            display: inline-block;
            border: solid 3px #cccccc;
            vertical-align: middle;
            text-shadow: 0 1px 0 #ffffff;
            line-height: 27px;
            min-width: 160px;
            text-align: center;
            padding: 5px 20px;
            text-decoration: none;
            border-radius: 0px;
            text-transform: capitalize;
        }

        .questionsRow span {
            float: right;
            display: inline-block;
            line-height: 30px;
            border: solid 1px #aeb9c0;
            padding: 0 10px;
            background: #FFF;
            color: #007fbe;
        }




        .hidden {
            display: none !important;
        }

        /* Disabled class used for disable element is examwizerd, usally it's exsists by defualt in bootstrap' */
        .disabled {
            pointer-events: none;
            cursor: not-allowed;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
            opacity: .65;
        }


        /* Temp Style Just For demo */
        body {
            padding-bottom: 60px;
            font-family: 'Roboto';
            background-color: #fafafa;
        }

        form {
            background: #f7f7f7;
            padding: 20px;
        }

        textarea {
            max-width: 100%;
        }

    </style>

    <div class="widget-bg">
        <h1>Cookie-enabled Exam Wizard Plugin Example</h1>
        <div class="row">
            <div class="col-md-9">
                <form id="examwizard-question">
                    <div class="privew">
                        @if (!empty($data))
                            @php
                                $first = $data[0];
                            @endphp
                            <div class="questionsBox" data-question="1">
                                <div class="questions">{{ $first['pertanyaan'] }}</div>
                                @if ($first['jenis'] != 'teks')
                                    <ul class="answerList">
                                        <li>
                                            <label>
                                                <input type="radio" name="{{ $first['id'] }}"
                                                    data-alternateName="answer[1]" data-alternateValue="Sangat baik"
                                                    data-alternatetype="checkbox" value="Sangat baik" id="answer-1-0">
                                                {{ $first['pilihan1'] }}.
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="radio" name="{{ $first['id'] }}" data-alternatetype="radio"
                                                    data-alternateName="answer[1]" data-alternateValue="Baik" value="Baik"
                                                    id="answer-1-1">
                                                {{ $first['pilihan2'] }}.
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="radio" name="{{ $first['id'] }}" data-alternatetype="radio"
                                                    data-alternateName="answer[1]" data-alternateValue="Cukup Baik"
                                                    value="Cukup Baik" id="answer-1-2">
                                                {{ $first['pilihan3'] }}.
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="radio" name="{{ $first['id'] }}" data-alternatetype="radio"
                                                    data-alternateName="answer[1]" data-alternateValue="Kurang Baik"
                                                    value="Kurang Baik" id="answer-1-3">
                                                {{ $first['pilihan4'] }}.
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="radio" name="{{ $first['id'] }}" data-alternatetype="radio"
                                                    data-alternateName="answer[1]" data-alternateValue="Tidak Baik"
                                                    value="Tidak Baik" id="answer-1-4">
                                                {{ $first['pilihan5'] }}.
                                            </label>
                                        </li>
                                    </ul>
                                @else
                                    <textarea name="{{ $first['id'] }}" data-alternateName="answer[1]"
                                        data-alternateValue="Text" id="answer-1-0" class="form-control"
                                        placeholder="Fill Textarea..." cols="30" rows="10"></textarea>
                                @endif
                            </div>
                            @php
                                $total = count($data);
                                $no = 2;
                                $datas = $data;
                                unset($datas[0]);
                            @endphp
                            @foreach ($datas as $dt)
                                <div class="questionsBox hidden" data-question="{{ $no }}">
                                    <div class="questions">{{ $dt['pertanyaan'] }}</div>
                                    @if ($dt['jenis'] != 'teks')
                                        <ul class="answerList">
                                            <li>
                                                <label>
                                                    <input type="radio" name="{{ $dt['id'] }}"
                                                        data-alternatetype="radio"
                                                        data-alternateName="answer[{{ $no }}]"
                                                        data-alternateValue="Sangat Baik" value="Sangat Baik"
                                                        id="answer-{{ $no }}-0"> {{ $dt['pilihan1'] }}</label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="{{ $dt['id'] }}"
                                                        data-alternatetype="radio"
                                                        data-alternateName="answer[{{ $no }}]"
                                                        data-alternateValue="Baik" value="Baik"
                                                        id="answer-{{ $no }}-1"> {{ $dt['pilihan2'] }}</label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="{{ $dt['id'] }}"
                                                        data-alternatetype="radio"
                                                        data-alternateName="answer[{{ $no }}]"
                                                        data-alternateValue="Cukup Baik" value="Cukup Baik"
                                                        id="answer-{{ $no }}-2">{{ $dt['pilihan3'] }}</label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="{{ $dt['id'] }}"
                                                        data-alternatetype="radio"
                                                        data-alternateName="answer[{{ $no }}]"
                                                        data-alternateValue="Kurang Baik" value="Kurang Baik"
                                                        id="answer-{{ $no }}-3">{{ $dt['pilihan4'] }}</label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="{{ $dt['id'] }}"
                                                        data-alternatetype="radio"
                                                        data-alternateName="answer[{{ $no }}]"
                                                        data-alternateValue="Tidak Baik" value="Tidak Baik"
                                                        id="answer-{{ $no }}-4">{{ $dt['pilihan5'] }}</label>
                                            </li>
                                        </ul>
                                    @else
                                        <textarea name="{{ $dt['id'] }}"
                                            data-alternateName="answer[{{ $no }}]" data-alternateValue="Text"
                                            id="answer-{{ $no }}-0" class="form-control"
                                            placeholder="Fill Textarea..." cols="30" rows="10"></textarea>
                                    @endif

                                </div>
                                @php
                                    $no++;
                                @endphp
                            @endforeach

                        @endif
                        <div class="questionsRow mt-3">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="javascript:void(0);" class="button disabled"
                                        id="back-to-prev-question">Back</a>
                                </div>
                                <div class="col-md-2 ml-2">
                                    <a href="javascript:void(0);" class="button" id="go-to-next-question">Next</a>
                                </div>
                                <div class="col-md-2 ml-2">
                                    <a href="javascript:void(0);" id="finishExams"
                                        class="btn btn-success {{ count($data) != 1 ? 'disabled' : '' }}">
                                        <b>Finish</b>
                                    </a>
                                </div>
                                <div class="col-md-2 ml-2">
                                    @if (!empty($data))
                                        <a class="btn btn-default disabled"><u id="current-question-number-label">1</u> Of
                                            {{ $total }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" value="1" id="currentQuestionNumber" name="current" />
                    <input type="hidden" value="{{ count($data) }}" id="totalOfQuestion" name="total" />
                    <input type="hidden" value="[]" id="markedQuestion" name="marked" />
                    <input type="hidden" value="{{ $id_kategori }}" id="kategoryQuestion" name="id_kategori" />

                </form>
            </div>
            <div class="col-md-3" id="quick-access-section">
                <table class="table table-responsive table-borderd table-hover table-striped text-center">
                    <thead class="question-response-header">
                        <tr>
                            <th class="text-center">Question</th>
                            <th class="text-center">Response</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $nomer = 1;
                        @endphp
                        @foreach ($data as $dt)
                            <tr class="question-response-rows" data-question="{{ $nomer }}">
                                <td>{{ $nomer }}</td>
                                <td class="question-response-rows-value">-</td>
                            </tr>
                            @php
                                $nomer++;
                            @endphp
                        @endforeach

                    </tbody>
                </table>
                <div class="col-xs-12">
                    <a href="javascript:void(0)" class="btn btn-success" id="quick-access-prev">
                        < Back</a>
                            <span class="alert alert-info" id="quick-access-info"></span>
                            <a href="javascript:void(0)" class="btn btn-success" id="quick-access-next">Next ></a>
                </div>
            </div>
        </div>

        <!-- Exmas Footer - Multi Step Pages Footer -->

    </div>
    <div class="modal fade" id="finishExamsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Rangkuman Survey Anda</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <span>Total Pertanyaan yang sudah dijawab</span>
                        <span class="finishExams-total-answerd"></span>
                    </div>
                    <div>
                        <span>Total Pertanyaan yang ditandai</span>
                        <span class="finishExams-total-marked"></span>
                    </div>
                    <div>
                        <span>Total Pertanyaan yang belum terjawab</span>
                        <span class="finishExams-total-remaining"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveData"><i class="fas fa-check-circle"></i>
                        Selesai</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var examWizard = $.fn.examWizard({
            finishOption: {
                enableModal: true,
            },
            quickAccessOption: {
                quickAccessPagerItem: 9,
            },
        });

        $('#saveData').click(function() {
            let totalJawaban = examWizard.getTotalOfAnswerdValue();
            let pertanyaan = "{{ count($data) }}";
            if (totalJawaban != pertanyaan) {
                swa("Gagal!", "Harap isi dulu semua survey", "error");
            } else {
                let data = examWizard.getAllFormData();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-simpan_survey') }}",
                    data: {
                        data
                    },
                    beforeSend: function() {
                        $("#saveData").html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $("#saveData").html('<i class="fas fa-check-circle"></i> Selesai');
                        if (data.status == 'berhasil') {
                            window.location = '{{ route('program', 'alumni') }}'
                        }
                        swa(data.status + "!", data.message, data.icon);
                    }
                });
            }

        });
    </script>
@endsection
