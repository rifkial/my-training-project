@extends('content.alumni.survey.admin.v_survey')
@section('content_admin_alumni')
    <style>
        i.fas.fa-chart-bar.fa-2x {
            background: #ff00d4d7 none repeat scroll 0 0;
            border: 1px solid transparent;
            border-radius: 50%;
            box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
            content: "";
            font-size: 25px;
            height: 55px;
            line-height: 55px;
            text-align: center;
            transition: all 0.3s ease 0s;
            width: 55px;
        }

    </style>
    <div class="col-lg-8">
        <div class="widget-bg" style="background: #f2f4f8 !important">
            <div class="row">
                <div class="col-md-12" id="resultSurvey">
                    <section class="pt-5 pb-5" style="">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table-responsive table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col"><a href="{{ route('alumni-survey_by_kategori', $id_kategori) }}" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Kembali</a></th>
                                                <th scope="col" class="bg-info text-white p-2 p-lg-3 border text-center">
                                                    Pilihan 1</th>
                                                <th scope="col" class="bg-info text-white p-2 p-lg-3 border text-center">
                                                    Pilihan 2</th>
                                                <th scope="col" class="bg-info text-white p-2 p-lg-3 border text-center">
                                                    Pilihan 3</th>
                                                <th scope="col" class="bg-info text-white p-2 p-lg-3 border text-center">
                                                    Pilihan 4</th>
                                                <th scope="col" class="bg-info text-white p-2 p-lg-3 border text-center">
                                                    Pilihan 5</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $jawaban1 = 0;
                                                $jawaban2 = 0;
                                                $jawaban3 = 0;
                                                $jawaban4 = 0;
                                                $jawaban5 = 0;
                                            @endphp
                                            @foreach ($data['pertanyaan'] as $pertanyaan)
                                                <tr class="bg-white">
                                                    <th scope="row" style="min-width:225px;" class="pr-lg-3 p-2">
                                                        {{ $pertanyaan['pertanyaan'] }}</th>
                                                    <td class="p-2 p-lg-3 border text-center">
                                                        {{ $pertanyaan['jawaban1'] }}</td>
                                                    <td class="p-2 p-lg-3 border text-center">
                                                        {{ $pertanyaan['jawaban2'] }}</td>
                                                    <td class="p-2 p-lg-3 border text-center">
                                                        {{ $pertanyaan['jawaban3'] }}</td>
                                                    <td class="p-2 p-lg-3 border text-center">
                                                        {{ $pertanyaan['jawaban4'] }}</td>
                                                    <td class="p-2 p-lg-3 border text-center">
                                                        {{ $pertanyaan['jawaban5'] }}</td>
                                                </tr>
                                                @php
                                                    $jawaban1 += $pertanyaan['jawaban1'];
                                                    $jawaban2 += $pertanyaan['jawaban2'];
                                                    $jawaban3 += $pertanyaan['jawaban3'];
                                                    $jawaban4 += $pertanyaan['jawaban4'];
                                                    $jawaban5 += $pertanyaan['jawaban5'];
                                                @endphp
                                            @endforeach

                                            <tr class="bg-white">
                                                <th scope="row" class="p-2 p-lg-3 border"
                                                    style="min-width:225px;" class="pr-lg-3">Total</th>
                                                <th class="p-2 p-lg-3 border text-center">{{ $jawaban1 }}
                                                </th>
                                                <th class="p-2 p-lg-3 border text-center">{{ $jawaban2 }}
                                                </th>
                                                <th class="p-2 p-lg-3 border text-center">{{ $jawaban3 }}
                                                </th>
                                                <th class="p-2 p-lg-3 border text-center">{{ $jawaban4 }}
                                                </th>
                                                <th class="p-2 p-lg-3 border text-center">{{ $jawaban5 }}
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <div class="widget-bg">
                                        {!! $jawabanChart->container() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    {!! $jawabanChart->script() !!}

@endsection
