@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .effect-1 {
            position: relative;
        }

        .effect-1::before,
        .effect-1::after {
            z-index: -1;
            position: absolute;
            content: "";
            bottom: 15px;
            left: 10px;
            width: 50%;
            top: 80%;
            max-width: 300px;
            background: #aaa;
            box-shadow: 0 15px 10px #aaa;
            transform: rotate(-3deg);
        }

        .effect-1::after {
            transform: rotate(3deg);
            right: 10px;
            left: auto;
        }

        body {
            min-height: 100vh;
            background: #fafafa;
        }

        section {
            position: relative;
        }

        .card {
            margin-bottom: 2.5rem;
            border: none;
            position: relative;
        }

        .aksi {
            position: absolute;
            right: 15px;
            top: 15px;
        }

        .widget-list {
            margin-top: 0;
        }

    </style>
    <div class="row">
        <div class="col-lg-4 mt-5">
            <section class="pb-5 header text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <button class="btn btn-info pull-right" id="addKategori"> <i class="fas fa-plus-circle"></i> Tambah Kategori</button>
                        </div>
                        <div class="col-lg-12" id="list_kategori">
                            @foreach ($kategori as $kat)
                                <div class="card effect-1">
                                    <div class="aksi">
                                        <a href="javascript:void(0)" data-id="{{ $kat['id'] }}"
                                            class="deleteKat{{ $kat['id'] }}"
                                            onclick="return deleteKat({{ $kat['id'] }})"><i
                                                class="fas fa-trash-alt"></i></a> &nbsp;&nbsp;
                                        <a href="javascript:void(0)" onclick="edit({{ $kat['id'] }})"
                                            class="editKat{{ $kat['id'] }}"><i class="fas fa-pencil-alt"></i></a>
                                    </div>
                                    <a
                                        href="{{ route('alumni-survey_by_kategori', (new \App\Helpers\Help())->encode($kat['id'])) }}">
                                        <div class="card-body p-5">

                                            <h2 class="h5">{{ $kat['nama'] }}</h2>
                                            <p class="font-italic text-muted">{{ $kat['keterangan'] }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </section>
        </div>
        @yield('content_admin_alumni')
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="kategoriModal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="headingKat"></h5>
                </div>
                <form id="formKategori" name="formKategori" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_kategori">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Nama Kategori</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan[]" id="keterangan" rows="3"
                                            class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="fomAddKategori">

                                </div>
                                <div class="form-group tambahBaris">
                                    <div class="col-sm-12">
                                        <a href="javascript:void(0)" onclick="tambahData()"
                                            class="btn btn-success btn-sm"><i class="fa fa-plus"></i> tambah baris</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveKat"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#addKategori').click(function() {
                $('#CustomerForm').trigger("reset");
                $('.tambahBaris').show('');
                $('#headingKat').html("Tambah Kategori Survey");
                $('#kategoriModal').modal('show');
                $('#action').val('Add');
            });

            $('#formKategori').on('submit', function(event) {
                event.preventDefault();
                $("#saveKat").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveKat").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-kategori_survey_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-kategori_survey_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        $('#list_kategori').html('<div id="loading" style="" ></div>');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            $('#kategoriModal').modal('hide');
                            $('#formKategori').trigger("reset");
                            location.reload();
                        } else {
                            $("#list_kategori").load(window.location.href +
                                " #list_kategori");
                        }
                        noti(data.icon, data.message);
                        $('#saveKat').html('Simpan');
                        $("#saveKat").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        });

        // const url = $(this).attr('href')

        function deleteKat(id) {
            swal({
                title: "Apa kamu yakin?",
                text: "ingin menghapus data ini!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function() {
                $.ajax({
                    url: "{{ route('alumni-kategori_survey_soft-delete') }}",
                    type: "POST",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $('.deleteKat' + id).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        if (data.status == 'berhasil') {
                            location.reload()
                        } else {
                            $(".deleteKat" + id).html('<i class="fas fa-trash-alt"></i>');
                        }
                        swa(data.status + "!", data.message, data.icon);
                    }
                })
            }, function(dismiss) {
                if (dismiss === 'cancel') {
                    swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                }
            })
        }

        function edit(id) {
            $('#form_result').html('');
            $('.fomAddKategori').html('');
            $('.tambahBaris').hide('');
            $.ajax({
                type: 'POST',
                url: "{{ route('alumni-kategori_survey_detail') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(".editKat" + id).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(data) {
                    $(".editKat" + id).html('<i class="fas fa-pencil-alt"></i>');
                    $('#headingKat').html("Edit Kategori Survey");
                    $('#saveBtn').val("edit-user");
                    $('#id_kategori').val(data.id);
                    $('#nama').val(data.nama);
                    $('#keterangan').val(data.keterangan);
                    $('#action').val('Edit');
                    $('#kategoriModal').modal('show');
                }
            });
        }

        var i = 1;

        function tambahData() {
            i++;
            $('.fomAddKategori').append(
                '<div id="row' + i +
                '"><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Nama Kategori</label><div class="col-sm-12"><input type="text" class="form-control" id="nama" name="nama[]" autocomplete="off" required></div></div><div class="form-group mb-1 mt-3"><label for="name" class="col-sm-12 control-label">Keterangan</label><div class="col-sm-12"><textarea name="keterangan[]" id="keterangan" cols="30" rows="3" class="form-control"></textarea></div></div><div class="col-sm-12 mt-1"><a href="javascript:void(0)" class="btn btn-danger btn-xs btn-remove mb-2" id="' +
                i + '">Hapus Baris</a></div></div>'
            );
        }

        $(document).on('click', '.btn-remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    </script>
@endsection
