@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@php
$temp = 'template/template' . $ext . '/app';
@endphp
@extends($temp)
@section('content')
    <style>
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }

    </style>
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">

                    <div class="row mt-3">
                        <div class="col-md-12 d-flex justify-content-between">
                            <h3 class="box-title">{{ Session::get('title') }}</h3>
                            @if (session('role') == 'admin' || session('role') == 'bk')
                                <div class="tombol">
                                    <button class="btn btn-info" id="addSiswa"><i class="fas fa-user-plus"></i> Tambah
                                        Siswa</button>
                                    <button id="importSiswa" class="btn btn-purple"><i class="fas fa-file-import"></i>
                                        Import</button>
                                    <button id="trashSiswa" class="btn btn-warning"><i class="fas fa-trash-restore"></i>
                                        Sampah</button>
                                </div>
                            @endif

                        </div>
                    </div>
                    <hr>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-between">
                                <button class="btn btn-danger" id="delete_select"><i class="fas fa-trash"></i></button>
                                <form class="navbar-form pull-right" role="search">
                                    <div class="input-group">
                                        @php
                                            $serc = str_replace('-', ' ', $search);
                                        @endphp
                                        <input type="text" value="{{ $serc }}" id="search" name="search"
                                            class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <a href="javascript:void(0)" id="fil" onclick="filter('{{ $routes }}')"
                                                class="btn btn-info"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <p></p>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-info vertical-middle">
                                                <th class="text-center">
                                                    <div class="form-check m-0">
                                                        <input class="form-check-input m-0" type="checkbox" id="checkAll">
                                                    </div>
                                                </th>
                                                <th>No</th>
                                                <th>Profile</th>
                                                <th>Telepon</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (empty($admin))
                                                <tr>
                                                    <td colspan="8" class="text-center">Data saat ini kosong</td>
                                                </tr>
                                            @else
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($admin as $adm)
                                                    @php
                                                        $status = 'Aktif';

                                                        $color = 'badge badge-success text-inverse';
                                                        if ($adm['status'] == '0') {
                                                            $status = 'Tidak Aktif';
                                                            $color = 'badge badge-danger text-inverse';
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td class="text-center vertical-middle">
                                                            <div class="form-check m-0">
                                                                <input class="form-check-input m-0 siswa_entry"
                                                                    type="checkbox" name="admin[]"
                                                                    value="{{ $adm['id'] }}">
                                                            </div>
                                                        </td>
                                                        <td class="vertical-middle">{{ $no++ }}</td>

                                                        <td class="vertical-middle"><b>{{ ucwords($adm['nama']) }}
                                                                <br><small>Username : {{ $adm['username'] }}</small></b>
                                                        </td>
                                                        <td class="vertical-middle">{{ $adm['telepon'] }}</td>
                                                        <td class="vertical-middle">{{ ucwords($adm['email']) }}</td>
                                                        <td class="vertical-middle">
                                                            <label class="switch">
                                                                <input type="checkbox"
                                                                    {{ $adm['status'] == 1 ? 'checked' : '' }}
                                                                    class="admin_check" data-id="{{ $adm['id'] }}">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </td>
                                                        <td class="text-center vertical-middle">
                                                            @if (session('role') == 'admin' || session('role') == 'bk' || session('role') == 'learning-admin')
                                                                <a href="{{ route('edit_detail-siswa', ['k' => (new \App\Helpers\Help())->encode($adm['id']), 'key' => str_slug($adm['nama'])]) }}"
                                                                    class="btn btn-info btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                            @endif
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                    {!! $pagination !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    {{-- <div class="widget-bg">
        <div class="row">
            <div class="col-md-6 col-12">
                <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
            </div>
            <div class="col-md-6 col-12">
                <div class="pull-right">
                    <button class="btn btn-info" id="addData"><i class="fas fa-plus-circle"></i>
                        Tambah Data</button>
                    <button id="import" class="btn btn-outline-success"><i class="fas fa-file-import"></i>
                        Import</button>
                    <button id="data_trash" class="btn btn-outline-danger"><i class="fas fa-trash-restore"></i>
                        Trash</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="m-portlet__body">

            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-4 mb-1">
                            <div class="m-input-icon m-input-icon--left">
                                @php
                                    $serc = str_replace('-', ' ', $search);
                                @endphp
                                <label class="filter-col" style="margin-right:0;" for="pref-search">Search:</label>
                                <input value="{{ $serc }}" id="search" name="search" type="text"
                                    class="form-control m-input" placeholder="Search..." id="generalSearch">
                            </div>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Filter:</label>
                            <select id="status" name="status" class="form-control">
                                @foreach ($statuses as $st)
                                    @if ($st['value'] == $status)
                                        <option selected value="{{ $st['value'] }}">{{ $st['label'] }}</option>
                                    @else
                                        <option value="{{ $st['value'] }}">{{ $st['label'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Order
                                by:</label>
                            <select id="sort" name="sort" class="form-control">
                                @foreach ($sorts as $s)
                                    @if ($s == $sort)
                                        <option selected value="{{ $s }}">{{ $s }}</option>
                                    @else
                                        <option value="{{ $s }}">{{ $s }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-perpage">Per
                                page:</label>
                            <select id="per_page" name="per_page" class="form-control">
                                @foreach ($pages as $pg)
                                    @if ($pg == $perPage)
                                        <option selected value="{{ $pg }}">{{ $pg }}</option>
                                    @else
                                        <option value="{{ $pg }}">{{ $pg }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 mb-1">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">&nbsp;</label>
                            <a href="javascript:void(0)" id="fil" onclick="test('{{ $routes }}')"
                                class="btn btn-outline-info btn-block"><i class="fa fa-dot-circle-o"></i> Display</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <p></p>
                <table class="table table-striped- table-bordered table-hover" id="table-data">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($data))
                            <tr>
                                <td colspan="4" style="text-align: center">Data saat ini kosong</td>
                            </tr>
                        @else
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($data as $adm)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $adm['username'] }}</td>
                                    <td>{{ $adm['nama'] }}</td>
                                    <td>{{ $adm['email'] }}</td>
                                    <td>

                                        <label class="switch">
                                            <input type="checkbox" {{ $adm['status'] == 1 ? 'checked' : '' }}
                                                class="admin_check" data-id="{{ $adm['id'] }}">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <button type="button" data-id="{{ $adm['id'] }}"
                                            class="btn btn-success btn-sm detail" data-toggle="tooltip"
                                            data-original-title="Detail"><i class="fas fa-info-circle"></i></button>
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit"
                                            data-id="{{ $adm['id'] }}" class="edit btn btn-info btn-sm edit"><i
                                                class="fas fa-user-edit"></i></a>
                                        <button type="button" name="delete" data-id="{{ $adm['id'] }}"
                                            class="btn btn-danger btn-sm delete"><i class="fas fa-backspace"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            {!! $pagination !!}
        </div>
    </div> --}}
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="ajaxModel" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse" style="background: #03a9f3;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_admin">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Username</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="username" name="username"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Nama</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-12">
                                        <select name="jenkel" id="jenkel" class="form-control">
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option value="l">Laki - laki</option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Agama</label>
                                    <div class="col-sm-12">
                                        <select name="agama" id="agama" class="form-control">
                                            <option value="">-- Pilih Agama</option>
                                            <option value="islam">Islam</option>
                                            <option value="kristen">Kristen</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="budha">Budha</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Telepon</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="telepon" id="telepon" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Email</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Tempat, Tanggal lahir</label>
                                    <div class="row mr-0 ml-0">
                                        <div class="col-sm-6">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Alamat</label>
                                    <div class="col-sm-12">
                                        <textarea name="alamat" class="form-control" id="alamat" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name" class="col-sm-12 control-label">Gambar</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" name="image" accept="image/*"
                                            onchange="readURL(this);">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn"
                            value="create">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('content.admin.master.import')

    <script type="text/javascript">
        var url_import = "{{ route('import-siswa') }}";
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#addData').click(function() {
                $('#modelHeading').html("Tambah {{ session('title') }}");
                $('.tambahBaris').show('');
                $('#ajaxModel').modal('show');
                $('#action').val('Add');
            });

            $('#import').click(function() {
                $('#importModal').modal('show');
                $('#file-chosen').html('No file choosen');
                $('#HeadingImport').html('Import Data Siswa');
            });

            $('body').on('submit', '#CustomerForm', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('alumni-admin_create') }}";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('alumni-admin_update') }}";
                }

                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: action_url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('.table-responsive').html('<div id="loading" style="" ></div>');
                    },
                    success: (data) => {
                        if (data.status == 'berhasil') {
                            $('#ajaxModel').modal('hide');
                            $('#CustomerForm').trigger("reset");
                        }
                        $(".table-responsive").load(window.location.href +
                            " .table-responsive");
                        noti(data.icon, data.message);
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $('#form_result').html('');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('alumni-admin_edit') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-user-edit"></i>');
                        $('#modelHeading').html("Edit Data {{ session('title') }}");
                        $('#id_admin').val(data.id);
                        $('#nama').val(data.nama);
                        $('#username').val(data.username);
                        $('#jenkel').val(data.jenkel_kode).trigger('change');
                        $('#agama').val(data.agama).trigger('change');
                        $('#telepon').val(data.telepon);
                        $('#email').val(data.email);
                        $('#tempat_lahir').val(data.tempat_lahir);
                        $('#tgl_lahir').val(data.tgl_lahir);
                        $('#alamat').val(data.alamat);
                        $('#ajaxModel').modal('show');
                        $('#action').val('Edit')
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('alumni-admin_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                            $('.table-responsive').html(
                                '<div id="loading" style="" ></div>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $(".table-responsive").load(window.location.href +
                                    " .table-responsive");
                                $('#data-trash').dataTable().fnDraw(false);
                            }
                            swa(data.status + "!", data.message, data.success);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });

        function changeStatus(val, id) {
            if (id == 1) {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Tidak Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="N"]')
                    .removeClass('active').addClass('notActive');
            } else {
                $('a[data-toggle="pilihan_status' + id + '"]').not('[data-title="Aktif"]')
                    .removeClass('notActive').addClass(
                        'active');
                $('a[data-toggle="pilihan_status' + id + '"][data-title="Tidak Aktif"]')
                    .removeClass('active').addClass('notActive');
            }
            updateStatus(val, id);
        }

        function updateStatus(value, id) {
            $.ajax({
                type: "POST",
                url: "{{ route('update_status-admin_alumni') }}",
                data: {
                    value,
                    id
                },
                success: function(data) {
                    noti(data.success, data.message);
                }
            });
        }

        function filter(routes) {
            console.log(routes);
            var searchs = (document.getElementById("search") != null) ? document.getElementById("search").value : "";
            var search = searchs.replace(/ /g, '-')
            var url_asli = window.location.href;
            var explode_url = url_asli.split("?")[0];
            var url = explode_url + "?search=" + search;
            document.location = url;
        }
    </script>
@endsection
