<x-spp.layout current-page="Siswa">
@push('gaya_custom')
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
/* ini gaya custom */
</style>
@endpush

<div class="row">
    <div class="col-md-10 col-sm-12 mx-auto">
        <div class="widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs tabs-bordered mr-t-10">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a href="#tagihan-bulan-ini" class="nav-link active" data-toggle="tab" aria-expanded="true">Tagihan</a>
                                    </li>
                                    <li class="nav-item"><a href="#tagihan-semua" class="nav-link" data-toggle="tab" aria-expanded="false">Semua Tagihan</a>
                                    </li>
                                    <li class="nav-item"><a href="#riwayat-transaksi" class="nav-link" data-toggle="tab" aria-expanded="false">Riwayat Transaksi</a>
                                    </li>
                                    <li class="nav-item"><a href="#info-siswa" class="nav-link" data-toggle="tab" aria-expanded="false">Informasi Orang Tua/Wali</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <!-- TAB: Tagihan/"Invoice" -->
                                    <div class="tab-pane active" id="tagihan-bulan-ini">
                                        <div class="row mr-b-20 clearfix">
                                            <div class="col-lg-8">
                                                <h5 class="box-title mr-b-0">
                                                    Tagihan Bulan {{ 'Juli' }}
                                                </h5>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="btn-group float-right">
                                                    <a
                                                        class="btn btn-block btn-outline-default ripple"
                                                        data-toggle="modal"
                                                        data-target="#modal-create-transaksi"
                                                    >
                                                        <i class="list-icon material-icons">receipt</i>
                                                        Bayar Manual
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mr-b-20">
                                            <div class="col-md-6">
                                                <div class="weather-card-default">
                                                    <section>
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 180px;">Nama Siswa Tertagih</td>
                                                                    <td class="text-muted">{{ $dataSiswa['nama'] }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 180px;">NIS</td>
                                                                    <td class="text-muted">{{ $dataSiswa['nis'] }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </section>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="weather-card-default">
                                                    <section>
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 180px;">Nama Orang Tua/Wali</td>
                                                                    <td class="text-muted">{{ $dataOrtu['nama'] }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 180px;">Kontak Orang Tua</td>
                                                                    <td class="text-muted">{{ $dataOrtu['telepon'] }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>

                                        <h5 class="mr-t-50 mr-b-20">Rincian Tagihan Bulan Ini</h5>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table rel="invoice-lines" class="table table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama Tagihan</th>
                                                            <th>Tahun Ajaran</th>
                                                            <th class="text-right">Jumlah Belum Terbayar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="list" rel="line-list">
                                                        @if (empty($dataTagihanBulanIni['tagihan']))

                                                        
                                                        @else
                                                            @foreach ($dataTagihanBulanIni['tagihan'] as $line)
                                                            <tr>
                                                                <td class="">{{ $line['nama'] }}</td>
                                                                <td class="">{{ $line['tahun_ajaran'] }}</td>
                                                                <td class="text-right">{{ $line['nominal'] - $line['dibayar'] }}</td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- TAB: Semua Tagihan -->
                                    <div class="tab-pane" id="tagihan-semua">
                                        <h5 class="box-title">Detail Kewajiban Pembayaran Siswa</h5>

                                        <div class="row mr-t-20">
                                            <div class="col-md-12">
                                                <i data-komponen="ikon-search-tagihan" class="list-icon material-icons fs-28">search</i>
                                                <table id="tabel-tagihan" class="table table-responsive mt-20">
                                                    <thead>
                                                        <tr>
                                                            <th>Tahun Ajaran</th>
                                                            <th>Kewajiban</th>
                                                            <th>Periode</th>
                                                            <th>Nominal</th>
                                                            <th>Tanggal Bayar Terakhir</th>
                                                            <th>Total Terbayar</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- TAB: Riwayat Transaksi -->
                                    <div class="tab-pane" id="riwayat-transaksi">
                                        <h5 class="box-title">Tagihan Terbayar</h5>

                                        <div class="row mr-t-20">
                                            <div class="col-md-12">
                                                <i data-komponen="ikon-search-transaksi" class="list-icon material-icons fs-28">search</i>
                                                <table id="tabel-transaksi-siswa" class="table-responsive mt-20">
                                                    <thead>
                                                        <tr>
                                                            <th>Kode</th>
                                                            <th>Tanggal Bayar</th>
                                                            <th>Metode Bayar</th>
                                                            <th>Nominal</th>
                                                            <th>Tahun Ajaran</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Tab: Info -->
                                    <div class="tab-pane" id="info-siswa">
                                        <h5 class="box-title">Kontak</h5>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <h6 class="text-muted text-uppercase">
                                                    <i class="list-icon material-icons fs-15">person</i>
                                                    Nama Orang Tua/Wali
                                                </h6>
                                                
                                                <p class="mr-t-0">{{ $dataOrtu['nama'] }}</p>
                                            </div>

                                            <div class="col-md-6">
                                                <h6 class="text-muted text-uppercase">
                                                    <i class="list-icon material-icons fs-15">phone</i>
                                                    Telepon
                                                </h6>
                                                <p class="mr-t-0">{{ $dataOrtu['telepon'] }}</p>
                                            </div>

                                            <div class="col-md-6">
                                                <h6 class="text-muted text-uppercase">
                                                    <i class="list-icon material-icons fs-15">email</i>
                                                    Email
                                                </h6>
                                                <p class="mr-t-0">{{ $dataOrtu['email'] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<!-- View Rincian Tagihan -->
<div
    id="modal-view-tagihan"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <span rel="field-nama"></span>
                    <small>Kewajiban pembayaran</small>
                </h5>

                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="text-uppercase">Tahun Ajaran</h6>
                                <p rel="field-tahun-ajaran" class="mr-t-0">Namamamama</p>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <h6 class="text-uppercase">Periode Pembayaran</h6>
                                <p rel="field-periode" class="mr-t-0">Namamamama</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <h6 class="text-uppercase">Besar Tagihan</h6>
                                <p rel="field-nominal" class="mr-t-0">Namamamama</p>
                            </div>
                            <div class="col-sm-12">
                                <h6 class="text-uppercase">Total Terbayar</h6>
                                <p rel="field-total-dibayar" class="mr-t-0">Namamamama</p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="mr-tb-50">

                <h5 class="mr-b-20">Riwayat Pembayaran</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div rel="list-riwayat" class="list-group">
                            elelelelee
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Create Transaksi / Bayar Manual -->
<div
    id="modal-create-transaksi"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="box-title mr-b-20">
                    <i class="list-icon material-icons text-muted">add_circle</i>
                    Transaksi
                    <small class="text-muted">Tambahkan transaksi.</small>
                </h5>

                <form id="form-create-transaksi" class="form-material">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <input
                                    class="form-control"
                                    id="field-nis-create"
                                    name="nis"
                                    placeholder="Nomor induk siswa..."
                                    type="text"
                                    value="{{ $dataSiswa['nis'] }}"
                                    disabled
                                >
                                <label for="field-nis-create">Nomor Induk Siswa</label>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <select
                                    name="tahun_ajaran"
                                    id="field-tahun-ajaran-create"
                                    class="form-control"
                                    data-placeholder="Pilih Tahun Ajaran"
                                    data-toggle="select2"
                                >
                                    <option disabled="disabled" selected="selected"></option>
                                    @foreach ($dataTahunAjaran as $tahun)
                                    <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                    @endforeach
                                </select>
                                <label for="field-tahun-ajaran-create">Tahun Ajaran</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input
                                        id="field-tgl-bayar-create"
                                        name="tgl_bayar"
                                        data-field="tgl-bayar"
                                        class="form-control datepicker"
                                        placeholder="Tanggal bayar..."
                                        type="text"
                                        value="{{ session('tanggalHariIni') }}"
                                        data-default="{{ session('tanggalHariIni') }}"
                                        data-plugin-options='{"autoclose": true}'
                                    >
                                    <span class="input-group-addon">
                                        <i class="list-icon material-icons">date_range</i>
                                    </span>
                                </div>
                                <label for="field-tgl-bayar-create">Tanggal Bayar</label>
                            </div>
                        </div>
                    </div>

                    <hr class="mr-tb-50" />
                    <h5 class="mr-b-20">Rincian Pembayaran</h5>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="list-details" class="table table-responsive">
                                <tbody class="list" rel="line-list"></tbody>
                            </table>

                            <div rel="line-action-editor">
                                <u>
                                    <a href="javascript:void(0)">
                                        Tambahkan item pembayaran
                                    </a>
                                </u>
                            </div>
                        </div>
                    </div>

                    <div rel="line-inline-editor" style="display: none;">
                        <div class="row mr-t-10">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="id_tagihan"
                                        id="field-line-id-tagihan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option selected disabled></option>
                                        @foreach ($dataSiswa['tagihan'] as $tagihan)
                                        <option value="{{ $tagihan['id'] }}">{{ $tagihan['nama'] }}</option>
                                        @endforeach
                                    </select>
                                    <label for="field-line-id-tagihan-create">Tagihan</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                @php
                                    $dataValues = [];
                                    $dataPeriode = [];
                                    foreach ($dataSiswa['tagihan'] as $tagihan) {
                                        $dataValues[$tagihan['id']] = $tagihan['nominal'];
                                        $dataPeriode[$tagihan['id']] = $tagihan['priode'];
                                    }
                                @endphp
                                <div class="form-group">
                                    <input
                                        id="field-line-nominal-create"
                                        name="nominal"
                                        class="form-control"
                                        placeholder=""
                                        type="text"
                                        data-values='@json($dataValues)'
                                        data-periode='@json($dataPeriode)'
                                    >
                                    <label for="field-line-nominal-create">Nominal</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select
                                        name="bulan"
                                        id="field-line-bulan-create"
                                        class="form-control"
                                        data-toggle="select2"
                                    >
                                        <option disabled selected></option>
                                        <option value="1">Juli</option>
                                        <option value="2">Agustus</option>
                                        <option value="3">September</option>
                                        <option value="4">Oktober</option>
                                        <option value="5">November</option>
                                        <option value="6">Desember</option>
                                        <option value="7">Januari</option>
                                        <option value="8">Februari</option>
                                        <option value="9">Maret</option>
                                        <option value="10">April</option>
                                        <option value="11">Mei</option>
                                        <option value="12">Juni</option>
                                    </select>
                                    <label for="field-line-bulan-create" class="form-control-label">Untuk Bulan</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a
                                    class="btn btn-default"
                                    type="button"
                                    rel="line-action-tambah"
                                >+</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="form-actions btn-list ml-auto">
                    <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                    <button class="btn btn-color-scheme" type="submit">Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- View Transaksi -->
<div
    id="modal-view-transaksi"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    style="display: none"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row header-transaksi mr-b-20">
                    <div class="col-md-8">
                        <h5 class="box-title mr-b-20">
                            # <span rel="field-kode"></span>
                            <small>Transaksi Tagihan Terbayar</small>
                        </h5>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="btn-group float-right">
                            <a
                                class="btn btn-block btn-outline-default ripple"
                                rel="tombol-cetak-kwitansi"
                            >
                                <i class="list-icon material-icons">print</i>
                                Cetak Kwitansi
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="weather-card-default">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <td style="width: 180px;">Nama Siswa</td>
                                        <td class="text-muted">{{ $dataSiswa['nama'] }}</td>
                                        <td style="width: 180px;">Nomor Induk Siswa</td>
                                        <td class="text-muted">{{ $dataSiswa['nis'] }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 180px;">Tahun Ajaran</td>
                                        <td rel="field-tahun-ajaran" class="text-muted"></td>
                                        <td style="width: 180px;">Tanggal Bayar</td>
                                        <td rel="field-tgl-bayar" class="text-muted"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <hr>

                <h5 class="mr-b-20">Rincian Pembayaran</h5>

                <div class="row">
                    <div class="col-md-12">
                        <table id="line-transaksi" class="table table-sm">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody class="list"></tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right">Total Bayar</th>
                                    <th class="text-right" rel="field-nominal"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('js_custom')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js" integrity="sha512-t3XNbzH2GEXeT9juLjifw/5ejswnjWWMMDxsdCg4+MmvrM+MwqGhxlWeFJ53xN/SBHPDnW0gXYvBx/afZZfGMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
(function($, global) {
    "use-strict"

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const HalamanSiswaDetail = (function () {

        let $DataTable;
        let $DataTableTransaksi;

        function init () {
            dataTableTagihan();
            dataTableTransaksi();

            modalViewTagihan();
            modalViewTransaksi();
            modalBayarManual();
        }

        function dataTableTagihan() {
            $DataTable = $('#tabel-tagihan').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                ordering:  false,
                ajax: "{{ route('spp-siswa_detail', 'idKelasSiswa') }}".replace("idKelasSiswa", {{ $dataSiswa['id'] }}),
                autoWidth: false,
                columns: [
                    { data: 'tahun_ajaran', name: 'tahun_ajaran' },
                    { data: 'nama', name: 'nama' },
                    { data: 'priode', name: 'priode' },
                    { data: 'nominal', name: 'nominal' },
                    { data: 'terakhir_bayar', name: 'terakhir_bayar' },
                    { data: 'total_dibayar', name: 'total_dibayar', render: (data) => (data ?? 0) },
                    {
                        data: 'id',
                        name: 'id',
                        render: function (data) {
                            return `
                                <a
                                    class="btn btn-outline-default ripple mr-l-20"
                                    rel="tombol-view"
                                    data-id-view="${data}"
                                    data-toggle="modal"
                                    data-target="#modal-view-tagihan"
                                >
                                    <i class="list-icon material-icons">visibility</i>
                                </a>
                            `;
                        },
                    },
                    { data: 'bulan', name: 'bulan', visible: false },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari tagihan...",
                },
            });

            $('[data-komponen="ikon-search-tagihan"]').prependTo('#tagihan-semua .form-input-icon label');
        }

        function dataTableTransaksi() {
            $DataTableTransaksi = $('#tabel-transaksi-siswa').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                ordering:  false,
                autoWidth: false,
                ajax: "{{ route('spp-siswa_transaksi', $dataSiswa['id']) }}",
                columns: [
                    { data: 'kode', name: 'kode' },
                    { data: 'tgl_bayar', name: 'tgl_bayar' },
                    { data: 'tipe', name: 'tipe' },
                    { data: 'nominal', name: 'nominal' },
                    { data: 'tahun_ajaran', name: 'tahun_ajaran' },
                    {
                        data: 'id',
                        name: 'id',
                        render (data) {
                            return `
                                <a
                                    class="btn btn-outline-default ripple mr-l-20"
                                    rel="tombol-view-transaksi"
                                    data-id-view="${data}"
                                    data-toggle="modal"
                                    data-target="#modal-view-transaksi"
                                >
                                    <i class="list-icon material-icons">visibility</i>
                                </a>
                            `;
                        },
                    },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari transaksi...",
                },
            });

            $('[data-komponen="ikon-search-transaksi"]').prependTo('#riwayat-transaksi .form-input-icon label');
        }

        function modalViewTagihan() {
            const $modal = $('#modal-view-tagihan');

            $modal.on('hidden.bs.modal', function () {
                $('[rel="field-nama"]').text('');
                $('[rel="field-tahun-ajaran"]').text('');
                $('[rel="field-periode"]').text('');
                $('[rel="field-nominal"]').text('');
                $('[rel="field-total-dibayar"]').text('');
                $('[rel="list-riwayat"]').html('');
            });

            $('#tabel-tagihan tbody').on('click', 'a[rel="tombol-view"]', function () {
                const $tr = $(this).closest('tr');
                const rowData = $DataTable.row($tr).data();

                $('[rel="field-nama"]').text(rowData.nama);
                $('[rel="field-tahun-ajaran"]').text(rowData.tahun_ajaran);
                $('[rel="field-periode"]').text(rowData.priode);
                $('[rel="field-nominal"]').text(rowData.nominal);
                $('[rel="field-total-dibayar"]').text(rowData.total_dibayar ?? 0);

                $('[rel="list-riwayat"]').html(function () {
                    // Ini perlu karena ada behavior aneh data kolom bulan jadi Objek bukan Array
                    // ...supaya jadi iterable bisa di-`map`
                    if (rowData.bulan && typeof rowData.bulan.length === 'undefined') {
                        rowData.bulan = Object.values(rowData.bulan);
                    }

                    if (!rowData.bulan || rowData.bulan.length === 0) {
                        return `
                            <div rel="item-riwayat" class="list-group-item text-center">
                                <span class="text-muted">
                                    Tidak ada riwayat untuk jenis tagihan ini
                                </span>
                            </div>
                        `;
                    }

                    return rowData.bulan.map(riwayat => {
                        const isSudahBayar = (sudah, belum) => (riwayat.tgl_bayar ? sudah : belum);
                        return `
                            <div rel="item-riwayat" class="list-group-item d-flex justify-content-end">
                                <span class="mr-auto">
                                    ${isSudahBayar(
                                        '<i class="material-icons list-icon text-success mr-2">check</i>',
                                        '<i class="material-icons list-icon text-muted mr-2">access_time</i>'
                                    )}
                                    ${riwayat.nama}
                                </span>
                                ${isSudahBayar(
                                    `<span class="fs-12 my-auto">${riwayat.tgl_bayar}</span>`,
                                    '<span class="fs-12 my-auto">&#8230;</span>'
                                )}
                            </div>
                        `;
                    });
                });
            });
        }

        function modalViewTransaksi() {
            const $modal = $('#modal-view-transaksi');

            const Lines = new List("line-transaksi", {
                valueNames: [
                    'nama_tagihan',
                    'nominal',
                    'nama_bulan',
                ],
                item:   `<tr>` +
                        `    <td><span class="nama_tagihan"></span> (<span class="nama_bulan"></span>)</td>` +
                        `    <td class="nominal text-right"></td>` +
                        `</tr>`,
            });

            $modal.on('hidden.bs.modal', function () {
                $('[rel="field-kode"]').text("");
                $('[rel="field-tgl-bayar"]').text("");
                $('[rel="field-tahun-ajaran"]').text("");
                $('[rel="field-nominal"]').text("");
                Lines.clear();
            });

            $('#tabel-transaksi-siswa tbody').on('click', 'a[rel="tombol-view-transaksi"]', async function () {
                const $tr = $(this).closest('tr');
                const rowData = $DataTableTransaksi.row($tr).data();

                $('[rel="field-kode"]').text(rowData.kode);
                $('[rel="field-tgl-bayar"]').text(rowData.tgl_bayar);
                $('[rel="field-tahun-ajaran"]').text(rowData.tahun_ajaran);
                $('[rel="field-nominal"]').text(rowData.nominal);

                try {
                    const endpointDetail = "{{ route('spp-transaksi_detail', 'KODEKODE') }}".replace("KODEKODE", rowData.kode);
                    const responDetail = await $.ajax(endpointDetail);
                    Lines.add(responDetail.body.data);
                } catch (error) {
                    console.error(error);
                }
            });

            $('[rel="tombol-cetak-kwitansi"]').on('click', function () {
                const kodeKw = $('[rel="field-kode"]').text();
                $modal.find('.modal-body').print({
                    title: kodeKw,
                    prepend: `<h5 class="text-center">Kwitansi Pembayaran Tagihan Siswa</h5>` +
                             `<h5 class="text-center mr-t-0">No. ${kodeKw}</h5>`,
                    noPrintSelector: '.btn, .header-transaksi',
                });
            });
        }

        function modalBayarManual() {
            const $modal = $('#modal-create-transaksi');
            const $form = $modal.find('form');

            const $lineItemTemplate = $modal.find('[rel="line-item"]').detach();
            const $lineActionEditor = $modal.find('[rel="line-action-editor"]');
            const $lineEditor = $modal.find('[rel="line-inline-editor"]');
            const $lineActionTambah = $modal.find('[rel="line-action-tambah"]');

            const $selectTahunAjaran = $form.find('[name="tahun_ajaran"]');
            const $inputTanggalBayar = $form.find('[name="tgl_bayar"]');
            const $inputTagihan = $lineEditor.find('[name="id_tagihan"]');
            const $inputNominal = $lineEditor.find('[name="nominal"]');
            const $inputBulan = $lineEditor.find('[name="bulan"]');

            const Lines = new List('list-details', {
                valueNames: [
                    { data: ['id_tagihan', 'bulan', 'key'] },
                    'nama_tagihan',
                    'nominal',
                    'nama_bulan',
                ],
                item:   `<tr>` +
                        `<td class="nama_tagihan"></td>` +
                        `<td class="nominal text-right"></td>` +
                        `<td class="nama_bulan"></td>` +
                        `<td style="width: 30px;">` +
                        `   <a href="javascript:void(0)" class="btn btn-sm" title="Hapus item ini">` +
                        `       <i class="material-icons list-icon fs-14 text-muted">delete</i>` +
                        `   </a>` +
                        `</td>` +
                        `</tr>`,
            });

            $modal.on('shown.bs.modal', function () {
                $selectTahunAjaran.focus();
            });

            $modal.on('hidden.bs.modal', function () {
                // Reset form & list rincian bayar
                $form.find('input:not([name="nis"], [name="tgl_bayar"]), select').val('').trigger('input');
                Lines.clear();
                _showLineEditor(false);
            });

            $lineActionEditor.on('click', 'a', function () {
                if ( !($selectTahunAjaran.val()) ) {
                    return $selectTahunAjaran.focus();
                }
                _showLineEditor(true);
            });

            $inputTagihan.change(function () {
                _renderInputNominal($(this).val());
            });

            $lineActionTambah.on('click', function (ev) {
                ev.preventDefault();

                if (!_validateEditor()) {
                    return;
                }

                const linesData = Lines.get();
                const keyNew = !linesData.length ? 1 : linesData[linesData.length - 1].values().key + 1;
                const data = {
                    key: keyNew,
                    id_tagihan: $inputTagihan.val(),
                    nama_tagihan: $inputTagihan.find('option:selected').text(),
                    nominal: $inputNominal.val(),
                    bulan: $inputBulan.val(),
                    nama_bulan: $inputBulan.find('option:selected').text(),
                }
                _renderNewLine(data);
                _showLineEditor(false);
            });

            $('[rel="line-list"]').on('click', 'a', function () {
                const key = $(this).closest('tr').data('key');
                Lines.remove('key', key);
            });

            // Submisi form diganti dengan event klik pada tombol submit itu sendiri,
            // tidak dengan event submit, untuk menghindari tidak sengaja pencet Enter ketika
            // belum selesai isi form.
            $modal.find('button[type="submit"]').on('click', async function (ev) {
                ev.preventDefault();

                _showLineEditor(false);

                // Validasi
                if ( !($selectTahunAjaran.val()) ) {
                    return $selectTahunAjaran.focus();
                }
                if ( !($inputTanggalBayar.val()) ) {
                    return $inputTanggalBayar.focus();
                }
                if ( !(Lines.get().length) ) {
                    return $lineActionEditor.find('a').focus();
                }

                const data = {
                    id_kelas_siswa: {{ $dataSiswa['id'] }},
                    nis: $form.find('[name="nis"]').val(),
                    tahun_ajaran: $selectTahunAjaran.val(),
                    tgl_bayar: $inputTanggalBayar.val(),
                    details: Lines.get()?.map(line => line.values()),
                };

                try {
                    const responPost = await $.ajax("{{ route('spp-transaksi_create') }}", {
                        method: "POST",
                        data,
                    });
                    $DataTable.ajax.reload();
                } catch (error) {
                    console.error(error);
                }

                $modal.modal('hide');
            });

            function _validateEditor($fields) {
                if (!$inputTagihan.val()) {
                    $inputTagihan.focus();
                    return false
                }
                if (!$inputBulan.val()) {
                    $inputBulan.focus();
                    return false
                }
                return true;
            }

            function _showLineEditor(show) {
                if (show) {
                    $lineActionEditor.hide();
                    $lineEditor.show();
                    $inputTagihan.focus();
                } else {
                    $lineActionEditor.show();
                    $lineEditor.hide();
                }
                $inputTagihan.val('');
                $inputNominal.val('');
                $inputBulan.val('');
            }

            function _renderNewLine(data) {
                Lines.add(data);
            }

            function _renderInputNominal(idTagihan) {
                const valNominalById = $inputNominal.data('values')[idTagihan];
                $inputNominal.val(valNominalById).trigger('input');

                // Beberapa jenis tagihan nominalnya bisa dibayar tidak sama dengan settingnya
                // ...jadi perlu editable. Sedangkan bulanan tidak bisa diedit, harus sama
                // dengan settingan.
                if ($inputNominal.data('periode')[idTagihan] === 'bulanan') {
                    $inputNominal.attr('disabled', true);
                } else {
                    $inputNominal.attr('disabled', false);
                }
            }
        }

        return { init };
    })();

    $(document).ready(function () {
        HalamanSiswaDetail.init();
    });
})(jQuery, window);
</script>
@endpush
</x-spp.layout>
