<x-spp.layout :current-page="$currentPage">
    <div class="row">
        <div class="col-md-8 widget-holder">
            <div class="widget-bg">
                <div class="widget-body pd-lr-30 clearfix">
                    <div class="row">
                        <div class="col-sm-9">
                            <h4>{{ $data['bulan'] }} &mdash; Tahun Ajaran 2021/2022</h4>
                        </div>
                        <div class="col-sm-3">
                            @switch($data['status'])
                                @case('lunas')
                                    <h6 class="text-success text-right">
                                        Lunas
                                        <i class="material-icons list-icon">check</i>
                                    </h6>
                                    @break

                                @case('konfirmasi')
                                    <h6 class="text-muted text-right">
                                        Konfirmasi
                                        <i class="material-icons list-icon">hourglass_empty</i>
                                    </h6>
                                    @break

                                @case('belum')
                                    <h6 class="text-muted text-right">
                                        Belum
                                        <i class="material-icons list-icon">schedule</i>
                                    </h6>
                                    @break

                                @default
                                    Default case...
                            @endswitch
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Jumlah</h6>
                            <p class="mr-t-0">
                                {{-- TODO: format angka monetary di JavaScript? --}}
                                Rp {{ $data['besaran_spp'] }}
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Status</h6>
                            <p>
                                {{ $data['status'] }}
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Field 3</h6>
                            <p>
                                Nilai
                            </p>
                        </div>

                        <div class="col-md-6">
                            <h6 class="text-muted text-uppercase">Field 4</h6>
                            <p>
                                Nilai
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>

        <div class="col-md-4 widget-holder">
            <div class="widget-bg">
                <div class="widget-heading">
                    <h5>
                        <i class="material-icons list-icon">info_outline</i> Informasi
                    </h5>
                </div>

                <div class="widget-body">
                    <p>
                        Konten informasi tagihan SPP bulan ini.
                    </p>
                </div>
            </div>
        </div>
    </div>
</x-spp.layout>
