<x-spp.layout current-page="Pos Pemasukan">
    <x-slot name="halamancss">
        <style>
            .list-pos {
                width: 100%;
            }

            .list-pos tr:hover td {
                background-color: rgb(247, 250, 252);
            }

            .list-pos td {
                padding: 20px;
            }

            .list-pos .list-pos-kode {
                width: 80px;
            }

            .list-pos .list-pos-action {
                width: 80px;
            }
        </style>
    </x-slot>

    <div class="row">
        <!-- Tombol Tindakan -->
        <div data-komponen="tombol-group-aksi" class="col-md-3 widget-holder widget-bg-transparent">
            <div class="btn-list">
                <a
                    class="btn btn-block btn-outline-default ripple"
                    data-komponen="tombol-create-rancangan-pemasukan"
                    data-toggle="modal"
                    data-target="#modal-create-pos-pemasukan"
                >
                    <i class="list-icon material-icons">add</i>
                    Pos Pemasukan
                </a>

                <a
                    class="btn btn-block btn-outline-default ripple"
                    data-komponen="tombol-ganti-ta"
                >
                    <i class="list-icon material-icons">event</i>
                    Ganti Tahun Ajaran
                </a>

                <span data-komponen="pilih-ta" class="form-material" style="display: none">
                    <select
                        class="form-control select2-hidden-accessible"
                        data-placeholder="Choose"
                        data-toggle="select2"
                        tabindex="-1"
                        aria-hidden="true"
                    >
                        <option disabled="disabled">Tahun Ajaran</option>
                        @foreach ($dataTahunAjaran as $tahun)
                        <option value="{{ $tahun['value'] }}" {{ $tahun['tahun_ajaran'] === $tahunAjaranDefault ? 'selected="selected"' : ''}}>{{ $tahun['tahun_ajaran'] }}</option>
                        @endforeach
                    </select>
                </span>

                <a
                    href="{{ route('master-tahun_ajaran') }}"
                    class="btn btn-block btn-outline-default ripple mr-t-10"
                    data-komponen="create-tahun-ajaran"
                >
                    <i class="list-icon material-icons">settings</i>
                    Kelola Master Tahun Ajaran
                </a>
            </div>
        </div>
        <!-- /Tombol Tindakan -->

        <!-- List view data -->
        <div
            data-komponen="view-list-pos-pemasukan"
            {{-- `id` ini dibutuhkan untuk inisiasi List.js, jangan dihapus maupun diganti ya :* --}}
            id="listjs-pos-pemasukan"
            class="col-md-8 col-sm-12 widget-holder mx-auto"
        >
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row mr-b-20 clearfix">
                        <div class="col-lg-8 col-sm-12">
                            <h5 class="box-title mr-b-0">Pos-Pos Pemasukan</h5>
                            <p class="text-muted">
                                Tahun Ajaran
                                <span data-komponen="label-tahun-ajaran">{{ $tahunAjaranDefault }}</span>
                            </p>
                        </div>
                    </div>

                    <div class="form-input-icon d-flex">
                        <i class="list-icon material-icons fs-28">search</i>
                        <input
                            type="search"
                            id="pencarian"
                            name="pencarian"
                            class="fuzzy-search form-control fs-22 fw-300 ml-3 border-0"
                            placeholder="Cari item pos pemasukan"
                        >
                    </div>

                    <hr>

                    <table
                        class="table-responsive list-pos mt-20"
                        data-komponen="tabel-pos"
                    >
                        <thead>
                            <tr>
                                <th colspan="2"></th>
                                <th class="text-right pd-r-20"><h6 class="text-muted">Target</h6></th>
                                <th class="text-right pd-r-20"><h6 class="text-muted">Realisasi</h6></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @foreach ($dataPosPemasukan as $pos)
                            <tr data-id="{{ $pos['id'] }}" data-id_pos="{{ $pos['id_pos'] }}" data-tahun_ajaran="{{ $pos['tahun_ajaran'] }}">
                                <td class="list-pos-kode text-center text-muted">
                                    {{-- TODO:
                                        <span class="kode badge bg-color-scheme-dark px-2 py-2">{{$pos['kode']}}</span>
                                    --}}
                                    <span class="badge bg-color-scheme-dark px-2 py-2">TBD...</span>
                                </td>
                                <td>
                                    <span class="nama_pos">{{$pos['nama_pos']}}</span>
                                </td>
                                <td class="text-right">
                                    <small class="mr-r-5">Rp </small><span class="target">{{$pos['target']}}</span>
                                </td>
                                <td class="text-right">
                                    <small class="mr-r-5">Rp </small><span class="realisasi">{{ $pos['realisasi'] ?? 0 }}</span>
                                </td>
                                <td style="width: 80px;">
                                    <a
                                        class="list-pos-tombol-edit btn btn-outline-default ripple"
                                        data-komponen="tombol-edit"
                                        data-toggle="modal"
                                        data-target="#modal-edit-pos-pemasukan"
                                        data-id-edit="{{$pos['id']}}"
                                    >
                                        <i class="list-icon material-icons">edit</i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <hr>

                    <div class="row p-3">
                        <div class="list-pos-count col-7 text-muted mt-1"><span></span> item</div>

                        <div class="col-5">
                            <div class="btn-group float-right">
                                <a href="javascript:void(0)" class="btn btn-sm btn-outline-default ripple disabled">
                                    <i class="list-icon material-icons">keyboard_arrow_left</i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-sm btn-outline-default ripple disabled">
                                    <i class="list-icon material-icons">keyboard_arrow_right</i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /List view data -->
    </div>

    <!-- Modal -->
    <!-- Create Pos Pemasukan -->
    <div
        id="modal-create-pos-pemasukan"
        class="modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
        style="display: none"
    >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="box-title mr-b-20">
                        <i class="list-icon material-icons text-muted">add</i> Pos Pemasukan
                        <small class="text-muted">Tambahkan item.</small>
                    </h5>

                    <form id="form-create-pos" data-komponen="form-create" class="form-material">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select
                                        name="tahun_ajaran"
                                        id="tahun-ajaran"
                                        required
                                        class="m-b-10 form-control select2-hidden-accessible"
                                        data-placeholder="Choose"
                                        data-toggle="select2"
                                        tabindex="-1"
                                        aria-hidden="true"
                                    >
                                        <option disabled="disabled" selected="selected">Tahun Ajaran</option>
                                        @foreach ($dataTahunAjaran as $tahun)
                                        <option value="{{ $tahun['tahun_ajaran'] }}">{{ $tahun['tahun_ajaran'] }}</option>
                                        @endforeach
                                    </select>
                                    <label for="tahun-ajaran">Tahun Ajaran</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select
                                        id="field-pos-create"
                                        name="id_pos"
                                        class="form-control"
                                        class="m-b-10 form-control select2-hidden-accessible"
                                        data-placeholder="Choose"
                                        data-toggle="select2"
                                        {{-- TODO: uncomment kalau select2 sudah bisa --}}
                                        {{-- tabindex="-1" --}}
                                        aria-hidden="true"
                                        required
                                    >
                                        <option selected="selected" disabled="disabled">Item pos keuangan...</option>
                                        @foreach ([['id' => 1, 'nama' => 'Bulanan'], ['id' => 2, 'nama' => 'Gedung']] as $pos)
                                        <option value="{{ $pos['id'] }}">{{ $pos['nama'] }}</option>
                                        @endforeach
                                    </select>
                                    <label for="field-pos-create">Pos</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="target" class="form-control" id="nominal-target" placeholder="Rp ..." type="number" required>
                                    <label for="nominal-target">Target Pemasukan</label>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <div class="form-actions btn-list ml-auto">
                        <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button form="form-create-pos" class="btn btn-color-scheme" type="submit">Simpan</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Edit Pos Pemasukan -->
    <div
        id="modal-edit-pos-pemasukan"
        class="modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
        style="display: none"
    >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <h5 class="box-title mr-b-20">
                        <i class="list-icon material-icons text-muted">edit</i> Pos Pemasukan
                        <small class="text-muted">Perbarui data item.</small>
                    </h5>

                    <form id="form-edit-pos" data-komponen="form-edit" class="form-material">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="target" class="form-control" id="edit-nominal-target" placeholder="Rp ..." type="number" required>
                                    <label for="edit-nominal-target">Target Pemasukan</label>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <div class="form-actions btn-list ml-auto">
                        <button class="btn btn-outline-default" type="button" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button form="form-edit-pos" class="btn btn-color-scheme" type="submit">Simpan</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <x-slot name="halamanjs">
        {{--
            Children slot akan disisipkan ke bagian bawah dokumen
            di `layout.blade.php` supaya script bisa dieksekusi terakhir
            setelah dokumen & library lain dimuat/sudah siap.
        --}}
        <script>
(function ($, global, List) {
    'use strict';

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const ListPos = (function () {

        let elTabelPos;
        let elListCount;

        let posAll;
        let instanceListJs;
        let idEdit;

        function init() {
            // setup List.js
            // Meng-index data list dengan List.js
            instanceListJs = new List('listjs-pos-pemasukan', {
                valueNames: [
                    { data: ['id', 'id_pos', 'tahun_ajaran'] },
                    'kode',
                    'nama_pos',
                    'target',
                    'realisasi',
                ],
            });

            elTabelPos = $('table[data-komponen="tabel-pos"]')
            elListCount = $('.list-pos-count > span');

            elTabelPos.on('click', '[data-komponen="tombol-edit"]', function (ev) {
                const idEdit = $(ev.target).closest('[data-komponen="tombol-edit"]').data('idEdit');
                const dataEdit = getItem(idEdit);
                ModalEdit.loadData(dataEdit);
            });

            elListCount.text(instanceListJs.size());
        }

        function getItem (idPos) {
            return instanceListJs.get('id', idPos)[0].values();
        }

        function updateTable (data) {
            instanceListJs.clear();
            instanceListJs.add(data);
            // Perlu update nilai atribut `data-id-edit` supaya tombol edit tetap normal.
            // Kalau tidak, nilainya akan diset oleh List.js tapi keliru.
            for (const pos of data) {
                elTabelPos
                    .find(`[data-id="${pos.id}"] [data-komponen="tombol-edit"]`)
                    .attr('data-id-edit', pos.id);
            }
        };

        function updateItem (data) {
            if(!data.id) {
                throw new Error('Butuh ID item untuk update data item');
            }
            instanceListJs.get('id', data.id)[0].values(data);
        };

        function addItem (data) {
            if (!data) {
                console.error('Data untuk menambahkan item harus dimasukkan.');
            }
            instanceListJs.add(data);
            instanceListJs.sort('id', { order: "desc" });
        }

        return {
            init,
            getItem,
            addItem,
            updateTable,
            updateItem,
         };
    })();

    const SelectTahunAjaran = (function () {

        let elPilih;
        let elTombol;

        let elLabelTahunAjaran;

        function init () {
            elPilih = $('[data-komponen="pilih-ta"]');
            elTombol = $('[data-komponen="tombol-ganti-ta"]');
            elLabelTahunAjaran = $('[data-komponen="label-tahun-ajaran"]');

            elTombol.hover(function () {
                elTombol.hide();
                elPilih.show();
                elPilih.find('select').prop('disabled', false).focus();
            });

            elPilih.on('change', 'select', async function (ev) {
                const elTargetSelect = $(ev.target);
                elTargetSelect.prop('disabled', true);

                try {
                    const paramTahun = elTargetSelect.val();
                    const endpoint = "{{ route('spp-pos_pemasukan_tahun', 'paramTahun') }}".replace('paramTahun', paramTahun);
                    const respon = await $.ajax(endpoint);

                    const dataUpdate = respon.body.data.map(record => {
                        return {
                            ...record,
                            target: record.target ?? 0,
                            realisasi: record.realisasi ?? 0,
                        };
                    });
                    ListPos.updateTable(dataUpdate);

                    const tahunAjaranSelected = $(ev.target).find('option:selected').text();
                    elLabelTahunAjaran.text(tahunAjaranSelected);

                    elPilih.hide();
                    elTombol.show();
                } catch (error) {
                    console.error(error.message);
                }
            });
        }

        return {
            init,
        };
    })();

    const ModalCreate = (function () {

        let el;

        function init () {
            el = $('#modal-create-pos-pemasukan');

            el.find('[data-komponen="create-tahun-ajaran"]').click(async function () {
                const respon = await $.ajax("{{ route('spp-pos_pemasukan_create') }}", {
                    method: 'POST',
                    data: dataCreate,
                });
            });

            el.find('form').submit(async function (ev) {
                ev.preventDefault();

                const dataCreate = {};
                for (const item of $(this).serializeArray()) {
                    dataCreate[item.name] = item.value;
                }

                try {
                    const respon = await $.ajax("{{ route('spp-pos_pemasukan_create') }}", {
                        method: 'POST',
                        data: dataCreate,
                    });
                    console.info('Berhasil membuat Pos Pemasukan baru.');
                    el.modal('hide');
                } catch (error) {
                    console.error(error);
                }
            });

            el.on('hidden.bs.modal', function onModalClose () {
                // reset semua value modal
                el.find('[name="tahun_ajaran"]').val('').trigger('input');
                el.find('[name="id_pos"]').val('').trigger('input');
                el.find('[name="target"]').val('').trigger('input');
                el.find('form').removeClass('was-validated');
            });
        }

        return {
            init,
        };
    })();

    const ModalEdit = (function () {

        let el;
        let dataEdit;

        function init () {
            el = $('#modal-edit-pos-pemasukan');

            el.on('hidden.bs.modal', function () {
                // reset semua value modal
                el.find('[name="target"]').val('').trigger('input');
                el.find('form').removeClass('was-validated');
            });

            el.find('form').on('submit', async function (ev) {
                ev.preventDefault();

                try {
                    const data = {
                        ...dataEdit,
                        target: el.find('[name="target"]').val(),
                    };
                    const endpointPut = "{{ route('spp-pos_pemasukan_edit', 'idEdit') }}".replace("idEdit", dataEdit.id);
                    const respon = await $.ajax(endpointPut, {
                        method: 'PUT',
                        data,
                    });
                    const valueTahun = $('[data-komponen="pilih-ta"] option:selected').val();
                    const endpointList = "{{ route('spp-pos_pemasukan_tahun', 'tahunAjaran') }}".replace('tahunAjaran', valueTahun);
                    const responList = await $.ajax(endpointList);
                    ListPos.updateTable(responList.body.data);
                    el.modal('hide');
                } catch (error) {
                    console.error('error update?', error.message);
                }
            });
        }

        async function loadData(data) {
            dataEdit = data;
            el.find('[name="target"]').val(data.target).trigger('input');
        }

        return {
            init,
            loadData,
        }
    })();

    $(document).ready(function() {
        ListPos.init();
        SelectTahunAjaran.init();
        ModalCreate.init();
        ModalEdit.init();
    });
})(jQuery, window, List);
        </script>
    </x-slot>
</x-spp.layout>
