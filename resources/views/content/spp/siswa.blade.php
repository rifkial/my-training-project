<x-spp.layout current-page="siswa">
@push('gaya_custom')
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
/* ini gaya custom */
</style>
@endpush

    <div class="row">
        <div class="col-5 bg-white">
            <div class="container">
                    <section class="d-flex justify-content-between my-3">
                        <h5 class="box-title">Data Seluruh Siswa</h5>
                        <span>
                            <a class="btn btn-success btn-sm" href="{{ route('spp-siswa_create') }}" >+ Tambah Siswa</a>
                            <!-- <a class="btn btn-info btn-sm" href="" >Export</a> -->
                        </span>
                    </section>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">NIS</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allSiswa as $siswa)
                        
                        <tr>
                            <th scope="row">{{$siswa["nis"]}}</th>
                            <td>{{$siswa["nama"]}}</td>
                            <td>
                                <span>
                                <a href="{{ route('spp-siswa_detail', $siswa["id"] )}}">edit</a>
                                <a class="text-primary" href="{{ route('spp-siswa_detail', $siswa["id"] )}}">detail</a>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>

            </div>
        </div>
        <div class="col-md-6 mx-auto widget-holder padded-reversed">
            <div class="widget-bg">
                <div class="widget-body">
                    <section class="d-flex justify-content-between">
                        <h5 class="box-title">Data SPP Siswa</h5>
                    </section>
                    <p>Pilih siswa untuk melihat informasi terkait SPP dan tagihan siswa.</p>

                    <form id="form-search-siswa">
                        <div class="row mr-t-50 clearfix">
                            <div class="col-md-8">
                                <div class="form-input-icon d-flex">
                                    <i class="list-icon material-icons fs-28">search</i>
                                    <input
                                        type="search"
                                        id="pencarian"
                                        name="pencarian"
                                        class="form-control fs-22 fw-300 border-0"
                                        placeholder="Cari siswa..."
                                    >
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <select id="jenis" name="jenis" class="form-control">
                                        <option value="nama" selected="selected" >Nama</option>
                                        <option value="nis">NIS</option>
                                        <option value="id_kelas">Kelas</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <button type="submit" class="btn btn-outline-default btn-block">
                                    <i class="list-icon material-icons">search</i>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="tabel-siswa" class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th class="pd-l-20" style="width: 240px">NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th class="text-center" style="width: 80px">
                                            <i class="material-icons text-muted">visibility</i>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>

@push('js_custom')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
(function($, global, undefined) {
    "use-strict"

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'}});

    const HalamanSiswa = (function () {

        let $DataTable;

        function init () {
            datatableSiswa();
            searchSiswa();
        }

        function datatableSiswa() {
            $DataTable = $('#tabel-siswa').DataTable({
                dom: `<"row"<"col-sm-12"<"form-input-icon d-flex"f>>>trip`,
                processing: true,
                serverSide: true,
                searching: false,
                ordering:  false,
                ajax: "{{ route('spp-siswa') }}",
                columns: [
                    { data: 'nis', name: 'nis' },
                    { data: 'nama', name: 'nama' },
                    { data: 'kelas', name: 'kelas' },
                    {
                        data: 'id',
                        name: 'id',
                        className: "text-center",
                        render: function (data) {
                            const route = "{{ route('spp-siswa_detail', 'idSiswa')}}".replace("idSiswa", data);
                            return `
                                <td style="width: 80px">
                                    <a class="text-muted" href="${route}"><i class="material-icons">exit_to_app</i></a>
                                </td>
                            `;
                        },
                    },
                ],
                language: {
                    search: '',
                    searchPlaceholder: "Cari siswa...",
                },
            });

            $('[data-komponen="ikon-search"]').prependTo('.form-input-icon label');
        }

        function searchSiswa() {
            const $form = $('#form-search-siswa');
            const $inputSearch = $form.find('[name="pencarian"]');
            const $selectJenisFilter = $form.find('[name="jenis"]');

            $form.submit(function (ev) {
                ev.preventDefault();
                if ( !($selectJenisFilter.val()) ) {
                    return $selectJenisFilter.focus();
                }
                if ( !($inputSearch.val()) ) {
                    $inputSearch.focus();
                    // Reset data tabel ke data semula kalau input searchnya kosong
                    $DataTable.ajax.url("{{ route('spp-siswa') }}").load();
                    return;
                }
                const endpointSearch = "{{ route('spp-siswa_search', ['jenis' => 'filterJenis', 'katakunci' => 'kataKunci']) }}"
                    .replace("&amp;", "&")
                    .replace("filterJenis", $selectJenisFilter.val())
                    .replace("kataKunci", $inputSearch.val());
                $DataTable.ajax.url(endpointSearch).load();
            });
        }

        return {
            init,
        }
    })();

    $(document).ready(function () {
        HalamanSiswa.init();
    });
})(jQuery, window);
</script>
@endpush
</x-spp.layout>
