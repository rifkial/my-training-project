<x-spp.layout :current-page="$currentPage">
    <div class="row">
        <div class="col-md-12 widget-holder padded-reversed">
            <div class="widget-bg">
                <div class="widget-heading">
                    <h5>Riwayat Tagihan SPP</h5>
                </div>

                <div class="widget-body">
                    <div class="padded-reverse">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="pd-l-20">SPP Bulan</th>
                                    <th class="text-right">Jumlah</th>
                                    <th class="pd-r-20">Status</th>
                                    <th class="text-center" style="width: 80px">
                                        <i class="material-icons text-muted">visibility</i>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            {{-- TODO: format angka monetary pakai JS --}}
                            @foreach ($dataListTagihan as $tagihan)
                                <tr>
                                    <td class="pd-l-20">{{ $tagihan['bulan'] }}</td>
                                    <td class="text-right">{{ $tagihan['besaran_spp'] }}</td>
                                    <td class="pd-r-20">{{ $tagihan['status'] }}</td>
                                    <td class="text-center" style="width: 80px">
                                        <a class="text-muted" href="{{route('spp-tagihan_detail', $tagihan['id'])}}"><i class="material-icons">exit_to_app</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>
</x-spp.layout>
