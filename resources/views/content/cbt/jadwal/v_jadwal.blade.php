@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        <a href="#" type="button" class="btn btn-sm btn-white text-dark">
                                            <i class="fa fa-sync"></i> <span
                                                class="d-none d-sm-inline-block ml-1">Reload</span>
                                        </a>
                                        <a href="{{ route('cbt-jadwal_create') }}" type="button"
                                            class="btn btn-info btn-sm ml-1">
                                            <i class="fas fa-plus-circle"></i> Tambah Jadwal
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            Kode Background Jadwal:
                            <table class="w-100">
                                <tbody>
                                    <tr>
                                        <td class="p-1" style="width: 20px"><i
                                                class="fas fa-square text-muted"></i></td>
                                        <td class="p-1">Tidak aktif</td>
                                        <td class="p-1" style="width: 20px"><i
                                                class="fas fa-square text-indigo"></i></td>
                                        <td class="p-1">Sedang dilaksanakan</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1"><i class="fas fa-square text-yellow"></i></td>
                                        <td class="p-1">Tidak digunakan</td>
                                        <td class="p-1"><i class="fas fa-square text-fuchsia"></i></td>
                                        <td class="p-1">Selesai, belum rekap nilai</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1"><i class="fas fa-square text-maroon"></i></td>
                                        <td class="p-1">Belum dimulai</td>
                                        <td class="p-1"><i class="fas fa-square text-success"></i></td>
                                        <td class="p-1">Selesai, sudah rekap nilai</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="alert alert-danger my-2" id="bulk-delete">
                        <form action="http://localhost/cbt/cbtjadwal" id="hapus_semua" method="post" accept-charset="utf-8">
                            <input type="hidden" name="csrf_token" value="4399c81fbae52e327c1802c746d6935d">
                            <input style="width: 28px; height: 28px" class="check-all m-1" id="check-all" type="checkbox">
                            <label for="check-all" class="align-middle">Pilih Semua</label>
                            <button id="submit-hapus" type="submit" class="btn btn-danger mb-3 ml-4 d-none">
                                <i class="far fa-trash-alt"></i> Hapus Jadwal Terpilih
                            </button>
                        </form>
                    </div>

                    <div id="konten-jadwal">
                        @foreach ($jenis as $jns)
                            @if (!empty($jns['jadwal']))
                                <div class="card card-default my-shadow mb-4">
                                    <div class="card-header bg-purple">
                                        <h5 class="box-title m-0 text-white"><b>{{ $jns['kode'] }}</b> 2021/2022 smt I
                                            (satu)</h5>
                                    </div>
                                    <div class="card-body" style="display: block;">
                                        <div><b>Kelas: 10</b></div>
                                        <div class="row mt-2">
                                            @foreach ($jns['jadwal'] as $jdwl)
                                                <div class="col-lg-3 col-md-4 col-sm-6 col-12">

                                                    <div class="card bg-purple">
                                                        <h5 class="box-title text-center text-white">Fikih - Ushul Fikih
                                                        </h5>
                                                        <p class="my-0 mx-1 text-white">Jumlah Soal 7</p>
                                                        <p class="my-0 mx-1 text-white">Kelas <span class="pull-right">7
                                                                MOB</span>
                                                        </p>
                                                        <ul class="list-group list-group-unbordered m-1">
                                                            <li class="list-group-item p-1"> Jenis
                                                                <span class="float-right"><b>PAT</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Kode Soal
                                                                <span
                                                                    class="float-right"><b>{{ $jdwl['kode_bank'] }}</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Mulai
                                                                <span class="float-right">
                                                                    <b>{{ (new \App\Helpers\Help())->getDay($jdwl['tgl_mulai']) }}</b>
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Sampai
                                                                <span
                                                                    class="float-right"><b>{{ (new \App\Helpers\Help())->getDay($jdwl['tgl_selesai']) }}</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Durasi
                                                                <span class="float-right"><b>{{ $jdwl['durasi_ujian'] }}
                                                                        Menit</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Acak Soal
                                                                <span class="float-right"><b>Ya</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Acak Jawaban
                                                                <span class="float-right"><b>Ya</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Hasil Tampil
                                                                <span class="float-right"><b>Tidak</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Gunakan Token
                                                                <span class="float-right"><b>Tidak</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Reset Login
                                                                <span class="float-right"><b>Tidak</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Status
                                                                <span class="float-right"><b>Non Aktif</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Sudah Rekap
                                                                <span class="float-right"><b>Belum</b></span>
                                                            </li>
                                                            <li class="list-group-item p-1"> Mengerjakan
                                                                <span class="float-right"><b>0</b></span>
                                                            </li>
                                                        </ul>
                                                        <div class="card-footer d-flex justify-content-between">
                                                            <input name="checked[]" value="1"
                                                                class="check-jadwal float-left" type="checkbox"
                                                                style="width: 28px;height: 28px">
                                                            <div class="aksi">
                                                                <a href="{{ route('cbt-jadwal_edit', ['k' => (new \App\Helpers\Help())->encode($jdwl['id'])]) }}"
                                                                    class="btn btn-info btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <button class="btn btn-danger btn-sm delete"
                                                                    data-id="{{ $jdwl['id'] }}"><i
                                                                        class="fas fa-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        {{-- <div class="card card-default my-shadow mb-4">
                            <div class="card-header bg-purple">
                                <h5 class="box-title m-0 text-white"><b>PAT</b> 2021/2022 smt I (satu)</h5>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div><b>Kelas: 10</b></div>
                                <div class="row mt-2">
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">

                                        <div class="card bg-purple">
                                            <h5 class="box-title text-center text-white">Fikih - Ushul Fikih</h5>
                                            <p class="my-0 mx-1 text-white">Jumlah Soal 7</p>
                                            <p class="my-0 mx-1 text-white">Kelas <span class="pull-right">7 MOB</span>
                                            </p>
                                            <ul class="list-group list-group-unbordered m-1">
                                                <li class="list-group-item p-1"> Jenis
                                                    <span class="float-right"><b>PAT</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Kode Soal
                                                    <span class="float-right"><b>Soal Pertama</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Mulai
                                                    <span class="float-right">
                                                        <b>Rabu 05 Januari 2022</b>
                                                    </span>
                                                </li>
                                                <li class="list-group-item p-1"> Sampai
                                                    <span class="float-right"><b>Rabu 05 Januari 2022</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Durasi
                                                    <span class="float-right"><b>30 Menit</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Acak Soal
                                                    <span class="float-right"><b>Ya</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Acak Jawaban
                                                    <span class="float-right"><b>Ya</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Hasil Tampil
                                                    <span class="float-right"><b>Tidak</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Gunakan Token
                                                    <span class="float-right"><b>Tidak</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Reset Login
                                                    <span class="float-right"><b>Tidak</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Status
                                                    <span class="float-right"><b>Non Aktif</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Sudah Rekap
                                                    <span class="float-right"><b>Belum</b></span>
                                                </li>
                                                <li class="list-group-item p-1"> Mengerjakan
                                                    <span class="float-right"><b>0</b></span>
                                                </li>
                                            </ul>
                                            <div class="card-footer d-flex justify-content-between">
                                                <input name="checked[]" value="1" class="check-jadwal float-left"
                                                    type="checkbox" style="width: 28px;height: 28px">
                                                <div class="aksi">
                                                    <a href="#" class="btn btn-info btn-sm"><i
                                                            class="fas fa-pencil-alt"></i></a>
                                                    <a href="#" class="btn btn-danger btn-sm"><i
                                                            class="fas fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>

            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "200",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');
                    input.onchange = function() {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                }
            });


        })
    </script>
@endsection
