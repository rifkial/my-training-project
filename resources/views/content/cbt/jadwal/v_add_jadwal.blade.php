@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <form action="javascript:void(0)" id="formJadwal">
                        <input type="text" name="action" id="action" value="{{ $action }}">
                        <input type="hidden" name="id" value="{{ $action == 'edit' ? $jadwal['id'] : '' }}">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pull-right">
                                            <a href="{{ route('cbt-jadwal') }}" class="btn btn-sm btn-danger">
                                                <i class="fas fa-arrow-alt-circle-left"></i> <span
                                                    class="d-none d-sm-inline-block ml-1">Kembali</span>
                                            </a>
                                            <button type="submit" class="btn btn-info btn-sm ml-1" id="btnJadwal">
                                                <i class="fas fa-plus-circle"></i> Tambah Jadwal
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <label>Guru</label>
                                        <select name="id_guru" id="id_guru" class="form-control" required="">
                                            <option value="0">Pilih Guru :</option>
                                            @foreach ($guru as $gr)
                                                <option value="{{ $gr['id'] }}"
                                                    {{ $action == 'edit' && $gr['id'] == $jadwal['id_guru'] ? 'selected' : '' }}>
                                                    {{ $gr['nama'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label>Bank Soal</label>
                                        <select name="bank_id" id="bank_id" class="form-control" required="">
                                            <option value="0">Pilih Bank Soal :</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label>Jenis</label>
                                        <select name="jenis_id" id="jenis-id" class="form-control" required="">
                                            <option value="" selected="selected">Jenis Penilaian :</option>
                                            @foreach ($jenis as $jn)
                                                <option value="{{ $jn['id'] }}"
                                                    {{ $action == 'edit' && $jn['id'] == $jadwal['id_jenis'] ? 'selected' : '' }}>
                                                    {{ $jn['kode'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label>Pengawas</label>
                                        <select name="pengawas[]" id="pengawas-ujian" class="select2 form-control"
                                            multiple="" data-placeholder="Pilih Pengawas" required="">
                                            <option value="0" disabled>Pilih Guru :</option>
                                            @if ($action == 'edit')
                                                @php
                                                    $pengawas = $jadwal['pengawas_guru'];
                                                    function checkfacility($id_guru, $pengawas)
                                                    {
                                                        foreach ($pengawas as $pg) {
                                                            if ($id_guru == $pg['id']) {
                                                                return true;
                                                            }
                                                        }
                                                    }
                                                @endphp
                                            @endif
                                            @foreach ($guru as $values)
                                                <option value="{{ $values['id'] }}"
                                                    {{ $action == 'edit' && checkfacility($values['id'], $pengawas) == true ? 'selected' : '' }}>
                                                    {{ $values['nama'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label>Tanggal Mulai</label>
                                        <input type="text" id="tgl-mulai" name="tgl_mulai"
                                            value="{{ date('d-m-Y', strtotime($jadwal['tgl_mulai'])) }}"
                                            class="datepicker form-control" autocomplete="off" required="true">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label>Tanggal Expired</label>
                                        <input type="text" id="tgl-selesai" name="tgl_selesai"
                                            value="{{ date('d-m-Y', strtotime($jadwal['tgl_selesai'])) }}"
                                            class="datepicker form-control" autocomplete="off" required="true">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <div class="form-group">
                                            <label>Durasi (menit)</label>
                                            <input type="number" id="durasi-ujian" name="durasi_ujian"
                                                class="form-control" value="{{ $jadwal['durasi_ujian'] }}"
                                                required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-soal" name="acak_soal" value="1"
                                                            {{ $jadwal['acak_soal'] == 1 ? 'checked' : '' }}>
                                                        <label for="check-soal">Acak Soal</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-opsi" name="acak_opsi" value="1">
                                                        <label for="check-opsi">Acak Jawaban</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-token" name="token" value="1"
                                                            {{ $jadwal['token'] == 1 ? 'checked' : '' }}>
                                                        <label for="check-token">Gunakan Token</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-hasil" name="hasil_tampil"
                                                            value="1">
                                                        <label for="check-hasil">Tampilkan Hasil</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-login" name="reset_login" value="1"
                                                            {{ $jadwal['reset_login'] == 1 ? 'checked' : '' }}>
                                                        <label for="check-login">Reset Login</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="icheck-cyan">
                                                        <input type="checkbox" id="check-status" name="status" value="1">
                                                        <label for="check-status">Aktif</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formJadwal').on('submit', function(event) {
                event.preventDefault();
                $("#btnJadwal").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Menambah');
                $("#btnJadwal").attr("disabled", true);
                // var action_url = '';

                // if ($('#action').val() == 'Add') {
                //     action_url = "{{ route('alumni-fakultas_create') }}";
                //     method_url = "POST";
                // }

                // if ($('#action').val() == 'Edit') {
                //     action_url = "{{ route('alumni-fakultas_update') }}";
                //     method_url = "PUT";
                // }
                $.ajax({
                    url: "{{ route('cbt-jadwal_store') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            window.location.href = "{{ route('cbt-jadwal') }}";
                        } else {
                            $('#btnJadwal').html(
                                '<i class="fas fa-plus-circle"></i> Tambah Jadwal');
                            $("#btnJadwal").attr("disabled", false);
                        }
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#btnJadwal').html(
                            '<i class="fas fa-plus-circle"></i> Tambah Jadwal');
                        $("#btnJadwal").attr("disabled", false);
                    }
                });
            });

            $('#id_guru').on('change', function() {
                var id = $(this).val();
                if (id) {
                    $.ajax({
                        url: "{{ route('cbt-bank_get_by_guru') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        success: function(data) {
                            // console.log(data);
                            if (!$.trim(data)) {
                                $('#bank_id').html(
                                    '<option value="">Belum ada Bank soal</option>');
                            } else {
                                var s = '<option value="">Pilih Bank Soal :</option>';
                                data.forEach(function(row) {
                                    s += '<option value="' + row.id + '">' + row
                                        .kode +
                                        '</option>';

                                })
                                $('#bank_id').removeAttr('disabled');
                            }


                            $('#bank_id').html(s)
                        }
                    });
                }
            })
        })
    </script>
@endsection
