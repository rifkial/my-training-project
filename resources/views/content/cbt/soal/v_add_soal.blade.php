@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.0/tinymce.min.js"
        integrity="sha512-XNYSOn0laKYg55QGFv1r3sIlQWCAyNKjCa+XXF5uliZH+8ohn327Ewr2bpEnssV9Zw3pB3pmVvPQNrnCTRZtCg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
    <main class="clearfix">
        <div class="widget-list">
            <form id="formSoal" action="javascript:void(0)">
                <div class="row">
                    <div class="col-md-12 my-3">
                        <div class="card my-shadow">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="box-title mr-b-0"><b>Bank Soal:</b> {{ $bank['kode'] }} | PG: 2, Essai:
                                            1
                                        </h5>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="" class="btn btn-danger pull-right"><i
                                                class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-white">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link {{ $_GET['tab'] == 1 ? 'active' : '' }}"
                                            href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($bank['id']), 'tab' => 1]) }}">Pilihan
                                            Ganda</a></li>

                                    <li class="nav-item"><a class="nav-link {{ $_GET['tab'] == 2 ? 'active' : '' }}"
                                            href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($bank['id']), 'tab' => 2]) }}">Pilihan
                                            Ganda Kompleks</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link {{ $_GET['tab'] == 3 ? 'active' : '' }}"
                                            href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($bank['id']), 'tab' => 3]) }}">Menjodohkan</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link {{ $_GET['tab'] == 4 ? 'active' : '' }}"
                                            href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($bank['id']), 'tab' => 4]) }}">Isian
                                            Singkat</a></li>
                                    <li class="nav-item"><a class="nav-link {{ $_GET['tab'] == 5 ? 'active' : '' }}"
                                            href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($bank['id']), 'tab' => 5]) }}">Essai/Uraian</a>
                                    </li>
                                </ul>
                                <hr>
                            </div>
                            <div class="card-body">
                                @php
                                    if ($_GET['tab'] == 1) {
                                        $nama = 'Pilihan Ganda';
                                        $jumlah = $bank['jml_pilgan'];
                                    } elseif ($_GET['tab'] == 2) {
                                        $nama = 'Pilihan Ganda Kompleks';
                                        $jumlah = $bank['jml_kompleks'];
                                    } elseif ($_GET['tab'] == 3) {
                                        $nama = 'Soal Menjodohkan';
                                        $jumlah = $bank['jml_jodohkan'];
                                    } elseif ($_GET['tab'] == 4) {
                                        $nama = 'Soal Isian Singkat';
                                        $jumlah = $bank['jml_isian'];
                                    } else {
                                        $nama = 'Soal Essay/Uraian';
                                        $jumlah = $bank['jml_essay'];
                                    }
                                @endphp
                                <div class="tab-content pt-0">
                                    <div class="tab-pane  active" id="ganda">
                                        <div class="alert alert-success align-content-center" role="alert">
                                            {{ $nama }}, jumlah soal seharusnya: <b>{{ $jumlah }}</b>
                                        </div>
                                        <span><b>{{ strtoupper($nama) }} NOMOR: </b></span><br>
                                        @php
                                            $nomer = 1;
                                        @endphp
                                        @foreach ($soal as $so)
                                            <a href="javascript:void(0)" class="btn btn-outline-danger"
                                                onclick="getSoalById({{ $so['id_bank'] }}, {{ $so['nomor_soal'] }}, {{ $so['id'] }}, {{ $_GET['tab'] }})"
                                                id="btn-11">{{ $nomer++ }}</a>
                                        @endforeach
                                        <a href="javascript:void(0)" class="btn btn-outline-success addSoal"
                                            data-bank="{{ $_GET['bank'] }}" data-nomor="{{ $nomer }}"
                                            data-tipe={{ $_GET['tab'] }} id="btn-add-new-pg"><i
                                                class="fa fa-plus"></i>
                                            Tambah Soal</a>
                                        <input type="hidden" id="jenis_id" name="jenis" value="{{ $_GET['tab'] }}"
                                            class="form-control">
                                        <input type="hidden" id="bank_id" name="bank_id" value="{{ $_GET['bank'] }}"
                                            class="form-control">
                                        <input type="hidden" id="nomor_soal" name="nomor_soal" value=""
                                            class="form-control">
                                        <input type="hidden" id="soal_id" name="id_soal" value="" class="form-control">
                                        <input type="hidden" id="method" name="method" value="edit" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="tampilan_soal">
                        <div class="card my-shadow my-3">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="box-title mr-b-0">{{ $nama }}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="soal-area mt-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning align-content-center" role="alert">
                                                Harap pilih / tambah soal terlebih dahulu
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </main>

    <script>
        let i = 2;
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            editor();
            $(document).on('click', '.addSoal', function() {
                let bank_id = $(this).data('bank');
                let nomor_soal = $(this).data('nomor');
                let jenis = $(this).data('tipe');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt-soal_create') }}",
                    data: {
                        bank_id,
                        nomor_soal,
                        jenis
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Menambah Soal..');
                    },
                    success: function(data) {

                        if (data.status == 'berhasil') {
                            window.location.reload();
                        } else {
                            $(loader).html('<i class="fa fa-plus"></i> Tambah Soal');
                            noti(data.icon, data.message);
                        }
                    }
                });
            });

            $('#formSoal').on('submit', function(event) {
                event.preventDefault();
                $(".simpanSoal").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Menyimpan..');
                $(".simpanSoal").attr("disabled", true);
                tinyMCE.triggerSave();
                $.ajax({
                    url: "{{ route('cbt-soal_create') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $(".simpanSoal").html(
                            '<i class="fa fa-plus mr-1"></i>Simpan');
                        $(".simpanSoal").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $(".simpanSoal").html(
                            '<i class="fa fa-plus mr-1"></i>Simpan');
                        $(".simpanSoal").attr("disabled", false);
                    }
                });
            });

        })

        function getSoalById(id_bank, nomor_soal, id, tipe) {
            $.ajax({
                type: 'POST',
                url: "{{ route('cbt-load_soal') }}",
                data: {
                    id_bank,
                    nomor_soal,
                    tipe,
                    id
                },
                // beforeSend: function() {
                //     $(loader).html(
                //         '<i class="fa fa-spin fa-spinner"></i> Menambah Soal..');
                // },
                success: function(data) {
                    console.log(data);
                    $('#tampilan_soal').html(data);
                    editor();
                    $('#soal_id').val(id);
                    $('#nomor_soal').val(nomor_soal);
                    i = $('#jumlah_pilgan').val();
                }
            });
        }



        function tambahData() {
            i++;
            // console.log(alphabet(i));
            let pilgan = String.fromCharCode(i + 64);
            $('.addKompleks').append(
                '<div class="row mb-3"  id="row' + i +
                '"><div class="col-md-6"><label>Jawaban ' + pilgan +
                '</label></div><div class="col-6 text-right d-flex justify-content-end"> <b>Jawaban benar</b> <input class="check-pg2" type="checkbox" style="width: 24px; height: 24px; margin-left: 8px;"name="jawaban_benar_pg2[]" value="' +
                pilgan.toLowerCase() + '"></div><div class="col-md-12"><textarea name="jawaban2_' + pilgan
                .toLowerCase() + '" id="" cols="30" rows="10" class="editor"></textarea></div></div>'
            );
            editor();
            $('#jumlah_pilgan').val(i);
        }

        function alphabet(number) {
            String.fromCharCode(number + 64);
        }

        // function duplicate() {
        //     var original = document.getElementById('service');
        //     var rows = original.parentNode.rows;
        //     var i = rows.length - 1;
        //     var clone = original.cloneNode(true); // "deep" clone
        //     clone.id = "duplic" + (i); // there can only be one element with an ID
        //     original.parentNode.insertBefore(clone, rows[i]);
        // }

        function editor() {
            tinymce.init({
                selector: "textarea.editor",
                branding: false,
                width: "100%",
                height: "200",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table  paste  wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter  alignright alignjustify | bullist numlist outdent indent | link image",
                image_title: true,
                file_picker_types: 'image',
                file_picker_callback: function(cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');
                    input.onchange = function() {

                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function() {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), {
                                title: file.name
                            });
                        };
                        reader.readAsDataURL(file);
                        tinymce.triggerSave();
                    };

                    input.click();
                }
            });
        }
    </script>
@endsection
