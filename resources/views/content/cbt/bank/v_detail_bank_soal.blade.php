@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .tampilan_soal p,
        .opsi_ganda p {
            margin-bottom: 0px;
        }

    </style>
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol pull-right">
                                        <a href="javascript:void(0)" class="btn btn-danger"><i
                                                class="fas fa-arrow-circle-left"></i> Kembali</a>
                                        <a href="javascript:void(0)" class="btn btn-purple"><i class="fas fa-sync-alt"></i>
                                            Reload</a>
                                        <a href="javascript:void(0)" class="btn btn-info"><i class="fas fa-download"></i>
                                            Download</a>
                                        <a href="{{ route('cbt-soal_new', ['bank' => (new \App\Helpers\Help())->encode($soal['id']), 'tab' => 1]) }}"
                                            class="btn btn-success"><i class="fas fa-plus-circle"></i> Tambah/Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item p-1"> Kode Bank Soal
                                            <span class="float-right"><b>{{ $soal['kode'] }}</b></span>
                                        </li>
                                        <li class="list-group-item p-1"> Mata Pelajaran
                                            <span class="float-right"><b>{{ $soal['mapel'] }}</b></span>
                                        </li>
                                        <li class="list-group-item p-1"> Guru
                                            <span class="float-right">
                                                @php
                                                    $guru = [];
                                                    foreach ($soal['gurus'] as $gr) {
                                                        $guru[] = $gr['nama'];
                                                    }
                                                @endphp
                                                <b>{{ implode('& ', $guru) }}</b>
                                            </span>
                                        </li>
                                        <li class="list-group-item p-1"> Rombel
                                            @php
                                                $rombel = [];
                                                foreach ($soal['rombels'] as $rm) {
                                                    $rombel[] = $rm['nama'];
                                                }
                                            @endphp
                                            <span class="float-right"><b>{{ implode(', ', $rombel) }}</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item p-1"> Total Seharusnya
                                            <span class="float-right"><b>7</b></span>
                                        </li>
                                        <li class="list-group-item p-1"> Total Soal dibuat
                                            <span class="float-right"><b>7</b></span>
                                        </li>
                                        <li class="list-group-item p-1"> Total ditampilkan
                                            <span class="float-right"><b>2</b></span>
                                        </li>
                                        <li class="list-group-item p-1"> Keterangan
                                            <span class="float-right"><b>Belum Selesai</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 mt-2">
                                    <div class="card">
                                        <div class="card-body text-center p-2 bg-danger">
                                            <b>Pembuatan Soal</b>
                                            <br>
                                            <span class="text-lg"><b>Belum Selesai</b></span>
                                            <br>
                                            <span>soal yang ditampilkan masih kurang</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link btn-purple mx-1 active" href="#pg"
                                        data-toggle="tab">Pilihan Ganda <i class="fas fa-check-circle"></i></a>
                                </li>
                                <li class="nav-item"><a class="nav-link btn-purple mx-1" href="#kompleks"
                                        data-toggle="tab">Pilihan Ganda Kompleks <i class="fas fa-info-circle"></i></a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#jodoh"
                                        data-toggle="tab">Menjodohkan <i class="fas fa-info-circle"></i></a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#isian"
                                        data-toggle="tab">Isian Singkat <i class="fas fa-info-circle"></i></a></li>
                                <li class="nav-item"><a class="mx-1 nav-link btn-purple" href="#essay"
                                        data-toggle="tab">Essai/Uraian <i class="fas fa-info-circle"></i></a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="pg">
                                    <table class="table table-bordered table-sm">
                                        <tbody>
                                            <tr class="alert alert-dark text-center align-middle">
                                                <th class="border-dark text-center">Jenis Soal</th>
                                                <th class="border-dark text-center" colspan="2">Jumlah Soal</th>
                                                <th class="border-dark text-center">Bobot Nilai</th>
                                                <th class="border-dark text-center">Point Per-nomor</th>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td rowspan="3" class="text-center align-middle border-dark">Pilihan Ganda
                                                </td>
                                                <td class="border-dark">Seharusnya</td>
                                                <td class="text-center border-dark">{{ $soal['jml_pilgan'] }}</td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_pilgan'] }} </td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_pilgan'] / $soal['jml_pilgan'] }} </td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Telah dibuat</td>
                                                <td class="text-center border-dark">{{ count($soal['soal_pilgan']) }}</td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Ditampilkan</td>
                                                <td class="text-center border-dark">
                                                    {{ $soal['pilgan_aktif'] == null ? 0 : $soal['pilgan_aktif'] }}</td>
                                            </tr>
                                            <tr class="alert alert-danger">
                                                <td colspan="5" class="border-dark">
                                                    Info :
                                                    <br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <form action="javascript:void(0)" id="formPG">
                                        <div class="d-sm-flex justify-content-between">
                                            <div>
                                                <input style="width: 24px; height: 24px" class="check-pg-all my-1"
                                                    id="check_all-pg" type="checkbox">
                                                <label for="all-pg" class="align-middle">Pilih Semua PG</label>
                                            </div>
                                            <div>
                                                <span><b>Jumlah PG terpilih: </b></span>
                                                <span class="text-lg"><b id="total-pg">0</b></span>

                                                <button type="submit" class="btn btn-sm btn-purple ml-3" id="save-pg">
                                                    <i class="fa fa-save"></i> <span
                                                        class="d-none d-md-inline-block ml-1">Simpan PG Terpilih</span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            @if (!empty($soal['soal_pilgan']))
                                                @php
                                                    $no_pilgan = 1;
                                                @endphp
                                                <input type="hidden" name="total_soal"
                                                    value="{{ count($soal['soal_pilgan']) }}">
                                                @foreach ($soal['soal_pilgan'] as $pilgan)
                                                    <table class="w-100">
                                                        <tr>
                                                            <td width="50">
                                                                <input type="hidden" name="id_soal_{{ $no_pilgan }}"
                                                                    value="{{ $pilgan['id'] }}">
                                                                <input style="width: 24px; height: 24px"
                                                                    class="check-pg mt-2" value="1" type="checkbox"
                                                                    name="status_soal_{{ $no_pilgan }}"
                                                                    id="pg_aktif_{{ $no_pilgan }}" onclick="check_pg()"
                                                                    {{ $pilgan['status_tampil'] == 1 ? 'checked' : '' }}>
                                                            </td>
                                                            <td width="50">{{ $no_pilgan++ }}.</td>
                                                            <td class="tampilan_soal">{!! $pilgan['soal'] !!}</td>
                                                        </tr>
                                                        @for ($i = 0; $i < $soal['opsi']; $i++)
                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>
                                                                    <table class="w-100">
                                                                        <tr>
                                                                            <td width="20">
                                                                                {{ range('A', 'Z')[$i] }}.</td>
                                                                            <td class="opsi_ganda">
                                                                                {!! $pilgan['opsi_' . range('a', 'z')[$i]] !!}</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>Jawaban. <b>{!! ucwords($pilgan['jawaban']) !!}</b></td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                @endforeach
                                            @else
                                                <div class="alert alert-danger align-content-center" role="alert">
                                                    Belum ada soal pilihan ganda yang terinput
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="kompleks">
                                    <table class="table table-bordered table-sm">
                                        <tbody>
                                            <tr class="alert alert-dark text-center align-middle">
                                                <th class="border-dark text-center">Jenis Soal</th>
                                                <th class="border-dark text-center" colspan="2">Jumlah Soal</th>
                                                <th class="border-dark text-center">Bobot Nilai</th>
                                                <th class="border-dark text-center">Point Per-nomor</th>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td rowspan="3" class="text-center align-middle border-dark">Pil. Ganda
                                                    Kompleks
                                                </td>
                                                <td class="border-dark">Seharusnya</td>
                                                <td class="text-center border-dark">{{ $soal['jml_kompleks'] }}</td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_kompleks'] }} </td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_kompleks'] / $soal['jml_kompleks'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Telah dibuat</td>
                                                <td class="text-center border-dark">
                                                    {{ count($soal['soal_pilgan_kompleks']) }}</td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Ditampilkan</td>
                                                <td class="text-center border-dark">
                                                    {{ $soal['kompleks_aktif'] == null ? 0 : $soal['kompleks_aktif'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-danger">
                                                <td colspan="5" class="border-dark">
                                                    Info :
                                                    <br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <form action="javascript:void(0)" id="formKompleks">
                                        <div class="d-sm-flex justify-content-between">
                                            <div>
                                                <input style="width: 24px; height: 24px" class="check-pg-all my-1"
                                                    id="check_all-komplek" type="checkbox">
                                                <label for="all-pg" class="align-middle">Pilih Semua</label>
                                            </div>
                                            <div>
                                                <span><b>Jumlah terpilih: </b></span>
                                                <span class="text-lg"><b id="total-komplek">0</b></span>

                                                <button type="submit" class="btn btn-sm btn-purple ml-3" id="save-komplek">
                                                    <i class="fa fa-save"></i> <span
                                                        class="d-none d-md-inline-block ml-1">Simpan PG Komplek
                                                        Terpilih</span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <input type="hidden" name="total_soal"
                                                value="{{ count($soal['soal_pilgan_kompleks']) }}">
                                            @if (!empty($soal['soal_pilgan_kompleks']))
                                                @php
                                                    $no_komplek = 1;
                                                @endphp
                                                @foreach ($soal['soal_pilgan_kompleks'] as $komplek)
                                                    <table class="w-100">
                                                        <tr>
                                                            <td width="50">
                                                                <input type="hidden" name="id_soal_{{ $no_komplek }}"
                                                                    value="{{ $komplek['id'] }}">
                                                                <input style="width: 24px; height: 24px"
                                                                    class="check-komplek mt-2" value="1" type="checkbox"
                                                                    name="status_soal_{{ $no_komplek }}"
                                                                    onclick="check_komplek()"
                                                                    value="{{ $komplek['status_tampil'] }}"
                                                                    {{ $komplek['status_tampil'] == 1 ? 'checked' : '' }}>
                                                            </td>
                                                            <td width="50">{{ $no_komplek++ }}.</td>
                                                            <td class="tampilan_soal">{!! $komplek['soal'] !!}</td>
                                                        </tr>
                                                        @php
                                                            $opsi_komplek = json_decode($komplek['opsi_a']);
                                                        @endphp
                                                        @foreach ($opsi_komplek as $ok)
                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>
                                                                    <table class="w-100">
                                                                        <tr>
                                                                            <td width="20">
                                                                                {{ $ok->jawaban }}.</td>
                                                                            <td class="opsi_ganda">
                                                                                {!! $ok->isian !!}</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>Jawaban. <b>{!! ucwords($komplek['jawaban']) !!}</b></td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                @endforeach
                                            @else
                                                <div class="alert alert-danger align-content-center" role="alert">
                                                    Belum ada soal pilihan ganda yang terinput
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="isian">
                                    <table class="table table-bordered table-sm">
                                        <tbody>
                                            <tr class="alert alert-dark text-center align-middle">
                                                <th class="border-dark text-center">Jenis Soal</th>
                                                <th class="border-dark text-center" colspan="2">Jumlah Soal</th>
                                                <th class="border-dark text-center">Bobot Nilai</th>
                                                <th class="border-dark text-center">Point Per-nomor</th>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td rowspan="3" class="text-center align-middle border-dark">Isian Singkat
                                                </td>
                                                <td class="border-dark">Seharusnya</td>
                                                <td class="text-center border-dark">{{ $soal['jml_isian'] }}</td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_isian'] }} </td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_isian'] / $soal['jml_isian'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Telah dibuat</td>
                                                <td class="text-center border-dark">
                                                    {{ count($soal['soal_isian_singkat']) }}</td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Ditampilkan</td>
                                                <td class="text-center border-dark">
                                                    {{ $soal['isian_aktif'] == null ? 0 : $soal['isian_aktif'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-danger">
                                                <td colspan="5" class="border-dark">
                                                    Info :
                                                    <br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <form action="javascript:void(0)" id="formIsian">
                                        <div class="d-sm-flex justify-content-between">
                                            <div>
                                                <input style="width: 24px; height: 24px" class="check-pg-all my-1"
                                                    id="check_all-isian" type="checkbox">
                                                <label for="all-pg" class="align-middle">Pilih Semua</label>
                                            </div>
                                            <div>
                                                <span><b>Jumlah terpilih: </b></span>
                                                <span class="text-lg"><b id="total-isian">0</b></span>

                                                <button type="submit" class="btn btn-sm btn-purple ml-3" id="save-isian">
                                                    <i class="fa fa-save"></i> <span
                                                        class="d-none d-md-inline-block ml-1">Simpan Isian Terpilih</span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <input type="hidden" name="total_soal"
                                                value="{{ count($soal['soal_isian_singkat']) }}">
                                            @if (!empty($soal['soal_isian_singkat']))
                                                @php
                                                    $no_isian = 1;
                                                @endphp
                                                @foreach ($soal['soal_isian_singkat'] as $isian)
                                                    <table class="w-100">
                                                        <input type="hidden" name="id_soal_{{ $no_isian }}"
                                                            value="{{ $isian['id'] }}">
                                                        <tr>
                                                            <td width="50">
                                                                <input style="width: 24px; height: 24px"
                                                                    class="check-isian mt-2" value="1" type="checkbox"
                                                                    name="status_soal_{{ $no_isian }}"
                                                                    onclick="check_isian()"
                                                                    {{ $isian['status_tampil'] == 1 ? 'checked' : '' }}>
                                                            </td>
                                                            <td width="50">{{ $no_isian++ }}.</td>
                                                            <td class="tampilan_soal">{!! $isian['soal'] !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>Jawaban. <b>{!! ucwords($isian['jawaban']) !!}</b></td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                @endforeach
                                            @else
                                                <div class="alert alert-danger align-content-center" role="alert">
                                                    Belum ada soal pilihan ganda yang terinput
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="essay">
                                    <table class="table table-bordered table-sm">
                                        <tbody>
                                            <tr class="alert alert-dark text-center align-middle">
                                                <th class="border-dark text-center">Jenis Soal</th>
                                                <th class="border-dark text-center" colspan="2">Jumlah Soal</th>
                                                <th class="border-dark text-center">Bobot Nilai</th>
                                                <th class="border-dark text-center">Point Per-nomor</th>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td rowspan="3" class="text-center align-middle border-dark">Uraian/Essai
                                                </td>
                                                <td class="border-dark">Seharusnya</td>
                                                <td class="text-center border-dark">{{ $soal['jml_essay'] }}</td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_essay'] }} </td>
                                                <td rowspan="3" class="text-center align-middle border-dark">
                                                    {{ $soal['bobot_essay'] / $soal['jml_essay'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Telah dibuat</td>
                                                <td class="text-center border-dark">
                                                    {{ count($soal['soal_essay']) }}</td>
                                            </tr>
                                            <tr class="alert alert-success">
                                                <td class="border-dark">Ditampilkan</td>
                                                <td class="text-center border-dark">
                                                    {{ $soal['essay_aktif'] == null ? 0 : $soal['essay_aktif'] }}
                                                </td>
                                            </tr>
                                            <tr class="alert alert-danger">
                                                <td colspan="5" class="border-dark">
                                                    Info :
                                                    <br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <form action="javascript:void(0)" id="formEssay">
                                        <div class="d-sm-flex justify-content-between">
                                            <div>
                                                <input style="width: 24px; height: 24px" class="check-pg-all my-1"
                                                    id="check_all-essay" type="checkbox">
                                                <label for="all-essay" class="align-middle">Pilih Semua</label>
                                            </div>
                                            <div>
                                                <span><b>Jumlah terpilih: </b></span>
                                                <span class="text-lg"><b id="total-essay">0</b></span>

                                                <button type="submit" class="btn btn-sm btn-purple ml-2" id="save-essay">
                                                    <i class="fa fa-save"></i> <span
                                                        class="d-none d-md-inline-block ml-1">Simpan Essay Terpilih</span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <input type="hidden" name="total_soal"
                                                value="{{ count($soal['soal_essay']) }}">
                                            @if (!empty($soal['soal_essay']))
                                                @php
                                                    $no_essay = 1;
                                                @endphp
                                                @foreach ($soal['soal_essay'] as $essay)
                                                    <table class="w-100">
                                                        <input type="hidden" name="id_soal_{{ $no_essay }}"
                                                            value="{{ $essay['id'] }}">
                                                        <tr>
                                                            <td width="50">
                                                                <input style="width: 24px; height: 24px"
                                                                    class="check-essay mt-2" value="1" type="checkbox"
                                                                    name="status_soal_{{ $no_essay }}"
                                                                    onclick="check_essay()"
                                                                    {{ $essay['status_tampil'] == 1 ? 'checked' : '' }}>
                                                            </td>
                                                            <td width="50">{{ $no_essay++ }}.</td>
                                                            <td class="tampilan_soal">{!! $essay['soal'] !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td>Jawaban. <b>{!! ucwords($essay['jawaban']) !!}</b></td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                @endforeach
                                            @else
                                                <div class="alert alert-danger align-content-center" role="alert">
                                                    Belum ada soal pilihan ganda yang terinput
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //checkbox select
            //Pilihan ganda
            $('#check_all-pg').change(function() {
                $('.check-pg').prop('checked', this.checked);
                let count_pg = $('.check-pg:checked').length;
                $('#total-pg').html(count_pg);
            });

            $('.check-pg').change(function() {
                if ($('.check-pg:checked').length == $('.check-pg').length) {
                    $('#check_all-pg').prop('checked', true);
                } else {
                    $('#check_all-pg').prop('checked', false);
                }
            });

            // Komplek
            $('#check_all-komplek').change(function() {
                $('.check-komplek').prop('checked', this.checked);
                let count_komplek = $('.check-komplek:checked').length;
                $('#total-komplek').html(count_komplek);
            });

            $('.check-komplek').change(function() {
                if ($('.check-komplek:checked').length == $('.check-komplek').length) {
                    $('#check_all-komplek').prop('checked', true);
                } else {
                    $('#check_all-komplek').prop('checked', false);
                }
            });

            //Isian
            $('#check_all-isian').change(function() {
                $('.check-isian').prop('checked', this.checked);
                let count_isian = $('.check-isian:checked').length;
                $('#total-isian').html(count_isian);
            });

            $('.check-isian').change(function() {
                if ($('.check-isian:checked').length == $('.check-isian').length) {
                    $('#check_all-isian').prop('checked', true);
                } else {
                    $('#check_all-isian').prop('checked', false);
                }
            });

            //Essay
            $('#check_all-essay').change(function() {
                $('.check-essay').prop('checked', this.checked);
                let count_essay = $('.check-essay:checked').length;
                $('#total-essay').html(count_essay);
            });

            $('.check-essay').change(function() {
                if ($('.check-essay:checked').length == $('.check-essay').length) {
                    $('#check_all-essay').prop('checked', true);
                } else {
                    $('#check_all-essay').prop('checked', false);
                }
            });

            $('#formPG, #formKompleks, #formIsian, #formEssay').on('submit', function(event) {
                event.preventDefault();

                // $("#saveBtn").html(
                //     '<i class="fa fa-spin fa-spinner"></i> Loading');
                // $("#saveBtn").attr("disabled", true);
                // var action_url = '';

                // if ($('#action').val() == 'Add') {
                //     action_url = "{{ route('alumni-fakultas_create') }}";
                //     method_url = "POST";
                // }

                // if ($('#action').val() == 'Edit') {
                //     action_url = "{{ route('alumni-fakultas_update') }}";
                //     method_url = "PUT";
                // }
                $.ajax({
                    url: "{{ route('cbt-update_tampil') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);
                        // if (data.status == 'berhasil') {
                        //     $('#ajaxModel').modal('hide');
                        //     $('#CustomerForm').trigger("reset");
                        //     $('#data_fakultas').html(data.fakultas);
                        // }
                        // $('#saveBtn').html('Simpan');
                        // $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

        })

        function check_pg() {
            var x = $(".check-pg:checked").length;
            $('#total-pg').html(x);

        }

        function check_komplek() {
            var x = $(".check-komplek:checked").length;
            $('#total-komplek').html(x)
        }

        function check_isian() {
            var x = $(".check-isian:checked").length;
            $('#total-isian').html(x)
        }

        function check_essay() {
            var x = $(".check-essay:checked").length;
            $('#total-essay').html(x)
        }
    </script>
@endsection
