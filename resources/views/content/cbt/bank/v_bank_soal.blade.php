@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol pull-right">
                                        <a href="javascript:void(0)" class="btn btn-light text-dark"><i
                                                class="fas fa-sync-alt"></i>
                                            Reload</a>
                                        <a href="{{ route('cbt-add_bank') }}" class="btn btn-info"><i
                                                class="fas fa-plus-circle"></i> Tambah Bank Soal</a>
                                        <a href="javascript:void(0)" id="copy" class="btn btn-purple"><i
                                                class="fas fa-copy"></i> Copy Soal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <label for="inputPassword6">Filter</label>
                                                <select name="" id="" class="form-control mx-sm-3">
                                                    <option value="">Semua</option>
                                                    <option value="">Guru</option>
                                                    <option value="">Mapel</option>
                                                    <option value="">Level</option>
                                                </select>
                                                <select name="" id="" class="form-control mx-sm-3">
                                                    <option value="">Semua</option>
                                                    <option value="">Guru</option>
                                                    <option value="">Mapel</option>
                                                    <option value="">Level</option>
                                                </select>
                                                {{-- <input type="password" id="inputPassword6" class="form-control mx-sm-3"
                                                    aria-describedby="passwordHelpInline"> --}}
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <form action="" class="w-100">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-check my-2">
                                                        <input class="form-check-input ml-0" type="checkbox"
                                                            id="inlineFormCheck">
                                                        <label class="form-check-label" for="inlineFormCheck">
                                                            Select All
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <button type="submit" class="btn btn-danger pull-right"><i
                                                            class="fas fa-trash"></i> Hapus Bank Soal
                                                        Terpilih</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered mr-b-0 hover">
                                            <thead>
                                                <tr class="bg-info">
                                                    <th class="text-center"></th>
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Kode</th>
                                                    <th class="text-center">Mapel</th>
                                                    <th class="text-center">Kelas</th>
                                                    <th class="text-center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (!empty($soal))
                                                    @php
                                                        $no = 1;
                                                    @endphp
                                                    @foreach ($soal as $s)
                                                        <tr>
                                                            <td>
                                                                <div class="form-check">
                                                                    <input class="form-check-input mx-0" type="checkbox"
                                                                        value="">
                                                                </div>
                                                            </td>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $s['kode'] }}</td>
                                                            <td>{{ ucwords($s['mapel']) }}</td>
                                                            <td>
                                                                <b>Kelas : {{ $s['kelas'] }}</b>
                                                                @php
                                                                    $rombel = [];
                                                                    foreach ($s['rombels'] as $r) {
                                                                        $rombel[] = $r['nama'];
                                                                    }
                                                                @endphp
                                                                <p class="m-0">Rombel :
                                                                    {{ implode(', ', $rombel) }}</p>
                                                            </td>
                                                            <td>
                                                                <a href="#" class="btn btn-info btn-sm"><i
                                                                        class="fas fa-pencil-alt"></i></a>
                                                                <a href="#" class="btn btn-success btn-sm"><i
                                                                        class="fas fa-file-import"></i> Import Soal</a>
                                                                <a href="{{ route('cbt-detail_bank', ['code' => (new \App\Helpers\Help())->encode($s['id']), 'key' => str_slug($s['kode'])]) }}"
                                                                    class="btn btn-purple btn-sm"><i
                                                                        class="fas fa-info-circle"></i> Detail Soal</a>
                                                                <a href="#" class="btn btn-facebook btn-sm"><i
                                                                        class="fas fa-file-download"></i> Download Soal</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">Bank Soal saat ini tidak
                                                            tersedia</td>
                                                    </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalRuang" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formRuang">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_ruang">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Ruang Ujian</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama" type="text" name="nama">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Kode Ruang</label>
                            <div class="col-md-9">
                                <input class="form-control" name="kode" id="kode" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Status</label>
                            <div class="col-md-9">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Enabled</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="button" data-dismiss="modal" class="btn btn-danger text-left"><i
                                class="fas fa-times-circle"></i> Batal</a>
                            <button type="reset" class="btn btn-warning text-left"><i class="fas fa-sync-alt"></i>
                                Reset</button>
                            <button type="submit" class="btn btn-info ripple text-left" id="saveBtn"><i
                                    class="fas fa-plus-circle"></i> Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });






        })
    </script>
@endsection
