@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol pull-right">
                                        <a href="javascript:void(0)" id="addData" class="btn btn-info"><i
                                                class="fas fa-plus-circle"></i> Tambah</a>
                                        <a href="" class="btn btn-purple"><i class="fas fa-download"></i> Download</a>
                                        <a href="" class="btn btn-primary"><i class="fas fa-file-export"></i> Export</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="d-flex justify-content-between my-2">
                                    <div>
                                    </div>
                                    <div>
                                        <form id="formFilter">
                                            <div class="input-group">
                                                <input type="text" id="search" name="search" class="form-control"
                                                    placeholder="Search">
                                                <div class="input-group-btn">
                                                    <button type="submit" id="btnSearch" class="btn btn-info"><i
                                                            class="fa fa-search"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <table class="table table-striped table-bordered mr-b-0 hover">
                                    <thead>
                                        <tr class="bg-info">
                                            <th class="text-center">No</th>
                                            <th class="text-center">Informasi</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Telepon</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_guru">
                                        @if (!empty($guru))
                                            @php
                                                $no = 1;
                                            @endphp

                                            @foreach ($guru as $gr)
                                                <tr>
                                                    <td class="vertical-middle">{{ $no++ }}</td>
                                                    <td><b>{{ $gr['nama'] }}</b>
                                                        <p class="m-0">NIP. {{ $gr['nip'] }}</p><small>Alamat.
                                                            {{ $gr['alamat'] }}</small>
                                                    </td>
                                                    <td class="vertical-middle">{{ $gr['email'] }}</td>
                                                    <td class="vertical-middle">{{ $gr['telepon'] }}</td>
                                                    <td class="vertical-middle">
                                                        <label class="switch">
                                                            <input type="checkbox"
                                                                {{ $gr['status'] == 1 ? 'checked' : '' }}
                                                                class="guru_check" data-id="{{ $gr['id'] }}">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                                    <td class="vertical-middle text-center">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-facebook"
                                                            data-toggle="collapse"
                                                            data-target="#guru{{ $gr['id'] }}"><i
                                                                class="fas fa-book"></i></a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-info edit"
                                                            data-id="{{ $gr['id'] }}"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger delete"
                                                            data-id="{{ $gr['id'] }}"><i
                                                                class="fas fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" class="hiddenRow">
                                                        <div class="accordian-body collapse" id="guru{{ $gr['id'] }}">
                                                            <button class="btn btn-facebook btn-sm my-3 pull-right"
                                                                onclick="tambahPelajaran({{ $gr['id'] }})"><i
                                                                    class="fas fa-plus-circle"></i>
                                                                Tambah Pelajaran</button>
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">No</th>
                                                                        <th class="text-center">Mapel</th>
                                                                        <th class="text-center">Kelas</th>
                                                                        <th class="text-center">Status</th>
                                                                        <th class="text-center">Opsi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="dataPelajaran{{ $gr['id'] }}">
                                                                    @if (!empty($gr['pelajaran']))
                                                                        @php
                                                                            $nomer = 1;
                                                                        @endphp
                                                                        @foreach ($gr['pelajaran'] as $pj)
                                                                            <tr>
                                                                                <td class="vertical-middle">
                                                                                    {{ $nomer++ }}</td>
                                                                                <td class="vertical-middle">
                                                                                    {{ $pj['mapel'] }}</td>
                                                                                <td><b>{{ $pj['rombel'] }}</b> -
                                                                                    {{ (new \App\Helpers\Help())->getKelas($pj['kelas']) }}
                                                                                    <br>
                                                                                    <small>Jurusan.
                                                                                        {{ $pj['jurusan'] }}</small>
                                                                                </td>
                                                                                <td class="vertical-middle text-center">
                                                                                    <label class="switch">
                                                                                        <input type="checkbox"
                                                                                            {{ $pj['status'] == 1 ? 'checked' : '' }}
                                                                                            class="mapel_check"
                                                                                            data-id="{{ $pj['id'] }}">
                                                                                        <span class="slider round"></span>
                                                                                    </label>
                                                                                </td>
                                                                                <td class="vertical-middle text-center">
                                                                                    <a href="javascript:void(0)"
                                                                                        class="btn btn-sm btn-info editPelajaran"
                                                                                        data-id="{{ $pj['id'] }}"><i
                                                                                            class="fas fa-pencil-alt"></i></a>
                                                                                    <a href="javascript:void(0)"
                                                                                        class="btn btn-sm btn-danger deletePelajaran"
                                                                                        data-id="{{ $pj['id'] }}"><i
                                                                                            class="fas fa-trash"></i></a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr>
                                                                            <td colspan="5" class="text-center">Pelajaran
                                                                                yg terkoneksi belum tersedia</td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center">Data saat ini belum tersedia</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#formFilter').on('submit', function(event) {
                event.preventDefault();
                $("#btnSearch").html(
                    '<i class="fa fa-spin fa-spinner"></i>');
                $.ajax({
                    url: "{{ route('cbt-guru_filter_name') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        $('#data_guru').html(data);
                        $('#btnSearch').html('<i class="fa fa-search"></i>');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.guru_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt-guru_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });


            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-guru_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_guru').html(data.guru);
                                $('#search').val('');
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


        })
    </script>
@endsection
