@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card card-default my-shadow mb-4">
                        <div class="card-header">
                            <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                        </div>
                        <div class="card-body">
                            <div class="container-fluid h-100">
                                <div class="row h-100 justify-content-center d-none">
                                    <div class="col-4 input-group mb-3">
                                        <div class="input-group-prepend w-40">
                                            <span class="input-group-text">Otomatis ? </span>
                                        </div>
                                        <select name="auto" id="auto" class="form-control">
                                            <option value="0">TIDAK</option>
                                            <option value="1">YA</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row h-100 justify-content-center my-3">
                                    <div class="card col-4 bg-purple p-4">
                                        <span class="text-center text-white">TOKEN SAAT INI</span>
                                        <h1 class="text-center text-white" id="token-view">
                                            {{ $token == null ? '------' : $token['token'] }}</h1>
                                    </div>
                                </div>
                                <div class="row h-100 justify-content-center">
                                    <button id="generate" onclick="generate()" class="btn btn-success"><i
                                            class="fas fa-sync-alt"></i> GENERATE
                                        TOKEN</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })

        function generate() {
            $.ajax({
                url: "{{ route('cbt-token_generate') }}",
                beforeSend: function() {
                    $('#generate').html(
                        '<i class="fa fa-spin fa-sync-alt"></i> Memperbarui Token');
                },
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        $('#token-view').html(data.token);
                    } else {
                        noti(data.icon, data.message);
                    }
                    $('#generate').html(
                        '<i class="fas fa-sync-alt"></i> GENERATE TOKEN');
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#generate').html('<i class="fas fa-sync-alt"></i> GENERATE TOKEN');
                }
            });
        }
    </script>
@endsection
