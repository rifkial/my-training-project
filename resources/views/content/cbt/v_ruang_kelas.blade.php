@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                        <div class="row">
                            <div class="col-md-6">
                                <h5><i class="fa fa-info-circle"></i> Atur Ruangan</h5>
                                <ul>
                                    <li>
                                        <b>Gabung ke Ruang:</b> menggabungkan kelas ke ruang yang dipilh
                                    </li>
                                    <li>
                                        <b>Gabung ke Sesi:</b> menggabungkan kelas ke sesi yang dipilh
                                    </li>
                                    <li>
                                        <b>Gabungkan Siswa:</b> jika dicentang, maka seluruh siswa akan digabungkan ke Ruang
                                        dan Sesi yang dipilih.
                                        Jika tidak dicentang, maka siswa harus dipilihkan Ruang dan Sesi dibagian <b>Atur
                                            Sesi Siswa</b>
                                    </li>
                                    <li>
                                        <b>Simpan</b> terlebih dulu sebelum berpindah ke bagian <b>Atur Sesi Siswa</b>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5><i class="fa fa-info-circle"></i> Atur Sesi Siswa</h5>
                                <ul>
                                    <li>
                                        Pilih Ruang dan Sesi untuk tiap siswa
                                    </li>
                                    <li>
                                        Jangan lupa <b>Simpan</b> pada setiap kelas yang diatur
                                    </li>
                                    <li>
                                        <b>Simpan</b> di bagian <b>Atur Ruangan</b> akan mereset semua ruangan dan sesi
                                        siswa, Anda harus mengatur ulang di bagian <b>Atur Sesi Siswa</b>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link btn-purple mx-1 active" id="hide_rombel" href="#ruangan"
                                                data-toggle="tab">Atur Ruangan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link btn-purple mx-1" href="#sesi" id="show_rombel"
                                                data-toggle="tab">Atur Sesi
                                                Siswa</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row" id="data_rombel" style="display: none">
                                        <label for="inputPassword" class="col-sm-3 col-form-label">Pilih Kelas</label>
                                        <div class="col-sm-9">
                                            <select name="rombel" id="rombel" class="form-control">
                                                <option value="">Pilih Rombel</option>
                                                @foreach ($rombel as $rm)
                                                    <option value="{{ $rm['id'] }}">{{ $rm['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 my-3">
                                    <div class="tab-content pt-2">
                                        <div class="tab-pane active" id="ruangan">
                                            <form action="javascript:void(0)" id="formRuang">
                                                <div class="table-responsive">
                                                    <table class="table table-striped" id="data-tabel">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Kelas</th>
                                                                <th>Jurusan</th>
                                                                <th>Gabung ke Ruang</th>
                                                                <th>Gabung ke Sesi</th>
                                                                <th class="text-center">Gabungkan Siswa</th>
                                                            </tr>
                                                        </thead>
                                                        <input type="text" name="form_aksi" value="{{ $form_ruang }}">
                                                        <tbody>
                                                            @if ($form_ruang == 'add')
                                                                <input type="hidden" name="jumlah"
                                                                    value="{{ count($rombel) }}">
                                                                @php
                                                                    $no = 1;
                                                                @endphp
                                                                @foreach ($rombel as $rm)
                                                                    <tr>
                                                                        <input type="hidden"
                                                                            name="id_rombel_{{ $no }}"
                                                                            value="{{ $rm['id'] }}">
                                                                        <td>{{ $no }}</td>
                                                                        <td>{{ $rm['nama'] }}</td>
                                                                        <td>{{ $rm['jurusan'] }}</td>
                                                                        <td>
                                                                            <select name="ruang_{{ $no }}" id=""
                                                                                class="form-control form-control-sm">
                                                                                @foreach ($ruang as $rg)
                                                                                    <option value="{{ $rg['id'] }}">
                                                                                        {{ $rg['nama'] }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <select name="sesi_{{ $no }}" id=""
                                                                                class="form-control form-control-sm">
                                                                                @foreach ($sesi as $ss)
                                                                                    <option value="{{ $ss['id'] }}">
                                                                                        {{ $ss['nama'] }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <input class="check" type="checkbox"
                                                                                value="1"
                                                                                name="status_ruang_{{ $no }}">
                                                                        </td>
                                                                    </tr>
                                                                    @php
                                                                        $no++;
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                <input type="hidden" name="jumlah"
                                                                    value="{{ count($kelas_ruang) }}">
                                                                @php
                                                                    $no = 1;
                                                                @endphp
                                                                @foreach ($kelas_ruang as $rm)
                                                                    <tr>
                                                                        <input type="hidden" name="id_{{ $no }}"
                                                                            value="{{ $rm['id'] }}">
                                                                        <input type="hidden"
                                                                            name="id_rombel_{{ $no }}"
                                                                            value="{{ $rm['id_rombel'] }}">
                                                                        <td>{{ $no }}</td>
                                                                        <td>{{ $rm['rombel'] }}</td>
                                                                        <td>{{ $rm['jurusan'] }}</td>
                                                                        <td>
                                                                            <select name="ruang_{{ $no }}" id=""
                                                                                class="form-control form-control-sm">
                                                                                @foreach ($ruang as $rg)
                                                                                    <option value="{{ $rg['id'] }}"
                                                                                        {{ $rg['id'] == $rm['id_ruang'] ? 'selected' : '' }}>
                                                                                        {{ $rg['nama'] }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <select name="sesi_{{ $no }}" id=""
                                                                                class="form-control form-control-sm">
                                                                                @foreach ($sesi as $ss)
                                                                                    <option value="{{ $ss['id'] }}"
                                                                                        {{ $ss['id'] == $rm['id_sesi'] ? 'selected' : '' }}>
                                                                                        {{ $ss['nama'] }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <input class="check" type="checkbox"
                                                                                value="1"
                                                                                name="status_ruang_{{ $no }}"
                                                                                {{ $rm['set_siswa'] == 1 ? 'checked' : '' }}>
                                                                        </td>
                                                                    </tr>
                                                                    @php
                                                                        $no++;
                                                                    @endphp
                                                                @endforeach
                                                            @endif


                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="float-right">
                                                    <button type="submit" id="saveRombel" class="btn btn-info">
                                                        <i class="fas fa-save mr-1"></i> Simpan
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane" id="sesi">
                                            <form action="">
                                                tes
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '#hide_rombel', function() {
                $('#data_rombel').hide();

            });
            $(document).on('click', '#show_rombel', function() {
                $('#data_rombel').show();

            });

            $('#formRuang').on('submit', function(event) {
                event.preventDefault();
                $("#saveRombel").html(
                    '<i class="fa fa-spin fa-sync-alt"></i> Memproses..');
                $("#saveRombel").attr("disabled", true);
                $.ajax({
                    url: "{{ route('cbt-kelas_ruang_save') }}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        $("#saveRombel").html(
                            '<i class="fas fa-save mr-1"></i>Simpan');
                        $("#saveRombel").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $("#saveRombel").html(
                            '<i class="fas fa-save mr-1"></i>Simpan');
                        $("#saveRombel").attr("disabled", false);
                    }
                });
            });





        })
    </script>
@endsection
