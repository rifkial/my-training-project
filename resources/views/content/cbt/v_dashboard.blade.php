@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            Selamat Datang di Sistem Ujian Online
                        </div>
                        <div class="card-body">
                            <div class="alert alert-icon alert-info border-info alert-dismissible fade show" role="alert">
                                Selamat Datang <strong>Administrator</strong>, Username <strong>Admin</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </main>
@endsection
