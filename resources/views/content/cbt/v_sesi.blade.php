@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <link href="{{ asset('asset/custom/toogle.css') }}" rel="stylesheet" type="text/css">
    <main class="clearfix">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 my-3">
                    <div class="card">
                        <div class="card-header alert-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="box-title mr-b-0">{{ session('title') }}</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="aksiTombol pull-right">
                                        <a href="javascript:void(0)" id="addData" class="btn btn-info"><i
                                                class="fas fa-plus-circle"></i> Tambah</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="data-tabel">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Kode Sesi</th>
                                            <th>Waktu</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal modal-color-scheme fade bs-modal-md-color-scheme" id="modalSesi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <form id="formSesi">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <input type="hidden" name="id" id="id_sesi">
                        {{-- <div class="row"> --}}
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l0">Sesi Ujian</label>
                            <div class="col-md-9">
                                <input class="form-control" id="nama" type="text" name="nama">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Kode Sesi</label>
                            <div class="col-md-9">
                                <input class="form-control" name="kode" id="kode" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Jam Mulai</label>
                            <div class="col-md-9">
                                <input class="form-control" id="jam_mulai" type="time" name="jam_mulai">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Jam Berakhir</label>
                            <div class="col-md-9">
                                <input class="form-control" id="jam_selesai" name="jam_selesai" type="time">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l10">Status</label>
                            <div class="col-md-9">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Enabled</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>
                        {{-- </div> --}}

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="button" data-dismiss="modal" class="btn btn-danger text-left"><i
                                class="fas fa-times-circle"></i> Batal</a>
                            <button type="reset" class="btn btn-warning text-left"><i class="fas fa-sync-alt"></i>
                                Reset</button>
                            <button type="submit" class="btn btn-info ripple text-left" id="saveBtn"><i
                                    class="fas fa-plus-circle"></i> Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });



            $(document).on('click', '.guru_check', function() {
                let id = $(this).data('id');
                let value = $(this).is(':checked') ? 1 : 0;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt-guru_update_status') }}",
                    data: {
                        id,
                        value
                    },
                    success: function(data) {
                        console.log(data)
                    }
                });
            });

            var table = $('#data-tabel').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'kode',
                        name: 'kode'
                    },
                    {
                        data: 'waktu',
                        name: 'waktu'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });


            $(document).on('click', '#addData', function() {
                $('#formSesi').trigger("reset");
                $('#modelHeading').html("Tambah {{ session('title') }}");
                $('#modalSesi').modal('show');
                $('#action').val('Add');
            });

            $('#formSesi').on('submit', function(event) {
                event.preventDefault();
                $("#saveBtn").html(
                    '<i class="fa fa-spin fa-spinner"></i> Loading');
                $("#saveBtn").attr("disabled", true);
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('cbt-sesi_create') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('cbt-sesi_update') }}";
                    method_url = "PUT";
                }
                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formSesi').trigger("reset");
                            $('#modalSesi').modal('hide');
                            table.ajax.reload();
                        }
                        noti(data.icon, data.message);
                        $('#saveBtn').html('<i class="fas fa-plus-circle"></i> Simpan');
                        $("#saveBtn").attr("disabled", false);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('cbt-sesi_detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(data) {
                        $(loader).html('<i class="fas fa-pencil-alt"></i>');
                        $('#modelHeading').html("Edit {{ session('title') }}");
                        $('#id_sesi').val(data.id);
                        $('#kode').val(data.kode);
                        $('#nama').val(data.nama);
                        $('#jam_mulai').val(data.jam_mulai);
                        $('#jam_selesai').val(data.jam_selesai);
                        $('#status').val(data.status);
                        $('#action').val('Edit');
                        $('#modalSesi').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('cbt-sesi_delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i>');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                table.ajax.reload();
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>');
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });


        })
    </script>
@endsection
