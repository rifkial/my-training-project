@php
$ext = '';
@endphp
@if ($template == 'collapsed_nav')
    @php
        $ext = '_collapsed_nav';
    @endphp
@elseif($template == "e_commerce")
    @php
        $ext = '_e_commerce';
    @endphp
@elseif($template == "real_state")
    @php
        $ext = '_real_state';
    @endphp
@elseif($template == "university")
    @php
        $ext = '_university';
    @endphp
@elseif($template == "default")
    @php
        $ext = '_default';
    @endphp
@else
    @php
        $ext = '_horizontal_nav_icons';
    @endphp
@endif
@extends('template/template'.$ext.'/app')
@section('content')
    <style>
        .pace {
            display: none;
        }

    </style>
    <div class="row">
        <div class="col-sm-12 col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <h2 class="box-title mt-1">{{ Session::get('title') }}</h2>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="pull-right">
                                @if (session('role') != 'supervisor')
                                    <button class="btn btn-outline-info mt-1" id="addData"><i class="fas fa-plus-circle"></i>
                                        Tambah</button>
                                @endif
                                <button id="import" class="btn btn-outline-success mt-1"><i class="fas fa-file-import"></i>
                                    Import</button>
                                <button id="btnTrash" class="btn btn-outline-danger mt-1"><i
                                        class="fas fa-trash-restore"></i>
                                    Trash</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div style="width: 100%;">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tabel-prestasi">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Guru</th>
                                        <th class="text-center">Prestasi</th>
                                        <th class="text-center">Tahun</th>
                                        @if (session('role') != 'supervisor')
                                            <th class="text-center">Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody id="data_prestasi">
                                    @if (!empty($prestasi))
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($prestasi as $pt)
                                            <tr>
                                                <td class="vertical-middle">{{ $no++ }}</td>
                                                <td>
                                                    <b>{{ $pt['nama'] }}</b>
                                                    <br><small>NIP. {{ $pt['nip'] != null ? $pt['nip'] : '-' }} </small>
                                                </td>
                                                <td><b>{{ $pt['prestasi'] }}</b><br><small>Keterangan :
                                                        {{ $pt['keterangan'] != null ? $pt['keterangan'] : '-' }}</small>
                                                </td>
                                                <td class="vertical-middle">{{ $pt['tahun'] }}</td>
                                                @if (session('role') != 'supervisor')
                                                    <td class="text-center vcertical-middle">
                                                        <a href="javascript:void(0)" data-id="{{ $pt['id'] }}"
                                                            class="edit btn btn-info btn-sm"><i
                                                                class="fas fa-pencil-alt"></i></a>
                                                        <a href="javascript:void(0)" data-id="{{ $pt['id'] }}"
                                                            class="delete btn btn-sm btn-danger"><i
                                                                class="fas fa-trash"></i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">Data saat ini belum tersedia</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalPrestasi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="titlePrestasi"></h5>
                </div>
                <form id="formPrestasi" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" id="id_prestasi">
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Guru</label>
                                    <div class="col-sm-12">
                                        <select name="id_guru" id="id_guru" class="form-control">
                                            <option value="">Pilih guru..</option>
                                            @foreach ($guru as $gr)
                                                <option value="{{ (new \App\Helpers\Help())->encode($gr['id']) }}">
                                                    {{ $gr['nama'] . ' NIP ' . $gr['nip'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Prestasi</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="nama" id="nama" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Tahun</label>
                                    <div class="col-sm-12">
                                        <input name="tahun" id="tahun" class="form-control datepickerYear" type="text"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12 control-label">Keterangan</label>
                                    <div class="col-sm-12">
                                        <textarea name="keterangan" class="form-control" id="keterangan"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <button type="submit" class="btn btn-info btn-rounded ripple text-left" id="saveBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-color-scheme fade bs-modal-lg-color-scheme" id="modalImportPrestasi" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="HeadingImport">Import Data Prestasi</h5>
                </div>
                <form action="javascript:void(0)" id="formImport" class="form-horizontal">
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <center>
                                    <div class="centr">
                                        <i class="material-icons text-success"
                                            style="font-size: 4.5rem;">file_download</i><br>
                                        <b class="text-success">Choose the file to be imported</b>
                                        <p style="margin-bottom: 0px">[only xls, xlsx and csv formats are supported]</p>
                                        <p>Maximum upload file size is 5 MB.</p>
                                        <div class="input_kelas"></div>

                                        <input type="file" id="actual-btn" class="margin" type="file" name="image"
                                            accept="*" hidden />
                                        <label for="actual-btn" id="label_file" class="bg-success p-2 rounded"
                                            style="cursor: pointer"> <i class="material-icons text-white">file_upload</i>
                                            Upload File</label>
                                        <span id="file-chosen">No file chosen</span>
                                        <br>
                                        <u id="template_import">
                                            <a href="{{ $url }}" target="_blank" class="text-success">Download
                                                sample template
                                                for
                                                import
                                            </a>
                                        </u>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-3"></div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-rounded ripple text-left" id="importBtn">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            // $('#tabel-prestasi').DataTable();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#import').click(function() {
                $('#modalImportPrestasi').modal('show');
                $('#file-chosen').html('No file choosen');
            });


            $('#addData').click(function() {
                $('#formPrestasi').trigger("reset");
                $('#titlePrestasi').html("Tambah Prestasi");
                $('#modalPrestasi').modal('show');
                $('#action').val('Add');
            });

            $('#formPrestasi').on('submit', function(event) {
                $('#saveBtn').html('Sending..');
                $("#saveBtn").attr("disabled", true);
                event.preventDefault();
                var action_url = '';

                if ($('#action').val() == 'Add') {
                    action_url = "{{ route('prestasi_guru-simpan') }}";
                    method_url = "POST";
                }

                if ($('#action').val() == 'Edit') {
                    action_url = "{{ route('prestasi_guru-update') }}";
                    method_url = "PUT";
                }

                $.ajax({
                    url: action_url,
                    method: method_url,
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.status == "berhasil") {
                            $('#modalPrestasi').modal('hide');
                            $('#formPrestasi').trigger("reset");
                            $('#data_prestasi').html(data.prestasi);
                        }
                        $('#saveBtn').html('Simpan');
                        $("#saveBtn").attr("disabled", false);
                        noti(data.icon, data.message);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Simpan');
                    }
                });
            });

            $('body').on('submit', '#formImport', function(e) {
                e.preventDefault();
                var actionType = $('#btn-save').val();
                $('#importBtn').html('Sending..');
                var formDatas = new FormData(document.getElementById("formImport"));
                $.ajax({
                    type: "POST",
                    url: "{{ route('prestasi_guru-import') }}",
                    data: formDatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        // console.log(data);
                        if (data.status == 'berhasil') {
                            $('#formImport').trigger("reset");
                            $('#modalImportPrestasi').modal('hide');
                            $('#data_prestasi').html(data.prestasi);
                        }
                        noti(data.icon, data.message);
                        $('#importBtn').html('Simpan');
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#importBtn').html('Simpan');
                    }
                });
            });


            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                let loader = $(this);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('prestasi_guru-detail') }}",
                    data: {
                        id
                    },
                    beforeSend: function() {
                        $(loader).html(
                            '<i class="fa fa-spin fa-spinner"></i> Loading');
                    },
                    success: function(data) {
                        $(loader).html(
                            '<i class="fas fa-pencil-alt"></i>');
                        $('#titlePrestasi').html("Edit Prestasi");
                        $('#id_prestasi').val(data.id);
                        $('#id_guru').val(data.id_guru);
                        $('#nama').val(data.prestasi);
                        $('#tahun').val(data.tahun);
                        $('#keterangan').val(data.keterangan)
                        $('#action').val('Edit');
                        $('#modalPrestasi').modal('show');
                    }
                });
            });
            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let loader = $(this);
                swal({
                    title: "Apa kamu yakin?",
                    text: "ingin menghapus data ini!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function() {
                    $.ajax({
                        url: "{{ route('prestasi_guru-delete') }}",
                        type: "POST",
                        data: {
                            id
                        },
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(data) {
                            if (data.status == 'berhasil') {
                                $('#data_prestasi').html(data.prestasi);
                            }
                            $(loader).html(
                                '<i class="fas fa-trash"></i>'
                            );
                            swa(data.status + "!", data.message, data.icon);
                        }
                    })
                }, function(dismiss) {
                    if (dismiss === 'cancel') {
                        swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                    }
                })
            });
        });
        const actualBtn = document.getElementById('actual-btn');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        });
    </script>
@endsection
