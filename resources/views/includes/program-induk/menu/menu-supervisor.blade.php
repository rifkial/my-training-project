<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<style>
    .pace {
        display: none;
    }


    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page' : '' }}">
            <a href="{{ url('program/induk') }}" class="{{ empty(request()->segment(3)) ? 'color-color-scheme' : '' }}">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="{{ request()->segment(3) == 'siswa' && request()->segment(2) == 'induk' ? 'current-page' : '' }}">
            <a href="{{ route('siswa_induk-beranda') }}"
                class="{{ request()->segment(3) == 'siswa' && request()->segment(2) == 'induk' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Siswa</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) === 'guru' ? 'current-page' : '' }}">
            <a href="{{ route('guru_induk-beranda') }}"
                class="{{ request()->segment(3) === 'guru' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-chalkboard-teacher"></i>
                <span class="hide-menu">Guru</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) === 'tata_usaha' ? 'current-page' : '' }}">
            <a href="{{ route('tata_usaha_induk-beranda') }}"
                class="{{ request()->segment(3) === 'tata_usaha' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-id-card-alt"></i>
                <span class="hide-menu">Tata Usaha</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) === 'kategori-lomba' ? 'current-page' : '' }}">
            <a href="{{ route('kategori_lomba-beranda') }}"
                class="{{ request()->segment(3) === 'kategori-lomba' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-award"></i>
                <span class="hide-menu">Prestasi</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) === 'transkip' ? 'current-page' : '' }}">
            <a href="{{ route('raport-transkip') }}"
                class="{{ request()->segment(4) === 'transkip' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-star"></i>
                <span class="hide-menu">Transkip</span>
            </a>
        </li>
    </ul>
</nav>
