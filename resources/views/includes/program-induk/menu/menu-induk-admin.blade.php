<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<style>
    .pace {
        display: none;
    }


    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page' : '' }}">
            <a href="{{ url('program/induk') }}"
                class="{{ empty(request()->segment(3)) ? 'color-color-scheme' : '' }}">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="{{ request()->segment(3) === 'siswa' && request()->segment(2) === 'user' ? 'current-page' : '' }}">
            <a href="{{ route('user-siswa') }}"
                class="{{ request()->segment(3) === 'siswa' && request()->segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Siswa</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'siswa' && request()->segment(2) == 'induk' ? 'current-page' : '' }}">
            <a href="{{ route('siswa_induk-beranda') }}"
                class="{{ request()->segment(3) == 'siswa' && request()->segment(2) == 'induk' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-users"></i>
                <span class="hide-menu">Data Siswa</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) === 'guru' ? 'current-page' : '' }}">
            <a href="{{ route('guru_induk-beranda') }}"
                class="{{ request()->segment(3) === 'guru' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-chalkboard-teacher"></i>
                <span class="hide-menu">Guru</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) === 'tata_usaha' ? 'current-page' : '' }}">
            <a href="{{ route('tata_usaha_induk-beranda') }}"
                class="{{ request()->segment(3) === 'tata_usaha' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-id-card-alt"></i>
                <span class="hide-menu">Tata Usaha</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'prestasi-guru' || request()->segment(3) == 'kategori-lomba' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);"><i class="fas fa-award"></i> <span
                    class="hide-menu">Prestasi</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('prestasi_guru-beranda') }}">Prestasi Guru</a>
                </li>
                <li><a href="{{ route('kategori_lomba-beranda') }}">Prestasi Siswa</a>
                </li>
            </ul>
        </li>
        {{-- <li  class="{{ request()->segment(3) === 'kategori-lomba' ? 'current-page' : '' }}">
            <a href="{{ route('kategori_lomba-beranda') }}"
                class="{{ request()->segment(3) === 'kategori-lomba' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-award"></i>
                <span class="hide-menu">Prestasi</span>
            </a>
        </li> --}}
        <li class="{{ request()->segment(4) === 'transkip' ? 'current-page' : '' }}">
            <a href="{{ route('raport-transkip') }}"
                class="{{ request()->segment(4) === 'transkip' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-star"></i>
                <span class="hide-menu">Transkip</span>
            </a>
        </li>
        {{-- <li class="menu-item-has-children {{ Request::segment(2) === 'master' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'master' ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-award"></i>
                    <span class="hide-menu {{ Request::segment(2) === 'master' ? 'color-color-scheme' : '' }}">
                        Prestasi</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('kategori_lomba-beranda') }}"
                        class="{{ Request::segment(3) === 'mapel' ? 'color-color-scheme' : '' }}">Kategori Lomba
                    </a>
                    <a href="{{ route('master-jadwal') }}"
                        class="{{ Request::segment(3) === 'jadwal_pelajaran' ? 'color-color-scheme' : '' }}">Jadwal Pelajaran
                    </a>
                    <a href="{{ route('e_learning-guru_pelajaran') }}"
                        class="{{ Request::segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Mapel perkelas
                    </a>
                    <a href="{{ route('e_learning-kelas_siswa') }}"
                        class="{{ Request::segment(3) === 'kelas_siswa' ? 'color-color-scheme' : '' }}">Siswa Perkelas
                    </a>
                </li>
            </ul>
        </li> --}}
    </ul>
</nav>

