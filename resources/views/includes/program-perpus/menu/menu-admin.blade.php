<style>



    .header-light .navbar {
        background: #f8f9fa !important;
    }

    .navbar-brand{
        background-color: #f8f9fa !important;
        width: 20%;
    }

    .spacer{
        width: 60%;
        background-color: #f8f9fa !important;
    }


    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    #radioBtn .notActive {
        color: #3276b1;
        background-color: #fff;
        border: 1px solid #fff;
    }

    .sidebar-dark .side-user .side-menu li a {
        color: #fff !important;
    }

    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    button.btn.btn-danger.btn-sm,
    a.btn.btn-success.btn-sm {
        margin-top: 3px;
    }

    .color-color-scheme{
        color: #ffffff !important;
    }

    .sidebar-dark .site-sidebar {
        background: #738091;
        border-right: 1px solid #ddd;
        overflow-y: auto;
        height: 100vh;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active,
    .sidebar-dark .side-menu li.act {
        background: #919aa5;
        width: 100%;
    }

    .sidebar-dark .side-menu li a:hover {
        color: #fff;
    }

    span.hide-menu {
        color: #fff;
    }

    .sidebar-dark .side-menu li a {
        color: #d6d6d6;
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #e6e6e6;
    }

    .sidebar-dark .side-menu li.active>a {
        color: #a8a0a0;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    /*SIDEBAR*/

    .sidebar-expand .main-wrapper {
        margin-left: 15.375rem !important;
    }


    .sidebar-expand .site-sidebar {
        width: 15.375rem !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }

    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

    .sidebar-nav{
         overflow-y: auto;
          height: 100vh;
    }

</style>

<nav class="sidebar-nav">
    <ul class="nav in side-menu px-0">

        <li class="{{Request::segment(3) == '' ? 'active act' : ''}}">
            <a href="{{ url('program/perpustakaan') }}" class="ripple">
                <i class="fa fa-home fa-lg mr-2" aria-hidden="true"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>

        <!-- sirkulasi -->
        <li class="menu-item-has-children ">
            <a href="" class="ripple">
                <span class="">
                    <i class="fas fa-exchange-alt mr-2"></i>
                    <span class="hide-menu ">Sirkulasi
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('sirkulasi-main')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Sirkulasi
                    </a>

                    <a href="{{route('peminjaman-active')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Peminjaman
                    </a>

                    <a href="{{route('pengadaan-active')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pengadaan
                    </a>

                    <a href="{{route('perpus-denda')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Denda
                    </a>

                </li>
            </ul>
        </li>


        <!-- master -->
        <li class="menu-item-has-children {{Request::segment(3) == 'master' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-th-large fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">Master
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('perpus-bahasa')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Bahasa
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-jenis')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Jenis
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-gmd')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">GMD
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-penerbit')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Penerbit
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-koleksi')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Koleksi
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-topik')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Topik
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-rak')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Rak
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-pengarang')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Pengarang
                    </a>
                </li>
                <li>
                    <a href="{{route('perpus-agen')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Agen
                    </a>
                </li>
            </ul>
        </li>

       <li class="{{Request::segment(3) == 'pengumuman' ? 'active act' : ''}}">
            <a href="{{ route('pengumuman-index') }}" class="ripple">
                <i class="fa fa-bell fa-lg mr-2" aria-hidden="true"></i>
                <span class="hide-menu">Pengumuman</span>
            </a>
        </li>

        <!-- pustaka -->
        <li class="menu-item-has-children ">
            <a href="" class="ripple">
                <span class="">
                    <i class="fas fa-book fa-lg mr-2"></i>
                    <span class="hide-menu ">Pustaka
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('buku-admin') }}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pustaka
                    </a>

                    <a href="{{ route('pusdig-admin') }}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pustaka digital
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children {{Request::segment(3) == 'user' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                <i class="fa fas fa-user-alt fa-lg mr-2" aria-hidden="true"></i>
                <span class="hide-menu">User</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('admin-perpus-all')}}"
                        class="">Admin
                    </a>
                    <a href="{{route('user-perpus-all')}}"
                        class="">User
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fas fa-cogs mr-2"></i>
                    <span class="hide-menu ">Setting
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('kartu-index')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">kartu
                    </a>
                    <a href="{{route('label-index')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">label
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fas fa-file-alt fa-lg mr-2"></i>
                    <span class="hide-menu ">Laporan
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu ml-4">
                <li>
                    <a href="{{route('laporan-index')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Bulanan
                    </a>
                    <a href="{{route('laporan-pustaka')}}"
                        class="{{Request::segment(4) == 'pustaka' ? 'color-color-scheme' : ''}}">Pustaka berdasarkan koleksi
                    </a>
                    <a href="{{route('laporan-pustaka-jenis')}}"
                        class="{{Request::segment(4) == 'pustaka' ? 'color-color-scheme' : ''}}">Pustaka berdasarkan jenis
                    </a>
                    <a href="{{route('laporan-pustaka-rak')}}"
                        class="{{Request::segment(4) == 'pustaka' ? 'color-color-scheme' : ''}}">Pustaka berdasarkan rak
                    </a>
                    <a href="{{route('laporan-pusdig')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pustaka digital berdasarkan kategori
                    </a>
                    <a href="{{route('laporan-pusdig-jenis')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pustaka digital berdasarkan jenis
                    </a>
                    <a href="{{route('laporan-peminjaman')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Peminjaman
                    </a>
                    <a href="{{route('laporan-peminjaman-terlambat')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Peminjaman terlambat
                    </a>
                    <a href="{{route('laporan-pengadaan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pengadaan
                    </a>
                    <a href="{{route('laporan-user')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">User
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
