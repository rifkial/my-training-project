
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand bg-light" href="{{ route('home-perpus')}}">PERPUSTAKAAN</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse bg-light" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">

	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('peminjaman-user-perpus') }}">Peminjaman<span class="sr-only">(current)</span></a>
	      </li>

	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('denda-user-perpus') }}">Denda</a>
	      </li>

	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('perpus-member') }}">Membership</a>
	      </li>

	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('perpus-about') }}">Tentang</a>
	      </li>

	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Bantuan
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">Kehilangan buku</a>
	          <a class="dropdown-item" href="#">Membuat kartu anggota</a>
	        </div>
	      </li>

	    </ul>

	    <div class="dropdown d-none d-md-flex">
	    	<a href="javascript:void(0);" class="p-2" data-toggle="dropdown">
	    		<div class="d-md-flex">
	    			<span>
		    			<p class="mb-0 mt-2"><strong>{{ $profile['nama'] }}</strong></p>
		    			<p class="mb-1">{{ $profile['role'] }}</p>
		    		</span>
		    		
		    		<span class="avatar thumb-sm ml-3">
	                    <img src="{{ $profile["file"] }}" class="rounded-circle" alt="">
	                </span>
	    		</div>	
	    	</a>

            <div class="dropdown-menu dropdown-left animated flipInY">  
            	<a class="dropdown-item" href="{{ route('profil-user-perpus') }}">Profil Saya</a>            	
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('auth.logout')}}">
                	<i class="fas fa-sign-out-alt"></i>
                	<strong>Logout</strong>
                </a>
            </div>
        </div>



	  </div>
	</nav>
		


