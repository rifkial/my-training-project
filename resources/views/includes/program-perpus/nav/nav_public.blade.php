
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand bg-light" href="{{ route('perpus-public')}}">PERPUSTAKAAN</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse bg-light" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto d-flex flex-sm-row flex-column align-items-center">
	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('perpus-member') }}">Membership</a>
	      </li>

	      <li class="nav-item">
	        <a class="nav-link" href="{{ route('perpus-about') }}">Tentang</a>
	      </li>

	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Bantuan
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">Kehilangan buku</a>
	          <a class="dropdown-item" href="#">Membuat kartu anggota</a>
	        </div>
	      </li>
	    </ul>
        <div class="d-flex justify-content-center">
            <a href="{{ url('program/perpustakaan') }}" class="btn btn-outline-success btn-sm my-2 my-sm-0 mr-2" type="submit">Login</a>
        </div>

	  </div>
	</nav>



