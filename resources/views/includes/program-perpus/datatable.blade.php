<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
{{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.6.0/datatables.min.js"></script> --}}



<script type="text/javascript">

	const konfigUmum = {
        responsive: true,
        serverSide: true,
        processing: true,
        ordering: true,
        paging: true,
        searching: true,
   };

   function MakeTable(tabel,url,column){
        tabel.DataTable({
            bDestroy: true,
            ...konfigUmum,
            dom : '<"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column
        });
    }

    function MakeFullTable(tabel,url,column,buttons){
        tabel.DataTable({
            ...konfigUmum,
            dom : '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            ajax: {
                "url": url,
                "method": "GET"
            },
            columns: column,
            buttons: buttons
        });
    }

</script>

