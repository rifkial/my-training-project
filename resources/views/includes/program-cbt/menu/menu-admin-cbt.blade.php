<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<div class="side-user bg-facebook">
    <a class="col-sm-12 media clearfix" href="javascript:void(0);">
        <div class="media-body hide-menu text-center">
            <h4 class="media-heading mr-b-5 text-uppercase">Tahun Pelajaran : {{ $tahun['tahun_ajaran'] }}</h4>
            <span class="user-type fs-12">Semester {{ $tahun['semester'] }}</span>
        </div>
    </a>
    <div class="clearfix"></div>
</div>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/cbt') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'libur' || request()->segment(3) == 'jadwal' || request()->segment(3) == 'hadir' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-signature"></i> <span class="hide-menu">Master</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'libur' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-mapel') }}">Mapel</a>
                </li>
                <li>
                    <a href="{{ route('user-siswa') }}">Siswa</a>
                </li>
                <li>
                    <a href="{{ route('user-guru') }}">Guru</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'jenis' || request()->segment(3) == 'bank_soal' || request()->segment(3) == 'sesi' || request()->segment(3) == 'ruang' || request()->segment(3) == 'ruang_kelas' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-laptop-medical"></i> <span class="hide-menu">Data Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'jenis' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jenis') }}">Jenis Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'sesi' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}">Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}">Ruang</a>
                </li>
                <li class="{{ request()->segment(3) == 'ruang_kelas' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-kelas_ruang') }}">Atur Ruang & Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'bank_soal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-bank_soal') }}">Bank Soal</a>
                </li>
                <li class="{{ request()->segment(3) == 'jadwal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jadwal') }}">Jadwal</a>
                </li>
                <li class="{{ request()->segment(3) == 'jadwal' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-token') }}">Token</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-laptop-medical"></i> <span class="hide-menu">Pelaksanaan Ujian</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-jenis') }}">Jenis Ujian</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-sesi') }}">Sesi</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-ruang') }}">Ruang</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-users-cog"></i> <span class="hide-menu">User Management</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-admin') }}">Administrator</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-siswa') }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('cbt-guru') }}">Guru</a>
                </li>
            </ul>
        </li>

    </ul>
</nav>



{{-- <nav class="navbar navbar-expand-xl navbar-dark bg-white px-0" style="height: 50px">
    <div class="collapse navbar-collapse d-flex justify-content-between mx-4" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <h2 class="box-title m-0">CAT - Computer Assisted Test 2.0</h2>
      </ul>

      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="line-height: 50px;">
            <strong class="text-dark">JHumintern (Admin) <i class="fas fa-user-circle"></i></strong>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a href="{{ route('cbt-setting') }}" class="dropdown-item">
              <strong>Setting CBT</strong>
            </a>
            <a class="dropdown-item">
              <strong>Ubah Password</strong>
            </a>
            <a href="#" class="dropdown-item">
              <strong>Logout</strong>
            </a>

          </div>
        </li>
      </ul>
    </div>
  </nav>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<main class="main-wrapper clearfix py-0">
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 my-3">
                <div class="card">
                    <div class="card-body d-flex justify-content-around">
                        <a href="{{ url('program/cbt') }}" class="btn btn-{{ empty(request()->segment(3)) ? 'success' : 'info' }} btn-sq">
                            <i class="fas fa-chart-line fa-3x"></i><br><br>Dashboard
                        </a>
                        <a href="{{ route('cbt-siswa') }}" class="btn btn-{{ request()->segment(3) === 'siswa' ? 'success' : 'info' }} btn-sq">
                            <i class="fas fa-users fa-3x"></i><br><br>Data Siswa
                        </a>
                        <a href="{{ route('cbt-guru') }}" class="btn btn-{{ request()->segment(3) === 'guru' ? 'success' : 'info' }} btn-sq">
                            <i class="fas fa-user-tie fa-3x"></i><br><br>Data Guru
                        </a>
                        <a href="{{ route('cbt-mapel') }}" class="btn btn-{{ request()->segment(3) === 'mapel' ? 'success' : 'info' }} btn-sq">
                            <i class="fas fa-book fa-3x"></i><br><br>Mapel
                        </a>
                        <a href="{{ route('cbt-soal') }}" class="btn btn-{{ request()->segment(3) === 'soal' ? 'success' : 'info' }}  btn-sq">
                            <i class="fas fa-list-ol fa-3x"></i><br><br>Soal
                        </a>
                        <a href="#" class="btn btn-info btn-sq">
                            <i class="fas fa-file-medical-alt fa-3x"></i><br><br>Hasil Ujian
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main> --}}
