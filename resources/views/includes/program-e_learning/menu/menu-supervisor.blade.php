<style>
    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/e_learning') }}" class="ripple">
                <span class="{{ empty(Request::segment(3)) ? 'color-color-scheme' : '' }}">
                    <i class="list-icon material-icons">network_check</i>
                    <span class="hide-menu">Dashboard</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/e_learning/kalendar') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'kalendar' ? 'color-color-scheme' : '' }}">
                    <i class="list-icon material-icons">perm_contact_calendar</i>
                    <span class="hide-menu">Kalender Akademik</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/e_learning/kalendar_rombel') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'kalendar_rombel' ? 'color-color-scheme' : '' }}">
                    <i class="list-icon material-icons">perm_contact_calendar</i>
                    <span class="hide-menu">Kalender Rombel</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('room-rombel') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'rombel' ? 'color-color-scheme' : '' }}">
                    <i class="list-icon material-icons">school</i>
                    <span class="hide-menu">Kelas</span>
                </span>
            </a>
        </li>
        <!-- <li>
            <a href="{{ route('auth.logout') }}" class="ripple">
                <i class="list-icon material-icons">power_settings_new</i> 
                <span class="hide-menu">Keluar</span>
            </a>
        </li> -->
    </ul>
</nav>
