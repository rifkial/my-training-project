<style>
    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

    @media only screen and (max-width: 600px) {
        .navbar-header {
            display: none;
        }
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/e_learning') }}" class="ripple">
                <span class="{{ empty(Request::segment(3)) ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-chart-line"></i>
                    <span class="hide-menu">Dashboard</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/e_learning/kalendar') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'kalendar' ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-calendar-alt"></i>
                    <span class="hide-menu">Kalender Akademik</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/e_learning/kalendar_rombel') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'kalendar_rombel' ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-volume-down"></i>
                    <span class="hide-menu">Pengumuman Kelas</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('room-rombel') }}" class="ripple">
                <span
                    class="{{ Request::segment(3) === 'rombel' || !empty(Request::segment(4)) ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-tv"></i>
                    <span class="hide-menu">Kelas</span>
                </span>
            </a>
        </li>
    </ul>
</nav>
