{{-- {{dd($tema)}} --}}
<style>
    .pace {
        display: none;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/e_learning') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'guru_pelajaran' || request()->segment(3) == 'kelas_siswa' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'tes' ? 'color-color-scheme' : '' }}"><i
                        class="fas fa-cogs"></i>
                    <span
                        class="hide-menu {{ Request::segment(2) === 'tes' ? 'color-color-scheme' : '' }}">Setting</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('e_learning-guru_pelajaran') }}"
                        class="{{ Request::segment(3) === 'guru_pelajaran' ? 'color-color-scheme' : '' }}">Mapel per
                        Kelas</a>
                    <a href="{{ route('e_learning-kelas_siswa') }}"
                        class="{{ Request::segment(3) === 'kelas_siswa' ? 'color-color-scheme' : '' }}">Siswa per
                        Kelas</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ Request::segment(2) === 'user' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}"><i
                        class="fas fa-users-cog"></i>
                    <span class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">Setting
                        User</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('user-wali_kelas') }}"
                        class="{{ Request::segment(3) === 'wali-kelas' ? 'color-color-scheme' : '' }}">Wali Kelas
                    </a>
                    <a href="{{ route('user-guru') }}"
                        class="{{ Request::segment(3) === 'guru' ? 'color-color-scheme' : '' }}">Guru
                    </a>
                    <a href="{{ route('user-siswa') }}"
                        class="{{ Request::segment(3) === 'siswa' ? 'color-color-scheme' : '' }}">Siswa
                    </a>
                    <a href="{{ route('user-supervisor') }}"
                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Supervisor
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(4) == 'room' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class=""><i class="fas fa-chalkboard-teacher"></i>
                    <span class="hide-menu">Learning</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('admin_learning-room') }}"
                        class="{{ Request::segment(4) === 'room' ? 'color-color-scheme' : '' }}">Data Room
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(4) == 'login' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_learning-login') }}" class="ripple">
                <i class="fas fa-chalkboard"></i>
                <span class="hide-menu">Masuk ke Program</span>
            </a>
        </li>

    </ul>
</nav>
