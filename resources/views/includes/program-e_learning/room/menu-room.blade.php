<style>
    span.room-menu {
        margin-left: 11px;
    }

    .list-group-item.active {
        z-index: 2;
        color: #fff;
        background-color: #51d2b7;
        border-color: #51d2b7;
    }


    .tampil {
        display: block;
    }

    .tidak_tampil {
        display: none;
    }

</style>
@if (session('role') == 'guru' && $room['status_kode'] == 2)
    <a href="javascript:void(0);" class="btn btn-block btn-success make_room aktif tampil my-2"
        data-id="{{ $room['id'] }}">
        <i class="fas fa-bullhorn"></i>
        <span> Buat Kelas</span>
    </a>
    <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/logout', Request::segment(4)) }}"
        class="btn btn-block btn-danger tidak_aktif none_room tidak_tampil my-2"
        onclick="return confirm('Apa kamu yakin ingin mengakhiri sessi?');">
        <i class="fas fa-power-off"></i>
        <span> Akhiri Sesi</span>
    </a>
@elseif(session('role') == 'guru' && $room['status_kode'] == 1)
    <a href="{{ url('program/e_learning/room/' . Request::segment(4) . '/logout', Request::segment(4)) }}"
        class="btn btn-block btn-danger tidak_aktif my-2"
        onclick="return confirm('Apa kamu yakin ingin mengakhiri sessi?');">
        <i class="fas fa-power-off"></i>
        <span> Akhiri Sesi</span>
    </a>
@endif
@if (session('role') == 'siswa' && $room['status_kode'] == 1)
    @if ($status['absensi'] != 'hadir')
        <div id="bungkus">
            <a href="javascript:void(0);" data-id="[{{ $room['id'] . ',' . $room['id_mapel'] }}]"
                class="btn btn-block btn-success absensi aktif my-2">
                <i class="fas fa-paste"></i>
                <span> Absensi Kehadiran</span>
            </a>
        </div>
    @endif
@endif
@if (session('role') == 'guru' && $room['link_virtual'] != null)
    <div id="virtual_video" class="row my-2">
        <div class="col-md-7" id="none_virtual">
            <a href="{{ $room['link_virtual'] }}" target="_blank" class="btn btn-block btn-facebook">
                <i class="fa fa-video-camera"></i>
                <span> Virtual Class</span>
            </a>
        </div>
        <div class="col-md-2 p-0 mr-1 ml-1">
            <a href="javascript:void(0);" data-id="{{ $room['id'] }}" class="btn btn-success editVirtual">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        <div class="col-md-2 p-0">
            <a href="javascript:void(0);" class="btn btn-danger btn-block deleteVirtual" data-id="{{ $room['id'] }}">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </div>
    <div id="tambah_virtual" style="display: none">
        <div class="row m-2">
            <div class="col-md-12 p-0">
                <a href="javascript:void(0);" class="btn btn-block btn-info tambahVirtual">
                    <i class="fa fa-video-camera"></i>
                    <span> Tambah Virtual Class</span>
                </a>
            </div>
        </div>
    </div>
@elseif(session('role') == 'guru' && $room['link_virtual'] == null)
    <div id="virtual_video" class="row my-2" style="display: none">
        <div class="col-md-7" id="none_virtual">
            <a href="{{ $room['link_virtual'] }}" target="_blank" class="btn btn-block btn-facebook">
                <i class="fa fa-video-camera"></i>
                <span> Virtual Class</span>
            </a>
        </div>
        <div class="col-md-2 mx-1 p-0">
            <a href="javascript:void(0);" data-id="{{ $room['id'] }}" class="btn btn-success editVirtual">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        <div class="col-md-2 p-0">
            <a href="javascript:void(0);" class="btn btn-danger btn-block deleteVirtual" data-id="{{ $room['id'] }}">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </div>
    <div id="tambah_virtual" class="my-2">
        <a href="javascript:void(0);" class="btn btn-block btn-info tambahVirtual">
            <i class="fa fa-video-camera"></i>
            <span> Tambah Virtual Class</span>
        </a>
    </div>
@endif

@if (session('role') == 'siswa' && $room['link_virtual'] != null)
    <div class="row my-2">
        <div class="col-md-12">
            <a href="{{ $room['link_virtual'] }}" target="_blank" class="btn btn-block btn-info">
                <i class="fa fa-video-camera"></i>
                <span> Virtual Class Dibuka</span>
            </a>
        </div>
    </div>
@elseif(session('role') == 'siswa' && $room['link_virtual'] == null)
    <div class="row my-2">
        <div class="col-md-12">
            <a class="btn btn-block btn-warning text-white" style="cursor: auto">
                <i class="fa fa-video-camera"></i>
                <span> Tidak ada Virtual Class</span>
            </a>
        </div>
    </div>
@endif



<div class="sidebar-widgets-wrap">
    <div class="widget widget_links clearfix">
        <div class="list-group">
            @php
                $active = '';
                if (Request::segment(3) == 'room') {
                    $active = 'active';
                }
            @endphp
            {{-- <a class="list-group-item list-group-item-action d-flex "
                href="{{ url('program/e_learning/room', Request::segment(4)) }}" title="Dashboard Kelas">
                <i class="list-icon material-icons">home</i>
                <span class="room-menu"> Beranda Kelas</span>
            </a> --}}
            <a class="list-group-item list-group-item-action d-flex "
                href="{{ url('program/e_learning/room', Request::segment(4)) }}" title="Dashboard Kelas">
                <i class="list-icon material-icons">home</i>
                <span class="room-menu"> Beranda Kelas</span>
            </a>
            {{-- <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'standar_kompetensi' ? 'active' : '' }}"
                href="{{ url('program/e_learning/room/' . Request::segment(4) . '/standar_kompetensi') }}"
                title="Standar Kompetensi (KI/KD)">
                <i class="list-icon material-icons">school</i>
                <span class="room-menu"> Standar Kompetensi (KI/KD)</span>
            </a> --}}
            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'bahan_ajar' ? 'active' : '' }}"
                href="{{ url('program/e_learning/room/' . Request::segment(4) . '/bahan_ajar') }}"
                title="Bahan Ajar">
                {{-- href="{{ route('room-bahan_ajar') }}" title="Bahan Ajar"> --}}
                <i class="list-icon material-icons">filter_none</i>
                <span class="room-menu"> Bahan Ajar</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'tugas_pengetahuan' ? 'active' : '' }}""
                {{-- href="{{ route('room-tugas_pengetahuan') }}" title="Penilaian Pengetahuan"> --}}
                href=" {{ url('program/e_learning/room/' . Request::segment(4) . '/tugas_pengetahuan') }}"
                title="Penilaian Pengetahuan">
                <i class="list-icon material-icons">laptop_windows</i>
                <span class="room-menu"> Tugas Pengetahuan</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'tugas_ketrampilan' ? 'active' : '' }}""
                {{-- href="{{ route('room-tugas_ketrampilan') }}" title="Penilaian Ketrampilan"> --}}
                href=" {{ url('program/e_learning/room/' . Request::segment(4) . '/tugas_ketrampilan') }}"
                title="Penilaian Ketrampilan">
                <i class="list-icon material-icons">laptop_windows</i>
                <span class="room-menu"> Tugas Ketrampilan</span>
            </a>
            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'data_siswa' ? 'active' : '' }}""
                href=" {{ url('program/e_learning/room/' . Request::segment(4) . '/data_siswa') }}"
                title="Data Siswa">
                <i class="list-icon material-icons">people</i>
                <span class="room-menu"> Data Siswa / Absensi</span>
            </a>

            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'jurnal_guru' ? 'active' : '' }}""
                href=" {{ url('program/e_learning/room/' . Request::segment(4) . '/jurnal_guru') }}"
                title="Jurnal Guru">
                <i class="list-icon material-icons">menu</i>
                <span class="room-menu"> Jurnal Guru</span>
            </a>

            <a class="list-group-item list-group-item-action d-flex {{ Request::segment(5) === 'kalendar_kelas' ? 'active' : '' }}""
                href=" {{ url('program/e_learning/room/' . Request::segment(4) . '/kalendar_kelas') }}"
                title="Penilaian Akhir Semester (PAS)">
                <i class="list-icon material-icons">perm_contact_calendar</i>
                <span class="room-menu"> Kalender Kelas</span>
            </a>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg-color-scheme" id="virtualModal" style="display: none">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="text-inverse">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" id="formUpdateVirtual">
                    <div class="row">
                        <div class="col-md-12" id="informasi">
                            <p class="mb-1">Petunjuk Penggunaan.</p>
                            <p><small>Anda bisa menambahkan link virtual video dari zoom, google meet dll di form bawah
                                    ini</small></p>
                        </div>
                        <div class="col-md-12">
                            <label for="" class="form-label mb-0">Contoh Pengisian Form</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" value="https://us02web.zoom.us/j/86235?pwd=Q0lxQyt" disabled
                                class="form-control">
                        </div>
                        <div class="col-md-12 mt-3">
                            <label for="" class="form-label mb-0">Masukan Link Virtual Video</label>
                        </div>
                        <input type="hidden" name="id_room" value="{{ $room['id'] }}">
                        <input type="hidden" name="params" value="update">
                        <div class="col-md-12">
                            <textarea name="link_virtual" id="link_virtual" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info ripple text-left" id="updtVirtual">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.tambahVirtual').click(function() {
            $('#formUpdateVirtual').trigger("reset");
            $('#virtualModal').modal('show');
        });

        $('#formUpdateVirtual').on('submit', function(event) {
            event.preventDefault();
            $("#updtVirtual").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#updtVirtual").attr("disabled", true);
            $.ajax({
                url: "{{ route('room-update_virtual') }}",
                method: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    // console.log(data)
                    if (data.status == 'berhasil') {
                        $('#formUpdateVirtual').trigger("reset");
                        $('#virtualModal').modal('hide');
                        $('#tambah_virtual').hide();
                        $('#virtual_video').show();
                    }
                    noti(data.icon, data.success);
                    $('#updtVirtual').html('Simpan');
                    $("#updtVirtual").attr("disabled", false);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#updtVirtual').html('Simpan');
                }
            });
        });

        $('.editVirtual').on('click', function() {
            let id = $(this).data('id');
            let loader = $(this);
            $.ajax({
                type: 'POST',
                url: "{{ route('room-get_by_id_room') }}",
                data: {
                    id
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(data) {
                    $(loader).html('<i class="fa fa-pencil"></i>');
                    $('#link_virtual').val(data.link_virtual);
                    $('#virtualModal').modal('show');
                }
            });
            // $('#ajaxModel').modal('show');
            // $('#link_virtual').val(linkV);
        })

        $('.absensi').on('click', function() {
            var input_id = $(this).data('id');
            var id_room = input_id[0];
            var id_mapel = input_id[1];
            $.ajax({
                type: 'POST',
                url: "{{ url('program/e_learning/room/' . Request::segment(4) . '/absensi_siswa') }}",
                data: {
                    id_room: id_room,
                    id_mapel: id_mapel,
                },
                beforeSend: function() {
                    $(".absensi").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                },
                success: function(data) {
                    $(".absensi").html(
                        '<i class="material-icons list-icon">power_settings_new</i><span> Absensi Kehadiran</span>'
                    );
                    $('#bungkus').hide();
                    noti(data.icon, data.message);
                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtns').html('Simpan');
                }
            });
        })
    });
    $(document).on('click', '.make_room', function() {
        let id = $(this).data('id');
        let loader = $(this);
        let konfirmasi = window.confirm("Apa kamu yakin ingin membuat room?");
        if (konfirmasi) {
            $.ajax({
                url: "{{ route('room-update') }}",
                method: "POST",
                data: {
                    id
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i> Membuat Room..');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        location.reload();
                    }
                    noti(data.icon, data.message);
                    $("#saveBtns").html('Simpan');

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtns').html('Simpan');
                }
            });
        }

    });

    $(document).on('click', '.deleteVirtual', function() {
        let id = $(this).data('id');
        let loader = $(this);
        swal({
            title: "Apa kamu yakin?",
            text: "ingin menghapus Link Virtual ini!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                url: "{{ route('room-update_virtual') }}",
                type: "POST",
                data: {
                    id_room: id,
                    params: 'delete'
                },
                beforeSend: function() {
                    $(loader).html(
                        '<i class="fa fa-spin fa-spinner"></i>');
                },
                success: function(data) {
                    if (data.status == 'berhasil') {
                        $('#tambah_virtual').show();
                        $('#virtual_video').hide();
                        $(loader).html(
                            '<i class="fa fa-trash"></i>');
                    }
                    swa(data.status + "!", data.success, data.icon);
                }
            })
        }, function(dismiss) {
            if (dismiss === 'cancel') {
                swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
            }
        })

    });
    //     $('#form_results').html('');
    //     $('#modelHeadings').html("Tambahkan data Pertemuan");
    //     $('#saveBtns').val("edit-user");
    //     $('#modal_pertemuan').modal('show');
    //     $('#id_rooms').val(id);
    //     $('#action_button').val('Edit');
    //     $('#action').val('Edit');
    // });

    $('#FormPertemuan').on('submit', function(event) {
        $('#saveBtns').html('Sending..');

        event.preventDefault();
        action_url = "{{ route('room-update') }}";
        method_url = "PUT";
        $.ajax({
            url: action_url,
            method: method_url,
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function() {

            },
            success: function(data) {
                if (data.status == 'berhasil') {
                    $('#modal_pertemuan').modal('hide');
                    location.reload();
                }
                noti(data.icon, data.success);
                $("#saveBtns").html('Simpan');

            },
            error: function(data) {
                console.log('Error:', data);
                $('#saveBtns').html('Simpan');
            }
        });
    });
</script>
