<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<style>
    .pace {
        display: none;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/' . Request::segment(2)) }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        {{-- <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style> --}}
        <li>
            <a href="{{ route('point_pelanggaran_siswa-beranda') }}"
                class="{{ request()->segment(3) === 'siswa' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-keyboard"></i>
                <span class="hide-menu">Input Pelanggaran</span>
            </a>
        </li>
        {{-- <li class="current-page">
            <a href="javascript:void(0);">
                <i class="fas fa-file-signature"></i>
                <span class="hide-menu">Pelanggaran
                    <span class="badge badge-border bg-primary pull-right">5</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('point_pelanggaran_siswa-beranda') }}">Input Pelanggaran</a>
                </li>
                <li>
                    <a href="{{ route('kesiswaan_histori-beranda') }}">History Pelanggaran</a>
                </li>
                <li>
                    <a href="{{ route('kesiswaan_sanksi-beranda') }}">Sanksi Pelanggaran</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('kesiswaan-program_kerja') }}"
                class="{{ request()->segment(3) === 'program_kerja' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-briefcase"></i>
                <span class="hide-menu">Program Kerja</span>
            </a>
        </li>
        <li>
            <a href="{{ route('kesiswaan-anggota_ekskul') }}"
                class="{{ request()->segment(3) === 'wali-kelas' ? 'color-color-scheme' : '' }}">
                <i class="fas fa-skating"></i>
                <span class="hide-menu">Ekstrakurikuler</span>
            </a>
        </li> --}}
    </ul>
</nav>
