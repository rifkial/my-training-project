<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<style>
    .pace {
        display: none;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/kesiswaan') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
            <a href="{{ route('kesiswaan_siswa-beranda') }}">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Data Siswa / Absensi</span>
            </a>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'sanksi' || request()->segment(3) == 'kategori_pelanggaran' || request()->segment(3) == 'histori' ? 'current-page active' : '' }}"><a href="javascript:void(0);"><i
                    class="fas fa-laptop-medical"></i> <span class="hide-menu">Pelanggaran</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('kesiswaan_ketegori_pelanggaran-beranda') }}">Kategori Pelanggaran</a>
                </li>
                <li><a href="{{ route('kesiswaan_sanksi-beranda') }}">Sanksi Pelanggaran</a>
                </li>
                <li><a href="{{ route('kesiswaan_histori-beranda') }}">History Pelanggaran</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(3) == 'program_kerja' ? 'current-page active' : '' }}">
            <a href="{{ route('kesiswaan-program_kerja') }}">
                <i class="fas fa-file-signature"></i>
                <span class="hide-menu">Program Kerja</span>
            </a>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'ekstrakurikuler' ? 'current-page active' : '' }}"><a href="javascript:void(0);"><i
                    class="fas fa-volleyball-ball"></i> <span class="hide-menu">Ekstrakurikuler</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('master-kategori_ekstrakurikuler') }}">Data Ekstrakurikuler</a>
                </li>
                <li><a href="{{ route('kesiswaan-anggota_ekskul') }}">Jumlah Ekstrakurikuler</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
