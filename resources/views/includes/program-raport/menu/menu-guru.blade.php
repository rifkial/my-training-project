{{-- {{dd($tema)}} --}}


<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="fas fa-chart-line"></i> 
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li>
            <a href="{{ route('raport-mapel') }}" class="ripple">
                <i class="fas fa-atlas"></i> 
                <span class="hide-menu">Mapel Diampu</span>
            </a>
        </li>
        <li>
            <a href="{{ route('raport-riwayat_mengajar') }}" class="ripple">
                <i class="fas fa-history"></i> 
                <span class="hide-menu">Riwayat Mengajar</span>
            </a>
        </li>
    </ul>
</nav>
