<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
@if (Session::has('message'))
<script>
    swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
        '{{ session('message')['icon'] }}');
</script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="list-icon material-icons">network_check</i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        {{-- <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style> --}}
        <li>
            <a href="{{ route('raport-cetak_raport') }}" class="ripple">
                <i class="list-icon material-icons">local_library</i>
                <span class="hide-menu">Lihat Raport</span>
            </a>
        </li>
    </ul>
</nav>
