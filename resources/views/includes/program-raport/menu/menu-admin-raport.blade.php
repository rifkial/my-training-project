<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/raport') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'sikap_sosial' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-sikap_sosial') }}" class="ripple">
                <i class="fas fa-user-friends"></i>
                <span class="hide-menu">Sikap Sosial</span>
            </a>
        </li>
        <li class="{{ request()->segment(4)== 'sikap_spiritual' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-sikap_spiritual') }}" class="ripple">
                <i class="fas fa-praying-hands"></i>
                <span class="hide-menu">Sikap Spiritual</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-predikat') }}" class="ripple">
                <i class="fas fa-star"></i>
                <span class="hide-menu">Predikat</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-setting_nilai') }}" class="ripple">
                <i class="fas fa-tools"></i>
                <span class="hide-menu">Settingan Nilai</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-config') }}" class="ripple">
                <i class="fas fa-cogs"></i>
                <span class="hide-menu">Config</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-kop_sampul') }}" class="ripple">
                <i class="fas fa-envelope-open-text"></i>
                <span class="hide-menu">Kop Sampul</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('raport-template_sampul') }}" class="ripple">
                <i class="fas fa-envelope-square"></i>
                <span class="hide-menu">Tamplate Sampul</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'raport_predikat' ? 'current-page active' : '' }}">
            <a href="{{ route('admin_raport-beranda') }}" class="ripple">
                <i class="fas fa-user-cog"></i>
                <span class="hide-menu">Admin Raport</span>
            </a>
        </li>
    </ul>
</nav>
