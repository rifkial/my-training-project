{{-- {{dd($tema)}} --}}
<style>
    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .sidebar-dark .site-sidebar {
        background: #000000 !important;
        border-color: rgba(255, 255, 255, 0.2);
    }

    a.dropdown-toggle.ripple:hover,
    a.right-sidebar-toggle.ripple:hover,
    a.ripple:hover,
    a.ripple:focus {
        background: transparent !important;
    }
    .side-menu>li.current-page>a {
        border-color: #ffffff !important;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #463939;
    }

</style>


<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/point') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>
        <li class="{{ Request::segment(4) === 'pelanggaran-sering' ? 'current-page active' : '' }}">
            <a href="#" class="ripple">
            <a href="{{ route('point_siswa-sering_dilakukan') }}" class="ripple">
                <i class="fas fa-exchange-alt"></i>
                <span class="hide-menu">Sering dilakukan</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'pesan' ? 'current-page active' : '' }}">
            <a href="{{ url('/program/point/pesan/inbox') }}" class="ripple">
                <i class="fas fa-comment-alt"></i>
                <span class="hide-menu">Pesan</span>
            </a>
        </li>
    </ul>
</nav>
