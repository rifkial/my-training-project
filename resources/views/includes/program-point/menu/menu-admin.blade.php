{{-- {{dd($tema)}} --}}


<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">network_check</i> <span class="hide-menu">Dashboard <span
                        class="badge badge-border badge-border-inverted bg-primary pull-right">5</span></span></a>
            <ul class="list-unstyled sub-menu">
                @foreach ($tema as $item)
                    <li><a href="{{ url('master/program_template/update', $item['id']) }}">{{ $item['nama'] }}</a>
                    </li>
                @endforeach
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><span
                    class="color-color-scheme"><i class="list-icon material-icons">mail_outline</i> <span
                        class="hide-menu">Master</span></span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="{{ route('jurusan') }}">Jurusan</a>
                </li>
                <li><a href="{{ route('kelas') }}">Kelas</a>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);">Inbox</a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="app-inbox.html">Inbox</a>
                        </li>
                        <li><a href="app-inbox-single.html">Inbox single</a>
                        </li>
                        <li><a href="app-inbox-compose.html">Compose mail</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);">Contacts</a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="app-contacts.html">Contacts List</a>
                        </li>
                        <li><a href="app-contacts-alt.html">Contacts List Alt</a>
                        </li>
                        <li><a href="app-contacts-details.html">Contact Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Tables</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="tables-basic.html">Basic Tables</a>
                </li>
                <li><a href="tables-data-table.html">Data Table</a>
                </li>
                <li><a href="tables-bootstrap.html">Bootstrap Tables</a>
                </li>
                <li><a href="tables-responsive.html">Responsive Tables</a>
                </li>
                <li><a href="tables-editable.html">Editable Tables</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">lightbulb_outline</i> <span class="hide-menu">UI Elements <span
                        class="badge badge-border badge-border-inverted bg-primary pull-right">9</span></span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="ui-typography.html">Typography</a>
                </li>
                <li><a href="ui-buttons.html">Buttons</a>
                </li>
                <li><a href="ui-cards.html">Cards</a>
                </li>
                <li><a href="ui-tabs.html">Tabs</a>
                </li>
                <li><a href="ui-panels.html">Panels</a>
                </li>
                <li><a href="ui-accordions.html">Accordions</a>
                </li>
                <li><a href="ui-modals.html">Modals</a>
                </li>
                <li><a href="ui-icon-boxes.html">Icon Boxes</a>
                </li>
                <li><a href="ui-lists.html">Lists &amp; Media Object</a>
                </li>
                <li><a href="ui-user-cards.html">User Cards</a>
                </li>
                <li><a href="ui-grid.html">Grid</a>
                </li>
                <li><a href="ui-progress.html">Progress Bars</a>
                </li>
                <li><a href="ui-notifications.html">Notifications &amp; Alerts</a>
                </li>
                <li><a href="ui-pagination.html">Pagination</a>
                </li>
                <li><a href="ui-media.html">Media</a>
                </li>
                <li><a href="ui-carousel.html">Carousel</a>
                </li>
                <li><a href="ui-bootstrap.html">Bootstrap Elements</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">developer_board</i> <span class="hide-menu">Forms</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="form-elements.html">Elements</a>
                </li>
                <li><a href="form-material.html">Material Design</a>
                </li>
                <li><a href="form-validation.html">Form Validation</a>
                </li>
                <li><a href="form-dropzone.html">File Upload</a>
                </li>
                <li><a href="form-pickers.html">Picker</a>
                </li>
                <li><a href="form-select.html">Select and Multiselect</a>
                </li>
                <li><a href="form-tags-categories.html">Tags and Categories</a>
                </li>
                <li><a href="form-addons.html">Addons</a>
                </li>
                <li><a href="form-editors.html">Editors</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">tune</i> <span class="hide-menu">Charts</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="charts-flot.html">Flot Charts</a>
                </li>
                <li><a href="charts-morris.html">Morris Charts</a>
                </li>
                <li><a href="charts-js.html">Chart-js</a>
                </li>
                <li><a href="charts-sparkline.html">Sparkline Charts</a>
                </li>
                <li><a href="charts-knob.html">Knob Charts</a>
                </li>
            </ul>
        </li>
        <li class="list-divider"></li>
        <li><a href="#" class="ripple"><i class="list-icon material-icons">notifications_none</i> <span
                    class="hide-menu">Updates</span></a>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">widgets</i> <span class="hide-menu">Widgets</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="widgets.html">Content Widgets</a>
                </li>
                <li><a href="widgets-statistics.html">Statistics Widgets</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">add_location</i> <span class="hide-menu">Maps</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="maps-google.html">Google Maps</a>
                </li>
                <li><a href="maps-vector.html">Vector Maps</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">lightbulb_outline</i> <span class="hide-menu">Icons</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="icons-material-design.html">Material Design</a>
                </li>
                <li><a href="icons-font-awesome.html">Font Awesome</a>
                </li>
                <li><a href="icons-mono-social.html">Social Icons</a>
                </li>
                <li><a href="icons-weather.html">Weather Icons</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">folder_open</i> <span class="hide-menu">Sample Pages <span
                        class="badge badge-border badge-border-inverted bg-info pull-right">3</span></span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="page-blank.html">Blank Page</a>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);">Email Templates</a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="email-templates/basic.html">Basic</a>
                        </li>
                        <li><a href="email-templates/billing.html">Billing</a>
                        </li>
                        <li><a href="email-templates/friend-request.html">Friend Request</a>
                        </li>
                    </ul>
                </li>
                <li><a href="page-lightbox.html">Lightbox Popup</a>
                </li>
                <li><a href="page-sitemap.html">Sitemap</a>
                </li>
                <li><a href="page-search-results.html">Search Results</a>
                </li>
                <li><a href="page-custom-scroll.html">Custom Scroll</a>
                </li>
                <li><a href="page-utility-classes.html">Utility Classes</a>
                </li>
                <li><a href="page-animations.html">Animations</a>
                </li>
                <li><a href="page-faq.html">FAQ</a>
                </li>
                <li><a href="page-pricing-table.html">Pricing</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"><i
                    class="list-icon material-icons">card_travel</i> <span class="hide-menu">Profile Pages</span></a>
            <ul class="list-unstyled sub-menu">
                <li><a href="page-profile.html">Profile</a>
                </li>
                <li><a href="page-login.html">Login Page</a>
                </li>
                <li><a href="page-login2.html">Login Page 2</a>
                </li>
                <li><a href="page-register.html">Sign Up</a>
                </li>
                <li><a href="page-register2.html">Sign Up 2</a>
                </li>
                <li><a href="page-register-3-step.html">3 Step Sign Up</a>
                </li>
                <li><a href="page-forgot-pwd.html">Forgot Password</a>
                </li>
                <li><a href="page-email-confirm.html">Confirm Email</a>
                </li>
                <li><a href="page-lock-screen.html">Lock Screen</a>
                </li>
                <li><a href="page-timeline.html">Timeline</a>
                </li>
                <li class="menu-item-has-children"><a href="javascript:void(0);">Error Pages</a>
                    <ul class="list-unstyled sub-menu">
                        <li><a href="page-error-403.html">Error 403</a>
                        </li>
                        <li><a href="page-error-404.html">Error 404</a>
                        </li>
                        <li><a href="page-error-500.html">Error 500</a>
                        </li>
                        <li><a href="page-error-503.html">Error 503</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="page-login.html"><i class="list-icon material-icons">settings_power</i> <span class="hide-menu">Log
                    Out</span></a>
        </li>
    </ul>
    <!-- /.side-menu -->
</nav>
