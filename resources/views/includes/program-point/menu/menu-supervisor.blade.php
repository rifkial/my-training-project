
<style>
    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .sidebar-dark .site-sidebar {
        background: #000000 !important;
        border-color: rgba(255, 255, 255, 0.2);
    }


    i.material-icons.list-icon {
        color: #fff !important;
    }

    a.dropdown-toggle.ripple:hover,
    a.right-sidebar-toggle.ripple:hover,
    a.ripple:hover,
    a.ripple:focus {
        background: transparent !important;
    }

    .side-menu>li.current-page>a {
        border-color: #ffffff !important;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #463939;
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

</style>


<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/point') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>

        <li class="{{ Request::segment(3) === 'siswa' ? 'current-page active' : '' }}">
            <a href="{{ route('point-siswa') }}" class="ripple">
                <i class="fas fa-users"></i>
                <span class="hide-menu">Siswa</span>
            </a>
        </li>
        <li class="{{ Request::segment(3) === 'wali_murid' ? 'current-page active' : '' }}">
            <a href="{{ route('point_wali_murid-beranda') }}" class="ripple">
                <i class="fas fa-house-user"></i>
                <span class="hide-menu">Wali Murid</span>
            </a>
        </li>
    </ul>
</nav>
