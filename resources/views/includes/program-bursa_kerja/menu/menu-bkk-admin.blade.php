{{-- {{dd($tema)}} --}}
<style>
    .pace {
        display: none;
    }


    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

    .sidebar-dark .site-sidebar {
        background: #02306b;
        border-color: rgba(255, 255, 255, 0.2);
    }

    a.navbar-brand {
        background: #02306b !important;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #0047a2 !important;
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createData {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }


    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

</style>
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(request()->segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/bursa_kerja') }}" class="ripple">
                <i class="fas fa-chart-line"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li
            class="{{ request()->segment(3) == 'user-bkk' && empty(request()->segment(4)) ? 'current-page active' : '' }}">
            <a href="{{ route('user_bkk-beranda') }}" class="ripple">
                <i class="fas fa-user-tag"></i>
                <span class="hide-menu">Data Pelamar</span>
            </a>
        </li>
        <li class="{{ request()->segment(4) == 'perusahaan' ? 'current-page active' : '' }}">
            <a href="{{ route('user_bkk-perusahaan') }}" class="ripple">
                <i class="fas fa-user-tie"></i>
                <span class="hide-menu">Manajemen Mitra</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'industri' ? 'current-page active' : '' }}">
            <a href="{{ route('bkk_industri-beranda') }}" class="ripple">
                <i class="fas fa-city"></i>
                <span class="hide-menu">Mitra Industri</span>
            </a>
        </li>
        <li
            class="menu-item-has-children {{ request()->segment(3) == 'bidang_loker' || request()->segment(3) == 'lowongan' || request()->segment(3) == 'agenda' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i class="fas fa-suitcase-rolling"></i>
                    <span class="hide-menu">Bursa
                        Kerja</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('bkk_bidang_loker-beranda') }}">Bidang Loker</a>
                </li>
                <li>
                    <a href="{{ route('lowongan-create') }}">Input Loker</a>
                </li>
                <li>
                    <a href="{{ route('lowongan-beranda') }}">List Loker</a>
                </li>
                <li>
                    <a href="{{ route('bkk_agenda-beranda') }}">Jadwal Recruitment </a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->segment(3) == 'alumni' ? 'current-page active' : '' }}">
            <a href="{{ route('user-alumni') }}" class="ripple">
                <i class="fas fa-user-graduate"></i>
                <span class="hide-menu">Sebaran Alumni</span>
            </a>
        </li>
        <li class="{{ request()->segment(3) == 'profile-sekolah' ? 'current-page active' : '' }}">
            <a href="{{ route('bkk_profile-beranda') }}" class="ripple">
                <i class="fas fa-cogs"></i>
                <span class="hide-menu">Setting Tampilan</span>
            </a>
        </li>

    </ul>
</nav>
