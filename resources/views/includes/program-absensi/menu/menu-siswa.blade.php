<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/absensi') }}" class="ripple">
                <span class="{{ empty(Request::segment(3)) ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-tachometer-alt"></i>
                    <span class="hide-menu">Dashboard</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('absensi-profile') }}" class="ripple">
                <span class="{{ Request::segment(3) === 'kalendar' ? 'color-color-scheme' : '' }}">
                    <i class="fas fa-user-check"></i>
                    <span class="hide-menu">Absen</span>
                </span>
            </a>
        </li>
        <li>
            <a href="{{ route('absensi-profile_izin') }}" class="ripple">
                <span class="">
                    <i class="fas fa-notes-medical"></i>
                    <span class="hide-menu">Izin</span>
                </span>
            </a>
        </li>
    </ul>
</nav>
