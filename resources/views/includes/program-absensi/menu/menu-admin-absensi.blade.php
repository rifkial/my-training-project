<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/absensi') }}" class="ripple">
                <i class="fas fa-laptop-house"></i>
                <span class="hide-menu">Beranda</span>
            </a>
        </li>
        <li class="list-divider"></li>
        {{-- <li class="menu-item-has-children">
            <a href="javascript:void(0);">
                <i class="fas fa-bars"></i> <span class="hide-menu">Kelola Menu</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('absensi_menu-akses') }}">Akses User</a>
                </li>
                <li>
                    <a href="{{ route('alumni-jurusan') }}">Menu</a>
                </li>
                <li>
                    <a href="{{ route('admin_menu-submenu') }}">Sub Menu</a>
                </li>
            </ul>
        </li> --}}
        <li class="menu-item-has-children {{ request()->segment(3) == 'libur' || request()->segment(3) == 'jadwal' || request()->segment(3) == 'hadir' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-file-signature"></i> <span class="hide-menu">Kelola Absensi</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'libur' ? 'current-page active' : '' }}">
                    <a href="{{ route('absensi-libur') }}">Atur Libur</a>
                </li>
                <li>
                    <a href="{{ route('absensi-jadwal') }}">Settingan Jadwal</a>
                </li>
                <li>
                    <a href="{{ route('absensi-hadir') }}">Rekap Absen</a>
                </li>
                <li>
                    <a href="{{ route('absensi-izin') }}">Jam Absen</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'guru' || request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-users-cog"></i> <span class="hide-menu">User</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('absensi-siswa') }}">Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('absensi-guru') }}">Guru</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'izin' ? 'current-page active' : '' }}">
            <a href="javascript:void(0);">
                <i class="fas fa-laptop-medical"></i> <span class="hide-menu">Izin</span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="{{ request()->segment(3) == 'siswa' ? 'current-page active' : '' }}">
                    <a href="{{ route('absensi-izin') }}">Izin Siswa</a>
                </li>
                <li class="{{ request()->segment(3) == 'guru' ? 'current-page active' : '' }}">
                    <a href="{{ route('absensi-izin_guru') }}">Izin Guru</a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
