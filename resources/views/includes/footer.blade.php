@if (session('role') != 'superadmin')
    <footer class="footer text-center clearfix">
        {{ !empty(Session::get('footer')) ? Session::get('footer') : '2021 © ' . strtoupper($sekolah['nama']) }}
    </footer>
@else
    <footer class="footer text-center clearfix">2021 © Tim Dev Bumitekno</footer>
@endif
