<style>
    .navbar,
    .navbar-brand {
        background: #50b0e0;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info{
        color: #fff !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/sistem_pkl') }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>

        <li>
            <a href="{{ route('pkl_kelompok-list_data') }}" class="ripple">
                <i class="fas fa-users-cog"></i>
                <span class="hide-menu">Kelompok Prakerin</span>
            </a>
        </li>
        {{-- <li>
            <a href="{{ route('pkl_siswa-beranda') }}" class="ripple">
                <i class="fas fa-users-cog"></i>
                <span class="hide-menu">Peserta PKL</span>
            </a>
        </li> --}}
        <li>
            <a href="{{ route('pkl_nilai-beranda') }}" class="ripple">
                <i class="fas fa-users-cog"></i>
                <span class="hide-menu">Nilai PKL</span>
            </a>
        </li>
    </ul>
</nav>
