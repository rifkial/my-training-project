{{-- {{dd($tema)}} --}}
<style>
    .navbar,
    .navbar-brand {
        background: #50b0e0;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: left;
        }
    }
    span.user-type.fs-12.roles.text-info{
        color: #fff !important;
    }

</style>

<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="{{ empty(Request::segment(3)) ? 'current-page active' : '' }}">
            <a href="{{ url('program/sistem_pkl') }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'industri' || request()->segment(3) == 'pembimbing_industri' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i
                        class="fa fa-industry"></i>
                    <span
                        class="hide-menu">Industri
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_industri-beranda') }}"
                        class="{{ Request::segment(3) === 'tahun_ajaran' ? 'color-color-scheme' : '' }}">Perusahaan</a>
                    <a href="{{ route('pkl_pembimbing_industri-beranda') }}"
                        class="{{ Request::segment(3) === 'jurusan' ? 'color-color-scheme' : '' }}">Pembimbing
                        Industri</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'kelompok' || request()->segment(3) == 'peserta' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i
                        class="fa fa-tools"></i>
                    <span class="hide-menu">PKL
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_kelompok-list_data') }}" >Kelompok  PKL</a>
                    {{-- <a href="{{ route('pkl_siswa-beranda') }}">Peserta PKL</a> --}}
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'jenis_nilai' || request()->segment(3) == 'nilai' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i class="fa fa-pencil-ruler"></i>
                    <span class="hide-menu">Nilai
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_jenis_nilai-beranda') }}">Jenis
                        Nilai</a>
                    <a href="{{ route('pkl_nilai-beranda') }}">Input Nilai</a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ request()->segment(3) == 'informasi' || request()->segment(3) == 'pedoman' || request()->segment(3) == 'slider' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span><i class="fa fa-cogs"></i>
                    <span
                        class="hide-menu">Setting
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('pkl_informasi-beranda') }}">Informasi</a>
                    <a href="{{ route('pkl_pedoman-beranda') }}">Pedoman</a>
                    <a href="{{ route('pkl_slider-beranda') }}">Slider</a>
                    <a href="{{ route('pkl_config-beranda') }}">Tampilan Pengunjung</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
