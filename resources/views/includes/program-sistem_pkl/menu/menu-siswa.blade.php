<style>
    #loading {
        background: url("{{ asset('asset/img/loader.gif') }}") no-repeat center;
        height: 300px;
        width: 300px;
        display: block;
        margin: auto;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        text-align: center;
        top: 66px;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    .navbar,
    .navbar-brand {
        background: #50b0e0;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    span.user-type.fs-12.roles.text-info {
        color: #fff !important;
    }

</style>
@if (Session::has('message'))
    <script>
        swal('{{ session('message')['status'] }}!', '{{ session('message')['message'] }}',
            '{{ session('message')['icon'] }}');
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/sistem_pkl') }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <li>
            <a href="{{ route('pkl_tugas-beranda') }}" class="ripple">
                <i class="fa fa-tasks"></i>
                <span class="hide-menu">Tugas/Kegiatan</span>
            </a>
        </li>
        <li>
            <a href="{{ url('program/sistem_pkl/nilai_siswa/jenis', $id_code) }}" class="ripple">
                <i class="fa fa-trophy"></i>
                <span class="hide-menu">Nilai PKL</span>
            </a>
        </li>
    </ul>
</nav>
