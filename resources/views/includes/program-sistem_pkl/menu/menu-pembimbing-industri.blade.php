<style>
    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    .navbar,
    .navbar-brand {
        background: #50b0e0;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }


</style>
@if (Session::has('error_api'))
    <script>
        swal({
            title: 'Gagal!',
            text: "{{ session('error_api')['message'] }}",
            timer: 5000,
            type: "{{ session('error_api')['icon'] }}"
        }).then((value) => {
        }).catch(swal.noop);

    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        <li class="current-page">
            <a href="{{ url('program/' . Request::segment(2)) }}" class="ripple">
                <i class="fa fa-tachometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="list-divider"></li>
        <style>
            .sidebar-horizontal .side-menu>li>a {
                padding-top: 0px;
            }

        </style>
        <li>
            <a href="{{ route('pkl_tugas-beranda') }}" class="ripple">
                <i class="fa fa-tasks"></i>
                <span class="hide-menu">Kegiatan Siswa</span>
            </a>
        </li>
        <li>
            <a href="{{ route('pkl_nilai_siswa-beranda') }}" class="ripple">
                <i class="fa fa-trophy"></i>
                <span class="hide-menu">Nilai Prakerin</span>
            </a>
        </li>
        <li>
            <a href="{{ route('pkl_setting-beranda') }}" class="ripple">
                <i class="fa fa-cog"></i>
                <span class="hide-menu">Setting Industri</span>
            </a>
        </li>
    </ul>
</nav>
