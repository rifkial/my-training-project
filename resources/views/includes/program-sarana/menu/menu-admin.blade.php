<style>

    

    .header-light .navbar {
        background: #7fb1f1 !important;
    }

    .navbar-brand{
        background-color: #7fb1f1 !important;
    }

    span.badge {
        text-transform: uppercase;
        letter-spacing: 0.19048em;
        border-radius: 1.42857em;
        padding: 0.47619em 1.42857em;
    }

    .pagination>li {
        display: inline;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
        float: right;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    #radioBtn .notActive {
        color: #3276b1;
        background-color: #fff;
        border: 1px solid #fff;
    }

    .sidebar-dark .side-user .side-menu li a {
        color: #fff !important;
    }

    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    button.btn.btn-danger.btn-sm,
    a.btn.btn-success.btn-sm {
        margin-top: 3px;
    }

    .color-color-scheme{
        color: #ffffff !important;
    }

    .sidebar-dark .site-sidebar {
        background: #2471d2;
        border-right: 1px solid #ddd;
        overflow-y: auto;
        height: 100vh;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active,
    .sidebar-dark .side-menu li.act {
        background: #6ea1e0;
        width: 100%;
    }

    .sidebar-dark .side-menu li a:hover {
        color: #fff;
    }

    span.hide-menu {
        color: #fff;
    }

    .sidebar-dark .side-menu li a {
        color: #d6d6d6;
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #e6e6e6;
    }

    .sidebar-dark .side-menu li.active>a {
        color: #a8a0a0;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    /*SIDEBAR*/

    .sidebar-expand .main-wrapper {
        margin-left: 18.375rem !important;
    }


    .sidebar-expand .site-sidebar {
        width: 18.375rem !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }

    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

</style>

<nav class="sidebar-nav">
    <ul class="nav in side-menu">
        
        <li class="{{Request::segment(3) == '' ? 'active act' : ''}}">
            <a href="{{ url('program/sarpras') }}" class="ripple">
                <i class="fa fa-home fa-lg mr-2" aria-hidden="true"></i>
                <span class="hide-menu">DASHBOARD</span>
            </a>
        </li>

        <li class="menu-item-has-children {{Request::segment(3) == 'barang' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-briefcase fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">BARANG
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('daftar-barang')}}"
                        class="{{Request::segment(3) == 'barang' ? 'color-color-scheme' : ''}}">Data Barang
                    </a>
                    <a href="{{route('daftar-item')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Data Item
                    </a>
                    <a href="{{route('daftar-barang-cetak')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Cetak Barcode
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-exchange fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">PEMINJAMAN
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('admin-page-peminjaman')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Peminjaman berjalan
                    </a>
                    <a href="{{route('expired-peminjaman')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">peminjaman expired
                    </a>
                    <a href="{{route('riwayat-peminjaman')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Riwayat peminjaman
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-plus-square fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">PENGADAAN
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('daftar-pengadaan')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Usulan Pengadaan 
                    </a>
                    <a href="{{route('approved-pengadaan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pengadaan Diterima
                    </a>
                    <a href="{{route('rejected-pengadaan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pengadaan Ditolak
                    </a>
                    <a href="{{route('riwayat-pengadaan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Riwayat Pengadaan
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-bitbucket fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">PEMUSNAHAN
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('daftar-pemusnahan')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Usulan Pemusnahan
                    </a>
                    <a href="{{route('approved-pemusnahan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Pemusnahan Diterima
                    </a>
                    <a href="{{route('riwayat-pemusnahan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Riwayat Pemusnahan
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children ">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-file-text fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">LAPORAN
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('daftar-laporan-barang')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Laporan Data Barang
                    </a>
                    <a href="{{route('daftar-laporan-item')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Data Item Barng
                    </a>
                    <a href="{{route('daftar-laporan-user')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan User
                    </a>
                    <a href="{{route('daftar-laporan-item-by-lokasi')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Laporan Item By Lokasi
                    </a>
                    <a href="{{route('daftar-laporan-item-by-kondisi')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Item By Kondisi
                    </a>
                    <a href="{{route('daftar-laporan-peminjaman')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Peminjaman
                    </a>
                    <a href="{{route('daftar-laporan-peminjaman-dipinjam')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Laporan Peminjaman Belum Kembali
                    </a>
                    <a href="{{route('daftar-laporan-pengadaan')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Usulan Pengadaan
                    </a>
                    <a href="{{route('daftar-laporan-pengadaan-diterima')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Pengadaan Diterima
                    </a>
                    <a href="{{route('daftar-laporan-pemusnahan')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Laporan Usulan Pemusnahan
                    </a>
                    <a href="{{route('daftar-laporan-pemusnahan-diterima')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Laporan Pemusnahan Diterima
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children {{Request::segment(3) == 'master' ? 'active act' : ''}}">
            <a href="javascript:void(0);" class="ripple">
                <span class="">
                    <i class="fa fa-th-large fa-lg mr-2" aria-hidden="true"></i>
                    <span class="hide-menu ">MASTER
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('main-kategori-barang')}}"
                        class="{{Request::segment(4) == 'jenjang' ? 'color-color-scheme' : ''}}">Kategori Barang
                    </a>
                    <a href="{{route('main-jenis-barang')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Jenis Barang
                    </a>
                    <a href="{{route('main-satuan-barang')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Satuan
                    </a>
                    <a href="{{route('main-lokasi-barang')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Lokasi
                    </a>
                    <a href="{{route('main-supllyer-barang')}}"
                        class="{{Request::segment(4) == 'fakultas' ? 'color-color-scheme' : ''}}">Supllyer
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{Request::segment(3) == 'dokumen' ? 'color-color-scheme' : ''}}"> 
                <i class="fa fa-user-circle-o fa-lg mr-2" aria-hidden="true"></i>
                <span class="hide-menu">USER</span>              
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{route('main-admin-barang')}}"
                        class="{{Request::segment(4) == 'publish' ? 'color-color-scheme' : ''}}">Admin
                    </a>
                    <a href="{{route('main-user-barang')}}"
                        class="{{Request::segment(4) == 'unpublish' ? 'color-color-scheme' : ''}}">User
                    </a>
                </li>
            </ul>
        </li>


        

        

    </ul>
</nav>