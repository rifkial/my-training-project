<!-- * welcome notification -->

<!-- ///////////// Js Files ////////////////////  -->
<!-- Jquery -->
{{-- <script src="{{asset('asset/mobile/js/lib/jquery-3.4.1.min.js')}}"></script> --}}
<!-- Bootstrap-->
<script src="{{ asset('asset/mobile/js/lib/popper.min.js') }}"></script>
<script src="{{ asset('asset/mobile/js/lib/bootstrap.min.js') }}"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Owl Carousel -->
<script src="{{ asset('asset/mobile/js/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- jQuery Circle Progress -->
<script src="{{ asset('asset/mobile/js/plugins/jquery-circle-progress/circle-progress.min.js') }}"></script>
<!-- Base Js File -->
<script src="{{ asset('asset/mobile/js/base.js') }}"></script>
<link rel="stylesheet" href="{{ asset('asset/mobile/css/style.css') }}">

<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css" rel="stylesheet"
    type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>

<script>
    setTimeout(() => {
        notification('notification-welcome', 5000);
    }, 2000);

    function noti(tipe, value) {
        $.toast({
            icon: tipe,
            text: value,
            hideAfter: 5000,
            showConfirmButton: true,
            position: 'top-right',
        });
        return true;
    }
</script>
