<style>
    .navbar-brand,
    .navbar {
        background: #03a9f3;
    }

    .color-color-scheme,
    .text-color-scheme {
        color: #03a9f3 !important;
    }

    h6.media-heading.mr-b-5.text-uppercase {
        color: #fff !important;
    }

    .btn-primary:active,
    .btn-primary.active,
    .show>.btn-primary.dropdown-toggle {
        background-color: #387ade;
        background-image: none;
        border-color: #3675d6;
        color: #fff !important;
        -webkit-box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
        box-shadow: inset 0 3px 5px rgb(0 0 0 / 13%);
    }

    #radioBtn .notActive {
        color: #3276b1;
        background-color: #fff;
        border: 1px solid #fff;
    }

    span.hide-menu {
        margin-left: 0px !important;
    }

    @media (min-width: 961px) {
        .sidebar-horizontal.header-centered .side-menu {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: flex-end;
        }
    }

    .dataTables_wrapper .dataTables_paginate {
        margin-bottom: 11px;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        background: #067d10 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-pdf.buttons-html5 {
        background: #b70000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-collection.buttons-colvis {
        background: #d46200 !important;
        color: #fff !important;
    }

    button#import {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-copy.buttons-html5 {
        background: #188e83 !important;
        color: #fff !important;
    }

    button.dt-button {
        background: #000 !important;
        color: #fff !important;
    }

    button.dt-button.buttons-print {
        background: #634141 !important;
        color: #fff !important;
    }

    button#createNewCustomer {
        background: #031e80 !important;
        color: #fff !important;
    }

    #data_trash {
        background: #820084 !important;
        color: #fff !important;
    }

    button.btn.btn-danger.btn-sm,
    a.btn.btn-success.btn-sm {
        margin-top: 3px;
    }

    .sidebar-dark .site-sidebar {
        background: #ffffff;
        border-right: 1px solid #ddd;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #ffffff;
    }

    .sidebar-dark .side-menu li a:hover {
        color: #51d2b7;
    }

    span.hide-menu {
        color: #434a5f;
    }

    .sidebar-dark .side-menu li a {
        color: #9e9595;
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #796f6f;
    }

    .sidebar-dark .side-menu li.active>a {
        color: #a8a0a0;
    }

    i.material-icons.list-icon.fs-24 {
        color: #888 !important;
    }

    #label_file {
        background-color: #03a9f3;
        border: 1px solid #0bbd98;
        color: white;
        padding: 0.5rem;
        font-family: sans-serif;
        cursor: pointer;
    }

    #file-chosen {
        margin-left: 0.3rem;
        font-family: sans-serif;
    }

    div#data-tabel_length {
        padding-top: .755em;
        margin-top: 1.42857em;
    }

</style>
@if (Session::has('error_api'))
    <script>
        swal({
            title: 'Gagal!',
            text: "{{ session('error_api')['message'] }}",
            timer: 5000,
            type: "{{ session('error_api')['icon'] }}"
        }).then((value) => {
            //location.reload();
        }).catch(swal.noop);
    </script>
@endif
<nav class="sidebar-nav">
    <ul class="nav in side-menu" style="justify-content: center !important;">
        <li>
            <a href="{{ url('admin/master/dashboard') }}" class="ripple">
                <i class="list-icon material-icons">dashboard</i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="menu-item-has-children {{ Request::segment(3) === 'sekolah' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(3) === 'sekolah' ? 'color-color-scheme' : '' }}"><i
                        class="list-icon material-icons">group_work</i>
                    <span class="hide-menu {{ Request::segment(3) === 'sekolah' ? 'color-color-scheme' : '' }}">Master
                        <span class="badge badge-border badge-border-inverted bg-primary pull-right"
                            style="color: black">5</span>
                    </span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('supermaster_sekolah') }}"
                        class="{{ Request::segment(3) === 'sekolah' ? 'color-color-scheme' : '' }}">Data
                        Sekolah</a>
                </li>

                <li>
                    <a href="{{ route('super_admin-kategori_program') }}"
                        class="{{ Request::segment(3) === 'sekolah' ? 'color-color-scheme' : '' }}">Kategori
                        Program</a>
                </li>
                <li>
                    <a href="{{ route('super_admin-program') }}"
                        class="{{ Request::segment(3) === 'sekolah' ? 'color-color-scheme' : '' }}">Program</a>
                </li>
            </ul>
        </li>
        <li
            class="menu-item-has-children {{ Request::segment(2) === 'program' || Request::segment(1) === 'program' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span
                    class="{{ Request::segment(2) === 'program' || Request::segment(1) === 'program' ? 'color-color-scheme' : '' }}">
                    <i class="list-icon material-icons">important_devices</i>
                    <span
                        class="hide-menu {{ Request::segment(2) === 'program' || Request::segment(1) === 'program' ? 'color-color-scheme' : '' }}">Program</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li class="menu-item-has-children {{ Request::segment(3) === 'learning' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ Request::segment(3) === 'learning' ? 'color-color-scheme' : '' }}">Elearning</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_learning-room') }}"
                                class="{{ Request::segment(4) === 'room' ? 'color-color-scheme' : '' }}">Room</a>
                        </li>
                        <li class="menu-item-has-children {{ Request::segment(2) === 'point' ? 'active' : '' }}">
                            <a href="javascript:void(0);"
                                class="{{ Request::segment(3) === 'kategori_pelanggaran' || Request::segment(3) === 'pelanggaran' || Request::segment(3) === 'sanksi' ? 'color-color-scheme' : '' }}">Set
                                Up E-Learning</a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('admin_learning-setting') }}"
                                        class="{{ Request::segment(3) === 'kategori_pelanggaran' ? 'color-color-scheme' : '' }}">Setting
                                        E-Learning</a>
                                </li>
                                <li>
                                    <a href="{{ route('point_pelanggaran-beranda') }}"
                                        class="{{ Request::segment(3) === 'pelanggaran' ? 'color-color-scheme' : '' }}">Layout
                                        E Learning</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ Request::segment(3) === 'raport' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ Request::segment(3) === 'raport' ? 'color-color-scheme' : '' }}">Raport</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('admin_raport-sikap_sosial') }}"
                                class="{{ Request::segment(4) === 'sikap_sosial' ? 'color-color-scheme' : '' }}">Sikap
                                Sosial</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_raport-sikap_spiritual') }}"
                                class="{{ Request::segment(4) === 'sikap_spiritual' ? 'color-color-scheme' : '' }}">Sikap
                                Spiritual</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-predikat') }}"
                                class="{{ Request::segment(4) === 'raport_predikat' ? 'color-color-scheme' : '' }}">Predikat</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-setting_nilai') }}"
                                class="{{ Request::segment(4) === 'raport_set_nilai' ? 'color-color-scheme' : '' }}">Settingan
                                Nilai</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-template_catatan') }}"
                                class="{{ Request::segment(4) === 'template_catatan' ? 'color-color-scheme' : '' }}">Template
                                Catatan Wali</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-config') }}"
                                class="{{ Request::segment(4) === 'config' ? 'color-color-scheme' : '' }}">Config</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-kop_sampul') }}"
                                class="{{ Request::segment(4) === 'kop_sampul' ? 'color-color-scheme' : '' }}">Kop
                                Sampul</a>
                        </li>
                        <li>
                            <a href="{{ route('raport-template_sampul') }}"
                                class="{{ Request::segment(4) === 'template_sampul' ? 'color-color-scheme' : '' }}">Template
                                Sampul</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ Request::segment(2) === 'point' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ Request::segment(2) === 'point' ? 'color-color-scheme' : '' }}">Point
                    </a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children {{ Request::segment(2) === 'point' ? 'active' : '' }}">
                            <a href="javascript:void(0);"
                                class="{{ Request::segment(3) === 'kategori_pelanggaran' || Request::segment(3) === 'pelanggaran' || Request::segment(3) === 'sanksi' ? 'color-color-scheme' : '' }}">Data
                                Tata Tertib</a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('point_ketegori_pelanggaran-beranda') }}"
                                        class="{{ Request::segment(3) === 'kategori_pelanggaran' ? 'color-color-scheme' : '' }}">Kategori
                                        Pelanggaran</a>
                                </li>
                                <li>
                                    <a href="{{ route('point_pelanggaran-beranda') }}"
                                        class="{{ Request::segment(3) === 'pelanggaran' ? 'color-color-scheme' : '' }}">Bentuk
                                        Pelanggaran</a>
                                </li>
                                <li>
                                    <a href="{{ route('point_sanksi-beranda') }}"
                                        class="{{ Request::segment(3) === 'sanksi' ? 'color-color-scheme' : '' }}">Sanksi
                                        Pelanggaran</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('point_pelanggaran_siswa-beranda') }}">Pelanggaran Siswa</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ Request::segment(3) === 'bkk' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ Request::segment(3) === 'bkk' ? 'color-color-scheme' : '' }}">BKK</a>
                    <ul class="list-unstyled sub-menu">
                        <li class="menu-item-has-children {{ Request::segment(3) === 'user-bkk' ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="ripple">
                                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                                    <span
                                        class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">Set
                                        up
                                        BKK</span>
                                </span>
                            </a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('user_bkk-beranda') }}"
                                        class="{{ Request::segment(3) === 'user-bkk' ? 'color-color-scheme' : '' }}">Manajemen
                                        User
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('bkk_admin-beranda') }}"
                                        class="{{ Request::segment(3) === 'user-bkk' ? 'color-color-scheme' : '' }}">Admin
                                        BKK
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-item-has-children {{ Request::segment(3) === 'industri' ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="ripple">
                                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                                    <span
                                        class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">Manajemen
                                        Mitra</span>
                                </span>
                            </a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('bkk_industri-beranda') }}"
                                        class="{{ Request::segment(3) === 'industri' ? 'color-color-scheme' : '' }}">Mitra
                                        Industri
                                    </a>
                                    <a href="{{ route('user_bkk-perusahaan') }}"
                                        class="{{ Request::segment(4) === 'perusahaan' ? 'color-color-scheme' : '' }}">User
                                        Perusahaan
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children {{ Request::segment(2) === 'user' ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="ripple">
                                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">
                                    <span
                                        class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">Bursa
                                        Kerja</span>
                                </span>
                            </a>
                            <ul class="list-unstyled sub-menu">
                                <li>
                                    <a href="{{ route('bkk_bidang_loker-beranda') }}"
                                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Bidang
                                        Loker
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('lowongan-create') }}"
                                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Input
                                        Loker
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('lowongan-beranda') }}"
                                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">List
                                        Loker
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href=""
                                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">Jadwal
                                        Test
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children {{ Request::segment(3) === 'bkk' ? 'active' : '' }}">
                    <a href="javascript:void(0);"
                        class="{{ Request::segment(3) === 'bkk' ? 'color-color-scheme' : '' }}">Prakerin</a>
                    <ul class="list-unstyled sub-menu">
                        <li>
                            <a href="{{ route('supermaster-template_sertifikat') }}"
                                class="{{ Request::segment(4) === 'room' ? 'color-color-scheme' : '' }}">Template</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>
        <li class="menu-item-has-children {{ Request::segment(2) === 'user' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}"><i
                        class="list-icon material-icons">group</i>
                    <span
                        class="hide-menu {{ Request::segment(2) === 'user' ? 'color-color-scheme' : '' }}">User</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('supermaster-superadmin') }}"
                        class="{{ Request::segment(3) === 'supervisor' ? 'color-color-scheme' : '' }}">SuperAdmin
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item-has-children {{ Request::segment(3) === 'backup' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="ripple">
                <span class="{{ Request::segment(3) === 'backup' ? 'color-color-scheme' : '' }}"><i
                        class="list-icon material-icons">settings</i>
                    <span
                        class="hide-menu {{ Request::segment(2) === 'setting' ? 'color-color-scheme' : '' }}">Setting</span>
                </span>
            </a>
            <ul class="list-unstyled sub-menu">
                <li>
                    <a href="{{ route('supermaster-backup_database') }}"
                        class="{{ Request::segment(3) === 'backup' ? 'color-color-scheme' : '' }}">Back Up
                        Database</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('auth.logout') }}">
                <i class="list-icon material-icons">settings_power</i>
                <span class="hide-menu">Log Out</span>
            </a>
        </li>
    </ul>
</nav>
