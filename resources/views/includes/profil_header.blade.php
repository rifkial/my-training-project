<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" rel="stylesheet"
        type="text/css">
</head>
<style>
    .center-cropped {
        margin-top: 14px;
        width: 60px;
        height: 60px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: 100%;
    }

    .dropdown-content {
        display: none;
        /* display: flex;
        flex-direction: column; */
        position: absolute;
        background-color: #fff;
        min-width: 160px;
        box-shadow: 0px 1px 10px 0px rgb(183 183 183);
        z-index: 1;
        bottom: -115px;
        right: -15px;
        border-radius: 10px;
    }

    .dropdown-content::before {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        border-left: 1rem solid transparent;
        border-right: 1rem solid transparent;
        border-bottom: 1rem solid #fff;
        right: 27%;
        top: -1rem;
    }

    .dropdown:hover .dropdown-content {
        position: absolute;
        display: flex;
        flex-direction: column;
    }

    .navbar-nav .avatar::before {
        display: none;
    }

    .roles {
        color: black;
    }

    .material-icons {
        color: #c2d1ce;
    }

    @media (max-width: 720px) {
        .media-body.hide-menu {
            display: none;
        }
    }

</style>



<ul class="nav navbar-nav d-none d-lg-flex">
    <li class="dropdown">
        @if (session('role') != 'admin' and session('role') != 'admin-kesiswaan' and session('role') != 'superadmin' and session('role') != 'pembimbing-industri' and session('role') != 'admin-prakerin' and session('role') != 'learning-admin' and session('role') != 'bk' and session('role') != 'ortu' and session('role') != 'bkk-admin' and session('role') != 'walikelas' and session('role') != 'supervisor' and session('role') != 'induk-admin' and session('role') != 'alumni-alumni' and session('role') != 'admin-alumni' and session('role') != 'user-kaprodi' and session('role') != 'bkk-perusahaan' and session('role') != 'admin-raport' and session('role') != 'admin-absensi' and session('role') != 'admin-cbt')
            <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
                <i class="material-icons list-icon">public</i>
                <span class="badge badge-pill bg-primary px-2">{{ count($notif) }}</span>
            </a>
            <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
                <div class="card">
                    <header class="card-header">New notifications
                        <span class="mr-l-10 badge badge-border bg-primary">{{ count($notif) }}</span>
                    </header>
                    <ul class="list-unstyled dropdown-list-group">
                        @if (empty($notif))
                            <li>
                                <a href="#" class="media">
                                    <span class="media-body">
                                        <span class="media-heading">Tidak ada pemberitahuan yang tersedia</span>
                                    </span>
                                </a>
                            </li>
                        @else
                            <a href="{{ route('room-read_all_notif') }}">tandai baca semua</a>
                            @foreach ($notif as $nt)
                                <li>
                                    <a href="{{ $nt['url'] }}" class="media"
                                        onclick="updateRead({{ $nt['id'] }})">
                                        <span class="d-flex">
                                            <i class="material-icons list-icon">comment</i>
                                        </span>
                                        <span class="media-body">
                                            <span class="media-heading">{{ $nt['isi'] }}</span>
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        @endif
    </li>
</ul>

<ul class="nav navbar-nav info px-3">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown" data-target="#multiCollapseExample2"
            aria-expanded="false" aria-controls="multiCollapseExample2">
            <div class="media-body hide-menu" style="line-height: normal; float: left; margin-right: 11px;">
                <h6 class="media-heading mr-b-5 text-uppercase text-info" style="line-height: 26px;"">
                    {{ $profile['nama'] }}
                </h6>
                <span class="  user-type fs-12 roles text-info">{{ session('role') }}</span>
            </div>

            <span class="avatar thumb-sm" style="float: right; ">
                <div class="center-cropped rounded-circle" style="background-image: url({{ $profile['file'] }});">
                </div>
            </span>
        </a>

        <div id="multiCollapseExample2" class="dropdown-content dropdown-menu collapse p-3 animated flipInY">
            <span class="my-2">
                @if (session('role') == 'admin')
                    <a href="{{ route('edit-profile_admin') }}" class="dropdown-toggle ripple">
                    @else
                        <a href="{{ route('edit-profile', Request::segment(2)) }}"
                            class="dropdown-toggle ripple text-info">
                @endif
                <i class="fa fa-pencil"></i>
                Edit profil
                </a>
            </span>

            <span class="my-2">
                <a href="{{ route('auth.logout') }}" class="text-info">
                    <i class="fa fa-power-off text-info"></i>
                    Logout
                </a>
            </span>
        </div>
    </li>
</ul>

<script>
    function updateRead(id) {
        $('#form_result').html('');
        $.ajax({
            type: 'POST',
            url: "{{ route('room-read_notif') }}",
            data: {
                id
            },
            success: function(data) {
                console.log(data);
            }
        });
    }
</script>
