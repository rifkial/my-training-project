<script src="{{ asset('asset/landing/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('asset/landing/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('asset/landing/js/main.js') }}"></script>

<script>
    function swa(status, message, icon) {
        swal(
            status,
            message,
            icon
        );
        return true;
    }
</script>
