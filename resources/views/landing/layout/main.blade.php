<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Demo SmartSchool - MySCH</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link rel="icon" type="image/png" sizes="20x20" href="{{ asset('asset/img/mysch.png') }}">
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('landing.layout.head')
</head>

<style>
    @media (max-width: 767.98px) {
        .hero-img{
            display:none;
        }
    }


</style>

<body>

    <header id="header" class="header fixed-top">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="{{ url('/') }}" class="logo d-flex align-items-center">
                <img src="{{ asset('asset/img/mysch.png') }}" alt="">
                <span>MySCH.ID</span>
            </a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="{{ url('/#hero') }}">Home</a></li>
                    <li><a class="nav-link scrollto" href="{{ url('/#pricing') }}">Paket</a></li>
                    <!-- <li><a class="nav-link scrollto" href="{{ url('/#features') }}">Portfolio</a></li>-->
                    <li><a class="nav-link scrollto" href="{{ url('/#features') }}">Fitur</a></li>
                    <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
                    <li><a class="getstarted scrollto" href="{{ route('halaman-demo') }}">Demo Khusus</a></li>
                    <li><a class="getstarted scrollto" href="{{ route('halaman-langganan') }}">Langganan</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
        </div>
    </header>
    @if (empty(Request::segment(1)))
        <section id="hero" class="hero d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 d-flex flex-column justify-content-center">
                        <h1 data-aos="fade-up">Buat Aplikasi E-Learning Untuk Sekolah Yang Anda Kelola</h1>
                        <h2 data-aos="fade-up" data-aos-delay="400">Menerbitkan Pengumuman, Tugas, Materi dan Tes ke
                            Peserta
                            Didik akan lebih mudah menggunakan aplikasi ini. Setiap fitur kami buat dengan
                            sunguh-sungguh
                            berdasarkan masukan yang kami terima, sehingga fitur benar-benar disesuai dengan kebutuhan
                            Sekolah Anda.</h2>
                        <div data-aos="fade-up" data-aos-delay="600">
                            <div class="text-center text-lg-start">
                                <a href="{{ url('/#demos') }}"
                                    class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                                    <span>Coba Demo</span>
                                    <i class="bi bi-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                        <img src="{{ asset('asset/landing/img/hero-img.png') }}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>
    @endif
    <main id="main">
        @yield('content')
    </main>
    @include('landing.layout.footer')

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    @include('landing.layout.foot')

</body>

</html>
