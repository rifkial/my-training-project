@extends('landing.layout.main')
@section('content')
    <section id="values" class="values">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2></h2>
                <p>Apa Kelebihan Menggunakan MySCH</p>
            </header>

            <div class="row p-3">

                <div class="col-lg-4">
                    <div class="box" data-aos="fade-up" data-aos-delay="200">
                        <img src="{{ asset('asset/landing/img/values-1.png') }}" class="img-fluid" alt="">
                        <h3>Management Yang Baik</h3>
                        <p>Dengan menggunakan MySCH, Anda akan dengan mudah mengatur data di Sekolah Anda
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0">
                    <div class="box" data-aos="fade-up" data-aos-delay="400">
                        <img src="{{ asset('asset/landing/img/values-2.png') }}" class="img-fluid" alt="">
                        <h3>Teknologi Terbarukan</h3>
                        <p>Sistem kami dibuat dengan teknologi terbarukan dan terkini yang cepat,aman dan mudah digunakan
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0">
                    <div class="box" data-aos="fade-up" data-aos-delay="600">
                        <img src="{{ asset('asset/landing/img/values-3.png') }}" class="img-fluid" alt="">
                        <h3>Hemat Waktu</h3>
                        <p>Dengan sistem online yang terstruktur dan terintegrasi, akan membuat pengelolaan data sekolah
                            lebih cepat dan optimal
                        </p>
                    </div>
                </div>

            </div>

        </div>

    </section>
    <section id="demos" class="demos">
        <div class="container">
            <header class="section-header">
                <h2>Silakan mencoba demo aplikasi smartschool di masing-masing peran berikut</h2>
                <p>Demo Aplikasi</p>
            </header>

            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                    <form action="{{ route('auth.login_demo') }}" method="POST">
                        @csrf
                        <input type="hidden" name="role" value="admin">
                        <input type="hidden" name="username" value="smartschooldev@gmail.com">
                        <input type="hidden" name="password" value="12345">
                        <button type="submit" class="btn btn-primary btn-lg rounded-pill">Sebagai Administrator</button>
                    </form>

                    </p>

                    <p>
                    <form action="{{ route('auth.login_demo') }}" method="POST">
                        @csrf
                        <input type="hidden" name="role" value="guru">
                        <input type="hidden" name="username" value="1505020012">
                        <input type="hidden" name="password" value="12345">
                        <button type="submit" class="btn btn-success btn-lg rounded-pill">Sebagai Pengajar</button>
                    </form>
                    </p>

                    <p>
                        <form action="{{ route('auth.login_demo') }}" method="POST">
                            @csrf
                            <input type="hidden" name="role" value="siswa">
                            <input type="hidden" name="username" value="150502001198787">
                            <input type="hidden" name="password" value="12345">
                            <button type="submit" class="btn btn-danger btn-lg rounded-pill">Sebagai Peserta Didik</button>
                        </form>
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- ======= Features Section ======= -->
    <section id="features" class="features">

        <div class="container p-3 p-3" data-aos="fade-up">

            <header class="section-header">
                <p>Fitur Aplikasi</p>
            </header>

            <div class="row">

                <div class="col-lg-6">
                    <img src="{{ asset('asset/landing/img/features.png') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                    <div class="row align-self-center gy-4">

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Domain</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Jadwal Mengajar</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Materi</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Tugas</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="600">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Manajemen Master Data</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="700">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Jadwal</h3>
                            </div>
                        </div>

                    </div>
                </div>

            </div> <!-- / row -->

            <!-- Feature Tabs -->
            <div class="row feture-tabs" data-aos="fade-up">
                <div class="col-lg-12 text-center">
                    <h3>Layanan Pendukung</h3>


                    <!-- Tabs -->
                    <!-- <ul class="nav nav-pills mb-3">
                                            <li>
                                                <a class="nav-link active" data-bs-toggle="pill" href="#tab1">Server</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-bs-toggle="pill" href="#tab2">Storage File</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-bs-toggle="pill" href="#tab3">Full Support</a>
                                            </li>
                                        </ul> -->
                    <!-- End Tabs -->

                    <!-- Tab Content -->


                </div>

                <!-- <div class="col-lg-6">
                                        <img src="{{ asset('asset/landing/img/features-2.png') }}" class="img-fluid" alt="">
                                    </div> -->

            </div>

        </div>

    </section><!-- End Features Section -->



    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts pt-0">
        <div class="container p-3" data-aos="fade-up">

            <div class="row gy-4">

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-hdd-stack"></i>
                        <div>
                            <!-- <span data-purecounter-start="0" data-purecounter-end="232"
                                                    data-purecounter-duration="1" class="purecounter">Server</span> -->
                            <h4>Server</h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-archive" style="color: #ee6c20;"></i>
                        <div>
                            <!-- <span data-purecounter-start="0" data-purecounter-end="521"
                                                    data-purecounter-duration="1" class="purecounter"></span> -->
                            <h4>Storage File</h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-headset" style="color: #15be56;"></i>
                        <div>
                            <!-- <span data-purecounter-start="0" data-purecounter-end="1463"
                                                    data-purecounter-duration="1" class="purecounter"></span> -->
                            <h4>Full Support</h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-laptop" style="color: #bb0852;"></i>
                        <div>
                            <!-- <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1"
                                                    class="purecounter"></span> -->
                            <h4>Free Demo</h4>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End Counts Section -->



    <!-- ======= Services Section ======= -->
    <!-- <section id="services" class="services">

                            <div class="container" data-aos="fade-up">

                                <header class="section-header">
                                    <h2>Services</h2>
                                    <p>Veritatis et dolores facere numquam et praesentium</p>
                                </header>

                                <div class="row gy-4">

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
                                        <div class="service-box blue">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Nesciunt Mete</h3>
                                            <p>Provident nihil minus qui consequatur non omnis maiores. Eos accusantium minus dolores
                                                iure perferendis tempore et consequatur.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
                                        <div class="service-box orange">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Eosle Commodi</h3>
                                            <p>Ut autem aut autem non a. Sint sint sit facilis nam iusto sint. Libero corrupti neque eum
                                                hic non ut nesciunt dolorem.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
                                        <div class="service-box green">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Ledo Markt</h3>
                                            <p>Ut excepturi voluptatem nisi sed. Quidem fuga consequatur. Minus ea aut. Vel qui id
                                                voluptas adipisci eos earum corrupti.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
                                        <div class="service-box red">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Asperiores Commodi</h3>
                                            <p>Non et temporibus minus omnis sed dolor esse consequatur. Cupiditate sed error ea fuga
                                                sit provident adipisci neque.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="600">
                                        <div class="service-box purple">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Velit Doloremque.</h3>
                                            <p>Cumque et suscipit saepe. Est maiores autem enim facilis ut aut ipsam corporis aut. Sed
                                                animi at autem alias eius labore.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
                                        <div class="service-box pink">
                                            <i class="ri-discuss-line icon"></i>
                                            <h3>Dolori Architecto</h3>
                                            <p>Hic molestias ea quibusdam eos. Fugiat enim doloremque aut neque non et debitis iure.
                                                Corrupti recusandae ducimus enim.</p>
                                            <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </section> -->
    <!-- End Services Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">

        <div class="container p-3" data-aos="fade-up">

            <header class="section-header">
                <h2>Paket</h2>
                <p>Cek Paket Kami</p>
            </header>

            <div class="row gy-4 justify-content-center" data-aos="fade-left">

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="100">
                    <div class="box">
                        <h3 style="color: #07d5c0;">Demo</h3>
                        <div class="price"><sup>Coba Gratis</sup><span></span></div>
                        <img src="{{ asset('asset/landing/img/pricing-free.png') }}" class="img-fluid" alt="">
                        <ul>
                            <li>Silakan mencoba</li>
                            <li>produk kami</li>
                            <li>secara gratis</li>
                            <li class="na"></li>
                            <li class="na"></li>
                        </ul>
                        <a href="#" class="btn-buy">Coba</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="200">
                    <div class="box" style="height:100%">
                        <!-- <span class="featured">Featured</span> -->
                        <h3 style="color: #65c600;">Basic</h3>
                        <div class="price"><sup>Rp</sup>600.000<span> / bulan</span></div>
                        <img src="{{ asset('asset/landing/img/pricing-starter.png') }}" class="img-fluid" alt="">
                        <ul>
                            <li>Gratis domain resmi</li>
                            <li>Fitur standart sekolah</li>
                            <li class="na">PPDB online</li>
                        </ul>
                        <a href="#" class="btn-buy">Pesan Sekarang</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="300">
                    <div class="box">
                        <h3 style="color: #ff901c;">Profesional</h3>
                        <div class="price"><sup>Rp</sup>1.000.000<span> / bulan</span></div>
                        <img src="{{ asset('asset/landing/img/pricing-business.png') }}" class="img-fluid" alt="">
                        <ul>
                            <li>Fitur Lengkap sekolah</li>
                            <li>PPDB online</li>
                            <li>Pengumuman online</li>
                        </ul>
                        <a href="#" class="btn-buy">Pesan Sekarang</a>
                    </div>
                </div>

            </div>

        </div>

    </section>
    <section id="faq" class="faq">

        <div class="container p-3" data-aos="fade-up">

            <header class="section-header">
                <h2>F.A.Q</h2>
                <p>Pertanyaan Yang Sering Diajukan</p>
            </header>

            <div class="row">
                <div class="col-lg-6">
                    <!-- F.A.Q List 1-->
                    <div class="accordion accordion-flush" id="faqlist1">
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-content-1">
                                    Apakah langganan bisa per bulan?
                                </button>
                            </h2>
                            <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                    Iya bisa, memang kami menyarankan untuk per bulan saja, supaya jika aplikasi ini
                                    ternyata
                                    tidak sesuai dengan yang dibutuhkan Sekolah, bisa berhenti kapan saja tanpa kehilangan
                                    banyak uang.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-content-2">
                                    Apakah ada biaya tambahan selain biaya langganan?
                                </button>
                            </h2>
                            <div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                    Tidak ada, kecuali jika Sekolah Anda menginginkan untuk peningkatan server
                                    untuk kebutuhan ujian dikarenakan server yang ada tidak sanggup untuk melayani.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq-content-3">
                                    Apakah data yang kami simpan aman?
                                </button>
                            </h2>
                            <div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                <div class="accordion-body">
                                    Kode program kami tulis dengan sangat hati-hati, tidak ada yang masuk ke server selain
                                    kami sendiri,
                                    sehingga kerahasiaan kode program benar-benar kami jaga.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6">

                    <!-- F.A.Q List 2-->
                    <div class="accordion accordion-flush" id="faqlist2">

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq2-content-1">
                                    Apakah sekolah melakukan hosting sendiri ?
                                </button>
                            </h2>
                            <div id="faq2-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                    Tidak, masalah server akan kami yang handle, Anda dan tim cukup fokus pada
                                    konten yang ingin Anda ajarkan.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq2-content-2">
                                    Kalo masa aktif langganan habis apakah data Sekolah kami dihapus?
                                </button>
                            </h2>
                            <div id="faq2-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                    Tidak, data Sekolah Anda tidak kami hapus, Anda dapat memperpanjang kapanpun dan data
                                    sebelumnya akan kami restore.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#faq2-content-3">
                                    Bagaimana jika terdapat masalah didalam sistem ?
                                </button>
                            </h2>
                            <div id="faq2-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist2">
                                <div class="accordion-body">
                                    Tenang, kami akan membantu Anda sampai masalah selesai. Anda dapat menghubungi
                                    customer service kami yang siap membantu Anda.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </section>
    <section id="contact" class="contact">

        <div class="container p-3" data-aos="fade-up">

            <header class="section-header">
                <h2>Kontak</h2>
                <p>Hubungi Kami</p>
            </header>

            <div class="row gy-4">

                <div class="col-lg-6">

                    <div class="row gy-4">
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-geo-alt"></i>
                                <h3>Alamat</h3>
                                <p>Jl. Raya Krangan - Pringsurat<br>Kabupaten Temanggung, Jawa Tengah</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-telephone"></i>
                                <h3>Telefon</h3>
                                <p>0857-4000-0146<br></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-envelope"></i>
                                <h3>Email Us</h3>
                                <p>info@example.com<br>contact@example.com</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-clock"></i>
                                <h3>Jam Buka</h3>
                                <p>Senin - Jumat : 08.00 - 16.00<br>Sabtu : 08.00 - 12.00<br>Minggu Tutup</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="forms/contact.php" method="post" class="php-email-form">
                        <div class="row gy-4">

                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control" placeholder="Your Name" required>
                            </div>

                            <div class="col-md-6 ">
                                <input type="email" class="form-control" name="email" placeholder="Your Email" required>
                            </div>

                            <div class="col-md-12">
                                <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                            </div>

                            <div class="col-md-12">
                                <textarea class="form-control" name="message" rows="6" placeholder="Message"
                                    required></textarea>
                            </div>

                            <div class="col-md-12 text-center">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>

                                <button type="submit">Send Message</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('#formAdministrator').on('submit', function(event) {
            event.preventDefault();
            $("#btnLogin").html(
                '<i class="fa fa-spin fa-spinner"></i> Loading');
            $("#btnLogin").attr("disabled", true);
            $.ajax({
                url: "{{ route('auth.verifylogin') }}",
                method: 'POST',
                data: $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data.status == 'berhasil') {
                        window.location.href = data.redirect;
                    } else {
                        $("#btnLogin").html(
                            'Masuk');
                        $("#btnLogin").attr("disabled", false);
                    }
                    swa(data.status + "!", data.message, data.icon);

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#btnLogin').html('Masuk');
                    $("#btnLogin").attr("disabled", false);
                }
            });
        });
    </script>
@endsection
