@extends('landing.layout.main')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style>
        .text-center {
            background: #f1f3f6;
            padding: 10px;
        }

        .alert.alert-shadowed {
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .2);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .2);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .2);
        }

        .alert.radius-bordered {
            -webkit-border-radius: 3px;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 3px;
            -moz-background-clip: padding;
            border-radius: 3px;
            background-clip: padding-box;
        }

        .alert {
            margin-bottom: 20px;
            margin-top: 0;
            color: #fff;
            border-width: 0;
            border-left-width: 5px;
            padding: 10px;
            border-radius: 0;
        }

        .alert.alert-warning {
            border-color: #ffce55;
            color: #555;
            background: #fff1a8;
        }

        .alert-info {
            color: #fff;
            background-color: #4154f1;
            border-color: #001aef;
        }

        .card-title{
            color: #273394;
        }

        .input-group{
            border: 0.1px solid #efdddd;
            border-radius: 5px;
        }

        label{
            font-size:15px;
        }

    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <section id="values" class="values values bg-light" style="margin-top: 10rem!important">
        <div class="container bg-light" data-aos="fade-up">
            <header class="section-header">
                <h2>Silakan menghubungi kami untuk memulai berlangganan.</h2>
                <p>Mulai Berlangganan</p>
            </header>

            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <center>
                        <span class="thumbnail" style="border: 1px solid #2bb33f;">
                            <i class="fa fa-whatsapp fa-5x" aria-hidden="true" style="color: #2bb33f"></i>
                            <h4 style="color: #2bb33f">WHATSAAP</h4>
                            <h4 style="color: #2bb33f">089635345353</h4>
                            <p style="color: #2bb33f">(Chat Only) </p>
                        </span>
                    </center>
                </div>

                <div class="col-md-4 col-sm-6">
                    <center>
                        <span class="thumbnail" style="border: 1px solid #289fd8;">
                            <i class="fa fa-telegram fa-5x" aria-hidden="true" style="color: #289fd8;"></i>
                            <h4 style="color: #289fd8;">TELEGRAM</h4>
                            <h4 style="color: #289fd8;">089635345353</h4>
                            <p style="color: #289fd8;">(Chat Only) </p>
                        </span>
                    </center>
                </div>

                <div class="col-md-4 col-sm-6">
                    <center>
                        <span class="thumbnail" style="border: 1px solid #d44b3f;">
                            <i class="fa fa-envelope-o fa-5x" aria-hidden="true" style="color: #d44b3f;"></i>
                            <h4 style="color: #d44b3f;">EMAIL</h4>
                            <h4 style="color: #d44b3f;">089635345353</h4>
                            <p style="color: #d44b3f;">(Chat Only) </p>
                        </span>
                    </center>
                </div>
            </div>
        </div>
    </section>


    <div class="container mt-5">
        <header class="section-header pb-0">
            <p>Pendaftaran Aplikasi</p>
        </header>
        <p style="text-align: center" class="mt-3">Formulir ini membutuhkan Kode Registrasi yang didapatkan setelah pembayaran.
            Pelajari terlebih dahulu Syarat dan Ketentuan kami, mendaftar berarti setuju</p>
        <div class="row justify-content-center ">
            <div class="col-lg-12 text-center bg-white">
                <form class="form-horizontal" id="SekolahForm" action="javascript:void(0)">
                    <fieldset class="my-5">
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label" for="Name (Full name)">Nama Sekolah</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university">
                                        </i>
                                    </div>
                                    <input id="" name="nama" required type="text" class="form-control input-lg">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group my-5 d-md-flex">
                            <label class="col-md-3 control-label" for="Date Of Birth">URL Website</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        https://
                                    </div>
                                    <input name="subdomain" required type="text" class="form-control input-lg">
                                    <div class="input-group-addon">
                                        .sch.id
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="row">

                            <div class="col-md-3"></div>
                            <div class="col-md-7">
                                <div class="alert alert-warning fade in radius-bordered alert-shadowed">
                                    <b>Url wajib diisi..</b>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-10 my-5">
                            <div class="col-md-3"></div>
                            <h3 class="card-title"><b>Akun Administrator </b></h3>
                        </div>
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label" for="Father">Nama</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user-circle-o" style="font-size: 20px;"></i>
                                    </div>
                                    <input required name="nama_admin" type="text" class="form-control input-lg">
                                </div>

                            </div>
                        </div>
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label" for="Mother">Alamat Email</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <input required name="email" type="text" class="form-control input-lg">
                                </div>
                                {{-- <div class="alert alert-info mt-3 mb-3 px-5">Sistem akan mengirimkan link aktivasi ke alamat
                                    e-mail ini, gunakan <span><b>Gmail</b></span> untuk mempermudah aktivasi.</div> --}}
                            </div>
                        </div>

                        
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label" for="Father">Nama Lengkap</label>
                            <input type="hidden" name="aksi" value="no_domain">
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user-circle-o" style="font-size: 20px;"></i>
                                    </div>
                                    <input required name="nama_admin" type="text" class="form-control input-lg">
                                </div>

                            </div>
                        </div>

                        <div class="form-group my-5">
                            <label class="col-md-3 control-label" for="Father">Nomor WA</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="bi bi-telephone" style="font-size: 20px;"></i>
                                    </div>
                                    <input required name="nomor_wa" type="number" class="form-control input-lg">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label">Kata Sandi</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-key"></i>

                                    </div>
                                    <input type="password" onkeyup="passwordChecker()" name="" id="password"
                                        class="form-control input-lg"><span class="input-group-btn"><button
                                            class="btn btn-defaultCUST" id="view_button3"
                                            style=" height: 34px;padding-left: 7px;" type="button"><span
                                                class="glyphicon glyphicon-eye-open"></span>
                                        </button></span>
                                </div>
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12 mobilePad" id="message8"
                                        style=" font-size: 10pt; padding-left: 0px;"></div>
                                    <div class="col-md-12 mobilePad" id="message" style=" font-size: 10pt;"></div>
                                    <div class="col-md-12 mobilePad" id="message2" style=" font-size: 10pt;"></div>
                                    <div class="col-md-12 mobilePad" id="message3" style=" font-size: 10pt;"></div>
                                    <div class="col-md-12 mobilePad" id="message4" style=" font-size: 10pt;"></div>
                                    <div class="col-md-12 mobilePad" id="message5" style=" font-size: 10pt;"></div>
                                    <div class="col-md-12 mobilePad" id="message6"
                                        style=" font-size: 10pt;padding-left: 0px;"></div>
                                    <div class="col-md-12 mobilePad" id="message7"
                                        style=" font-size: 10pt;padding-left: 0px;"></div>

                                </div>
                            </div>
                        </div>
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label">Konfirmasi
                                Sandi</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    <input type="password" name="password" id="verifypassword" class="form-control input-lg"><span
                                        class="input-group-btn"><button class="btn btn-defaultCUST" id="view_button4"
                                            style=" height: 34px;padding-left: 7px;" type="button"><span
                                                class="glyphicon glyphicon-eye-open"></span>
                                        </button></span>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12" id="message1"
                                        style="font-weight: bold; text-align: center;font-size: 10pt;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group my-5">
                            <label class="col-md-3 control-label col-xs-12" for="Permanent Address">License No.</label>
                            <div class="col-md-7  col-xs-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input id="License No." name="License No." type="number" class="form-control input-lg">

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <button type="submit" id="btnRegister" class="btn btn-lg btn-success"><span
                                        class="glyphicon glyphicon-log-in"></span> Register</button>
                                <a href="#" class="btn btn-lg btn-danger" value=""><span
                                        class="glyphicon glyphicon-remove-sign"></span> Clear</a>

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#SekolahForm').on('submit', function(event) {
            if (document.getElementById("password").value.trim() === "" && document.getElementById("password")
                .value !== null) {
                $('#message1').css('color', 'red');
                $('#message1').html('Please enter your Password');
            } else if (document.getElementById("verifypassword").value.trim() === "" && document.getElementById(
                    "verifypassword").value !== null) {
                $('#message1').css('color', 'red');
                $('#message1').html('Please confirm your password');
            } else {
                var password = $('#password').val();
                var confirm = $('#verifypassword').val();
                if (password == confirm) {
                    event.preventDefault();
                    $("#btnRegister").html(
                        '<i class="fa fa-spin fa-spinner"></i> Loading');
                    // $("#btnRegister").attr("disabled", true);
                    $('#message1').css('color', 'green');
                    $('#message1').html('Success! password berhasil ditemukan');
                    $.ajax({
                        url: "{{ route('proses_save_sekolah') }}",
                        method: "POST",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function(data) {
                            // console.log(data);
                            if (data.status == 'berhasil') {
                                $('#SekolahForm').trigger("reset");
                                $('#message').html("");
                                $('#message1').html("");
                                $('#message2').html("");
                                $('#message3').html("");
                                $('#message4').html("");
                                $('#message5').html("");
                                $('#message6').html("");
                                $('#message7').html("");
                                $('#message8').html("");
                            }
                            swal({
                                title: data.status + "!",
                                text: data.success,
                                type: data.icon,
                                confirmButtonText: "Coba Demo"
                            }).then(function() {
                                window.location = window.location.protocol + "//" + window.location.host + "/demo";
                            });
                            $('#btnRegister').html(
                                '<span class="glyphicon glyphicon-log-in"></span> Register');
                            $(
                                "#btnRegister").attr("disabled", false);

                        },
                        error: function(data) {
                            console.log('Error:', data);
                            $('#btnRegister').html(
                                '<span class="glyphicon glyphicon-log-in"></span> Register');
                        }
                    });

                } else {
                    $('#message1').css('color', 'red');
                    $('#message1').html('Confirm password and password must be same');
                    return false;

                }
                return true;
            }

        });
        $(document).ready(function() {
            $("#view_button3").bind("mousedown touchstart", function() {
                $("#password").attr("type", "text");
            }), $("#view_button3").bind("mouseup touchend", function() {
                $("#password").attr("type", "password");
            }), $("#view_button4").bind("mousedown touchstart", function() {
                $("#verifypassword").attr("type", "text");
            }), $("#view_button4").bind("mouseup touchend", function() {
                $("#verifypassword").attr("type", "password")
            })
        });

        function passwordChecker() {
            $('#verifypassword').val('');
            $('#message1').html('');
            $('#message8').html('');
            $('#message10').html('');
            $('#message').html('');
            $('#message2').html('');
            $('#message3').html('');
            $('#message4').html('');
            $('#message5').html('');
            $('#message6').html('');
            $('#message7').html('');
            if ($('#password').val().length >= 4) {
                if (newValPassPoilcy() === true) {
                    $('#message').css('color', 'green');
                    $('#message').html('Although looks like a good password, try to make it more stronger');
                    if ($('#password').val().length >= 9) {
                        $('#message').html('');
                        $('#message1').html('');
                    }
                    return true;
                }
            }
        }

        function NumAndWordRep() {
            var password = $('#password').val().toLowerCase();
            if (password.match(/(.)\1\1/)) {
                //	alert("Your Password cannot contain Character or Number repetition");
                $('#message7').css('color', 'red');
                $('#message7').html('Your Password cannot contain Character or Number repetition.');
                return false;
            }
            return true;
        }



        function newValPassPoilcy() {

            var password = $('#password').val();
            if (!password.match(
                    /^(?=.{6,})(?=.[0-9])(?=.[a-z])(?=.[A-Z])(?=.[!@#$%^&_+=\\\\-\\(\\)\\{\\}\\:\\;\\<\\>\\|\\,\\.\\?\\/\\'\\"]).$/
                ) || NumAndWordRep() === false) {

                $('#message8').css('color', 'red');
                $('#message8').html('Your password must contain:');
                if (!password.match(/^(?=.{6,}).*$/)) {
                    $('#message').css('color', 'red');
                    $('#message').html(' - minimum 6 characters.');

                }
                if (!password.match(/^(?=.[0-9]).$/)) {
                    $('#message2').css('color', 'red');
                    $('#message2').html(' - at least 1 Number.');

                }
                if (!password.match(/^(?=.[a-z]).$/)) {
                    $('#message3').css('color', 'red');
                    $('#message3').html(' - at least 1 Lowercase character.');

                }
                if (!password.match(/^(?=.[A-Z]).$/)) {
                    $('#message4').css('color', 'red');
                    $('#message4').html(' - at least 1 Uppercase character.');

                }
                if (!password.match(/^(?=.[!@#$%^&_+=\\\\-\\(\\)\\{\\}\\:\\;\\<\\>\\|\\,\\.\\?\\/\\'\\"]).*$/)) {

                    $('#message5').css('color', 'red');
                    $('#message5').html('	- at least 1 Special character.');

                }

                if (NumAndWordRep() === false) {
                    if (password.match(
                            /^(?=.{6,})(?=.[0-9])(?=.[a-z])(?=.[A-Z])(?=.[!@#$%^&_+=\\\\-\\(\\)\\{\\}\\:\\;\\<\\>\\|\\,\\.\\?\\/\\'\\"]).$/
                        )) {
                        $('#message8').html('');
                    }

                }
                return false;
            } else {

                return true;
            }

        }

        function checkEmail() {
            var email = $('#yourEmail').val();
            if ((email.indexOf(".") > 2) && (email.indexOf("@") > 0)) {
                return true;
            } else {
                return false;
            }

        }
    </script>
@endsection