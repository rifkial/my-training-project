@if(session('role-sarana') == 'admin')
    <style>
        .side-user{
            background: #7fb1f1 !important;
            padding: 18px;
        }
    </style>
@else
    <style>
        .side-user{
            background: #fff !important;
            padding: 18px;
        }
    </style>
@endif

<style>
    .side-user{
        background: #7fb1f1;
        padding: 18px;
    }

    .side-user .side-menu li a:hover{
        color: #1f2021 !important;
    }

</style>             


<div class="side-user p-2 pt-3">
    <a class="col-sm-12 media clearfix d-flex align-items-center" href="javascript:void(0);">
        <figure class="media-left media-middle mr-r-10 mr-b-0">
            <img src="{{$profile["file"]}}" alt="judul"
                class="media-object rounded-circle" alt="image profile" height="40px" width="40px">
        </figure>
        <div class="media-body hide-menu">
            <h4 class="media-heading mr-b-5 text-uppercase text-dark">{{$profile["nama"]}}</h4>
            <span class="user-type fs-12"></span>
        </div>
    </a>
    <div class="clearfix"></div>

    <ul class="nav in side-menu">
        <li class="my-4">
            <a href="{{route('edit-profile-sarana')}}">
                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                Ubah profil
            </a>
        </li>

        <li class="my-4">
            <a href="{{route('change_password_sarana')}}">
                <i class="fa fa-key" aria-hidden="true"></i>
                Ubah password
            </a>
        </li>
        
        <li class="my-4">
            <a href="{{route('auth.logout')}}">
                <i class="fa fa-sign-in" aria-hidden="true"></i>
                Logout
            </a>
        </li>
    </ul>
</div>