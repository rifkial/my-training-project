
<div class="col-12 d-flex flex-column align-items-center col-sm-3 mb-2">
	<img src="{{$buku['file']}}" class="img-fluid" width="200px">
    
          
    @if($buku['lampiran'] == null)
    <a href="" class="btn btn-outline-secondary btn-sm mt-2">
      Lampiran tidak tersedia
    @else
    <a href="{{ $buku['lampiran'] }}" class="btn btn-outline-secondary btn-sm mt-2">
      Lampiran tersedia
    @endif
        <i class="far fa-file-alt"></i>
    </a>
    
</div>

<div class="col-12 col-sm-9">
	<div class="widget-bg mb-5">
		<div class="container">
            <span class="mb-1 d-flex">
                <i class="list-icon fa fa-book fa-lg"></i>
                <h6 class="my-0 ml-2">{{$buku['gmd']}}</h6>
            </span>        
			<h4>{{$buku['judul']}}</h4>
			<span class="mb-2">
				@foreach($buku['topik'] as $key => $topik)
    				<a href="{{ route('perpus-topik-detail',$topik['id'])}}" class="badge badge-pill badge-light text-dark px-3 py-2">{{$topik['nama']}}</a>
    			@endforeach
			</span>
			<p>{{$buku['desc_buku']}}</p>
		</div>
        <div class="widget-body clearfix px-3">
            
            <div class="tabs tabs-bordered">
                <ul class="nav nav-tabs">
                    <li class="nav-item "><a class="nav-link" href="#home-tab-bordered-1" data-toggle="tab" aria-expanded="true">Detail Buku</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#profile-tab-bordered-1" data-toggle="tab" aria-expanded="true">Ketersedian</a>
                    </li>
                </ul>
                <!-- /.nav-tabs -->
                <div class="tab-content">
                    <!-- Detail Buku -->
                    <div class="tab-pane" id="home-tab-bordered-1">
                        <div class="row">
                        	<div class="col-4 col-sm-2">
                        		<h6>ISBN</h6>
                                <h6>Deskripsi fisik</h6>
                        		<h6>Penerbit</h6>
                        		<h6>Tahun terbit</h6>
                        		<h6>Pengarang</h6>
                        		<h6>Bahasa</h6>
                        		<h6>Halaman</h6>
                        		<h6>Lokasi</h6>
                        		
                        	</div>
                        	<div class="col-8">
                        		<h6>: {{$buku['ISBN']}}</h6>
                                <h6>: {{$buku['desc_fisik']}}</h6>
                        		<h6>: {{$buku['penerbit']}}</h6>
                        		<h6>: {{$buku['tahun_terbit']}}</h6>
                        		<h6>:      
                                    @foreach($buku['penulis'] as $key => $penulis)
                                    <a href="{{ route('perpus-pengarang-detail',$penulis['id'])}}" class="badge badge-secondary">
                                        {{$penulis['nama']}}
                                    </a>
                                    @endforeach  
                        		</h6>
                        		<h6>: {{$buku['bahasa']}}</h6>
                        		<h6>: {{$buku['halaman']}}</h6>
                        		<h6>: {{$buku['rak']}}</h6>
                        		
                        	</div>
                        </div>
                    </div>

                    <!-- Item Tersedia -->
                    <div class="tab-pane" id="profile-tab-bordered-1">
                        <div class="list-group bg-transparent auto">
                            @foreach($items as $item)
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-end">
                                <span class="mr-auto">{{$item['buku']}}-{{$item['kode']}}</span> 
                                <span class="badge badge-pill badge-{{ $item['status_dipinjam'] == 'tersedia' ? 'success' : 'danger' }} fs-12 mr-1 my-auto">{{$item['status_dipinjam']}}</span>
                            </a>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.tabs -->
        </div>
        <!-- /.widget-body -->
    </div>
    <!-- /.widget-bg -->

</div>



