
<div class="col-3">
	<img src="{{$buku['file']}}" class="img-fluid" width="200px">
    <button type="button" class="btn btn-outline-secondary btn-sm mt-2">
        <i class="far fa-file-alt"></i>
        @if($buku['lampiran'] == null)
          Lampiran tidak tersedia
        @else
          Lampiran tersedia
        @endif
    </button>

</div>

<div class="col-9">
	<div class="widget-bg mb-5">
		<div class="container">
            <span class="mb-1 d-flex">
                <i class="list-icon fa fa-book fa-lg"></i>
                <h6 class="my-0 ml-2">{{$buku['gmd']}}</h6>
            </span>
			<h4>{{$buku['judul']}}</h4>
			<span class="mb-2">
				@foreach($buku['topik'] as $key => $topik)
    				<a href="#" class="badge badge-pill badge-light text-dark px-3 py-2">{{$topik['nama']}}</a>
    			@endforeach
			</span>
			<p>{{$buku['desc_buku']}}</p>
		</div>

        <!-- /.widget-body -->
    </div>
    <!-- /.widget-bg -->
</div>


<!-- modal -->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-edit-item" action="javascript:void(0)">
          <input type="hidden" class="form-control" id="inputId" name="id">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">Kode</label>
              <input type="text" name="kode" class="form-control" id="inputKode">
            </div>
            <div class="form-group col-md-6">
              <label for="exampleFormControlSelect1">Kondisi</label>
                <select name="kondisi" class="form-control" id="inputKondisi">
                  <option value="1">Ada</option>
                  <option value="2">Rusak</option>
                  <option value="3">Hilang</option>
                </select>
            </div>
            <div class="form-group col-md-6">
              <label for="inputEmail4">Tanggal masuk</label>
              <input name="tanggal" type="date" class="form-control" id="inputTanggal">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>





