<div class="container d-flex flex-wrap justify-content-sm-start justify-content-between px-4">
	<!-- Daftar buku -->
	@forelse($bukus as $buku)
	<div class="col-5 col-md-3 col-xl-2 items mx-2 mx-md-2 my-3 {{ $loop->first ? 'gold' : '' }}">
		<a href="{{ route('detail-buku',$buku['id']) }}">
			<div class="card-body p-1 pt-3">
			<div class="card-image fit-height">
				<img src="{{$buku['file']}}">
			</div>
			<h6 class="">{{$buku['judul']}}</h6>
		</div>
		</a>
	</div>

    @empty

    <h5>Pustaka tidak ada</h5>

	@endforelse
	<!-- end daftar buku -->
</div>



