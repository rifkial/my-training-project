<style type="text/css">
	* {
	    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
	    color-adjust: exact !important;                 /*Firefox*/
	}

	.label-body{
		border: 1px solid black;
		width: 300px;
		background-color: white;
	}
		.label-head{
			border-bottom: 1px solid black;
			display: flex;
		    align-items: center;
		    justify-content: center;
		    padding: 10px;
		}

	.label-content{
		display: flex;
	    flex-direction: column;
	    justify-content: center;
	    align-items: center;
	}

	img{
		height: 50px;
		margin: 0 10px;

	}

	.label-main{
		display: flex;
	}

	h5,h6{
		margin : 5px 0;
	}

	.label-text{
		text-align: center;
	}

	.label-barcode{
		display: flex;
	    align-items: center;
	    justify-content: end;
	    background-color: white;
	    width: 80px;
	    border-bottom: 1px solid black;
	    border-top: 1px solid black;
	    border-right: 1px solid black;
	    position: relative;
	}

	.barcode{
		-webkit-transform: rotate(90deg);
	    -moz-transform: rotate(90deg);
	    -o-transform: rotate(90deg);
	    -ms-transform: rotate(90deg);
	    transform: rotate(90deg);
	    display: flex;
	    right: -26px;
   		position: absolute;
	}

	@media print{

		.barcode{
		    right: 0px;
	   		
		}

		.label-head-text{
			margin : 5px 0 !important;
		}

		.label-body{
			width: 250px;
		}

		.label-barcode{
		    width: 50px;
		}

	}
	
	
</style>

