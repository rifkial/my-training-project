
<style type="text/css">
	* {
	    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
	    color-adjust: exact !important;                 /*Firefox*/
	}

	h6,p{
		margin-top: 0.25rem !important;
    	margin-bottom: 0.25rem !important;
	}

	img{
		height: 50px;
	}

	.kartu-body{
		display: flex;
		background-color: white;
		border-radius: 10px;
		height: 250px;
		width: 480px;
	    background-repeat:no-repeat;
	    background-size:cover;
	}

		.kartu-left{
			flex: 2;
		    padding: 30px 0 10px 0;
		    display: flex;
		    flex-direction: column;
		    justify-content: space-between;
		}

			.kartu-title{
				display: flex;
			    flex-direction: column;
			    align-items: center;
			}
				.kartu-sistem{
					margin-top: 10px;
				    font-size: 17px;
				    font-weight: 700;
				}

				.kartu-instansi{
					font-size: 17px;
				}

			.kartu-identify{
				display: flex;
			    flex-direction: column;
			    align-items: center;
			    margin-top: 20px;
			}

		.kartu-right{
			flex: 1;
    		padding: 30px 0;
    		display: flex;
    		flex-direction: column;
    		justify-content: space-between;
		}
			.kartu-data{
				align-items: center;
				display: flex;
			    flex-direction: column;
			    align-items: center;
			}

			.kartu-user{
				display: flex;
    			justify-content: center;
			}

	.kartu-body-back{
		margin-top: 20px;
		display: flex;
		flex-direction: column;
		border-radius: 10px;
		height: 250px;
		width: 480px;
	    background-repeat:no-repeat;
	    background-size:cover;
	}
		.kartu-head{
			display: flex;
		    justify-content: flex-start;
		    align-items: center;
		    padding: 0;
		}


			.kartu-logo{
				margin: 0 20px 0 20px;
			}

			.kartu-logo-back{
				margin: 0 10px 0 20px;
			}

		.kartu-ket{
			padding: 0 25px;
		}

			ol{
				margin : 0 ;
			}

			ul{
				padding: 0 20px !important;
				margin-bottom: 0;
			}

		.kartu-foot{
			display: flex;
		    justify-content: end;
		    padding: 0 40px;
		    margin: 0 0 -5px 0;
		}

		.kartu-footer{
			display: flex;
		    justify-content: end;
		    padding: 0 10px;
		    margin: 5px;
		}

		.kartu-nama-ttd{
		    display: flex;
		    flex-direction: column;
		    align-items: center;
		}

		.kartu-ttds{
			font-size: 11px;
			margin-bottom: -5px;
		}



	@media print{

        body{
            margin : 0;
        }

		h4{
			margin: 15px 0;
		}

        .kartu{
            display: inline-block;
            margin: 0 2px;
        }

		.kartu-body,.kartu-body-back{
			height: 205px;
			width: 350px;
            background-color : lightblue;
		}

        .kartu-body-back{
            margin-top: 0px;
            position :absolute;
            right: 0;
            top: 0;
        }

		.kartu-head{
			margin-bottom: 0;
			padding-bottom: 0px;
		}

		#kartu-peraturan{
			margin: 2px 0 0 0 !important;
		}

		.kartu-sistem{
			margin-top: 10px;
		    font-size: 14px;
		    font-weight: 700;
            text-align: center;
		}

		.kartu-instansi{
			font-size: 14px;
            text-align: center;
		}

		.kartu-user-tipe{
			font-weight: bold;
		}

		.kartu-ket{
			margin: 0 0 3px 0;
		}

		ol{
			margin : 0 ;
		}


		ol li{
			font-size: 11px;
		}

		.kartu-foot{
		    padding: 0 35px;

		}

		.kartu-ttds{
			font-size: 8px;
			margin-bottom: 0;
		}

        .kartu-bottom{
            position : absolute;
            bottom: 0;
            right: 0;
        }
	}


</style>

<div style="height: 206px;">

    <!-- depan -->
    <div class="kartu">
        <div class="kartu-body" style="background-image:url('{{$kartu['cover']}}') !important;">
            <!-- left -->
            <section class="kartu-left">
                <div class="kartu-title">
                    <div class="kartu-logo">
                    <img src="{{$kartu['logo']}}">
                    </div>
                    <div class="kartu-sistem">{{$kartu['nama_sistem']}}</div>
                    <div class="kartu-instansi">{{$kartu['nama_instansi']}}</div>
                </div>
                <div class="kartu-identify">
                    <div>{{$user['nama']}}</div>
                    <div class="kartu-barcode">
                        {!! DNS1D::getBarcodeHTML($user['no_anggota'],"C128",2.1,24) !!}
                    </div>
                </div>

            </section>

            <!-- right -->
            <section class="kartu-right">
                <div class="kartu-data">
                    <p class="kartu-user-tipe">{{$user['role']}}</p>
                    <p class="kartu-user-id">{{$user['no_anggota']}}</p>
                    <!-- <p>Masa berlaku</p> -->
                </div>
                <div class="kartu-user">
                    <img src="{{$user['foto']}}" style="height: 60px;">
                </div>

            </section>
        </div>
    </div>

    <!-- belakang -->
    <div class="kartu">
        <div class="kartu-body-back" style="background-image:url('{{$kartu['cover']}}') !important;">
            <div class="kartu-head">
                <img class="kartu-logo-back" src="{{$kartu['logo']}}" style="height: 35px;">
                <h4>{{$kartu['nama_sistem']}}</h4>
            </div>
            <div class="kartu-ket">
                <p id="kartu-peraturan" style="margin: 5px;">Peraturan</p>

                    {!! $kartu['peraturan'] !!}

            </div>

            <div class="kartu-bottom">
                <div class="kartu-foot">
                    <div class="kartu-ttd" style="position: absolute;">
                        <img class="kartu-logos" src="{{$kartu['ttd_jabatan']}}" style="height: 35px;">
                    </div>
                    <div class="kartu-stempel">
                        <img src="{{$kartu['stempel']}}" style="height: 35px;">
                    </div>
                </div>

                <div class="kartu-footer">
                    <div class="kartu-nama-ttd">
                        <div class="kartu-ttds">{{$kartu['nama_jabatan']}}</div>
                        <div class="kartu-ttds">{{$kartu['jabatan']}}</div>
                        <div class="kartu-ttds">NIP. {{$kartu['nip_jabatan']}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

