<div class="modal fade bd-example-modal-lg" id="modal-store-master" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Tambah</h5>
        </div>

        <div class="modal-body">
    		<form id="form-add-master" action="javascript:void(0)" method="post">
			@csrf
    		<div class="form-row">
			    <div class="form-group col-md-8">
			      <label for="inputEmail4">Nama</label>
			      <input type="text" class="form-control" name="nama" id="nama" placeholder="nama">
			    </div>

			    <div class="form-group col-md-4 {{Request::segment(4) == 'jenis' || Request::segment(4) == 'koleksi' || Request::segment(4) == 'topik' || Request::segment(4) == 'pengarang' || Request::segment(4) == 'rak' ? 'd-none' : ''}}">
			      <label for="inputPassword4">Kode</label>
			      <input type="text" class="form-control" name="kode" id="kode" placeholder="kode">
			    </div>
			</div>

			<div class="form-group {{Request::segment(4) != 'pengarang' ? 'd-none' : 'd-block' }} ">
			    <label for="exampleInputPassword1">Biodata</label>
			    <input type="text" name="biodata" id="biodata" class="form-control" id="exampleInputPassword1" placeholder="biodata">
			  </div>
    	</div>

    	<div class="modal-footer">
	    	<button type="submit" class="btn btn-primary">Tambah</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
	    </div>
	    </form>
    </div>
  </div>
</div>