<div class="modal fade bd-example-modal-lg" id="modal-store-master" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Tambah</h5>
        </div>

        <div class="modal-body">
        	<form action="{{ route('user-perpus-store') }}" method="POST" enctype="multipart/form-data">
			@csrf
				<input id="id" name="id" type="hidden" >
	    		<div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l16">Foto</label>
	                <div class="col-md-9">
	                    <input id="image" type="file" name="image" >
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Nama</label>
	                <div class="col-md-9">
	                    <input id="nama" class="form-control" name="nama" type="text">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Username</label>
	                <div class="col-md-9">
	                    <input id="username" class="form-control" name="username" type="text">
	                </div>
	            </div>
	            <div class="form-group row" id="password">
	                <label class="col-md-3 col-form-label" for="l0">Password</label>
	                <div class="col-md-9">
	                    <input class="form-control" name="password" type="password">
	                </div>
	            </div>

	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Nomor Induk</label>
	                <div class="col-md-9">
	                    <input id="no_induk" class="form-control" name="no_induk"  type="number">
	                </div>
	            </div>

	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Tipe Anggota</label>
	                <div class="col-md-9">
	                    <select class="form-control" name="tipe_anggota" id="tipe_anggota">
	                      @foreach($tipe as $tp)
					      	<option value="{{$tp['id']}}">{{$tp['nama']}}</option>
					      @endforeach
					    </select>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Role</label>
	                <div class="col-md-9">
	                    <select class="form-control" name="role" id="role">
	                    	<option value="siswa">siswa</option>
	                    	<option value="guru">guru</option>
					    </select>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Email</label>
	                <div class="col-md-9">
	                    <input class="form-control" id="email" name="email" type="email">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Telepon</label>
	                <div class="col-md-9">
	                    <input class="form-control" id="telepon" name="telepon" type="number">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Tempat Lahir</label>
	                <div class="col-md-9">
	                    <input class="form-control" id="tempat_lahir" name="tempat_lahir" type="text">
	                </div>
	            </div>
	            <fieldset class="form-group">
				    <div class="row">
				      <legend class="col-form-label col-md-3 pt-0">Jenis Kelamin</legend>
				      <div class="col-md-9">
				        <div class="form-check">
				          <input class="form-check-input" type="radio" id="gridRadios1" name="jenkel" value="l" checked>
				          <label class="form-check-label" for="gridRadios1">
				            Laki-laki
				          </label>
				        </div>
				        <div class="form-check">
				          <input class="form-check-input" type="radio" id="gridRadios2" name="jenkel" value="p">
				          <label class="form-check-label" for="gridRadios2">
				            Perempuan
				          </label>
				        </div>
				      </div>
				    </div>
			  	</fieldset>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Tanggal Lahir</label>
	                <div class="col-md-9">
	                    <input class="form-control" id="tgl_lahir" name="tgl_lahir" type="date">
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-3 col-form-label" for="l0">Alamat</label>
	                <div class="col-md-9">
	                    <input class="form-control" id="alamat" name="alamat" type="text">
	                </div>
	            </div>

	            <div class="modal-footer">
			    	<button type="submit" class="btn btn-primary btn-action">Tambah</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
			    </div>
	        </form>
        </div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-detail-user" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 class="modal-title" id="myLargeModalLabel">Detail</h5>
        </div>
        <div class="modal-body" id="modal-detail-body">

        </div>
    </div>
  </div>
</div>

