
<div class="container d-flex flex-wrap justify-content-sm-start justify-content-between mb-3">

	<!-- Daftar buku -->
	@foreach($pusdigs as $pusdig)
	<div class="{{Request::segment(3) == 'pengarang' ? 'col-sm-3 col-md-3' : 'col-sm-2 col-md-2 my-2'}} col-5 items bg-white mx-sm-3 mx-1 px-0">
		<a href="{{ route('detail-pusdig',$pusdig['id']) }}">
			<div class="card-body p-1 pt-0">
			<div class="card-image fit-height">
				<img src="{{$pusdig['foto']}}">
			</div>
			<h6 class="px-2 mt-2">{{$pusdig['judul']}}</h6>
		</div>
		</a>
	</div>

	@endforeach

	<!-- end daftar buku -->

</div>
