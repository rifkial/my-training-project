
<div class="col-12 d-flex flex-column align-items-center col-sm-3 mb-2">
	<img src="{{$buku['foto']}}" class="img-fluid" width="200px">
    <a href="{{$buku['lampiran']}}" class="btn btn-outline-secondary btn-sm mt-2">
        <i class="far fa-file-alt"></i>
          Download
    </a>
    {{-- <a href="{{ route('read-pusdig',$buku['id'])}}" class="btn btn-outline-secondary btn-sm mt-2">
        <i class="far fa-file-alt"></i>
          Baca
    </a> --}}

</div>

<div class="col-12 col-sm-9">
	<div class="widget-bg mb-5">
		<div class="container">
            <span class="mb-1 d-flex">
                <i class="list-icon fa fa-book fa-lg"></i>
            </span>
			<h4>{{$buku['judul']}}</h4>
			<span class="mb-2">
				@foreach($buku['topik'] as $key => $topik)
    				<a href="{{ route('perpus-topik-detail',$topik['id'])}}" class="badge badge-pill badge-light text-dark px-3 py-2">{{$topik['nama']}}</a>
    			@endforeach
			</span>
			<p>{{$buku['deskripsi']}}</p>
		</div>
        <div class="widget-body clearfix px-3">

            <div class="tabs tabs-bordered">
                <ul class="nav nav-tabs">
                    <li class="nav-item "><a class="nav-link" href="#home-tab-bordered-1" data-toggle="tab" aria-expanded="true">Detail Buku</a>
                    </li>
                </ul>
                <!-- /.nav-tabs -->
                <div class="tab-content">
                    <!-- Detail Buku -->
                    <div class="tab-pane active" id="home-tab-bordered-1">
                        <div class="row">
                        	<div class="col-4 col-sm-2">
                        		<h6>ISBN</h6>
                        		<h6>Penerbit</h6>
                        		<h6>Tahun terbit</h6>
                        		<h6>Pengarang</h6>
                        		<h6>Bahasa</h6>
                        		<h6>Halaman</h6>
                        	</div>
                        	<div class="col-8">
                        		<h6>: {{$buku['ISBN']}}</h6>
                        		<h6>: {{$buku['penerbit']}}</h6>
                        		<h6>: {{$buku['tahun_terbit']}}</h6>
                        		<h6>:
                                    @foreach($buku['penulis'] as $key => $penulis)
                                    <a href="{{ route('perpus-pengarang-detail',$penulis['id'])}}" class="badge badge-secondary">
                                        {{$penulis['nama']}}
                                    </a>
                                    @endforeach
                        		</h6>
                        		<h6>: {{$buku['bahasa']}}</h6>
                        		<h6>: {{$buku['halaman']}}</h6>
                        	</div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.tabs -->
        </div>
        <!-- /.widget-body -->
    </div>
    <!-- /.widget-bg -->

</div>



