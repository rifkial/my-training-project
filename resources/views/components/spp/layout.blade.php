@props([
    'currentPage' => '',
])

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{{ asset("asset/css/pace.css") }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("asset/demo/favicon.png") }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>&#91;Mock Title&#93; App SPP</title>
    <!-- CSS -->
    <link href="{{ asset("asset/vendors/material-icons/material-icons.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("asset/vendors/mono-social-icons/monosocialiconsfont.css") }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset("asset/vendors/weather-icons-master/weather-icons.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("asset/vendors/weather-icons-master/weather-icons-wind.min.css") }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset("asset/css/style.css") }}" rel="stylesheet" type="text/css">

    <style>
        .dropdown-list-group-menu-profil {
            /* background-color: rgba(0, 255, 255, 0.048); */
        }

        .dropdown-list-group-menu-profil .item-menu-profil {
            display: flex;
            padding: 20px 0;
        }

        .my-custom-scrollbar {
            position: relative;
            height: 200px;
            overflow: auto;
        }

        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <!-- Styling custom halaman yang disisipkan dari masing-masing template halaman/konten -->
    {{-- Slot: --}}{{ $halamancss ?? '' }}
    @stack('gaya_custom')

    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        {{-- TODO: ekstrak komponen nav barnya --}}
        <nav class="navbar">
            <!-- Logo Smart School -->
            <div class="navbar-header bg-secondary">
                <a href="{{ route('program', 'spp') }}" class="navbar-brand text-center">
                    <img class="logo-expand" alt="" src="{{ asset('asset/img/smartschool_p.png') }}">
                    <img class="logo-collapse" alt="" src="{{ asset('asset/img/putih.png') }}">
                </a>
            </div>
            <!-- /.navbar-header -->

            <!-- Left Menu & Sidebar Toggle -->
            <!-- Tombol Menu ketika media query layar kecil -->
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <!-- /.navbar-left -->

            <div class="spacer"></div>
            <!-- Dropdown profil user -->
            <x-spp.user-dropdown />
            <!-- /.navbar-right -->
        </nav>
        <!-- /.navbar -->

        <div class="content-wrapper">
            <!-- SIDEBAR -->
            <aside class="site-sidebar clearfix">
                <x-spp.sidebar-nav :current-menu="$currentPage" />
            </aside>
            <!-- /.site-sidebar -->

            <main class="main-wrapper clearfix">
                <!-- Page Title Area -->
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h5 class="mr-0 mr-r-5">SPP</h5>
                        <p class="mr-0 text-muted d-none d-md-inline-block">Informasi tagihan keuangan sekolah</p>
                    </div>
                    <!-- /.page-title-left -->

                    <div class="page-title-right d-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('program', 'spp') }}">Beranda</a>
                            </li>
                            @if ($currentPage)
                            <li class="breadcrumb-item active">{{ $currentPage }}</li>
                            @endif
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->

                <!-- =================================== -->
                <!-- Bagian konten ada di sini ========= -->
                <!-- =================================== -->
                <div class="widget-list">
                    {{ $slot }}
                </div>
                <!-- /.widget-list -->

            </main>
            <!-- /.main-wrappper -->
        </div>
        <!-- /.content-wrapper -->

        <!-- FOOTER -->
        {{-- TODO: pasang tahunnya dinamis, perhatikan timezone user/browser karena PHP di server --}}
        <footer class="footer text-center clearfix">2021 &copy; Smart School</footer>
    </div>
    <!--/ #wrapper -->

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="{{ asset("asset/js/bootstrap.min.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script src="{{ asset("asset/vendors/charts/utils.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
    <script src="{{ asset("asset/vendors/charts/excanvas.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mithril/1.1.1/mithril.js"></script>
    <script src="{{ asset("asset/vendors/theme-widgets/widgets.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jquery.vmap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.usa.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

    <script src="{{ asset("asset/js/theme.js") }}"></script>
    <script src="{{ asset("asset/js/custom.js") }}"></script>

    <!-- script custom yang disisipkan di masing-masing template Blade halaman -->
    {{-- Slot: --}}{{ $halamanjs ?? '' }}
    @stack('js_custom')
</body>

</html>
