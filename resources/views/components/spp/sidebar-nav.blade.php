@props([
    'current-menu' => '',
])

<nav class="sidebar-nav">
    @switch (session('role'))
        @case('siswa')
        <ul class="nav in side-menu">
            <li class="menu-item-has-children {{ $currentMenu === "dashboard" ? "current-page" : "" }}">
                <a href="{{ route('program', 'spp') }}"><i class="list-icon material-icons">dashboard</i> <span class="hide-menu">Beranda</span></a>
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "tagihan" ? "current-page" : "" }}">
                <a href="javascript:void(0)"><i class="list-icon material-icons">school</i> <span class="hide-menu">SPP</span></a>
                <ul class="list-unstyled sub-menu">
                    <li><a href="javascript:void(0)">Tagihan Terakhir</a>
                    </li>
                    <li><a href="{{ route('spp-tagihan') }}">Riwayat</a>
                    </li>
                </ul>
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "pembayaran" ? "current-page" : "" }}">
                <a href="javascript:void(0)"><i class="list-icon material-icons">business_center</i> <span class="hide-menu">Pembayaran</span></a>
                <ul class="list-unstyled sub-menu">
                    <li><a href="javascript:void(0)">Konfirmasi</a>
                    </li>
                    <li><a href="{{ route('spp-info') }}">Informasi Pembayaran</a>
                    </li>
                </ul>
            </li>
        </ul>
        @break

        @case('tata-usaha')
        <ul class="nav in side-menu">
            <li class="menu-item-has-children {{ $currentMenu === "dashboard" ? "current-page" : "" }}">
                <a href="{{ route('program', 'spp') }}"><i class="list-icon material-icons">dashboard</i> <span class="hide-menu">Beranda</span></a>
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "siswa" ? "current-page" : "" }}">
                <a href="{{ route('spp-siswa') }}">
                    <i class="list-icon material-icons">account_box</i>
                    <span class="hide-menu">Siswa</span>
                </a>
                {{-- <ul class="list-unstyled sub-menu">
                    <li>
                        <a href="{{ route('spp-siswa') }}">Siswa</a>
                    </li>
                </ul> --}}
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "siswa" ? "current-page" : "" }}">
                <a href="javascript:void(0)">
                    <i class="list-icon material-icons">payment</i>
                    <span class="hide-menu">Pembayaran</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li>
                        <a href="{{ route('spp-konfirmasi') }}">Konfirmasi</a>
                    </li>
                    <li>
                        <a href="{{ route('spp-transaksi') }}">Transaksi</a>
                    </li>
                </ul>
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "siswa" ? "current-page" : "" }}">
                <a href="javascript:void(0)">
                    <i class="list-icon material-icons">notifications</i>
                    <span class="hide-menu">Pusat Notifikasi</span>
                </a>
            </li>

            <li class="menu-item-has-children {{ $currentMenu === "laporan" ? "current-page" : "" }}">
                <a href="{{ route('spp-laporan') }}">
                    <i class="list-icon material-icons">library_books</i>
                    <span class="hide-menu">Laporan</span>
                </a>
                <ul class="list-unstyled sub-menu">
                    <li><a href="{{ route('spp-laporan_penerimaan') }}">Penerimaan SPP</a>
                    </li>
                </ul>
            </li>

            <li class="menu-item-has-children {{-- {{ $currentMenu === "tagihan" ? "current-page" : "" }} --}}">
                <a href="javascript:void(0)"><i class="list-icon material-icons">settings</i> <span class="hide-menu">Pengaturan</span></a>
                <ul class="list-unstyled sub-menu">
                    <li>
                        <a href="{{ route('spp-setting_tagihan') }}">Tagihan</a>
                    </li>
                    <li>
                        <a href="{{ route('spp-pos_pemasukan') }}">Pos Pemasukan</a>
                    </li>
                    <li>
                        <a href="{{ route('spp-pos') }}">Jenis Pos</a>
                    </li>
                </ul>
            </li>
        </ul>
        @break
    @endswitch
    <!-- /.side-menu -->
</nav>
