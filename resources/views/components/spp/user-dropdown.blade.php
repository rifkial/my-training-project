@php
    $itemMenu = [
        [
            'label' => 'Profil',
            'icon' => 'account_circle',
        ],
        // Menu lain kalau mau ditambahkan...
        // [
        //     'label' => 'Pengaturan',
        //     'icon' => 'settings',
        // ],
    ];
@endphp
<style>
    .card-heading-extra{
        padding:10px !important;
        margin:10px !important;
    }

    .dropdown-card.dropdown-card-wide {
        min-width: auto;
        width: 15rem;
        max-width: 100vw;
    }

    .dropdown-card{
        padding: 10px auto;
    }


</style>

<ul class="nav navbar-nav">
    <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown"><span class="avatar thumb-sm"><img src="/asset/demo/users/user-image.png" class="rounded-circle" alt=""> <i class="material-icons list-icon">expand_more</i></span></a>
        <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-wide">
            <div class="card">
                <header class="card-heading-extra">
                    <div class="row flex-column">
                        <!-- Identitas user -->
                        <div class="col-12">
                            <h3 class="mr-b-10 sub-heading-font-family fw-300">
                                {{ ucwords($profil['nama']) }}
                            </h3>
                            <span>
                            @if (session('role') === 'tata-usaha')
                                Tata Usaha Keuangan
                            @elseif (session('role') === 'ortu')
                                Wali Siswa
                            @endif
                            </span>
                        </div>

                        <div class="col-8 my-2">
                            <a href="{{ route('auth.logout') }}" class="mr-t-10">
                                <i class="material-icons list-icon">person</i>
                                Profil
                            </a>
                        </div>

                        <!-- Tombol Logout -->
                        <div class="col-8 my-2">
                            <a href="{{ route('auth.logout') }}" class="mr-t-10">
                                <i class="material-icons list-icon">power_settings_new</i>
                                Logout
                            </a>
                        </div>
                    </div>
                    <!-- /.row -->
                </header>

                <!-- <section class="card-header">
                    Menu
                </section>
                <ul class="list-unstyled dropdown-list-group-menu-profil">
                    @foreach ($itemMenu as $menu)
                    <li class="item-menu-profil">
                        <a href="javascript:void(0)" style="display:flex;">
                            <i class="material-icons list-icon mr-r-20 text-muted">{{ $menu['icon'] }}</i>
                            <span class="h5 fs-15 mt-1 mb-0 sub-heading-font-family block">
                                {{ $menu['label']}}
                            </span>
                        </a>
                    </li>
                    @endforeach
                </ul> -->
            </div>
        </div>
    </li>
</ul>
