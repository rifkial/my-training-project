<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body class="header-dark sidebar-light sidebar-collapse">
    <div id="wrapper" class="wrapper">
        <nav class="navbar">
            <div class="navbar-header">
                <a href="#" class="navbar-brand admin">
                    <img class="logo-expand" alt="" src="{{ session('logo_sekolah') != null ? session('logo_sekolah') :  asset('asset/img/sma.png') }}" style="max-height: 59px;">
                    <img class="logo-collapse" alt="" src="{{ session('logo_sekolah') != null ? session('logo_sekolah') :  asset('asset/img/sma.png') }}" style="max-height: 59px;">
                    <span class="text-white logo-expand text-uppercase"> {{ str_replace('_', ' ', session()->get('config')) }} </span>
                    <span class="text-white logo-collapse text-uppercase"> {{ str_replace('_', ' ', session()->get('config')) }} </span>
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="material-icons list-icon">menu</i>
                    </a>
                </li>
            </ul>
            <form class="navbar-search d-none d-md-block" role="search">
                <a href="javascript:void(0);" class="remove-focus">
                    <i class="material-icons">clear</i>
                </a>
            </form>
            <div class="spacer"></div>
            <!-- ini header -->
                @include('includes.profil_header') 
        </nav>
                        <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                            <img src="{{ asset('asset/demo/users/user-image.png') }}"
                                class="media-object rounded-circle" alt="">
                        </figure>
                        <div class="media-body hide-menu">
                            <h4 class="media-heading mr-b-5 text-uppercase">Scott Adams</h4>
                            <span class="user-type fs-12">Edit Profile (...)</span>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                    <ul class="nav in side-menu">
                        <li>
                            <a href="page-profile.html">
                                <i class="list-icon material-icons">face</i>
                                My Profile
                            </a>
                        </li>
                        <li>
                            <a href="app-inbox.html">
                                <i class="list-icon material-icons">mail_outline</i>
                                Inbox
                            </a>
                        </li>
                        <li>
                            <a href="page-lock-screen.html">
                                <i class="list-icon material-icons">settings</i>
                                Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="page-login.html">
                                <i class="list-icon material-icons">settings_power</i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- ini sidebar -->
                @if(session('role') == NULL)
                    @include('includes.program-mutu.mutu')
                @else
                    @php
                    $program = Session::get('config');
                    $role = Session::get('role');
                    @endphp
                    @include('includes.program-'.$program.'.menu.menu-'.$role)
                @endif   

            </aside>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.html">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Collapsed Sidebar</li>
                        </ol>
                        <div class="d-none d-sm-inline-flex justify-center align-items-center">
                            <a href="javascript: void(0);"
                                class="btn btn-outline-primary mr-l-20 btn-sm btn-rounded hidden-xs hidden-sm ripple"
                                target="_blank">Buy Now
                            </a>
                        </div>
                    </div>
                </div>
                <div class="widget-list">
                    @yield('content')
                </div>
            </main>
        </div>
        @include('includes.footer')
    </div>
    @include('includes.foot')
</body>

</html>
