<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>
<style>
    .pace{
        display: none;
    }
    .sidebar-dark .site-sidebar {
        background: #2471d2;
        border-color: rgba(255, 255, 255, 0.2);
    }

    .sidebar-dark .side-menu :not([class*="color-"])>.list-icon,
    .sidebar-dark .side-menu .menu-item-has-children>a::before {
        color: #ffffff;
    }

    .sidebar-dark .side-menu li a {
        color: #ffffff;
    }

    .sidebar-dark .side-menu li:hover,
    .sidebar-dark .side-menu li.active {
        background: #5792dc;
    }
</style>

@if(session('role') == 'user-mutu')
    <style>
        .sidebar-dark .site-sidebar {
            background: #2471d2 !important;
            border-color: rgba(255, 255, 255, 0.2);
        }

        .main-wrapper{
            background: #ffffff !important;
        }

        .title-content{
            font-family: 'Poppins', sans-serif;
            font-size: 40px;
            color: #103e79;
        }
    </style>
@endif

<body class="header-light sidebar-dark sidebar-expand">
    <div id="wrapper" class="wrapper">
        <nav class="navbar">
            <div class="navbar-header">
                <a href="#" class="navbar-brand bg-info">
                    <span class="text-white logo-expand text-uppercase"  style="{{ session('role') == 'admin' ? "font-size: 14px;" : "" }}">
                        {{ session('role') != "admin" ? str_replace('_', ' ', session()->get('nama_program')) : session('sekolah') }}
                    </span>
                    <span class="text-white logo-collapse text-uppercase" style="{{ session('role') == 'admin' ? "font-size: 14px;" : "" }}">
                        {{ session('role') != "admin" ? str_replace('_', ' ', session()->get('nama_program')) : session('sekolah') }}
                    </span>
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle">
                    <a href="javascript:void(0)" class="ripple">
                        <i class="fa fa-bars fa-lg" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
            <div class="spacer"></div>
            <!-- ini header -->
            @if(session('role') == 'user-mutu')
                @include('components.penjamin_mutu.header-profil')
            @elseif(session('role') == 'user-sarpras' || session('role') == 'admin-sarpras' )
                @include('components.sarana.header-profil-sarana')
            @else
                @include('includes.profil_header')
            @endif
        </nav>
        <div class="content-wrapper">
            <aside class="site-sidebar scrollbar-enabled clearfix">
                @php
                    $program = Session::get('config');
                    $role = Session::get('role');
                    $roles = Session::get('role-sarana');
                @endphp


                @if (session('role') == 'admin')
                    @include('includes.menu-admin')
                @elseif(session('role') == 'superadmin')
                    @include('includes.menu-superadmin')
                @elseif(session('role') == 'induk-admin')
                    @include('includes.program-induk.menu.menu-induk-admin')
                @elseif(session('role') == 'user-mutu')
                    @include('includes.program-mutu.mutu')
                @elseif(session('role') == 'user-sarpras' || session('role') == 'admin-sarpras')
                    @include('includes.program-sarana.menu.menu-'.$roles.'')
                @else
                    @include('includes.program-'.$program.'.menu.menu-'.$role)
                @endif
            </aside>
            <main class="main-wrapper clearfix">
                <div class="row page-title clearfix" style="height: 0;">
                </div>
                <div class="widget-list">
                    @yield('content')
                </div>
            </main>
        </div>
        @include('includes.footer')
    </div>
    @include('includes.foot')
</body>

</html>
