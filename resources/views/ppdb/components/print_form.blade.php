@extends('ppdb.layouts.app')
@section('ppdb.components')
    <style type="text/css">
        .table {
            width: 100%
        }

        .table tr td,
        .table tr th {
            padding: 3px;
            font-size: 12px
        }

        .rgt {
            text-align: right;
        }

        .ctr {
            text-align: center;
        }

        table tr td {
            vertical-align: top
        }

        @media print {
            .pagebreak {
                clear: both;
                page-break-after: always;
            }
        }

    </style>
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Preview Form </h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Cetak Form Pendaftaran
                    <a href="{{ route('printform') }}" target="_blank">
                        <i class="material-icons list-icon">print</i>
                    </a>
                </a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('print-card-user') }}">Cetak Kartu Peserta</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('print_pengumuman-user') }}">Cetak Hasil Pengumuman</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="widget-list" id="print-preview2">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form['kop']['logo'] != null)
                                        <img src="{{ $form['kop']['logo'] }}" style="max-height:138px; min-width: 128px">
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    <b>{{ $form['kop']['header1'] }}</b>
                                </td>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form['kop']['logo2'] != null)
                                        <img src="{{ $form['kop']['logo2'] }}" style="max-height:138px; min-width: 128px">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <h2 style="margin: 0"><b>{{ $form['kop']['header2'] }}</b></h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                        <b>{{ $form['kop']['header3'] }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <b>{{ $form['kop']['alamat'] }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr style="border: solid 2px #000">
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt"><u>Form Pendaftaran</u>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                                <table style="width: 100%">
                                    <tbody>
                                        @foreach ($form['form'] as $key => $val)
                                        <tr>
                                            <td width="20%">{{ $val['nama'] }}</td>
                                            <td width="1%">:</td>
                                            <td width="39%" class="tbl">
                                                @isset($val['value'])
                                                    @switch($val['value'])
                                                        @case('l')
                                                            <small> laki -laki </small>
                                                        @break
                                                        @case('p')
                                                            <small> Perempuan </small>
                                                        @break
                                                        @default
                                                            <small>{{ $val['value'] }}</small>
                                                    @endswitch
                                                @else
                                                    <small class="text-muted">-</small>
                                                @endisset
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
