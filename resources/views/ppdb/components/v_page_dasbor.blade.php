@extends('ppdb.layouts.app')
@section('ppdb.components')

    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Dashboard</h5>
            <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard-ppdb') }} ">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('message'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message['message'] }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">

        <div class="row">
            <!-- Counter: Sales -->
            <div class="col-md-4 col-sm-6 widget-holder widget-full-height">
                <div class="widget-bg bg-primary text-inverse">
                    <div class="widget-body">
                        <div class="widget-counter">
                            <h6>Total Payment</h6>
                            <h3 class="h1">Rp.<span
                                    class="counter conter1">{{ number_format($total_pembayaran_all, 0) }}</span>
                            </h3><i class="material-icons list-icon">add_shopping_cart</i>
                            <hr>
                            <p>Total Payment Terkonfirm</p>
                            <p>Rp.<span class="counter conter0">{{ number_format($total_pembayaran_confirm, 0) }}</span>
                            </p><i class="material-icons list-icon">add_shopping_cart</i>
                        </div>
                        <!-- /.widget-counter -->
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <!-- Counter: Subscriptions -->
            <div class="col-md-4 col-sm-6 widget-holder widget-full-height">
                <div class="widget-bg bg-color-scheme text-inverse">
                    <div class="widget-body clearfix">
                        <div class="widget-counter">
                            <h6>Peserta Registrasi</h6>
                            <h3 class="h1"><span class="counter conter2">{{ $total_register }}</span></h3><i
                                class="material-icons list-icon">event_available</i>
                        </div>
                        <!-- /.widget-counter -->
                        <hr>
                        <div class="widget-counter">
                            <h6>Peserta Belum Diputusan </h6>
                            <h3 class="h1"><span
                                    class="counter conter conter5">{{ $total_belum_diputuskan }}</span></h3><i
                                class="material-icons list-icon">show_chart</i>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <!-- Counter: Users -->
            <div class="col-md-4 col-sm-6 widget-holder widget-full-height">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="widget-counter">
                            <h6>Peserta Diterima</h6>
                            <h3 class="h1"><span class="counter conter3">{{ $total_peserta_diterima }}</span>
                            </h3><i class="material-icons list-icon">public</i>
                        </div>
                        <!-- /.widget-counter -->
                        <hr>
                        <div class="widget-counter">
                            <h6>Peserta Ditolak </h6>
                            <h3 class="h1"><span class="counter counter4">{{ $total_peserta_ditolak }}</span>
                            </h3><i class="material-icons list-icon">show_chart</i>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
        </div>

        <div class="row">
            <!-- Contact Info -->
            <div class="hidden-xs hidden-sm col-md-3 widget-holder widget-full-height">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title">Pesan Informasi</h5>
                        <div class="widget-body">
                            <ul class="list-unstyled pesand dropdown-list-group ps ps--theme_default ps--active-y">
                            </ul>
                        </div>
                        <!-- /.contact-info -->
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
            <!-- Table: Order Status -->
            <div class="col-sm-6 col-md-9 widget-holder widget-full-height">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <h5 class="box-title">Konfirmasi Pembayaran </h5>
                        <div class="table-responsive">
                            <table class="table table-striped widget-status-table" id="table_pembayaran_confirm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th class="pd-l-20">Nama</th>
                                        <th>Status</th>
                                        <th class="hidden-xs">Metode</th>
                                        <th class="hidden-xs">Tanggal</th>
                                        <th class="hidden-xs">Nominal</th>
                                        <th class="hidden-xs">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- /.widget-status-table -->
                        </div>
                        <!-- /.padded-reverse -->
                    </div>
                    <!-- /.widget-body badge -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let table_payment;
            let config;
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('payment-status') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'status_pembayaran',
                            name: 'status_pembayaran',
                        },
                        {
                            data: 'bayar_via',
                            name: 'bayar_via'
                        },
                        {
                            data: 'tgl_bayar',
                            name: 'tgl_bayar',
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ],
                };

                table_payment = $('#table_pembayaran_confirm').dataTable(config);

                //ajax pesan

                window.notifyForThisMinute = function() {

                    table_payment.fnDraw(false);

                    const loader = $('ul.pesand');
                    const loader0 = $('.conter0');
                    const loader1 = $('.conter1');
                    const loader2 = $('.conter2');
                    const loader3 = $('.conter3');
                    const loader4 = $('.conter4');
                    const loader5 = $('.conter5');

                    var ajasx = $.ajax({
                        type: 'GET',
                        url: '{{ route('data-pesan') }}',
                        beforeSend: function() {
                            $(loader).html(
                                '<center><i class="fa fa-spin fa-spinner"></i> Loading </center>'
                            );
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    var balasan = '';


                                    if (rows.length == 0) {
                                        balasan = balasan +`<li class="media"><div class="media-body">tidak ada catatan </div></li>`;
                                    } else {
                                        rows.forEach(element => {
                                            balasan = balasan + '<li class="media">' +
                                                '<div class="d-flex mr-3">' +
                                                '<a href="#" class="block user--online thumb-xs">' +
                                                '<img src="{{ asset('asset/img/default-avatar.jpg') }} " class="rounded-circle" alt="">' +
                                                '</a>' +
                                                '</div>' +
                                                '<div class="media-body">' +
                                                '<h5 class="media-heading mb-0 d-flex"><a href="#" class="mr-auto">' +
                                                element['peserta'] +
                                                '</a> </h5><small class="mr-4">' +
                                                element[
                                                    'judul'] + '</small>' +
                                                '</div>' +
                                                '</li>';


                                        });
                                    }

                                    $('ul.pesand').html(balasan);
                                }
                            }
                        }
                    });

                    var ajas2 = $.ajax({
                        type: 'GET',
                        url: '{{ route('data-analystic') }}',
                        beforeSend: function() {},
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $(loader0).html(rows.total_pembayaran_all);
                                    $(loader1).html(rows.total_pembayaran_confirm);
                                    $(loader2).html(rows.total_register);
                                    $(loader3).html(rows.total_peserta_diterima);
                                    $(loader4).html(rows.total_peserta_ditolak);
                                    $(loader5).html(rows.total_belum_diputuskan);
                                }
                            }
                        }
                    });

                    setTimeout(notifyForThisMinute, (61 - new Date().getSeconds()) * 10000);
                }

                setTimeout(() => {
                    notifyForThisMinute();
                }, 5000);


                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
