@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="card">
    <div class="card-header">

        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h5 class="mr-0 mr-r-5"><i class="fa fa-bullhorn"></i></h5>
                <p class="mr-0 text-muted d-none d-md-inline-block">Detail Pengumuman</p>
            </div>
            <!-- /.page-title-left -->
            <div class="page-title-right d-inline-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><h5 class="mr-0 mr-r-5"><i class="fa fa-arrow-left"></i></h5><a href="{{ route('user-pengumuman') }}">Kembali</a>
                    </li>
                </ol>
            </div>
            <!-- /.page-title-right -->
        </div>
    </div>
    <div class="card-body">
            @if ($list['status'] == 'Aktif')
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-4">
                                @isset($list['file'])
                                    <img alt="100%x200" data-src="holder.js/100%x200" style="width: 100%; display: block"
                                        src="{{ $list['file'] }}" data-holder-rendered="true">
                                @else
                                    <img alt="100%x200" data-src="holder.js/100%x200" style="width: 100%; display: block"
                                        src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjQyIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDI0MiAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MjAwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTVjOGMzOWZlZTAgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMnB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNWM4YzM5ZmVlMCI+PHJlY3Qgd2lkdGg9IjI0MiIgaGVpZ2h0PSIyMDAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI4OS44MDQ2ODc1IiB5PSIxMDUuMSI+MjQyeDIwMDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                        data-holder-rendered="true">
                                @endisset
                            </div>
                            <div class="col-md-8">
                                <h4>{{ $list['judul'] ?? '' }}</h4>
                                <div class="row page-title clearfix">
                                    <div class="page-title-left">
                                        <span class="text-mute">{{ $list['waktu'] }}</span>
                                        -
                                        <span class="text-muted">
                                            <i class="fa fa-calendar"></i> {{ $list['diposting'] }}
                                        </span>
                                    </div>
                                    <!-- /.page-title-left -->
                                    <div class="page-title-right d-inline-flex">
                                        <span class="text-mute">
                                            <i class="fa fa-eye"></i>
                                            {{ intval($list['dilihat']) }}
                                        </span>
                                    </div>
                                    <!-- /.page-title-right -->
                                </div>
                                <div class="caption px-3 mt-3">
                                    {!! $list['isi'] ?? '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
    </div>
</div>
@endsection
