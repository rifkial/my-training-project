@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Tagihan Pembayaran</h5>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list" id="print-preview2">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                                <table id="table_history_pembayaran" class="table table-striped table-responsive table-hover">
                                    <thead class="table-info">
                                        <tr>
                                            <th>No</th>
                                            <th>Nisn</th>
                                            <th>Tahun Ajaran</th>
                                            <th>Tagihan</th>
                                            <th>Status</th>
                                            <th>Catatan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>{{ $tagihan['nisn'] }}</td>
                                            <td>{{ $tagihan['tahun_ajaran'] }} </td>
                                            <td>{{ number_format($tagihan['tagihan'], 0) }}</td>
                                            <td>
                                                @if ($tagihan['bukti'] == null)
                                                    <b>Belum Dibayar</b>
                                                @elseif($tagihan['bukti'] != null)
                                                    @if ($tagihan['status_pembayaran_kode'] == 0)
                                                     <b>Belum di bayar </b>
                                                     @elseif ($tagihan['status_pembayaran_kode'] == 1)
                                                    <b>Lunas </b>
                                                    @elseif ($tagihan['status_pembayaran_kode'] == 2)
                                                    <b>Belum di bayar</b>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{ $tagihan['keterangan'] ?? '-' }} </td>
                                            <td>
                                                <a href="{{ route('invoice_pembayaran') }}" id="print"><i
                                                    class="fa fa-download"></i></a>
                                                @if ($tagihan['status_pembayaran_kode'] == 0 || $tagihan['status_pembayaran_kode'] == 2)
                                                    | <a href="{{ route('konfirmasi-pembayaran') }}">  Konfirmasi</a>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

@endsection
