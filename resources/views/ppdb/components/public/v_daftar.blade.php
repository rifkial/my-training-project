@extends('ppdb.layouts.public')
@section('ppdb.components')
<div class="card">
    <div class="card-body">
        <form class="form-material" autocomplete="off" role="presentation" id="sigupform" method="POST"
            action="{{ route('daftar') }}">
            @csrf
            <h5 class="card-title text-body" id="daftar">PENDAFTARAN AKUN</h5>
            <div class="form-group">
                <input class="form-control" type="text" name="nama" required="" placeholder="Nama Lengkap"
                    pattern="[a-zA-Z][a-zA-Z0-9/\s]*" title="Format harus berisi alfanumerik atau huruf saja ">
                <label>Nama Lengkap</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="tel" name="nisn" required="" placeholder="NISN" pattern="[0-9]+"
                    title="Masukan hanya angka saja" autocomplete="false">
                <label>NISN</label>
            </div>
            <div class="form-group ">
                <select class="form-control" name="jenkel" id="l1">
                    <option value="">--Pilih jenis kelamin ---</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                </select>
                <label>Jenis Kelamin</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="tel" name="telepon" required="" placeholder="No.Telp/WA"
                    pattern="[0-9]+" title="Masukan hanya angka saja" autocomplete="false">
                <label>No.Telp/WA</label>
            </div>
            <div class="form-group">
                <input class="form-control" type="email" name="email" required="" placeholder="Email"
                    pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" title="format email salah "
                    autocomplete="false">
                <label>Email</label>
            </div>
            <div class="form-group ">
                <label class="mr-b-0">Password</label>
                <div class="input-group">
                    <input class="form-control password" type="password" name="password" required=""
                        placeholder="Password" autocomplete="false">
                    <div class="input-group-addon toggle-password">
                        <a href="javascript:void(0)"><i
                                class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                    </div>
                </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group text-center no-gutters mb-5">
                <button class="btn btn-info btn-lg btn-block text-uppercase ripple" type="submit"><i
                        class=" list-icon fa fa-sign-in"></i> Daftar</button>
            </div>
        </form>
    </div>
</div>
<script>
    (function($, global) {
        "use-strict"
        $(document).ready(function() {
            //auto hide alert
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                $(".alert-dismissible").alert('close');
            });

            const mata = document.querySelector(".eye")
            const inputPass = document.querySelector(".password");
            mata.addEventListener("click", () => {
                mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                if (inputPass.type === "password") {
                    inputPass.setAttribute("type", "text")

                } else if (inputPass.type === "text") {
                    inputPass.setAttribute("type", "password")
                    mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                }
            });

        });

    })(jQuery, window);
</script>
@endsection
