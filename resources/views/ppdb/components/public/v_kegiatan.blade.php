@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-body">RANGKAIAN KEGIATAN </h5>
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <!-- /.widget-heading -->
                            <div class="widget-body clearfix">
                                <table id="table_jadwal" class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tanggal Mulai </th>
                                            <th>Tanggal Berakhir </th>
                                            <th>Keterangan </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- Append Create Datatables-->
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tanggal Mulai </th>
                                            <th>Tanggal Berakhir </th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let config ='';
            $(document).ready(function() {
                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('jadwal-kegiatan') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'tgl_mulai',
                            name: 'tgl_mulai',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'tgl_akhir',
                            name: 'tgl_akhir',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan',
                        }
                    ],
                };
                table_jadwal = $('#table_jadwal').dataTable(config);
            });

        })(jQuery, window);
    </script>
@endsection
