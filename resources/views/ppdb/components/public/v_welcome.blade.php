@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            @if (empty($sambutan))
                <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada catatan Sambutan </p>
            @else
                <h5 class="card-title text-title">{{ $sambutan['judul'] ?? '' }}</h5>
                <hr>
                <div class="card-text text-body">
                    <div class="row">
                        @isset($sambutan['file'])
                            <div class="col-md-4">
                                <img src="{{ $sambutan['file'] }}" loading="lazy" class="text-center">
                            </div>
                            <div class="col-md-8 scrollbar-enabled ps ps--theme_default ps--active">
                                <div class="content-text" style="height: 37.8em;">
                                    {!! $sambutan['isi'] ?? '' !!}
                                </div>
                            </div>
                        @else
                            <div class="col-md-12 scrollbar-enabled ps ps--theme_default ps--active">
                                <div class="content-text" style="height: 37.8em;">
                                    {!! $sambutan['isi'] ?? '' !!}
                                </div>
                            </div>
                        @endisset
                    </div>
                </div>

            @endif
        </div>
    </div>
@endsection
