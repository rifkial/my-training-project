@extends('ppdb.layouts.public')
@section('ppdb.components')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-body">DOWNLOAD </h5>
            <table id="table_file" class="table table-striped table-responsive">
                <thead>
                    <legend> Dokumen File </legend>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Download </th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Append Create Datatables-->
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Download </th>
                    </tr>
                </tfoot>
            </table>
            <table id="table_brosur" class="table table-striped table-responsive">
                <thead>
                    <legend> Dokumen Brosur </legend>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Download </th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Append Create Datatables-->
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Download </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"
            let confi, config2, table_file, table_brosur;

            $(document).ready(function() {
                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('file-download') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'file',
                            name: 'file',
                        },
                    ],
                };
                table_file = $('#table_file').dataTable(config);
                config2 = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('brosur-download') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'file',
                            name: 'file',
                        },
                    ],
                };
                table_brosur = $('#table_brosur').dataTable(config2);
            });

        })(jQuery, window);
    </script>
@endsection
