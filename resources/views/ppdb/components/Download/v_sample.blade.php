@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Download > Sample Document PPDB</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Sample Document
                </li>
                <li class="breadcrumb-item "><a href="{{ route('brosur-ppdb') }}">Brosur </a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_sample" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Extension File</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Extension File</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <!-- detail popup -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowSample" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="result-body"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEditSample" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="SampleForm" name="SampleForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeading"></h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-danger float-right "> <i class="material-icons list-icon">warning</i> Tanda
                                    (*) Form harus disi!.</p>
                                <div class="result_form_error"></div>
                            </div>
                            <div class="col-sm-12">
                                <input type="hidden" id="id_sample" name="id_sample" class="form-control" />
                                <div class="form-group row">
                                    <p class="mr-b-0">Nama <span class="text-red">*</span></p>
                                    <div class="input-group">
                                        <input id="nama" type="text" name="nama" class="form-control"
                                            pattern="[a-zA-Z][a-zA-Z0-9/\s]*" placeholder="Masukan nama brosur"
                                            title="Format harus berisi alfanumerik atau huruf saja ">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <p class="col-md-8 col-form-label">Keterangan <span class="text-red">*</span> </p>
                                    <div class="input-group">
                                        <textarea name="isi" id="isi" rows="5" class="form-control"
                                            placeholder="Masukan deskripsi brosur">
                                            </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row mb-2">
                                            <label class="col-md-12 col-form-label" for="l1">Status</label>
                                            <div class="input-group">
                                                <select class="form-control" name="status" id="status">
                                                    <option value="1">Tampilkan</option>
                                                    <option value="0">Sembunyikan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p class="mr-b-0">Gambar</p>
                                            <div class="input-group">
                                                <input id="image" type="file" name="image" accept="image/*"
                                                    onchange="readURL(this);">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1 " width="150px" height="150px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update feed">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Tempat Sampah  -->
    <div class="modal modal-primary fade bs-modal-lg-primary" id="ajaxModelTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="ajaxModelTrashLabel"><i class="list-icon fa fa-trash"></i> Tempat
                        Sampah </h5>
                </div>
                <div class="modal-body">
                    <table id="table_sample_trash" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Append Create Datatables-->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script>
        (function($, global) {
            "use-strict"

            let table_sample;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewSample'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('data-sample') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'file',
                            name: 'file',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_sample = $('#table_sample').dataTable(config);

                //open modal form redirect
                $('#createNewSample').click(function() {
                    window.location.href = '{{ route('sample-add') }}';
                });

                //preview file

                $('body').on('click', '.show.sample', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('info-sample', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    let imagex = "";
                    let keterangan = "";

                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Sample");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    if(window.getExt(rows.file) =='png' || window.getExt(rows.file) =='jpg'
                                      || window.getExt(rows.file) =='jpeg'
                                      || window.getExt(rows.file) =='gif'
                                      || window.getExt(rows.file) =='svg'
                                      || window.getExt(rows.file) =='tif'
                                      || window.getExt(rows.file) =='bmp'
                                    ){
                                        if (rows.file !== "") {
                                            imagex = imagex + '<div class="col-sm-2">' +
                                                '<img src="' + rows.file + '" alt="">' +
                                                '</div>';
                                        }
                                    }else if(window.getExt(rows.file) =='pdf'){
                                        if (rows.file !== "") {
                                            imagex = imagex  + '<object width="100%" height="700px" data="'+rows.file+'"></object>';
                                        }
                                    }else if(window.getExt(rows.file) =='doc' || window.getExt(rows.file) =='docx'){
                                        if (rows.file !== "") {
                                            imagex = imagex  + '<object src="'+rows.file+'"><embed src="'+rows.file+'"></embed></object> auto download to open file ';
                                        }
                                    }


                                    if (rows.keterangan == 'null' || rows.keterangan ==
                                        null) {
                                        keterangan = keterangan + '-';
                                    } else {
                                        keterangan = keterangan + rows.keterangan;
                                    }


                                    bodytag = bodytag +
                                        '<div class="col-md-12 widget-holder">' +
                                        '<div class="widget-bg">' +
                                        '<div class="widget-body clearfix">' +
                                        '<h5 class="box-title">' + rows.nama + '</h5>' +
                                        '<section class="row">' +
                                        imagex +
                                        '<div class="col-sm-12 custom-scroll-content scrollbar-enabled ps ps--theme_default ps--active-y">' +
                                           keterangan +
                                        '</div>' +
                                        '</section>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                    $('.result-body').html(bodytag);

                                    $('#ajaxModelShowSample').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Sample");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowSample').modal('show');
                                }
                            }

                        }
                    });
                });

                //edit data detail
                $('body').on('click', '.edit.sample', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('sample-edit', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            //console.log(result['data']);
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    $('#ajaxModelEditSample').modal('show');
                                    $('#modelHeading.edit').html("Edit Sample");
                                    $('.result-body.edit').html('');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_sample"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    //tinymce.get('isi').setContent(rows.isi);
                                    $('textarea[name="isi"]').val(rows.keterangan);
                                    if (rows.file !== null) {

                                        if(window.getExt(rows.file) =='png' || window.getExt(rows.file) =='jpg'
                                        || window.getExt(rows.file) =='jpeg'
                                        || window.getExt(rows.file) =='gif'
                                        || window.getExt(rows.file) =='svg'
                                        || window.getExt(rows.file) =='tif'
                                        || window.getExt(rows.file) =='bmp'
                                        ){
                                            $('#modal-preview').show();
                                            $('#modal-preview').removeAttr('src');
                                            $('#modal-preview').attr('src', rows.file);
                                        }else{
                                            $('#modal-preview').hide();
                                        }

                                    } else {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src',
                                            'https://via.placeholder.com/150');
                                    }
                                    $("select[name='status'] > option[value=" + rows
                                        .status_kode + "]").prop("selected", true);
                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    $('#modelHeading.edit').html("Edit Pengumuman");
                                    $('.result_form_error').html(result['message']);
                                    $('#ajaxModelEditSample').modal('show');
                                }
                            }

                        }
                    });

                });

                 //update
                $('body').on('submit', '#SampleForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_sample"]').val();
                    var urlx = '{{ route('sample-update', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update.feed');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEditSample').modal('hide');
                                    table_sample.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });


                //delete
                $('body').on('click', 'button.remove.sample', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-sample', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                  //action button remove
                $('body').on('click', 'button.remove_force.sample', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove-permanent-sample', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini secara permanen , nantinya tidak dapat dipulihkan lagi!!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.ForceDelete(url, loader);
                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });

                //data trash
                $('body').on('click', 'button#data_trash', function() {
                    $('#ajaxModelTrash').modal('show');
                    config_trash = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('data-trash-sample') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    table_trash = $('#table_sample_trash').dataTable(config_trash);
                });

                  //action button restore
                $('body').on('click', 'button.restore.sample', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore-sample', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin pulihkan data ini!",
                        type: "question",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restoreData(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });


                 //function remove
                 window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_sample.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                 }

                   //function restore data
                window.restoreData = function restore(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_sample.fnDraw(false);
                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-undo"></i> restore');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-undo"></i> restore');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function forcedelete
                window.ForceDelete = function clear(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();
                    }
                };

                window.getExt = function getx(path) {
                    var basename = path.split(/[\\/]/).pop(), // extract file name from full path ...
                        // (supports `\\` and `/` separators)
                        pos = basename.lastIndexOf("."); // get last position of `.`

                    if (basename === "" || pos < 1) // if file name is empty or ...
                        return ""; //  `.` not found (-1) or comes first (0)

                    return basename.slice(pos + 1);
                }

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>

@endsection
