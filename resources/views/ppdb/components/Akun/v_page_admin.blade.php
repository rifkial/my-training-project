@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Administrator PPDB</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item ">Akun
                </li>
                <li class="breadcrumb-item active">Administrator
                </li>
                <li class="breadcrumb-item "><a href="{{ route('akun-pendaftar') }} ">Peserta</a></li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- popup detail -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowAdmin" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control" readonly="readonly" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="email" name="email" id="emailx" autocomplete="off"
                                                class="form-control" readonly="readonly"
                                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                title="Format email salah " placeholder="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="col-md-3 col-form-label" for="l1">Alamat
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group input-has-value">
                                            <textarea name="alamat" id="alamat" rows="5" cols="5"
                                                class="form-control text-left" placeholder="alamat" readonly="readonly">

                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Username
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="username" id="username" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja "
                                                placeholder="username" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                                </label>
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" style="margin-top: 10px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tempat Lahir </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja "
                                                placeholder="tempat lahir" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="jenkel" id="l1" readonly="readonly">
                                                <option value="">--Pilih jenis kelamin ---</option>
                                                <option value="l">Laki-Laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Agama
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="agama" id="agama" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z/\s]*"
                                                title="Format harus berisi huruf saja " placeholder="Agama"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Tanggal Lahir
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group input-has-value">
                                            <input type="text" id="tanggal_lahir" name="tanggal_lahir" readonly="readonly"
                                                class="form-control" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEditAdmin" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="PFormx" name="PFormx" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeadingx"> Edit Form Peserta Pendaftar
                        </h5>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row page-title clearfix mb-3 ">
                                    <div class="page-title-left">
                                    </div>
                                    <div class="page-title-right d-inline-flex">
                                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*)
                                            Form harus disi!.</p>
                                    </div>
                                </div>
                                <input type="hidden" name="id_admin" class="form-control ">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="namax" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Username <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="username" id="usernamex" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l3">Password</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input class="form-control password" id="password" name="password"
                                                placeholder="Password" type="password"> <span class="input-group-addon">
                                                <a href="javascript:void(0)"><i
                                                        class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                            </span>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-info mt-2 float-right "
                                            id="generate_pass">Generate</a>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Email
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="email" name="email" id="email" autocomplete="off"
                                                class="form-control"
                                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                title="Format email salah " placeholder="email">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="jenkel" id="l1">
                                                <option value="">--Pilih jenis kelamin ---</option>
                                                <option value="l">Laki-Laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="status">Status<span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="status" id="status" disabled="disabled">
                                                <option value="">--Pilih Status Admin ---</option>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="col-md-3 col-form-label pt-1" for="l1">Photo </label>
                                                <img id="modal-previewedit" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="auto" height="auto"
                                                    style="margin-top: 10px">
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input id="image" type="file" name="image" accept="image/*"
                                                                onchange="readURL(this);">
                                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update ">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_admin" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Telephone</th>
                                    <th>Status</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            let table_adminx;
            let config;

            $(document).ready(function() {

                config = {
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [{
                            text: '<i class="material-icons list-icon">add_circle</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                window.location.href = '{{ route('create-admin') }}';
                            }
                        },
                        {
                            text: '<i class="material-icons list-icon">refresh</i>',
                            className: "btn btn-outline-default ripple",
                            action: function(e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }

                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('source_ajax') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama_admin',
                            name: 'nama_admin'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'photo',
                            name: 'photo',
                        },
                        {
                            data: 'telepon',
                            name: 'telepon',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'alamat',
                            name: 'alamat',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_adminx = $('#table_admin').dataTable(config);

                $('body').on('click', '.show.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-admin', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Admin ");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    //console.log(result['data']);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="username"]').val(rows.username);
                                    $('input[name="email"]').val(rows.email);
                                    $('input[name="tempat_lahir"]').val(rows.tempat_lahir);
                                    $('input[name="tanggal_lahir"]').val(rows.tgl_lahir);
                                    $('input[name="agama"]').val(rows.agama);
                                    if (rows.file !== null) {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src', rows.file);
                                    } else {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src',
                                            'https://via.placeholder.com/150');
                                    }

                                    $('textarea[name="alamat"]').val(rows.alamat);

                                    $("select[name='jenkel'] > option[value=" + rows
                                        .jenkel_kode + "]").prop("selected", true);


                                    $('#ajaxModelShowAdmin').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Admin");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowAdmin').modal('show');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-admin', ':id') }}';
                    url = url.replace(':id', id);
                    let status;
                    var gambar = $(this).data('gambar');
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Admin ");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    //console.log(result['data']);
                                    $('input[name="id_admin"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="username"]').val(rows.username);
                                    $('input[name="email"]').val(rows.email);
                                    $('input[name="tempat_lahir"]').val(rows.tempat_lahir);
                                    $('input[name="tanggal_lahir"]').val(rows.tgl_lahir);
                                    $('input[name="agama"]').val(rows.agama);
                                    if (rows.file !== null || gambar !== null) {
                                        $('#modal-previewedit').removeAttr('src');
                                        $('#modal-previewedit').attr('src', gambar);
                                    } else {
                                        $('#modal-previewedit').removeAttr('src');
                                        $('#modal-previewedit').attr('src',
                                            'https://via.placeholder.com/150');
                                    }

                                    $('textarea[name="alamat"]').val(rows.alamat);

                                    $("select[name='jenkel'] > option[value=" + rows
                                        .jenkel_kode + "]").prop("selected", true);

                                    if(rows.status =='Aktif'){
                                        status = '1';
                                    }else if(rows.status == 'Tidak Aktif'){
                                        status = '0';
                                    }

                                    $("select[name='status'] > option[value=" +
                                        status + "]").prop("selected", true);

                                    $('#ajaxModelEditAdmin').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Admin");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelEditAdmin').modal('show');
                                }
                            }

                        }
                    });

                });

                 //update
                 $('body').on('submit', '#PFormx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_admin"]').val();
                    var urlx = '{{ route('update-admin', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEditAdmin').modal('hide');
                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                 });

                   //remove  data
                 $('body').on('click', 'button.remove.admin', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-admin', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                 });

                 //function remove
                 window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_adminx.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-previewedit';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-previewedit').removeClass('hidden');

                    }
                };

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });

        })(jQuery, window);
    </script>
@endsection
