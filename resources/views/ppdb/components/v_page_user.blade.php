@extends('ppdb.layouts.app')
@section('ppdb.components')

    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Dashboard</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user-ppdb') }} ">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('message'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message['message'] }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12 mr-b-20 mt-2">
            <div class="card text-inverse bg-success">
                <div class="card-body">
                    <h5 class="card-title">Selamat Datang, {{ session('username') }}</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="widget-list">
        <div class="row">
            @if ($check_tagihan['pembayaran'] == '1' && $check_access == false)
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <p class="text-info mt-3"> Silahkan lakukan pembayaran pada menu pembayaran atau <a href="{{ route('history-pembayaran') }}" class="btn btn-primary">Klik disini </a></p>
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-body">
                    @if (empty($sambutan))
                    <p class="text-muted text-center text-red"><i class="fa fa-warning"></i> Belum ada catatan Sambutan </p>
                    @else
                    <h5 class="card-title text-title">{{ $sambutan['judul'] ?? '' }}</h5>
                    <hr>
                    <div class="card-text text-body">
                        <div class="row">
                             @isset($sambutan['file'])
                             <div class="col-md-4">
                                <img src="{{ $sambutan['file'] }}" loading="lazy" class="text-center">
                             </div>
                             <div class="col-md-8 scrollbar-enabled ps ps--theme_default ps--active">
                                <div class="content-text" style="height: 37.8em;">
                                    {!! $sambutan['isi'] ?? '' !!}
                                </div>
                             </div>
                             @else
                             <div class="col-md-12 scrollbar-enabled ps ps--theme_default ps--active">
                                <div class="content-text" style="height: 37.8em;">
                                    {!! $sambutan['isi'] ?? '' !!}
                                </div>
                             </div>
                             @endisset
                        </div>
                    </div>

                    @endif
                </div>
            </div>
            @endif
        </div>
        <!-- /.row -->
    </div>


@endsection
