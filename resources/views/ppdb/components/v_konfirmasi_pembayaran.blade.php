@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Konfirmasi Pembayaran</h5>
        </div>
    </div>
    @if ($check_tagihan['status'] == 'Aktif' && $check_tagihan['pembayaran'] == '1' && $check_access == false)
        @if (count($errors) > 0)
            <div class="alert alert-danger border-info mt-1" role="alert">
                <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                        aria-hidden="true">×</span>
                </button>
                <div class="widget-list">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-body clearfix">
                            <div class="row">
                                <i class="material-icons list-icon md-48">warning</i>
                                <ul class="mr-t-10">
                                    @foreach ($errors->all() as $error)
                                        <li class="text-red">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                    </div>
                </div>
            </div>
        @else
            <div class="row page-title clearfix">
                <div class="page-title-left">
                </div>
                <div class="page-title-right d-inline-flex">
                    <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.
                    </p>
                </div>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-error border-error" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="widget-list">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-body clearfix">
                            <strong class="text-red">{{ $message }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success border-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="widget-list">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-body clearfix text-center">
                            <i class="material-icons list-icon">check_circle</i>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('send-konfirmasi') }}" class="form-horizontal"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="id_peserta" name="id_peserta" value="{{ $tagihan['id_peserta'] ?? '' }}"/>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nama
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="nama" id="nama" autocomplete="off" class="form-control"
                                            value="{{ old('nama') ?? ($tagihan['nama'] ?? '') }}" placeholder="nama"
                                            readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">NISN
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="nisn" id="nisn" autocomplete="off" class="form-control"
                                            value="{{ old('nisn') ?? ($tagihan['nisn'] ?? '') }}" placeholder="nama"
                                            readonly="readonly">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Tahun Ajaran
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="tahun_ajaran" id="tahun_ajaran" autocomplete="off" class="form-control"
                                            value="{{ old('tahun_ajaran') ?? ($tagihan['tahun_ajaran'] ?? '') }}" placeholder="Tahun Ajaran"
                                            readonly="readonly">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nama Pengirim
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="nama_pengirim" id="nama_pengirim" autocomplete="off"
                                            class="form-control" value="{{ old('nama_pengirim') ?? '' }}"
                                            placeholder="Nama Rekening Pengirim">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nama Bank Asal Pengirim
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="asal_bank" id="asal_bank" autocomplete="off"
                                            class="form-control" value="{{ old('asal_bank') ?? '' }}"
                                            placeholder="Nama Bank Asal Pengirim">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nomor Rekening Tujuan
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="no_rekening" id="no_rekening" autocomplete="off"
                                            class="form-control" value="{{ old('no_rekening') ?? '' }}"
                                            placeholder="Nomor Rekening Tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nama Bank Tujuan
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="tujuan_bank" id="tujuan_bank" autocomplete="off"
                                            class="form-control" value="{{ old('tujuan_bank') ?? '' }}"
                                            placeholder="tujuan_bank">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Tanggal Kirim
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-5">
                                    <div class="input-group input-has-value">
                                        <input type="text" id="tanggal_kirim" name="tanggal_kirim"
                                            value="{{ old('tanggal_kirim') ?? '' }}" readonly="readonly"
                                            class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                            data-plugin-options='{"autoclose": true}'>
                                        <span class="input-group-addon"><i
                                                class="list-icon material-icons">date_range</i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">Nominal Transfer (Rp.)
                                    <span class="text-red">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="number" name="nominal" id="nominal" autocomplete="off"
                                            class="form-control" value="{{ old('nominal') ?? $tagihan['tagihan'] ?? '' }}"
                                            placeholder="Nominal Transfer">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <label class="col-md-3 col-form-label" for="l1"></label>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-md-6 col-form-label pt-1" for="l1">Bukti Pembayaran
                                                <span class="text-red">*</span> </label>
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px"
                                                style="margin-top: 10px">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <input id="image" type="file" name="image" accept="image/*"
                                                            onchange="readURL(this);">
                                                        <input type="hidden" name="hidden_image" id="hidden_image">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-send"></i>
                                                Konfirmasi
                                            </button>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <script>
            (function($, global) {
                $(document).ready(function() {
                    window.readURL = function name(input, id) {
                        id = id || '#modal-preview';
                        if (input.files[0]) {
                            var reader = new FileReader();
                            var blob = input.files[0];

                            if (blob.type == 'application/pdf' || blob.type == 'application/doc') {
                                $('#modal-preview').hide();
                                $('#start').hide();
                            } else {
                                reader.onload = function(e) {
                                    $(id).attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                                $('#modal-preview').show();
                                $('#modal-preview').removeClass('hidden');
                                $('#start').hide();
                            }

                        }
                    };
                });
            })(jQuery, window);
        </script>
    @else
    <p class="alert-info"> Silahkan cek History Pembayaran !. </p>
    <a href="{{ route('history-pembayaran') }}" class="btn btn-warning btn-lg btn-rounded mr-b-20 ripple"> History Pembayaran</a>
    @endif
@endsection
