@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > Tipe Form</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"> Tipe Form
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Form Setting </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('custom-form') }}">Custom Form </a>
                    </li>
                </ol>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    <!-- detail info  -->
    <div class="modal modal-danger fade bs-modal-md" id="ajaxModelShowFormType" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="l30">Nama </label>
                        <span>:</span>
                        <span class="nama_label"></span>
                    </div>
                    <div class="form-group">
                        <label for="l30">Status </label>
                        <span>:</span>
                        <span class="status_label"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

     <!-- Edit info  -->
     <div class="modal modal-danger fade bs-modal-md" id="ajaxModelShowFormTypeEdit" tabindex="-1" role="dialog"
     aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
     <div class="modal-dialog modal-md">
         <div class="modal-content">
            <form name="formx" id="formx" class="form-control" enctype="multipart/form-data">
                @csrf
             <div class="modal-header text-inverse">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h5 class="modal-title" id="modelHeadingEdit"></h5>
             </div>
             <div class="modal-body">
                <input type="hidden" name="id_form" id="id_form" class="form-control ">
                <div class="form-group">
                    <label for="l30">Nama <span class="text-red">*</span></label>
                    <input class="form-control" id="nama" name="nama" placeholder="nama" type="text">
                </div>

             </div>
             <div class="modal-footer">
                <button type="submit" class="btn btn-info update form_custom type">
                    <i class="material-icons list-icon">save</i>
                    Update
                </button>
                 <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                     this</button>
             </div>
            </form>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_custom_form_type" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Initial</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Initial</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            let table_form;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewFormType'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-form-type') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'initial',
                            name: 'initial',
                        },
                        {
                            data: 'nama',
                            name: 'nama',
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_form = $('#table_custom_form_type').dataTable(config);

                $('body').on('click', '#createNewFormType', function() {
                    window.location.href = '{{ route('create-type-form') }}';
                });

                $('body').on('click','.show.form_custom.type',function(){
                    var id = $(this).data('id');
                    var url = '{{ route('detail-info-typeform', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Form type ");

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('span.nama_label').html(rows.nama);

                                    $('span.status_label').html(rows.status);

                                    $('#ajaxModelShowFormType').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Form type");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowFormType').modal('show');
                                }
                            }

                        }
                    });
                });

                $('body').on('click', '.edit.form_custom.type', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-type-form', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let status;
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingEdit').html("Info Edit Form Type ");

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_form"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);

                                    if (rows.status == 'Aktif') {
                                        status = '1';
                                    } else if (rows.status == 'Tidak Aktif') {
                                        status = '0';
                                    }

                                    $("select[name='status'] > option[value=" + status +
                                        "]").prop("selected", true);

                                    $('#ajaxModelShowFormTypeEdit').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Edit Form Type");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowFormTypeEdit').modal('show');
                                }
                            }

                        }
                    });
                });

                 //update
                 $('body').on('submit', '#formx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_form"]').val();
                    var urlx = '{{ route('update-form-type', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);
                    const loader = $('button.update.form_custom');
                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelShowFormTypeEdit').modal('hide');
                                    table_form.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });


                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });


                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
