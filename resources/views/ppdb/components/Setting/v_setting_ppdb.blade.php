@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > PPDB </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
                </li>
                <li class="breadcrumb-item active">Pengaturan PPDB
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-surat') }}">Template Kartu</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form-kartu') }}">Template Surat</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="SettingForm" name="SettingForm" method="POST" novalidate="novalidate" class="form-horizontal" enctype="multipart/form-data"
            action="{{ route('simpan-ppdb') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p class="mr-b-0">Header 1 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head1" type="text" name="head1" value="{{ old('head1') ?? $rows['head1'] ?? '' }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <p class="mr-b-0">Header 2 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head2" type="text" name="head2" value="{{ old('head2') ?? $rows['head2'] ?? '' }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <p class="mr-b-0">Header 3 <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="head3" type="text" name="head3" value="{{ old('head3') ?? $rows['head3'] ?? '' }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Alamat <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="alamat" id="alamat"
                                                class="form-control">{{ old('alamat') ?? $rows['alamat'] ?? '' }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Prolog <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="prolog" id="prolog"
                                                class="form-control">{{ old('prolog') ?? $rows['prolog'] ?? '' }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Penutup <span class="text-red">*</span> </p>
                                        <div class="input-group">
                                            <textarea name="penutup" id="penutup"
                                                class="form-control">{{ old('penutup') ?? $rows['penutup'] ?? '' }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Nama Kepsek <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nama_kepsek" type="text" name="nama_kepsek" value="{{ old('nama_kepsek') ?? $rows['nama_kepsek'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Gelar <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="gelar" type="text" name="gelar" value="{{ old('gelar') ?? $rows['gelar'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">NIP Kepsek <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nip_kepsek" type="text" name="nip_kepsek" value="{{ old('nip_kepsek') ?? $rows['nip_kepsek'] ?? '' }}"
                                                class="form-control"
                                                title="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="mr-b-0">Login PPDB Menggunakan ? <span class="text-red">*Kosongkan Jika Tidak Perlu</span></p>
                                        <div class="input-group">
                                            <input id="syarat_login" type="text" name="syarat_login" value="{{ old('syarat_login') ??  $rows['syarat_login'] ?? '' }}"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p class="mr-b-0">Tempat Keputusan <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="tempat_keputusan" type="text" name="tempat_keputusan" value="{{ old('tempat_keputusan') ?? $rows['tempat_keputusan'] ?? '' }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Tanggal Keputusan <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="tanggal_keputusan" type="text" name="tanggal_keputusan" value="{{ old('tgl_keputusan') ?? $rows['tgl_keputusan'] ?? '' }}"
                                                class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'
                                                title="Format harus berisi tanggal " readonly="readonly">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <p class="mr-b-0">Tahun Ajaran <span class="text-red">*</span> ex:2020/2021</p>
                                        <div class="input-group">
                                            <input id="tahun_ajaran" type="text" name="tahun_ajaran" value="{{ old('tahun_ajaran') ?? $rows['tahun_ajaran'] ?? '' }}"
                                                class="form-control" pattern="^\d+(\/\d+)*$"
                                                title="Format harus berisi angka ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Tanggal Tutup <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="tanggal_tutup" type="text" name="tanggal_tutup" value="{{ old('tgl_tutup') ?? $rows['tgl_tutup'] ?? '' }}"
                                                class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'
                                                title="Format harus berisi tanggal " readonly="readonly">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Jam Tutup <span class="text-red">*</span> Ex:06:05</p>
                                        <div class="input-group">
                                            <input id="jam_tutup" type="text" name="jam_tutup" value="{{ old('jam_tutup') ?? $rows['jam_tutup'] ?? '-' }}"
                                                class="form-control" pattern="([0-1]{1}[0-9]{1}|20|21|22|23):[0-5]{1}[0-9]{1}"
                                                title="Format harus berisi waktu ">
                                        </div>
                                    </div>

                                    <!--div class="form-group">
                                        <p class="mr-b-0">Nilai Minimal <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="nilai_minimal" type="text" name="nilai_minimal" value="{{ old('nilai_minimal') ?? $rows['nilai_minimal'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi number ">
                                        </div>
                                    </div-->

                                    <div class="form-group">
                                        <p class="mr-b-0">Jurusan <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="jurusan" type="text" name="jurusan" value="{{ old('jurusan') ??  $rows['jurusan'] ?? '' }}"
                                                class="form-control " pattern="[a-zA-Z][a-zA-Z0-9/\s]*" data-role="tagsinput"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Jalur PPDB <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="jalur_ppdb" type="text" name="jalur_ppdb" value="{{ old('jalur_ppdb') ?? $rows['jalur_ppdb'] ?? '' }}"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*" data-role="tagsinput"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Ubah Copyright <span class="text-red">*</span></p>
                                        <div class="input-group">
                                            <input id="copyright" type="text" name="copyright" value="{{ old('copyright') ?? $rows['copyright'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Kuota <span class="text-red">*Isi jika sekolah menggunakan sistem kuota. Kosongi jika tidak</span></p>
                                        <div class="input-group">
                                            <input id="kuota" type="text" name="kuota" value="{{ old('kouta') ?? $rows['kuota'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="mr-b-0" for="penomoran_otomatis">Gunakan Penomoran Otomatis</label>
                                        <div class="input-group">
                                            <select class="form-control" id="penomoran_otomatis" name="penomoran_otomatis">
                                                @switch($rows['penomoran_otomatis'] ?? '')
                                                    @case('1')
                                                    <option value="1" selected="selected">Ya </option>
                                                    @break
                                                    @case('0')
                                                    <option value="0" selected="selected">Tidak </option>
                                                    @break
                                                    @default
                                                    <option value="1">Ya </option>
                                                    <option value="0">Tidak </option>
                                                @endswitch
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Kontak WhatApss <span class="text-red"></span></p>
                                        <div class="input-group">
                                            <input id="whatsapp" type="whatsapp" name="whatsapp" value="{{ old('whatsapp') ?? $rows['whatsapp'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="mr-b-0">Kontak Telepon <span class="text-red"></span></p>
                                        <div class="input-group">
                                            <input id="telepon" type="number" name="telepon" value="{{ old('telepon') ?? $rows['telepon'] ?? '' }}"
                                                class="form-control"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['logo1'])
                                            <p class="mr-b-0">Logo 1</p>
                                        @else
                                            <p class="mr-b-0">Logo 1 <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="logo1" type="file" name="logo1" accept="image/*" onchange="readURL(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['logo1'])
                                            <img id="modal-preview" src="{{ $rows['logo1'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['logo2'])
                                            <p class="mr-b-0">Logo 2</p>
                                        @else
                                            <p class="mr-b-0">Logo 2 <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="logo2" type="file" name="logo2" accept="image/*" onchange="readURL2(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['logo2'])
                                            <img id="modal-preview2" src="{{ $rows['logo2'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview2" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['stempel'])
                                            <p class="mr-b-0">Stempel</p>
                                        @else
                                            <p class="mr-b-0">Stempel <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="stempel" type="file" name="stempel" accept="image/*" onchange="readURL3(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['stempel'])
                                            <img id="modal-preview3" src="{{ $rows['stempel'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview3" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        @isset($rows['stempel'])
                                            <p class="mr-b-0">TTD Kepsek</p>
                                        @else
                                            <p class="mr-b-0">TTD Kepsek <span class="text-red">*</span></p>
                                        @endisset
                                        <div class="input-group">
                                            <input id="ttd_kepsek" type="file" name="ttd_kepsek" accept="image/*" onchange="readURL4(this);">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        @isset($rows['ttd_kepsek'])
                                            <img id="modal-preview4" src="{{ $rows['ttd_kepsek'] }}" alt="Preview"
                                                class="form-group mb-1 mr-50 " width="150px" height="150px">
                                            <p class="text-muted ">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                        @else
                                            <img id="modal-preview4" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px">
                                        @endisset
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL2 = function name(input, id) {
                    id = id || '#modal-preview2';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview2').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL3 = function name(input, id) {
                    id = id || '#modal-preview3';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview3').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                window.readURL4 = function name(input, id) {
                    id = id || '#modal-preview4';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview4').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

            });

        })(jQuery, window);
    </script>
@endsection
