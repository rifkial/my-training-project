@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left d-inline-flex">
        <h5 class="mr-0 mr-r-5">Setting Kartu </h5>
    </div>
    <!-- /.page-title-left -->
    <div class="page-title-right d-inline-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> Setting Kartu
            </li>
            <li class="breadcrumb-item"><a href="{{ route('custom-form') }}">Custom Form </a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('type-form') }}">Tipe Form  </a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('setting-ppdb') }}">Pengaturan PPDB</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('setting-sambutan') }}">Sambutan</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('template-surat') }}">Template Kartu</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('setting-form-kartu') }}">Template Surat</a>
            </li>
        </ol>
    </div>
    <!-- /.page-title-right -->
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger border-info mt-1" role="alert">
        <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                aria-hidden="true">×</span>
        </button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix">
                    <div class="row">
                        <i class="material-icons list-icon md-48">warning</i>
                        <ul class="mr-t-10">
                            @foreach ($errors->all() as $error)
                                <li class="text-red">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
        </div>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-error border-error" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix">
                    <strong class="text-red">{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success border-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix text-center">
                    <i class="material-icons list-icon">check_circle</i>
                    <strong>{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <!-- /.widget-heading -->
                <div class="widget-body clearfix">
                    <form id="FormRegis" name="FormRegis" method="POST" class="form-horizontal"
                        enctype="multipart/form-data" action="{{ route('update_statusform') }}">
                        @csrf
                        <div class="modal-body">
                            @foreach ($rows as $form => $val)
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="form-control" name="form[{{ $val['id'] }}]" id="{{ $val['id'] }}">
                                            @switch($val['status_kartu'])
                                                @case('Aktif')
                                                <option value="1" selected="selected">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                                @break
                                                @case('Tidak Aktif')
                                                <option value="1">Aktif</option>
                                                <option value="0" selected="selected">Tidak Aktif</option>
                                                @break
                                            @endswitch
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-actions ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 btn-list ml-10">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons list-icon">save</i>
                                            Simpan
                                        </button>
                                        <a class="btn btn-info" href="{{ route('setting-form') }}">
                                            <i class="material-icons list-icon">keyboard_arrow_left</i>
                                            Kembali
                                        </a>
                                    </div>
                                    <!-- /.col-sm-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </form>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.widget-bg -->
        </div>
        <!-- /.widget-holder -->
    </div>
    <!-- /.row -->
</div>
@endsection
