@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > Formulir </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"> Form Setting
                </li>

                <li class="breadcrumb-item"><a href="{{ route('setting-ppdb') }}">Pengaturan PPDB</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-sambutan') }}">Sambutan</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-kartu') }}">Template Kartu</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form-kartu') }}">Template Surat</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <a href="{{ route('setting-form-aktif') }}"> <i class="list-icon fa fa-sort-numeric-asc"></i> Urutkan Form </a> |
                        <a href="{{ route('custom-form') }}">Custom Form </a> |
                        <a href="{{ route('type-form') }}">Tipe Form </a>

                        <form id="FormRegis" name="FormRegis" method="POST" class="form-horizontal"
                            enctype="multipart/form-data" action="{{ route('save-setting-form') }}">
                            @csrf
                            <div class="modal-body">
                                <div class="tabs tabs-vertical">
                                    <ul class="nav nav-tabs flex-column">
                                        @foreach ($rows as $form => $key)
                                            @if ($form == '0')
                                                <li class="nav-item" aria-expanded="false"><a class="nav-link active"
                                                        href="#{{ Str::slug($key['nama_jenis']) ?? '' }}"
                                                        data-toggle="tab"
                                                        aria-expanded="true">{{ $key['initial_jenis'] ?? '' }}</a>
                                                </li>
                                            @else
                                                <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                                        href="#{{ Str::slug($key['nama_jenis']) ?? '' }}"
                                                        data-toggle="tab"
                                                        aria-expanded="true">{{ $key['nama_jenis'] ?? '' }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    <!-- /.nav-tabs -->
                                    <div class="tab-content">
                                        @foreach ($rows as $form => $key)
                                            @if ($form == '0')
                                                <div class="tab-pane active"
                                                    id="{{ Str::slug($key['nama_jenis']) ?? '' }}" aria-expanded="true">
                                                    <div class="col-sm-12 mb-3">
                                                        <div class="card card-outline-info">
                                                            <div class="card-header">
                                                                <h3 class="box-title mr-b-0">
                                                                    {{ $key['initial_jenis'] ?? '' }}</h3>

                                                            </div>
                                                            <div class="card-body" id="check{{ $form }}">
                                                                <div class="checkbox">
                                                                    <label class="checkbox-checked">
                                                                        <input type="checkbox" name="select-all"
                                                                            id="select-all" class="select-all"> <span
                                                                            class="label-text">Select/Desellect
                                                                            All</span>
                                                                    </label>
                                                                </div>
                                                                @foreach ($key['forms'] as $child => $list)
                                                                    <div class="checkbox ml-4">
                                                                        <label>
                                                                            <input type="hidden" id="option_check"
                                                                                class="option_check{{ $form }}"
                                                                                name="form[{{ $list['id'] }}]" value="0">
                                                                            <input type="checkbox" id="option_check"
                                                                                class="option_check{{ $form }}"
                                                                                name="form[{{ $list['id'] }}]" value="1"
                                                                                @if ($list['status_form'] == 'Aktif') checked="checked" @endif>
                                                                            <span
                                                                                class="label-text">{{ $list['nama'] ?? '' }}</span>
                                                                        </label>

                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="tab-pane"
                                                    id="{{ Str::slug($key['nama_jenis']) ?? '' }}" aria-expanded="false">
                                                    <div class="col-sm-12 mb-3">
                                                        <div class="card card-outline-info">
                                                            <div class="card-header">
                                                                <h3 class="box-title mr-b-0">
                                                                    {{ $key['initial_jenis'] ?? '' }}</h3>
                                                            </div>
                                                            <div class="card-body" id="check{{ $form }}">
                                                                <label class="checkbox-checked">
                                                                    <input type="checkbox" name="select-all" id="select-all"
                                                                        class="select-all"> <span
                                                                        class="label-text">Select/Desellect
                                                                        All</span>
                                                                </label>
                                                                @foreach ($key['forms'] as $child => $list)
                                                                    <div class="checkbox ml-4">
                                                                        <label>
                                                                            <input type="hidden" id="option_check"
                                                                                class="option_check{{ $form }}"
                                                                                name="form[{{ $list['id'] }}]" value="0">
                                                                            <input type="checkbox" id="option_check"
                                                                                class="option_check{{ $form }}"
                                                                                name="form[{{ $list['id'] }}]" value="1"
                                                                                @if ($list['status_form'] == 'Aktif') checked="checked" @endif>
                                                                            <span
                                                                                class="label-text">{{ $list['nama'] ?? '' }}</span>
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                            </div>
                            <div class="form-actions modal-footer">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //check option
                $('body').on('click', '.option_check', function() {
                    if ($(this).is(":checked")) {
                        $(this).prop('checked', true);
                        $(this).val('1');
                        $(this).attr('checked', 'checked');
                    } else {
                        $(this).prop('checked', false);
                        $(this).val('0');
                        $(this).attr('checked', 'checked');
                    }
                });

                @foreach ($rows as $form => $key)
                    $('body').on('click','#check{{ $form }} .select-all',function(){
                        if ($('#check{{ $form }} .select-all:checked').length == 0) {
                            $('#check{{ $form }} input[type="checkbox"].option_check{{ $form }}').prop('checked', false);
                        }else{
                            $('#check{{ $form }} input[type="checkbox"].option_check{{ $form }}').prop('checked', true);
                        }
                    });
                @endforeach



                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

            });

        })(jQuery, window);
    </script>
@endsection
