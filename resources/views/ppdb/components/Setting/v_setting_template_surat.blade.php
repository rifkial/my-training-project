@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > Template Surat </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('setting-ppdb') }}">Pengaturan PPDB</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
                </li>
                <li class="breadcrumb-item active">Template Surat
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-kartu') }}">Template Kartu</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-sambutan') }}">Sambutan</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body clearfix">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                Pilih Select / Unselect Option di bawah ini sesuai kebutuhan
                            </div>
                            <div class="card-body">
                                <form id="FormSet" name="FormSet" method="POST" class="form-horizontal"
                                    enctype="multipart/form-data" action="{{ route('update-template-surat') }}">
                                    @csrf
                                    <div class="checkbox ml-2">
                                        <label class="checkbox-checked">
                                            <input type="checkbox" name="select-all"
                                                id="select-all" class="select-all"> <span
                                                class="label-text">Select/Desellect
                                                All</span>
                                        </label>
                                    </div>
                                    <div class="row">
                                        @foreach ($form_surat as $form => $list)
                                            <div class="form-group">
                                                <div class="checkbox ml-4">
                                                    <label>
                                                        <input type="hidden" id="option_check"
                                                            class="option_check{{ $form }}"
                                                            name="form[{{ $list['id'] }}]" value="0">
                                                        <input type="checkbox" id="option_check"
                                                            class="option_check{{ $form }}"
                                                            name="form[{{ $list['id'] }}]" value="1"
                                                            @if ($list['aktif'] == 'Aktif') checked="checked" @endif>
                                                        <span class="label-text">{{ $list['nama'] ?? '' }}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-actions">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 btn-list">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="material-icons list-icon">save</i>
                                                        Simpan
                                                    </button>
                                                    <a class="btn btn-info" href="{{ route('preview-form') }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </div>
                                                <!-- /.col-sm-12 -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //check option
                $('body').on('click', '.option_check', function() {
                    if ($(this).is(":checked")) {
                        $(this).prop('checked', true);
                        $(this).val('1');
                        $(this).attr('checked', 'checked');
                    } else {
                        $(this).prop('checked', false);
                        $(this).val('0');
                        $(this).attr('checked', 'checked');
                    }
                });

                @foreach ($form_surat as $form => $key)
                    $('body').on('click',' .select-all',function(){
                        if ($('.select-all:checked').length == 0) {
                            $('input[type="checkbox"].option_check{{ $form }}').prop('checked', false);
                        }else{
                            $('input[type="checkbox"].option_check{{ $form }}').prop('checked', true);
                        }
                    });
                @endforeach

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

            });

        })(jQuery, window);
    </script>
@endsection
