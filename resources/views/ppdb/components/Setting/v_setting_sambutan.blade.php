@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan  >Sambutan</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('setting-ppdb') }}">Pengaturan PPDB</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-pembayaran') }}">Pembayaran</a>
                </li>
                <li class="breadcrumb-item active">Sambutan
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-surat') }}">Template Kartu</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form-kartu') }}">Template Surat</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-error border-error" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix">
                    <strong class="text-red">{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="SambutanForm" name="SambutanForm" method="POST" class="form-horizontal" enctype="multipart/form-data"
            action="{{ route('simpan-pengaturan-sambutan') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="form-group row">
                                <p class="mr-b-0">Judul <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="judul" type="text" name="judul" pattern="[a-zA-Z][a-zA-Z0-9/\s]*" title="Format harus berisi alfanumerik atau huruf saja " class="form-control" value="{{ old('judul') ??  $rows['judul'] ?? ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Isi <span class="text-red">*</span> </p>
                                <div class="input-group">
                                    <textarea name="isi" id="isi" class="form-control editor">
                                        {{ old('isi') ?? $rows['isi'] ?? '' }}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                @isset($rows['file'])
                                <p class="mr-b-0">Gambar </p>
                                @else
                                <p class="mr-b-0">Gambar <span class="text-red">*</span></p>
                                @endisset
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                @isset($rows['file'])
                                <img id="modal-preview" src="{{ $rows['file'] }}" alt="Preview"
                                    class="form-group mb-1" width="150px" height="150px">
                                    <p class="text-muted ml-50">Kosongkon form ini jika tidak ada perubahan gambar</p>
                                 @else
                                 <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group mb-1" width="150px" height="150px">
                                @endisset
                            </div>
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert-dismissible").alert('close');
                });

            });

        })(jQuery, window);
    </script>
@endsection
