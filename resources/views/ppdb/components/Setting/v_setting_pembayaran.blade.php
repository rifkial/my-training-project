@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > Pembayaran </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Formulir Pendaftaran</a>
                </li>
                <li class="breadcrumb-item active">Pembayaran
                </li>
                <li class="breadcrumb-item"><a href="{{ route('template-surat') }}">Template Kartu</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-form-kartu') }}">Template Surat</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('setting-sambutan') }}">Sambutan</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong class="text-red">{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row page-title clearfix">
        <div class="page-title-left">
        </div>
        <div class="page-title-right d-inline-flex">
            <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
        </div>
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <form id="FormNew" name="FormNew" method="POST" action="" class="form-horizontal"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="nama">Nama <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="nama" name="nama" placeholder="Nama Pembayaran "
                                            type="text" value="{{ old('nama') ?? $rows['nama'] ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="biaya_pendaftaran">Biaya Pendaftaran <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <input class="form-control" id="biaya_pendaftaran" name="biaya_pendaftaran"
                                            placeholder="Biaya Pembayaran " type="number" value="{{ old('biaya_pendaftaran') ?? $rows['biaya_pendaftaran'] ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l13">Verifikasi Pembayaran <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="pembayaran" name="pembayaran">
                                            <option value="">Pilih opsi Verifikasi</option>
                                            @switch($rows['pembayaran'])
                                                @case('1')
                                                <option value="1" selected="selected">Ya</option>
                                                <option value="0">Tidak</option>
                                                @break
                                                @case('0')
                                                <option value="0" selected="selected">Tidak</option>
                                                <option value="1">Ya</option>
                                                @break
                                                @default
                                                <option value="0">Tidak</option>
                                                <option value="1">Ya</option>
                                            @endswitch
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan">Keterangan <span class="text-red">*</span></label>
                                    <textarea class="form-control editor" id="keterangan" name="keterangan"
                                        rows="2">
                                        {{ old('keterangan') ?? $rows['keterangan'] ?? '' }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info update">
                                    <i class="material-icons list-icon">save</i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            let table_doc;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('body').on('submit', '#FormNew', function(e) {
                    e.preventDefault();
                    const loader = $('button.update');
                    var formData = new FormData(this);
                    $.ajax({
                        type: "POST",
                        url: '{{ route('simpan-pengaturan-pembayaran') }}',
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            if (typeof data !== 'underfined') {
                                var notif = '';
                                var message = [];
                                if (typeof data.responseJSON['errors'] !== 'underfined') {

                                    message = data.responseJSON['errors'];

                                    for (let item in message) {
                                        notif = notif + message[item] + ' <br>';
                                    }
                                    window.notif('error', notif);
                                }
                            }

                            $(loader).html(
                                '<i class="material-icons list-icon"></i> Simpan');
                        }
                    });

                });


                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
