@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left d-inline-flex">
        <h5 class="mr-0 mr-r-5">Pengaturan > Preview Formulir Pendaftaran </h5>
    </div>
    <!-- /.page-title-left -->
    <div class="page-title-right d-inline-flex">
        <ol class="breadcrumb">

        </ol>
    </div>
    <!-- /.page-title-right -->
</div>
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="tabs tabs-vertical">
                <ul class="nav nav-tabs flex-column">
                    @foreach ($kategori_form as $form => $key)

                        @if ($form == '0')
                            <li class="nav-item" aria-expanded="false"><a class="nav-link active"
                                    href="#{{ Str::slug($key['nama_jenis']) ?? '' }}"
                                    data-toggle="tab"
                                    aria-expanded="true">{{ $key['initial_jenis'] ?? '' }}</a>
                            </li>
                        @else
                            <li class="nav-item" aria-expanded="false"><a class="nav-link"
                                    href="#{{ Str::slug($key['nama_jenis']) ?? '' }}"
                                    data-toggle="tab"
                                    aria-expanded="true">{{ $key['nama_jenis'] ?? '' }}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach ($kategori_form as $form => $key)

                        @if ($form == '0')
                            <div class="tab-pane active"
                                id="{{ Str::slug($key['nama_jenis']) ?? '' }}" aria-expanded="true">
                                <div class="col-sm-12 mb-3">
                                    <div class="card card-outline-info">
                                        <div class="card-header">
                                            <h3 class="box-title mr-b-0">{{ $key['jenis'] ?? '' }}</h3>
                                        </div>
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="tab-pane"
                                id="{{ Str::slug($key['nama_jenis']) ?? '' }}" aria-expanded="false">
                                <div class="col-sm-12 mb-3">
                                    <div class="card card-outline-info">
                                        <div class="card-header">
                                            <h3 class="box-title mr-b-0">{{ $key['jenis'] ?? '' }}</h3>
                                        </div>
                                        <div class="card-body">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</div>
@endsection
