@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Pengaturan > Custom Form </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"> Custom Form
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('setting-form') }}">Setting Form </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('type-form') }}">Type Form </a>
                    </li>
                </ol>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- detail info  -->
    <div class="modal modal-danger fade bs-modal-md" id="ajaxModelShowForm" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="l30">Nama </label>
                        <span>:</span>
                        <span class="nama_label"></span>
                    </div>
                    <div class="form-group">
                        <label for="l30">Tipe </label>
                        <span>:</span>
                        <span class="tipe_label"></span>
                    </div>
                    <div class="form-group">
                        <label for="l30">Kode Jenis </label>
                        <span>:</span>
                        <span class="jenis_label"></span>
                    </div>
                    <div class="form-group">
                        <label for="l30">Status </label>
                        <span>:</span>
                        <span class="status_label"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-danger fade bs-modal-md" id="ajaxModelShowFormEdit" tabindex="-1" role="dialog"
        aria-labelledby="myMediumModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form name="formx" id="formx" class="form-control" enctype="multipart/form-data">
                    @csrf
                    <div class=" modal-header text-inverse">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="modelHeadingEdit"></h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id_form" id="id_form" class="form-control ">
                        <div class="form-group">
                            <label class="form-control-label">Jenis Form <span class="text-red">*</span></label>
                            <select class="m-b-10 form-control" name="jenis_form" data-placeholder="Choose"
                                data-toggle="select2">
                                <option value="">Pilih jenis form</option>
                                @foreach ($typeform as $form => $v)
                                    <option value="{{ $v['id'] }}">{{ $v['initial'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="l30">Nama <span class="text-red">*</span></label>
                            <input class="form-control" id="nama" name="nama" placeholder="nama" type="text"
                                value="{{ old('nama') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="l30">Tipe <span class="text-red">*</span></label>
                            <select class="form-control" id="tipe" name="tipe">
                                <option value="">Pilih Tipe Form</option>
                                <option value="text">Text</option>
                                <option value="textarea">Text Area</option>
                                <option value="option">Option</option>
                                <option value="date">Date</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="l30">Status <span class="text-red">*</span></label>
                            <select class="form-control" id="status" name="status" disabled="disabled">
                                <option value="">Status</option>
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update form_custom">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_custom_form" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Form</th>
                                    <th>Initial</th>
                                    <th>Nama</th>
                                    <th>tipe</th>
                                    <th>Status Form</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Form</th>
                                    <th>Initial</th>
                                    <th>Name</th>
                                    <th>tipe</th>
                                    <th>Status Form</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            let table_form;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewForm'
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('ajax-form-custom') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },

                        {
                            data: 'jenis',
                            name: 'jenis'
                        },
                        {
                            data: 'initial',
                            name: 'initial',
                        },
                        {
                            data: 'nama',
                            name: 'nama',
                        },
                        {
                            data: 'tipe',
                            name: 'tipe',
                        },
                        {
                            data: 'status_form',
                            name: 'status_form',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_form = $('#table_custom_form').dataTable(config);

                $('body').on('click', '#createNewForm', function() {
                    window.location.href = '{{ route('custom-new-form') }}';
                });

                $('body').on('click', '.show.form_custom', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-form-custom', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Form ");

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('span.nama_label').html(rows.nama);
                                    $('span.tipe_label').html(rows.tipe);
                                    $('span.jenis_label').html(rows.id_jenis);
                                    $('span.status_label').html(rows.status);

                                    $('#ajaxModelShowForm').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Form");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowForm').modal('show');
                                }
                            }

                        }
                    });
                });

                $('body').on('click', '.edit.form_custom', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-form-custom', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let status;
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingEdit').html("Info Edit Form ");

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_form"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);

                                    $("select[name='jenis_form']").select2().val(rows.id_jenis).trigger("change");

                                    $("select[name='tipe'] > option[value=" + rows
                                        .tipe + "]").prop("selected", true);

                                    if (rows.status == 'Aktif') {
                                        status = '1';
                                    } else if (rows.status == 'Tidak Aktif') {
                                        status = '0';
                                    }

                                    $("select[name='status'] > option[value=" + status +
                                        "]").prop("selected", true);

                                    $('#ajaxModelShowFormEdit').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Edit Form");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowFormEdit').modal('show');
                                }
                            }

                        }
                    });
                });

                 //update
                $('body').on('submit', '#formx', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_form"]').val();
                    var urlx = '{{ route('update-custom-form', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);
                    const loader = $('button.update.form_custom');
                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelShowFormEdit').modal('hide');
                                    table_form.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });


                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });


        })(jQuery, window);
    </script>
@endsection
