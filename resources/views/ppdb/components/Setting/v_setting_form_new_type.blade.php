@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left">
        <h5 class="mr-0 mr-r-5">Type Form > New Type Form </h5>
    </div>
</div>
@if (count($errors) > 0)
<div class="alert alert-danger border-info mt-1" role="alert">
    <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
            aria-hidden="true">×</span>
    </button>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="widget-body clearfix">
                <div class="row">
                    <i class="material-icons list-icon md-48">warning</i>
                    <ul class="mr-t-10">
                        @foreach ($errors->all() as $error)
                            <li class="text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- /.widget-body -->
        </div>
    </div>
</div>
@else
<div class="row page-title clearfix">
    <div class="page-title-left">
    </div>
    <div class="page-title-right d-inline-flex">
        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
    </div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-error border-error" role="alert">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="widget-body clearfix">
                <strong>{{ $message }}</strong>
            </div>
        </div>
    </div>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success border-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <div class="widget-list">
        <div class="col-md-12 widget-holder">
            <div class="widget-body clearfix text-center">
                <i class="material-icons list-icon">check_circle</i>
                <strong>{{ $message }}</strong>
            </div>
        </div>
    </div>
</div>
@endif
<div class="widget-list">
    <div class="row">
        <div class="col-md-6 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <form id="DocFormx" name="DocFormx" method="POST" action="{{ route('save-type-form') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                    <div class="form-group">
                        <label for="l30">Nama <span class="text-red">*</span></label>
                        <input class="form-control" id="nama" name="nama" placeholder="nama" type="text" value="{{ old('nama') ?? '' }}">
                    </div>
                    <div class="form-actions">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12 btn-list">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="material-icons list-icon">save</i>
                                        Simpan
                                    </button>
                                    <a class="btn btn-info" href="{{ route('type-form') }}">
                                        <i class="material-icons list-icon">keyboard_arrow_left</i>
                                        Kembali
                                    </a>
                                </div>
                                <!-- /.col-sm-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.form-group -->
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
