@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left">
        <h5 class="mr-0 mr-r-5">Pesan > Buat Pesan Baru </h5>
    </div>
</div>
@if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
@if ($message = Session::get('error'))
    <div class="alert alert-error border-error" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix">
                    <strong>{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success border-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix text-center">
                    <i class="material-icons list-icon">check_circle</i>
                    <strong>{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="widget-list">
    <div class="row">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body">
                    <form id="DocForm" name="DocForm" method="POST" action="{{ route('send-message-admin') }}"
                        class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <span id="form_result"></span>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l1">Judul Pesan
                                            <span
                                                class="text-red">*</span></label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input type="text" name="nama" id="nama" autocomplete="off"
                                                    class="form-control" value="{{ old('nama') }}"
                                                    pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                    title="Format harus berisi alfanumerik atau huruf saja "
                                                    placeholder="judul pesan">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-3 col-form-label" for="l1">Pesan
                                            <span
                                                class="text-red">*</span>
                                         </label>
                                        <div class="col-md-9">
                                            <div class="input-group input-has-value">
                                                <textarea name="pesan" id="pesan" rows="5" cols="5" class="form-control editor text-left"
                                                    placeholder="isi pesan">
                                                    {{ old('pesan') }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row mb-0">
                                        <label class="col-md-3 col-form-label" for="l1"></label>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="col-md-3 col-form-label pt-1" for="l1">Photo
                                                     </label>
                                                    <img id="modal-preview" src="https://via.placeholder.com/150"
                                                        alt="Preview" class="form-group mb-1" width="150px" height="150px"
                                                        style="margin-top: 10px">
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group row">
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input id="image" type="file" name="image" accept="image/*"
                                                                    onchange="readURL(this);">
                                                                <input type="hidden" name="hidden_image" id="hidden_image">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 btn-list">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons list-icon">save</i>
                                            Kirim
                                        </button>
                                        <a class="btn btn-info" href="{{ route('pesan-user') }}">
                                            <i class="material-icons list-icon">keyboard_arrow_left</i>
                                            Kembali
                                        </a>
                                    </div>
                                    <!-- /.col-sm-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function($, global) {
        "use-strict"
        let config;
        $(document).ready(function() {
            //read file image upload
            window.readURL = function name(input, id) {
                id = id || '#modal-preview';
                if (input.files[0]) {
                    var reader = new FileReader();
                    var blob = input.files[0];

                    if (blob.type == 'application/pdf' || blob.type == 'application/doc') {
                        $('#modal-preview').hide();
                        $('#start').hide();
                    } else {
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').show();
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();
                    }

                }
            };

            //auto hide alert
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                $(".alert-dismissible").alert('close');
            });

        });

    })(jQuery, window);
</script>
@endsection
