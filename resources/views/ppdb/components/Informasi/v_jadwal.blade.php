@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex ">
            <h5 class="mr-0 mr-r-5">Informasi > Jadwal Pendaftaran</h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('informasi-banner') }}">Banner</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('informasi-alur') }}">Alur Pendaftaran</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('informasi-syarat') }}">Syarat Pendaftaran</a>
                </li>
                <li class="breadcrumb-item active">Jadwal Pendaftaran
                </li>
                <li class="breadcrumb-item"><a href="{{ route('informasi-panduan') }}">Panduan Pendaftaran</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShow" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="result-body"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEdit" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="JadwalForm" name="JadwalForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeading"></h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-danger float-right "> <i class="material-icons list-icon">warning</i> Tanda
                                    (*) Form harus disi!.</p>
                                <div class="result_form_error"></div>
                            </div>
                            <div class="col-sm-6">
                                <input type="hidden" id="id_jadwal" name="id_jadwal" class="form-control" />
                                <div class="form-group row">
                                    <label class="col-md-8 col-form-label" for="l1">Nama <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control" placeholder="Masukan sebuah nama "
                                                pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-8 col-form-label" for="l1">Tempat <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" name="tempat" id="tempat" autocomplete="off"
                                                class="form-control" placeholder="Masukan sebuah tempat "
                                                pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-8 col-form-label" for="l1">Tanggal Mulai <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-12">
                                        <div class="input-group input-has-value">
                                            <input type="text" id="tanggal_mulai" name="tanggal_mulai" readonly="readonly"
                                                class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-8 col-form-label" for="l1">Tanggal Akhir <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-12">
                                        <div class="input-group input-has-value">
                                            <input type="text" id="tanggal_akhir" name="tanggal_akhir" readonly="readonly"
                                                class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                data-plugin-options='{"autoclose": true}'>
                                            <span class="input-group-addon"><i
                                                    class="list-icon material-icons">date_range</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row mb-0">
                                    <p class="col-md-8 col-form-label">Deskripsi <span class="text-red">*</span> </p>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <textarea name="isi" id="isi" rows="5" class="form-control"
                                                placeholder="Masukan deskripsi kegiatan">
                                                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-4 col-form-label" for="l1">Urutan <span
                                            class="text-red">*</span></label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" name="urutan" id="urutan" autocomplete="off"
                                                class="form-control" placeholder="Masukan number urutan" pattern="[0-9]*"
                                                title="Format harus berisi angka saja ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-md-12 col-form-label" for="l1">Status</label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <select class="form-control" name="status" id="status">
                                                <option value="1">Tampilkan</option>
                                                <option value="0">Sembunyikan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update jadwal">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Tempat Sampah  -->
    <div class="modal modal-primary fade bs-modal-lg-primary" id="ajaxModelTrash" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="ajaxModelTrashLabel"><i class="list-icon fa fa-trash"></i> Tempat
                        Sampah </h5>
                </div>
                <div class="modal-body">
                    <table id="table_jadwal_trash" class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Append Create Datatables-->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

     <!-- Modal Table config  -->
     <div class="modal modal-primary fade bs-modal-sm-primary" id="ajaxModelConfig" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
     <div class="modal-dialog modal-sm">
         <div class="modal-content">
             <div class="modal-header text-inverse">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h5 class="modal-title" id="ajaxModelConfiglabel"><i class="list-icon fa fa-filter"></i> Filter Config Table Change </h5>
             </div>
             <div class="modal-body text-center">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="l13">Status</label>
                        <div class="col-md-9">
                            <select class="form-control" name="config_table_change" id="config_table_change">
                                <option value="">Pilih Status</option>
                                <option value="0">Semua</option>
                                <option value="1">Tampilkan - (Aktif)</option>
                                <option value="2">Sembunyikan - (Tidak Aktif)</option>
                            </select>
                        </div>
                    </div>
                </div>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-info change status">
                    <i class="material-icons list-icon">save</i>
                    Ubah
                </button>
                 <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                     this</button>
             </div>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_jadwal" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal Mulai </th>
                                    <th>Tanggal Berakhir </th>
                                    <th>Keterangan </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal Mulai </th>
                                    <th>Tanggal Berakhir </th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"
            $(document).ready(function() {

                var table_jadwal, table_trash;
                var config, config_trash,config_all_status,config_hidden;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewJadwal'
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        {
                            text: '<i class="fa fa-filter"></i>',
                            attr: {
                                title: 'Mode Config table',
                                id: 'changeConfigtable'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('data-ajax') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'tgl_mulai',
                            name: 'tgl_mulai',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'tgl_akhir',
                            name: 'tgl_akhir',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan',
                            visible: false
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                config_all_status = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewJadwal'
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        {
                            text: '<i class="fa fa-filter"></i>',
                            attr: {
                                title: 'Mode Config table',
                                id: 'changeConfigtable'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('jadwal-all-status') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'tgl_mulai',
                            name: 'tgl_mulai',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'tgl_akhir',
                            name: 'tgl_akhir',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan',
                            visible: false
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                config_hidden = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-plus"></i>',
                            attr: {
                                title: 'Tambah Data',
                                id: 'createNewJadwal'
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            text: '<i class="fa fa-trash"></i>',
                            attr: {
                                title: 'Data Trash',
                                id: 'data_trash'
                            }
                        },
                        {
                            text: '<i class="fa fa-filter"></i>',
                            attr: {
                                title: 'Mode Config table',
                                id: 'changeConfigtable'
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('status-hidden') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'tgl_mulai',
                            name: 'tgl_mulai',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'tgl_akhir',
                            name: 'tgl_akhir',
                            render: function(data, type, row) {
                                return moment(data).locale('id').format('LL');
                            }
                        },
                        {
                            data: 'keterangan',
                            name: 'keterangan',
                            visible: false
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_jadwal = $('#table_jadwal').dataTable(config);

                //change model table config
                $('body').on('click','button#changeConfigtable',function(){
                    $('#ajaxModelConfig').modal('show');
                });

                //select config
                $('body').on('click','button.change.status',function(){
                   var item = $('select[name="config_table_change"] option:selected').val();
                   if(item == '0'){
                      table_jadwal = $('#table_jadwal').dataTable(config_all_status);
                      $('#ajaxModelConfig').modal('hide');
                      window.notif("success", "Successfull Datatable all Status is change display ");
                   }else if(item =='1'){
                      table_jadwal = $('#table_jadwal').dataTable(config);
                      $('#ajaxModelConfig').modal('hide');
                      window.notif("success", "Successfull Datatable with Status Active is change display shown ");
                   }else if(item == '2'){
                      table_jadwal = $('#table_jadwal').dataTable(config_hidden);
                      $('#ajaxModelConfig').modal('hide');
                      window.notif("success", "Successfull Datatable with Status Non Active is change display shown ");
                   }else{
                     window.notif("error", "Silahkan Pilih Status ! ");
                   }

                });


                //open modal form
                $('#createNewJadwal').click(function() {
                    window.location.href = '{{ route('jadwal-tambah') }}';
                });

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //show data detail

                $('body').on('click', '.show.jadwal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('info-detail', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Jadwal Kegiatan");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    let status_tampilkan = "";
                                    if (rows.status_kode == 1) {
                                        status_tampilkan = "Tayangkan di web ";
                                    } else {
                                        status_tampilkan = "Disembunyikan sementara ";
                                    }

                                    bodytag = bodytag +
                                        '<dl class="row"><dt class="col-sm-3">Nama</dt>' +
                                        '<dd class="col-sm-9">' + rows.nama +
                                        '</dd><dt class="col-sm-3 text-truncate">Tempat</dt>' +
                                        '<dd class="col-sm-9">' + rows.tempat +
                                        '</dd><dt class="col-sm-3">Tanggal</dt>' +
                                        '<dd class="col-sm-9">' +
                                        '<dl class="row"><dt class="col-sm-3">' + moment(
                                            rows.tgl_mulai).locale('id').format('LL') +
                                        '</dt>' +
                                        '<dd class="col-sm-2"> s/d </dd>' +
                                        '<dd class="col-sm-3">' + moment(rows.tgl_akhir)
                                        .locale('id').format('LL') + '</dd>' +
                                        '</dl>' +
                                        '</dd>' +
                                        '<dt class="col-sm-3">Deskripsi</dt>' +
                                        '<dd class="col-sm-9">' +
                                        '<dl class="row">' +
                                        '<dd class="col-sm-12">' + rows.keterangan +
                                        '</dd>' +
                                        '</dl>' +
                                        '</dd>' +
                                        '<dt class="col-sm-3">Urutan</dt>' +
                                        '<dd class="col-sm-9">' +
                                        '<dl class="row">' +
                                        '<dd class="col-sm-3">' + rows.urutan + '</dd>' +
                                        '<dd class="col-sm-9">' + status_tampilkan +
                                        '</dd>' +
                                        '</dl>' +
                                        '</dd>' +
                                        '</dl>';
                                    $('.result-body').html(bodytag);

                                    $('#ajaxModelShow').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Jadwal Kegiatan");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShow').modal('show');
                                }
                            }

                        }
                    });

                });

                //edit data detail
                $('body').on('click', '.edit.jadwal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-detail', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            //console.log(result['data']);
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    $('#ajaxModelEdit').modal('show');
                                    $('#modelHeading.edit').html("Edit Jadwal Kegiatan");
                                    $('.result-body.edit').html('');
                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    $('input[name="id_jadwal"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="tempat"]').val(rows.tempat);
                                    $('input[name="tanggal_mulai"]').val(rows.tgl_mulai);
                                    $('input[name="tanggal_akhir"]').val(rows.tgl_akhir);
                                    $('input[name="urutan"]').val(rows.urutan);
                                    $('textarea[name="isi"]').val(rows.keterangan);
                                    $("select[name='status'] > option[value=" + rows
                                        .status_kode + "]").prop("selected", true);
                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-edit"></i>');
                                    $('#modelHeading.edit').html("Edit Jadwal Kegiatan");
                                    $('.result_form_error').html(result['message']);
                                    $('#ajaxModelEdit').modal('show');
                                }
                            }

                        }
                    });

                });

                //update data info jadwal

                $('body').on('submit', '#JadwalForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_jadwal"]').val();
                    var urlx = '{{ route('update-detail', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update.jadwal');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEdit').modal('hide');
                                    table_jadwal.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log ="";
                            if(typeof data !=='underfined'){
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                //remove  data jadwal
                $('body').on('click', 'button.remove.jadwal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('remove-detail', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //data trash


                $('body').on('click', 'button#data_trash', function() {
                    $('#ajaxModelTrash').modal('show');
                    config_trash = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: "{{ route('trash-info') }}",
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };
                    table_trash = $('#table_jadwal_trash').dataTable(config_trash);
                });

                //action button restore
                $('body').on('click', 'button.restore.jadwal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('restore-info', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin pulihkan data ini!",
                        type: "question",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.restoreData(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });

                //action button restore
                $('body').on('click', 'button.remove_force.jadwal', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-permanen-info', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini secara permanen , nantinya tidak dapat dipulihkan lagi!!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Lanjutkan!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.ForceDelete(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Proses Pemulihan data dibatalkan',
                                'error');
                        }
                    });
                });


                //function forcedelete
                window.ForceDelete = function clear(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }
                //function restore data
                window.restoreData = function restore(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_jadwal.fnDraw(false);
                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-undo"></i> restore');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-undo"></i> restore');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_jadwal.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }

            });

        })(jQuery, window);
    </script>
@endsection
