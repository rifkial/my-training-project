@extends('ppdb.layouts.app')
@section('ppdb.components')
<div class="row page-title clearfix">
    <div class="page-title-left d-inline-flex">
        <h5 class="mr-0 mr-r-5">Tambah Pengumuman </h5>
    </div>
    <!-- /.page-title-right -->
</div>
@if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-error border-error" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="widget-list">
            <div class="col-md-12 widget-holder">
                <div class="widget-body clearfix">
                    <strong>{{ $message }}</strong>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <form id="PengumumanForm" name="PengumumanForm" method="POST" class="form-horizontal" enctype="multipart/form-data"
            action="{{ route('pengumuman-simpan') }}">
            @csrf
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body">
                            <div class="form-group row">
                                <p class="mr-b-0">Judul <span class="text-red">*</span></p>
                                <div class="input-group">
                                    <input id="judul" type="text" name="judul" value="{{ old('judul') ?? '' }}" class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*" title="Format harus berisi alfanumerik atau huruf saja ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="mr-b-0">Isi <span class="text-red">*</span> </p>
                                <div class="input-group">
                                    <textarea name="isi" id="isi" class="form-control editor">{{ old('isi') ?? '' }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <p class="mr-b-0">Gambar</p>
                                <div class="input-group">
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                    class="form-group mb-1" width="auto" height="auto">
                                <p class="text-muted ml-2">Kosongkon form ini jika tidak ada perubahan gambar</p>
                            </div>
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <div class="form-actions">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 btn-list">
                            <button type="submit" class="btn btn-primary">
                                <i class="material-icons list-icon">save</i>
                                Simpan
                            </button>
                            <a class="btn btn-info" href="{{ route('informasi-pengumuman') }}">
                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                Kembali
                            </a>
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </div>
        </form>
    </div>
    <script>
        (function($, global) {
            "use-strict"

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table_brosur;
            let config;

            $(document).ready(function() {
                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();

                    }
                };

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert-dismissible").alert('close');
                });

            });

        })(jQuery, window);
    </script>
@endsection
