@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left d-inline-flex">
            <h5 class="mr-0 mr-r-5">Preview Pengumuman </h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Cetak Hasil Pengumuman
                    <a href="{{ route('print_pengumuman_cetak') }}" target="_blank">
                        <i class="material-icons list-icon">print</i>
                    </a>
                </a>
                </li>
            </ol>
        </div>
    </div>
    <div class="widget-list text-center" id="print-preview">
        <div class="col-md-12 widget-holder">
            <div class="widget-bg">
                <div class="widget-body clearfix">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form[4]['initial'] == 'logo1')
                                        <img src="{{ $form[4]['value'] }}" style="max-height:138px; min-width: 125px;">
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    @if ($form[0]['initial'] == 'head1')
                                        <b>{{ $form[0]['value'] }}</b>
                                    @endif
                                </td>
                                <td rowspan="4" style="vertical-align: middle">
                                    @if ($form[5]['initial'] == 'logo2')
                                        <img src="{{ $form[5]['value'] }}" style="max-height:138px; min-width: 125px;">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    @if ($form[1]['initial'] == 'head2')
                                    <h2 style="margin: 0"> <b>{{ $form[1]['value'] }}</b></h2>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    @if ($form[2]['initial'] == 'head3')
                                       <b>{{ $form[2]['value'] }}</b></h2>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    @if ($form[3]['initial'] == 'alamat')
                                        <b>{{ $form[3]['value'] }}</b>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr style="border: solid 2px #000">
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" style="text-align: center; font-weight: bold; font-size: 14pt">
                                    <u> Hasil Pengumuman </u>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px; text-align: center;" colspan="3">
                                    @if ($form[6]['initial'] == 'prolog')
                                        <b>{{ $form[6]['value'] }}</b>
                                    @endif
                                    @if ($form[15]['initial'] == 'nama')
                                        <b>{{ $form[15]['value'] }}</b>
                                    @endif
                                    <br>
                                    @if ($form[16]['initial'] == 'nisn')
                                        <b> NISN : {{ $form[16]['value'] }}</b>
                                    @endif

                                    @if ($form[17]['initial'] == 'keputusan')
                                        <h1><b>{{ $form[17]['value'] }}</b></h1>
                                    @endif
                                    @if ($form[18]['initial'] == 'keterangan_keputusan')
                                        <b> catatan : {{ $form[18]['value'] ?? '-' }}</b>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                                    @if ($form[12]['initial'] == 'tempat_keputusan')
                                        <small><b>{{ $form[12]['value'] }}</b></small>,
                                    @endif
                                    @if ($form[13]['initial'] == 'tgl_keputusan')
                                        <small>{{ $form[13]['value'] }}</small>
                                    @endif
                                    <h5>Kepala Sekolah</h5>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                                    @if ($form[10]['initial'] == 'stempel')
                                        <img src="{{ $form[10]['value'] }}" width="170px" height="170px"
                                            style=" text-align: right;
                                                   position: absolute;margin-top: -89px;margin-right: -33px;
                                                   margin-left: -90px;
                                            ">
                                    @endif
                                </td>
                                <td colspan="2" style="text-align: right; font-weight: bold; font-size: 14pt">
                                    @if ($form[8]['initial'] == 'ttd_kepsek')
                                        <img src="{{ $form[8]['value'] }}"
                                            style=" text-align: right; max-height:138px; min-width: 125px; padding: 13px; margin-right: -16px;">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 14pt">
                                    @if ($form[9]['initial'] == 'nama_kepsek')
                                        <small><u><b>{{ $form[9]['value'] }}</b></u></small><br>
                                    @endif
                                    @if ($form[11]['initial'] == 'nip_kepsek')
                                        <small><b>{{ $form[11]['value'] }}</b></small>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
