@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Pembayaran Pending (Belum dibayar)</h5>
        </div>
    </div>
    <!-- popup detail -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPayment" role="dialog" aria-labelledby="ajaxModelShow"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="result-body"></div>
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">NISN</label>
                                        <div class="col-md-9">
                                            <input class="form-control nisn" id="nisn" name="nisn" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Peserta</label>
                                        <div class="col-md-9">
                                            <input class="form-control peserta" id="peserta" name="peserta" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Metode bayar</label>
                                        <div class="col-md-9">
                                            <input class="form-control metode" id="metode" name="metode" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nominal</label>
                                        <div class="col-md-9">
                                            <input class="form-control nominal" id="nominal" name="nominal" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tanggal Bayar</label>
                                        <div class="col-md-9">
                                            <input class="form-control tanggal_bayar" id="tanggal_bayar"
                                                name="tanggal_bayar" type="text" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Status</label>
                                        <div class="col-md-9">
                                            <input class="form-control status" id="status" name="status" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nama Pengirim</label>
                                        <div class="col-md-9">
                                            <input class="form-control pengirim" id="pengirim" name="pengirim" type="text"
                                                readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Asal Bank Pengirim</label>
                                        <div class="col-md-9">
                                            <input class="form-control asal_bank" id="asal_bank" name="asal_bank"
                                                type="text" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Tujuan Bank Transfer</label>
                                        <div class="col-md-9">
                                            <input class="form-control tujuan_bank" id="tujuan_bank" name="tujuan_bank"
                                                type="text" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Nomor Rekening Tujuan </label>
                                        <div class="col-md-9">
                                            <input class="form-control nomor_rekening" id="nomor_rekening"
                                                name="nomor_rekening" type="text" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l0">Bukti Pembayaran </label>
                                        <div class="col-md-9">
                                            <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                class="form-group mb-1" width="150px" height="150px"
                                                style="margin-top: 10px">
                                        </div>
                                        <div class="download-file"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- popup detail -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPaymentEdit" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading2"></h5>
                </div>
                <form id="PxForm" name="PxForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="result-body"></div>
                            <div class="table-responsive">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="id_pembayaran" class="form-control"
                                            id="id_pembayaran" />
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">NISN</label>
                                            <div class="col-md-9">
                                                <input class="form-control nisn" id="nisn" name="nisn" type="text"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Peserta</label>
                                            <div class="col-md-9">
                                                <input class="form-control peserta" id="peserta" name="peserta" type="text"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Metode bayar</label>
                                            <div class="col-md-9">
                                                <input class="form-control metode" id="metode" name="metode" type="text"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nominal</label>
                                            <div class="col-md-9">
                                                <input class="form-control nominal" id="nominal" name="nominal" type="text"
                                                    readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tanggal Bayar</label>
                                            <div class="col-md-9">
                                                <input class="form-control tanggal_bayar" id="tanggal_bayar"
                                                    name="tanggal_bayar" type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Status</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select class="form-control" name="status_pembayaran"
                                                        id="status_pembayaran">
                                                        <option value="">--Pilih Status Pembayaran ---</option>
                                                        <option value="1">Diterima</option>
                                                        <option value="2">Ditolak</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="l38">Catatan</label>
                                            <textarea class="form-control" name="catatan" id="catatan"
                                                rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nama Pengirim</label>
                                            <div class="col-md-9">
                                                <input class="form-control pengirim" id="pengirim" name="pengirim"
                                                    type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Asal Bank Pengirim</label>
                                            <div class="col-md-9">
                                                <input class="form-control asal_bank" id="asal_bank" name="asal_bank"
                                                    type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Tujuan Bank Transfer</label>
                                            <div class="col-md-9">
                                                <input class="form-control tujuan_bank" id="tujuan_bank" name="tujuan_bank"
                                                    type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Nomor Rekening Tujuan </label>
                                            <div class="col-md-9">
                                                <input class="form-control nomor_rekening" id="nomor_rekening"
                                                    name="nomor_rekening" type="text" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l0">Bukti Pembayaran </label>
                                            <div class="col-md-9">
                                                <img id="modal-preview2" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="150px" height="150px"
                                                    style="margin-top: 10px">
                                            </div>
                                            <div class="download-file2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update jadwal">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left"
                            data-dismiss="modal">Close
                            this</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- /.modal -->
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_history_pembayaran" class="table table-striped table-responsive table-hover">
                            <thead class="table-info">
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>tagihan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--- Append datatables  -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>tagihan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>
    <script>
        (function($, global) {
            "use-strict"

            let table_payment;
            let config;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        {
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('datasource-pending') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'telepon',
                            name: 'telepon'
                        },
                        {
                            data: 'nominal',
                            name: 'nominal'
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },

                    ],
                };

                table_payment = $('#table_history_pembayaran').dataTable(config);

                $('body').on('click', '.show.payment', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-payment', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    if (result['data'] == null || result['data'] == '') {
                                        swa('info', 'Peserta belum melakukan pembayaran!');
                                        $(loader).html('<i class="fa fa-eye"></i>');

                                    } else {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                        $('#modelHeading').html("Info Detail Pembayaran ");
                                        $('.result-body').html('');

                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));
                                        $('input[name="nisn"]').val(rows.nisn);
                                        $('input[name="peserta"]').val(rows.nama);
                                        $('input[name="metode"]').val(rows.bayar_via);
                                        $('input[name="nominal"]').val(rows.nominal);
                                        $('input[name="tanggal_bayar"]').val(rows
                                            .tgl_bayar);
                                        $('input[name="pengirim"]').val(rows.atas_nama);
                                        $('input[name="asal_bank"]').val(rows.asal_bank);
                                        $('input[name="tujuan_bank"]').val(rows
                                            .tujuan_bank);
                                        $('input[name="nomor_rekening"]').val(rows
                                            .no_rekening);
                                        $('input[name="status"]').val(rows
                                            .status_pembayaran);

                                        if (rows.bukti != 'null' || rows.bukti !==
                                            'underfined' || rows.bukti != '') {
                                            $('#modal-preview').removeAttr('src');
                                            $('#modal-preview').attr('src', rows.bukti);
                                            $('.download-file').html(result['download']);
                                        }

                                        $('#ajaxModelShowPayment').modal('show');
                                    }

                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Pembayaran");
                                    $('#result-body').html(result['message']);
                                    $('#ajaxModelShowPayment').modal('show');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.payment', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('edit-payment', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    if (result['data'] == null || result['data'] == '') {
                                        swa('info', 'Peserta belum melakukan pembayaran!');
                                        $(loader).html('<i class="fa fa-edit"></i>');
                                    } else {
                                        $(loader).html('<i class="fa fa-eye"></i>');
                                        $('#modelHeading2').html(
                                            "Info Edit Detail Pembayaran ");
                                        $('.result-body').html('');

                                        var rows = JSON.parse(JSON.stringify(result[
                                            'data']));
                                        $('input[name="id_pembayaran"]').val(rows.id);
                                        $('input[name="nisn"]').val(rows.nisn);
                                        $('input[name="peserta"]').val(rows.nama);
                                        $('input[name="metode"]').val(rows.bayar_via);
                                        $('input[name="nominal"]').val(rows.nominal);
                                        $('input[name="tanggal_bayar"]').val(rows
                                        .tgl_bayar);
                                        $('input[name="pengirim"]').val(rows.atas_nama);
                                        $('input[name="asal_bank"]').val(rows.asal_bank);
                                        $('input[name="tujuan_bank"]').val(rows
                                        .tujuan_bank);
                                        $('input[name="nomor_rekening"]').val(rows
                                            .no_rekening);

                                        $("select[name='status_pembayaran'] > option[value=" +
                                            rows.status_pembayaran_kode + "]").prop(
                                            "selected", true);

                                        if (rows.bukti != 'null' || rows.bukti !==
                                            'underfined' || rows.bukti != '') {
                                            $('#modal-preview2').removeAttr('src');
                                            $('#modal-preview2').attr('src', rows.bukti);
                                            $('.download-file2').html(result['download']);
                                        }
                                        $('#ajaxModelShowPaymentEdit').modal('show');
                                    }


                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading2').html("Info Edit Detail Pembayaran");
                                    $('#result-body').html(result['message']);
                                    $('#ajaxModelShowPaymentEdit').modal('show');
                                }
                            }

                        }
                    });

                });

                //update
                $('body').on('submit', '#PxForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_pembayaran"]').val();
                    var urlx = '{{ route('update-pembayaran', ':id') }}';
                    urlx = urlx.replace(':id', id);
                    var formData = new FormData(this);

                    const loader = $('button.update');

                    $.ajax({
                        type: "PUT",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    $('#ajaxModelShowPaymentEdit').modal('hide');

                                    table_payment.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['info'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['info'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });


                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>
@endsection
