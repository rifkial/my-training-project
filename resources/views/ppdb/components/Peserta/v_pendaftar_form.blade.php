@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta > Tambah Data Pendaftar </h5>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @else
        <div class="row page-title clearfix">
            <div class="page-title-left">
            </div>
            <div class="page-title-right d-inline-flex">
                <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*) Form harus disi!.</p>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('add-save') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <span id="form_result"></span>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Nama <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nama" id="nama" autocomplete="off"
                                                        class="form-control" value="{{ old('nama') }}"
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja "
                                                        placeholder="nama">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">NISN <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="nisn" id="nisn" autocomplete="off"
                                                        class="form-control" value="{{ old('nisn') }}"
                                                        pattern="[0-9/\s]*"
                                                        title="Format harus berisi angka saja "
                                                        placeholder="Nisn">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Email <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="email" name="email" id="email" autocomplete="off"
                                                        class="form-control" value="{{ old('email') }}"
                                                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                        title="Format email salah "
                                                        placeholder="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Telepon
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="tel" name="telepon" id="telepon" autocomplete="off"
                                                        class="form-control" value="{{ old('telepon') }}"
                                                        pattern="[0-9/\s]*"
                                                        title="Format harus berisi angka saja "
                                                        placeholder="telepon">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin <span
                                                    class="text-red">*</span></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select class="form-control" name="jenkel" id="l1">
                                                        <option value="">--Pilih jenis kelamin ---</option>
                                                        <option value="l">Laki-Laki</option>
                                                        <option value="p">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 col-form-label" for="l1">Alamat
                                             </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <textarea name="alamat" id="alamat" rows="5" cols="5" class="form-control text-left"
                                                        placeholder="alamat">
                                                        {{ old('alamat') }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tempat Lahir </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="tempat_lahir" id="tempat_lahir" autocomplete="off"
                                                        class="form-control" value="{{ old('tempat_lahir') }}"
                                                        pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                        title="Format harus berisi alfanumerik atau huruf saja "
                                                        placeholder="tempat lahir">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Agama
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" name="agama" id="agama" autocomplete="off"
                                                        class="form-control" value="{{ old('agama') }}"
                                                        pattern="[a-zA-Z/\s]*"
                                                        title="Format harus berisi huruf saja "
                                                        placeholder="Agama">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Tanggal Lahir
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <input type="text" id="tanggal_lahir" name="tanggal_lahir"
                                                        value="{{ old('tanggal_lahir') }}" readonly="readonly"
                                                        class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                                        data-plugin-options='{"autoclose": true}'>
                                                    <span class="input-group-addon"><i
                                                            class="list-icon material-icons">date_range</i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <label class="col-md-3 col-form-label" for="l1"></label>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label class="col-md-3 col-form-label pt-1" for="l1">Photo <span
                                                            class="text-red">*</span> </label>
                                                        <img id="modal-preview" src="https://via.placeholder.com/150"
                                                            alt="Preview" class="form-group mb-1" width="150px" height="150px"
                                                            style="margin-top: 10px">
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="form-group row">
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input id="image" type="file" name="image" accept="image/*"
                                                                        onchange="readURL(this);">
                                                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <p class="mr-b-0">Password<span class="text-red">*</span></p>
                                                            <div class="input-group">
                                                                <input id="password" type="password" name="password" class="form-control password"
                                                                    placeholder="xxxx"
                                                                    title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                                                    <div class="input-group-addon toggle-password">
                                                                        <a href="javascript:void(0)"><i
                                                                            class="material-icons list-icon eye text-dark">remove_red_eye</i></a>
                                                                    </div>
                                                                    <div class="input-group-addon">
                                                                        <a href="javascript:void(0)" id="generate_pass">Generate Password</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Simpan
                                            </button>
                                            <a class="btn btn-info" href="{{ route('akun-pendaftar') }}">
                                                <i class="material-icons list-icon">keyboard_arrow_left</i>
                                                Kembali
                                            </a>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        (function($, global) {
        "use-strict"
        let config;
        let table_doc;
        $(document).ready(function() {
            //read file image upload
            window.readURL = function name(input, id) {
                id = id || '#modal-preview';
                if (input.files[0]) {
                    var reader = new FileReader();
                    var blob = input.files[0];

                    if (blob.type == 'application/pdf' || blob.type == 'application/doc') {
                        $('#modal-preview').hide();
                        $('#start').hide();
                    } else {
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').show();
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();
                    }

                }
            };

            //auto hide alert
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                $(".alert-dismissible").alert('close');
            });

            const mata = document.querySelector(".eye")
            const inputPass = document.querySelector(".password");
            mata.addEventListener("click", () => {
                    mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                    if (inputPass.type === "password") {
                        inputPass.setAttribute("type", "text")

                    } else if (inputPass.type === "text") {
                        inputPass.setAttribute("type", "password")
                        mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                    }
                });

              //generate password
              function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPass.value = retVal;
                    inputPass.value = retVal;
                }

                $('body').on('click','a#generate_pass',function(){
                    generatePassword();
                });

        });

        })(jQuery, window);
    </script>
@endsection
