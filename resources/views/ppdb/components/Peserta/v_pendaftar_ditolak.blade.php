@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Peserta > Pendaftar Ditolak </h5>
        </div>
        <!-- /.page-title-left -->
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Pendaftar Ditolak
                </li>
                <li class="breadcrumb-item"><a href="{{ route('akun-pendaftar') }}">Akun</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('register-ulang') }}">Register</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('doc-support') }}">Dokumen Pendukung</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ route('peserta_diterima') }}">Pendaftar Diterima</a>
                </li>
            </ol>
        </div>
        <!-- /.page-title-right -->
    </div>
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- popup detail -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelShowPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background:#fb9678;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title text-center text-white" id="modelHeading"></h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="tabs tabs-vertical">
                            <ul class="nav nav-tabs flex-column">
                                <li class="nav-item active"><a class="nav-link" href="#profile-tab-v1"
                                        data-toggle="tab" aria-expanded="false">Profil</a>
                                </li>
                                <li class="nav-item"><a class="nav-link doc_file" href="#messages-tab-v1"
                                        data-toggle="tab" aria-expanded="false">Dokumen Pendukung</a>
                                </li>
                            </ul>
                            <!-- /.nav-tabs -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile-tab-v1" aria-expanded="false">
                                    <div class="result-body"></div>
                                </div>
                                <div class="tab-pane" id="messages-tab-v1" aria-expanded="false">
                                    <table id="table_doc_file" class="table table-striped table-responsive">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Gambar</th>
                                                <th>Jenis File </th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Append Create Datatables-->
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Gambar</th>
                                                <th>Jenis File </th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                            <!-- /.tab-content -->
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                        this</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit  -->
    <div class="modal fade bs-modal-lg" tabindex="-1" id="ajaxModelEditPendaftar" role="dialog"
        aria-labelledby="ajaxModelShow" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <form id="UpdateForm" name="UpdateForm" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="background:#fb9678;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title text-center text-white edit" id="modelHeadingx"> Edit Form Peserta Pendaftar
                        </h5>
                    </div>
                    <div class="modal-body">
                        <span id="form_result"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row page-title clearfix mb-3 ">
                                    <div class="page-title-left">
                                    </div>
                                    <div class="page-title-right d-inline-flex">
                                        <p class="text-danger "> <i class="material-icons list-icon">warning</i> Tanda (*)
                                            Form harus disi!.</p>
                                    </div>
                                </div>
                                <input type="hidden" name="id_peserta" class="form-control ">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Nama
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nama" id="nama" autocomplete="off"
                                                class="form-control" pattern="[a-zA-Z][a-zA-Z0-9/\s]*"
                                                title="Format harus berisi alfanumerik atau huruf saja " placeholder="nama"
                                                disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">NISN
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" name="nisn" id="nisn" autocomplete="off"
                                                class="form-control" pattern="[0-9/\s]*"
                                                title="Format harus berisi angka saja " placeholder="Nisn"
                                                disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label" for="l1">Jenis Kelamin
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="jenkel" id="l1" disabled="disabled">
                                                <option value="">--Pilih jenis kelamin ---</option>
                                                <option value="l">Laki-Laki</option>
                                                <option value="p">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row mb-0">
                                    <label class="col-md-3 col-form-label" for="l1"></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="col-md-3 col-form-label pt-1" for="l1">Photo </label>
                                                <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview"
                                                    class="form-group mb-1" width="150px" height="150px"
                                                    style="margin-top: 10px">
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="status">Keputusan<span
                                                        class="text-red">*</span></label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <select class="form-control" name="keputusan" id="keputusan">
                                                            <option value="">--Pilih Keputusan ---</option>
                                                            <option value="1">Diterima</option>
                                                            <option value="0">Ditolak</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">Catatan
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <textarea class="form-control" id="catatan" name="catatan" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info update jadwal">
                            <i class="material-icons list-icon">save</i>
                            Update
                        </button>
                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal">Close
                            this</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <!-- /.widget-heading -->
                    <div class="widget-body clearfix">
                        <table id="table_pendaftar_peserta" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Keputusan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Append Create Datatables-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nisn</th>
                                    <th>Peserta</th>
                                    <th>Email</th>
                                    <th>Photo</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Telephone</th>
                                    <th>Keputusan</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.widget-body -->
                </div>
                <!-- /.widget-bg -->
            </div>
            <!-- /.widget-holder -->
        </div>
        <!-- /.row -->
    </div>

    <script>
        (function($, global) {
            "use-strict"

            let table_doc;
            let config, config_trash;

            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                config = {
                    destroy: true,
                    dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                    buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 5, 6, 7]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 5, 6, 7]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            },
                            customize: function(doc) {
                                doc.content[1].table.widths =
                                    Array(doc.content[1].table.body[0].length + 1).join('*').split(
                                        '');
                            }
                        },
                        'colvis',
                    ],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('data-peserta_ditolak') }}",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'nisn',
                            name: 'nisn'
                        },
                        {
                            data: 'nama_peserta',
                            name: 'nama_peserta'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'photo',
                            name: 'photo',
                        },
                        {
                            data: 'jenkel',
                            name: 'jenkel',
                        },
                        {
                            data: 'telepon',
                            name: 'telepon',
                        },
                        {
                            data: 'keputusan',
                            name: 'keputusan',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        }
                    ],
                };

                table_doc = $('#table_pendaftar_peserta').dataTable(config);

                //open modal form redirect
                $('#createNewPeserta').click(function() {
                    //window.location.href = '{{ route('add-form') }}';
                });

                $('body').on('click', '.show.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('detail-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    let bodytag = "";
                    let profiltag = "";
                    let alamat = '';
                    let email = '';
                    let telepon = '';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Peserta ");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));

                                    $('.doc_file').attr('data-id', rows.id);

                                    var url_detail = '{{ route('detail-pendaftar-regiter', ':id') }}';
                                    url_detail = url_detail.replace(':id', id);

                                    bodytag = bodytag + '<div class="row">' +
                                        '<div class="col-md-12 text-center">' +
                                        '<div class="col-md-12 col-lg-12 widget-holder">' +
                                        '<div class="widget-bg">' +
                                        '<div class="widget-body clearfix">' +
                                        '<div class="contact-info">' +
                                        '<header class="text-center">' +
                                        '<figure class="inline-block user--online thumb-lg">' +
                                        '<img src="' + rows.file +
                                        '" class="rounded-circle img-thumbnail" alt="">' +
                                        '</figure>' +
                                        '<h4 class="mt-1"><a href="#">' + rows.nama +
                                        '</a></h4>' +
                                        '<p>' + rows.nisn + '</p>' +
                                        '<a href="'+url_detail+'" target="_blank">Lihat Selengkapnya </a>' +
                                        '</header>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                    $('.result-body').html(bodytag);

                                    $('#ajaxModelShowPendaftar').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeading').html("Info Detail Jadwal Kegiatan");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelShowPendaftar').modal('show');
                                }
                            }

                        }
                    });

                });

                $('body').on('click', '.edit.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var gambar = $(this).data('gambar');
                    var url = '{{ route('edit-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Pendaftar");
                                    $('.result-body').html('');

                                    var rows = JSON.parse(JSON.stringify(result['data']));
                                    //console.log(result['data']);
                                    $('input[name="id_peserta"]').val(rows.id);
                                    $('input[name="nama"]').val(rows.nama);
                                    $('input[name="nisn"]').val(rows.nisn);
                                    $("select[name='jenkel'] > option[value=" + rows
                                        .jenkel + "]").prop("selected", true);

                                    $("select[name='keputusan'] > option[value=" + rows
                                        .diterima + "]").prop("selected", true);

                                    if (rows.file !== null || gambar !== null) {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src', rows.file);
                                    } else {
                                        $('#modal-preview').removeAttr('src');
                                        $('#modal-preview').attr('src',
                                            'https://via.placeholder.com/150');
                                    }

                                    $('#ajaxModelEditPendaftar').modal('show');
                                } else {
                                    $(loader).html('<i class="fa fa-eye"></i>');
                                    $('#modelHeadingx').html("Edit Form Peserta Pendaftar");
                                    $('#result-body').html($result['message']);
                                    $('#ajaxModelEditPendaftar').modal('show');
                                }
                            }

                        }
                    });
                });

                //update
                $('body').on('submit', '#UpdateForm', function(e) {
                    e.preventDefault();
                    var id = $('input[name="id_peserta"]').val();
                    var urlx = '{{ route('update-pendaftar-status', ':id') }}';
                    urlx = urlx.replace(':id', id);

                    var formData = new FormData(this);

                    const loader = $('button.update.feed');

                    $.ajax({
                        type: "POST",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    $('#ajaxModelEditPendaftar').modal('hide');
                                    table_doc.fnDraw(false);
                                    $(loader).html('<i class="fa fa-save"></i> Update');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-save"></i> Update');
                                    $('.result_form_error').html(result['message']);
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            //$('#ajaxModelEdit').modal('hide');
                            let log = "";
                            if (typeof data !== 'underfined') {
                                let item = data['responseJSON']['errors'];
                                for (var key in item) {
                                    console.log(item[key])
                                    log = log + item[key];
                                }
                                window.notif('error', log);
                                $(loader).html('<i class="fa fa-save"></i> Update');
                            }

                        }
                    });

                });

                $('body').on('click', '.doc_file', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('file-peserta', ':id') }}';
                    url = url.replace(':id', id);
                    var config_file = {
                        destroy: true,
                        dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
                        buttons: [{
                            text: '<i class="fa fa-refresh"></i>',
                            action: function(e, dt, node, config) {
                                dt.ajax.reload(null, false);
                            }
                        }, ],
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: url,
                        columns: [{
                                data: 'DT_RowIndex',
                                name: 'DT_RowIndex'
                            },
                            {
                                data: 'nama',
                                name: 'nama'
                            },
                            {
                                data: 'photo',
                                name: 'photo'
                            },
                            {
                                data: 'extension',
                                name: 'extension'
                            },
                            {
                                data: 'action',
                                name: 'action',
                                orderable: false,
                                searchable: false
                            }
                        ],
                    };

                    var table_doc_file = $('#table_doc_file').dataTable(config_file);

                });

                //remove  data
                $('body').on('click', 'button.remove.download.pendaftar', function() {
                    var id = $(this).data('id');
                    var url = '{{ route('delete-pendaftar', ':id') }}';
                    url = url.replace(':id', id);
                    const loader = $(this);
                    swal({
                        title: "Apa kamu yakin?",
                        text: "ingin menghapus data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus Saja!',
                        cancelButtonText: 'Tidak, Batalkan!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function() {
                        window.remove(url, loader);

                    }, function(dismiss) {
                        if (dismiss === 'cancel') {
                            window.swa("Dibatalkan!", 'Data Kamu masih aman', 'error');
                        }
                    });
                });

                //function remove
                window.remove = function remove(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['message'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {
                                    table_doc.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');

                                    window.notif(result['icon'], result['message']);

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Remove');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function restore data
                window.restoreData = function restore(urlx, loader) {
                    $.ajax({
                        type: "PATCH",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_doc.fnDraw(false);
                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-undo"></i> restore');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-undo"></i> restore');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }

                //function forcedelete
                window.ForceDelete = function clear(urlx, loader) {
                    $.ajax({
                        type: "DELETE",
                        url: urlx,
                        beforeSend: function() {
                            $(loader).html(
                                '<i class="fa fa-spin fa-spinner"></i> Loading');
                        },
                        data: "",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (typeof result['data'] !== 'underfined' && typeof result[
                                    'info'] !== 'underfined') {
                                if (result['info'] == 'success') {

                                    table_trash.fnDraw(false);
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');

                                    window.notif(result['icon'], result['message']);

                                    //$('#ajaxModelTrash').modal('hide');

                                } else if (result['info'] == 'error') {
                                    $(loader).html('<i class="fa fa-trash-o"></i> Delete');
                                    window.notif(result['icon'], result['message']);
                                }
                            }
                        },
                        error: function(data) {
                            window.notif('error', data);
                        }
                    });

                    return true;
                }


                //read file image upload
                window.readURL = function name(input, id) {
                    id = id || '#modal-preview';
                    if (input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(id).attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                        $('#modal-preview').removeClass('hidden');
                        $('#start').hide();
                    }
                };

                window.getExt = function getx(path) {
                    var basename = path.split(/[\\/]/).pop(), // extract file name from full path ...
                        // (supports `\\` and `/` separators)
                        pos = basename.lastIndexOf("."); // get last position of `.`

                    if (basename === "" || pos < 1) // if file name is empty or ...
                        return ""; //  `.` not found (-1) or comes first (0)

                    return basename.slice(pos + 1);
                }

                //auto hide alert
                $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert-dismissible").alert('close');
                });

                const mata = document.querySelector(".eye")
                const inputPass = document.querySelector(".password");
                if (mata) {
                    mata.addEventListener("click", () => {
                        mata.innerHTML = `<i class="fa fa-eye-slash" aria-hidden="true"></i>`;

                        if (inputPass.type === "password") {
                            inputPass.setAttribute("type", "text")

                        } else if (inputPass.type === "text") {
                            inputPass.setAttribute("type", "password")
                            mata.innerHTML = `<i class="fa fa-eye" aria-hidden="true"></i>`;
                        }
                    });
                }

                //generate password
                function generatePassword() {
                    var length = 8,
                        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                        retVal = "";
                    for (var i = 0, n = charset.length; i < length; ++i) {
                        retVal += charset.charAt(Math.floor(Math.random() * n));
                    }

                    inputPass.value = retVal;
                    inputPass.value = retVal;
                }

                $('body').on('click', 'a#generate_pass', function() {
                    generatePassword();
                });

                //toast
                window.notif = function notif(tipe, value) {
                    $.toast({
                        icon: tipe,
                        text: value,
                        hideAfter: 5000,
                        showConfirmButton: true,
                        position: 'top-right',
                    });
                    return true;
                }

                //sweet alert
                window.swa = function swa(status, message, icon) {
                    swal(
                        status,
                        message,
                        icon
                    );
                    return true;
                }


            });


        })(jQuery, window);
    </script>

@endsection
