@extends('ppdb.layouts.app')
@section('ppdb.components')
    <div class="row page-title clearfix">
        <div class="page-title-left">
            <h5 class="mr-0 mr-r-5">Form Pendaftaran PPDB </h5>
        </div>
        <div class="page-title-right d-inline-flex">
            <ol class="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Form Pendaftaran
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('upload-file') }}">Upload Dokumen Pendukung</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('print-form-user') }}">Cetak Form Pendaftaran </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('print-card-user') }}">Cetak Kartu Peserta</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('print_pengumuman-user') }}">Cetak Hasil Pengumuman</a>
                    </li>
                </ol>
            </ol>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger border-info mt-1" role="alert">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button"><span
                    aria-hidden="true">×</span>
            </button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <div class="row">
                            <i class="material-icons list-icon md-48">warning</i>
                            <ul class="mr-t-10">
                                @foreach ($errors->all() as $error)
                                    <li class="text-red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-error border-error" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($message = Session::get('success'))
        <div class="alert alert-success border-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div class="widget-list">
                <div class="col-md-12 widget-holder">
                    <div class="widget-body clearfix text-center">
                        <i class="material-icons list-icon">check_circle</i>
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="widget-list">
        <div class="row">
            <div class="col-md-12 widget-holder">
                <div class="widget-bg">
                    <div class="widget-body">
                        <form id="DocForm" name="DocForm" method="POST" action="{{ route('save-register') }}"
                            class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            @foreach ($form as $key => $val)
                                @switch($val['tipe'])
                                    @case('option')
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l13">{{ $val['nama'] }}</label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="{{ $val['id'] }}"
                                                    name="form[{{ $val['id'] }}]">
                                                    @if ($val['initial'] == 'jenkel')
                                                        <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }} ---
                                                        </option>
                                                        @foreach (explode(',', $val['value']) as $info)
                                                            <option value="l">
                                                                {{ $info == 'l' ? 'Laki-Laki' : 'Perempuan' }}</option>
                                                            <option value="p">
                                                                {{ $info == 'p' ? 'Laki-laki' : 'Perempuan' }}</option>
                                                        @endforeach
                                                    @elseif ($val['initial'] == 'jurusan' || $val['initial'] =='jurusan2' ||
                                                        $val['initial'] =='jurusan3' || $val['initial'] =='jurusan_diterima')
                                                        <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }} ---
                                                        </option>
                                                        @foreach (explode(',', $val['value']) as $info)
                                                            <option value="{{ $info }}">{{ $info }}</option>
                                                        @endforeach
                                                    @elseif($val['initial'] == 'gelombang')
                                                        <option value="1"> Gelombang 1
                                                        </option>
                                                        <option value="2"> Gelombang 2
                                                        </option>
                                                        <option value="3"> Gelombang 3
                                                        </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @break
                                    @case('date')
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <input type="text" id="{{ $val['id'] }}"
                                                        name="form[{{ $val['id'] }}]"
                                                        value="{{ old('value') ?? ($val['value'] ?? '') }}"
                                                        readonly="readonly" class="form-control datepicker"
                                                        data-date-format="yyyy-mm-dd" data-plugin-options='{"autoclose": true}'>
                                                    <span class="input-group-addon"><i
                                                            class="list-icon material-icons">date_range</i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @break
                                    @case('text')
                                        @if ($val['initial'] == 'jalur_pendaftaran')
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l13">{{ $val['nama'] }}</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="{{ $val['id'] }}"
                                                        name="form[{{ $val['id'] }}]">
                                                        <option value="" disabled="disabled">-- Pilih {{ $val['nama'] }} ---
                                                        </option>
                                                        @foreach (explode(',', $val['value']) as $info)
                                                            <option value="{{ $info }}">{{ $info }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @else
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <input type="text" name="form[{{ $val['id'] }}]"
                                                            id="{{ $val['id'] }}" autocomplete="off" class="form-control"
                                                            value="{{ old('value') ?? ($val['value'] ?? '') }}"
                                                            title="Format harus berisi angka saja "
                                                            placeholder="{{ $val['nama'] }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @break
                                    @case('textarea')
                                        <div class="form-group row ">
                                            <label class="col-md-3 col-form-label" for="l1">{{ $val['nama'] }}
                                            </label>
                                            <div class="col-md-9">
                                                <div class="input-group input-has-value">
                                                    <textarea name="form[{{ $val['id'] }}]" id="{{ $val['id'] }}"
                                                        rows="5" cols="5" class="form-control text-left"
                                                        placeholder="{{ $val['nama'] }}">
                                                                                                        {{ old('value') ?? ($val['value'] ?? '') }}
                                                                                     </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @break

                                @endswitch
                            @endforeach

                            <div class="form-actions">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 btn-list">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="material-icons list-icon">save</i>
                                                Daftar
                                            </button>
                                        </div>
                                        <!-- /.col-sm-12 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
