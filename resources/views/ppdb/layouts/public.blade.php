<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('ppdb.includes.public.head')
</head>

<body class="sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <header>
            <nav class="navbar navbar_fixed">
                <div class="navbar-header">
                    <a href="{{ route('ppdb-public') }} " class="navbar-brand">
                        <img class="logo-expand" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                            style="max-width: 25%;">
                        <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                            style="max-width: 50%;">
                        <span class="text-white logo-expand"> E-PPDB </span>
                        <span class="text-white logo-collapse"> E-PPDB </span>
                    </a>
                </div>
                <div class="spacer"></div>

                <ul class="nav navbar-nav float-right">
                    <li class="sidebar-toggle">
                        <a href="javascript:void(0)" class="ripple">
                            <i class="material-icons list-icon">menu</i>
                        </a>
                    </li>
                </ul>
            </nav>
            <aside class="site-sidebar x clearfix valign">
                <div class="nav-top ">
                    @include('ppdb.includes.public.navbar')
                </div>
            </aside>
        </header>

        <main class="main-wrapper clearfix mb-2">
            @switch(session('sidebar'))
                @case('')
                    <div class="body_fix mr-4 ml-1 ">
                        <div class="mb-2">
                            <div class="row">
                                <div class="col-md-12 xb">
                                    <div class="widget-list banner_slide">
                                        <div class="widget-body clearfix">
                                            @include('ppdb.includes.public.slide_banner')
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 ">
                                    <div class="widget-list">
                                        <div class="widget-body clearfix">
                                            @yield('ppdb.components')
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <div class="widget-list">
                                        <div class="widget-body clearfix">
                                            @include('ppdb.includes.public.v_formreg')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @break
                @case('show')
                    <div class="col-md-12 body_fix ">
                        <div class="row">
                            @if ($message = Session::get('success'))
                                <div class="col-md-12" style="margin-bottom: 6rem !important;">
                                    <div class="alert alert-info border-info" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div class="widget-list card">
                                            <div class="col-md-12 widget-holder card-body">
                                                <div class="widget-body clearfix">
                                                    <strong class="text-info text-center"> <i
                                                            class="list-icon fa fa-check-square-o"></i>
                                                        {{ $message }}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if ($message = Session::get('error'))
                                <div class="col-md-12" style="margin-bottom: 6rem !important;">
                                    <div class="alert alert-error border-error" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div class="widget-list card">
                                            <div class="col-md-12 widget-holder card-body">
                                                <div class="widget-body clearfix">
                                                    <strong class="text-red text-center"><i
                                                            class="list-icon fa fa-warning"></i>
                                                        {{ $message }}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-8 xb">
                                <div class="widget-list banner_slide">
                                    <div class="widget-body clearfix">
                                        @include('ppdb.includes.public.slide_banner')
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 xb">
                                <div class="widget-list">
                                    <div class="widget-body clearfix">
                                        @yield('ppdb.components')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @break
            @endswitch
            <div id="myBtn" title="Go to top"><i class="list-icon material-icons">keyboard_arrow_up</i></button>
        </main>

        @include('ppdb.includes.public.footer')
    </div>
    @include('ppdb.includes.public.foot')
</body>

</html>
