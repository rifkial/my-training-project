<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('ppdb.includes.head')
</head>

<body class="sidebar-light sidebar-expand ">
    <div id="wrapper" class="wrapper">
        @switch (session('role'))
            @case('admin-ppdb')
                <nav class="navbar admin">
                    <div class="navbar-header admin">
                        <a href="{{ route('dashboard-ppdb') }} " class="navbar-brand admin">
                            <img class="logo-expand" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                                style="max-width: 25%;">
                            <img class="logo-collapse" alt="" src="{{ session('logo') ?? asset('asset/img/sma.png') }}"
                                style="max-width: 50%;">
                            <span class="text-white logo-expand"> E-PPDB </span>
                            <span class="text-white logo-collapse"> E-PPDB </span>
                        </a>
            @break
            @case('peserta-ppdb')
                <nav class="navbar user">
                    <div class="navbar-header user">
                        <a href="{{ route('user-ppdb') }} " class="navbar-brand user">
                            <img class="logo-expand" alt="" src="{{ asset('asset/img/sma.png') }}"
                                style="max-width: 25%;">
                            <img class="logo-collapse" alt="" src="{{ asset('asset/img/sma.png') }}"
                                style="max-width: 50%;">
                            <span class="text-white logo-expand"> E-PPDB </span>
                            <span class="text-white logo-collapse"> E-PPDB </span>
                        </a>
            @break
        @endswitch
    </div>
    <div class="spacerx" style="margin-left: 32px;"></div>
    <ul class="nav navbar-nav">
        <li class="sidebar-toggle">
            <a href="javascript:void(0)" class="ripple">
                <i class="material-icons list-icon">menu</i>
            </a>
        </li>
    </ul>
    <div class="spacer"></div>
    @include('ppdb.includes.profile_header')
    </nav>
    <div class="content-wrapper">
        @switch (session('role'))
            @case('admin-ppdb')
                <aside class="site-sidebar admin scrollbar-enabled clearfix">
            @break
            @case('peserta-ppdb')
                <aside class="site-sidebar user scrollbar-enabled clearfix">
            @break
        @endswitch
        @include('ppdb.includes.navbar')
        </aside>
        <main class="main-wrapper clearfix">
            <div class="row page-title clearfix" style="height: 0;">
            </div>
            <div class="widget-list">
                @yield('ppdb.components')
            </div>
        </main>
    </div>
    @include('ppdb.includes.footer')
    </div>
    @include('ppdb.includes.foot')
</body>

</html>
