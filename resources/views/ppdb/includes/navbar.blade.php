<!-- User Details -->
<!-- Sidebar Menu -->
<nav class="sidebar-nav">
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <img src="{{ session('avatar') }}" class="media-object rounded-circle" alt="">
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase">{{ session('username') }}</h4><span
                    class="user-type fs-12">{{ session('role') }}</span>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <ul class="nav in side-menu">
        @switch (session('role'))
            @case('admin-ppdb')

                <li><a href="{{ route('dashboard-ppdb') }} " class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">dashboard</i> <span class="hide-menu">Dashboard</span></a>
                </li>

                <li><a href="{{ route('administrator') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">supervisor_account</i> <span
                            class="hide-menu">User</span></a>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons">perm_contact_calendar</i> <span
                            class="hide-menu">
                            Peserta</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('akun-pendaftar') }}">- Pendaftar </a>
                        </li>
                        <li><a href="{{ route('doc-support') }}">- Dokumen Pendukung </a>
                        </li>
                        <li><a href="{{ route('peserta_diterima') }}">- Pendaftar diterima </a>
                        </li>
                        <li><a href="{{ route('peserta_ditolak') }}">- Pendaftar ditolak </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons">payment</i> <span
                            class="hide-menu">Pembayaran</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('payment-confirm') }}">- Konfirmasi (Confirm)</a></li>
                        <li><a href="{{ route('payment-pending') }}">- Menunggu (Pending) </a></li>
                        <li><a href="{{ route('payment-paid') }}">- Diterima (Paid)</a></li>
                        <li><a href="{{ route('payment-reject') }}">- Dibatalkan (Cancel)</a></li>
                    </ul>
                </li>

                <li><a href="{{ route('pesan-admin') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">message</i>
                        <span class="hide-menu">Pesan</span></a>
                </li>

                <li><a href="{{ route('informasi-pengumuman') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">rss_feed</i>
                        <span class="hide-menu">Pengumuman</span></a>
                </li>

                <li class="menu-item-has-children "><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons">playlist_add</i> <span
                            class="hide-menu">Informasi</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('informasi-banner') }}">- Banner </a>
                        </li>
                        <li><a href="{{ route('informasi-alur') }}">- Alur Pendaftaran </a>
                        </li>
                        <li><a href="{{ route('informasi-syarat') }}">- Syarat Pendaftaran</a>
                        </li>
                        <li><a href="{{ route('informasi-jadwal') }}">- Jadwal Pendaftaran</a>
                        </li>
                        <li><a href="{{ route('informasi-panduan') }}">- Panduan Pendaftaran </a>
                        </li>
                    </ul>
                </li>

                <li class="admin menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons"><span class="material-icons">
                                attach_file
                            </span></i> <span class="hide-menu">Download</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('brosur-ppdb') }}">- Brosur PPDB</a>
                        </li>
                        <li><a href="{{ route('sample-doc') }}">- Sample Pendaftaran PPDB</a>
                        </li>
                    </ul>
                </li>

                <li><a href="{{ route('informasi-faq') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">help</i>
                        <span class="hide-menu">FAQ</span></a>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons">tune</i> <span
                            class="hide-menu">Pengaturan</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('setting-ppdb') }}">- Pengaturan PPDB</a></li>
                        <li><a href="{{ route('setting-form') }}">- Formulir Pendaftaran</a></li>
                        <li><a href="{{ route('template-surat') }}">- Template Surat</a></li>
                        <li><a href="{{ route('setting-pembayaran') }}">- Pembayaran</a></li>
                        <li><a href="{{ route('template-kartu') }}">- Template Kartu</a></li>
                        <li><a href="{{ route('setting-sambutan') }}">- Sambutan</a></li>
                    </ul>
                </li>
            @break
            @case('peserta-ppdb')
                <li><a href="{{ route('user-ppdb') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon material-icons">rss_feed</i><span
                            class="hide-menu text-uppercase">Dashboard</span></a>
                </li>
                <li><a href="{{ route('user-pengumuman') }}" class="ripple" aria-expanded="false"><i
                            class="list-icon fa fa-bullhorn"></i>
                        <span class="hide-menu text-uppercase">Pengumuman</span></a>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon material-icons">playlist_add</i> <span
                            class="hide-menu info text-uppercase">Informasi</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('alur-pendaftaran-user') }}">- Alur Pendaftaran </a>
                        </li>
                        <li><a href="{{ route('syarat-pendaftaran-user') }}">- Syarat Pendaftaran</a>
                        </li>
                        <li><a href="{{ route('jadwal-pendaftaran-user') }}">- Jadwal Kegiatan</a>
                        </li>
                        <li><a href="{{ route('panduan-pendaftaran-user') }}">- Panduan Pendaftaran </a>
                        </li>
                        <li><a href="{{ route('faq-user') }}">- FAQ</a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon fa fa-wpforms"></i><span
                            class="hide-menu single text-uppercase">Pendaftaran</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('formulir-pendaftaran') }}">- Formulir Pendaftaran </a>
                        </li>
                        <li><a href="{{ route('upload-file') }}">- Upload Dokumen Pendukung</a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon fa fa-print"></i> <span
                            class="hide-menu print text-uppercase">Cetak</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('print-form-user') }}">- Cetak Form Pendaftaran </a>
                        </li>
                        <li><a href="{{ route('print-card-user') }}">- Cetak Kartu Peserta</a>
                        </li>
                        <li><a href="{{ route('print_pengumuman-user') }}">- Cetak Hasil Pengumuman</a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon fa fa-cloud-download"></i> <span
                            class="hide-menu download text-uppercase">Download</span></a>
                    <ul class="list-unstyled sub-menu collapse" aria-expanded="false" style="">
                        <li><a href="{{ route('user-download-brosur') }}">- Download Brosur </a>
                        </li>
                        <li><a href="{{ route('user-download-file') }}">- Download Contoh Dokumen</a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="list-icon fa fa-inbox"></i> <span
                            class="hide-menu pesan text-uppercase ">Pesan</span></a>
                    <ul class="list-unstyled sub-menu" aria-expanded="false" style="">
                        <li><a href="{{ route('pesan-user') }}">- Kirim Pesan </a>
                        </li>
                        @if (session('whatapps') != null)
                        <li><a href="https://api.whatsapp.com/send?phone={{ session('whatapps') ?? '' }}&text=help_ppdb_{{ session('username') }}" target="_blank">- Chat WA</a>
                        </li>
                        @endif
                        @if (session('telepone_cs') != null)
                        <li><a href="tel:{{ session('telepone_cs') }}">- Telepone</a>
                        </li>
                        @endif

                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="javascript:void(0);" class="ripple"
                        aria-expanded="true"><i class="material-icons list-icon">payment</i> <span
                            class="hide-menu pesan text-uppercase">Pembayaran</span></a>
                    <ul class="list-unstyled sub-menu" aria-expanded="false" style="">
                        <li><a href="{{ route('history-pembayaran') }}">-Tagihan Pembayaran</a></li>
                    </ul>
                </li>
            @break
        @endswitch
        <li><a href="{{ route('auth.logout') }}"><i class="list-icon material-icons">settings_power</i> <span
                    class="hide-menu">Log Out</span></a>
        </li>
    </ul>
    <!-- /.side-menu -->
</nav>
<!-- /.sidebar-nav -->
