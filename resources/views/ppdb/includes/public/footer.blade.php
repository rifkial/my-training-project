@if (session('role') != 'superadmin')
    <div class="footer text-center  text-white clearfix fix_bottom">
        {{ !empty(Session::get('footer')) ? Session::get('footer') : '2021 © ' . strtoupper(session('sekolah')) }}
    </div>
@else
    <div class="footer text-center text-white clearfix fix_bottom">2021 © Tim Dev Bumitekno</div>
@endif
