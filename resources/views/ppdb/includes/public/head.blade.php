<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="msapplication-starturl" content="/">
<meta name="theme-color" content="#e5e5e5">
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" sizes="20x20" href="{{ session('logo') ?? asset('asset/img/sma.png') }}">
{{-- <title>E-Learning MYSCH.ID</title> --}}
<title>{{ !empty(Session::get('title')) ? session('title') : 'Demo Smartschool' }}</title>
<!-- CSS -->
<link rel="stylesheet" href="{{ asset('asset/css/pace.css') }}">
<link href="{{ asset('asset/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css"
    rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet"
    type="text/css">
<link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css">
<!-- css custom -->
<style type="text/css">
    .navbar-brand {
        padding: 0;
        height: 5.625rem;
        text-align: center;
        width: 100%;
        font-size: 1.5rem;
        font-weight: 700;
        background: #0a5429;
        color: #fff;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .navbar {
        border-radius: 0;
        border: 0;
        background: #0a5429;
        width: 100%;
        margin-bottom: 0;
        -webkit-box-shadow: 0 2px 2px 0 rgb(0 0 0 / 10%);
        box-shadow: 0 2px 2px 0 rgb(0 0 0 / 10%);
        position: relative;
        z-index: 99;
        height: 5.625rem;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
        padding: 0;
    }

    .side-menu :not([class*="color-"])>.list-icon {
        color: #607d8b;
    }

    .color-white,
    .text-body {
        color: #555 !important;
    }

    .card.well {
        background-color: rgb(0 0 0 / 10%);
    }

    .banner_slide {}

    .carousel.slick-slider .slick-slide img {
        width: 100%;
        height: 300px;
    }

    .widget-bg.banner {
        background: transparent;
    }

    @media screen and (min-width: 961px) {
        .x-banner {
            margin-top: 1.5rem !important;
            background-color: #610b91;
        }

        .xb{
            margin-top:-6rem !important
        }

        .site-sidebar.x {
            position: relative;
            z-index: 99999;
            background: transparent;
            border-right: 0px solid #51d2b7;
            padding-bottom: 2.14286em;
            padding: 0 1.07143em;
            -webkit-box-shadow: 0 0px 0px 0 rgb(0 0 0 / 10%);
            box-shadow: 0 0px 0px 0 rgb(0 0 0 / 10%);
            position: relative;
            width: 100%;
        }

        .valign {
            position: relative;
            transform: translateY(-50%);
            z-index: 99999;
        }

        .nav-top {
            margin-top: -46px;
            margin-bottom: 46px;
            margin-left: 330px;
            margin-right: 10px;
            background: #0a5429;
            float: right;
        }

        .custom_layout {
            position: fixed;
            margin-top: -6rem !important;
        }

        .navbar_fixed {
            position: absolute;
        }

        .side-menu>li>a {
            padding-left: 1em;
            font-size: 1.15385em;
            line-height: 3.2em;
            color: #efebeb;
            border-left: 3px solid transparent;
            font-family: "Nunito Sans", sans-serif;
            font-weight: 600;
            white-space: nowrap;
        }

        .body_fix {
            position: relative;
        }

        .side-menu :not([class*="color-"])>.list-icon {
            color: #f1f5f7;
        }

        .side-menu li:hover,
        .side-menu li.active {
            background: rgb(255 255 255 / 15%);
        }

        .side-menu>li>a:hover,
        .side-menu>li>a:focus {
            color: #5c5a5a;
            background: transparent;
        }

        ul.list-unstyled.sub-menu.collapse:before {
            position: absolute;
            top: -8px;
            left: 84px;
            display: inline-block;
            border-right: 9px solid transparent;
            border-bottom: 9px solid #f1f5f7;
            border-left: 9px solid transparent;
            content: '';
        }
    }

    @media (min-width: 961px) {
        .sidebar-horizontal .fix-top .site-sidebar {
            position: fixed;
            top: 93px;
        }
    }


    @media screen and (max-width: 960px) {
        .x-banner {
            margin-top: 1.5rem !important;
            background-color: #610b91;
        }

        .sidebar-toggle::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 0;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 40%;
            width: 1px;
            background: #0a5429;
        }
    }

    header {
        position: sticky;
        top: 0;
        z-index: 999999;
    }

    .footer.fix_bottom {
        padding-top: 2px;
        padding-bottom: 0px;
        position: fixed;
        bottom: 0;
        width: 100%;
        background: #0a5429;
    }

    #myBtn {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Fixed/sticky position */
        bottom: 4px;
        /* Place the button at the bottom of the page */
        right: 8px;
        /* Place the button 30px from the right */
        z-index: 99;
        /* Make sure it does not overlap */
        border: none;
        /* Remove borders */
        outline: none;
        /* Remove outline */
        background-color: blue;
        /* Set a background color */
        color: white;
        /* Text color */
        cursor: pointer;
        /* Add a mouse pointer on hover */
        padding: 10px;
        /* Some padding */
        border-radius: 10px;
        /* Rounded corners */
        font-size: 18px;
        /* Increase font size */
    }

    #myBtn:hover {
        background-color: #555;
        /* Add a dark-grey background on hover */
    }

    .pengumuman:hover{
        background-color: seashell;
    }

</style>
<!-- Head Libs -->
<script type="text/javascript">
    //disabled pace loading
    window.paceOptions = {
        ajax: false,
        restartOnRequestAfter: false,
    };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
